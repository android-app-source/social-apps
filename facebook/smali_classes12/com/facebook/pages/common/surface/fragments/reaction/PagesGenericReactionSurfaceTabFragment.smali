.class public Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/8EK;
.implements LX/HOo;
.implements LX/145;
.implements LX/HPA;
.implements LX/HPB;
.implements LX/GZf;
.implements LX/CfG;
.implements LX/0o8;


# instance fields
.field public A:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/HQr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/HLl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/HPX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/CfK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/CSN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/2U1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/8Do;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/8Fe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private L:LX/CfJ;

.field private M:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field private N:LX/HBS;

.field private O:LX/HXi;

.field private P:LX/1PT;

.field public Q:LX/HBQ;

.field public R:LX/1B1;

.field public S:LX/1B1;

.field public T:LX/1B1;

.field public U:LX/0Yb;

.field private V:LX/HQq;

.field private W:LX/8EI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private X:Z

.field public Y:J

.field public Z:LX/HPn;

.field public a:LX/HXj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aa:LX/CZd;

.field private ab:LX/HPW;

.field public ac:Z

.field public ad:Z

.field public ae:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private af:Z

.field private ag:Z

.field private ah:Z

.field private ai:Z

.field public aj:Landroid/os/ParcelUuid;

.field public ak:Z

.field public al:Z

.field private am:Z

.field private an:I

.field private ao:I

.field public i:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/BPq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1vi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/HOq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/HOx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/HPo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/99v;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HMH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Tx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2461577
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;-><init>()V

    .line 2461578
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->X:Z

    .line 2461579
    iput-boolean v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ad:Z

    .line 2461580
    iput v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->an:I

    .line 2461581
    return-void
.end method

.method private P()LX/HBS;
    .locals 8

    .prologue
    .line 2461582
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->N:LX/HBS;

    if-nez v0, :cond_1

    .line 2461583
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Q:LX/HBQ;

    if-nez v0, :cond_0

    .line 2461584
    const-class v0, LX/HQx;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HQx;

    .line 2461585
    if-eqz v0, :cond_0

    .line 2461586
    invoke-interface {v0}, LX/HQx;->mm_()LX/HBQ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Q:LX/HBQ;

    .line 2461587
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->i:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    const-string v6, "page_only"

    iget-object v7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Q:LX/HBQ;

    invoke-static/range {v0 .. v7}, LX/HBS;->a(JJLjava/lang/String;Landroid/os/ParcelUuid;Ljava/lang/String;LX/HBQ;)LX/HBS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->N:LX/HBS;

    .line 2461588
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->N:LX/HBS;

    return-object v0
.end method

.method private Q()Z
    .locals 1

    .prologue
    .line 2461589
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ah:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->W:LX/8EI;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(JZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/ParcelUuid;ZLjava/lang/String;ZLjava/lang/String;LX/8YB;)Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;
    .locals 6
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 2461330
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2461331
    new-instance v2, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    invoke-direct {v2}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;-><init>()V

    .line 2461332
    const-string v3, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v3, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2461333
    const-string v3, "extra_should_enable_related_pages_like_chaining"

    invoke-virtual {v1, v3, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2461334
    const-string v3, "arg_should_support_cache"

    invoke-virtual {v1, v3, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2461335
    const-string v3, "arg_pages_surface_reaction_surface"

    invoke-virtual {v1, v3, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461336
    const-string v3, "arg_precreated_reaction_session_id"

    invoke-virtual {v1, v3, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461337
    const-string v3, "arg_precreated_cached_reaction_session_id"

    invoke-virtual {v1, v3, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461338
    const-string v3, "page_fragment_uuid"

    invoke-virtual {v1, v3, p7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2461339
    const-string v3, "extra_is_landing_fragment"

    invoke-virtual {v1, v3, p8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2461340
    const-string v3, "source_name"

    invoke-virtual {v1, v3, p9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461341
    const-string v3, "extra_is_inside_page_surface_tab"

    move/from16 v0, p10

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2461342
    const-string v3, "extra_page_view_referrer"

    move-object/from16 v0, p11

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461343
    const-string v3, "empty_view"

    move-object/from16 v0, p12

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2461344
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2461345
    return-object v2
.end method

.method private static a(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;LX/HXj;Ljava/lang/String;LX/1Ck;LX/0kL;LX/BPq;LX/0bH;LX/1vi;LX/HOq;LX/HOx;LX/9XE;LX/HPo;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Xl;LX/HQr;LX/HLl;LX/HPX;LX/CfK;LX/CSN;LX/0if;LX/2U1;LX/0ad;LX/8Do;LX/8Fe;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;",
            "LX/HXj;",
            "Ljava/lang/String;",
            "LX/1Ck;",
            "LX/0kL;",
            "LX/BPq;",
            "LX/0bH;",
            "LX/1vi;",
            "LX/HOq;",
            "LX/HOx;",
            "LX/9XE;",
            "LX/HPo;",
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/99v;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HMH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3Tx;",
            ">;",
            "LX/0Xl;",
            "LX/HQr;",
            "LX/HLl;",
            "LX/HPX;",
            "LX/CfK;",
            "LX/CSN;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/2U1;",
            "LX/0ad;",
            "LX/8Do;",
            "LX/8Fe;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2461590
    iput-object p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->a:LX/HXj;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->i:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->j:LX/1Ck;

    iput-object p4, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->k:LX/0kL;

    iput-object p5, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->l:LX/BPq;

    iput-object p6, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->m:LX/0bH;

    iput-object p7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->n:LX/1vi;

    iput-object p8, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->o:LX/HOq;

    iput-object p9, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->p:LX/HOx;

    iput-object p10, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->q:LX/9XE;

    iput-object p11, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->r:LX/HPo;

    iput-object p12, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->s:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->t:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->u:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->v:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->w:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->x:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->y:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->z:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->A:LX/0Xl;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->B:LX/HQr;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->C:LX/HLl;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->D:LX/HPX;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->E:LX/CfK;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->F:LX/CSN;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->G:LX/0if;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->H:LX/2U1;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->I:LX/0ad;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->J:LX/8Do;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->K:LX/8Fe;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 33

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v32

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    const-class v3, LX/HXj;

    move-object/from16 v0, v32

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/HXj;

    invoke-static/range {v32 .. v32}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static/range {v32 .. v32}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static/range {v32 .. v32}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v6

    check-cast v6, LX/0kL;

    invoke-static/range {v32 .. v32}, LX/BPq;->a(LX/0QB;)LX/BPq;

    move-result-object v7

    check-cast v7, LX/BPq;

    invoke-static/range {v32 .. v32}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v8

    check-cast v8, LX/0bH;

    invoke-static/range {v32 .. v32}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v9

    check-cast v9, LX/1vi;

    invoke-static/range {v32 .. v32}, LX/HOq;->a(LX/0QB;)LX/HOq;

    move-result-object v10

    check-cast v10, LX/HOq;

    const-class v11, LX/HOx;

    move-object/from16 v0, v32

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/HOx;

    invoke-static/range {v32 .. v32}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v12

    check-cast v12, LX/9XE;

    const-class v13, LX/HPo;

    move-object/from16 v0, v32

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/HPo;

    const/16 v14, 0x3096

    move-object/from16 v0, v32

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x1061

    move-object/from16 v0, v32

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x19c6

    move-object/from16 v0, v32

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x259

    move-object/from16 v0, v32

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x542

    move-object/from16 v0, v32

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x1d67

    move-object/from16 v0, v32

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x2be4

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x10ba    # 6.0E-42f

    move-object/from16 v0, v32

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    invoke-static/range {v32 .. v32}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v22

    check-cast v22, LX/0Xl;

    const-class v23, LX/HQr;

    move-object/from16 v0, v32

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/HQr;

    invoke-static/range {v32 .. v32}, LX/HLl;->a(LX/0QB;)LX/HLl;

    move-result-object v24

    check-cast v24, LX/HLl;

    const-class v25, LX/HPX;

    move-object/from16 v0, v32

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v25

    check-cast v25, LX/HPX;

    const-class v26, LX/CfK;

    move-object/from16 v0, v32

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/CfK;

    invoke-static/range {v32 .. v32}, LX/CSN;->a(LX/0QB;)LX/CSN;

    move-result-object v27

    check-cast v27, LX/CSN;

    invoke-static/range {v32 .. v32}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v28

    check-cast v28, LX/0if;

    invoke-static/range {v32 .. v32}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v29

    check-cast v29, LX/2U1;

    invoke-static/range {v32 .. v32}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v30

    check-cast v30, LX/0ad;

    invoke-static/range {v32 .. v32}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v31

    check-cast v31, LX/8Do;

    invoke-static/range {v32 .. v32}, LX/8Fe;->a(LX/0QB;)LX/8Fe;

    move-result-object v32

    check-cast v32, LX/8Fe;

    invoke-static/range {v2 .. v32}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->a(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;LX/HXj;Ljava/lang/String;LX/1Ck;LX/0kL;LX/BPq;LX/0bH;LX/1vi;LX/HOq;LX/HOx;LX/9XE;LX/HPo;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Xl;LX/HQr;LX/HLl;LX/HPX;LX/CfK;LX/CSN;LX/0if;LX/2U1;LX/0ad;LX/8Do;LX/8Fe;)V

    return-void
.end method


# virtual methods
.method public final E_(I)V
    .locals 2

    .prologue
    .line 2461591
    iput p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->an:I

    .line 2461592
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2461593
    if-eqz v0, :cond_0

    .line 2461594
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2461595
    check-cast v0, LX/HOw;

    iget v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->an:I

    invoke-virtual {v0, v1}, LX/HOw;->f(I)V

    .line 2461596
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2461597
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2461598
    :cond_0
    return-void
.end method

.method public final G()LX/1SX;
    .locals 3

    .prologue
    .line 2461599
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->O:LX/HXi;

    if-nez v0, :cond_0

    .line 2461600
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->a:LX/HXj;

    invoke-direct {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->P()LX/HBS;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/HXj;->a(LX/5SB;LX/1Qj;)LX/HXi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->O:LX/HXi;

    .line 2461601
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->O:LX/HXi;

    .line 2461602
    iput-object p0, v0, LX/HXi;->m:Landroid/support/v4/app/Fragment;

    .line 2461603
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    sget-object v1, LX/8A3;->CREATE_CONTENT:LX/8A3;

    invoke-virtual {v0, v1}, LX/CZd;->a(LX/8A3;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2461604
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->O:LX/HXi;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    invoke-virtual {v1}, LX/CZd;->n()Ljava/lang/String;

    move-result-object v1

    .line 2461605
    iput-object v1, v0, LX/HXi;->n:Ljava/lang/String;

    .line 2461606
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->O:LX/HXi;

    return-object v0
.end method

.method public final H()LX/1PT;
    .locals 2

    .prologue
    .line 2461607
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->P:LX/1PT;

    if-nez v0, :cond_0

    .line 2461608
    new-instance v0, LX/FuR;

    invoke-direct {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->P()LX/HBS;

    move-result-object v1

    invoke-direct {v0, v1}, LX/FuR;-><init>(LX/5SB;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->P:LX/1PT;

    .line 2461609
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->P:LX/1PT;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)LX/E8m;
    .locals 10

    .prologue
    .line 2461700
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->p:LX/HOx;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->H()LX/1PT;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->G()LX/1SX;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->H:LX/2U1;

    iget-wide v6, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2U1;->c(Ljava/lang/String;)LX/8Dk;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 2461701
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2461702
    const-string v2, "empty_view"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, LX/8YB;

    iget-wide v8, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    move-object v2, p1

    move-object v6, p0

    invoke-virtual/range {v1 .. v9}, LX/HOx;->a(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Boolean;LX/0o8;LX/8YB;J)LX/HOw;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2461610
    const-string v0, "page_reaction_fragment"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2461611
    iput p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ao:I

    .line 2461612
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2461613
    iget v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ao:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2461614
    return-void
.end method

.method public final a(LX/8EI;)V
    .locals 0

    .prologue
    .line 2461615
    iput-object p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->W:LX/8EI;

    .line 2461616
    return-void
.end method

.method public final a(LX/CZd;)V
    .locals 2

    .prologue
    .line 2461617
    iput-object p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    .line 2461618
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    sget-object v1, LX/8A3;->CREATE_CONTENT:LX/8A3;

    invoke-virtual {v0, v1}, LX/CZd;->a(LX/8A3;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->O:LX/HXi;

    if-eqz v0, :cond_0

    .line 2461619
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->O:LX/HXi;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    invoke-virtual {v1}, LX/CZd;->n()Ljava/lang/String;

    move-result-object v1

    .line 2461620
    iput-object v1, v0, LX/HXi;->n:Ljava/lang/String;

    .line 2461621
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 2461622
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2461623
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->K:LX/8Fe;

    invoke-virtual {v0}, LX/8Fe;->a()V

    .line 2461624
    const/4 v11, 0x0

    .line 2461625
    iget-object v7, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v7, v7

    .line 2461626
    const-string v8, "com.facebook.katana.profile.id"

    const-wide/16 v9, -0x1

    invoke-virtual {v7, v8, v9, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    .line 2461627
    iget-object v7, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v7, v7

    .line 2461628
    const-string v8, "extra_should_enable_related_pages_like_chaining"

    invoke-virtual {v7, v8, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ac:Z

    .line 2461629
    iget-object v7, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v7, v7

    .line 2461630
    const-string v8, "arg_should_support_cache"

    invoke-virtual {v7, v8, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ad:Z

    .line 2461631
    iget-object v7, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v7, v7

    .line 2461632
    const-string v8, "arg_pages_surface_reaction_surface"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ae:Ljava/lang/String;

    .line 2461633
    iget-object v7, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v7, v7

    .line 2461634
    const-string v8, "extra_is_landing_fragment"

    invoke-virtual {v7, v8, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ak:Z

    .line 2461635
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2461636
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->W:LX/8EI;

    .line 2461637
    iget-object v1, v0, LX/8EI;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x13007d

    const/16 v3, 0x52

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 2461638
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2461639
    const-string v1, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    .line 2461640
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->D:LX/HPX;

    iget-wide v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2461641
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2461642
    const-string v3, "arg_precreated_reaction_session_id"

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2461643
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2461644
    const-string v4, "arg_precreated_cached_reaction_session_id"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/HP0;

    invoke-direct {v4, p0}, LX/HP0;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;)V

    iget-boolean v6, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ah:Z

    if-eqz v6, :cond_1

    iget-object v5, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->W:LX/8EI;

    :cond_1
    invoke-virtual/range {v0 .. v5}, LX/HPX;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/HP0;LX/8EI;)LX/HPW;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ab:LX/HPW;

    .line 2461645
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2461646
    const-string v1, "page_fragment_uuid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    .line 2461647
    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aj:Landroid/os/ParcelUuid;

    .line 2461648
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->R:LX/1B1;

    .line 2461649
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->S:LX/1B1;

    .line 2461650
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->T:LX/1B1;

    .line 2461651
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->R:LX/1B1;

    new-instance v1, LX/HP9;

    invoke-direct {v1, p0}, LX/HP9;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2461652
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->R:LX/1B1;

    new-instance v1, LX/2zK;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->k()Landroid/os/ParcelUuid;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/2zK;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;Landroid/os/ParcelUuid;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2461653
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->R:LX/1B1;

    new-instance v1, LX/HP5;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->k()Landroid/os/ParcelUuid;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/HP5;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;Landroid/os/ParcelUuid;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2461654
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->S:LX/1B1;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->o:LX/HOq;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2461655
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ae:Ljava/lang/String;

    invoke-static {v0}, LX/2s8;->i(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2461656
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->T:LX/1B1;

    new-instance v1, LX/HP6;

    invoke-direct {v1, p0}, LX/HP6;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2461657
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->T:LX/1B1;

    new-instance v1, LX/HP7;

    invoke-direct {v1, p0}, LX/HP7;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2461658
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->A:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    new-instance v2, LX/HOy;

    invoke-direct {v2, p0}, LX/HOy;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->U:LX/0Yb;

    .line 2461659
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->U:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2461660
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2461661
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ac:Z

    if-eqz v0, :cond_3

    .line 2461662
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->B:LX/HQr;

    iget-wide v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aj:Landroid/os/ParcelUuid;

    invoke-virtual {v0, v1, p0, v2}, LX/HQr;->a(Ljava/lang/String;Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;Landroid/os/ParcelUuid;)LX/HQq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->V:LX/HQq;

    .line 2461663
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->V:LX/HQq;

    .line 2461664
    iget-object v1, v0, LX/HQq;->f:LX/HQo;

    iget-object v2, v0, LX/HQq;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/HQo;->a(Ljava/lang/String;)V

    .line 2461665
    iget-object v1, v0, LX/HQq;->h:LX/CXj;

    iget-object v2, v0, LX/HQq;->i:LX/CXo;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2461666
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ak:Z

    if-eqz v0, :cond_4

    .line 2461667
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->G:LX/0if;

    sget-object v1, LX/0ig;->an:LX/0ih;

    const-string v2, "landing_tab_created"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2461668
    :cond_4
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2461669
    const-string v1, "extra_is_inside_page_surface_tab"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->am:Z

    .line 2461670
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->am:Z

    if-nez v0, :cond_5

    .line 2461671
    iget-object v7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->r:LX/HPo;

    iget-wide v9, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    sget-object v9, LX/89z;->PAGE:LX/89z;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, p0, v8, v9, v10}, LX/HPo;->a(LX/145;Ljava/lang/Long;LX/89z;Ljava/lang/String;)LX/HPn;

    move-result-object v7

    iput-object v7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Z:LX/HPn;

    .line 2461672
    new-instance v7, LX/CZd;

    iget-wide v9, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    const/4 v8, 0x0

    invoke-direct {v7, v9, v10, v8}, LX/CZd;-><init>(JLandroid/location/Location;)V

    iput-object v7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    .line 2461673
    new-instance v7, LX/HP1;

    invoke-direct {v7, p0}, LX/HP1;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;)V

    iput-object v7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Q:LX/HBQ;

    .line 2461674
    :cond_5
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    .line 2461675
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->M:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2461676
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->M:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->D()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2461677
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;LX/HPm;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
            ">;",
            "LX/HPm;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2461678
    if-eqz p1, :cond_0

    .line 2461679
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2461680
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    if-nez v0, :cond_1

    .line 2461681
    :cond_0
    :goto_0
    return-void

    .line 2461682
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    .line 2461683
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2461684
    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 2461685
    iget-object v2, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v2

    .line 2461686
    invoke-virtual {v1, v0, v2}, LX/CZd;->a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;LX/0ta;)V

    .line 2461687
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2461688
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2461689
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->b()V

    .line 2461690
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->a(LX/CZd;)V

    .line 2461691
    new-instance v0, LX/8A4;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    .line 2461692
    iget-object v2, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v2

    .line 2461693
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8A4;-><init>(Ljava/util/List;)V

    .line 2461694
    sget-object v1, LX/8A3;->BASIC_ADMIN:LX/8A3;

    invoke-virtual {v0, v1}, LX/8A4;->a(LX/8A3;)Z

    move-result v0

    .line 2461695
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aa:LX/CZd;

    .line 2461696
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2461697
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/8A4;->c(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2461698
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->K:LX/8Fe;

    iget-wide v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8Fe;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2461575
    iput-object p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->M:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2461576
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2461699
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2461328
    iput-boolean p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ah:Z

    .line 2461329
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2461346
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->af:Z

    .line 2461347
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->af:Z

    .line 2461348
    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ag:Z

    if-eqz v0, :cond_0

    .line 2461349
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->kL_()V

    .line 2461350
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ag:Z

    .line 2461351
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2461352
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->am:Z

    if-nez v0, :cond_0

    .line 2461353
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->c(Ljava/lang/String;)V

    .line 2461354
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2461355
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->af:Z

    .line 2461356
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2461357
    iput-boolean v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ai:Z

    .line 2461358
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2iz;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    .line 2461359
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ag:Z

    .line 2461360
    iput-boolean v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->X:Z

    .line 2461361
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->y()V

    .line 2461362
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->z()V

    .line 2461363
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2461364
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v1, v1

    .line 2461365
    invoke-virtual {v0, v1}, LX/E8m;->b(LX/2jY;)V

    .line 2461366
    return-void
.end method

.method public final k()Landroid/os/ParcelUuid;
    .locals 1

    .prologue
    .line 2461367
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->aj:Landroid/os/ParcelUuid;

    return-object v0
.end method

.method public final kJ_()Z
    .locals 1

    .prologue
    .line 2461368
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->X:Z

    return v0
.end method

.method public final kK_()V
    .locals 1

    .prologue
    .line 2461369
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->kK_()V

    .line 2461370
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2461371
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->W:LX/8EI;

    invoke-virtual {v0}, LX/8EI;->f()V

    .line 2461372
    :cond_0
    return-void
.end method

.method public final kL_()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2461373
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->af:Z

    if-nez v0, :cond_0

    .line 2461374
    iput-boolean v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ag:Z

    .line 2461375
    :goto_0
    return-void

    .line 2461376
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2461377
    if-eqz v0, :cond_3

    .line 2461378
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v0, v0

    .line 2461379
    invoke-virtual {v0}, LX/2jY;->z()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2461380
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2461381
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->W:LX/8EI;

    invoke-virtual {v0}, LX/8EI;->d()V

    .line 2461382
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ak:Z

    if-eqz v0, :cond_2

    .line 2461383
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->G:LX/0if;

    sget-object v1, LX/0ig;->an:LX/0ih;

    const-string v2, "page_data_loaded"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2461384
    :cond_2
    iput-boolean v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->al:Z

    .line 2461385
    :cond_3
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->kL_()V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    .prologue
    .line 2461386
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2461387
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2461388
    :goto_0
    return-void

    .line 2461389
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HMH;

    invoke-virtual {v0, p1}, LX/HMH;->a(I)LX/HMI;

    move-result-object v0

    .line 2461390
    if-eqz v0, :cond_2

    .line 2461391
    invoke-interface {v0}, LX/HMI;->a()LX/4At;

    move-result-object v1

    .line 2461392
    if-eqz v1, :cond_1

    .line 2461393
    invoke-virtual {v1}, LX/4At;->beginShowingProgress()V

    .line 2461394
    :cond_1
    iget-wide v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    const/4 v5, 0x0

    move-object v2, v0

    move-object v6, p0

    move-object v7, p3

    move v8, p1

    invoke-interface/range {v2 .. v8}, LX/HMI;->a(JLX/8A4;Lcom/facebook/base/fragment/FbFragment;Landroid/content/Intent;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2461395
    if-nez v2, :cond_4

    .line 2461396
    :goto_1
    goto :goto_0

    .line 2461397
    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2461398
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    if-nez p3, :cond_5

    .line 2461399
    :cond_3
    :goto_2
    goto :goto_0

    .line 2461400
    :cond_4
    new-instance v3, LX/HP3;

    invoke-direct {v3, p0, v1, v0}, LX/HP3;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;LX/4At;LX/HMI;)V

    .line 2461401
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->j:LX/1Ck;

    const-string v4, "pages_activity_result_handler"

    new-instance v5, LX/HP4;

    invoke-direct {v5, p0, v2}, LX/HP4;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v1, v4, v5, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_1

    .line 2461402
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x6de
        :pswitch_0
    .end packed-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2461403
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2461404
    if-eqz v0, :cond_0

    .line 2461405
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2461406
    invoke-virtual {v0, p1}, LX/E8m;->a(Landroid/content/res/Configuration;)V

    .line 2461407
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x334d8eed    # -9.355484E7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461408
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x50719f29

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2ccd9589

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461409
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onDestroy()V

    .line 2461410
    iget-boolean v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ac:Z

    if-eqz v1, :cond_0

    .line 2461411
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->V:LX/HQq;

    .line 2461412
    iget-object v2, v1, LX/HQq;->f:LX/HQo;

    iget-object v3, v1, LX/HQq;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/HQo;->a(Ljava/lang/String;)V

    .line 2461413
    iget-object v2, v1, LX/HQq;->h:LX/CXj;

    iget-object v3, v1, LX/HQq;->i:LX/CXo;

    invoke-virtual {v2, v3}, LX/0b4;->b(LX/0b2;)Z

    .line 2461414
    iget-object v2, v1, LX/HQq;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2461415
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->C:LX/HLl;

    iget-wide v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2461416
    iget-object v3, v1, LX/HLl;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2461417
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Z:LX/HPn;

    if-eqz v1, :cond_1

    .line 2461418
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Z:LX/HPn;

    invoke-virtual {v1}, LX/HPn;->a()V

    .line 2461419
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->K:LX/8Fe;

    invoke-virtual {v1}, LX/8Fe;->b()V

    .line 2461420
    const/16 v1, 0x2b

    const v2, 0x5b4f228c

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5d36e98b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461421
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onDestroyView()V

    .line 2461422
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->j:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2461423
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->U:LX/0Yb;

    if-eqz v1, :cond_0

    .line 2461424
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->U:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2461425
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2461426
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v1

    invoke-virtual {v1}, LX/2ja;->e()V

    .line 2461427
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x2179fe6b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4fd1180f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461428
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onPause()V

    .line 2461429
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->R:LX/1B1;

    if-eqz v1, :cond_0

    .line 2461430
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->R:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->l:LX/BPq;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2461431
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->S:LX/1B1;

    if-eqz v1, :cond_1

    .line 2461432
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->S:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->m:LX/0bH;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2461433
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->T:LX/1B1;

    if-eqz v1, :cond_2

    .line 2461434
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->T:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->n:LX/1vi;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2461435
    :cond_2
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Q()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2461436
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->W:LX/8EI;

    invoke-virtual {v1}, LX/8EI;->e()V

    .line 2461437
    :cond_3
    const/16 v1, 0x2b

    const v2, 0x59c50c6c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4c2488ed    # 4.3131828E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461438
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onResume()V

    .line 2461439
    iget-boolean v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->am:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Z:LX/HPn;

    if-eqz v1, :cond_0

    .line 2461440
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Z:LX/HPn;

    invoke-virtual {v1}, LX/HPn;->b()V

    .line 2461441
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->R:LX/1B1;

    if-eqz v1, :cond_1

    .line 2461442
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->R:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->l:LX/BPq;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2461443
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->S:LX/1B1;

    if-eqz v1, :cond_2

    .line 2461444
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->S:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->m:LX/0bH;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2461445
    :cond_2
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->T:LX/1B1;

    if-eqz v1, :cond_3

    .line 2461446
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->T:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->n:LX/1vi;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2461447
    :cond_3
    const/16 v1, 0x2b

    const v2, 0x20d80b52

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2461448
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2461449
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->am:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Z:LX/HPn;

    if-eqz v0, :cond_0

    .line 2461450
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Z:LX/HPn;

    sget-object v1, LX/HPm;->DEFAULT:LX/HPm;

    invoke-virtual {v0, v1}, LX/HPn;->a(LX/HPm;)V

    .line 2461451
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2461452
    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVerticalScrollBarEnabled(Z)V

    .line 2461453
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2461454
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2461455
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->W:LX/8EI;

    .line 2461456
    new-instance v2, LX/8EH;

    invoke-direct {v2, v1}, LX/8EH;-><init>(LX/8EI;)V

    move-object v1, v2

    .line 2461457
    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->a(LX/0fu;)V

    .line 2461458
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->W:LX/8EI;

    .line 2461459
    iget-object v1, v0, LX/8EI;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x13007d

    const/16 p1, 0x65

    invoke-interface {v1, v2, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 2461460
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ak:Z

    if-eqz v0, :cond_2

    .line 2461461
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2461462
    new-instance v1, LX/HP2;

    invoke-direct {v1, p0}, LX/HP2;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->a(LX/0fu;)V

    .line 2461463
    :cond_2
    iget v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->an:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->E_(I)V

    .line 2461464
    iget v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ao:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->a(I)V

    .line 2461465
    return-void
.end method

.method public final v()LX/2jY;
    .locals 11

    .prologue
    .line 2461466
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2461467
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->W:LX/8EI;

    .line 2461468
    iget-object v1, v0, LX/8EI;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x13007d

    const/16 v3, 0x66

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 2461469
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ai:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ab:LX/HPW;

    .line 2461470
    iget-object v1, v0, LX/HPW;->m:LX/2jY;

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2461471
    if-eqz v0, :cond_1

    .line 2461472
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ab:LX/HPW;

    .line 2461473
    iget-object v4, v0, LX/HPW;->m:LX/2jY;

    if-nez v4, :cond_7

    .line 2461474
    const/4 v4, 0x0

    .line 2461475
    :goto_1
    move-object v0, v4

    .line 2461476
    :goto_2
    return-object v0

    .line 2461477
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2461478
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->W:LX/8EI;

    const-string v1, "SurfaceFirstCardFromEarlyFetcher"

    const-string v2, "false"

    invoke-virtual {v0, v1, v2}, LX/8EI;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461479
    :cond_2
    new-instance v0, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v0}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    iget-wide v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2461480
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    .line 2461481
    move-object v0, v0

    .line 2461482
    iget-wide v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->Y:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2461483
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2461484
    move-object v0, v0

    .line 2461485
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->J:LX/8Do;

    invoke-virtual {v1}, LX/8Do;->j()I

    move-result v1

    int-to-long v2, v1

    .line 2461486
    iput-wide v2, v0, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2461487
    move-object v0, v0

    .line 2461488
    sget-object v1, LX/0wD;->TIMELINE:LX/0wD;

    invoke-virtual {v1}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v1

    .line 2461489
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    .line 2461490
    move-object v0, v0

    .line 2461491
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2461492
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2461493
    move-object v0, v0

    .line 2461494
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ae:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2461495
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    .line 2461496
    move-object v0, v0

    .line 2461497
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a()Ljava/lang/String;

    move-result-object v1

    .line 2461498
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    .line 2461499
    move-object v1, v0

    .line 2461500
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ai:Z

    if-eqz v0, :cond_3

    const-string v0, "SubsequentLoad"

    .line 2461501
    :goto_3
    iput-object v0, v1, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    .line 2461502
    move-object v0, v1

    .line 2461503
    const-string v1, "page_reaction_initial_fetch_tag"

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    .line 2461504
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->z:LX/0Rf;

    .line 2461505
    move-object v0, v0

    .line 2461506
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->I:LX/0ad;

    sget-short v2, LX/8Dn;->j:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2461507
    iput-boolean v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->A:Z

    .line 2461508
    move-object v0, v0

    .line 2461509
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2461510
    const-string v2, "extra_page_view_referrer"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2461511
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    .line 2461512
    move-object v1, v0

    .line 2461513
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ad:Z

    if-eqz v0, :cond_5

    .line 2461514
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ai:Z

    if-nez v0, :cond_4

    .line 2461515
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->E:LX/CfK;

    new-instance v2, LX/HOz;

    invoke-direct {v2, p0}, LX/HOz;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;)V

    invoke-virtual {v0, p0, v2}, LX/CfK;->a(LX/CfG;LX/CfI;)LX/CfJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->L:LX/CfJ;

    .line 2461516
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->L:LX/CfJ;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ae:Ljava/lang/String;

    .line 2461517
    const/4 v4, 0x0

    .line 2461518
    const/4 v10, 0x1

    move-object v5, v0

    move-object v6, v1

    move-object v7, v1

    move-object v8, v2

    move-object v9, v4

    invoke-virtual/range {v5 .. v10}, LX/CfJ;->a(Lcom/facebook/reaction/ReactionQueryParams;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/Long;Z)LX/2jY;

    move-result-object v5

    move-object v4, v5

    .line 2461519
    move-object v0, v4

    .line 2461520
    :goto_4
    const-string v2, "SubsequentLoad"

    .line 2461521
    iput-object v2, v1, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    .line 2461522
    const-wide/16 v2, 0x5

    .line 2461523
    iput-wide v2, v1, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2461524
    goto/16 :goto_2

    .line 2461525
    :cond_3
    const-string v0, "InitialLoad"

    goto :goto_3

    .line 2461526
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CfW;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ae:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/CfW;->b(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    .line 2461527
    invoke-virtual {v0, p0}, LX/2jY;->a(LX/CfG;)V

    goto :goto_4

    .line 2461528
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CfW;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->ae:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    .line 2461529
    invoke-virtual {v0, p0}, LX/2jY;->a(LX/CfG;)V

    goto :goto_4

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2461530
    :cond_7
    iget-object v6, v0, LX/HPW;->m:LX/2jY;

    if-nez v6, :cond_f

    .line 2461531
    :goto_5
    iget-object v4, v0, LX/HPW;->k:LX/8EI;

    if-eqz v4, :cond_8

    .line 2461532
    iget-object v4, v0, LX/HPW;->k:LX/8EI;

    const-string v5, "SurfaceFirstCardFromEarlyFetcher"

    const-string v6, "true"

    invoke-virtual {v4, v5, v6}, LX/8EI;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461533
    iget-object v5, v0, LX/HPW;->k:LX/8EI;

    const-string v6, "SurfaceFirstCardCachedWithEarlyFetcher"

    iget-object v4, v0, LX/HPW;->l:LX/2jY;

    if-eqz v4, :cond_10

    const-string v4, "true"

    :goto_6
    invoke-virtual {v5, v6, v4}, LX/8EI;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2461534
    :cond_8
    iget-object v4, v0, LX/HPW;->m:LX/2jY;

    invoke-virtual {v4}, LX/2jY;->z()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2461535
    iget-object v4, v0, LX/HPW;->l:LX/2jY;

    if-eqz v4, :cond_9

    .line 2461536
    iget-object v4, v0, LX/HPW;->e:LX/2iz;

    iget-object v5, v0, LX/HPW;->l:LX/2jY;

    .line 2461537
    iget-object v6, v5, LX/2jY;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2461538
    invoke-virtual {v4, v5}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    .line 2461539
    :cond_9
    iget-object v4, v0, LX/HPW;->m:LX/2jY;

    invoke-virtual {v4, p0}, LX/2jY;->a(LX/CfG;)V

    .line 2461540
    iget-object v6, v0, LX/HPW;->m:LX/2jY;

    if-nez v6, :cond_11

    .line 2461541
    :cond_a
    :goto_7
    iget-object v4, v0, LX/HPW;->m:LX/2jY;

    goto/16 :goto_1

    .line 2461542
    :cond_b
    iget-object v4, v0, LX/HPW;->l:LX/2jY;

    if-eqz v4, :cond_d

    iget-object v4, v0, LX/HPW;->l:LX/2jY;

    invoke-virtual {v4}, LX/2jY;->z()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2461543
    iget-object v4, v0, LX/HPW;->l:LX/2jY;

    invoke-virtual {v4, p0}, LX/2jY;->a(LX/CfG;)V

    .line 2461544
    iget-object v4, v0, LX/HPW;->m:LX/2jY;

    iget-object v5, v0, LX/HPW;->p:LX/CfG;

    invoke-virtual {v4, v5}, LX/2jY;->a(LX/CfG;)V

    .line 2461545
    iget-object v4, v0, LX/HPW;->l:LX/2jY;

    if-nez v4, :cond_12

    .line 2461546
    :cond_c
    :goto_8
    iget-object v4, v0, LX/HPW;->l:LX/2jY;

    goto/16 :goto_1

    .line 2461547
    :cond_d
    iget-object v4, v0, LX/HPW;->l:LX/2jY;

    if-eqz v4, :cond_e

    .line 2461548
    iget-object v4, v0, LX/HPW;->l:LX/2jY;

    invoke-virtual {v4, p0}, LX/2jY;->a(LX/CfG;)V

    .line 2461549
    iget-object v4, v0, LX/HPW;->m:LX/2jY;

    iget-object v5, v0, LX/HPW;->p:LX/CfG;

    invoke-virtual {v4, v5}, LX/2jY;->a(LX/CfG;)V

    .line 2461550
    iget-object v4, v0, LX/HPW;->l:LX/2jY;

    goto/16 :goto_1

    .line 2461551
    :cond_e
    iget-object v4, v0, LX/HPW;->m:LX/2jY;

    invoke-virtual {v4, p0}, LX/2jY;->a(LX/CfG;)V

    .line 2461552
    iget-object v4, v0, LX/HPW;->m:LX/2jY;

    goto/16 :goto_1

    .line 2461553
    :cond_f
    iget-object v6, v0, LX/HPW;->m:LX/2jY;

    .line 2461554
    iget-object v7, v6, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v6, v7

    .line 2461555
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2461556
    iget-object v6, v0, LX/HPW;->m:LX/2jY;

    .line 2461557
    iget-object v7, v6, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v6, v7

    .line 2461558
    const-wide/16 v8, 0x5

    .line 2461559
    iput-wide v8, v6, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2461560
    iget-object v6, v0, LX/HPW;->m:LX/2jY;

    .line 2461561
    iget-object v7, v6, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v6, v7

    .line 2461562
    const-string v7, "SubsequentLoad"

    .line 2461563
    iput-object v7, v6, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    .line 2461564
    goto/16 :goto_5

    .line 2461565
    :cond_10
    const-string v4, "false"

    goto/16 :goto_6

    .line 2461566
    :cond_11
    iget-object v6, v0, LX/HPW;->m:LX/2jY;

    invoke-static {v6}, LX/HPW;->a(LX/2jY;)LX/0Px;

    move-result-object v6

    .line 2461567
    if-eqz v6, :cond_a

    .line 2461568
    new-instance v7, LX/HPT;

    invoke-direct {v7, v0, v6, p0}, LX/HPT;-><init>(LX/HPW;LX/0Px;LX/CfG;)V

    invoke-static {v0, v7}, LX/HPW;->a(LX/HPW;Ljava/util/concurrent/Callable;)LX/8E6;

    move-result-object v6

    .line 2461569
    iget-object v7, v0, LX/HPW;->f:LX/8E2;

    invoke-virtual {v7, v6}, LX/8E2;->a(LX/8E6;)V

    .line 2461570
    iget-object v6, v0, LX/HPW;->h:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v7, Lcom/facebook/pages/common/surface/fragments/reaction/PagesReactionSessionEarlyFetchController$2;

    invoke-direct {v7, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesReactionSessionEarlyFetchController$2;-><init>(LX/HPW;)V

    const-wide/16 v8, 0x3

    sget-object v10, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v6, v7, v8, v9, v10}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto/16 :goto_7

    .line 2461571
    :cond_12
    iget-object v4, v0, LX/HPW;->l:LX/2jY;

    invoke-static {v4}, LX/HPW;->a(LX/2jY;)LX/0Px;

    move-result-object v4

    .line 2461572
    if-eqz v4, :cond_c

    .line 2461573
    new-instance v5, LX/HPU;

    invoke-direct {v5, v0, v4, p0}, LX/HPU;-><init>(LX/HPW;LX/0Px;LX/CfG;)V

    invoke-static {v0, v5}, LX/HPW;->a(LX/HPW;Ljava/util/concurrent/Callable;)LX/8E6;

    move-result-object v4

    .line 2461574
    iget-object v5, v0, LX/HPW;->f:LX/8E2;

    invoke-virtual {v5, v4}, LX/8E2;->a(LX/8E6;)V

    goto/16 :goto_8
.end method
