.class public Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;
.source ""


# instance fields
.field private i:J

.field public j:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2460984
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;

    invoke-static {v2}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    const/16 p0, 0x3096

    invoke-static {v2, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;->j:LX/0SG;

    iput-object v2, p1, Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;->k:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2460983
    const-string v0, "pages_public_view"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2460978
    const-class v0, Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2460979
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2460980
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;->i:J

    .line 2460981
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2460982
    return-void
.end method

.method public final v()LX/2jY;
    .locals 11

    .prologue
    const-wide/16 v8, 0x3e8

    .line 2460962
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2460963
    const-wide/32 v2, 0x1ee62800

    sub-long v2, v0, v2

    .line 2460964
    new-instance v4, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v4}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    iget-wide v6, p0, Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;->i:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 2460965
    iput-object v5, v4, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2460966
    move-object v4, v4

    .line 2460967
    const-wide/16 v6, 0x3

    .line 2460968
    iput-wide v6, v4, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2460969
    move-object v4, v4

    .line 2460970
    div-long/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2460971
    iput-object v2, v4, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    .line 2460972
    move-object v2, v4

    .line 2460973
    div-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 2460974
    iput-object v0, v2, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    .line 2460975
    move-object v1, v2

    .line 2460976
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/PageInsightsFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CfW;

    const-string v2, "ANDROID_PAGES_INSIGHTS_OVERVIEW"

    invoke-virtual {v0, v2, v1}, LX/CfW;->b(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    .line 2460977
    return-object v0
.end method
