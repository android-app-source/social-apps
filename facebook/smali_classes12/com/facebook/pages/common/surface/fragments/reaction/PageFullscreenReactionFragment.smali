.class public Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private A:LX/0Yb;

.field private B:LX/1B1;

.field private C:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field public i:LX/HMH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/HR7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/CfW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/HXj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/99v;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/BPq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:J

.field private x:Ljava/lang/String;

.field private y:LX/HXi;

.field public z:LX/HBS;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2461120
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;-><init>()V

    .line 2461121
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;LX/HMH;LX/0Sh;LX/HR7;LX/CfW;LX/HXj;Ljava/lang/String;LX/0kL;LX/0Ot;LX/0Ot;LX/0Xl;LX/1Ck;LX/0Ot;LX/0Ot;LX/BPq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;",
            "LX/HMH;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/HR7;",
            "LX/CfW;",
            "LX/HXj;",
            "Ljava/lang/String;",
            "LX/0kL;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Xl;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/99v;",
            ">;",
            "LX/BPq;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2461122
    iput-object p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->i:LX/HMH;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->j:LX/0Sh;

    iput-object p3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->k:LX/HR7;

    iput-object p4, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->l:LX/CfW;

    iput-object p5, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->m:LX/HXj;

    iput-object p6, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->n:Ljava/lang/String;

    iput-object p7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->o:LX/0kL;

    iput-object p8, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->p:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->q:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->r:LX/0Xl;

    iput-object p11, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->s:LX/1Ck;

    iput-object p12, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->t:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->u:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->v:LX/BPq;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;

    invoke-static {v14}, LX/HMH;->b(LX/0QB;)LX/HMH;

    move-result-object v1

    check-cast v1, LX/HMH;

    invoke-static {v14}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {v14}, LX/HR7;->a(LX/0QB;)LX/HR7;

    move-result-object v3

    check-cast v3, LX/HR7;

    invoke-static {v14}, LX/CfW;->b(LX/0QB;)LX/CfW;

    move-result-object v4

    check-cast v4, LX/CfW;

    const-class v5, LX/HXj;

    invoke-interface {v14, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/HXj;

    invoke-static {v14}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v14}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    const/16 v8, 0x19c6

    invoke-static {v14, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x259

    invoke-static {v14, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v14}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-static {v14}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v11

    check-cast v11, LX/1Ck;

    const/16 v12, 0x542

    invoke-static {v14, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x1d67

    invoke-static {v14, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static {v14}, LX/BPq;->a(LX/0QB;)LX/BPq;

    move-result-object v14

    check-cast v14, LX/BPq;

    invoke-static/range {v0 .. v14}, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->a(Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;LX/HMH;LX/0Sh;LX/HR7;LX/CfW;LX/HXj;Ljava/lang/String;LX/0kL;LX/0Ot;LX/0Ot;LX/0Xl;LX/1Ck;LX/0Ot;LX/0Ot;LX/BPq;)V

    return-void
.end method


# virtual methods
.method public final G()LX/1SX;
    .locals 11

    .prologue
    .line 2461123
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->y:LX/HXi;

    if-nez v0, :cond_1

    .line 2461124
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->m:LX/HXj;

    .line 2461125
    iget-object v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->z:LX/HBS;

    if-nez v3, :cond_0

    .line 2461126
    new-instance v10, LX/HOs;

    invoke-direct {v10, p0}, LX/HOs;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;)V

    .line 2461127
    iget-object v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->n:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    iget-wide v5, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->w:J

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    const-string v9, "page_only"

    invoke-static/range {v3 .. v10}, LX/HBS;->a(JJLjava/lang/String;Landroid/os/ParcelUuid;Ljava/lang/String;LX/HBQ;)LX/HBS;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->z:LX/HBS;

    .line 2461128
    :cond_0
    iget-object v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->z:LX/HBS;

    move-object v1, v3

    .line 2461129
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/HXj;->a(LX/5SB;LX/1Qj;)LX/HXi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->y:LX/HXi;

    .line 2461130
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->y:LX/HXi;

    .line 2461131
    iput-object p0, v0, LX/HXi;->m:Landroid/support/v4/app/Fragment;

    .line 2461132
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->y:LX/HXi;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2461106
    const-string v0, "pages_public_view"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2461107
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2461108
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2461109
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->w:J

    .line 2461110
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2461111
    const-string v1, "extra_page_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->x:Ljava/lang/String;

    .line 2461112
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2461113
    const-string v1, "extra_reaction_surface"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->C:Ljava/lang/String;

    .line 2461114
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2461115
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->B:LX/1B1;

    .line 2461116
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->B:LX/1B1;

    new-instance v1, LX/HOv;

    invoke-direct {v1, p0}, LX/HOv;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2461117
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->r:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    new-instance v2, LX/HOr;

    invoke-direct {v2, p0}, LX/HOr;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->A:LX/0Yb;

    .line 2461118
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->A:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2461119
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    .prologue
    .line 2461066
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2461067
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2461068
    :goto_0
    return-void

    .line 2461069
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->i:LX/HMH;

    invoke-virtual {v0, p1}, LX/HMH;->a(I)LX/HMI;

    move-result-object v0

    .line 2461070
    if-eqz v0, :cond_1

    .line 2461071
    iget-wide v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->w:J

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->k:LX/HR7;

    iget-wide v5, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->w:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/HR7;->a(Ljava/lang/String;)Lcom/facebook/ipc/pages/PageInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/pages/PageInfo;->a()LX/8A4;

    move-result-object v5

    move-object v2, v0

    move-object v6, p0

    move-object v7, p3

    move v8, p1

    invoke-interface/range {v2 .. v8}, LX/HMI;->a(JLX/8A4;Lcom/facebook/base/fragment/FbFragment;Landroid/content/Intent;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2461072
    if-nez v1, :cond_3

    .line 2461073
    :goto_1
    goto :goto_0

    .line 2461074
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2461075
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    if-nez p3, :cond_4

    .line 2461076
    :cond_2
    :goto_2
    goto :goto_0

    .line 2461077
    :cond_3
    new-instance v2, LX/HOt;

    invoke-direct {v2, p0, v0}, LX/HOt;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;LX/HMI;)V

    .line 2461078
    iget-object v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->j:LX/0Sh;

    invoke-virtual {v3, v1, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_1

    .line 2461079
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x6de
        :pswitch_0
    .end packed-switch
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x26f8ab33

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461080
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onDestroyView()V

    .line 2461081
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->A:LX/0Yb;

    if-eqz v1, :cond_0

    .line 2461082
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->A:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2461083
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x22520564

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x21b64274

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461084
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onPause()V

    .line 2461085
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->B:LX/1B1;

    if-eqz v1, :cond_0

    .line 2461086
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->B:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->v:LX/BPq;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2461087
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x33e9595e    # -3.9492232E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x75ad935f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461088
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onResume()V

    .line 2461089
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->B:LX/1B1;

    if-eqz v1, :cond_0

    .line 2461090
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->B:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->v:LX/BPq;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2461091
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x775c51cb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4cbd3c96

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2461092
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onStart()V

    .line 2461093
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->x:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2461094
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2461095
    if-eqz v0, :cond_0

    .line 2461096
    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->x:Ljava/lang/String;

    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2461097
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 2461098
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x2cbd3d8f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final v()LX/2jY;
    .locals 7

    .prologue
    .line 2461099
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->l:LX/CfW;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->C:Ljava/lang/String;

    new-instance v2, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v2}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    iget-wide v4, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;->w:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2461100
    iput-object v3, v2, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2461101
    move-object v2, v2

    .line 2461102
    const-wide/16 v4, 0x5

    .line 2461103
    iput-wide v4, v2, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2461104
    move-object v2, v2

    .line 2461105
    invoke-virtual {v0, v1, v2}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    return-object v0
.end method
