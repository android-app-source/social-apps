.class public Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;
.source ""


# instance fields
.field public i:LX/E1d;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:J

.field public k:Ljava/lang/String;

.field public l:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2461013
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;

    invoke-static {p0}, LX/E1d;->b(LX/0QB;)LX/E1d;

    move-result-object p0

    check-cast p0, LX/E1d;

    iput-object p0, p1, Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;->i:LX/E1d;

    return-void
.end method


# virtual methods
.method public final M()I
    .locals 3

    .prologue
    .line 2461028
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2461029
    const-string v1, "empty_view"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2461030
    if-lez v0, :cond_0

    .line 2461031
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->M()I

    move-result v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2461032
    const-string v0, "page_reaction_fragment"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2461019
    const-class v0, Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2461020
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2461021
    const-string v2, "page_context_item_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;->l:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    .line 2461022
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2461023
    const-string v2, "com.facebook.katana.profile.id"

    const-wide/16 v3, -0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;->j:J

    .line 2461024
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2461025
    const-string v2, "reaction_session_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;->k:Ljava/lang/String;

    .line 2461026
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2461027
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3ec847c3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461015
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2461016
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m()LX/2ja;

    move-result-object v1

    invoke-virtual {v1}, LX/2ja;->e()V

    .line 2461017
    :cond_0
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->onDestroy()V

    .line 2461018
    const/16 v1, 0x2b

    const v2, 0x5176f8ef

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final v()LX/2jY;
    .locals 5

    .prologue
    .line 2461014
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;->i:LX/E1d;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;->l:Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    iget-wide v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;->j:J

    iget-object v4, p0, Lcom/facebook/pages/common/surface/fragments/reaction/BasePagesReactionFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/E1d;->a(Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;JLjava/lang/String;)LX/2jY;

    move-result-object v0

    return-object v0
.end method
