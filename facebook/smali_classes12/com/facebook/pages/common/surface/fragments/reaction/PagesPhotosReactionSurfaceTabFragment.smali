.class public Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/HOo;
.implements LX/GZf;
.implements LX/CfG;
.implements LX/0o8;


# instance fields
.field public A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/intent/PhotosViewIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9at;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private F:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field private G:I

.field private H:I

.field public I:Z

.field public J:LX/DxJ;

.field private K:LX/E93;

.field public L:LX/HGj;

.field private M:Landroid/view/View;

.field private N:Lcom/facebook/pages/common/ui/PagesEmptyView;

.field private final O:LX/HPQ;

.field private final P:LX/HPO;

.field private final Q:LX/HPS;

.field private final R:LX/HPR;

.field private final S:LX/9bb;

.field private final T:LX/9bd;

.field private final U:LX/9bh;

.field private final V:LX/1L3;

.field private final W:LX/HPP;

.field private X:Z

.field public Y:Z

.field public Z:Z

.field public a:LX/Dxd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/pages/common/photos/PagePhotosType;

.field public ac:Ljava/lang/String;

.field public ad:Landroid/os/ParcelUuid;

.field private ae:Z

.field public i:LX/DxK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Dvg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/HH9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Dcd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1L1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0b3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Dv9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1vi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/3iH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/9bY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2U1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2462033
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;-><init>()V

    .line 2462034
    iput v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->G:I

    .line 2462035
    new-instance v0, LX/HPQ;

    invoke-direct {v0, p0}, LX/HPQ;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->O:LX/HPQ;

    .line 2462036
    new-instance v0, LX/HPO;

    invoke-direct {v0, p0}, LX/HPO;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->P:LX/HPO;

    .line 2462037
    new-instance v0, LX/HPS;

    invoke-direct {v0, p0}, LX/HPS;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->Q:LX/HPS;

    .line 2462038
    new-instance v0, LX/HPR;

    invoke-direct {v0, p0}, LX/HPR;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->R:LX/HPR;

    .line 2462039
    new-instance v0, LX/HPE;

    invoke-direct {v0, p0}, LX/HPE;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->S:LX/9bb;

    .line 2462040
    new-instance v0, LX/HPF;

    invoke-direct {v0, p0}, LX/HPF;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->T:LX/9bd;

    .line 2462041
    new-instance v0, LX/HPG;

    invoke-direct {v0, p0}, LX/HPG;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->U:LX/9bh;

    .line 2462042
    new-instance v0, LX/HPH;

    invoke-direct {v0, p0}, LX/HPH;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->V:LX/1L3;

    .line 2462043
    new-instance v0, LX/HPP;

    invoke-direct {v0, p0}, LX/HPP;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->W:LX/HPP;

    .line 2462044
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->X:Z

    .line 2462045
    iput-boolean v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->Y:Z

    .line 2462046
    iput-boolean v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->Z:Z

    .line 2462047
    sget-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->UNSET_PAGE_PHOTOS_TYPE:Lcom/facebook/pages/common/photos/PagePhotosType;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ab:Lcom/facebook/pages/common/photos/PagePhotosType;

    .line 2462048
    return-void
.end method

.method public static P(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V
    .locals 1

    .prologue
    .line 2462049
    sget-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->UNSET_PAGE_PHOTOS_TYPE:Lcom/facebook/pages/common/photos/PagePhotosType;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ab:Lcom/facebook/pages/common/photos/PagePhotosType;

    .line 2462050
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->Z:Z

    .line 2462051
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->aa:Ljava/lang/String;

    .line 2462052
    return-void
.end method

.method public static Q(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2462053
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2462054
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 2462055
    if-eqz v0, :cond_1

    .line 2462056
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2462057
    :cond_0
    :goto_0
    return-object v0

    .line 2462058
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->r:LX/0SI;

    invoke-interface {v0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 2462059
    if-eqz v0, :cond_2

    .line 2462060
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v1

    .line 2462061
    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 2462062
    :goto_1
    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 2462063
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2462064
    if-eqz p0, :cond_0

    .line 2462065
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2462066
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2462067
    :cond_0
    return-object p0
.end method

.method public static a(JLandroid/os/ParcelUuid;Ljava/lang/String;Z)Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;
    .locals 4

    .prologue
    .line 2462068
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2462069
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2462070
    new-instance v1, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-direct {v1}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;-><init>()V

    .line 2462071
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2462072
    const-string v2, "page_fragment_uuid"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2462073
    const-string v2, "source_name"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2462074
    const-string v2, "extra_is_inside_page_surface_tab"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2462075
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2462076
    return-object v1
.end method

.method private static a(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;LX/Dxd;LX/DxK;LX/Dvg;LX/HH9;LX/Dcd;LX/1L1;LX/0b3;LX/Dv9;LX/1vi;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0SI;LX/3iH;LX/9bY;LX/0Uh;LX/1Ck;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;",
            "LX/Dxd;",
            "LX/DxK;",
            "LX/Dvg;",
            "LX/HH9;",
            "LX/Dcd;",
            "LX/1L1;",
            "LX/0b3;",
            "LX/Dv9;",
            "LX/1vi;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/0SI;",
            "LX/3iH;",
            "LX/9bY;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/CfW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2U1;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/intent/PhotosViewIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9at;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2462077
    iput-object p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a:LX/Dxd;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->i:LX/DxK;

    iput-object p3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->j:LX/Dvg;

    iput-object p4, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->k:LX/HH9;

    iput-object p5, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->l:LX/Dcd;

    iput-object p6, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->m:LX/1L1;

    iput-object p7, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->n:LX/0b3;

    iput-object p8, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->o:LX/Dv9;

    iput-object p9, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->p:LX/1vi;

    iput-object p10, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p11, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->r:LX/0SI;

    iput-object p12, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->s:LX/3iH;

    iput-object p13, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->t:LX/9bY;

    iput-object p14, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->u:LX/0Uh;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->v:LX/1Ck;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->w:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->x:LX/0Ot;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->y:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->z:LX/0Ot;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->A:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->B:LX/0Ot;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->C:LX/0Ot;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->D:LX/0Ot;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->E:Lcom/facebook/quicklog/QuickPerformanceLogger;

    return-void
.end method

.method public static a(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Ljava/lang/String;LX/0Ve;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2462078
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->k:LX/HH9;

    const/4 v2, 0x0

    .line 2462079
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2462080
    new-instance v1, LX/HHG;

    invoke-direct {v1}, LX/HHG;-><init>()V

    move-object v1, v1

    .line 2462081
    const-string v3, "page_id"

    invoke-virtual {v1, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2462082
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 2462083
    iget-object v3, v0, LX/HH9;->b:LX/0tX;

    invoke-virtual {v3, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2462084
    new-instance v3, LX/HH8;

    invoke-direct {v3}, LX/HH8;-><init>()V

    iget-object v2, v0, LX/HH9;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v3, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2462085
    move-object v0, v1

    .line 2462086
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->v:LX/1Ck;

    const-string v2, "fetchPageMetaData_%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/2sv;

    invoke-direct {v3, p0, v0}, LX/2sv;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v1, v2, v3, p2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2462087
    return-void

    :cond_0
    move v1, v2

    .line 2462088
    goto :goto_0
.end method

.method private a(Lcom/facebook/pages/common/ui/PagesEmptyView;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/facebook/pages/common/ui/PagesEmptyView;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2462024
    if-nez p1, :cond_0

    .line 2462025
    :goto_0
    return-void

    .line 2462026
    :cond_0
    if-nez p2, :cond_1

    const-string v0, ""

    .line 2462027
    :goto_1
    invoke-virtual {p1, v0}, Lcom/facebook/pages/common/ui/PagesEmptyView;->setMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 2462028
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08369d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 27

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v26

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;

    invoke-static/range {v26 .. v26}, LX/Dxd;->a(LX/0QB;)LX/Dxd;

    move-result-object v3

    check-cast v3, LX/Dxd;

    const-class v4, LX/DxK;

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/DxK;

    invoke-static/range {v26 .. v26}, LX/Dvg;->a(LX/0QB;)LX/Dvg;

    move-result-object v5

    check-cast v5, LX/Dvg;

    invoke-static/range {v26 .. v26}, LX/HH9;->a(LX/0QB;)LX/HH9;

    move-result-object v6

    check-cast v6, LX/HH9;

    invoke-static/range {v26 .. v26}, LX/Dcd;->a(LX/0QB;)LX/Dcd;

    move-result-object v7

    check-cast v7, LX/Dcd;

    invoke-static/range {v26 .. v26}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v8

    check-cast v8, LX/1L1;

    invoke-static/range {v26 .. v26}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v9

    check-cast v9, LX/0b3;

    invoke-static/range {v26 .. v26}, LX/Dv9;->a(LX/0QB;)LX/Dv9;

    move-result-object v10

    check-cast v10, LX/Dv9;

    invoke-static/range {v26 .. v26}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v11

    check-cast v11, LX/1vi;

    invoke-static/range {v26 .. v26}, LX/0eQ;->a(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v12

    check-cast v12, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static/range {v26 .. v26}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v13

    check-cast v13, LX/0SI;

    invoke-static/range {v26 .. v26}, LX/3iH;->a(LX/0QB;)LX/3iH;

    move-result-object v14

    check-cast v14, LX/3iH;

    invoke-static/range {v26 .. v26}, LX/9bY;->a(LX/0QB;)LX/9bY;

    move-result-object v15

    check-cast v15, LX/9bY;

    invoke-static/range {v26 .. v26}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v16

    check-cast v16, LX/0Uh;

    invoke-static/range {v26 .. v26}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v17

    check-cast v17, LX/1Ck;

    const/16 v18, 0x3096

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x1061

    move-object/from16 v0, v26

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x259

    move-object/from16 v0, v26

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0xec6

    move-object/from16 v0, v26

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x2e65

    move-object/from16 v0, v26

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x455

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x2dfd

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x2b68

    move-object/from16 v0, v26

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    invoke-static/range {v26 .. v26}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v26

    check-cast v26, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v2 .. v26}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;LX/Dxd;LX/DxK;LX/Dvg;LX/HH9;LX/Dcd;LX/1L1;LX/0b3;LX/Dv9;LX/1vi;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0SI;LX/3iH;LX/9bY;LX/0Uh;LX/1Ck;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;)V
    .locals 4
    .param p0    # Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2462089
    if-nez p1, :cond_0

    sget-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->UNSET_PAGE_PHOTOS_TYPE:Lcom/facebook/pages/common/photos/PagePhotosType;

    :goto_0
    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ab:Lcom/facebook/pages/common/photos/PagePhotosType;

    .line 2462090
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->Z:Z

    .line 2462091
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2462092
    :goto_2
    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    :goto_3
    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->aa:Ljava/lang/String;

    .line 2462093
    return-void

    .line 2462094
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->j()Z

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->a()Z

    move-result v3

    invoke-static {v0, v3}, Lcom/facebook/pages/common/photos/PagePhotosType;->fromPageMetaData(ZZ)Lcom/facebook/pages/common/photos/PagePhotosType;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2462095
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2462096
    goto :goto_2

    .line 2462097
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static a$redex0(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Z)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 2462098
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    if-eqz v0, :cond_1

    .line 2462099
    if-nez p2, :cond_0

    sget-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->UNSET_PAGE_PHOTOS_TYPE:Lcom/facebook/pages/common/photos/PagePhotosType;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ab:Lcom/facebook/pages/common/photos/PagePhotosType;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/photos/PagePhotosType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2462100
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->k:LX/HH9;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ab:Lcom/facebook/pages/common/photos/PagePhotosType;

    .line 2462101
    sget-object v2, LX/HH7;->a:[I

    invoke-virtual {v1}, Lcom/facebook/pages/common/photos/PagePhotosType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2462102
    const/4 v2, 0x0

    iput-object v2, v0, LX/HH9;->f:LX/Dcc;

    .line 2462103
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->N:Lcom/facebook/pages/common/ui/PagesEmptyView;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->aa:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(Lcom/facebook/pages/common/ui/PagesEmptyView;Ljava/lang/String;)V

    .line 2462104
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    const-string v3, "PagesPhotosReactionSurfaceTabFragment"

    const/4 v6, 0x0

    move-object v2, p1

    move v5, v4

    invoke-virtual/range {v0 .. v6}, LX/Dvb;->a(Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Ljava/lang/String;ZZZ)V

    .line 2462105
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    invoke-virtual {v0}, LX/Dvb;->c()V

    .line 2462106
    :cond_1
    return-void

    .line 2462107
    :pswitch_0
    iget-object v2, v0, LX/HH9;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Dcc;

    iput-object v2, v0, LX/HH9;->f:LX/Dcc;

    goto :goto_0

    .line 2462108
    :pswitch_1
    iget-object v2, v0, LX/HH9;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Dcc;

    iput-object v2, v0, LX/HH9;->f:LX/Dcc;

    goto :goto_0

    .line 2462109
    :pswitch_2
    iget-object v2, v0, LX/HH9;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Dcc;

    iput-object v2, v0, LX/HH9;->f:LX/Dcc;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const-wide/16 v2, -0x1

    .line 2462110
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2462111
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 2462112
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid page id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2462113
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    .line 2462114
    const-string v0, "page_fragment_uuid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    .line 2462115
    if-nez v0, :cond_1

    .line 2462116
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid fragment uuid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2462117
    :cond_1
    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ad:Landroid/os/ParcelUuid;

    .line 2462118
    const-string v0, "extra_is_inside_page_surface_tab"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ae:Z

    .line 2462119
    return-void
.end method

.method public static p(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)Z
    .locals 3

    .prologue
    .line 2462120
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2462121
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2462122
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->Z:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V
    .locals 2

    .prologue
    .line 2462123
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    if-eqz v0, :cond_0

    .line 2462124
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->N:Lcom/facebook/pages/common/ui/PagesEmptyView;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->aa:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(Lcom/facebook/pages/common/ui/PagesEmptyView;Ljava/lang/String;)V

    .line 2462125
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    invoke-virtual {v0}, LX/Dvb;->c()V

    .line 2462126
    :cond_0
    return-void
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2462127
    iput p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->G:I

    .line 2462128
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->M:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2462129
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->M:Landroid/view/View;

    new-instance v1, LX/1a3;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->G:I

    invoke-direct {v1, v2, v3}, LX/1a3;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2462130
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)LX/1P0;
    .locals 1

    .prologue
    .line 2462131
    new-instance v0, LX/HMB;

    invoke-direct {v0, p1}, LX/HMB;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;)LX/E8m;
    .locals 1

    .prologue
    .line 2461904
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->K:LX/E93;

    if-nez v0, :cond_0

    .line 2461905
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/content/Context;)LX/E8m;

    move-result-object v0

    .line 2461906
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->K:LX/E93;

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2462132
    const-string v0, "page_reaction_fragment"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2462029
    iput p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->H:I

    .line 2462030
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2462031
    iget v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->H:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2462032
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2461889
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2461890
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2461891
    if-eqz v0, :cond_0

    .line 2461892
    invoke-direct {p0, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->b(Landroid/os/Bundle;)V

    .line 2461893
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->E:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x130086

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2461894
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2461895
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->t:LX/9bY;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->S:LX/9bb;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2461896
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->t:LX/9bY;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->T:LX/9bd;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2461897
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->t:LX/9bY;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->U:LX/9bh;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2461898
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->m:LX/1L1;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->V:LX/1L3;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2461899
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->n:LX/0b3;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->W:LX/HPP;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2461900
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    .line 2461901
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->F:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2461902
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->F:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->D()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2461903
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2461907
    iput-object p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->F:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2461908
    return-void
.end method

.method public final a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/E8m;Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2461909
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2461910
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2461911
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0b41

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2461912
    new-instance v2, LX/HPI;

    invoke-direct {v2, p0, v1, v0}, LX/HPI;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;ILandroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2461913
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    if-nez v0, :cond_0

    .line 2461914
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->u:LX/0Uh;

    const/16 v1, 0x41a

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 2461915
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->l:LX/Dcd;

    .line 2461916
    :goto_0
    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->i:LX/DxK;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->j:LX/Dvg;

    invoke-virtual {v2, v0, v3, v4, v5}, LX/DxK;->a(LX/Dcc;Ljava/lang/Boolean;Ljava/lang/Boolean;LX/Dvf;)LX/DxJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    .line 2461917
    if-eqz v1, :cond_2

    new-instance v0, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ALL"

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/localcontent/photos/PhotosByCategoryPandoraInstanceId;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;)V

    .line 2461918
    :goto_1
    new-instance v2, LX/HPN;

    invoke-direct {v2, p0, v0, v1}, LX/HPN;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Z)V

    .line 2461919
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    invoke-static {p0, v0, v2}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Ljava/lang/String;LX/0Ve;)V

    .line 2461920
    :cond_0
    check-cast p2, LX/E93;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->K:LX/E93;

    .line 2461921
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->K:LX/E93;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2461922
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2461923
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->M:Landroid/view/View;

    .line 2461924
    iget v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->G:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->E_(I)V

    .line 2461925
    new-instance v0, Lcom/facebook/pages/common/ui/PagesEmptyView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/pages/common/ui/PagesEmptyView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->N:Lcom/facebook/pages/common/ui/PagesEmptyView;

    .line 2461926
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->N:Lcom/facebook/pages/common/ui/PagesEmptyView;

    const v1, 0x7f02062f

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/ui/PagesEmptyView;->setImageResource(I)V

    .line 2461927
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->N:Lcom/facebook/pages/common/ui/PagesEmptyView;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->aa:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(Lcom/facebook/pages/common/ui/PagesEmptyView;Ljava/lang/String;)V

    .line 2461928
    new-instance v0, LX/HGj;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->K:LX/E93;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->N:Lcom/facebook/pages/common/ui/PagesEmptyView;

    invoke-static {v1}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->M:Landroid/view/View;

    invoke-static {v1}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/HGj;-><init>(Landroid/support/v7/widget/RecyclerView;LX/E93;LX/Dvb;Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->L:LX/HGj;

    .line 2461929
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->L:LX/HGj;

    iget-boolean v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->Y:Z

    .line 2461930
    iput-boolean v1, v0, LX/HGj;->d:Z

    .line 2461931
    new-instance v0, LX/HPJ;

    invoke-direct {v0, p0}, LX/HPJ;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2461932
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->L:LX/HGj;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2461933
    return-void

    .line 2461934
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->k:LX/HH9;

    goto/16 :goto_0

    .line 2461935
    :cond_2
    new-instance v0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2461936
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ae:Z

    if-nez v0, :cond_0

    .line 2461937
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->c(Ljava/lang/String;)V

    .line 2461938
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2461939
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2iz;

    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2iz;->g(Ljava/lang/String;)LX/2jY;

    .line 2461940
    invoke-static {p0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->P(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    .line 2461941
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->X:Z

    .line 2461942
    iput-boolean v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->Y:Z

    .line 2461943
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->N:Lcom/facebook/pages/common/ui/PagesEmptyView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(Lcom/facebook/pages/common/ui/PagesEmptyView;Ljava/lang/String;)V

    .line 2461944
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->L:LX/HGj;

    .line 2461945
    iput-boolean v2, v0, LX/HGj;->d:Z

    .line 2461946
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->y()V

    .line 2461947
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->z()V

    .line 2461948
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2461949
    iget-object v1, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->q:LX/2jY;

    move-object v1, v1

    .line 2461950
    invoke-virtual {v0, v1}, LX/E8m;->b(LX/2jY;)V

    .line 2461951
    new-instance v0, LX/HPD;

    invoke-direct {v0, p0}, LX/HPD;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    .line 2461952
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    invoke-static {p0, v1, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;Ljava/lang/String;LX/0Ve;)V

    .line 2461953
    return-void
.end method

.method public final k()Landroid/os/ParcelUuid;
    .locals 1

    .prologue
    .line 2461954
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ad:Landroid/os/ParcelUuid;

    return-object v0
.end method

.method public final kJ_()Z
    .locals 1

    .prologue
    .line 2461955
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->X:Z

    return v0
.end method

.method public final kK_()V
    .locals 1

    .prologue
    .line 2461956
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->kK_()V

    .line 2461957
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->X:Z

    .line 2461958
    return-void
.end method

.method public final kL_()V
    .locals 1

    .prologue
    .line 2461959
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->kL_()V

    .line 2461960
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->X:Z

    .line 2461961
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->L:LX/HGj;

    if-eqz v0, :cond_0

    .line 2461962
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->L:LX/HGj;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2461963
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->I:Z

    .line 2461964
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2461965
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->o:LX/E8m;

    move-object v0, v0

    .line 2461966
    invoke-virtual {v0, p1}, LX/E8m;->a(Landroid/content/res/Configuration;)V

    .line 2461967
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x62c4f418

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461968
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->t:LX/9bY;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->S:LX/9bb;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2461969
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->t:LX/9bY;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->T:LX/9bd;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2461970
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->t:LX/9bY;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->U:LX/9bh;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2461971
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->m:LX/1L1;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->V:LX/1L3;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2461972
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->n:LX/0b3;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->W:LX/HPP;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2461973
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onDestroy()V

    .line 2461974
    const/16 v1, 0x2b

    const v2, -0x1501e053

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x27c270af

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461975
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->O:LX/HPQ;

    invoke-virtual {v1, v2}, LX/DxJ;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2461976
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    invoke-virtual {v1, v3}, LX/DxJ;->a(LX/Dce;)V

    .line 2461977
    iput-object v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->N:Lcom/facebook/pages/common/ui/PagesEmptyView;

    .line 2461978
    iput-object v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->M:Landroid/view/View;

    .line 2461979
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onDestroyView()V

    .line 2461980
    const/16 v1, 0x2b

    const v2, -0x621e83e4

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7fef0408

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461981
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->p:LX/1vi;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->R:LX/HPR;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2461982
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->p:LX/1vi;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->Q:LX/HPS;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2461983
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->o:LX/Dv9;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->P:LX/HPO;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2461984
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onPause()V

    .line 2461985
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->E:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x130086

    const/16 v3, 0x16

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2461986
    const/16 v1, 0x2b

    const v2, 0x7b5e5365

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x260688f8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2461987
    invoke-super {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->onResume()V

    .line 2461988
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->p:LX/1vi;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->R:LX/HPR;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2461989
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->p:LX/1vi;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->Q:LX/HPS;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2461990
    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->o:LX/Dv9;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->P:LX/HPO;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2461991
    const/16 v1, 0x2b

    const v2, 0x7ac2e0be

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2461992
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2461993
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2461994
    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVerticalScrollBarEnabled(Z)V

    .line 2461995
    iget-object v0, p0, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->m:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2461996
    new-instance v1, LX/HPK;

    invoke-direct {v1, p0}, LX/HPK;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->a(LX/0fu;)V

    .line 2461997
    iget v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->G:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->E_(I)V

    .line 2461998
    iget v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->H:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->a(I)V

    .line 2461999
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->O:LX/HPQ;

    invoke-virtual {v0, v1}, LX/DxJ;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2462000
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->J:LX/DxJ;

    new-instance v1, LX/HPL;

    invoke-direct {v1, p0}, LX/HPL;-><init>(Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;)V

    invoke-virtual {v0, v1}, LX/DxJ;->a(LX/Dce;)V

    .line 2462001
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 3

    .prologue
    .line 2462002
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->setUserVisibleHint(Z)V

    .line 2462003
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->E:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v0, :cond_0

    .line 2462004
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->E:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x130086

    const/16 v2, 0x16

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2462005
    :cond_0
    return-void
.end method

.method public final v()LX/2jY;
    .locals 5

    .prologue
    .line 2462006
    new-instance v0, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v0}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->ac:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2462007
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2462008
    move-object v0, v0

    .line 2462009
    const-wide/16 v2, 0x5

    .line 2462010
    iput-wide v2, v0, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2462011
    move-object v0, v0

    .line 2462012
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2462013
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2462014
    move-object v0, v0

    .line 2462015
    const-string v1, "ANDROID_PAGE_PHOTOS_TAB"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2462016
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    .line 2462017
    move-object v0, v0

    .line 2462018
    invoke-virtual {p0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->a()Ljava/lang/String;

    move-result-object v1

    .line 2462019
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    .line 2462020
    move-object v1, v0

    .line 2462021
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesPhotosReactionSurfaceTabFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CfW;

    const-string v2, "ANDROID_PAGE_PHOTOS_TAB"

    invoke-virtual {v0, v2, v1}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    .line 2462022
    invoke-virtual {v0, p0}, LX/2jY;->a(LX/CfG;)V

    .line 2462023
    return-object v0
.end method
