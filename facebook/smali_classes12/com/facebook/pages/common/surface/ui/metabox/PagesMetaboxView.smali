.class public Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/HQh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CXj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HDT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8Do;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Landroid/widget/TextView;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Lcom/facebook/fig/starrating/FigStarRatingBar;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:LX/HQn;

.field public m:Z

.field public n:Landroid/os/ParcelUuid;

.field private o:Landroid/view/View$OnClickListener;

.field private p:Landroid/view/View$OnClickListener;

.field private q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2463934
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2463935
    iput-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->g:Landroid/view/View;

    .line 2463936
    iput-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->j:Landroid/widget/TextView;

    .line 2463937
    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->m:Z

    .line 2463938
    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->q:Z

    .line 2463939
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->a()V

    .line 2463940
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2463941
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2463942
    iput-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->g:Landroid/view/View;

    .line 2463943
    iput-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->j:Landroid/widget/TextView;

    .line 2463944
    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->m:Z

    .line 2463945
    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->q:Z

    .line 2463946
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->a()V

    .line 2463947
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2463971
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2463972
    iput-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->g:Landroid/view/View;

    .line 2463973
    iput-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->j:Landroid/widget/TextView;

    .line 2463974
    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->m:Z

    .line 2463975
    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->q:Z

    .line 2463976
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->a()V

    .line 2463977
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2463948
    const-class v0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2463949
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->d:LX/8Do;

    invoke-virtual {v0}, LX/8Do;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->q:Z

    .line 2463950
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->q:Z

    if-eqz v0, :cond_2

    .line 2463951
    const v0, 0x7f030eda

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2463952
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->setOrientation(I)V

    .line 2463953
    const v0, 0x7f0d243c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->e:Landroid/widget/TextView;

    .line 2463954
    const v0, 0x7f0d2438

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->f:Landroid/view/View;

    .line 2463955
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->q:Z

    if-eqz v0, :cond_0

    .line 2463956
    const v0, 0x7f0d243d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->g:Landroid/view/View;

    .line 2463957
    :cond_0
    const v0, 0x7f0d2439

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->h:Landroid/widget/TextView;

    .line 2463958
    const v0, 0x7f0d243a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/starrating/FigStarRatingBar;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->i:Lcom/facebook/fig/starrating/FigStarRatingBar;

    .line 2463959
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->q:Z

    if-eqz v0, :cond_1

    .line 2463960
    const v0, 0x7f0d243e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->j:Landroid/widget/TextView;

    .line 2463961
    :cond_1
    const v0, 0x7f0d243b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->k:Landroid/widget/TextView;

    .line 2463962
    new-instance v0, LX/HQi;

    invoke-direct {v0, p0}, LX/HQi;-><init>(Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->o:Landroid/view/View$OnClickListener;

    .line 2463963
    new-instance v0, LX/HQj;

    invoke-direct {v0, p0}, LX/HQj;-><init>(Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->p:Landroid/view/View$OnClickListener;

    .line 2463964
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0213ed    # 1.729031E38f

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2463965
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0069

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2463966
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b006b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2463967
    invoke-virtual {p0, v0, v1, v0, v1}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->setPadding(IIII)V

    .line 2463968
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->b()V

    .line 2463969
    return-void

    .line 2463970
    :cond_2
    const v0, 0x7f030ed9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    goto/16 :goto_0
.end method

.method private static a(Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;LX/HQh;LX/CXj;LX/HDT;LX/8Do;)V
    .locals 0

    .prologue
    .line 2463933
    iput-object p1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->a:LX/HQh;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->b:LX/CXj;

    iput-object p3, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->c:LX/HDT;

    iput-object p4, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->d:LX/8Do;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;

    new-instance p1, LX/HQh;

    invoke-static {v3}, LX/BNM;->a(LX/0QB;)LX/BNM;

    move-result-object v0

    check-cast v0, LX/BNM;

    invoke-static {v3}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v1

    check-cast v1, LX/0W9;

    invoke-static {v3}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v2

    check-cast v2, LX/8Do;

    const-class v4, Landroid/content/Context;

    invoke-interface {v3, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {p1, v0, v1, v2, v4}, LX/HQh;-><init>(LX/BNM;LX/0W9;LX/8Do;Landroid/content/Context;)V

    move-object v0, p1

    check-cast v0, LX/HQh;

    invoke-static {v3}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v1

    check-cast v1, LX/CXj;

    invoke-static {v3}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v2

    check-cast v2, LX/HDT;

    invoke-static {v3}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v3

    check-cast v3, LX/8Do;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->a(Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;LX/HQh;LX/CXj;LX/HDT;LX/8Do;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2463931
    new-instance v0, LX/HQk;

    invoke-direct {v0, p0}, LX/HQk;-><init>(Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 2463932
    return-void
.end method


# virtual methods
.method public final a(LX/CZd;)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 2463899
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->a:LX/HQh;

    invoke-virtual {v0, p1}, LX/HQh;->a(LX/CZd;)LX/HQn;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    .line 2463900
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->m:Z

    .line 2463901
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    iget-object v1, v1, LX/HQn;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2463902
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->q:Z

    if-eqz v0, :cond_0

    .line 2463903
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2463904
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    iget-object v0, v0, LX/HQn;->b:LX/HQm;

    sget-object v1, LX/HQm;->HIDDEN:LX/HQm;

    if-ne v0, v1, :cond_1

    .line 2463905
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2463906
    :goto_0
    return-void

    .line 2463907
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2463908
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->j:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 2463909
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    iget-object v0, v0, LX/HQn;->b:LX/HQm;

    sget-object v1, LX/HQm;->RATING_AND_OPEN_HOURS:LX/HQm;

    if-ne v0, v1, :cond_6

    .line 2463910
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2463911
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    iget-object v0, v0, LX/HQn;->b:LX/HQm;

    sget-object v1, LX/HQm;->RATING_AND_OPEN_HOURS:LX/HQm;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    iget-object v0, v0, LX/HQn;->b:LX/HQm;

    sget-object v1, LX/HQm;->RATING_ONLY:LX/HQm;

    if-ne v0, v1, :cond_7

    .line 2463912
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->i:Lcom/facebook/fig/starrating/FigStarRatingBar;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    iget v1, v1, LX/HQn;->c:F

    invoke-virtual {v0, v1}, Lcom/facebook/fig/starrating/FigStarRatingBar;->setRating(F)V

    .line 2463913
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    iget-object v1, v1, LX/HQn;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2463914
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2463915
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->i:Lcom/facebook/fig/starrating/FigStarRatingBar;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/starrating/FigStarRatingBar;->setVisibility(I)V

    .line 2463916
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->g:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->d:LX/8Do;

    .line 2463917
    iget-object v1, v0, LX/8Do;->a:LX/0ad;

    sget-object v4, LX/0c0;->Live:LX/0c0;

    sget-short v5, LX/8Dn;->i:S

    const/4 p1, 0x0

    invoke-interface {v1, v4, v5, p1}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v1

    move v0, v1

    .line 2463918
    if-eqz v0, :cond_4

    .line 2463919
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2463920
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    iget-object v0, v0, LX/HQn;->b:LX/HQm;

    sget-object v1, LX/HQm;->RATING_AND_OPEN_HOURS:LX/HQm;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    iget-object v0, v0, LX/HQn;->b:LX/HQm;

    sget-object v1, LX/HQm;->OPEN_HOURS_ONLY:LX/HQm;

    if-ne v0, v1, :cond_8

    .line 2463921
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    iget-object v1, v1, LX/HQn;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2463922
    sget-object v0, LX/HQl;->a:[I

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->l:LX/HQn;

    iget-object v1, v1, LX/HQn;->f:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2463923
    :goto_3
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2463924
    :cond_6
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 2463925
    :cond_7
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2463926
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->i:Lcom/facebook/fig/starrating/FigStarRatingBar;

    invoke-virtual {v0, v2}, Lcom/facebook/fig/starrating/FigStarRatingBar;->setVisibility(I)V

    goto :goto_2

    .line 2463927
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->k:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a053d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    .line 2463928
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->k:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a053e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    .line 2463929
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->k:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a053f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3

    .line 2463930
    :cond_8
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setLoggingUuid(Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2463897
    iput-object p1, p0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->n:Landroid/os/ParcelUuid;

    .line 2463898
    return-void
.end method
