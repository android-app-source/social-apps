.class public Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;
.super LX/Ban;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/HQN;


# static fields
.field private static final w:Lcom/facebook/common/callercontext/CallerContext;

.field private static final x:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Landroid/os/ParcelUuid;

.field private B:LX/CZd;

.field public C:LX/D2H;

.field private D:Landroid/view/View$OnClickListener;

.field private E:LX/D2J;

.field public F:Z

.field private final G:Landroid/view/View$OnClickListener;

.field private final H:Landroid/view/View$OnClickListener;

.field public m:LX/CXj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/9hF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/23R;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/8Do;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/Bas;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/HQT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/D2I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/HQL;

.field private y:Landroid/view/View;

.field public z:LX/HQa;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2463364
    const-class v0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    const-string v1, "pages_identity"

    const-string v2, "profile_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->w:Lcom/facebook/common/callercontext/CallerContext;

    .line 2463365
    const-class v0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    const-string v1, "pages_identity"

    const-string v2, "cover_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->x:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2463299
    invoke-direct {p0, p1}, LX/Ban;-><init>(Landroid/content/Context;)V

    .line 2463300
    new-instance v0, LX/HQC;

    invoke-direct {v0, p0}, LX/HQC;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->G:Landroid/view/View$OnClickListener;

    .line 2463301
    new-instance v0, LX/HQD;

    invoke-direct {v0, p0}, LX/HQD;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->H:Landroid/view/View$OnClickListener;

    .line 2463302
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->g()V

    .line 2463303
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2463304
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Ban;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2463305
    new-instance v0, LX/HQC;

    invoke-direct {v0, p0}, LX/HQC;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->G:Landroid/view/View$OnClickListener;

    .line 2463306
    new-instance v0, LX/HQD;

    invoke-direct {v0, p0}, LX/HQD;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->H:Landroid/view/View$OnClickListener;

    .line 2463307
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->g()V

    .line 2463308
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2463309
    invoke-direct {p0, p1, p2, p3}, LX/Ban;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2463310
    new-instance v0, LX/HQC;

    invoke-direct {v0, p0}, LX/HQC;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->G:Landroid/view/View$OnClickListener;

    .line 2463311
    new-instance v0, LX/HQD;

    invoke-direct {v0, p0}, LX/HQD;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->H:Landroid/view/View$OnClickListener;

    .line 2463312
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->g()V

    .line 2463313
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;)I
    .locals 2

    .prologue
    .line 2463314
    sget-object v0, LX/HQK;->b:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2463315
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "No asset for verification status "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2463316
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2463317
    :pswitch_0
    const v0, 0x7f0213c3

    .line 2463318
    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f0213c2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/15i;I)Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2463319
    if-eqz p1, :cond_0

    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2463320
    :cond_0
    const/4 v0, 0x0

    .line 2463321
    :goto_0
    return-object v0

    .line 2463322
    :cond_1
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    .line 2463323
    new-instance v1, LX/4Xy;

    invoke-direct {v1}, LX/4Xy;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 2463324
    iput-object v2, v1, LX/4Xy;->I:Ljava/lang/String;

    .line 2463325
    move-object v1, v1

    .line 2463326
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/9JZ;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2463327
    iput-object v2, v1, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2463328
    move-object v1, v1

    .line 2463329
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/9JZ;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2463330
    iput-object v2, v1, LX/4Xy;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2463331
    move-object v1, v1

    .line 2463332
    new-instance v2, LX/4Vp;

    invoke-direct {v2}, LX/4Vp;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->k()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel$AlbumModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel$AlbumModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2463333
    iput-object v0, v2, LX/4Vp;->m:Ljava/lang/String;

    .line 2463334
    move-object v0, v2

    .line 2463335
    invoke-virtual {v0}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    .line 2463336
    iput-object v0, v1, LX/4Xy;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2463337
    move-object v0, v1

    .line 2463338
    invoke-virtual {v0}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;)Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 3

    .prologue
    .line 2463339
    if-nez p0, :cond_0

    .line 2463340
    const/4 v0, 0x0

    .line 2463341
    :goto_0
    return-object v0

    .line 2463342
    :cond_0
    new-instance v0, LX/4Xy;

    invoke-direct {v0}, LX/4Xy;-><init>()V

    .line 2463343
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2463344
    iput-object v1, v0, LX/4Xy;->I:Ljava/lang/String;

    .line 2463345
    move-object v1, v0

    .line 2463346
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/9JZ;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2463347
    iput-object v2, v1, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2463348
    move-object v1, v1

    .line 2463349
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/9JZ;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2463350
    iput-object v2, v1, LX/4Xy;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2463351
    move-object v1, v1

    .line 2463352
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/9JZ;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2463353
    iput-object v2, v1, LX/4Xy;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2463354
    move-object v1, v1

    .line 2463355
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/9JZ;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2463356
    iput-object v2, v1, LX/4Xy;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2463357
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->k()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel$AlbumModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2463358
    new-instance v1, LX/4Vp;

    invoke-direct {v1}, LX/4Vp;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->k()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel$AlbumModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel$AlbumModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 2463359
    iput-object v2, v1, LX/4Vp;->m:Ljava/lang/String;

    .line 2463360
    move-object v1, v1

    .line 2463361
    invoke-virtual {v1}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 2463362
    iput-object v1, v0, LX/4Xy;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2463363
    :cond_1
    invoke-virtual {v0}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLPhoto;LX/HQM;)V
    .locals 5

    .prologue
    .line 2463268
    new-instance v0, LX/6WS;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2463269
    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 2463270
    new-instance v2, LX/HQF;

    invoke-direct {v2, p0, p2, p3}, LX/HQF;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;Lcom/facebook/graphql/model/GraphQLPhoto;LX/HQM;)V

    .line 2463271
    new-instance v3, LX/HQG;

    invoke-direct {v3, p0, p3}, LX/HQG;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;LX/HQM;)V

    .line 2463272
    const v4, 0x7f0815cc

    invoke-virtual {v1, v4}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2463273
    if-eqz p2, :cond_0

    .line 2463274
    const v3, 0x7f0815bf

    invoke-virtual {v1, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2463275
    :cond_0
    invoke-virtual {v0, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2463276
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPhoto;LX/74S;)V
    .locals 4

    .prologue
    .line 2463277
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/9hF;->b(LX/0Px;)LX/9hE;

    move-result-object v0

    .line 2463278
    :goto_0
    invoke-virtual {v0, p2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v1

    const/4 v2, 0x1

    .line 2463279
    iput-boolean v2, v1, LX/9hD;->m:Z

    .line 2463280
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2463281
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    .line 2463282
    :goto_1
    invoke-static {v1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    .line 2463283
    invoke-virtual {v0, v1}, LX/9hD;->a(LX/1bf;)LX/9hD;

    .line 2463284
    new-instance v2, LX/HQH;

    invoke-direct {v2, p0, p1, p2, v1}, LX/HQH;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;Lcom/facebook/graphql/model/GraphQLPhoto;LX/74S;LX/1bf;)V

    .line 2463285
    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->p:LX/23R;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v0

    invoke-interface {v1, v3, v0, v2}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2463286
    return-void

    .line 2463287
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->k()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    invoke-static {v0}, LX/9hF;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/9hE;

    move-result-object v0

    goto :goto_0

    .line 2463288
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2463289
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1

    .line 2463290
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1
.end method

.method private static a(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;LX/CXj;Lcom/facebook/content/SecureContextHelper;LX/9hF;LX/23R;LX/9XE;LX/8Do;LX/Bas;LX/HQT;LX/D2I;)V
    .locals 0

    .prologue
    .line 2463366
    iput-object p1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->m:LX/CXj;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->n:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->o:LX/9hF;

    iput-object p4, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->p:LX/23R;

    iput-object p5, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->q:LX/9XE;

    iput-object p6, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->r:LX/8Do;

    iput-object p7, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->s:LX/Bas;

    iput-object p8, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->t:LX/HQT;

    iput-object p9, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->u:LX/D2I;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    invoke-static {v9}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v1

    check-cast v1, LX/CXj;

    invoke-static {v9}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v9}, LX/9hF;->a(LX/0QB;)LX/9hF;

    move-result-object v3

    check-cast v3, LX/9hF;

    invoke-static {v9}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v4

    check-cast v4, LX/23R;

    invoke-static {v9}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v5

    check-cast v5, LX/9XE;

    invoke-static {v9}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v6

    check-cast v6, LX/8Do;

    invoke-static {v9}, LX/Bas;->b(LX/0QB;)LX/Bas;

    move-result-object v7

    check-cast v7, LX/Bas;

    const-class v8, LX/HQT;

    invoke-interface {v9, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/HQT;

    const-class v10, LX/D2I;

    invoke-interface {v9, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/D2I;

    invoke-static/range {v0 .. v9}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;LX/CXj;Lcom/facebook/content/SecureContextHelper;LX/9hF;LX/23R;LX/9XE;LX/8Do;LX/Bas;LX/HQT;LX/D2I;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;LX/HQM;)V
    .locals 6

    .prologue
    .line 2463367
    sget-object v2, LX/8A9;->NONE:LX/8A9;

    .line 2463368
    sget-object v0, LX/HQK;->c:[I

    invoke-virtual {p1}, LX/HQM;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2463369
    :goto_0
    return-void

    .line 2463370
    :pswitch_0
    sget-object v1, LX/8AB;->PAGE_COVER_PHOTO:LX/8AB;

    .line 2463371
    const/16 v0, 0xc34

    move v5, v0

    move-object v0, v1

    move v1, v5

    .line 2463372
    :goto_1
    new-instance v3, LX/8AA;

    invoke-direct {v3, v0}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v3}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->t()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 2463373
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2463374
    const-string v3, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2463375
    iget-object v3, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->n:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v3, v2, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    .line 2463376
    :pswitch_1
    sget-object v1, LX/8AB;->PAGE_PROFILE_PIC:LX/8AB;

    .line 2463377
    sget-object v2, LX/8A9;->LAUNCH_PROFILE_PIC_CROPPER:LX/8A9;

    .line 2463378
    const/16 v0, 0xc35

    move v5, v0

    move-object v0, v1

    move v1, v5

    .line 2463379
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;Lcom/facebook/graphql/model/GraphQLPhoto;LX/HQM;)V
    .locals 2

    .prologue
    .line 2463380
    if-nez p1, :cond_0

    .line 2463381
    :goto_0
    return-void

    .line 2463382
    :cond_0
    sget-object v0, LX/HQK;->c:[I

    invoke-virtual {p2}, LX/HQM;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2463383
    :pswitch_0
    sget-object v0, LX/74S;->PAGE_COVER_PHOTO:LX/74S;

    .line 2463384
    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a(Lcom/facebook/graphql/model/GraphQLPhoto;LX/74S;)V

    goto :goto_0

    .line 2463385
    :pswitch_1
    sget-object v0, LX/74S;->PAGE_PROFILE_PHOTO:LX/74S;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b$redex0(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2463386
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    .line 2463387
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2463388
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    .line 2463389
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2463390
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v0

    .line 2463391
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-boolean v1, v1, LX/HQa;->a:Z

    if-eqz v1, :cond_1

    .line 2463392
    invoke-static {v0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;)Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    sget-object v1, LX/HQM;->PROFILE_PHOTO:LX/HQM;

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLPhoto;LX/HQM;)V

    .line 2463393
    :goto_1
    return-void

    .line 2463394
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2463395
    :cond_1
    invoke-static {v0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;)Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    sget-object v1, LX/HQM;->PROFILE_PHOTO:LX/HQM;

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a$redex0(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;Lcom/facebook/graphql/model/GraphQLPhoto;LX/HQM;)V

    goto :goto_1
.end method

.method public static c(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2463396
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    .line 2463397
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2463398
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2463399
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    .line 2463400
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2463401
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2463402
    :goto_0
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2463403
    iget-object v2, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-boolean v2, v2, LX/HQa;->a:Z

    if-eqz v2, :cond_1

    .line 2463404
    invoke-static {v1, v0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a(LX/15i;I)Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    sget-object v1, LX/HQM;->COVER_PHOTO:LX/HQM;

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLPhoto;LX/HQM;)V

    .line 2463405
    :goto_1
    return-void

    .line 2463406
    :cond_0
    const/4 v1, 0x0

    const/4 v0, 0x0

    goto :goto_0

    .line 2463407
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2463408
    :cond_1
    invoke-static {v1, v0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a(LX/15i;I)Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    sget-object v1, LX/HQM;->COVER_PHOTO:LX/HQM;

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a$redex0(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;Lcom/facebook/graphql/model/GraphQLPhoto;LX/HQM;)V

    goto :goto_1
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2463409
    const-class v0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2463410
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->t:LX/HQT;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HQT;->a(Landroid/content/Context;)LX/HQS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->v:LX/HQL;

    .line 2463411
    sget-object v0, LX/Bam;->NARROW:LX/Bam;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->d:LX/Bam;

    .line 2463412
    sget-object v0, LX/Bap;->IMAGE:LX/Bap;

    invoke-virtual {p0, v0}, LX/Ban;->setCoverType(LX/Bap;)V

    .line 2463413
    iget-object v0, p0, LX/Ban;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2463414
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2463415
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->k()V

    .line 2463416
    return-void
.end method

.method public static getCoverPhotoId(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2463291
    const/4 v1, 0x0

    .line 2463292
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    .line 2463293
    iget-object v2, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v2

    .line 2463294
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2463295
    if-eqz v3, :cond_0

    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2, v3, v5, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2, v3, v5, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2463296
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2, v3, v5, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 2463297
    :goto_0
    return-object v0

    .line 2463298
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static getProfilePhotoId(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2463160
    const/4 v0, 0x0

    .line 2463161
    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    .line 2463162
    iget-object v2, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v2

    .line 2463163
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v1

    .line 2463164
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2463165
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2463166
    :cond_0
    return-object v0
.end method

.method private getProfileVideoClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2463167
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->D:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2463168
    new-instance v0, LX/HQJ;

    invoke-direct {v0, p0}, LX/HQJ;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->D:Landroid/view/View$OnClickListener;

    .line 2463169
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->D:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public static getProfileVideoController(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)LX/D2H;
    .locals 6

    .prologue
    .line 2463170
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->C:LX/D2H;

    if-nez v0, :cond_0

    .line 2463171
    new-instance v0, LX/HQI;

    invoke-direct {v0, p0}, LX/HQI;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->E:LX/D2J;

    .line 2463172
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->u:LX/D2I;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    iget-object v5, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->E:LX/D2J;

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, LX/D2I;->a(Landroid/content/Context;LX/Ban;ZLX/5w5;LX/D2J;)LX/D2H;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->C:LX/D2H;

    .line 2463173
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->C:LX/D2H;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2463174
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v0, v0, LX/HQa;->g:LX/HQZ;

    iget-object v0, v0, LX/HQZ;->a:LX/HQY;

    sget-object v1, LX/HQY;->NOT_VISIBLE:LX/HQY;

    if-ne v0, v1, :cond_0

    .line 2463175
    invoke-virtual {p0}, LX/Ban;->e()V

    .line 2463176
    :goto_0
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->l()V

    .line 2463177
    return-void

    .line 2463178
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->j()V

    .line 2463179
    invoke-static {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getProfileVideoController(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)LX/D2H;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getProfileVideoClickListener()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/D2H;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private i()V
    .locals 12

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x0

    .line 2463180
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->r:LX/8Do;

    invoke-virtual {v0}, LX/8Do;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2463181
    const v0, 0x7f0e067d

    .line 2463182
    iget-object v2, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const v3, 0x7f0e067e

    invoke-virtual {v2, v3}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleTextAppearance(I)V

    move v2, v0

    .line 2463183
    :goto_0
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v0, v2}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleTextAppearance(I)V

    .line 2463184
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v0, v0, LX/HQa;->c:LX/HQV;

    iget-boolean v0, v0, LX/HQV;->g:Z

    if-eqz v0, :cond_0

    .line 2463185
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2463186
    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2463187
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v0, v0, LX/HQa;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0, v10, v2, v1, v3}, LX/EQR;->a(Ljava/lang/String;Ljava/lang/String;IILandroid/content/Context;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 2463188
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v0, v0, LX/HQa;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2463189
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    .line 2463190
    iget-object v3, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v3

    .line 2463191
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    .line 2463192
    iget-object v3, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v3

    .line 2463193
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->O()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v4, v0

    .line 2463194
    :goto_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->NOT_VERIFIED:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    invoke-virtual {v0, v4}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    invoke-virtual {v0, v4}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    .line 2463195
    :goto_2
    iget-object v11, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    if-eqz v0, :cond_6

    iget-object v3, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v3}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f08177a

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_3
    if-eqz v0, :cond_7

    invoke-static {v4}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->a(Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;)I

    move-result v4

    :goto_4
    const v5, 0x7f021afe

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0d94

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0d58

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    sget-object v9, LX/03R;->UNSET:LX/03R;

    invoke-static {v9}, LX/52d;->a(Ljava/lang/Object;)LX/0Or;

    move-result-object v9

    invoke-static/range {v0 .. v9}, LX/EQR;->a(ZZLandroid/text/SpannableStringBuilder;Ljava/lang/String;IILandroid/content/Context;IILX/0Or;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2463196
    :cond_2
    sget-object v0, LX/HQK;->a:[I

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v1, v1, LX/HQa;->c:LX/HQV;

    iget-object v1, v1, LX/HQV;->d:LX/HQU;

    invoke-virtual {v1}, LX/HQU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2463197
    :goto_5
    return-void

    .line 2463198
    :cond_3
    const v0, 0x7f0e0677

    .line 2463199
    iget-object v2, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    const v3, 0x7f0e0678

    invoke-virtual {v2, v3}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleTextAppearance(I)V

    move v2, v0

    goto/16 :goto_0

    .line 2463200
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    .line 2463201
    iget-object v3, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v3

    .line 2463202
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->O()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    :cond_5
    move v0, v1

    .line 2463203
    goto :goto_2

    :cond_6
    move-object v3, v10

    .line 2463204
    goto :goto_3

    :cond_7
    move v4, v1

    goto :goto_4

    .line 2463205
    :pswitch_0
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08150b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2463206
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v1, v1, LX/HQa;->c:LX/HQV;

    iget-object v1, v1, LX/HQV;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5

    .line 2463207
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v0, v0, LX/HQa;->c:LX/HQV;

    iget-object v0, v0, LX/HQV;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2463208
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v1, v1, LX/HQa;->c:LX/HQV;

    iget-object v1, v1, LX/HQV;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2463209
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v1, v1, LX/HQa;->c:LX/HQV;

    iget-object v1, v1, LX/HQV;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5

    .line 2463210
    :cond_8
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v0, v10}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5

    .line 2463211
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v0, v0, LX/HQa;->c:LX/HQV;

    iget-object v0, v0, LX/HQV;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2463212
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v1, v1, LX/HQa;->c:LX/HQV;

    iget-object v1, v1, LX/HQV;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2463213
    :cond_9
    iget-object v0, p0, LX/Ban;->f:Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;

    invoke-virtual {v0, v10}, Lcom/facebook/caspian/ui/standardheader/StandardHeaderTitlesContainer;->setSubtitleOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private j()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v4, 0x1

    .line 2463214
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v0, v0, LX/HQa;->g:LX/HQZ;

    iget-object v0, v0, LX/HQZ;->c:LX/HQX;

    sget-object v1, LX/HQX;->HAS_DATA:LX/HQX;

    if-ne v0, v1, :cond_1

    move v0, v4

    .line 2463215
    :goto_0
    if-eqz v0, :cond_0

    .line 2463216
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->s:LX/Bas;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v1, v1, LX/HQa;->g:LX/HQZ;

    iget-object v1, v1, LX/HQZ;->d:Ljava/lang/String;

    invoke-static {v1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v2, v2, LX/HQa;->g:LX/HQZ;

    iget-object v2, v2, LX/HQZ;->e:Ljava/lang/String;

    invoke-static {v2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-boolean v3, v3, LX/HQa;->h:Z

    iget-object v5, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-boolean v5, v5, LX/HQa;->a:Z

    sget-object v7, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->w:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v8, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->G:Landroid/view/View$OnClickListener;

    iget-object v6, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->v:LX/HQL;

    invoke-interface {v6}, LX/HQL;->a()LX/1cC;

    move-result-object v9

    invoke-virtual {p0}, LX/Ban;->getProfileImageView()Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    move-result-object v10

    move v6, v4

    invoke-virtual/range {v0 .. v10}, LX/Bas;->a(LX/1bf;LX/1bf;ZZZZLcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;)V

    .line 2463217
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v0, v0, LX/HQa;->g:LX/HQZ;

    iget-object v0, v0, LX/HQZ;->b:LX/HQW;

    sget-object v1, LX/HQW;->VISIBLE:LX/HQW;

    if-ne v0, v1, :cond_0

    .line 2463218
    invoke-virtual {p0}, LX/Ban;->getProfileEditIconViewStub()Landroid/view/ViewStub;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 2463219
    :cond_0
    return-void

    :cond_1
    move v0, v11

    .line 2463220
    goto :goto_0
.end method

.method private k()V
    .locals 15

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2463221
    new-array v9, v3, [Ljava/lang/String;

    .line 2463222
    sget-object v0, LX/Bap;->IMAGE:LX/Bap;

    invoke-virtual {p0, v0}, LX/Ban;->setCoverType(LX/Bap;)V

    .line 2463223
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v1

    iget v2, p0, LX/Ban;->c:I

    sget-object v10, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->x:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v13, 0x1

    move-object v5, v4

    move-object v6, v4

    move v7, v3

    move v8, v3

    move-object v11, v4

    move-object v12, v4

    move v14, v3

    invoke-virtual/range {v0 .. v14}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V

    .line 2463224
    return-void
.end method

.method private l()V
    .locals 15

    .prologue
    .line 2463225
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {p0}, LX/Ban;->getScreenWidth()I

    move-result v1

    iget v2, p0, LX/Ban;->c:I

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v5, v5, LX/HQa;->c:LX/HQV;

    iget-object v5, v5, LX/HQV;->a:Ljava/lang/String;

    invoke-static {v5}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v6, v6, LX/HQa;->c:LX/HQV;

    iget-object v6, v6, LX/HQV;->b:Landroid/graphics/PointF;

    iget-object v7, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-boolean v7, v7, LX/HQa;->a:Z

    iget-object v8, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-boolean v8, v8, LX/HQa;->a:Z

    iget-object v9, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v9, v9, LX/HQa;->c:LX/HQV;

    iget-object v9, v9, LX/HQV;->c:[Ljava/lang/String;

    sget-object v10, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->x:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v11, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->H:Landroid/view/View$OnClickListener;

    iget-object v12, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->v:LX/HQL;

    invoke-interface {v12}, LX/HQL;->b()LX/1cC;

    move-result-object v12

    iget-object v13, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-object v13, v13, LX/HQa;->c:LX/HQV;

    iget-boolean v13, v13, LX/HQV;->f:Z

    iget-object v14, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-boolean v14, v14, LX/HQa;->d:Z

    invoke-virtual/range {v0 .. v14}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a(IIZLX/1bf;LX/1bf;Landroid/graphics/PointF;ZZ[Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;LX/1cC;ZZ)V

    .line 2463226
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2463227
    iget-object v0, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v0, p1}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2463228
    return-void
.end method

.method public final a(ZLandroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, -0x2

    .line 2463229
    if-eqz p1, :cond_0

    .line 2463230
    iput-object p2, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->y:Landroid/view/View;

    .line 2463231
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2463232
    const v1, 0x800055

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2463233
    iget-object v1, p0, LX/Ban;->e:Lcom/facebook/widget/CustomLinearLayout;

    iget-object v2, p0, LX/Ban;->e:Lcom/facebook/widget/CustomLinearLayout;

    iget-object v3, p0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/CustomLinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, p2, v2, v0}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2463234
    :goto_0
    return-void

    .line 2463235
    :cond_0
    if-eqz p2, :cond_1

    .line 2463236
    iget-object v0, p0, LX/Ban;->e:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/CustomLinearLayout;->removeView(Landroid/view/View;)V

    .line 2463237
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->y:Landroid/view/View;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2463238
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    iget-boolean v0, v0, LX/HQa;->b:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2463239
    invoke-static {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getProfileVideoController(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)LX/D2H;

    move-result-object v0

    invoke-virtual {v0}, LX/D2H;->b()Z

    move-result v0

    return v0
.end method

.method public getCaspianPagesHeaderViewHandler()LX/HQL;
    .locals 1

    .prologue
    .line 2463240
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->v:LX/HQL;

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2463241
    invoke-super {p0, p1}, LX/Ban;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2463242
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->b:I

    .line 2463243
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2463244
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    if-eqz v0, :cond_0

    .line 2463245
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->h()V

    .line 2463246
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2463247
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->y:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2463248
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->y:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 2463249
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->y:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2463250
    iget-object v1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->y:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2463251
    :cond_0
    invoke-super {p0, p1, p2}, LX/Ban;->onMeasure(II)V

    .line 2463252
    return-void
.end method

.method public setFragmentUuidForLogging(Landroid/os/ParcelUuid;)V
    .locals 1

    .prologue
    .line 2463253
    iput-object p1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->A:Landroid/os/ParcelUuid;

    .line 2463254
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->v:LX/HQL;

    invoke-interface {v0, p1}, LX/HQL;->a(Landroid/os/ParcelUuid;)V

    .line 2463255
    return-void
.end method

.method public setPageHeaderData(LX/CZd;)V
    .locals 1

    .prologue
    .line 2463256
    iput-object p1, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    .line 2463257
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->B:LX/CZd;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2463258
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->v:LX/HQL;

    invoke-interface {v0, p1, p0}, LX/HQL;->a(LX/CZd;LX/HQN;)LX/HQa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->z:LX/HQa;

    .line 2463259
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->F:Z

    if-nez v0, :cond_0

    .line 2463260
    new-instance v0, LX/HQE;

    invoke-direct {v0, p0}, LX/HQE;-><init>(Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->a(LX/10U;)V

    .line 2463261
    :cond_0
    invoke-virtual {p0}, LX/Ban;->f()V

    .line 2463262
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->i()V

    .line 2463263
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->C:LX/D2H;

    if-eqz v0, :cond_1

    .line 2463264
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->C:LX/D2H;

    .line 2463265
    iput-object p1, v0, LX/D2H;->e:LX/5w5;

    .line 2463266
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->h()V

    .line 2463267
    return-void
.end method
