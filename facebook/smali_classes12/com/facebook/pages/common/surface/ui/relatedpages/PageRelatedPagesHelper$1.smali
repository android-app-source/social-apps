.class public final Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;
.super LX/CXo;
.source ""


# instance fields
.field public final synthetic b:LX/HQq;


# direct methods
.method public constructor <init>(LX/HQq;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2464034
    iput-object p1, p0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;->b:LX/HQq;

    invoke-direct {p0, p2}, LX/CXo;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 7

    .prologue
    .line 2464035
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2464036
    const-string v0, "fetchFollowUpFeedUnitParamsKey"

    new-instance v2, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;->b:LX/HQq;

    iget-object v3, v3, LX/HQq;->d:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->PAGE_LIKE:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    invoke-direct {v2, v3, v4, v5}, Lcom/facebook/api/feed/FetchFollowUpFeedUnitParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)V

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2464037
    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;->b:LX/HQq;

    iget-object v2, v0, LX/HQq;->b:LX/1Ck;

    const-string v3, "related_pages_fetch_task"

    iget-object v0, p0, Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;->b:LX/HQq;

    iget-object v0, v0, LX/HQq;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v4, "feed_fetch_followup_feed_unit"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    const v6, -0x18e639e7

    invoke-static {v0, v4, v1, v5, v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/HQp;

    invoke-direct {v1, p0}, LX/HQp;-><init>(Lcom/facebook/pages/common/surface/ui/relatedpages/PageRelatedPagesHelper$1;)V

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2464038
    return-void
.end method
