.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/HOQ;


# instance fields
.field private a:Lcom/facebook/widget/text/BetterEditTextView;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:I

.field private d:LX/HOU;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2460085
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2460086
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->f()V

    .line 2460087
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2460139
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460140
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->f()V

    .line 2460141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2460136
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2460137
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->f()V

    .line 2460138
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2460128
    const v0, 0x7f030e22

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2460129
    const v0, 0x7f0d2292

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;

    .line 2460130
    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    move-object v0, v1

    .line 2460131
    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2460132
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setInputType(I)V

    .line 2460133
    const v0, 0x7f0d2293

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2460134
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->c:I

    .line 2460135
    return-void
.end method

.method private setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2460126
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2460127
    return-void
.end method

.method private setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2460124
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460125
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/HOU;)V
    .locals 1

    .prologue
    .line 2460119
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->setHint(Ljava/lang/CharSequence;)V

    .line 2460120
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2460121
    invoke-direct {p0, p2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->setText(Ljava/lang/CharSequence;)V

    .line 2460122
    :cond_0
    iput-object p3, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->d:LX/HOU;

    .line 2460123
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2460118
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/CY5;
    .locals 1

    .prologue
    .line 2460109
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2460110
    sget-object v0, LX/CY5;->EMPTY:LX/CY5;

    .line 2460111
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 2460112
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 2460113
    const/4 p0, 0x0

    .line 2460114
    :goto_1
    move v0, p0

    .line 2460115
    if-eqz v0, :cond_1

    sget-object v0, LX/CY5;->NONE:LX/CY5;

    goto :goto_0

    :cond_1
    sget-object v0, LX/CY5;->INVALID:LX/CY5;

    goto :goto_0

    .line 2460116
    :cond_2
    sget-object p0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {p0, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    .line 2460117
    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result p0

    goto :goto_1
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2460099
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->c:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2460100
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2460101
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->b:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0d2293

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2460102
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2460103
    const v1, 0x7f081682

    .line 2460104
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2460105
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460106
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->d:LX/HOU;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v1, v2, v3

    invoke-interface {v0, v2}, LX/HOU;->a([Ljava/lang/String;)V

    .line 2460107
    return-void

    .line 2460108
    :cond_0
    const v1, 0x7f081683

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2460096
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2460097
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->requestFocus()Z

    .line 2460098
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2460093
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 2460094
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->b:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2460095
    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2460089
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2460090
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2460091
    :goto_0
    move-object v0, v0

    .line 2460092
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    sget-object p0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, p0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2460088
    return-object p0
.end method
