.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/HOQ;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2460902
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2460903
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->f()V

    .line 2460904
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2460882
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460883
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->f()V

    .line 2460884
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2460899
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2460900
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->f()V

    .line 2460901
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2460898
    iput-object p1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->b:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;

    const/16 v1, 0x455

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xb19

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->a(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 2460894
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2460895
    const v0, 0x7f030e34

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2460896
    const v0, 0x7f0d22a7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2460897
    return-void
.end method


# virtual methods
.method public final a(LX/175;LX/HOU;)V
    .locals 2

    .prologue
    .line 2460892
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    new-instance v1, LX/HOn;

    invoke-direct {v1, p0, p2}, LX/HOn;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;LX/HOU;)V

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/175;LX/8uH;)V

    .line 2460893
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2460891
    const/4 v0, 0x0

    return v0
.end method

.method public final b()LX/CY5;
    .locals 1

    .prologue
    .line 2460890
    sget-object v0, LX/CY5;->NONE:LX/CY5;

    return-object v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2460889
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2460888
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 2460887
    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2460886
    const/4 v0, 0x0

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2460885
    return-object p0
.end method
