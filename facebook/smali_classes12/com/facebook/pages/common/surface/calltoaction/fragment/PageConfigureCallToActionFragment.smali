.class public Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->PAGE_CONFIGURE_CALL_TO_ACTION_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public A:Z

.field public B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

.field public C:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

.field public D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

.field public E:LX/CYE;

.field public F:LX/HOg;

.field public G:LX/HNc;

.field private final H:LX/63W;

.field private final I:Landroid/view/View$OnClickListener;

.field private final J:Landroid/view/View$OnClickListener;

.field private final K:Landroid/view/View$OnClickListener;

.field private L:Z

.field public M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

.field public final N:Landroid/content/DialogInterface$OnClickListener;

.field public final O:Landroid/content/DialogInterface$OnClickListener;

.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CY3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/CYR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/H0t;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Gze;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/HOh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/HNW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/GF2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/2Pb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/HNd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/CYT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/HNV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

.field public s:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

.field private t:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

.field private u:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

.field private v:Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;

.field private w:Landroid/view/View;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2459456
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2459457
    new-instance v0, LX/HO3;

    invoke-direct {v0, p0}, LX/HO3;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->H:LX/63W;

    .line 2459458
    new-instance v0, LX/HO1;

    invoke-direct {v0, p0}, LX/HO1;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->I:Landroid/view/View$OnClickListener;

    .line 2459459
    new-instance v0, LX/HO4;

    invoke-direct {v0, p0}, LX/HO4;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->J:Landroid/view/View$OnClickListener;

    .line 2459460
    new-instance v0, LX/HO2;

    invoke-direct {v0, p0}, LX/HO2;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->K:Landroid/view/View$OnClickListener;

    .line 2459461
    iput-boolean v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->L:Z

    .line 2459462
    new-instance v0, LX/HNp;

    invoke-direct {v0, p0}, LX/HNp;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->N:Landroid/content/DialogInterface$OnClickListener;

    .line 2459463
    new-instance v0, LX/HNq;

    invoke-direct {v0, p0}, LX/HNq;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->O:Landroid/content/DialogInterface$OnClickListener;

    .line 2459464
    return-void
.end method

.method public static a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Ljava/lang/String;Ljava/lang/String;LX/CYE;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;
    .locals 3
    .param p4    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459427
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;-><init>()V

    .line 2459428
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2459429
    const-string v2, "arg_page_admin_cta"

    invoke-static {v1, v2, p0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459430
    const-string v2, "arg_page_admin_info"

    invoke-static {v1, v2, p1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459431
    const-string v2, "arg_admin_config"

    invoke-static {v1, v2, p2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459432
    const-string v2, "arg_page_id"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459433
    const-string v2, "arg_cta_type"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459434
    const-string v2, "arg_config_action_data"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2459435
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2459436
    return-object v0
.end method

.method private static a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;LX/1Ck;LX/CY3;LX/0Ot;LX/CYR;Lcom/facebook/content/SecureContextHelper;LX/0ad;LX/0if;LX/H0t;LX/Gze;LX/HOh;LX/HNW;LX/GF2;LX/2Pb;LX/HNd;LX/0Ot;LX/CYT;LX/HNV;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;",
            "LX/1Ck;",
            "LX/CY3;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/CYR;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0ad;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/H0t;",
            "LX/Gze;",
            "LX/HOh;",
            "LX/HNW;",
            "LX/GF2;",
            "LX/2Pb;",
            "LX/HNd;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/CYT;",
            "LX/HNV;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2459437
    iput-object p1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a:LX/1Ck;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->b:LX/CY3;

    iput-object p3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->d:LX/CYR;

    iput-object p5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object p6, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->f:LX/0ad;

    iput-object p7, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    iput-object p8, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->h:LX/H0t;

    iput-object p9, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->i:LX/Gze;

    iput-object p10, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->j:LX/HOh;

    iput-object p11, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->k:LX/HNW;

    iput-object p12, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->l:LX/GF2;

    iput-object p13, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->m:LX/2Pb;

    iput-object p14, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->n:LX/HNd;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->o:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p:LX/CYT;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->q:LX/HNV;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 21

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v19

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-static/range {v19 .. v19}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static/range {v19 .. v19}, LX/CY3;->a(LX/0QB;)LX/CY3;

    move-result-object v4

    check-cast v4, LX/CY3;

    const/16 v5, 0x259

    move-object/from16 v0, v19

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {v19 .. v19}, LX/CYR;->a(LX/0QB;)LX/CYR;

    move-result-object v6

    check-cast v6, LX/CYR;

    invoke-static/range {v19 .. v19}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v19 .. v19}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static/range {v19 .. v19}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v9

    check-cast v9, LX/0if;

    invoke-static/range {v19 .. v19}, LX/H0t;->a(LX/0QB;)LX/H0t;

    move-result-object v10

    check-cast v10, LX/H0t;

    invoke-static/range {v19 .. v19}, LX/Gze;->a(LX/0QB;)LX/Gze;

    move-result-object v11

    check-cast v11, LX/Gze;

    const-class v12, LX/HOh;

    move-object/from16 v0, v19

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/HOh;

    invoke-static/range {v19 .. v19}, LX/HNW;->a(LX/0QB;)LX/HNW;

    move-result-object v13

    check-cast v13, LX/HNW;

    invoke-static/range {v19 .. v19}, LX/GF2;->a(LX/0QB;)LX/GF2;

    move-result-object v14

    check-cast v14, LX/GF2;

    invoke-static/range {v19 .. v19}, LX/2Pb;->a(LX/0QB;)LX/2Pb;

    move-result-object v15

    check-cast v15, LX/2Pb;

    const-class v16, LX/HNd;

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/HNd;

    const/16 v17, 0x2eb

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {v19 .. v19}, LX/CYT;->a(LX/0QB;)LX/CYT;

    move-result-object v18

    check-cast v18, LX/CYT;

    const-class v20, LX/HNV;

    invoke-interface/range {v19 .. v20}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/HNV;

    invoke-static/range {v2 .. v19}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;LX/1Ck;LX/CY3;LX/0Ot;LX/CYR;Lcom/facebook/content/SecureContextHelper;LX/0ad;LX/0if;LX/H0t;LX/Gze;LX/HOh;LX/HNW;LX/GF2;LX/2Pb;LX/HNd;LX/0Ot;LX/CYT;LX/HNV;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2459438
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->aG:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-static {v2, v3, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2459439
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2459440
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Z)V

    .line 2459441
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Ljava/lang/String;)V

    .line 2459442
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2459443
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->b:LX/CY3;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    sget-object v2, LX/CY5;->SERVER:LX/CY5;

    invoke-virtual {v0, v1, v2}, LX/CY3;->a(Ljava/lang/String;LX/CY5;)V

    .line 2459444
    return-void
.end method

.method public static b$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2459445
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {v0, v5}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Z)V

    .line 2459446
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2459447
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p:LX/CYT;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CYT;->b(Ljava/lang/String;)V

    .line 2459448
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2459449
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->y(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    .line 2459450
    :goto_0
    return-void

    .line 2459451
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v1, LX/0ig;->ak:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2459452
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->d:LX/CYR;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081688

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/CYR;->a(Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 2459453
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->F:LX/HOg;

    invoke-virtual {v0, p1}, LX/HOg;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static d(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 2459358
    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2459359
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->l(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->C:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->C:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->f:LX/0ad;

    sget-short v1, LX/8wM;->b:S

    invoke-interface {v0, v1, v8}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2459360
    :cond_1
    const v0, 0x7f0d22b7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2459361
    const v0, 0x7f0d22b8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2459362
    :goto_0
    const v0, 0x7f0d22b0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2459363
    const v1, 0x7f0d22b3

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    .line 2459364
    const v2, 0x7f0d22b4

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    .line 2459365
    const v3, 0x7f0d22b5

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    .line 2459366
    const v4, 0x7f0d22b6

    invoke-virtual {p0, v4}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    .line 2459367
    iget-boolean v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-eqz v5, :cond_4

    .line 2459368
    invoke-virtual {v2, v6}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    .line 2459369
    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->I:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459370
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->l(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2459371
    :cond_2
    invoke-virtual {v3, v6}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    .line 2459372
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->J:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459373
    invoke-virtual {v4, v6}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    .line 2459374
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->K:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459375
    :cond_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2459376
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->k()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2459377
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081670

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setText(Ljava/lang/String;)V

    .line 2459378
    :goto_1
    invoke-virtual {v1, v6}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    .line 2459379
    new-instance v2, LX/HNu;

    invoke-direct {v2, p0}, LX/HNu;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459380
    :cond_4
    iget-boolean v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->t:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->isShown()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->u:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->isShown()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2459381
    :cond_5
    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2459382
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->w:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a010a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2459383
    :cond_6
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p()V

    .line 2459384
    return-void

    .line 2459385
    :cond_7
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08166f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setText(Ljava/lang/String;)V

    goto :goto_1

    .line 2459386
    :cond_8
    const v0, 0x7f0d22ba

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    .line 2459387
    const v1, 0x7f0d22bb

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    .line 2459388
    const v2, 0x7f0d22b7

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2459389
    const v2, 0x7f0d22b8

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2459390
    const v2, 0x7f0d22b9

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2459391
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08169a

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2459392
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08169b

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2459393
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->C:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x74836b19

    invoke-static {v3, v2, v8, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_2
    invoke-virtual {v2, v8}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    sget-object v4, LX/HNr;->a:[I

    const-class v5, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v3, v2, v8, v5, v7}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    .line 2459394
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08169d

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setText(Ljava/lang/String;)V

    .line 2459395
    :goto_3
    invoke-virtual {v0, v8}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    .line 2459396
    new-instance v2, LX/HNw;

    invoke-direct {v2, p0}, LX/HNw;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459397
    sget-object v0, LX/HNr;->b:[I

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_1

    .line 2459398
    invoke-virtual {v1, v10}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    goto/16 :goto_0

    .line 2459399
    :cond_9
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_2

    .line 2459400
    :pswitch_0
    const v2, 0x7f08166e

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setText(I)V

    goto :goto_3

    .line 2459401
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->l:LX/GF2;

    .line 2459402
    iget-object v2, v0, LX/GF2;->f:LX/0ad;

    sget-short v3, LX/GF3;->a:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 2459403
    if-nez v0, :cond_a

    .line 2459404
    invoke-virtual {v1, v10}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    goto/16 :goto_0

    .line 2459405
    :cond_a
    new-instance v0, LX/HNx;

    invoke-direct {v0, p0}, LX/HNx;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459406
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0816a8

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setText(Ljava/lang/String;)V

    .line 2459407
    invoke-virtual {v1, v8}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static k$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 5

    .prologue
    .line 2459454
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    iget-boolean v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;Z)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminPromoteAddServiceFragment;

    move-result-object v1

    invoke-static {v0, p0, v1}, LX/CYR;->a(LX/0gc;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2459455
    return-void
.end method

.method public static l(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)Z
    .locals 2

    .prologue
    .line 2459532
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->E:LX/CYE;

    if-eqz v0, :cond_0

    const-string v0, "PRIMARY_BUTTONS"

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->E:LX/CYE;

    .line 2459533
    iget-object p0, v1, LX/CYE;->mActionChannelType:Ljava/lang/String;

    move-object v1, p0

    .line 2459534
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 2459493
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->L:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_QUOTE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2459494
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->i:LX/Gze;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/Gze;->a(Ljava/lang/String;Z)V

    .line 2459495
    iput-boolean v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->L:Z

    .line 2459496
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-static {v0}, LX/CYR;->f(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2459497
    const v0, 0x7f0d22af

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2459498
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2459499
    const v0, 0x7f0d145a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->v:Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;

    .line 2459500
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->v:Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2459501
    invoke-static {v1}, LX/CYR;->b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2459502
    :cond_1
    :goto_0
    move-object v1, v3

    .line 2459503
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->setFormTitle(Ljava/lang/String;)V

    .line 2459504
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->v:Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;

    new-instance v1, LX/HNv;

    invoke-direct {v1, p0}, LX/HNv;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->setEditFormCtaClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459505
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2459506
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-nez v0, :cond_3

    .line 2459507
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Z)V

    .line 2459508
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->w()V

    .line 2459509
    :cond_3
    :goto_1
    return-void

    .line 2459510
    :cond_4
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->d(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    goto :goto_1

    .line 2459511
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$ComponentFlowServiceConfigModel;

    move-result-object v4

    if-nez v4, :cond_6

    move v4, v5

    :goto_2
    if-eqz v4, :cond_8

    move v4, v5

    :goto_3
    if-nez v4, :cond_1

    .line 2459512
    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$ComponentFlowServiceConfigModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$ComponentFlowServiceConfigModel;->j()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v4, v3, v5}, LX/15i;->g(II)I

    move-result v3

    invoke-virtual {v4, v3, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 2459513
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$ComponentFlowServiceConfigModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$ComponentFlowServiceConfigModel;->j()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    .line 2459514
    if-nez v4, :cond_7

    move v4, v5

    goto :goto_2

    :cond_7
    move v4, v6

    goto :goto_2

    .line 2459515
    :cond_8
    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$ComponentFlowServiceConfigModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$ComponentFlowServiceConfigModel;->j()LX/1vs;

    move-result-object v4

    iget-object v7, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2459516
    invoke-virtual {v7, v4, v5}, LX/15i;->g(II)I

    move-result v4

    if-nez v4, :cond_9

    move v4, v5

    goto :goto_3

    :cond_9
    move v4, v6

    goto :goto_3
.end method

.method public static n(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 2459517
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-static {v0}, LX/CYR;->f(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->h:LX/H0t;

    .line 2459518
    iget-boolean v1, v0, LX/H0t;->b:Z

    move v0, v1

    .line 2459519
    if-eqz v0, :cond_1

    .line 2459520
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->v:Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;

    if-eqz v0, :cond_0

    .line 2459521
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->v:Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->h:LX/H0t;

    .line 2459522
    iget-object v2, v1, LX/H0t;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2459523
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/professionalservices/getquote/ui/GetQuoteFormBuilderEditFormBox;->setFormTitle(Ljava/lang/String;)V

    .line 2459524
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p()V

    .line 2459525
    return-void

    .line 2459526
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2459527
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p:LX/CYT;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CYT;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2459528
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->d:LX/CYR;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081688

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/CYR;->a(Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    .line 2459529
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p:LX/CYT;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CYT;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2459530
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {v0, v3}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Z)V

    .line 2459531
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->w()V

    goto :goto_0
.end method

.method private p()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2459466
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2459467
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    if-eqz v1, :cond_1

    .line 2459468
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v3

    .line 2459469
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2459470
    const v1, 0x7f081669

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2459471
    :goto_0
    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2459472
    iget-boolean v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-eqz v1, :cond_4

    .line 2459473
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-static {v1}, LX/CYR;->f(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_0
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2459474
    if-eqz v1, :cond_4

    .line 2459475
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 2459476
    :cond_1
    :goto_2
    return-void

    .line 2459477
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f08166c

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f08166b

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 2459478
    :cond_4
    iget-boolean v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-eqz v1, :cond_5

    .line 2459479
    const v1, 0x7f081664

    .line 2459480
    :goto_4
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2459481
    iput-object v1, v3, LX/108;->g:Ljava/lang/String;

    .line 2459482
    move-object v1, v3

    .line 2459483
    iput-boolean v2, v1, LX/108;->d:Z

    .line 2459484
    move-object v1, v1

    .line 2459485
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2459486
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->H:LX/63W;

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    goto :goto_2

    .line 2459487
    :cond_5
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p:LX/CYT;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v1, v3}, LX/CYT;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2459488
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->q()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2459489
    const v1, 0x7f081666

    .line 2459490
    const/4 v2, 0x0

    goto :goto_4

    .line 2459491
    :cond_6
    const v1, 0x7f081665

    goto :goto_4

    .line 2459492
    :cond_7
    const v1, 0x7f081666

    goto :goto_4

    :cond_8
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private q()Z
    .locals 1

    .prologue
    .line 2459465
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static s$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 4

    .prologue
    .line 2459408
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2459409
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    invoke-static {v1, v0}, LX/CYR;->a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;

    move-result-object v1

    .line 2459410
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2459411
    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    const/4 v1, 0x0

    .line 2459412
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_5

    .line 2459413
    :cond_1
    :goto_1
    move v0, v1

    .line 2459414
    if-eqz v0, :cond_3

    .line 2459415
    :cond_2
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->t(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    .line 2459416
    :goto_2
    return-void

    .line 2459417
    :cond_3
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->u(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1
.end method

.method public static t(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/16 v1, 0x8

    .line 2459418
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->s:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    invoke-virtual {v2, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->setVisibility(I)V

    .line 2459419
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {v2, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->setVisibility(I)V

    .line 2459420
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->s:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iget-boolean v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->a(ZLcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Ljava/lang/String;)V

    .line 2459421
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->u:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2459422
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    invoke-static {v4, v3}, LX/CYR;->a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    move v3, v4

    .line 2459423
    if-eqz v3, :cond_0

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    .line 2459424
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->t:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    .line 2459425
    return-void

    :cond_0
    move v0, v1

    .line 2459426
    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static u(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 13

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 2459243
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->s:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    invoke-virtual {v0, v6}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->setVisibility(I)V

    .line 2459244
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {v0, v5}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->setVisibility(I)V

    .line 2459245
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2459246
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)V

    .line 2459247
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->t:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2459248
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    invoke-static {v2, v1}, LX/CYR;->a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;

    move-result-object v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 2459249
    if-eqz v1, :cond_7

    :goto_2
    invoke-virtual {v0, v5}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    .line 2459250
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->u:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    invoke-virtual {v0, v6}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setVisibility(I)V

    .line 2459251
    return-void

    .line 2459252
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    .line 2459253
    const/4 v3, 0x0

    .line 2459254
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->l()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v4

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    if-eq v4, v7, :cond_1

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->l()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v4

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->NONE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    if-ne v4, v7, :cond_2

    .line 2459255
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->o()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;

    move-result-object v3

    .line 2459256
    :cond_2
    move-object v3, v3

    .line 2459257
    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    move-object v0, p0

    .line 2459258
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 2459259
    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;->a()LX/0Px;

    move-result-object v9

    .line 2459260
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v7, 0x0

    move v8, v7

    :goto_3
    if-ge v8, v10, :cond_6

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;

    .line 2459261
    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel$FieldsModel;

    move-result-object v11

    if-eqz v11, :cond_9

    .line 2459262
    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    move-result-object v11

    .line 2459263
    if-eqz v5, :cond_3

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    if-eq v11, v12, :cond_5

    :cond_3
    if-nez v5, :cond_9

    .line 2459264
    sget-object v12, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->LEAD_GEN:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    if-eq v11, v12, :cond_4

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->PHONE_CALL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    if-eq v11, v12, :cond_4

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->MESSENGER:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    if-eq v11, v12, :cond_4

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    if-eq v11, v12, :cond_4

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->FIRST_PARTY:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    if-ne v11, v12, :cond_a

    :cond_4
    const/4 v12, 0x1

    :goto_4
    move v11, v12

    .line 2459265
    if-eqz v11, :cond_9

    .line 2459266
    :cond_5
    iget-object v8, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->d:LX/CYR;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v9

    .line 2459267
    sget-object v10, LX/CYQ;->a:[I

    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 2459268
    :goto_5
    move-object v8, v9

    .line 2459269
    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel$FieldsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel$FieldsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v2, v3, v8, v7, v4}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 2459270
    :cond_6
    goto/16 :goto_0

    :cond_7
    move v5, v6

    .line 2459271
    goto/16 :goto_2

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 2459272
    :cond_9
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_3

    :cond_a
    const/4 v12, 0x0

    goto :goto_4

    .line 2459273
    :pswitch_0
    iget-object v10, v8, LX/CYR;->a:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f081675

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_5

    .line 2459274
    :pswitch_1
    iget-object v10, v8, LX/CYR;->a:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f081676

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_5

    .line 2459275
    :pswitch_2
    iget-object v10, v8, LX/CYR;->a:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f081677

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_5

    .line 2459276
    :pswitch_3
    iget-object v10, v8, LX/CYR;->a:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f081678

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private w()V
    .locals 8

    .prologue
    .line 2459277
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a:LX/1Ck;

    const-string v1, "fetch_request_time_CTA_setup_info"

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->k:LX/HNW;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2459278
    new-instance v3, LX/8EP;

    invoke-direct {v3}, LX/8EP;-><init>()V

    move-object v3, v3

    .line 2459279
    const-string v6, "page_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/8EP;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v6, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v3, v6}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v3

    .line 2459280
    iget-object v6, v2, LX/HNW;->b:LX/0tX;

    invoke-virtual {v6, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-static {v3}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v2, v3

    .line 2459281
    new-instance v3, LX/HNz;

    invoke-direct {v3, p0}, LX/HNz;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2459282
    return-void
.end method

.method public static x(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)Z
    .locals 1

    .prologue
    .line 2459283
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->E:LX/CYE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->E:LX/CYE;

    .line 2459284
    iget-boolean p0, v0, LX/CYE;->mUseActionFlow:Z

    move v0, p0

    .line 2459285
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static y(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V
    .locals 1

    .prologue
    .line 2459286
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2459287
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->k$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    .line 2459288
    :cond_0
    :goto_0
    return-void

    .line 2459289
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->M:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->k()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2459290
    const-string v0, "admin_publish_services"

    invoke-static {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final S_()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2459291
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    if-nez v2, :cond_1

    .line 2459292
    :cond_0
    :goto_0
    return v0

    .line 2459293
    :cond_1
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->b:LX/CY3;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->s:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->isShown()Z

    move-result v4

    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v5

    invoke-static {v4, v5}, LX/CYR;->a(ZLcom/facebook/graphql/enums/GraphQLPageCallToActionType;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/CY3;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459294
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p:LX/CYT;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/CYT;->c(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->p:LX/CYT;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/CYT;->f(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2459295
    :cond_2
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v3, LX/0ig;->bd:LX/0ih;

    const-string v4, "tap_back"

    const-string v5, "on_cta_config_entry"

    invoke-virtual {v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2459296
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->d:LX/CYR;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081688

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v6}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v2, v0, v3, v4}, LX/CYR;->a(Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;)V

    move v0, v1

    .line 2459297
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459298
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2459299
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2459300
    if-eqz p1, :cond_0

    .line 2459301
    const-string v0, "state_logged_get_quote_edit_flow"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->L:Z

    .line 2459302
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2459303
    const-string v0, "arg_page_admin_cta"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    .line 2459304
    const-string v0, "arg_page_admin_info"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->C:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    .line 2459305
    const-string v0, "arg_admin_config"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2459306
    const-string v0, "arg_page_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    .line 2459307
    const-string v0, "arg_cta_type"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->y:Ljava/lang/String;

    .line 2459308
    const-string v0, "arg_config_action_data"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CYE;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->E:LX/CYE;

    .line 2459309
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    if-nez v0, :cond_1

    .line 2459310
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a:LX/1Ck;

    const-string v1, "fecth_page_call_to_admin_model"

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->q:LX/HNV;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/HNV;->a(Ljava/lang/String;)LX/HNU;

    move-result-object v2

    invoke-virtual {v2}, LX/HNU;->c()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/HNy;

    invoke-direct {v3, p0}, LX/HNy;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2459311
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->j:LX/HOh;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->C:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->E:LX/CYE;

    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/HOh;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;LX/CYE;Ljava/lang/String;Landroid/app/Activity;)LX/HOg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->F:LX/HOg;

    .line 2459312
    return-void

    .line 2459313
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->z:Z

    goto :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2459314
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->s:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2459315
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->s:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->getFieldValues()Ljava/util/Map;

    move-result-object v0

    .line 2459316
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getFieldValues()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2459317
    const/16 v0, 0xd6

    if-ne p1, v0, :cond_0

    .line 2459318
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->m:LX/2Pb;

    sget-object v1, LX/8wW;->EXIT_FLOW:LX/8wW;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    const-string v3, "edit_cta"

    invoke-virtual {v0, v1, v2, v3}, LX/2Pb;->a(LX/8wW;Ljava/lang/String;Ljava/lang/String;)V

    .line 2459319
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 2459320
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->m:LX/2Pb;

    sget-object v1, LX/8wW;->SUBMIT_FLOW_CLICK:LX/8wW;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    const-string v3, "edit_cta"

    invoke-virtual {v0, v1, v2, v3}, LX/2Pb;->a(LX/8wW;Ljava/lang/String;Ljava/lang/String;)V

    .line 2459321
    :cond_0
    :goto_0
    return-void

    .line 2459322
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->m:LX/2Pb;

    sget-object v1, LX/8wW;->CANCEL_FLOW:LX/8wW;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x:Ljava/lang/String;

    const-string v3, "edit_cta"

    invoke-virtual {v0, v1, v2, v3}, LX/2Pb;->a(LX/8wW;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x7a738990

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459323
    const v1, 0x7f030e3d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->w:Landroid/view/View;

    .line 2459324
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->w:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x63dc019e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x30e69497

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459325
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2459326
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2459327
    const/16 v1, 0x2b

    const v2, 0x7c3ed273

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x78ac0d15

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459328
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->d:LX/CYR;

    .line 2459329
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2459330
    invoke-virtual {v1, v2}, LX/CYR;->a(Landroid/view/View;)V

    .line 2459331
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2459332
    const/16 v1, 0x2b

    const v2, -0x791898a9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3add758f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459333
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2459334
    iget-boolean v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->z:Z

    if-eqz v1, :cond_0

    .line 2459335
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->n(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    .line 2459336
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x7e495fea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2459337
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2459338
    const-string v0, "state_logged_get_quote_edit_flow"

    iget-boolean v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->L:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2459339
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x512e22b8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459340
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2459341
    iget-boolean v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->z:Z

    if-eqz v1, :cond_0

    .line 2459342
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->s$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    .line 2459343
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x41c43d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2459344
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2459345
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->x(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2459346
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    invoke-static {v2}, LX/CYR;->b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->l()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v3

    if-ne v2, v3, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    .line 2459347
    :goto_1
    const v0, 0x7f0d22ad

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->r:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    .line 2459348
    const v0, 0x7f0d22ae

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->s:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    .line 2459349
    const v0, 0x7f0d22b1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->t:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    .line 2459350
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->t:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    new-instance v1, LX/HNs;

    invoke-direct {v1, p0}, LX/HNs;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459351
    const v0, 0x7f0d22b2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->u:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    .line 2459352
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->u:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;

    new-instance v1, LX/HNt;

    invoke-direct {v1, p0}, LX/HNt;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459353
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->z:Z

    if-eqz v0, :cond_0

    .line 2459354
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->m$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;)V

    .line 2459355
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 2459356
    goto :goto_0

    .line 2459357
    :cond_2
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->B:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    invoke-static {v2}, LX/CYR;->b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)Z

    move-result v2

    if-nez v2, :cond_3

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->A:Z

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
