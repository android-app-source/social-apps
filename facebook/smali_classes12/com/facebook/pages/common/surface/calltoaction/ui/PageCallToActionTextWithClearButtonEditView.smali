.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/HOQ;


# instance fields
.field public a:Lcom/facebook/widget/text/BetterEditTextView;

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private final c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2460837
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2460838
    new-instance v0, LX/HOm;

    invoke-direct {v0, p0}, LX/HOm;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->c:Landroid/view/View$OnClickListener;

    .line 2460839
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->f()V

    .line 2460840
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2460868
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460869
    new-instance v0, LX/HOm;

    invoke-direct {v0, p0}, LX/HOm;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->c:Landroid/view/View$OnClickListener;

    .line 2460870
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->f()V

    .line 2460871
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2460864
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2460865
    new-instance v0, LX/HOm;

    invoke-direct {v0, p0}, LX/HOm;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->c:Landroid/view/View$OnClickListener;

    .line 2460866
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->f()V

    .line 2460867
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2460859
    const v0, 0x7f030e32

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2460860
    const v0, 0x7f0d22a6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2460861
    const v0, 0x7f0d088b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2460862
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2460863
    return-void
.end method

.method private setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2460857
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2460858
    return-void
.end method

.method private setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2460855
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460856
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2460851
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->setHint(Ljava/lang/CharSequence;)V

    .line 2460852
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2460853
    invoke-direct {p0, p2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->setText(Ljava/lang/CharSequence;)V

    .line 2460854
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2460872
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/CY5;
    .locals 1

    .prologue
    .line 2460850
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/CY5;->NONE:LX/CY5;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/CY5;->EMPTY:LX/CY5;

    goto :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2460849
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2460846
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2460847
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->requestFocus()Z

    .line 2460848
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2460844
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 2460845
    return-void
.end method

.method public getEditTextView()Lcom/facebook/widget/text/BetterEditTextView;
    .locals 1

    .prologue
    .line 2460843
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2460842
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2460841
    return-object p0
.end method
