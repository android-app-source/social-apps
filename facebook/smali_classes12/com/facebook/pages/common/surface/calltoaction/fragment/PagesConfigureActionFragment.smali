.class public Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:J

.field public b:LX/CYE;

.field public c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

.field private d:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2459720
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;Lcom/facebook/base/fragment/FbFragment;)V
    .locals 2

    .prologue
    .line 2459721
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2459722
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2459723
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2459724
    iget v1, p0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v1, v1

    .line 2459725
    invoke-virtual {v0, v1, p1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2459726
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459727
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2459728
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2459729
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->a:J

    .line 2459730
    const-string v0, "extra_config_action_data"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CYE;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->b:LX/CYE;

    .line 2459731
    const-string v0, "extra_action_channel_edit_action"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    .line 2459732
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7d5e60ed

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459733
    const v1, 0x7f030ec5

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x8928f4d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2062eee8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459734
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2459735
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2459736
    if-eqz v1, :cond_0

    .line 2459737
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2459738
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x37c64d14

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 11
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459739
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2459740
    const v0, 0x7f0d2411

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2459741
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    if-nez v0, :cond_0

    .line 2459742
    const/4 v5, 0x0

    .line 2459743
    iget-wide v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->a:J

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->b:LX/CYE;

    invoke-static {v2, v3, v4, v5, v5}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->a(JLX/CYE;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2459744
    :goto_0
    return-void

    .line 2459745
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x7ceb8589

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->k()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$FormFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2459746
    iget-wide v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->a:J

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->k()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$FormFieldsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$FormFieldsModel;->a()LX/0Px;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v7, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->o()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->b:LX/CYE;

    iget-object v10, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v10}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gB_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v10

    check-cast v10, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-static/range {v2 .. v10}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a(JLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/ArrayList;Ljava/lang/String;Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;LX/CYE;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2459747
    goto :goto_0

    .line 2459748
    :cond_1
    iget-wide v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->a:J

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->b:LX/CYE;

    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    sget-object v6, LX/CYF;->EDIT_ACTION:LX/CYF;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->a(JLX/CYE;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;LX/CYF;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesConfigureActionFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2459749
    goto :goto_0
.end method
