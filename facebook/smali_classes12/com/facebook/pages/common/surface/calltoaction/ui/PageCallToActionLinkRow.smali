.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field private j:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2460441
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2460442
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460443
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2460438
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460439
    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460440
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2460435
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2460436
    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460437
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 2460422
    const v0, 0x7f030e28

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2460423
    const v0, 0x7f0d2298

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2460424
    if-eqz p2, :cond_0

    .line 2460425
    sget-object v0, LX/03r;->PageCallToActionLinkRow:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2460426
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x1

    invoke-static {v1, v0, v2}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    .line 2460427
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460428
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->j:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x0

    const/high16 v3, -0x1000000

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2460429
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2460430
    :cond_0
    return-void
.end method


# virtual methods
.method public setText(I)V
    .locals 1

    .prologue
    .line 2460433
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2460434
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2460431
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkRow;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460432
    return-void
.end method
