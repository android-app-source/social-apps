.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/HOQ;
.implements Ljava/util/Observer;


# instance fields
.field private a:Lcom/facebook/widget/text/BetterEditTextView;

.field public b:Lcom/facebook/widget/text/BetterTextView;

.field private c:I

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:I

.field private final f:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2460703
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2460704
    new-instance v0, LX/HOi;

    invoke-direct {v0, p0}, LX/HOi;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->f:Landroid/text/TextWatcher;

    .line 2460705
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->f()V

    .line 2460706
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2460699
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460700
    new-instance v0, LX/HOi;

    invoke-direct {v0, p0}, LX/HOi;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->f:Landroid/text/TextWatcher;

    .line 2460701
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->f()V

    .line 2460702
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2460695
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2460696
    new-instance v0, LX/HOi;

    invoke-direct {v0, p0}, LX/HOi;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->f:Landroid/text/TextWatcher;

    .line 2460697
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->f()V

    .line 2460698
    return-void
.end method

.method private a(Landroid/text/Editable;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2460694
    iget v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->e:I

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;Landroid/text/Editable;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2460693
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a(Landroid/text/Editable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2460684
    const v0, 0x7f030e2b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2460685
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->e:I

    .line 2460686
    const v0, 0x7f0d229f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2460687
    const v0, 0x7f0d22a0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2460688
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a(Landroid/text/Editable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460689
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->f:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2460690
    const v0, 0x7f0d2293

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2460691
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->c:I

    .line 2460692
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2460679
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2460680
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2460681
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 2460682
    :goto_0
    return-void

    .line 2460683
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterEditTextView;->append(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2460707
    iput p3, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->e:I

    .line 2460708
    invoke-virtual {p0, p1, p2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2460709
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2460678
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/CY5;
    .locals 2

    .prologue
    .line 2460675
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 2460676
    sget-object v0, LX/CY5;->NONE:LX/CY5;

    .line 2460677
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, LX/CY5;->NONE:LX/CY5;

    goto :goto_0

    :cond_1
    sget-object v0, LX/CY5;->EMPTY:LX/CY5;

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2460671
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->c:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2460672
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2460673
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08167d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460674
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2460668
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2460669
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->requestFocus()Z

    .line 2460670
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2460665
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 2460666
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2460667
    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2460664
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2460663
    return-object p0
.end method

.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2460657
    check-cast p2, Ljava/lang/Boolean;

    .line 2460658
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 2460659
    :goto_0
    if-eqz v1, :cond_0

    const/16 v0, 0x8

    .line 2460660
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2460661
    return-void

    :cond_1
    move v1, v0

    .line 2460662
    goto :goto_0
.end method
