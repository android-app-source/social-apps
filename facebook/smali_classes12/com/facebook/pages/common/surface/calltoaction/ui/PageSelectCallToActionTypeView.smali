.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field private j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private k:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2460945
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2460946
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;->d()V

    .line 2460947
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2460948
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460949
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;->d()V

    .line 2460950
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2460951
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2460952
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;->d()V

    .line 2460953
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 2460954
    const v0, 0x7f030ea5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2460955
    const v0, 0x7f0d23dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2460956
    const v0, 0x7f0d23db

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2460957
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;
    .locals 1

    .prologue
    .line 2460958
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460959
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;
    .locals 2

    .prologue
    .line 2460960
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2460961
    return-object p0
.end method
