.class public Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->PAGE_CALL_TO_ACTION_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CY3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HNV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z

.field private j:Z

.field public k:Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

.field public l:LX/CYE;

.field public m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

.field private n:Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

.field private o:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2459037
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(JLjava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/ArrayList;Ljava/lang/String;Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;LX/CYE;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;
    .locals 4
    .param p2    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/CYE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;",
            "LX/CYE;",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;",
            ")",
            "Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;"
        }
    .end annotation

    .prologue
    .line 2459023
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;-><init>()V

    .line 2459024
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2459025
    const-string v2, "arg_page_id"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2459026
    if-eqz p2, :cond_0

    .line 2459027
    const-string v2, "arg_can_edit_cta"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2459028
    :cond_0
    if-eqz p3, :cond_1

    .line 2459029
    const-string v2, "arg_force_creation_flow"

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2459030
    :cond_1
    const-string v2, "arg_page_call_to_action_label"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459031
    const-string v2, "arg_page_call_to_action_fields"

    invoke-static {v1, v2, p4}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2459032
    const-string v2, "arg_optional_admin_flow_control_param"

    invoke-static {v1, v2, p6}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459033
    const-string v2, "arg_config_action_data"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2459034
    const-string v2, "arg_cta_config"

    invoke-static {v1, v2, p8}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459035
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2459036
    return-object v0
.end method

.method public static a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/base/fragment/FbFragment;)V
    .locals 3

    .prologue
    .line 2459017
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2459018
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2459019
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2459020
    iget v1, p0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v1, v1

    .line 2459021
    const-string v2, "page_call_to_action_tag"

    invoke-virtual {v0, v1, p1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2459022
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Z)V
    .locals 2

    .prologue
    .line 2459013
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 2459014
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->n:Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;->setVisibility(I)V

    .line 2459015
    return-void

    .line 2459016
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)V
    .locals 6

    .prologue
    .line 2459007
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a:LX/1Ck;

    const-string v1, "fetch_page_admin_info"

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->d:LX/HNV;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/HNV;->a(Ljava/lang/String;)LX/HNU;

    move-result-object v2

    .line 2459008
    new-instance v3, LX/8EL;

    invoke-direct {v3}, LX/8EL;-><init>()V

    move-object v3, v3

    .line 2459009
    const-string v4, "page_id"

    iget-object v5, v2, LX/HNU;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/8EL;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2459010
    iget-object v4, v2, LX/HNU;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-static {v3}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v2, v3

    .line 2459011
    new-instance v3, LX/HNl;

    invoke-direct {v3, p0, p1}, LX/HNl;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2459012
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2458966
    invoke-static {p0, v1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Z)V

    .line 2458967
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    .line 2458968
    iget-boolean v2, v0, LX/CYE;->mUseActionFlow:Z

    move v0, v2

    .line 2458969
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2458970
    :goto_0
    if-nez v0, :cond_0

    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->k()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$CtaAdminInfoModel;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->k()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$CtaAdminInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$CtaAdminInfoModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$CtaAdminInfoModel$EligibleCallToActionsModel;

    move-result-object v2

    if-eqz v2, :cond_b

    :cond_0
    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 2458971
    if-nez v0, :cond_2

    .line 2458972
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2458973
    :goto_2
    return-void

    :cond_1
    move v0, v1

    .line 2458974
    goto :goto_0

    .line 2458975
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    .line 2458976
    iget-boolean v1, v0, LX/CYE;->mUseActionFlow:Z

    move v0, v1

    .line 2458977
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    .line 2458978
    iget-boolean v1, v0, LX/CYE;->mIncompleteAction:Z

    move v0, v1

    .line 2458979
    if-nez v0, :cond_9

    .line 2458980
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->k:Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->k:Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    iget-boolean v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->a:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2458981
    if-eqz v0, :cond_4

    .line 2458982
    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;)V

    goto :goto_2

    .line 2458983
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->k:Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->k:Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->b:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->k:Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->b:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 2458984
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->m(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->k:Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->b:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-static {p1, v0}, LX/CYR;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 2458985
    :cond_5
    const/4 v0, 0x1

    .line 2458986
    :goto_4
    move v0, v0

    .line 2458987
    if-eqz v0, :cond_7

    .line 2458988
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->m(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2458989
    :goto_5
    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    move-object v2, p1

    move-object v3, p2

    move-object v4, v0

    invoke-static/range {v2 .. v7}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Ljava/lang/String;Ljava/lang/String;LX/CYE;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2458990
    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->k:Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->b:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-static {p1, v0}, LX/CYR;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v0

    goto :goto_5

    .line 2458991
    :cond_7
    invoke-static {p1}, LX/CYR;->b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2458992
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    .line 2458993
    iget-boolean v1, v0, LX/CYE;->mUseActionFlow:Z

    move v0, v1

    .line 2458994
    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    if-nez v0, :cond_9

    .line 2458995
    :cond_8
    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;)V

    goto/16 :goto_2

    .line 2458996
    :cond_9
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->m(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    :goto_6
    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v2 .. v7}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Ljava/lang/String;Ljava/lang/String;LX/CYE;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2458997
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->c:LX/CY3;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    const-string v4, "header"

    .line 2458998
    iget-object v5, v2, LX/CY3;->a:LX/0Zb;

    sget-object v6, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_EDIT:LX/CY4;

    invoke-static {v6, v3}, LX/CY3;->a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "edit_location"

    invoke-virtual {v6, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2458999
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->e:LX/0if;

    sget-object v3, LX/0ig;->ak:LX/0ih;

    invoke-virtual {v2, v3}, LX/0if;->a(LX/0ih;)V

    .line 2459000
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->e:LX/0if;

    sget-object v3, LX/0ig;->ak:LX/0ih;

    const-string v4, "tap_edit_button"

    invoke-virtual {v2, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2459001
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2459002
    if-eqz v2, :cond_a

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2459003
    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->e:LX/0if;

    sget-object v4, LX/0ig;->bd:LX/0ih;

    invoke-virtual {v3, v4}, LX/0if;->a(LX/0ih;)V

    .line 2459004
    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->e:LX/0if;

    sget-object v4, LX/0ig;->bd:LX/0ih;

    const-string v5, "edit"

    invoke-virtual {v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2459005
    :cond_a
    goto/16 :goto_2

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 2459006
    :cond_e
    invoke-static {p1}, LX/CYR;->d(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v4

    goto :goto_6
.end method

.method public static a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2458962
    invoke-static {p0, v1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Z)V

    .line 2458963
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2458964
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-class v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2458965
    return-void
.end method

.method public static b(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)V
    .locals 8

    .prologue
    .line 2458934
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->i:Z

    if-eqz v0, :cond_1

    .line 2458935
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    .line 2458936
    iget-boolean v1, v0, LX/CYE;->mUseActionFlow:Z

    move v0, v1

    .line 2458937
    if-eqz v0, :cond_0

    .line 2458938
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    .line 2458939
    iget-object v1, v0, LX/CYE;->mActionId:Ljava/lang/String;

    move-object v0, v1

    .line 2458940
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2458941
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)V

    .line 2458942
    :goto_0
    return-void

    .line 2458943
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a:LX/1Ck;

    const-string v1, "fetch_call_to_action_types"

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->d:LX/HNV;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/HNV;->a(Ljava/lang/String;)LX/HNU;

    move-result-object v2

    invoke-virtual {v2}, LX/HNU;->c()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/HNm;

    invoke-direct {v3, p0}, LX/HNm;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2458944
    goto :goto_0

    .line 2458945
    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Z)V

    .line 2458946
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->h:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->f:Ljava/lang/String;

    .line 2458947
    new-instance v3, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    invoke-direct {v3}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;-><init>()V

    .line 2458948
    new-instance v4, Landroid/os/Bundle;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/os/Bundle;-><init>(I)V

    .line 2458949
    const-string v5, "arg_page_call_to_action_fields"

    invoke-static {v4, v5, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2458950
    const-string v5, "arg_page_id"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458951
    const-string v5, "arg_page_call_to_action_label"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2458952
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2458953
    move-object v0, v3

    .line 2458954
    invoke-static {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2458955
    goto :goto_0

    .line 2458956
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a:LX/1Ck;

    const-string v1, "fetch_call_to_action_by_id"

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->d:LX/HNV;

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/HNV;->a(Ljava/lang/String;)LX/HNU;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    .line 2458957
    iget-object v4, v3, LX/CYE;->mActionId:Ljava/lang/String;

    move-object v3, v4

    .line 2458958
    new-instance v4, LX/8EN;

    invoke-direct {v4}, LX/8EN;-><init>()V

    move-object v4, v4

    .line 2458959
    const-string v5, "cta_id"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "cta_icon_size"

    iget-object v6, v2, LX/HNU;->c:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0e2c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "cta_icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/8EN;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 2458960
    iget-object v5, v2, LX/HNU;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v2, v4

    .line 2458961
    new-instance v3, LX/HNk;

    invoke-direct {v3, p0}, LX/HNk;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0
.end method

.method private b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;)V
    .locals 3

    .prologue
    .line 2458921
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->m(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2458922
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    invoke-static {v0, v1, v2, p2, p1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->a(JLX/CYE;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2458923
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->c:LX/CY3;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    .line 2458924
    iget-object v2, v0, LX/CY3;->a:LX/0Zb;

    sget-object p1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_CREATE:LX/CY4;

    invoke-static {p1, v1}, LX/CY3;->a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2458925
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->e:LX/0if;

    sget-object v1, LX/0ig;->aj:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2458926
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->e:LX/0if;

    sget-object v1, LX/0ig;->aj:LX/0ih;

    const-string v2, "tap_create_button"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2458927
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2458928
    if-eqz v0, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2458929
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->e:LX/0if;

    sget-object v2, LX/0ig;->bd:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 2458930
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->e:LX/0if;

    sget-object v2, LX/0ig;->bd:LX/0ih;

    const-string p1, "create"

    invoke-virtual {v1, v2, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2458931
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->e:LX/0if;

    sget-object v2, LX/0ig;->bd:LX/0ih;

    const-string p1, "choose_request_time"

    invoke-virtual {v1, v2, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2458932
    :cond_0
    return-void

    .line 2458933
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    invoke-static {p1, p2, v0, v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Ljava/lang/String;ZLX/CYE;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/base/fragment/FbFragment;)V

    goto :goto_0
.end method

.method public static m(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)Z
    .locals 1

    .prologue
    .line 2458918
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    .line 2458919
    iget-boolean p0, v0, LX/CYE;->mUseActionFlow:Z

    move v0, p0

    .line 2458920
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2458878
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2458879
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v4, p0

    check-cast v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    const/16 v6, 0x259

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/CY3;->b(LX/0QB;)LX/CY3;

    move-result-object v7

    check-cast v7, LX/CY3;

    const-class p1, LX/HNV;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/HNV;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    iput-object v5, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a:LX/1Ck;

    iput-object v6, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->b:LX/0Ot;

    iput-object v7, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->c:LX/CY3;

    iput-object p1, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->d:LX/HNV;

    iput-object v0, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->e:LX/0if;

    .line 2458880
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v0

    .line 2458881
    const-string v0, "arg_page_id"

    const-wide/16 v4, -0x1

    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    .line 2458882
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    const-string v4, "arg_can_edit_cta"

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2458883
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->j:Z

    .line 2458884
    const-string v0, "arg_can_edit_cta"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->i:Z

    .line 2458885
    :goto_0
    const-string v0, "arg_page_call_to_action_fields"

    invoke-static {v3, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->h:Ljava/util/ArrayList;

    .line 2458886
    const-string v0, "arg_page_call_to_action_label"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->f:Ljava/lang/String;

    .line 2458887
    const-string v0, "arg_optional_admin_flow_control_param"

    invoke-static {v3, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->k:Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    .line 2458888
    const-string v0, "arg_force_creation_flow"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2458889
    invoke-static {}, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->a()Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->k:Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    .line 2458890
    :cond_0
    const-string v0, "arg_config_action_data"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CYE;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    .line 2458891
    const-string v0, "arg_cta_config"

    invoke-static {v3, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->m:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2458892
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    if-nez v0, :cond_1

    .line 2458893
    new-instance v0, LX/CYE;

    move-object v3, v2

    move-object v4, v2

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, LX/CYE;-><init>(ZLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->l:LX/CYE;

    .line 2458894
    :cond_1
    return-void

    .line 2458895
    :cond_2
    iput-boolean v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->j:Z

    .line 2458896
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a:LX/1Ck;

    const-string v4, "fetch_viewer_profile_permissions"

    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->d:LX/HNV;

    iget-object v6, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->g:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/HNV;->a(Ljava/lang/String;)LX/HNU;

    move-result-object v5

    .line 2458897
    new-instance v6, LX/8EM;

    invoke-direct {v6}, LX/8EM;-><init>()V

    move-object v6, v6

    .line 2458898
    const-string v7, "page_id"

    iget-object p1, v5, LX/HNU;->b:Ljava/lang/String;

    invoke-virtual {v6, v7, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/8EM;

    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    .line 2458899
    iget-object v7, v5, LX/HNU;->a:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    invoke-static {v6}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v5, v6

    .line 2458900
    new-instance v6, LX/HNj;

    invoke-direct {v6, p0}, LX/HNj;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)V

    invoke-virtual {v0, v4, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2458901
    goto/16 :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x28d7cdc6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2458917
    const v1, 0x7f030e24

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x482f363

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x21ec69a5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2458914
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2458915
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2458916
    const/16 v1, 0x2b

    const v2, -0x628aa1b3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xd25bac4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2458909
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2458910
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2458911
    if-eqz v1, :cond_0

    .line 2458912
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2458913
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x39e35b6e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2458902
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2458903
    const v0, 0x7f0d2295

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->n:Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    .line 2458904
    const v0, 0x7f0d2296

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2458905
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Z)V

    .line 2458906
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->j:Z

    if-eqz v0, :cond_0

    .line 2458907
    invoke-static {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->b(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;)V

    .line 2458908
    :cond_0
    return-void
.end method
