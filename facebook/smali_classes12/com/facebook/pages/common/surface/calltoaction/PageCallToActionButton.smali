.class public Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/CYI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/fbui/glyph/GlyphView;

.field private e:LX/CY8;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2458517
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2458518
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->a()V

    .line 2458519
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2458514
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2458515
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->a()V

    .line 2458516
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2458511
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2458512
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->a()V

    .line 2458513
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2458503
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2458504
    const v0, 0x7f030e20

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2458505
    const v0, 0x7f0d228e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2458506
    const v0, 0x7f0d228f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2458507
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->a:LX/CYI;

    .line 2458508
    new-instance v1, LX/CYG;

    invoke-direct {v1, v0}, LX/CYG;-><init>(LX/CYI;)V

    move-object v0, v1

    .line 2458509
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 2458510
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;LX/CYI;LX/0iA;)V
    .locals 0

    .prologue
    .line 2458502
    iput-object p1, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->a:LX/CYI;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->b:LX/0iA;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;

    invoke-static {v1}, LX/CYI;->b(LX/0QB;)LX/CYI;

    move-result-object v0

    check-cast v0, LX/CYI;

    invoke-static {v1}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->a(Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;LX/CYI;LX/0iA;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2458493
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->e:LX/CY8;

    iget-object v0, v0, LX/CY8;->f:LX/CY9;

    sget-object v1, LX/CY9;->NOT_CLICKABLE:LX/CY9;

    if-ne v0, v1, :cond_0

    .line 2458494
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2458495
    :goto_0
    return-void

    .line 2458496
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->c()V

    .line 2458497
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->d()V

    .line 2458498
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->e()V

    .line 2458499
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->f()V

    .line 2458500
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->i()V

    .line 2458501
    new-instance v0, LX/HNS;

    invoke-direct {v0, p0}, LX/HNS;-><init>(Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;)V

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2458520
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2458521
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->e:LX/CY8;

    iget-object v1, v1, LX/CY8;->g:LX/CYA;

    sget-object v2, LX/CYA;->WIDE:LX/CYA;

    if-ne v1, v2, :cond_0

    .line 2458522
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0e2a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2458523
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2458524
    return-void

    .line 2458525
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0e2b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2458489
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->e:LX/CY8;

    iget-object v0, v0, LX/CY8;->g:LX/CYA;

    sget-object v1, LX/CYA;->WIDE:LX/CYA;

    if-ne v0, v1, :cond_0

    .line 2458490
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021382

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2458491
    :goto_0
    return-void

    .line 2458492
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021385

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 2458485
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->e:LX/CY8;

    iget-object v0, v0, LX/CY8;->g:LX/CYA;

    sget-object v1, LX/CYA;->NARROW:LX/CYA;

    if-ne v0, v1, :cond_0

    .line 2458486
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004e

    invoke-static {v1, v2}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(F)V

    .line 2458487
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->e:LX/CY8;

    iget-object v0, v0, LX/CY8;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2458488
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2458478
    sget-object v0, LX/HNT;->a:[I

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->e:LX/CY8;

    iget-object v1, v1, LX/CY8;->b:LX/CYB;

    invoke-virtual {v1}, LX/CYB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2458479
    :goto_0
    return-void

    .line 2458480
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2458481
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->g()V

    goto :goto_0

    .line 2458482
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2458483
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->h()V

    goto :goto_0

    .line 2458484
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->d:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private g()V
    .locals 3

    .prologue
    .line 2458476
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020954

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2458477
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 2458474
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020df6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2458475
    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    .line 2458457
    const-string v0, ""

    .line 2458458
    sget-object v1, LX/HNT;->b:[I

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->e:LX/CY8;

    iget-object v2, v2, LX/CY8;->e:LX/CYC;

    invoke-virtual {v2}, LX/CYC;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    move-object v1, v0

    .line 2458459
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->b:LX/0iA;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_CALL_TO_ACTION_BUTTON:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/3kk;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kk;

    .line 2458460
    if-eqz v0, :cond_0

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2458461
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p0, v2, v3, v1}, LX/3kk;->a(Landroid/view/View;Landroid/view/View;Landroid/content/Context;Ljava/lang/String;)V

    .line 2458462
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->b:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-virtual {v0}, LX/3kk;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2458463
    :cond_0
    :pswitch_0
    return-void

    .line 2458464
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081696

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2458465
    goto :goto_0

    .line 2458466
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081697

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2458467
    goto :goto_0

    .line 2458468
    :pswitch_3
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081698

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->e:LX/CY8;

    iget-object v4, v4, LX/CY8;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->e:LX/CY8;

    iget-object v4, v4, LX/CY8;->d:LX/0am;

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2458469
    goto :goto_0

    .line 2458470
    :pswitch_4
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081694

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2458471
    goto :goto_0

    .line 2458472
    :pswitch_5
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081695

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2458473
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/CY7;)V
    .locals 2

    .prologue
    .line 2458451
    const-string v0, "PageCallToActionButton.bindModel"

    const v1, -0x7070a17b

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2458452
    :try_start_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->a:LX/CYI;

    invoke-virtual {v0, p1}, LX/CYI;->a(LX/CY7;)LX/CY8;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->e:LX/CY8;

    .line 2458453
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2458454
    const v0, 0x7b686b80

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2458455
    return-void

    .line 2458456
    :catchall_0
    move-exception v0

    const v1, -0x626ca081

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public setLoggingUuid(Landroid/os/ParcelUuid;)V
    .locals 1

    .prologue
    .line 2458448
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->a:LX/CYI;

    .line 2458449
    iput-object p1, v0, LX/CYI;->i:Landroid/os/ParcelUuid;

    .line 2458450
    return-void
.end method
