.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/HOQ;


# instance fields
.field private a:Lcom/facebook/widget/text/BetterEditTextView;

.field private b:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private c:I

.field private d:LX/HOU;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2460941
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2460942
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->f()V

    .line 2460943
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2460938
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460939
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->f()V

    .line 2460940
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2460935
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2460936
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->f()V

    .line 2460937
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2460930
    const v0, 0x7f030e36

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2460931
    const v0, 0x7f0d22a6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2460932
    const v0, 0x7f0d22a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->b:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2460933
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->c:I

    .line 2460934
    return-void
.end method

.method private setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2460928
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460929
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/HOU;)V
    .locals 1

    .prologue
    .line 2460924
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2460925
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->setText(Ljava/lang/CharSequence;)V

    .line 2460926
    :cond_0
    iput-object p2, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->d:LX/HOU;

    .line 2460927
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2460944
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/CY5;
    .locals 1

    .prologue
    .line 2460921
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2460922
    sget-object v0, LX/CY5;->EMPTY:LX/CY5;

    .line 2460923
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CYR;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/CY5;->NONE:LX/CY5;

    goto :goto_0

    :cond_1
    sget-object v0, LX/CY5;->INVALID:LX/CY5;

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2460911
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->c:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2460912
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->b:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2460913
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->b:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const v1, 0x7f0d2293

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2460914
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2460915
    const v1, 0x7f081680

    .line 2460916
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2460917
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460918
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->d:LX/HOU;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v1, v2, v3

    invoke-interface {v0, v2}, LX/HOU;->a([Ljava/lang/String;)V

    .line 2460919
    return-void

    .line 2460920
    :cond_0
    const v1, 0x7f081684

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2460910
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2460907
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 2460908
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->b:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 2460909
    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2460905
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CYR;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2460906
    return-object p0
.end method
