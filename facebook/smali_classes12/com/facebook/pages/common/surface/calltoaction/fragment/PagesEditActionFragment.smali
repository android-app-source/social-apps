.class public Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/H9E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HNW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:LX/HOE;

.field public h:J

.field public i:LX/CYE;

.field public j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

.field public k:LX/CYF;

.field public l:Lcom/facebook/fig/listitem/FigListItem;

.field public m:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field private n:LX/H9D;

.field public o:LX/H8Z;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2459848
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2459849
    new-instance v0, LX/HOE;

    invoke-direct {v0, p0}, LX/HOE;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->g:LX/HOE;

    .line 2459850
    return-void
.end method

.method public static a(JLX/CYE;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;LX/CYF;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;
    .locals 4

    .prologue
    .line 2459840
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;-><init>()V

    .line 2459841
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2459842
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2459843
    const-string v2, "extra_config_action_data"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2459844
    const-string v2, "extra_action_channel_edit_action"

    invoke-static {v1, v2, p3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459845
    const-string v2, "extra_action_channel_mode"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2459846
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2459847
    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2459821
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2459822
    if-eqz v0, :cond_1

    .line 2459823
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->o:LX/H8Z;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->o:LX/H8Z;

    invoke-interface {v1}, LX/H8Z;->a()LX/HA7;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2459824
    :cond_0
    const v1, 0x7f081669

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 2459825
    :goto_0
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 2459826
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2459827
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->k:LX/CYF;

    sget-object v2, LX/CYF;->CREATE_ACTION:LX/CYF;

    if-ne v1, v2, :cond_1

    .line 2459828
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->i:LX/CYE;

    .line 2459829
    iget-object v3, v1, LX/CYE;->mActionType:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v1, v3

    .line 2459830
    if-eqz v1, :cond_4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f081667

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2459831
    :goto_1
    iput-object v1, v2, LX/108;->g:Ljava/lang/String;

    .line 2459832
    move-object v1, v2

    .line 2459833
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2459834
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->g:LX/HOE;

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2459835
    :cond_1
    return-void

    .line 2459836
    :cond_2
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->k:LX/CYF;

    sget-object v2, LX/CYF;->EDIT_ACTION:LX/CYF;

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08166c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->o:LX/H8Z;

    invoke-interface {v2}, LX/H8Z;->a()LX/HA7;

    move-result-object v2

    .line 2459837
    iget v3, v2, LX/HA7;->b:I

    move v2, v3

    .line 2459838
    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08166b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2459839
    :cond_4
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f081666

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static e(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;)V
    .locals 8

    .prologue
    .line 2459816
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "page_action_channel_add_to_ordering_mutation"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2459817
    :goto_0
    return-void

    .line 2459818
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2459819
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_0

    .line 2459820
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v7, "page_action_channel_add_to_ordering_mutation"

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HNW;

    iget-wide v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->h:J

    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->i:LX/CYE;

    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, LX/HNW;->a(JLX/CYE;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/HOC;

    invoke-direct {v2, p0}, LX/HOC;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;)V

    invoke-virtual {v0, v7, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public static k(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;)V
    .locals 4

    .prologue
    .line 2459810
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2459811
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2459812
    const-string v2, "extra_updated_actions"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2459813
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2459814
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2459815
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459802
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2459803
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    invoke-static {p1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    const-class v4, LX/H9E;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/H9E;

    const/16 v5, 0x12b1

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2bf7

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x12c4

    invoke-static {p1, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v0, 0x2b68

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->a:LX/0wM;

    iput-object v4, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->b:LX/H9E;

    iput-object v5, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->c:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->d:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->e:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->f:LX/0Ot;

    .line 2459804
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2459805
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->h:J

    .line 2459806
    const-string v0, "extra_config_action_data"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CYE;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->i:LX/CYE;

    .line 2459807
    const-string v0, "extra_action_channel_edit_action"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    .line 2459808
    const-string v0, "extra_action_channel_mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CYF;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->k:LX/CYF;

    .line 2459809
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x4dbb777b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459801
    const v1, 0x7f030ecd

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x2cf5601f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3ed7b555

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459777
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2459778
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->d()V

    .line 2459779
    const/16 v1, 0x2b

    const v2, 0x138fffed

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459780
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2459781
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->b:LX/H9E;

    invoke-virtual {v0, p1}, LX/H9E;->a(Landroid/view/View;)LX/H9D;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->n:LX/H9D;

    .line 2459782
    const v0, 0x7f0d2423

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->l:Lcom/facebook/fig/listitem/FigListItem;

    .line 2459783
    const v0, 0x7f0d2424

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->m:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2459784
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->n:LX/H9D;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0, v1}, LX/H9D;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/H8Y;

    move-result-object v0

    check-cast v0, LX/H8Z;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->o:LX/H8Z;

    .line 2459785
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->l:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->a:LX/0wM;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->o:LX/H8Z;

    invoke-interface {v2}, LX/H8Z;->a()LX/HA7;

    move-result-object v2

    .line 2459786
    iget p1, v2, LX/HA7;->d:I

    move v2, p1

    .line 2459787
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0a00a6

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2459788
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->l:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->o:LX/H8Z;

    invoke-interface {v1}, LX/H8Z;->a()LX/HA7;

    move-result-object v1

    .line 2459789
    iget v2, v1, LX/HA7;->b:I

    move v1, v2

    .line 2459790
    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(I)V

    .line 2459791
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gA_()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2459792
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->l:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gA_()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2459793
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->k:LX/CYF;

    sget-object v1, LX/CYF;->EDIT_ACTION:LX/CYF;

    if-ne v0, v1, :cond_0

    .line 2459794
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030ece

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->m:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const/4 p1, 0x0

    invoke-virtual {v0, v1, v2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2459795
    const v1, 0x7f081671

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2459796
    new-instance v1, LX/HOB;

    invoke-direct {v1, p0}, LX/HOB;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459797
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->m:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 2459798
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->d()V

    .line 2459799
    return-void

    .line 2459800
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->l:Lcom/facebook/fig/listitem/FigListItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
