.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/HOb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CYR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/HOQ;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/2EJ;

.field private e:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2460407
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460408
    new-instance v0, LX/026;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/026;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->c:Ljava/util/Map;

    .line 2460409
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->b()V

    .line 2460410
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;LX/HOb;LX/CYR;)V
    .locals 0

    .prologue
    .line 2460406
    iput-object p1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a:LX/HOb;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->b:LX/CYR;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    const-class v0, LX/HOb;

    invoke-interface {v1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/HOb;

    invoke-static {v1}, LX/CYR;->a(LX/0QB;)LX/CYR;

    move-result-object v1

    check-cast v1, LX/CYR;

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;LX/HOb;LX/CYR;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2460403
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2460404
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->e:Landroid/view/LayoutInflater;

    .line 2460405
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2460398
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0e31

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2460399
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->b:LX/CYR;

    invoke-virtual {v1, p1, p0}, LX/CYR;->a(Ljava/lang/CharSequence;Landroid/view/ViewGroup;)Lcom/facebook/resources/ui/FbTextView;

    move-result-object v1

    .line 2460400
    invoke-virtual {v1, v2, v0, v2, v2}, Lcom/facebook/resources/ui/FbTextView;->setPadding(IIII)V

    .line 2460401
    invoke-virtual {p0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->addView(Landroid/view/View;)V

    .line 2460402
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2460395
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->removeAllViews()V

    .line 2460396
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2460397
    return-void
.end method


# virtual methods
.method public final a()LX/CY5;
    .locals 5

    .prologue
    .line 2460411
    sget-object v0, LX/CY5;->NONE:LX/CY5;

    .line 2460412
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2460413
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HOQ;

    .line 2460414
    invoke-interface {v0}, LX/HOQ;->b()LX/CY5;

    move-result-object v2

    .line 2460415
    sget-object v4, LX/CY5;->NONE:LX/CY5;

    if-ne v2, v4, :cond_0

    .line 2460416
    invoke-interface {v0}, LX/HOQ;->e()V

    goto :goto_0

    .line 2460417
    :cond_0
    invoke-interface {v0}, LX/HOQ;->c()V

    move-object v1, v2

    .line 2460418
    goto :goto_0

    .line 2460419
    :cond_1
    sget-object v0, LX/CY5;->NONE:LX/CY5;

    if-eq v1, v0, :cond_2

    .line 2460420
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->b:LX/CYR;

    invoke-virtual {v0, p0}, LX/CYR;->a(Landroid/view/View;)V

    .line 2460421
    :cond_2
    return-object v1
.end method

.method public final a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2460376
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->c()V

    .line 2460377
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f030ea3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    .line 2460378
    const v1, 0x7f0d23d4

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2460379
    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460380
    const v1, 0x7f0d23d5

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2460381
    const/4 v4, 0x0

    .line 2460382
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel$FieldsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2460383
    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel$FieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel$FieldsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2460384
    :goto_0
    move-object v2, v2

    .line 2460385
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460386
    new-instance v2, LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v2, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 2460387
    const v1, 0x7f0d23d6

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2460388
    const v3, 0x7f020895

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0568

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2460389
    const v1, 0x7f0d23d7

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2460390
    invoke-static {}, LX/10A;->a()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0569

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2460391
    const v1, 0x7f0d23d8

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2460392
    const v3, 0x7f0207ab

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a056a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2460393
    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->addView(Landroid/view/View;)V

    .line 2460394
    return-void

    :cond_0
    const-string v2, ""

    goto :goto_0
.end method

.method public final a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .param p1    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2460337
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->c()V

    .line 2460338
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2460339
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->b:LX/CYR;

    invoke-virtual {v0, p2, p0}, LX/CYR;->a(Ljava/lang/CharSequence;Landroid/view/ViewGroup;)Lcom/facebook/resources/ui/FbTextView;

    move-result-object v0

    .line 2460340
    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->addView(Landroid/view/View;)V

    .line 2460341
    :cond_0
    if-nez p3, :cond_2

    .line 2460342
    :cond_1
    return-void

    .line 2460343
    :cond_2
    const/4 v0, 0x0

    .line 2460344
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;

    .line 2460345
    if-eqz v0, :cond_3

    .line 2460346
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 2460347
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2460348
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;->GROUP:Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    if-ne v4, v5, :cond_7

    .line 2460349
    invoke-direct {p0, v3}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->b(Ljava/lang/String;)V

    .line 2460350
    :cond_4
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->m()Ljava/lang/String;

    move-result-object v3

    .line 2460351
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2460352
    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->b:LX/CYR;

    .line 2460353
    const v5, 0x7f0b0060

    invoke-static {v4, v3, p0, v5}, LX/CYR;->a(LX/CYR;Ljava/lang/CharSequence;Landroid/view/ViewGroup;I)Lcom/facebook/resources/ui/FbTextView;

    move-result-object v5

    .line 2460354
    iget-object p2, v4, LX/CYR;->a:Landroid/content/Context;

    const p3, 0x7f0e06c3

    invoke-virtual {v5, p2, p3}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2460355
    move-object v3, v5

    .line 2460356
    invoke-virtual {p0, v3}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->addView(Landroid/view/View;)V

    .line 2460357
    :cond_5
    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a:LX/HOb;

    .line 2460358
    new-instance v5, LX/HOa;

    const-class v4, Landroid/content/Context;

    invoke-interface {v3, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 p2, 0x2bfd

    invoke-static {v3, p2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p2

    const/16 p3, 0x2bf8

    invoke-static {v3, p3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p3

    invoke-direct {v5, v4, p2, p3, p4}, LX/HOa;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;Ljava/lang/String;)V

    .line 2460359
    move-object v3, v5

    .line 2460360
    invoke-virtual {v3, p1, v0, p0}, LX/HOa;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;LX/8ET;Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;)LX/HOQ;

    move-result-object v3

    .line 2460361
    if-eqz v3, :cond_3

    .line 2460362
    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->c:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2460363
    invoke-interface {v3}, LX/HOQ;->getView()Landroid/view/View;

    move-result-object v0

    .line 2460364
    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->addView(Landroid/view/View;)V

    .line 2460365
    invoke-interface {v3}, LX/HOQ;->a()Z

    move-result v4

    if-eqz v4, :cond_6

    if-nez v1, :cond_6

    .line 2460366
    const/4 v1, 0x1

    .line 2460367
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 2460368
    invoke-interface {v3}, LX/HOQ;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2460369
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->b:LX/CYR;

    invoke-virtual {v0}, LX/CYR;->a()V

    :cond_6
    move v0, v1

    move v1, v0

    .line 2460370
    goto/16 :goto_0

    .line 2460371
    :cond_7
    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->b:LX/CYR;

    .line 2460372
    const v5, 0x7f0b007c

    invoke-static {v4, v3, p0, v5}, LX/CYR;->a(LX/CYR;Ljava/lang/CharSequence;Landroid/view/ViewGroup;I)Lcom/facebook/resources/ui/FbTextView;

    move-result-object v5

    .line 2460373
    iget-object p2, v4, LX/CYR;->a:Landroid/content/Context;

    const p3, 0x7f0e06c2

    invoke-virtual {v5, p2, p3}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2460374
    move-object v3, v5

    .line 2460375
    invoke-virtual {p0, v3}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->addView(Landroid/view/View;)V

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2460335
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08003c

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->c(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2460336
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 2460325
    if-nez p1, :cond_1

    .line 2460326
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->d:LX/2EJ;

    if-eqz v0, :cond_0

    .line 2460327
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->d:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 2460328
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->b:LX/CYR;

    invoke-virtual {v0, p0}, LX/CYR;->a(Landroid/view/View;)V

    .line 2460329
    :goto_0
    return-void

    .line 2460330
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->d:LX/2EJ;

    if-nez v0, :cond_2

    .line 2460331
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2460332
    const v1, 0x7f030ea2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    .line 2460333
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e06ca

    invoke-direct {v1, v2, v3}, LX/0ju;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->d:LX/2EJ;

    .line 2460334
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->d:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 2460323
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08003c

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->c(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2460324
    return-void
.end method

.method public getFieldValues()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2460314
    new-instance v3, LX/026;

    const/4 v0, 0x2

    invoke-direct {v3, v0}, LX/026;-><init>(I)V

    .line 2460315
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2460316
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HOQ;

    .line 2460317
    instance-of v2, v1, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;

    if-eqz v2, :cond_1

    .line 2460318
    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;

    .line 2460319
    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->getChildInputValues()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0

    .line 2460320
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HOQ;

    invoke-interface {v2}, LX/HOQ;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2460321
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1}, LX/HOQ;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2460322
    :cond_2
    return-object v3
.end method
