.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/CY3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CYR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Lcom/facebook/widget/CustomLinearLayout;

.field private i:Landroid/support/v7/internal/widget/TintCheckBox;

.field private j:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2460526
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2460527
    new-instance v0, LX/026;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, LX/026;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->d:Ljava/util/Map;

    .line 2460528
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->b()V

    .line 2460529
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2460530
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460531
    new-instance v0, LX/026;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, LX/026;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->d:Ljava/util/Map;

    .line 2460532
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->b()V

    .line 2460533
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2460535
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2460536
    new-instance v0, LX/026;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, LX/026;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->d:Ljava/util/Map;

    .line 2460537
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->b()V

    .line 2460538
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;LX/CY3;LX/CYR;LX/0if;)V
    .locals 0

    .prologue
    .line 2460534
    iput-object p1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->a:LX/CY3;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->b:LX/CYR;

    iput-object p3, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->c:LX/0if;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    invoke-static {v2}, LX/CY3;->b(LX/0QB;)LX/CY3;

    move-result-object v0

    check-cast v0, LX/CY3;

    invoke-static {v2}, LX/CYR;->a(LX/0QB;)LX/CYR;

    move-result-object v1

    check-cast v1, LX/CYR;

    invoke-static {v2}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v2

    check-cast v2, LX/0if;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->a(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;LX/CY3;LX/CYR;LX/0if;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2460518
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2460519
    const v0, 0x7f030e29

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2460520
    const v0, 0x7f0d2299

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->j:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    .line 2460521
    const v0, 0x7f0d229a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/TintCheckBox;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->i:Landroid/support/v7/internal/widget/TintCheckBox;

    .line 2460522
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->i:Landroid/support/v7/internal/widget/TintCheckBox;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081673

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/TintCheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 2460523
    const v0, 0x7f0d229b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->h:Lcom/facebook/widget/CustomLinearLayout;

    .line 2460524
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->i:Landroid/support/v7/internal/widget/TintCheckBox;

    new-instance v1, LX/HOc;

    invoke-direct {v1, p0}, LX/HOc;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/TintCheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2460525
    return-void
.end method


# virtual methods
.method public final a()LX/CY5;
    .locals 5

    .prologue
    .line 2460504
    sget-object v0, LX/CY5;->NONE:LX/CY5;

    .line 2460505
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->j:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->getEditText()Ljava/lang/String;

    move-result-object v1

    .line 2460506
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2460507
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->c:LX/0if;

    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->g:Z

    if-eqz v0, :cond_3

    sget-object v0, LX/0ig;->ak:LX/0ih;

    :goto_0
    const-string v3, "error_message_shown"

    const-string v4, "empty_link"

    invoke-virtual {v2, v0, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2460508
    sget-object v0, LX/CY5;->EMPTY:LX/CY5;

    .line 2460509
    :cond_0
    sget-object v2, LX/CY5;->NONE:LX/CY5;

    if-ne v0, v2, :cond_1

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->j:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    if-eqz v2, :cond_1

    invoke-static {v1}, LX/CYR;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2460510
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->b:LX/CYR;

    invoke-virtual {v0, p0}, LX/CYR;->a(Landroid/view/View;)V

    .line 2460511
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->c:LX/0if;

    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->g:Z

    if-eqz v0, :cond_4

    sget-object v0, LX/0ig;->ak:LX/0ih;

    :goto_1
    const-string v2, "error_message_shown"

    const-string v3, "invalid_link"

    invoke-virtual {v1, v0, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2460512
    sget-object v0, LX/CY5;->INVALID:LX/CY5;

    .line 2460513
    :cond_1
    sget-object v1, LX/CY5;->NONE:LX/CY5;

    if-eq v0, v1, :cond_2

    .line 2460514
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->j:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081681

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->setError(Ljava/lang/String;)V

    .line 2460515
    :cond_2
    return-object v0

    .line 2460516
    :cond_3
    sget-object v0, LX/0ig;->aj:LX/0ih;

    goto :goto_0

    .line 2460517
    :cond_4
    sget-object v0, LX/0ig;->aj:LX/0ih;

    goto :goto_1
.end method

.method public final a(ZLcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2460469
    iput-boolean p1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->g:Z

    .line 2460470
    const/4 v1, 0x0

    .line 2460471
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_0
    move-object v0, v1

    .line 2460472
    :goto_0
    move-object v5, v0

    .line 2460473
    if-nez v5, :cond_2

    .line 2460474
    :cond_1
    :goto_1
    return-void

    .line 2460475
    :cond_2
    iput-object p3, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->f:Ljava/lang/String;

    .line 2460476
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->h:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->removeAllViews()V

    .line 2460477
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v4

    move v1, v4

    :goto_2
    if-ge v3, v6, :cond_4

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;

    .line 2460478
    if-eqz v0, :cond_3

    .line 2460479
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->c()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v7

    .line 2460480
    sget-object v8, LX/HOd;->a:[I

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;->ordinal()I

    move-result v7

    aget v7, v8, v7

    packed-switch v7, :pswitch_data_0

    :cond_3
    move v0, v1

    .line 2460481
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_2

    .line 2460482
    :pswitch_0
    iget-object v7, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->j:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->a(Ljava/lang/String;)Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->b(Ljava/lang/String;)Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->c(Ljava/lang/String;)Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    .line 2460483
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->e:Ljava/lang/String;

    .line 2460484
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->d:Ljava/util/Map;

    iget-object v7, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->e:Ljava/lang/String;

    iget-object v8, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->j:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    invoke-interface {v0, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 2460485
    goto :goto_3

    .line 2460486
    :pswitch_1
    new-instance v7, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->a(Ljava/lang/String;)Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->b(Ljava/lang/String;)Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->c(Ljava/lang/String;)Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    move-result-object v7

    .line 2460487
    iget-object v8, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->h:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v8, v7}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    .line 2460488
    iget-object v8, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->d:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->b()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2460489
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 2460490
    goto :goto_3

    .line 2460491
    :cond_4
    if-eqz v1, :cond_5

    .line 2460492
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->i:Landroid/support/v7/internal/widget/TintCheckBox;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/TintCheckBox;->setChecked(Z)V

    .line 2460493
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->h:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2460494
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->j:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->requestFocus()Z

    .line 2460495
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->j:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->getEditText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2460496
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->b:LX/CYR;

    invoke-virtual {v0}, LX/CYR;->a()V

    goto/16 :goto_1

    .line 2460497
    :cond_6
    invoke-virtual {p2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel;->a()LX/0Px;

    move-result-object v5

    .line 2460498
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v3, v0

    :goto_4
    if-ge v3, v6, :cond_8

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;

    .line 2460499
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel$FieldsModel;

    move-result-object v7

    if-eqz v7, :cond_7

    .line 2460500
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->WEBSITE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;

    invoke-virtual {v7, v8}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionActionType;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2460501
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel$FieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel$ConfigFieldsModel$NodesModel$FieldsModel;->a()LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 2460502
    :cond_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_8
    move-object v0, v1

    .line 2460503
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getFieldValues()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2460457
    new-instance v2, LX/026;

    invoke-direct {v2}, LX/026;-><init>()V

    .line 2460458
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->i:Landroid/support/v7/internal/widget/TintCheckBox;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/TintCheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2460459
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2460460
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->j:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->getEditText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CYR;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move-object v0, v2

    .line 2460461
    :goto_0
    return-object v0

    .line 2460462
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2460463
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutGenericView;->getEditText()Ljava/lang/String;

    move-result-object v1

    .line 2460464
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2460465
    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->e:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2460466
    invoke-static {v1}, LX/CYR;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2460467
    :cond_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    move-object v0, v2

    .line 2460468
    goto :goto_0
.end method
