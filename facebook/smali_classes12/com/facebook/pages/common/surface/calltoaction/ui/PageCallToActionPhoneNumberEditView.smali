.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/HOQ;


# instance fields
.field public a:LX/3Lz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/widget/text/BetterEditTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:I

.field private final h:Landroid/view/View$OnClickListener;

.field public final i:LX/6u6;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2460758
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2460759
    new-instance v0, LX/HOj;

    invoke-direct {v0, p0}, LX/HOj;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->h:Landroid/view/View$OnClickListener;

    .line 2460760
    new-instance v0, LX/HOk;

    invoke-direct {v0, p0}, LX/HOk;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->i:LX/6u6;

    .line 2460761
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->f()V

    .line 2460762
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2460763
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460764
    new-instance v0, LX/HOj;

    invoke-direct {v0, p0}, LX/HOj;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->h:Landroid/view/View$OnClickListener;

    .line 2460765
    new-instance v0, LX/HOk;

    invoke-direct {v0, p0}, LX/HOk;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->i:LX/6u6;

    .line 2460766
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->f()V

    .line 2460767
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2460768
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2460769
    new-instance v0, LX/HOj;

    invoke-direct {v0, p0}, LX/HOj;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->h:Landroid/view/View$OnClickListener;

    .line 2460770
    new-instance v0, LX/HOk;

    invoke-direct {v0, p0}, LX/HOk;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->i:LX/6u6;

    .line 2460771
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->f()V

    .line 2460772
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;LX/3Lz;LX/7Tk;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;",
            "LX/3Lz;",
            "LX/7Tk;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2460773
    iput-object p1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->a:LX/3Lz;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->b:LX/7Tk;

    iput-object p3, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->c:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;

    invoke-static {v2}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v0

    check-cast v0, LX/3Lz;

    const-class v1, LX/7Tk;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/7Tk;

    const/16 v3, 0x15ec

    invoke-static {v2, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->a(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;LX/3Lz;LX/7Tk;LX/0Or;)V

    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 2460774
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2460775
    const v0, 0x7f030e2d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2460776
    const v0, 0x7f0d22a1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2460777
    const v0, 0x7f0d22a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2460778
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->g:I

    .line 2460779
    const v0, 0x7f0d22a2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;

    .line 2460780
    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->a:Lcom/facebook/widget/text/BetterEditTextView;

    move-object v0, v1

    .line 2460781
    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2460782
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v1, Landroid/telephony/PhoneNumberFormattingTextWatcher;

    invoke-direct {v1}, Landroid/telephony/PhoneNumberFormattingTextWatcher;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2460783
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setInputType(I)V

    .line 2460784
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getFilters()[Landroid/text/InputFilter;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2460785
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    const/16 v2, 0xf

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2460786
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Landroid/text/InputFilter;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/InputFilter;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterEditTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 2460787
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2460788
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->setDefaultCountryCode(Ljava/lang/String;)V

    .line 2460789
    return-void
.end method

.method private setCountryCode(I)V
    .locals 2

    .prologue
    .line 2460790
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "+"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->setDialingCode(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;Ljava/lang/String;)V

    .line 2460791
    return-void
.end method

.method private setDefaultCountryCode(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2460754
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->a:LX/3Lz;

    invoke-virtual {v0, p1}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->setCountryCode(I)V

    .line 2460755
    return-void
.end method

.method public static setDialingCode(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2460756
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460757
    return-void
.end method

.method private setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2460752
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2460753
    return-void
.end method

.method private setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2460721
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460722
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 2460739
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->setHint(Ljava/lang/CharSequence;)V

    .line 2460740
    if-eqz p2, :cond_2

    .line 2460741
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2460742
    invoke-direct {p0, p3}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->setText(Ljava/lang/CharSequence;)V

    .line 2460743
    :cond_0
    const/4 v0, -0x1

    if-eq p4, v0, :cond_1

    .line 2460744
    invoke-direct {p0, p4}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->setCountryCode(I)V

    .line 2460745
    :cond_1
    :goto_0
    return-void

    .line 2460746
    :cond_2
    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2460747
    :try_start_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->a:LX/3Lz;

    const/4 v1, 0x0

    invoke-virtual {v0, p5, v1}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v0

    .line 2460748
    iget v1, v0, LX/4hT;->countryCode_:I

    move v1, v1

    .line 2460749
    invoke-direct {p0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->setCountryCode(I)V

    .line 2460750
    iget-wide v2, v0, LX/4hT;->nationalNumber_:J

    move-wide v0, v2

    .line 2460751
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2460738
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/CY5;
    .locals 1

    .prologue
    .line 2460737
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/CY5;->NONE:LX/CY5;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/CY5;->EMPTY:LX/CY5;

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2460734
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->g:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2460735
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2460736
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2460731
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->g:I

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2460732
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->requestFocus()Z

    .line 2460733
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2460728
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 2460729
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->f:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2460730
    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2460724
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2460725
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2460726
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2460727
    :cond_0
    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2460723
    return-object p0
.end method
