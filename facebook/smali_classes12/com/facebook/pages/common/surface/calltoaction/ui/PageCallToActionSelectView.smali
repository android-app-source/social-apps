.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/HOQ;


# instance fields
.field private a:Landroid/widget/RadioGroup;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLInterfaces$CallToActionConfigCommonFields$Options;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2460831
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2460832
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->f()V

    .line 2460833
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2460796
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460797
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->f()V

    .line 2460798
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2460828
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2460829
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->f()V

    .line 2460830
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 2460825
    const v0, 0x7f030e30

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2460826
    const v0, 0x7f0d22a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->a:Landroid/widget/RadioGroup;

    .line 2460827
    return-void
.end method

.method private setCheck(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2460819
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2460820
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigCommonFieldsModel$OptionsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigCommonFieldsModel$OptionsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2460821
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 2460822
    :goto_1
    return-void

    .line 2460823
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2460824
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0Px;Ljava/lang/String;LX/HOU;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLInterfaces$CallToActionConfigCommonFields$Options;",
            ">;",
            "Ljava/lang/String;",
            "LX/HOU;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2460806
    iput-object p1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->b:LX/0Px;

    .line 2460807
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->removeAllViews()V

    .line 2460808
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    move v2, v3

    .line 2460809
    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 2460810
    const v0, 0x7f030e2f

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v4, v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 2460811
    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setId(I)V

    .line 2460812
    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigCommonFieldsModel$OptionsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigCommonFieldsModel$OptionsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 2460813
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 2460814
    const v0, 0x7f03088e

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->a:Landroid/widget/RadioGroup;

    const/4 v5, 0x1

    invoke-virtual {v4, v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2460815
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2460816
    :cond_0
    invoke-direct {p0, p2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->setCheck(Ljava/lang/String;)V

    .line 2460817
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->a:Landroid/widget/RadioGroup;

    new-instance v1, LX/HOl;

    invoke-direct {v1, p0, p3}, LX/HOl;-><init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;LX/HOU;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2460818
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2460805
    const/4 v0, 0x0

    return v0
.end method

.method public final b()LX/CY5;
    .locals 1

    .prologue
    .line 2460804
    sget-object v0, LX/CY5;->NONE:LX/CY5;

    return-object v0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2460803
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2460802
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 2460801
    return-void
.end method

.method public getValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2460799
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    .line 2460800
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->b:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigCommonFieldsModel$OptionsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigCommonFieldsModel$OptionsModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2460795
    return-object p0
.end method
