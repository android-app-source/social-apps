.class public Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/0kL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/HNd;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/CYT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/CYR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private h:LX/2EJ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2458717
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 4

    .prologue
    .line 2458718
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->g:LX/0if;

    sget-object v1, LX/0ig;->bd:LX/0ih;

    const-string v2, "tap_back"

    const-string v3, "on_enable_message_nux"

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2458719
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2458720
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2458721
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v3

    check-cast v3, LX/0kL;

    const-class v4, LX/HNd;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/HNd;

    const/16 v5, 0xc49

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/CYT;->a(LX/0QB;)LX/CYT;

    move-result-object v7

    check-cast v7, LX/CYT;

    invoke-static {v0}, LX/CYR;->a(LX/0QB;)LX/CYR;

    move-result-object p1

    check-cast p1, LX/CYR;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    iput-object v3, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->a:LX/0kL;

    iput-object v4, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->b:LX/HNd;

    iput-object v5, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->c:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object v7, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->e:LX/CYT;

    iput-object p1, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->f:LX/CYR;

    iput-object v0, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->g:LX/0if;

    .line 2458722
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 2458723
    if-nez p1, :cond_1

    .line 2458724
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->h:LX/2EJ;

    if-eqz v0, :cond_0

    .line 2458725
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->h:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 2458726
    :cond_0
    :goto_0
    return-void

    .line 2458727
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->h:LX/2EJ;

    if-nez v0, :cond_2

    .line 2458728
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2458729
    const v1, 0x7f030ea2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/framerateprogressbar/FrameRateProgressBar;

    .line 2458730
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e06ca

    invoke-direct {v1, v2, v3}, LX/0ju;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->h:LX/2EJ;

    .line 2458731
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->h:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x2

    const/16 v0, 0x2a

    const v1, -0x3b481014

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2458732
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2458733
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2458734
    const v0, 0x7f030e18

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2458735
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2458736
    const-string v1, "arg_setup_info"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;

    .line 2458737
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2458738
    const-string v2, "arg_page_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2458739
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2458740
    const-string v2, "arg_admin_config"

    invoke-static {v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2458741
    const v2, 0x7f0d227b

    invoke-static {v4, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2458742
    if-eqz v0, :cond_1

    .line 2458743
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 2458744
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel$MessagingSetupImageModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const-class v7, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2458745
    :cond_0
    const v2, 0x7f0d227c

    invoke-static {v4, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2458746
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->n()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2458747
    const v2, 0x7f0d227d

    invoke-static {v4, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2458748
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2458749
    :cond_1
    const v2, 0x7f0d227e

    invoke-static {v4, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fig/button/FigButton;

    .line 2458750
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0816a3

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2458751
    new-instance v6, LX/HNe;

    invoke-direct {v6, p0}, LX/HNe;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;)V

    invoke-virtual {v2, v6}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2458752
    const v2, 0x7f0d227f

    invoke-static {v4, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fig/button/FigButton;

    .line 2458753
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0816a4

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2458754
    new-instance v6, LX/HNg;

    invoke-direct {v6, p0, v5, v1, v0}, LX/HNg;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;Ljava/lang/String;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$RequestTimeCTASetupInfoQueryModel;)V

    invoke-virtual {v2, v6}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2458755
    const/16 v0, 0x2b

    const v1, 0x4a925218    # 4794636.0f

    invoke-static {v8, v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v4
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x28501e99

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2458756
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2458757
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageAdminEnableMessagingFragment;->e:LX/CYT;

    .line 2458758
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2458759
    const-string v3, "arg_page_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/CYT;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2458760
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2458761
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x51a92265

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5d6418e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2458762
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2458763
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2458764
    if-eqz v1, :cond_0

    .line 2458765
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0816a2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2458766
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 2458767
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2458768
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x23ef57a3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
