.class public Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/HOQ;


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field private b:Landroid/widget/TextView;

.field private final c:LX/026;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "LX/HOQ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/026;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/026",
            "<",
            "Ljava/lang/String;",
            "LX/HOQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2460177
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2460178
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->c:LX/026;

    .line 2460179
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->d:LX/026;

    .line 2460180
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->f()V

    .line 2460181
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2460172
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2460173
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->c:LX/026;

    .line 2460174
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->d:LX/026;

    .line 2460175
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->f()V

    .line 2460176
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2460167
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2460168
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->c:LX/026;

    .line 2460169
    new-instance v0, LX/026;

    invoke-direct {v0}, LX/026;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->d:LX/026;

    .line 2460170
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->f()V

    .line 2460171
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 2460163
    const v0, 0x7f030e25

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2460164
    const v0, 0x7f0d2297

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->a:Landroid/widget/LinearLayout;

    .line 2460165
    const v0, 0x7f0d2293

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->b:Landroid/widget/TextView;

    .line 2460166
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/HOQ;Z)V
    .locals 2
    .param p2    # LX/HOQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2460157
    if-eqz p2, :cond_0

    .line 2460158
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->a:Landroid/widget/LinearLayout;

    invoke-interface {p2}, LX/HOQ;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2460159
    if-eqz p3, :cond_1

    .line 2460160
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->d:LX/026;

    invoke-virtual {v0, p1, p2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2460161
    :cond_0
    :goto_0
    return-void

    .line 2460162
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->c:LX/026;

    invoke-virtual {v0, p1, p2}, LX/01J;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2460156
    const/4 v0, 0x0

    return v0
.end method

.method public final b()LX/CY5;
    .locals 4

    .prologue
    .line 2460182
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->c:LX/026;

    invoke-virtual {v0}, LX/026;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2460183
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HOQ;

    invoke-interface {v1}, LX/HOQ;->b()LX/CY5;

    move-result-object v2

    .line 2460184
    sget-object v1, LX/CY5;->NONE:LX/CY5;

    if-ne v2, v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HOQ;

    invoke-interface {v1}, LX/HOQ;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2460185
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HOQ;

    invoke-interface {v0}, LX/HOQ;->d()V

    .line 2460186
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08167e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v2

    .line 2460187
    :goto_1
    return-object v0

    .line 2460188
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HOQ;

    invoke-interface {v0}, LX/HOQ;->e()V

    goto :goto_0

    .line 2460189
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->d:LX/026;

    invoke-virtual {v0}, LX/01J;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2460190
    const/4 v0, 0x0

    .line 2460191
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->d:LX/026;

    invoke-virtual {v1}, LX/026;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2460192
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HOQ;

    invoke-interface {v1}, LX/HOQ;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2460193
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HOQ;

    invoke-interface {v1}, LX/HOQ;->b()LX/CY5;

    move-result-object v1

    .line 2460194
    sget-object v2, LX/CY5;->NONE:LX/CY5;

    if-ne v1, v2, :cond_3

    .line 2460195
    const/4 v1, 0x1

    .line 2460196
    :goto_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HOQ;

    invoke-interface {v0}, LX/HOQ;->e()V

    move v2, v1

    .line 2460197
    goto :goto_2

    .line 2460198
    :cond_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HOQ;

    invoke-interface {v0}, LX/HOQ;->d()V

    .line 2460199
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081685

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 2460200
    goto :goto_1

    .line 2460201
    :cond_4
    if-nez v2, :cond_6

    .line 2460202
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->d:LX/026;

    invoke-virtual {v0}, LX/026;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2460203
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HOQ;

    invoke-interface {v0}, LX/HOQ;->d()V

    goto :goto_4

    .line 2460204
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08167f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460205
    sget-object v0, LX/CY5;->EMPTY:LX/CY5;

    goto/16 :goto_1

    .line 2460206
    :cond_6
    sget-object v0, LX/CY5;->NONE:LX/CY5;

    goto/16 :goto_1

    :cond_7
    move v1, v2

    goto :goto_3
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2460154
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2460155
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2460153
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2460151
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2460152
    return-void
.end method

.method public getChildInputValues()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2460144
    new-instance v2, LX/026;

    invoke-direct {v2}, LX/026;-><init>()V

    .line 2460145
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->c:LX/026;

    invoke-virtual {v0}, LX/026;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2460146
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HOQ;

    invoke-interface {v0}, LX/HOQ;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2460147
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->d:LX/026;

    invoke-virtual {v0}, LX/026;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2460148
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HOQ;

    invoke-interface {v1}, LX/HOQ;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2460149
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HOQ;

    invoke-interface {v0}, LX/HOQ;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2460150
    :cond_2
    return-object v2
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2460143
    const/4 v0, 0x0

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 2460142
    return-object p0
.end method
