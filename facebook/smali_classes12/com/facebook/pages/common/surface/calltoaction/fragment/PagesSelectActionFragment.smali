.class public Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/HNW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/H9E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/HOO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:J

.field public h:LX/CYE;

.field public i:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

.field public j:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

.field public k:Landroid/widget/ViewSwitcher;

.field private l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public m:LX/HON;

.field private n:LX/H9D;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2459925
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(JLX/CYE;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;
    .locals 4
    .param p3    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459917
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;-><init>()V

    .line 2459918
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2459919
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2459920
    const-string v2, "extra_config_action_data"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2459921
    const-string v2, "arg_page_admin_info"

    invoke-static {v1, v2, p3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459922
    const-string v2, "arg_page_admin_cta"

    invoke-static {v1, v2, p4}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459923
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2459924
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459926
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2459927
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/HNW;->a(LX/0QB;)LX/HNW;

    move-result-object v4

    check-cast v4, LX/HNW;

    const-class v5, LX/H9E;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/H9E;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    const-class p1, LX/HOO;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/HOO;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    iput-object v3, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->a:LX/1Ck;

    iput-object v4, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->b:LX/HNW;

    iput-object v5, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->c:LX/H9E;

    iput-object v6, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->d:LX/0Uh;

    iput-object p1, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->e:LX/HOO;

    iput-object v0, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->f:LX/0if;

    .line 2459928
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2459929
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->g:J

    .line 2459930
    const-string v0, "extra_config_action_data"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CYE;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->h:LX/CYE;

    .line 2459931
    const-string v0, "arg_page_admin_info"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->i:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    .line 2459932
    const-string v0, "arg_page_admin_cta"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->j:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    .line 2459933
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1766dd74

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459916
    const v1, 0x7f030edd

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x6f8c1958

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xbb3bcc2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459912
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2459913
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->a:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2459914
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->a:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2459915
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x413aa820

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x38d6c371

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459905
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2459906
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2459907
    if-eqz v1, :cond_0

    .line 2459908
    const v2, 0x7f08168c

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2459909
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 2459910
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2459911
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x55fc41eb    # 3.46700057E13f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 13
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459878
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2459879
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->c:LX/H9E;

    invoke-virtual {v0, p1}, LX/H9E;->a(Landroid/view/View;)LX/H9D;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->n:LX/H9D;

    .line 2459880
    const v0, 0x7f0d243f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->k:Landroid/widget/ViewSwitcher;

    .line 2459881
    const v0, 0x7f0d2440

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2459882
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->e:LX/HOO;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->n:LX/H9D;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2459883
    new-instance p2, LX/HON;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p1

    check-cast p1, LX/0wM;

    invoke-direct {p2, v3, p1, v1, v2}, LX/HON;-><init>(LX/23P;LX/0wM;LX/H9D;Landroid/content/Context;)V

    .line 2459884
    move-object v0, p2

    .line 2459885
    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->m:LX/HON;

    .line 2459886
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->m:LX/HON;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2459887
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2459888
    const/4 v3, 0x0

    .line 2459889
    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->k:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4, v3}, Landroid/widget/ViewSwitcher;->setDisplayedChild(I)V

    .line 2459890
    const-string v4, "PRIMARY_BUTTONS"

    iget-object v5, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->h:LX/CYE;

    .line 2459891
    iget-object v6, v5, LX/CYE;->mActionChannelType:Ljava/lang/String;

    move-object v5, v6

    .line 2459892
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2459893
    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->d:LX/0Uh;

    sget v5, LX/8Dm;->n:I

    invoke-virtual {v4, v5, v3}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v3, 0x1

    .line 2459894
    :cond_0
    iget-object v4, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->a:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fetch_addable_actions_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v7, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->g:J

    invoke-virtual {v5, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->b:LX/HNW;

    iget-wide v7, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->g:J

    .line 2459895
    new-instance v6, LX/9XR;

    invoke-direct {v6}, LX/9XR;-><init>()V

    move-object v6, v6

    .line 2459896
    const-string v9, "page_id"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v9, "cta_icon_size"

    iget-object v10, v3, LX/HNW;->a:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b0f8e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v9, "cta_icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/9XR;

    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    .line 2459897
    iget-object v9, v3, LX/HNW;->b:LX/0tX;

    invoke-virtual {v9, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    invoke-static {v6}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v3, v6

    .line 2459898
    :goto_0
    new-instance v6, LX/HOF;

    invoke-direct {v6, p0}, LX/HOF;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;)V

    invoke-virtual {v4, v5, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2459899
    return-void

    .line 2459900
    :cond_1
    iget-object v3, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->b:LX/HNW;

    iget-wide v7, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->g:J

    .line 2459901
    new-instance v6, LX/9XQ;

    invoke-direct {v6}, LX/9XQ;-><init>()V

    move-object v6, v6

    .line 2459902
    const-string v9, "page_id"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v9, "cta_icon_size"

    iget-object v10, v3, LX/HNW;->a:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b0f8e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v9, "cta_icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/9XQ;

    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    .line 2459903
    iget-object v9, v3, LX/HNW;->b:LX/0tX;

    invoke-virtual {v9, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    invoke-static {v6}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v3, v6

    .line 2459904
    goto :goto_0
.end method
