.class public Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field public static m:I


# instance fields
.field public a:LX/CYR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CY3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Gze;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

.field public f:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:LX/CYE;

.field public final j:Landroid/view/View$OnClickListener;

.field public k:Lcom/facebook/widget/FbScrollView;

.field public l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2459548
    const/4 v0, 0x0

    sput v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->m:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2459634
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2459635
    new-instance v0, LX/HO5;

    invoke-direct {v0, p0}, LX/HO5;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->j:Landroid/view/View$OnClickListener;

    .line 2459636
    return-void
.end method

.method public static a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Ljava/lang/String;ZLX/CYE;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;
    .locals 3

    .prologue
    .line 2459625
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;-><init>()V

    .line 2459626
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2459627
    const-string v2, "arg_page_admin_cta"

    invoke-static {v1, v2, p0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459628
    const-string v2, "arg_page_admin_info"

    invoke-static {v1, v2, p1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2459629
    const-string v2, "arg_page_id"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459630
    const-string v2, "arg_is_edit_mode"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2459631
    const-string v2, "arg_config_action_data"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2459632
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2459633
    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    invoke-static {p0}, LX/CYR;->a(LX/0QB;)LX/CYR;

    move-result-object v1

    check-cast v1, LX/CYR;

    invoke-static {p0}, LX/CY3;->b(LX/0QB;)LX/CY3;

    move-result-object v2

    check-cast v2, LX/CY3;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-static {p0}, LX/Gze;->b(LX/0QB;)LX/Gze;

    move-result-object p0

    check-cast p0, LX/Gze;

    iput-object v1, p1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->a:LX/CYR;

    iput-object v2, p1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->b:LX/CY3;

    iput-object v3, p1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->c:LX/0if;

    iput-object p0, p1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->d:LX/Gze;

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    .line 2459623
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->b:LX/CY3;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->g:Ljava/lang/String;

    const-string v2, "cta_select_list"

    invoke-virtual {v0, v1, v2}, LX/CY3;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2459624
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Landroid/view/ViewGroup;)Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2459616
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2459617
    const v1, 0x7f030ea6

    invoke-virtual {v0, v1, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;

    .line 2459618
    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->dQ_()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v4}, LX/15i;->g(II)I

    move-result v1

    .line 2459619
    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;->a(Ljava/lang/CharSequence;)Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;

    move-result-object v3

    invoke-virtual {v2, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;->a(Ljava/lang/String;)Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;

    .line 2459620
    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;->setTag(Ljava/lang/Object;)V

    .line 2459621
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageSelectCallToActionTypeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459622
    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459607
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2459608
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2459609
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2459610
    const-string v0, "arg_page_admin_cta"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    .line 2459611
    const-string v0, "arg_page_admin_info"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->f:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    .line 2459612
    const-string v0, "arg_page_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->g:Ljava/lang/String;

    .line 2459613
    const-string v0, "arg_is_edit_mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->h:Z

    .line 2459614
    const-string v0, "arg_config_action_data"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CYE;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->i:LX/CYE;

    .line 2459615
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x14f8d9fd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459606
    const v1, 0x7f030ea4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0xd837138

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4fa6aa6c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459603
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2459604
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->k:Lcom/facebook/widget/FbScrollView;

    invoke-virtual {v1}, Lcom/facebook/widget/FbScrollView;->getScrollY()I

    move-result v1

    sput v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->m:I

    .line 2459605
    const/16 v1, 0x2b

    const v2, -0x234bbbe2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x64c2fe10

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459600
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2459601
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->k:Lcom/facebook/widget/FbScrollView;

    new-instance v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment$1;

    invoke-direct {v2, p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment$1;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/FbScrollView;->post(Ljava/lang/Runnable;)Z

    .line 2459602
    const/16 v1, 0x2b

    const v2, 0x37c51faa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x39062ca1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459593
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2459594
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2459595
    if-eqz v1, :cond_0

    .line 2459596
    const v2, 0x7f08168c

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2459597
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 2459598
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2459599
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x78777654

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 11
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459549
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2459550
    check-cast p1, Lcom/facebook/widget/FbScrollView;

    iput-object p1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->k:Lcom/facebook/widget/FbScrollView;

    .line 2459551
    const v0, 0x7f0d23da

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2459552
    const v0, 0x7f0d23d9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2459553
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->i:LX/CYE;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->i:LX/CYE;

    .line 2459554
    iget-boolean p1, v1, LX/CYE;->mUseActionFlow:Z

    move v1, p1

    .line 2459555
    if-nez v1, :cond_8

    .line 2459556
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->k()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$CtaAdminInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$CtaAdminInfoModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2459557
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->e:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;->k()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$CtaAdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$CtaAdminInfoModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$CtaAdminInfoModel$EligibleCallToActionsModel;

    move-result-object v0

    .line 2459558
    if-eqz v0, :cond_8

    .line 2459559
    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->l:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->j:Landroid/view/View$OnClickListener;

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2459560
    const/4 v5, 0x0

    .line 2459561
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2459562
    new-instance v9, Ljava/util/LinkedHashMap;

    invoke-direct {v9}, Ljava/util/LinkedHashMap;-><init>()V

    .line 2459563
    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel$CtaAdminInfoModel$EligibleCallToActionsModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result p1

    move v6, v5

    :goto_0
    if-ge v6, p1, :cond_3

    invoke-virtual {v10, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2459564
    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->d()Ljava/lang/String;

    move-result-object p2

    .line 2459565
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2459566
    invoke-virtual {v9, p2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2459567
    invoke-virtual {v9, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2459568
    :goto_2
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_0

    :cond_1
    move v4, v5

    .line 2459569
    goto :goto_1

    .line 2459570
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2459571
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2459572
    invoke-virtual {v9, p2, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2459573
    :cond_3
    move-object v3, v9

    .line 2459574
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    .line 2459575
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    if-ne v4, v7, :cond_6

    move v6, v7

    .line 2459576
    :goto_3
    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 2459577
    if-nez v6, :cond_5

    .line 2459578
    const v4, 0x7f030e26

    invoke-virtual {v9, v4, v1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbTextView;

    .line 2459579
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2459580
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2459581
    :cond_5
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v7

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 2459582
    invoke-virtual {p0, v3, v1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageSelectCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Landroid/view/ViewGroup;)Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    move-result-object p1

    .line 2459583
    invoke-virtual {p1, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setTag(Ljava/lang/Object;)V

    .line 2459584
    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2459585
    if-eqz v4, :cond_7

    .line 2459586
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0034

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p2, 0x7f0b0034

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p1, v8, v3, v8, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(IIII)V

    move v3, v8

    .line 2459587
    :goto_5
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p2, 0x7f0a00e8

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setBorderColor(I)V

    .line 2459588
    invoke-virtual {v1, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move v4, v3

    .line 2459589
    goto :goto_4

    :cond_6
    move v6, v8

    .line 2459590
    goto :goto_3

    .line 2459591
    :cond_7
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p2, 0x7f0b0034

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {p1, v8, v8, v8, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(IIII)V

    move v3, v4

    goto :goto_5

    .line 2459592
    :cond_8
    return-void
.end method
