.class public Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CY3;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HNV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2459718
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2459719
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    const/16 v2, 0x12b1

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2bf8

    invoke-static {v1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const-class p0, LX/HNV;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/HNV;

    iput-object v2, p1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->a:LX/0Ot;

    iput-object v3, p1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->b:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->c:LX/HNV;

    return-void
.end method

.method public static b$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;)V
    .locals 1

    .prologue
    .line 2459714
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2459715
    if-eqz v0, :cond_0

    .line 2459716
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2459717
    :cond_0
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 2459708
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CY3;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->e:Ljava/lang/String;

    .line 2459709
    iget-object v2, v0, LX/CY3;->a:LX/0Zb;

    sget-object v4, LX/CY4;->EVENT_VIEWER_CALL_TO_ACTION_BACK_BUTTON:LX/CY4;

    invoke-static {v4, v1}, LX/CY3;->a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v2, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2459710
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08168e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x108009b

    invoke-virtual {v0, v1}, LX/0ju;->c(I)LX/0ju;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08168f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/HO7;

    invoke-direct {v2, p0}, LX/HO7;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    new-instance v2, LX/HO6;

    invoke-direct {v2, p0}, LX/HO6;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2459711
    invoke-virtual {v0}, LX/2EJ;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v2, 0x3f59999a    # 0.85f

    invoke-virtual {v1, v2}, Landroid/view/Window;->setDimAmount(F)V

    .line 2459712
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2459713
    return v3
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2459701
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2459702
    const-class v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2459703
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2459704
    const-string v0, "arg_page_call_to_action_fields"

    invoke-static {v1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->d:Ljava/util/ArrayList;

    .line 2459705
    const-string v0, "arg_page_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->e:Ljava/lang/String;

    .line 2459706
    const-string v0, "arg_page_call_to_action_label"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->f:Ljava/lang/String;

    .line 2459707
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x69ff906a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459700
    const v1, 0x7f030eb1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6c28a5b5

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6ca1876c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459689
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2459690
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2459691
    if-eqz v1, :cond_0

    .line 2459692
    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->f:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2459693
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2459694
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f081668

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2459695
    iput-object v4, v2, LX/108;->g:Ljava/lang/String;

    .line 2459696
    move-object v2, v2

    .line 2459697
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2459698
    new-instance v2, LX/HOA;

    invoke-direct {v2, p0}, LX/HOA;-><init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2459699
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x76b8a58d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 2459685
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2459686
    const v0, 0x7f0d23e7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->g:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    .line 2459687
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->g:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->d:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 2459688
    return-void
.end method
