.class public final Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2a1b120e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2462993
    const-class v0, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2462992
    const-class v0, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2462990
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2462991
    return-void
.end method

.method private a()Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2462975
    iget-object v0, p0, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;->e:Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;->e:Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;

    .line 2462976
    iget-object v0, p0, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;->e:Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2462994
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2462995
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;->a()Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2462996
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2462997
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2462998
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2462999
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2462982
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2462983
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;->a()Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2462984
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;->a()Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;

    .line 2462985
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;->a()Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2462986
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;

    .line 2462987
    iput-object v0, v1, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;->e:Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel$ServicesCardModel;

    .line 2462988
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2462989
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2462979
    new-instance v0, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/tabs/tabbar/graphql/PageServicesSectionMutationModels$PageServicesSectionMutationFragmentModel;-><init>()V

    .line 2462980
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2462981
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2462978
    const v0, 0x59a0179b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2462977
    const v0, 0x3c3d04f5

    return v0
.end method
