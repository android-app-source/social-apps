.class public final Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1ceb7eb9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2462721
    const-class v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2462720
    const-class v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2462691
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2462692
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2462713
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2462714
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2462715
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2462716
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2462717
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2462718
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2462719
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2462711
    iget-object v0, p0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->e:Ljava/util/List;

    .line 2462712
    iget-object v0, p0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2462703
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2462704
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2462705
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2462706
    if-eqz v1, :cond_0

    .line 2462707
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    .line 2462708
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->e:Ljava/util/List;

    .line 2462709
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2462710
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2462700
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2462701
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->f:Z

    .line 2462702
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2462697
    new-instance v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;-><init>()V

    .line 2462698
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2462699
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2462695
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2462696
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->f:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2462694
    const v0, -0x375635ad

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2462693
    const v0, -0x6073c482

    return v0
.end method
