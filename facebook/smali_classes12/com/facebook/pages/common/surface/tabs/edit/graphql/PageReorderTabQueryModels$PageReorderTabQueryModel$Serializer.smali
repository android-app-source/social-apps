.class public final Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2462799
    const-class v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabQueryModel;

    new-instance v1, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2462800
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2462801
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2462802
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2462803
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2462804
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2462805
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2462806
    if-eqz v2, :cond_0

    .line 2462807
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2462808
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2462809
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2462810
    if-eqz v2, :cond_1

    .line 2462811
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2462812
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2462813
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2462814
    if-eqz v2, :cond_2

    .line 2462815
    const-string p0, "profileTabNavigationEditChannel"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2462816
    invoke-static {v1, v2, p1, p2}, LX/HPz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2462817
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2462818
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2462819
    check-cast p1, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabQueryModel$Serializer;->a(Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
