.class public final Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/HCg;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x171cc8ac
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2462764
    const-class v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2462735
    const-class v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2462736
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2462737
    return-void
.end method

.method private j()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2462738
    iget-object v0, p0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;->e:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    iput-object v0, p0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;->e:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    .line 2462739
    iget-object v0, p0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;->e:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2462740
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2462741
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;->j()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2462742
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2462743
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2462744
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2462745
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2462746
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2462747
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;->j()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2462748
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;->j()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    .line 2462749
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;->j()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2462750
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;

    .line 2462751
    iput-object v0, v1, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;->e:Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    .line 2462752
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2462753
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2462754
    new-instance v0, LX/HPw;

    invoke-direct {v0, p1}, LX/HPw;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2462755
    invoke-direct {p0}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;->j()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2462756
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2462757
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2462758
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2462759
    new-instance v0, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel;-><init>()V

    .line 2462760
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2462761
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2462762
    const v0, -0x2264a321

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2462763
    const v0, 0x25d6af

    return v0
.end method
