.class public Lcom/facebook/pages/common/services/PagesServicesItemFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0rq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public j:Lcom/facebook/pages/common/services/PagesServiceItemLarge;

.field public k:Landroid/widget/ScrollView;

.field public l:Landroid/widget/ProgressBar;

.field public m:J

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Z

.field public q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2457947
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2457948
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->p:Z

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/services/PagesServicesItemFragment;I)V
    .locals 2

    .prologue
    .line 2457949
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->e:LX/0kL;

    new-instance v1, LX/27k;

    invoke-direct {v1, p1}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2457950
    return-void
.end method

.method public static b$redex0(Lcom/facebook/pages/common/services/PagesServicesItemFragment;)V
    .locals 3

    .prologue
    .line 2457951
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2457952
    if-eqz v0, :cond_0

    .line 2457953
    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->n:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081810    # 1.8089995E38f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2457954
    :cond_0
    return-void

    .line 2457955
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->n:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2457956
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2457957
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v6

    check-cast v6, LX/0hB;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v8

    check-cast v8, LX/17W;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p1

    check-cast p1, LX/0Uh;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v0

    check-cast v0, LX/0rq;

    iput-object v3, v2, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->a:LX/1Ck;

    iput-object v4, v2, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->b:LX/0tX;

    iput-object v5, v2, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->c:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->d:LX/0hB;

    iput-object v7, v2, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->e:LX/0kL;

    iput-object v8, v2, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->f:LX/17W;

    iput-object p1, v2, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->g:LX/0Uh;

    iput-object v0, v2, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->h:LX/0rq;

    .line 2457958
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2457959
    const-string v1, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->m:J

    .line 2457960
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2457961
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->n:Ljava/lang/String;

    .line 2457962
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2457963
    const-string v1, "page_service_id_extra"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->o:Ljava/lang/String;

    .line 2457964
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2457965
    const-string v1, "extra_service_launched_from_page"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->p:Z

    .line 2457966
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x896a3c8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2457967
    const v0, 0x7f030ee8

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2457968
    const v0, 0x7f0d2453

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->k:Landroid/widget/ScrollView;

    .line 2457969
    const v0, 0x7f0d2454

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->i:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2457970
    const v0, 0x7f0d2455

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->j:Lcom/facebook/pages/common/services/PagesServiceItemLarge;

    .line 2457971
    const v0, 0x7f0d04de

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->l:Landroid/widget/ProgressBar;

    .line 2457972
    const/16 v0, 0x2b

    const v3, 0x382574a9

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xcf51121

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2457973
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2457974
    invoke-static {p0}, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->b$redex0(Lcom/facebook/pages/common/services/PagesServicesItemFragment;)V

    .line 2457975
    const/16 v1, 0x2b

    const v2, 0x597cef1f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2457976
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2457977
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->a:LX/1Ck;

    const-string v1, "fetch_single_page_service"

    iget-object v2, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->b:LX/0tX;

    .line 2457978
    new-instance v3, LX/9aE;

    invoke-direct {v3}, LX/9aE;-><init>()V

    move-object v3, v3

    .line 2457979
    const-string v4, "service_id"

    iget-object v5, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->o:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2457980
    iget-object v4, p0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->d:LX/0hB;

    invoke-virtual {v4}, LX/0hB;->c()I

    move-result v4

    .line 2457981
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f0b0e91

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 2457982
    const-string p1, "page_service_image_width"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v3, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2457983
    const-string p1, "page_service_image_height"

    int-to-float v4, v4

    const/high16 p2, 0x3f800000    # 1.0f

    div-float/2addr v4, p2

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2457984
    const-string v4, "profile_pic_size"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2457985
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    move-object v3, v3

    .line 2457986
    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    new-instance v3, LX/HMy;

    invoke-direct {v3, p0}, LX/HMy;-><init>(Lcom/facebook/pages/common/services/PagesServicesItemFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2457987
    return-void
.end method
