.class public Lcom/facebook/pages/common/services/PagesServiceItemLarge;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2457494
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2457495
    invoke-direct {p0}, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->a()V

    .line 2457496
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2457497
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2457498
    invoke-direct {p0}, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->a()V

    .line 2457499
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2457500
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2457501
    invoke-direct {p0}, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->a()V

    .line 2457502
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2457503
    const v0, 0x7f030ee0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2457504
    const v0, 0x7f0d2447

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->a:Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;

    .line 2457505
    const v0, 0x7f0d2448

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2457506
    const v0, 0x7f0d2449

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2457507
    const v0, 0x7f0d244a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2457508
    const v0, 0x7f0d244b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->e:Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;

    .line 2457509
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;)V
    .locals 6
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v1, 0x8

    const/4 v4, 0x0

    .line 2457510
    if-eqz p1, :cond_0

    .line 2457511
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->a:Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->setImageURI(Landroid/net/Uri;)V

    .line 2457512
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->a:Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;

    invoke-virtual {v0, v4}, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->setVisibility(I)V

    .line 2457513
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2457514
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2457515
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2457516
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2457517
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2457518
    :goto_1
    if-eqz p7, :cond_2

    if-eqz p5, :cond_2

    .line 2457519
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->e:Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;

    new-instance v1, LX/CY6;

    invoke-direct {v1}, LX/CY6;-><init>()V

    invoke-virtual {p7}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2457520
    iput-wide v2, v1, LX/CY6;->a:J

    .line 2457521
    move-object v1, v1

    .line 2457522
    invoke-virtual {p7}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2457523
    iput-object v2, v1, LX/CY6;->b:Ljava/lang/String;

    .line 2457524
    move-object v1, v1

    .line 2457525
    invoke-virtual {p7}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->p()LX/0Px;

    move-result-object v2

    .line 2457526
    iput-object v2, v1, LX/CY6;->c:LX/0Px;

    .line 2457527
    move-object v1, v1

    .line 2457528
    invoke-virtual {p7}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    .line 2457529
    iput-object v2, v1, LX/CY6;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 2457530
    move-object v1, v1

    .line 2457531
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->PAGES_SERVICES_SURFACE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 2457532
    iput-object v2, v1, LX/CY6;->e:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 2457533
    move-object v1, v1

    .line 2457534
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    const-string v3, "selected_service"

    invoke-virtual {v2, v3, p6}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    .line 2457535
    iput-object v2, v1, LX/CY6;->g:LX/0P1;

    .line 2457536
    move-object v1, v1

    .line 2457537
    invoke-virtual {v1}, LX/CY6;->a()LX/CY7;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->a(LX/CY7;)V

    .line 2457538
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->e:Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;

    invoke-virtual {v0, v4}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->setVisibility(I)V

    .line 2457539
    :goto_2
    return-void

    .line 2457540
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->a:Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->setVisibility(I)V

    goto :goto_0

    .line 2457541
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1

    .line 2457542
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->e:Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/PageCallToActionButton;->setVisibility(I)V

    goto :goto_2
.end method
