.class public Lcom/facebook/pages/common/services/PagesServicesFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fj;
.implements LX/GZf;


# static fields
.field public static final I:[I


# instance fields
.field private A:LX/E8t;

.field public B:Landroid/view/View;

.field public C:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field public D:Z

.field public E:Ljava/lang/String;

.field public F:I

.field private G:I

.field private H:I

.field public J:Z

.field public K:I

.field public L:LX/6WJ;

.field public M:LX/4At;

.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/HMx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/HDT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HQ9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/HN8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/CYT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/HN7;

.field public q:LX/HN5;

.field public r:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public s:Lcom/facebook/widget/listview/BetterListView;

.field public t:LX/HN4;

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9Zu;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9Zu;",
            ">;"
        }
    .end annotation
.end field

.field public w:Z

.field public x:Z

.field private final y:LX/HMl;

.field private z:LX/E8s;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2457787
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->I:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2457788
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2457789
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->p:LX/HN7;

    .line 2457790
    iput-boolean v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->w:Z

    .line 2457791
    iput-boolean v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->x:Z

    .line 2457792
    new-instance v0, LX/HMm;

    invoke-direct {v0, p0}, LX/HMm;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->y:LX/HMl;

    .line 2457793
    iput-boolean v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->D:Z

    .line 2457794
    iput v2, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->H:I

    .line 2457795
    iput-boolean v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->J:Z

    .line 2457796
    iput v2, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->K:I

    return-void
.end method

.method public static a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/pages/common/services/PagesServicesFragment;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2457797
    new-instance v0, Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-direct {v0}, Lcom/facebook/pages/common/services/PagesServicesFragment;-><init>()V

    .line 2457798
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2457799
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2457800
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2457801
    const-string v2, "profile_name"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457802
    :cond_0
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2457803
    const-string v2, "page_clicked_item_id_extra"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457804
    :cond_1
    const-string v2, "extra_is_inside_page_surface_tab"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2457805
    invoke-static {p4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2457806
    const-string v2, "extra_page_tab_entry_point"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457807
    :cond_2
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2457808
    return-object v0
.end method

.method private static a(Lcom/facebook/pages/common/services/PagesServicesFragment;LX/1Ck;LX/0tX;LX/0Ot;LX/0Ot;LX/0Ot;LX/HMx;LX/0hB;LX/17W;LX/0Uh;LX/0if;LX/0Ot;LX/HDT;LX/0Ot;LX/HN8;LX/CYT;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/services/PagesServicesFragment;",
            "LX/1Ck;",
            "LX/0tX;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "Lcom/facebook/pages/common/services/PagesServicesFragment$PagesServicesFragmentController;",
            "LX/0hB;",
            "LX/17W;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/HDT;",
            "LX/0Ot",
            "<",
            "LX/HQ9;",
            ">;",
            "LX/HN8;",
            "LX/CYT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2457783
    iput-object p1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->a:LX/1Ck;

    iput-object p2, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->b:LX/0tX;

    iput-object p3, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->f:LX/HMx;

    iput-object p7, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->g:LX/0hB;

    iput-object p8, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->h:LX/17W;

    iput-object p9, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->i:LX/0Uh;

    iput-object p10, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->j:LX/0if;

    iput-object p11, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->k:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->l:LX/HDT;

    iput-object p13, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->m:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->n:LX/HN8;

    iput-object p15, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->o:LX/CYT;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-static {v15}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {v15}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    const/16 v3, 0x259

    invoke-static {v15, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x455

    invoke-static {v15, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xc49

    invoke-static {v15, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v15}, LX/HMx;->b(LX/0QB;)LX/HMx;

    move-result-object v6

    check-cast v6, LX/HMx;

    invoke-static {v15}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v7

    check-cast v7, LX/0hB;

    invoke-static {v15}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v8

    check-cast v8, LX/17W;

    invoke-static {v15}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {v15}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v10

    check-cast v10, LX/0if;

    const/16 v11, 0x12c4

    invoke-static {v15, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v15}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v12

    check-cast v12, LX/HDT;

    const/16 v13, 0x2c04

    invoke-static {v15, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const-class v14, LX/HN8;

    invoke-interface {v15, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/HN8;

    invoke-static {v15}, LX/CYT;->a(LX/0QB;)LX/CYT;

    move-result-object v15

    check-cast v15, LX/CYT;

    invoke-static/range {v0 .. v15}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a(Lcom/facebook/pages/common/services/PagesServicesFragment;LX/1Ck;LX/0tX;LX/0Ot;LX/0Ot;LX/0Ot;LX/HMx;LX/0hB;LX/17W;LX/0Uh;LX/0if;LX/0Ot;LX/HDT;LX/0Ot;LX/HN8;LX/CYT;)V

    return-void
.end method

.method private a(Z)V
    .locals 8

    .prologue
    .line 2457809
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->a:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetch_pages_services_list_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-wide v2, v2, LX/HN5;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->b:LX/0tX;

    .line 2457810
    new-instance v4, LX/9Zs;

    invoke-direct {v4}, LX/9Zs;-><init>()V

    move-object v4, v4

    .line 2457811
    const-string v5, "page_id"

    iget-object v6, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-wide v6, v6, LX/HN5;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2457812
    iget-object v5, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->g:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->c()I

    move-result v5

    .line 2457813
    const-string v6, "page_service_image_width"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2457814
    const-string v6, "page_service_image_height"

    int-to-float v5, v5

    const/high16 v7, 0x3f800000    # 1.0f

    div-float/2addr v5, v7

    float-to-int v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v6, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2457815
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    move-object v3, v4

    .line 2457816
    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    new-instance v3, LX/HMk;

    invoke-direct {v3, p0, p1}, LX/HMk;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;Z)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2457817
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;LX/15i;ILjava/util/List;Ljava/lang/String;)V
    .locals 6
    .param p0    # Lcom/facebook/pages/common/services/PagesServicesFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "I",
            "Ljava/util/List",
            "<",
            "LX/9Zu;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2457818
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->u:Ljava/util/List;

    .line 2457819
    if-eqz p2, :cond_2

    .line 2457820
    const v0, 0x69e33861

    invoke-static {p1, p2, v1, v0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 2457821
    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    invoke-virtual {v3, v4, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    invoke-virtual {v3, v4, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-class v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    invoke-virtual {v3, v4, v1, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2457822
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->u:Ljava/util/List;

    const-class v5, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServiceItemModel;

    invoke-virtual {v3, v4, v1, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2457823
    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 2457824
    :cond_2
    if-eqz p3, :cond_3

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2457825
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->v:Ljava/util/List;

    .line 2457826
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2457827
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0xa

    if-ge v1, v0, :cond_5

    .line 2457828
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9Zu;

    .line 2457829
    invoke-static {v0}, LX/HMg;->a(LX/9Zu;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 2457830
    iget-object v3, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->v:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2457831
    add-int/lit8 v0, v1, 0x1

    :goto_3
    move v1, v0

    .line 2457832
    goto :goto_2

    .line 2457833
    :cond_4
    iput-object p3, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->v:Ljava/util/List;

    .line 2457834
    :cond_5
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "published"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "staging"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_6
    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 2457835
    if-eqz v0, :cond_7

    .line 2457836
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->n:LX/HN8;

    invoke-virtual {v0, p4}, LX/HN8;->a(Ljava/lang/String;)LX/HN7;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->p:LX/HN7;

    .line 2457837
    :cond_7
    invoke-static {p0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->n(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    .line 2457838
    return-void

    :cond_8
    move v0, v1

    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public static a$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2457839
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    .line 2457840
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x0

    :cond_0
    iput-object p1, v0, LX/HN4;->h:Ljava/lang/String;

    .line 2457841
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    const v1, 0x23788dd5

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2457842
    invoke-direct {p0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->t()V

    .line 2457843
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2457844
    return-void
.end method

.method public static d(Lcom/facebook/pages/common/services/PagesServicesFragment;I)V
    .locals 4

    .prologue
    .line 2457845
    iput p1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->H:I

    .line 2457846
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->B:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2457847
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->B:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->H:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2457848
    :cond_0
    return-void
.end method

.method public static k$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2457849
    iget-boolean v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->x:Z

    if-eqz v0, :cond_0

    .line 2457850
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    .line 2457851
    iput-object v1, v0, LX/HN4;->f:LX/HMi;

    .line 2457852
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    new-instance v1, LX/HMv;

    invoke-direct {v1, p0}, LX/HMv;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    invoke-virtual {v0, v1}, LX/HN4;->a(Landroid/view/View$OnClickListener;)V

    .line 2457853
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    new-instance v1, LX/HMw;

    invoke-direct {v1, p0}, LX/HMw;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    .line 2457854
    iput-object v1, v0, LX/HN4;->e:LX/HMw;

    .line 2457855
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    new-instance v1, LX/HMh;

    invoke-direct {v1, p0}, LX/HMh;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    .line 2457856
    iput-object v1, v0, LX/HN4;->g:LX/HMh;

    .line 2457857
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    const v1, 0x5ea01b85

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2457858
    return-void

    .line 2457859
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    invoke-virtual {v0, v1}, LX/HN4;->a(Landroid/view/View$OnClickListener;)V

    .line 2457860
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    .line 2457861
    iput-object v1, v0, LX/HN4;->e:LX/HMw;

    .line 2457862
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    .line 2457863
    iput-object v1, v0, LX/HN4;->g:LX/HMh;

    .line 2457864
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    new-instance v1, LX/HMi;

    invoke-direct {v1, p0}, LX/HMi;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    .line 2457865
    iput-object v1, v0, LX/HN4;->f:LX/HMi;

    .line 2457866
    goto :goto_0
.end method

.method public static l(Lcom/facebook/pages/common/services/PagesServicesFragment;)V
    .locals 4

    .prologue
    .line 2457867
    iget-boolean v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->D:Z

    if-nez v0, :cond_0

    .line 2457868
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2457869
    if-eqz v0, :cond_0

    .line 2457870
    const v1, 0x7f081810    # 1.8089995E38f

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2457871
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2457872
    const-string v1, "admin_setup_cta"

    iget-object v2, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->E:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2457873
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081665

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2457874
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2457875
    move-object v1, v1

    .line 2457876
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2457877
    new-instance v1, LX/HMj;

    invoke-direct {v1, p0}, LX/HMj;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2457878
    :cond_0
    return-void
.end method

.method public static n(Lcom/facebook/pages/common/services/PagesServicesFragment;)V
    .locals 6

    .prologue
    .line 2457879
    invoke-direct {p0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->t()V

    .line 2457880
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2457881
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->u:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    iget-object v2, v2, LX/HN5;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->x:Z

    iget-object v4, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->p:LX/HN7;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->p:LX/HN7;

    .line 2457882
    iget-object v5, v4, LX/HN7;->e:Ljava/lang/String;

    move-object v4, v5

    .line 2457883
    :goto_0
    iget-object v5, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->E:Ljava/lang/String;

    .line 2457884
    iput-object v1, v0, LX/HN4;->i:Ljava/util/List;

    .line 2457885
    iput-object v2, v0, LX/HN4;->j:Ljava/lang/String;

    .line 2457886
    iput-boolean v3, v0, LX/HN4;->k:Z

    .line 2457887
    iput-object v4, v0, LX/HN4;->l:Ljava/lang/String;

    .line 2457888
    iput-object v5, v0, LX/HN4;->m:Ljava/lang/String;

    .line 2457889
    const p0, 0xb11f014

    invoke-static {v0, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2457890
    return-void

    .line 2457891
    :cond_0
    const-string v4, "staging"

    goto :goto_0
.end method

.method public static p$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2457892
    iget-boolean v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->x:Z

    if-eqz v0, :cond_0

    .line 2457893
    invoke-static {p0, v2}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;Ljava/lang/String;)V

    .line 2457894
    :goto_0
    const/4 v0, 0x0

    invoke-static {p0, v2, v0, v2, v2}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;LX/15i;ILjava/util/List;Ljava/lang/String;)V

    .line 2457895
    return-void

    .line 2457896
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081815    # 1.8090005E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private q()V
    .locals 3

    .prologue
    .line 2457676
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->z:LX/E8s;

    if-nez v0, :cond_2

    .line 2457677
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->A:LX/E8t;

    if-nez v0, :cond_1

    .line 2457678
    new-instance v0, LX/E8s;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->z:LX/E8s;

    .line 2457679
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->z:LX/E8s;

    const/4 v1, 0x1

    .line 2457680
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2457681
    :cond_0
    :goto_1
    return-void

    .line 2457682
    :cond_1
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->A:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->z:LX/E8s;

    .line 2457683
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->A:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->z:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    goto :goto_0

    .line 2457684
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->A:LX/E8t;

    if-eqz v0, :cond_0

    .line 2457685
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->z:LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->A:LX/E8t;

    .line 2457686
    iput-object v1, v0, LX/E8s;->a:Landroid/view/View;

    .line 2457687
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->A:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->z:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2457688
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->z:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->requestLayout()V

    goto :goto_1
.end method

.method private t()V
    .locals 2

    .prologue
    .line 2457784
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->r:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 2457785
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->r:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2457786
    return-void
.end method


# virtual methods
.method public final E_(I)V
    .locals 1

    .prologue
    .line 2457689
    iget v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->H:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->H:I

    if-le p1, v0, :cond_0

    .line 2457690
    :goto_0
    return-void

    .line 2457691
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/pages/common/services/PagesServicesFragment;->d(Lcom/facebook/pages/common/services/PagesServicesFragment;I)V

    goto :goto_0
.end method

.method public final S_()Z
    .locals 4

    .prologue
    .line 2457692
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->j:LX/0if;

    sget-object v1, LX/0ig;->bd:LX/0ih;

    const-string v2, "tap_back"

    const-string v3, "on_service_list"

    invoke-virtual {v0, v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2457693
    const/4 v0, 0x0

    return v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2457694
    const-string v0, "page_service_fragment"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2457695
    iput p1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->G:I

    .line 2457696
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    iget v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->G:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2457697
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2457698
    iput-object p1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->A:LX/E8t;

    .line 2457699
    invoke-direct {p0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->q()V

    .line 2457700
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2457701
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2457702
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2457703
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->f:LX/HMx;

    .line 2457704
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2457705
    const-wide/16 v7, -0x1

    .line 2457706
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2457707
    const-string v3, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v3, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    .line 2457708
    cmp-long v3, v5, v7

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 2457709
    const-string v3, "profile_name"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2457710
    const-string v3, "page_clicked_item_id_extra"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2457711
    iget-object v3, v0, LX/HMx;->a:LX/2U1;

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8Dk;

    .line 2457712
    if-eqz v3, :cond_2

    .line 2457713
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2457714
    iget-object v4, v3, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v3, v4

    .line 2457715
    invoke-virtual {v3}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v3

    .line 2457716
    :goto_1
    new-instance v4, LX/HN5;

    invoke-direct {v4, v5, v6, v3, v7}, LX/HN5;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    .line 2457717
    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    .line 2457718
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2457719
    const-string v1, "extra_is_inside_page_surface_tab"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->D:Z

    .line 2457720
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2457721
    const-string v1, "extra_page_tab_entry_point"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->E:Ljava/lang/String;

    .line 2457722
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->j:LX/0if;

    sget-object v1, LX/0ig;->ah:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2457723
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->i:LX/0Uh;

    const/16 v1, 0x5ba

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2457724
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->j:LX/0if;

    sget-object v1, LX/0ig;->ah:LX/0ih;

    const-string v2, "use_react_native"

    invoke-virtual {v0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2457725
    :cond_0
    new-instance v0, LX/HN4;

    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->j:LX/0if;

    invoke-direct {v0, v1, v2}, LX/HN4;-><init>(Landroid/view/LayoutInflater;LX/0if;)V

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    .line 2457726
    return-void

    .line 2457727
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    move-object v3, v4

    goto :goto_1
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2457728
    iput-object p1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->C:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2457729
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2457730
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a(Z)V

    .line 2457731
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/16 v0, 0x2a

    const v1, 0x396f3822

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2457732
    const v0, 0x7f030ee5

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 2457733
    const v0, 0x7f0d2452

    invoke-static {v3, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2457734
    const-string v1, "admin_publish_services"

    iget-object v4, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->E:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "admin_publish_services_edit_flow"

    iget-object v4, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->E:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2457735
    :cond_0
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2457736
    const v1, 0x7f0d227e

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/button/FigButton;

    .line 2457737
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0816a5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2457738
    new-instance v4, LX/HMn;

    invoke-direct {v4, p0}, LX/HMn;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    invoke-virtual {v1, v4}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2457739
    const v1, 0x7f0d227f

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 2457740
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0816a7

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2457741
    new-instance v1, LX/HMp;

    invoke-direct {v1, p0}, LX/HMp;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2457742
    :cond_1
    const v0, 0x7f0d2451

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->r:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2457743
    const v0, 0x7f0d2450

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    .line 2457744
    iget-boolean v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->D:Z

    if-eqz v0, :cond_2

    .line 2457745
    invoke-virtual {v3, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2457746
    :cond_2
    const/16 v0, 0x2b

    const v1, 0x6b1c3df3

    invoke-static {v7, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v3
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6f6928ac

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2457747
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2457748
    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->j:LX/0if;

    sget-object v2, LX/0ig;->ah:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 2457749
    const/16 v1, 0x2b

    const v2, -0x2563fea4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x56bd8adb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2457750
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2457751
    iget-boolean v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->w:Z

    if-eqz v1, :cond_0

    .line 2457752
    invoke-virtual {p0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->g()V

    .line 2457753
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->w:Z

    .line 2457754
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x1e048f2e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4723ef87

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2457755
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2457756
    invoke-static {p0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->l(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    .line 2457757
    const/16 v1, 0x2b

    const v2, 0x291ac41b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2457758
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2457759
    new-instance v0, LX/6WI;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6WI;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0817e4

    invoke-virtual {v0, v1}, LX/6WI;->a(I)LX/6WI;

    move-result-object v0

    const v1, 0x7f0817d0

    invoke-virtual {v0, v1}, LX/6WI;->b(I)LX/6WI;

    move-result-object v0

    const v1, 0x7f08001c

    new-instance v2, LX/HMs;

    invoke-direct {v2, p0}, LX/HMs;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    invoke-virtual {v0, v1, v2}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    const v1, 0x7f080017

    new-instance v2, LX/HMr;

    invoke-direct {v2, p0}, LX/HMr;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    invoke-virtual {v0, v1, v2}, LX/6WI;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    invoke-virtual {v0}, LX/6WI;->a()LX/6WJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->L:LX/6WJ;

    .line 2457760
    iget-boolean v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->D:Z

    if-eqz v0, :cond_0

    .line 2457761
    invoke-direct {p0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->q()V

    .line 2457762
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/listview/BetterListView;->setVerticalScrollBarEnabled(Z)V

    .line 2457763
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v4, v4, v4, v4}, Lcom/facebook/widget/listview/BetterListView;->setPadding(IIII)V

    .line 2457764
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->z:LX/E8s;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 2457765
    const v0, 0x7f0d244f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->r:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 2457766
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->r:Lcom/facebook/widget/listview/EmptyListViewItem;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2457767
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->r:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/listview/EmptyListViewItem;->setBackgroundResource(I)V

    .line 2457768
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->r:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 2457769
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->B:Landroid/view/View;

    .line 2457770
    iget v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->H:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->E_(I)V

    .line 2457771
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;)V

    .line 2457772
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/HMq;

    invoke-direct {v1, p0}, LX/HMq;-><init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2457773
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2457774
    :cond_0
    invoke-static {p0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->k$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    .line 2457775
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2457776
    iget v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->G:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a(I)V

    .line 2457777
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->r:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 2457778
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->r:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2457779
    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->u:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/services/PagesServicesFragment;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2457780
    :cond_1
    invoke-direct {p0, v4}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a(Z)V

    .line 2457781
    :goto_0
    return-void

    .line 2457782
    :cond_2
    invoke-static {p0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->n(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    goto :goto_0
.end method
