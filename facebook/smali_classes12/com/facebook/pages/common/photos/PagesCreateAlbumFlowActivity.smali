.class public Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private final A:LX/8Fd;

.field public p:LX/2U1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/9at;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/8Fe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/8I0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Z

.field public y:Z

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2447094
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2447095
    iput-boolean v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->x:Z

    .line 2447096
    iput-boolean v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->y:Z

    .line 2447097
    new-instance v0, LX/HH5;

    invoke-direct {v0, p0}, LX/HH5;-><init>(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->A:LX/8Fd;

    return-void
.end method

.method public static a(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;)V
    .locals 4

    .prologue
    .line 2447086
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->r:LX/8Fe;

    .line 2447087
    iget-object v1, v0, LX/8Fe;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v1

    .line 2447088
    if-eqz v0, :cond_0

    .line 2447089
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->q:LX/9at;

    sget-object v2, LX/9au;->ALBUMSTAB:LX/9au;

    invoke-virtual {v1, v2, v0}, LX/9at;->a(LX/9au;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;

    move-result-object v0

    .line 2447090
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->t:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2447091
    :goto_0
    return-void

    .line 2447092
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "getCreateAlbumIntent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "not page context for page "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->z:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2447093
    invoke-static {p0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->l(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;LX/2U1;LX/9at;LX/8Fe;LX/8I0;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;",
            "LX/2U1;",
            "LX/9at;",
            "LX/8Fe;",
            "Lcom/facebook/photos/intent/PhotosViewIntentBuilder;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2446993
    iput-object p1, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->p:LX/2U1;

    iput-object p2, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->q:LX/9at;

    iput-object p3, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->r:LX/8Fe;

    iput-object p4, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->s:LX/8I0;

    iput-object p5, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->t:Lcom/facebook/content/SecureContextHelper;

    iput-object p6, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->u:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p7, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->v:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->w:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;

    invoke-static {v8}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v1

    check-cast v1, LX/2U1;

    invoke-static {v8}, LX/9at;->b(LX/0QB;)LX/9at;

    move-result-object v2

    check-cast v2, LX/9at;

    invoke-static {v8}, LX/8Fe;->a(LX/0QB;)LX/8Fe;

    move-result-object v3

    check-cast v3, LX/8Fe;

    invoke-static {v8}, LX/8I0;->b(LX/0QB;)LX/8I0;

    move-result-object v4

    check-cast v4, LX/8I0;

    invoke-static {v8}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v8}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v6

    check-cast v6, Lcom/facebook/auth/viewercontext/ViewerContext;

    const/16 v7, 0x259

    invoke-static {v8, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v9, 0x12c4

    invoke-static {v8, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {v0 .. v8}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->a(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;LX/2U1;LX/9at;LX/8Fe;LX/8I0;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2447031
    new-instance v0, LX/4Vp;

    invoke-direct {v0}, LX/4Vp;-><init>()V

    .line 2447032
    iput-object p2, v0, LX/4Vp;->m:Ljava/lang/String;

    .line 2447033
    move-object v0, v0

    .line 2447034
    invoke-virtual {v0}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    .line 2447035
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->s:LX/8I0;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, p0, v5}, LX/8I0;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 2447036
    const-string v1, "extra_album_selected"

    invoke-static {v6, v1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2447037
    const-string v0, "extra_photo_tab_mode_params"

    sget-object v1, LX/5SD;->VIEWING_MODE:LX/5SD;

    iget-object v5, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->u:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2447038
    iget-object v7, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v5, v7

    .line 2447039
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v1, v8, v9}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a(LX/5SD;J)Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2447040
    const-string v0, "is_page"

    invoke-virtual {v6, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2447041
    const-string v0, "owner_id"

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2447042
    const-string v0, "pick_hc_pic"

    invoke-virtual {v6, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2447043
    const-string v0, "pick_pic_lite"

    invoke-virtual {v6, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2447044
    const-string v0, "disable_adding_photos_to_albums"

    invoke-virtual {v6, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2447045
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->p:LX/2U1;

    invoke-virtual {v0, p1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 2447046
    if-eqz v0, :cond_0

    .line 2447047
    iget-object v1, v0, LX/8Dk;->b:LX/0am;

    move-object v1, v1

    .line 2447048
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2447049
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->t:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v6, v10, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2447050
    :goto_0
    return-void

    .line 2447051
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2447052
    iget-object v1, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v1, v1

    .line 2447053
    invoke-virtual {v1}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v4

    .line 2447054
    :goto_1
    if-ge v5, v9, :cond_2

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2447055
    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2447056
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 2447057
    :cond_2
    iget-object v1, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v1, v1

    .line 2447058
    invoke-virtual {v1}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 2447059
    if-eqz v1, :cond_6

    move v1, v3

    .line 2447060
    :goto_2
    if-eqz v1, :cond_7

    .line 2447061
    iget-object v1, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v1, v1

    .line 2447062
    invoke-virtual {v1}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2447063
    invoke-virtual {v5, v1, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2447064
    :goto_3
    iget-object v5, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v5, v5

    .line 2447065
    invoke-virtual {v5}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_8

    .line 2447066
    :goto_4
    if-eqz v3, :cond_3

    .line 2447067
    iget-object v2, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v0, v2

    .line 2447068
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 2447069
    :cond_3
    new-instance v0, LX/89I;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v3, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v0, v4, v5, v3}, LX/89I;-><init>(JLX/2rw;)V

    .line 2447070
    iput-object v2, v0, LX/89I;->c:Ljava/lang/String;

    .line 2447071
    move-object v0, v0

    .line 2447072
    iput-object v1, v0, LX/89I;->d:Ljava/lang/String;

    .line 2447073
    move-object v0, v0

    .line 2447074
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    .line 2447075
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->r:LX/8Fe;

    .line 2447076
    iget-object v2, v1, LX/8Fe;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, v2

    .line 2447077
    if-eqz v1, :cond_5

    .line 2447078
    const-string v2, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v6, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2447079
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2447080
    const-string v1, "extra_pages_admin_permissions"

    invoke-virtual {v6, v1, v7}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2447081
    :cond_4
    const-string v1, "extra_composer_target_data"

    invoke-virtual {v6, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2447082
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->t:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v6, v10, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto/16 :goto_0

    :cond_6
    move v1, v4

    .line 2447083
    goto :goto_2

    :cond_7
    move-object v1, v2

    .line 2447084
    goto :goto_3

    :cond_8
    move v3, v4

    .line 2447085
    goto :goto_4
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2447025
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2447026
    const-string v1, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2447027
    const-wide/16 v0, 0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "No valid page id"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2447028
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->z:Ljava/lang/String;

    .line 2447029
    return-void

    .line 2447030
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;)V
    .locals 1

    .prologue
    .line 2447022
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->finish()V

    .line 2447023
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->x:Z

    .line 2447024
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2447016
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2447017
    invoke-static {p0, p0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2447018
    const v0, 0x7f030ed0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->setContentView(I)V

    .line 2447019
    invoke-direct {p0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->b()V

    .line 2447020
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->r:LX/8Fe;

    invoke-virtual {v0}, LX/8Fe;->a()V

    .line 2447021
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, -0x1

    .line 2447001
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2447002
    if-ne p1, v0, :cond_1

    .line 2447003
    if-ne p2, v3, :cond_0

    .line 2447004
    iput-boolean v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->y:Z

    .line 2447005
    const-string v0, "extra_album"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2447006
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2447007
    const-string v2, "extra_album"

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2447008
    invoke-virtual {p0, v3, v1}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->setResult(ILandroid/content/Intent;)V

    .line 2447009
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->z:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2447010
    :goto_0
    return-void

    .line 2447011
    :cond_0
    invoke-static {p0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->l(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;)V

    goto :goto_0

    .line 2447012
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 2447013
    invoke-static {p0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->l(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;)V

    goto :goto_0

    .line 2447014
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "getCreateAlbumIntent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Result is not handled for page "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->z:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2447015
    invoke-static {p0}, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->l(Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x74c9530f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2446998
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->r:LX/8Fe;

    invoke-virtual {v1}, LX/8Fe;->b()V

    .line 2446999
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2447000
    const/16 v1, 0x23

    const v2, -0xd523222

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x22

    const v1, -0x3af165e9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2446994
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2446995
    iget-boolean v1, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->x:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->y:Z

    if-nez v1, :cond_0

    .line 2446996
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->r:LX/8Fe;

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->z:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/pages/common/photos/PagesCreateAlbumFlowActivity;->A:LX/8Fd;

    invoke-virtual {v1, v2, v3}, LX/8Fe;->a(Ljava/lang/String;LX/8Fd;)V

    .line 2446997
    :cond_0
    const/16 v1, 0x23

    const v2, -0x4a5f51f1

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
