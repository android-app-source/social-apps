.class public final enum Lcom/facebook/pages/common/photos/PagePhotosType;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/pages/common/photos/PagePhotosType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/pages/common/photos/PagePhotosType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/common/photos/PagePhotosType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PHOTOS_TAKEN_HERE:Lcom/facebook/pages/common/photos/PagePhotosType;

.field public static final enum PHOTOS_TAKEN_OF:Lcom/facebook/pages/common/photos/PagePhotosType;

.field public static final enum POSTED_PHOTOS:Lcom/facebook/pages/common/photos/PagePhotosType;

.field public static final enum UNSET_PAGE_PHOTOS_TYPE:Lcom/facebook/pages/common/photos/PagePhotosType;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2446521
    new-instance v0, Lcom/facebook/pages/common/photos/PagePhotosType;

    const-string v1, "UNSET_PAGE_PHOTOS_TYPE"

    invoke-direct {v0, v1, v2}, Lcom/facebook/pages/common/photos/PagePhotosType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->UNSET_PAGE_PHOTOS_TYPE:Lcom/facebook/pages/common/photos/PagePhotosType;

    .line 2446522
    new-instance v0, Lcom/facebook/pages/common/photos/PagePhotosType;

    const-string v1, "POSTED_PHOTOS"

    invoke-direct {v0, v1, v3}, Lcom/facebook/pages/common/photos/PagePhotosType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->POSTED_PHOTOS:Lcom/facebook/pages/common/photos/PagePhotosType;

    .line 2446523
    new-instance v0, Lcom/facebook/pages/common/photos/PagePhotosType;

    const-string v1, "PHOTOS_TAKEN_HERE"

    invoke-direct {v0, v1, v4}, Lcom/facebook/pages/common/photos/PagePhotosType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->PHOTOS_TAKEN_HERE:Lcom/facebook/pages/common/photos/PagePhotosType;

    .line 2446524
    new-instance v0, Lcom/facebook/pages/common/photos/PagePhotosType;

    const-string v1, "PHOTOS_TAKEN_OF"

    invoke-direct {v0, v1, v5}, Lcom/facebook/pages/common/photos/PagePhotosType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->PHOTOS_TAKEN_OF:Lcom/facebook/pages/common/photos/PagePhotosType;

    .line 2446525
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/pages/common/photos/PagePhotosType;

    sget-object v1, Lcom/facebook/pages/common/photos/PagePhotosType;->UNSET_PAGE_PHOTOS_TYPE:Lcom/facebook/pages/common/photos/PagePhotosType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/pages/common/photos/PagePhotosType;->POSTED_PHOTOS:Lcom/facebook/pages/common/photos/PagePhotosType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/pages/common/photos/PagePhotosType;->PHOTOS_TAKEN_HERE:Lcom/facebook/pages/common/photos/PagePhotosType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/pages/common/photos/PagePhotosType;->PHOTOS_TAKEN_OF:Lcom/facebook/pages/common/photos/PagePhotosType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->$VALUES:[Lcom/facebook/pages/common/photos/PagePhotosType;

    .line 2446526
    new-instance v0, LX/HGk;

    invoke-direct {v0}, LX/HGk;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2446520
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromPageMetaData(ZZ)Lcom/facebook/pages/common/photos/PagePhotosType;
    .locals 1

    .prologue
    .line 2446514
    if-eqz p0, :cond_0

    .line 2446515
    sget-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->POSTED_PHOTOS:Lcom/facebook/pages/common/photos/PagePhotosType;

    .line 2446516
    :goto_0
    return-object v0

    .line 2446517
    :cond_0
    if-eqz p1, :cond_1

    .line 2446518
    sget-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->PHOTOS_TAKEN_HERE:Lcom/facebook/pages/common/photos/PagePhotosType;

    goto :goto_0

    .line 2446519
    :cond_1
    sget-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->PHOTOS_TAKEN_OF:Lcom/facebook/pages/common/photos/PagePhotosType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/pages/common/photos/PagePhotosType;
    .locals 1

    .prologue
    .line 2446509
    const-class v0, Lcom/facebook/pages/common/photos/PagePhotosType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/photos/PagePhotosType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/pages/common/photos/PagePhotosType;
    .locals 1

    .prologue
    .line 2446513
    sget-object v0, Lcom/facebook/pages/common/photos/PagePhotosType;->$VALUES:[Lcom/facebook/pages/common/photos/PagePhotosType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/pages/common/photos/PagePhotosType;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2446512
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2446510
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/PagePhotosType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2446511
    return-void
.end method
