.class public final Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2447647
    const-class v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    new-instance v1, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2447648
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2447659
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2447649
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2447650
    invoke-static {p1, v0}, LX/HHQ;->a(LX/15w;LX/186;)I

    move-result v1

    .line 2447651
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2447652
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2447653
    new-instance v1, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    invoke-direct {v1}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;-><init>()V

    .line 2447654
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2447655
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2447656
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2447657
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2447658
    :cond_0
    return-object v1
.end method
