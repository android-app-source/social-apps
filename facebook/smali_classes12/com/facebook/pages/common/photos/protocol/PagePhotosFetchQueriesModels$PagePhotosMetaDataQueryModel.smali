.class public final Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xf08074a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2447616
    const-class v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2447615
    const-class v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2447613
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2447614
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2447603
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2447604
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2447605
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 2447606
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2447607
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 2447608
    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->f:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 2447609
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2447610
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2447611
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2447612
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2447600
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2447601
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2447602
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2447599
    new-instance v0, LX/HHL;

    invoke-direct {v0, p1}, LX/HHL;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2447595
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2447596
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->e:Z

    .line 2447597
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->f:Z

    .line 2447598
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2447617
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2447618
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2447594
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2447592
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2447593
    iget-boolean v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2447589
    new-instance v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;-><init>()V

    .line 2447590
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2447591
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2447588
    const v0, -0x593395f2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2447587
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2447585
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2447586
    iget-boolean v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->f:Z

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2447583
    iget-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->g:Ljava/lang/String;

    .line 2447584
    iget-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2447581
    iget-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->h:Ljava/util/List;

    .line 2447582
    iget-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
