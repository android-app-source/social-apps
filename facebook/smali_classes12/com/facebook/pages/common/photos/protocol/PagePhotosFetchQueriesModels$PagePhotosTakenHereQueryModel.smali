.class public final Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x59f87919
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2447742
    const-class v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2447741
    const-class v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2447739
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2447740
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2447733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2447734
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2447735
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2447736
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2447737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2447738
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2447725
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2447726
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2447727
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    .line 2447728
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2447729
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;

    .line 2447730
    iput-object v0, v1, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    .line 2447731
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2447732
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2447714
    new-instance v0, LX/HHM;

    invoke-direct {v0, p1}, LX/HHM;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2447723
    iget-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    iput-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    .line 2447724
    iget-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel$PhotosTakenHereModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2447721
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2447722
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2447720
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2447717
    new-instance v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenHereQueryModel;-><init>()V

    .line 2447718
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2447719
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2447716
    const v0, 0x31e44703

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2447715
    const v0, 0x25d6af

    return v0
.end method
