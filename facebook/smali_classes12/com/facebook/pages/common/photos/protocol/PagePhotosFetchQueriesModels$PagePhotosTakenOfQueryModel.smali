.class public final Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x396972a2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2447864
    const-class v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2447863
    const-class v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2447861
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2447862
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2447855
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2447856
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2447857
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2447858
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2447859
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2447860
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2447838
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2447839
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2447840
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;

    .line 2447841
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2447842
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;

    .line 2447843
    iput-object v0, v1, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;

    .line 2447844
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2447845
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2447854
    new-instance v0, LX/HHN;

    invoke-direct {v0, p1}, LX/HHN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2447865
    iget-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;

    iput-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;

    .line 2447866
    iget-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel$PhotosTakenOfModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2447852
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2447853
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2447851
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2447848
    new-instance v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosTakenOfQueryModel;-><init>()V

    .line 2447849
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2447850
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2447847
    const v0, -0x5ae81106

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2447846
    const v0, 0x25d6af

    return v0
.end method
