.class public final Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2542ed8d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2447990
    const-class v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2447989
    const-class v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2447987
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2447988
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2447962
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2447963
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2447964
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2447965
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2447966
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2447967
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2447979
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2447980
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2447981
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;

    .line 2447982
    invoke-virtual {p0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;->a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2447983
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;

    .line 2447984
    iput-object v0, v1, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;

    .line 2447985
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2447986
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2447978
    new-instance v0, LX/HHO;

    invoke-direct {v0, p1}, LX/HHO;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2447976
    iget-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;

    iput-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;

    .line 2447977
    iget-object v0, p0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;->e:Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel$PostedPhotosModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2447974
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2447975
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2447973
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2447970
    new-instance v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePostedPhotosQueryModel;-><init>()V

    .line 2447971
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2447972
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2447969
    const v0, -0xea80c6c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2447968
    const v0, 0x25d6af

    return v0
.end method
