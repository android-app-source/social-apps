.class public final Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2447579
    const-class v0, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;

    new-instance v1, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2447580
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2447556
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2447558
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2447559
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x3

    .line 2447560
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2447561
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2447562
    if-eqz v2, :cond_0

    .line 2447563
    const-string p0, "expressed_as_place"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2447564
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2447565
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2447566
    if-eqz v2, :cond_1

    .line 2447567
    const-string p0, "is_owned"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2447568
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2447569
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2447570
    if-eqz v2, :cond_2

    .line 2447571
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2447572
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2447573
    :cond_2
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 2447574
    if-eqz v2, :cond_3

    .line 2447575
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2447576
    invoke-virtual {v1, v0, p2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2447577
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2447578
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2447557
    check-cast p1, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel$Serializer;->a(Lcom/facebook/pages/common/photos/protocol/PagePhotosFetchQueriesModels$PagePhotosMetaDataQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
