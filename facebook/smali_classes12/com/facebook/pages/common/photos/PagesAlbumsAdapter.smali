.class public Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/HGm;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:LX/11R;

.field private c:Landroid/content/Context;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:LX/HH2;

.field private g:LX/HH1;

.field private h:Landroid/view/LayoutInflater;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2446686
    const-class v0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/11R;Landroid/content/Context;Ljava/util/List;ZLX/HH2;LX/HH1;)V
    .locals 2
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/HH2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/HH1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11R;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            ">;Z",
            "Lcom/facebook/pages/common/photos/PagesAlbumsAdapter$OnClickAlbumListener;",
            "Lcom/facebook/pages/common/photos/PagesAlbumsAdapter$OnClickCreateAlbumListener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2446646
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2446647
    iput-boolean v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->e:Z

    .line 2446648
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2446649
    if-eqz p4, :cond_0

    if-eqz p6, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    const-string v1, "canViewerAddAlbum without providing onClickCreateAlbumListener"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2446650
    iput-object p1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->b:LX/11R;

    .line 2446651
    iput-object p2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->c:Landroid/content/Context;

    .line 2446652
    iput-object p3, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->d:Ljava/util/List;

    .line 2446653
    iput-boolean p4, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->e:Z

    .line 2446654
    iput-object p5, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->f:LX/HH2;

    .line 2446655
    iput-object p6, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->g:LX/HH1;

    .line 2446656
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->h:Landroid/view/LayoutInflater;

    .line 2446657
    return-void
.end method

.method public static e(Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;)I
    .locals 1

    .prologue
    .line 2446685
    invoke-direct {p0}, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 2446684
    iget-boolean v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->g:LX/HH1;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2446676
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->h:Landroid/view/LayoutInflater;

    const v1, 0x7f030e1e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2446677
    sget-object v0, LX/HGl;->a:[I

    invoke-static {}, LX/HGp;->values()[LX/HGp;

    move-result-object v2

    aget-object v2, v2, p2

    invoke-virtual {v2}, LX/HGp;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2446678
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2446679
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->f:LX/HH2;

    if-eqz v0, :cond_0

    .line 2446680
    new-instance v0, LX/HGn;

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->f:LX/HH2;

    invoke-direct {v0, v1, v2}, LX/HGn;-><init>(Landroid/view/View;LX/HH2;)V

    .line 2446681
    :goto_0
    return-object v0

    .line 2446682
    :cond_0
    new-instance v0, LX/HGn;

    invoke-direct {v0, v1}, LX/HGn;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2446683
    :pswitch_1
    new-instance v0, LX/HGo;

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->g:LX/HH1;

    invoke-direct {v0, v1, v2}, LX/HGo;-><init>(Landroid/view/View;LX/HH1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2446667
    check-cast p1, LX/HGm;

    .line 2446668
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2446669
    sget-object v1, LX/HGp;->ALBUM:LX/HGp;

    invoke-virtual {v1}, LX/HGp;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 2446670
    invoke-static {p0}, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->e(Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;)I

    move-result v0

    sub-int v0, p2, v0

    move v1, v0

    .line 2446671
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2446672
    check-cast p1, LX/HGn;

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->b:LX/11R;

    invoke-virtual {p1, v0, v1, v2, v3}, LX/HGn;->a(Lcom/facebook/graphql/model/GraphQLAlbum;ILandroid/content/Context;LX/11R;)V

    .line 2446673
    :cond_0
    return-void

    .line 2446674
    :cond_1
    sget-object v1, LX/HGp;->CREATE_ALBUM:LX/HGp;

    invoke-virtual {v1}, LX/HGp;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2446675
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid view Holder "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLAlbumsConnection;)V
    .locals 2

    .prologue
    .line 2446664
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2446665
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->d:Ljava/util/List;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2446666
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2446662
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2446663
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2446659
    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2446660
    sget-object v0, LX/HGp;->CREATE_ALBUM:LX/HGp;

    invoke-virtual {v0}, LX/HGp;->ordinal()I

    move-result v0

    .line 2446661
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/HGp;->ALBUM:LX/HGp;

    invoke-virtual {v0}, LX/HGp;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2446658
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p0}, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->e(Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
