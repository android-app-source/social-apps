.class public Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/GZf;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public B:Z

.field public C:Z

.field public D:Ljava/lang/String;

.field private E:Lcom/facebook/common/callercontext/CallerContext;

.field public F:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public G:Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Ljava/lang/String;

.field public I:Z

.field private J:LX/E8s;

.field private K:LX/E8t;

.field private L:Landroid/view/View;

.field private M:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field private N:I

.field private O:I

.field public b:LX/9bY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9bq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1L1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0b3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/HGq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/2U1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9at;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1mR;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/intent/PhotosViewIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final r:LX/9bb;

.field private final s:LX/9bd;

.field private final t:LX/9bh;

.field private final u:LX/1L3;

.field private final v:LX/HH4;

.field private w:Landroid/support/v7/widget/RecyclerView;

.field public x:LX/1P1;

.field public y:Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

.field public z:LX/HME;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2446977
    const-class v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2446915
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2446916
    new-instance v0, LX/HGu;

    invoke-direct {v0, p0}, LX/HGu;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->r:LX/9bb;

    .line 2446917
    new-instance v0, LX/HGv;

    invoke-direct {v0, p0}, LX/HGv;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->s:LX/9bd;

    .line 2446918
    new-instance v0, LX/HGw;

    invoke-direct {v0, p0}, LX/HGw;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->t:LX/9bh;

    .line 2446919
    new-instance v0, LX/HGx;

    invoke-direct {v0, p0}, LX/HGx;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->u:LX/1L3;

    .line 2446920
    new-instance v0, LX/HH4;

    invoke-direct {v0, p0}, LX/HH4;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->v:LX/HH4;

    .line 2446921
    iput-boolean v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->B:Z

    .line 2446922
    iput-boolean v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->C:Z

    .line 2446923
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->H:Ljava/lang/String;

    .line 2446924
    iput-boolean v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->I:Z

    .line 2446925
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->O:I

    .line 2446926
    return-void
.end method

.method private static a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2446927
    if-eqz p0, :cond_0

    .line 2446928
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2446929
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2446930
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/ArrayList;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;
    .locals 4
    .param p2    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/ipc/composer/intent/ComposerTargetData;",
            ")",
            "Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;"
        }
    .end annotation

    .prologue
    .line 2446931
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2446932
    const-string v1, "owner_id"

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2446933
    const-string v1, "extra_caller_context"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2446934
    if-eqz p2, :cond_0

    .line 2446935
    const-string v1, "extra_pages_admin_permissions"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2446936
    :cond_0
    if-eqz p3, :cond_1

    .line 2446937
    const-string v1, "extra_composer_target_data"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2446938
    :cond_1
    new-instance v1, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-direct {v1}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;-><init>()V

    .line 2446939
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2446940
    return-object v1
.end method

.method private static a(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;LX/9bY;LX/9bq;LX/1L1;LX/0b3;LX/HGq;LX/1Ck;Lcom/facebook/auth/viewercontext/ViewerContext;LX/2U1;LX/0SI;LX/0Uh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;",
            "LX/9bY;",
            "LX/9bq;",
            "LX/1L1;",
            "LX/0b3;",
            "LX/HGq;",
            "LX/1Ck;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/2U1;",
            "LX/0SI;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/9at;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1mR;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/intent/PhotosViewIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2446941
    iput-object p1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->b:LX/9bY;

    iput-object p2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->c:LX/9bq;

    iput-object p3, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->d:LX/1L1;

    iput-object p4, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->e:LX/0b3;

    iput-object p5, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->f:LX/HGq;

    iput-object p6, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->g:LX/1Ck;

    iput-object p7, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p8, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->i:LX/2U1;

    iput-object p9, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->j:LX/0SI;

    iput-object p10, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->k:LX/0Uh;

    iput-object p11, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->l:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->m:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->n:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->o:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->p:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->q:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 20

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v18

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    invoke-static/range {v18 .. v18}, LX/9bY;->a(LX/0QB;)LX/9bY;

    move-result-object v3

    check-cast v3, LX/9bY;

    invoke-static/range {v18 .. v18}, LX/9bq;->a(LX/0QB;)LX/9bq;

    move-result-object v4

    check-cast v4, LX/9bq;

    invoke-static/range {v18 .. v18}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v5

    check-cast v5, LX/1L1;

    invoke-static/range {v18 .. v18}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v6

    check-cast v6, LX/0b3;

    const-class v7, LX/HGq;

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/HGq;

    invoke-static/range {v18 .. v18}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static/range {v18 .. v18}, LX/0eQ;->a(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v9

    check-cast v9, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static/range {v18 .. v18}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v10

    check-cast v10, LX/2U1;

    invoke-static/range {v18 .. v18}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v11

    check-cast v11, LX/0SI;

    invoke-static/range {v18 .. v18}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    const/16 v13, 0x2dfd

    move-object/from16 v0, v18

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x259

    move-object/from16 v0, v18

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0xb0b

    move-object/from16 v0, v18

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x455

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x2e65

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v19, 0x2b68

    invoke-static/range {v18 .. v19}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {v2 .. v18}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->a(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;LX/9bY;LX/9bq;LX/1L1;LX/0b3;LX/HGq;LX/1Ck;Lcom/facebook/auth/viewercontext/ViewerContext;LX/2U1;LX/0SI;LX/0Uh;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;LX/0zS;Ljava/util/Set;)V
    .locals 8
    .param p1    # LX/0zS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zS;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 2446978
    iput-boolean v7, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->B:Z

    .line 2446979
    invoke-static {p0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->p(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    .line 2446980
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->c:LX/9bq;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    const/16 v2, 0xfa

    iget-object v4, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->H:Ljava/lang/String;

    const/16 v5, 0x14

    move-object v3, p1

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, LX/9bq;->a(Ljava/lang/String;ILX/0zS;Ljava/lang/String;ILjava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2446981
    new-instance v2, LX/HGs;

    invoke-direct {v2, p0}, LX/HGs;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    .line 2446982
    iget-object v3, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->g:LX/1Ck;

    const-string v4, "fetchPagesAlbumsList_%s_%s"

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    aput-object v6, v5, v0

    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->H:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    aput-object v0, v5, v7

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, LX/HGt;

    invoke-direct {v4, p0, v1}, LX/HGt;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v3, v0, v4, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2446983
    return-void

    .line 2446984
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->H:Ljava/lang/String;

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2446942
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->J:LX/E8s;

    if-nez v0, :cond_2

    .line 2446943
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->K:LX/E8t;

    if-nez v0, :cond_1

    .line 2446944
    new-instance v0, LX/E8s;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->J:LX/E8s;

    .line 2446945
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->J:LX/E8s;

    const/4 v1, 0x1

    .line 2446946
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2446947
    :cond_0
    :goto_1
    return-void

    .line 2446948
    :cond_1
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->K:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->J:LX/E8s;

    .line 2446949
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->K:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->J:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    goto :goto_0

    .line 2446950
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->K:LX/E8t;

    if-eqz v0, :cond_0

    .line 2446951
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->J:LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->K:LX/E8t;

    .line 2446952
    iput-object v1, v0, LX/E8s;->a:Landroid/view/View;

    .line 2446953
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->K:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->J:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2446954
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->J:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->requestLayout()V

    goto :goto_1
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 2446955
    const-string v0, "owner_id"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2446956
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 2446957
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid page id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2446958
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    .line 2446959
    const-string v0, "extra_caller_context"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->E:Lcom/facebook/common/callercontext/CallerContext;

    .line 2446960
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->E:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v0, :cond_1

    .line 2446961
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid caller context "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->E:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2446962
    :cond_1
    const-string v0, "extra_pages_admin_permissions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->F:Ljava/util/ArrayList;

    .line 2446963
    const-string v0, "extra_composer_target_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->G:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2446964
    return-void
.end method

.method private d()V
    .locals 13

    .prologue
    .line 2446965
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2446966
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->F:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    new-instance v0, LX/8A4;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->F:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v1, LX/8A3;->CREATE_CONTENT:LX/8A3;

    invoke-virtual {v0, v1}, LX/8A4;->a(LX/8A3;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    .line 2446967
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->f:LX/HGq;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2446968
    new-instance v4, LX/HH2;

    invoke-direct {v4, p0}, LX/HH2;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    move-object v4, v4

    .line 2446969
    if-eqz v3, :cond_1

    .line 2446970
    new-instance v5, LX/HH1;

    invoke-direct {v5, p0}, LX/HH1;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    move-object v5, v5

    .line 2446971
    :goto_1
    new-instance v6, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v7

    check-cast v7, LX/11R;

    move-object v8, v1

    move-object v9, v2

    move v10, v3

    move-object v11, v4

    move-object v12, v5

    invoke-direct/range {v6 .. v12}, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;-><init>(LX/11R;Landroid/content/Context;Ljava/util/List;ZLX/HH2;LX/HH1;)V

    .line 2446972
    move-object v0, v6

    .line 2446973
    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->y:Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

    .line 2446974
    return-void

    .line 2446975
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 2446976
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public static l(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2446900
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2446901
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 2446902
    if-eqz v0, :cond_1

    .line 2446903
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2446904
    :cond_0
    :goto_0
    return-object v0

    .line 2446905
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->j:LX/0SI;

    invoke-interface {v0}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 2446906
    if-eqz v0, :cond_2

    .line 2446907
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v1

    .line 2446908
    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 2446909
    :goto_1
    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 2446910
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private n()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2446911
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->c:LX/9bq;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    const/16 v2, 0xfa

    sget-object v3, LX/0zS;->b:LX/0zS;

    const/16 v5, 0x14

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, LX/9bq;->a(Ljava/lang/String;ILX/0zS;Ljava/lang/String;ILjava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2446912
    new-instance v1, LX/HH3;

    invoke-direct {v1, p0}, LX/HH3;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    .line 2446913
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->g:LX/1Ck;

    const-string v3, "loadPagesAlbumsListFromCache_%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/HGr;

    invoke-direct {v4, p0, v0}, LX/HGr;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v2, v3, v4, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2446914
    return-void
.end method

.method public static o(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V
    .locals 2

    .prologue
    .line 2446797
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid pageId"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2446798
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1mR;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    invoke-static {v1}, LX/9bq;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2446799
    return-void

    .line 2446800
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V
    .locals 2

    .prologue
    .line 2446896
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->A:Lcom/facebook/widget/listview/EmptyListViewItem;

    if-nez v0, :cond_0

    .line 2446897
    :goto_0
    return-void

    .line 2446898
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->A:Lcom/facebook/widget/listview/EmptyListViewItem;

    iget-boolean v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->B:Z

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2446899
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->A:Lcom/facebook/widget/listview/EmptyListViewItem;

    iget-boolean v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->B:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2446801
    iget v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->O:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->O:I

    if-le p1, v0, :cond_1

    .line 2446802
    :cond_0
    :goto_0
    return-void

    .line 2446803
    :cond_1
    iput p1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->O:I

    .line 2446804
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->L:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2446805
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->L:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->O:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2446806
    iput p1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->N:I

    .line 2446807
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->w:Landroid/support/v7/widget/RecyclerView;

    iget v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->N:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2446808
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2446809
    iput-object p1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->K:LX/E8t;

    .line 2446810
    invoke-direct {p0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->b()V

    .line 2446811
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2446812
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2446813
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2446814
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2446815
    if-eqz v0, :cond_0

    .line 2446816
    invoke-direct {p0, v0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->b(Landroid/os/Bundle;)V

    .line 2446817
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->b:LX/9bY;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->r:LX/9bb;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2446818
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->b:LX/9bY;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->s:LX/9bd;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2446819
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->b:LX/9bY;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->t:LX/9bh;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2446820
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->d:LX/1L1;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->u:LX/1L3;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2446821
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->e:LX/0b3;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->v:LX/HH4;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2446822
    return-void
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2446823
    iput-object p1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->M:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2446824
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 2446825
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->H:Ljava/lang/String;

    .line 2446826
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->C:Z

    .line 2446827
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->y:Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

    invoke-virtual {v0}, Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;->d()V

    .line 2446828
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->z:LX/HME;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2446829
    sget-object v0, LX/0zS;->d:LX/0zS;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    invoke-static {v1}, LX/9bq;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->a$redex0(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;LX/0zS;Ljava/util/Set;)V

    .line 2446830
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/16 v0, 0x2a

    const v1, 0x3e8f60ea

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2446831
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->i:LX/2U1;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 2446832
    if-eqz v0, :cond_0

    .line 2446833
    iget-object v1, v0, LX/8Dk;->b:LX/0am;

    move-object v1, v1

    .line 2446834
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2446835
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2446836
    iget-boolean v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v2

    .line 2446837
    if-nez v1, :cond_0

    .line 2446838
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v1

    const/4 v2, 0x1

    .line 2446839
    iput-boolean v2, v1, LX/0SK;->d:Z

    .line 2446840
    move-object v1, v1

    .line 2446841
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2446842
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2446843
    iput-object v2, v1, LX/0SK;->c:Ljava/lang/String;

    .line 2446844
    move-object v1, v1

    .line 2446845
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2446846
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    move-object v2, v3

    .line 2446847
    iput-object v2, v1, LX/0SK;->f:Ljava/lang/String;

    .line 2446848
    move-object v1, v1

    .line 2446849
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->h:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2446850
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    move-object v2, v3

    .line 2446851
    iput-object v2, v1, LX/0SK;->e:Ljava/lang/String;

    .line 2446852
    move-object v1, v1

    .line 2446853
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    .line 2446854
    iput-object v2, v1, LX/0SK;->a:Ljava/lang/String;

    .line 2446855
    move-object v2, v1

    .line 2446856
    iget-object v1, v0, LX/8Dk;->b:LX/0am;

    move-object v1, v1

    .line 2446857
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2446858
    iput-object v1, v2, LX/0SK;->b:Ljava/lang/String;

    .line 2446859
    move-object v1, v2

    .line 2446860
    iget-object v2, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v0, v2

    .line 2446861
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 2446862
    iput-object v0, v1, LX/0SK;->g:Ljava/lang/String;

    .line 2446863
    move-object v0, v1

    .line 2446864
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->j:LX/0SI;

    invoke-virtual {v0}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0SI;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2446865
    :cond_0
    const v0, 0x7f030e1d

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 2446866
    const v0, 0x7f0d2287

    invoke-static {v7, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->w:Landroid/support/v7/widget/RecyclerView;

    .line 2446867
    invoke-direct {p0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->b()V

    .line 2446868
    new-instance v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->A:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2446869
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->A:Lcom/facebook/widget/listview/EmptyListViewItem;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2446870
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->A:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v8}, Lcom/facebook/widget/listview/EmptyListViewItem;->setBackgroundResource(I)V

    .line 2446871
    invoke-static {p0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->p(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    .line 2446872
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->L:Landroid/view/View;

    .line 2446873
    iget v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->O:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->E_(I)V

    .line 2446874
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->y:Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

    if-nez v0, :cond_1

    .line 2446875
    invoke-direct {p0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->d()V

    .line 2446876
    invoke-direct {p0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->n()V

    .line 2446877
    sget-object v0, LX/0zS;->d:LX/0zS;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->D:Ljava/lang/String;

    invoke-static {v1}, LX/9bq;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->a$redex0(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;LX/0zS;Ljava/util/Set;)V

    .line 2446878
    :cond_1
    new-instance v0, LX/HME;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->J:LX/E8s;

    invoke-static {v2}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->y:Lcom/facebook/pages/common/photos/PagesAlbumsAdapter;

    iget-object v4, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->A:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-static {v4}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->L:Landroid/view/View;

    invoke-static {v5}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/HME;-><init>(Landroid/content/Context;Landroid/view/View;LX/1OM;Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->z:LX/HME;

    .line 2446879
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v8}, LX/1P1;-><init>(IZ)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->x:LX/1P1;

    .line 2446880
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0213b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2446881
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->w:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, LX/HGz;

    invoke-direct {v2, p0, v0}, LX/HGz;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2446882
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->w:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->x:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2446883
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->w:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->z:LX/HME;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2446884
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->w:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/HH0;

    invoke-direct {v1, p0}, LX/HH0;-><init>(Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2446885
    const/16 v0, 0x2b

    const v1, 0x650a10e0

    invoke-static {v9, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v7
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x78fce167

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2446886
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->b:LX/9bY;

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->r:LX/9bb;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2446887
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->b:LX/9bY;

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->s:LX/9bd;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2446888
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->b:LX/9bY;

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->t:LX/9bh;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2446889
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->d:LX/1L1;

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->u:LX/1L3;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2446890
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->e:LX/0b3;

    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->v:LX/HH4;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2446891
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2446892
    const/16 v1, 0x2b

    const v2, -0x6613afd8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2446893
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2446894
    iget v0, p0, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->N:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->a(I)V

    .line 2446895
    return-void
.end method
