.class public Lcom/facebook/pages/common/photos/PagesPhotosFragment;
.super Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/GZf;


# static fields
.field public static final l:[I


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/view/View;

.field private c:Lcom/facebook/widget/listview/EmptyListViewItem;

.field private d:LX/E8s;

.field private e:LX/E8t;

.field public f:Landroid/view/View;

.field public g:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field private h:Z

.field public i:I

.field private j:I

.field private k:I

.field public m:Z

.field public n:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2447117
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->l:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2447197
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;-><init>()V

    .line 2447198
    iput v1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->k:I

    .line 2447199
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->m:Z

    .line 2447200
    iput v1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->n:I

    return-void
.end method

.method public static a(JZ)Lcom/facebook/pages/common/photos/PagesPhotosFragment;
    .locals 6

    .prologue
    .line 2447189
    new-instance v0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    invoke-direct {v0}, Lcom/facebook/pages/common/photos/PagesPhotosFragment;-><init>()V

    .line 2447190
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2447191
    const-string v2, "owner_id"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2447192
    const-string v2, "is_page"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2447193
    const-string v2, "customized_res"

    new-instance v3, Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2447194
    const-string v2, "extra_is_inside_page_surface_tab"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2447195
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2447196
    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object p0, p1, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->a:LX/0hB;

    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 2447176
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->d:LX/E8s;

    if-nez v0, :cond_2

    .line 2447177
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->e:LX/E8t;

    if-nez v0, :cond_1

    .line 2447178
    new-instance v0, LX/E8s;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->d:LX/E8s;

    .line 2447179
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->d:LX/E8s;

    const/4 v1, 0x1

    .line 2447180
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2447181
    :cond_0
    :goto_1
    return-void

    .line 2447182
    :cond_1
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->e:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->d:LX/E8s;

    .line 2447183
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->e:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->d:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    goto :goto_0

    .line 2447184
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->e:LX/E8t;

    if-eqz v0, :cond_0

    .line 2447185
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->d:LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->e:LX/E8t;

    .line 2447186
    iput-object v1, v0, LX/E8s;->a:Landroid/view/View;

    .line 2447187
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->e:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->d:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2447188
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->d:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->requestLayout()V

    goto :goto_1
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2447171
    iget v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->k:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->k:I

    if-le p1, v0, :cond_1

    .line 2447172
    :cond_0
    :goto_0
    return-void

    .line 2447173
    :cond_1
    iput p1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->k:I

    .line 2447174
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2447175
    iget-object v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->f:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->k:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2447170
    const-string v0, "page_photo_pandora"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2447166
    iput p1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->j:I

    .line 2447167
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v0

    .line 2447168
    iget v1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->j:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2447169
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2447163
    iput-object p1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->e:LX/E8t;

    .line 2447164
    invoke-direct {p0}, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->k()V

    .line 2447165
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2447158
    invoke-super {p0, p1}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a(Landroid/os/Bundle;)V

    .line 2447159
    const-class v0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2447160
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2447161
    const-string v1, "extra_is_inside_page_surface_tab"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->h:Z

    .line 2447162
    return-void
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2447156
    iput-object p1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->g:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2447157
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2447150
    invoke-super {p0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->d()V

    .line 2447151
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v0

    .line 2447152
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->removeHeaderView(Landroid/view/View;)Z

    .line 2447153
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2447154
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0062

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setDividerHeight(I)V

    .line 2447155
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 2447148
    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c()V

    .line 2447149
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/16 v0, 0x2a

    const v1, 0x6a7261ed

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2447127
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->b:Landroid/view/View;

    .line 2447128
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c:Lcom/facebook/widget/listview/BetterListView;

    move-object v1, v1

    .line 2447129
    if-eqz v1, :cond_0

    .line 2447130
    invoke-direct {p0}, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->k()V

    .line 2447131
    invoke-virtual {v1, v6}, Lcom/facebook/widget/listview/BetterListView;->setVerticalScrollBarEnabled(Z)V

    .line 2447132
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->d:LX/E8s;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 2447133
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->b:Landroid/view/View;

    const v3, 0x7f0d0521

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2447134
    new-instance v2, Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2447135
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v2, v6}, Lcom/facebook/widget/listview/EmptyListViewItem;->setBackgroundResource(I)V

    .line 2447136
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v2, v6}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 2447137
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2447138
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 2447139
    invoke-virtual {v1, v6}, Lcom/facebook/widget/listview/BetterListView;->setHeaderDividersEnabled(Z)V

    .line 2447140
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2447141
    new-instance v2, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->f:Landroid/view/View;

    .line 2447142
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->f:Landroid/view/View;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00d5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v2, v3}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2447143
    iget v2, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->k:I

    invoke-virtual {p0, v2}, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->E_(I)V

    .line 2447144
    iget-object v2, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->f:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;)V

    .line 2447145
    new-instance v2, LX/HH6;

    invoke-direct {v2, p0, v1}, LX/HH6;-><init>(Lcom/facebook/pages/common/photos/PagesPhotosFragment;Lcom/facebook/widget/listview/BetterListView;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2447146
    invoke-virtual {v1, v6}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2447147
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->b:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x6fa34b0a

    invoke-static {v7, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4a7013a8    # 3933418.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2447121
    invoke-super {p0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->onStart()V

    .line 2447122
    iget-boolean v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->h:Z

    if-nez v0, :cond_0

    .line 2447123
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2447124
    if-eqz v0, :cond_0

    .line 2447125
    const v2, 0x7f081274

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2447126
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x1883e78d

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2447118
    invoke-super {p0, p1, p2}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2447119
    iget v0, p0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->j:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->a(I)V

    .line 2447120
    return-void
.end method
