.class public Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;
.super Lcom/facebook/reviews/ui/PageReviewsFeedFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/GZf;


# static fields
.field public static final p:[I


# instance fields
.field private i:LX/E8s;

.field private j:LX/E8t;

.field public k:Landroid/view/View;

.field public l:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field public m:I

.field private n:I

.field private o:I

.field public q:Z

.field public r:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2457304
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->p:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2457300
    invoke-direct {p0}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;-><init>()V

    .line 2457301
    iput v1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->o:I

    .line 2457302
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->q:Z

    .line 2457303
    iput v1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->r:I

    return-void
.end method

.method public static a(JLjava/lang/String;Ljava/lang/String;Z)Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;
    .locals 4

    .prologue
    .line 2457338
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2457339
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2457340
    const-string v1, "session_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457341
    const-string v1, "profile_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457342
    const-string v1, "fragment_title"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2457343
    const-string v1, "extra_is_inside_page_surface_tab"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2457344
    new-instance v1, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    invoke-direct {v1}, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;-><init>()V

    .line 2457345
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2457346
    return-object v1
.end method

.method private v()V
    .locals 3

    .prologue
    .line 2457352
    iget-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->i:LX/E8s;

    if-nez v0, :cond_2

    .line 2457353
    iget-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->j:LX/E8t;

    if-nez v0, :cond_1

    .line 2457354
    new-instance v0, LX/E8s;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->i:LX/E8s;

    .line 2457355
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->i:LX/E8s;

    const/4 v1, 0x1

    .line 2457356
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2457357
    :cond_0
    :goto_1
    return-void

    .line 2457358
    :cond_1
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->j:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->i:LX/E8s;

    .line 2457359
    iget-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->j:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->i:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    goto :goto_0

    .line 2457360
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->j:LX/E8t;

    if-eqz v0, :cond_0

    .line 2457361
    iget-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->i:LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->j:LX/E8t;

    .line 2457362
    iput-object v1, v0, LX/E8s;->a:Landroid/view/View;

    .line 2457363
    iget-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->j:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->i:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2457364
    iget-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->i:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->requestLayout()V

    goto :goto_1
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2457347
    iget v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->o:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->o:I

    if-le p1, v0, :cond_1

    .line 2457348
    :cond_0
    :goto_0
    return-void

    .line 2457349
    :cond_1
    iput p1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->o:I

    .line 2457350
    iget-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2457351
    iget-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->k:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->o:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2457333
    const-string v0, "reviews_feed"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2457334
    iput p1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->n:I

    .line 2457335
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v0

    .line 2457336
    iget v1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->n:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2457337
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2457330
    iput-object p1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->j:LX/E8t;

    .line 2457331
    invoke-direct {p0}, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->v()V

    .line 2457332
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2457312
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v0

    .line 2457313
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2457314
    invoke-direct {p0}, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->v()V

    .line 2457315
    iget-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->i:LX/E8s;

    if-eqz v0, :cond_0

    .line 2457316
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v0

    .line 2457317
    iget-object v2, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->i:LX/E8s;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 2457318
    :cond_0
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v0

    .line 2457319
    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVerticalScrollBarEnabled(Z)V

    .line 2457320
    invoke-super {p0, p1}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->a(Landroid/view/LayoutInflater;)V

    .line 2457321
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->k:Landroid/view/View;

    .line 2457322
    iget v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->o:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->E_(I)V

    .line 2457323
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v0

    .line 2457324
    iget-object v1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;)V

    .line 2457325
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->m:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 2457326
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v0

    .line 2457327
    new-instance v1, LX/HMY;

    invoke-direct {v1, p0}, LX/HMY;-><init>(Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2457328
    return-void

    :cond_1
    move v0, v1

    .line 2457329
    goto :goto_0
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2457310
    iput-object p1, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->l:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2457311
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2457308
    iget-object v0, p0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->f:LX/EA0;

    invoke-virtual {v0}, LX/EA0;->f()V

    .line 2457309
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2457305
    invoke-super {p0, p1, p2}, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2457306
    iget v0, p0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->n:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->a(I)V

    .line 2457307
    return-void
.end method
