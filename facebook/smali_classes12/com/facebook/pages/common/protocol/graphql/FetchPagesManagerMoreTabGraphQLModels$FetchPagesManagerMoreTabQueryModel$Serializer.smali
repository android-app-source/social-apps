.class public final Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2450375
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;

    new-instance v1, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2450376
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2450377
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2450378
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2450379
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 v4, 0xd

    const/4 v3, 0x0

    .line 2450380
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2450381
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2450382
    if-eqz v2, :cond_0

    .line 2450383
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450384
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2450385
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2450386
    if-eqz v2, :cond_1

    .line 2450387
    const-string v3, "admin_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450388
    invoke-static {v1, v2, p1, p2}, LX/HIA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2450389
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2450390
    if-eqz v2, :cond_2

    .line 2450391
    const-string v3, "commerce_store"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450392
    invoke-static {v1, v2, p1}, LX/HIB;->a(LX/15i;ILX/0nX;)V

    .line 2450393
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2450394
    if-eqz v2, :cond_6

    .line 2450395
    const-string v3, "ig_presence_account_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450396
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2450397
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 2450398
    if-eqz v3, :cond_3

    .line 2450399
    const-string v5, "is_linked"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450400
    invoke-virtual {p1, v3}, LX/0nX;->a(Z)V

    .line 2450401
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2450402
    if-eqz v3, :cond_4

    .line 2450403
    const-string v5, "settings_webview_url"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450404
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2450405
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 2450406
    if-eqz v3, :cond_5

    .line 2450407
    const-string v5, "should_show_settings"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450408
    invoke-virtual {p1, v3}, LX/0nX;->a(Z)V

    .line 2450409
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2450410
    :cond_6
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2450411
    if-eqz v2, :cond_7

    .line 2450412
    const-string v3, "instant_articles_enabled"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450413
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2450414
    :cond_7
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2450415
    if-eqz v2, :cond_9

    .line 2450416
    const-string v3, "menu_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450417
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2450418
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 2450419
    if-eqz v3, :cond_8

    .line 2450420
    const-string v5, "can_edit_menu"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450421
    invoke-virtual {p1, v3}, LX/0nX;->a(Z)V

    .line 2450422
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2450423
    :cond_9
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2450424
    if-eqz v2, :cond_d

    .line 2450425
    const-string v3, "page_call_to_action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450426
    const/4 p0, 0x1

    .line 2450427
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2450428
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2450429
    if-eqz v3, :cond_b

    .line 2450430
    const-string v5, "cta_admin_info"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450431
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2450432
    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5}, LX/15i;->b(II)Z

    move-result v5

    .line 2450433
    if-eqz v5, :cond_a

    .line 2450434
    const-string p2, "can_see_new_cta"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450435
    invoke-virtual {p1, v5}, LX/0nX;->a(Z)V

    .line 2450436
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2450437
    :cond_b
    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result v3

    .line 2450438
    if-eqz v3, :cond_c

    .line 2450439
    const-string v3, "cta_type"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450440
    invoke-virtual {v1, v2, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2450441
    :cond_c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2450442
    :cond_d
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2450443
    if-eqz v2, :cond_e

    .line 2450444
    const-string v3, "preferred_merchant_settings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450445
    invoke-static {v1, v2, p1}, LX/HIF;->a(LX/15i;ILX/0nX;)V

    .line 2450446
    :cond_e
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2450447
    if-eqz v2, :cond_f

    .line 2450448
    const-string v3, "should_show_recent_activity_entry_point"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450449
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2450450
    :cond_f
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2450451
    if-eqz v2, :cond_10

    .line 2450452
    const-string v3, "should_show_recent_checkins_entry_point"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450453
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2450454
    :cond_10
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2450455
    if-eqz v2, :cond_11

    .line 2450456
    const-string v3, "should_show_recent_mentions_entry_point"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450457
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2450458
    :cond_11
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2450459
    if-eqz v2, :cond_12

    .line 2450460
    const-string v3, "should_show_recent_shares_entry_point"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450461
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2450462
    :cond_12
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2450463
    if-eqz v2, :cond_13

    .line 2450464
    const-string v3, "should_show_reviews_on_profile"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450465
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2450466
    :cond_13
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2450467
    if-eqz v2, :cond_14

    .line 2450468
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450469
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2450470
    :cond_14
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2450471
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2450472
    check-cast p1, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$Serializer;->a(Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
