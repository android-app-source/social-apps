.class public final Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2449455
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2449456
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2449453
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2449454
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    .line 2449336
    if-nez p1, :cond_0

    .line 2449337
    const/4 v0, 0x0

    .line 2449338
    :goto_0
    return v0

    .line 2449339
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2449340
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2449341
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2449342
    const v1, -0x54cdad6f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2449343
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 2449344
    const v2, -0x1d7035e8

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2449345
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2449346
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2449347
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->p(II)I

    move-result v3

    .line 2449348
    const v4, 0x422f9257    # 43.89291f

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 2449349
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v4

    .line 2449350
    const v5, -0x18e5c62

    invoke-static {p0, v4, v5, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v4

    .line 2449351
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 2449352
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2449353
    const/4 v6, 0x6

    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v6

    .line 2449354
    const/4 v7, 0x7

    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v7

    .line 2449355
    const/16 v8, 0x8

    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v8

    .line 2449356
    const v9, -0x3c505fb5

    invoke-static {p0, v8, v9, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v8

    .line 2449357
    const/16 v9, 0x9

    invoke-virtual {p0, p1, v9}, LX/15i;->p(II)I

    move-result v9

    .line 2449358
    const v10, 0x6a78c48a

    invoke-static {p0, v9, v10, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2449359
    const/16 v10, 0xa

    invoke-virtual {p3, v10}, LX/186;->c(I)V

    .line 2449360
    const/4 v10, 0x0

    invoke-virtual {p3, v10, v0}, LX/186;->b(II)V

    .line 2449361
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2449362
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 2449363
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 2449364
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 2449365
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->b(II)V

    .line 2449366
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v6}, LX/186;->a(IZ)V

    .line 2449367
    const/4 v0, 0x7

    invoke-virtual {p3, v0, v7}, LX/186;->a(IZ)V

    .line 2449368
    const/16 v0, 0x8

    invoke-virtual {p3, v0, v8}, LX/186;->b(II)V

    .line 2449369
    const/16 v0, 0x9

    invoke-virtual {p3, v0, v9}, LX/186;->b(II)V

    .line 2449370
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2449371
    :sswitch_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2449372
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2449373
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v0, v2}, LX/186;->a(III)V

    .line 2449374
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2449375
    :sswitch_2
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2449376
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2449377
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v0, v2}, LX/186;->a(III)V

    .line 2449378
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2449379
    :sswitch_3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2449380
    const v1, -0xa168aab

    const/4 v3, 0x0

    .line 2449381
    if-nez v0, :cond_1

    move v2, v3

    .line 2449382
    :goto_1
    move v0, v2

    .line 2449383
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2449384
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2449385
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2449386
    :sswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v0

    .line 2449387
    invoke-virtual {p3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2449388
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2449389
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2449390
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2449391
    :sswitch_5
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    move-result-object v0

    .line 2449392
    invoke-virtual {p3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2449393
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2449394
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2449395
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v4

    .line 2449396
    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v2, v3}, LX/15i;->a(III)I

    move-result v5

    .line 2449397
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v2, v3}, LX/15i;->a(III)I

    move-result v6

    .line 2449398
    const/4 v2, 0x5

    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLAdproLimitResetPeriod;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAdproLimitResetPeriod;

    move-result-object v2

    .line 2449399
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 2449400
    const/4 v2, 0x6

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2449401
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2449402
    const/4 v2, 0x7

    const-wide/16 v10, 0x0

    invoke-virtual {p0, p1, v2, v10, v11}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2449403
    const/16 v9, 0x8

    const-wide/16 v10, 0x0

    invoke-virtual {p0, p1, v9, v10, v11}, LX/15i;->a(IIJ)J

    move-result-wide v10

    .line 2449404
    const/16 v9, 0x9

    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2449405
    const/4 v9, 0x0

    invoke-virtual {p3, v9, v0}, LX/186;->b(II)V

    .line 2449406
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2449407
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v4}, LX/186;->a(IZ)V

    .line 2449408
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v5, v1}, LX/186;->a(III)V

    .line 2449409
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v6, v1}, LX/186;->a(III)V

    .line 2449410
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v7}, LX/186;->b(II)V

    .line 2449411
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v8}, LX/186;->b(II)V

    .line 2449412
    const/4 v1, 0x7

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2449413
    const/16 v1, 0x8

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2449414
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2449415
    :sswitch_6
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2449416
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2449417
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v0, v2}, LX/186;->a(III)V

    .line 2449418
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2449419
    :sswitch_7
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2449420
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(III)I

    move-result v1

    .line 2449421
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2449422
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v0, v3}, LX/186;->a(III)V

    .line 2449423
    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {p3, v0, v1, v2}, LX/186;->a(III)V

    .line 2449424
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2449425
    :sswitch_8
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    move-result-object v0

    .line 2449426
    invoke-virtual {p3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2449427
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(III)I

    move-result v1

    .line 2449428
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2449429
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 2449430
    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {p3, v0, v1, v2}, LX/186;->a(III)V

    .line 2449431
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2449432
    :sswitch_9
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2449433
    const v1, -0xb257049

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2449434
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v1

    .line 2449435
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2449436
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2449437
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 2449438
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2449439
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2449440
    :sswitch_a
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2449441
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2449442
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2449443
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2449444
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2449445
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    .line 2449446
    if-nez v4, :cond_2

    const/4 v2, 0x0

    .line 2449447
    :goto_2
    if-ge v3, v4, :cond_3

    .line 2449448
    invoke-virtual {p0, v0, v3}, LX/15i;->q(II)I

    move-result v5

    .line 2449449
    invoke-static {p0, v5, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v5

    aput v5, v2, v3

    .line 2449450
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2449451
    :cond_2
    new-array v2, v4, [I

    goto :goto_2

    .line 2449452
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p3, v2, v3}, LX/186;->a([IZ)I

    move-result v2

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x61818962 -> :sswitch_0
        -0x54cdad6f -> :sswitch_1
        -0x3c505fb5 -> :sswitch_6
        -0x1d7035e8 -> :sswitch_2
        -0xb257049 -> :sswitch_a
        -0xa168aab -> :sswitch_4
        -0x18e5c62 -> :sswitch_5
        0x31ead959 -> :sswitch_8
        0x422f9257 -> :sswitch_3
        0x464909b1 -> :sswitch_9
        0x6a78c48a -> :sswitch_7
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2449327
    if-nez p0, :cond_0

    move v0, v1

    .line 2449328
    :goto_0
    return v0

    .line 2449329
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2449330
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2449331
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2449332
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2449333
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2449334
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2449335
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2449320
    const/4 v7, 0x0

    .line 2449321
    const/4 v1, 0x0

    .line 2449322
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2449323
    invoke-static {v2, v3, v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2449324
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2449325
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2449326
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2449319
    new-instance v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2449293
    sparse-switch p2, :sswitch_data_0

    .line 2449294
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2449295
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2449296
    const v1, -0x54cdad6f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2449297
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2449298
    const v1, -0x1d7035e8

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2449299
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2449300
    const v1, 0x422f9257    # 43.89291f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2449301
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2449302
    const v1, -0x18e5c62

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2449303
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2449304
    const v1, -0x3c505fb5

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2449305
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2449306
    const v1, 0x6a78c48a

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2449307
    :goto_0
    :sswitch_1
    return-void

    .line 2449308
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2449309
    const v1, -0xa168aab

    .line 2449310
    if-eqz v0, :cond_0

    .line 2449311
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2449312
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 2449313
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2449314
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2449315
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2449316
    :cond_0
    goto :goto_0

    .line 2449317
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2449318
    const v1, -0xb257049

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x61818962 -> :sswitch_0
        -0x54cdad6f -> :sswitch_1
        -0x3c505fb5 -> :sswitch_1
        -0x1d7035e8 -> :sswitch_1
        -0xb257049 -> :sswitch_1
        -0xa168aab -> :sswitch_1
        -0x18e5c62 -> :sswitch_1
        0x31ead959 -> :sswitch_1
        0x422f9257 -> :sswitch_2
        0x464909b1 -> :sswitch_3
        0x6a78c48a -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2449287
    if-eqz p1, :cond_0

    .line 2449288
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2449289
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;

    .line 2449290
    if-eq v0, v1, :cond_0

    .line 2449291
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2449292
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2449286
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2449457
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2449458
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2449281
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2449282
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2449283
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2449284
    iput p2, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->b:I

    .line 2449285
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2449280
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2449279
    new-instance v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2449276
    iget v0, p0, LX/1vt;->c:I

    .line 2449277
    move v0, v0

    .line 2449278
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2449273
    iget v0, p0, LX/1vt;->c:I

    .line 2449274
    move v0, v0

    .line 2449275
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2449270
    iget v0, p0, LX/1vt;->b:I

    .line 2449271
    move v0, v0

    .line 2449272
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2449267
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2449268
    move-object v0, v0

    .line 2449269
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2449258
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2449259
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2449260
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2449261
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2449262
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2449263
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2449264
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2449265
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2449266
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2449255
    iget v0, p0, LX/1vt;->c:I

    .line 2449256
    move v0, v0

    .line 2449257
    return v0
.end method
