.class public final Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4acae2bb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I

.field private l:Z

.field private m:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2449619
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2449620
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2449621
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2449622
    return-void
.end method

.method private q()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2449623
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2449624
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2449625
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2449669
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->j:Ljava/lang/String;

    .line 2449670
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->j:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 2449626
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2449627
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->q()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2449628
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x61818962

    invoke-static {v2, v1, v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2449629
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->k()LX/2uF;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v2

    .line 2449630
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x13b41b9

    invoke-static {v4, v3, v5}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2449631
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->m()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x706c071e

    invoke-static {v5, v4, v6}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2449632
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->r()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2449633
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->o()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, 0x464909b1

    invoke-static {v7, v6, v8}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2449634
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->p()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, -0x11f52a1d

    invoke-static {v8, v7, v9}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2449635
    const/16 v8, 0xf

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2449636
    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 2449637
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2449638
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2449639
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2449640
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2449641
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2449642
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->k:I

    invoke-virtual {p1, v0, v1, v10}, LX/186;->a(III)V

    .line 2449643
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2449644
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2449645
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2449646
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->o:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2449647
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->p:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2449648
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->q:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2449649
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->r:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2449650
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->s:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2449651
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2449652
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2449570
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2449571
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 2449572
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x61818962

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2449573
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2449574
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    .line 2449575
    iput v3, v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->f:I

    .line 2449576
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->k()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2449577
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->k()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2449578
    if-eqz v1, :cond_0

    .line 2449579
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    .line 2449580
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->g:LX/3Sb;

    :cond_0
    move-object v1, v0

    .line 2449581
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2449582
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x13b41b9

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2449583
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2449584
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    .line 2449585
    iput v3, v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->h:I

    move-object v1, v0

    .line 2449586
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2449587
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x706c071e

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2449588
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2449589
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    .line 2449590
    iput v3, v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->i:I

    move-object v1, v0

    .line 2449591
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2449592
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x464909b1

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2449593
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2449594
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    .line 2449595
    iput v3, v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->m:I

    move-object v1, v0

    .line 2449596
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2449597
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->p()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x11f52a1d

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 2449598
    invoke-virtual {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2449599
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    .line 2449600
    iput v3, v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->n:I

    move-object v1, v0

    .line 2449601
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2449602
    if-nez v1, :cond_5

    :goto_1
    return-object p0

    .line 2449603
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    .line 2449604
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 2449605
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    .line 2449606
    :catchall_3
    move-exception v0

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v0

    .line 2449607
    :catchall_4
    move-exception v0

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0

    :cond_5
    move-object p0, v1

    .line 2449608
    goto :goto_1

    :cond_6
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2449653
    new-instance v0, LX/HHv;

    invoke-direct {v0, p1}, LX/HHv;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2449654
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2449655
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2449656
    const/4 v0, 0x1

    const v1, -0x61818962

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->f:I

    .line 2449657
    const/4 v0, 0x3

    const v1, -0x13b41b9

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->h:I

    .line 2449658
    const/4 v0, 0x4

    const v1, 0x706c071e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->i:I

    .line 2449659
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->k:I

    .line 2449660
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l:Z

    .line 2449661
    const/16 v0, 0x8

    const v1, 0x464909b1

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->m:I

    .line 2449662
    const/16 v0, 0x9

    const v1, -0x11f52a1d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->n:I

    .line 2449663
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->o:Z

    .line 2449664
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->p:Z

    .line 2449665
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->q:Z

    .line 2449666
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->r:Z

    .line 2449667
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->s:Z

    .line 2449668
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2449616
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2449617
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2449618
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2449613
    new-instance v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;-><init>()V

    .line 2449614
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2449615
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2449612
    const v0, -0x24d701f9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2449611
    const v0, 0x252222

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActivityAdminInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2449609
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2449610
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActivityFeeds"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2449568
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->g:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x2

    const v4, 0x31ead959

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->g:LX/3Sb;

    .line 2449569
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->g:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2449566
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2449567
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommsHubConfig"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2449564
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2449565
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 2449562
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2449563
    iget v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->k:I

    return v0
.end method

.method public final o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageCallToAction"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2449560
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2449561
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->m:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final p()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageLikers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2449558
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2449559
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->n:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
