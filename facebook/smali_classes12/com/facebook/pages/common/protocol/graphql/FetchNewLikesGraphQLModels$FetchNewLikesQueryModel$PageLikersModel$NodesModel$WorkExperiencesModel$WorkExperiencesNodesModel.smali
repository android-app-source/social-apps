.class public final Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6886653c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2448818
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2448817
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2448793
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2448794
    return-void
.end method

.method private a()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2448815
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;

    .line 2448816
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;

    return-object v0
.end method

.method private j()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2448813
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->f:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->f:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;

    .line 2448814
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->f:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2448819
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2448820
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2448821
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->j()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2448822
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2448823
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2448824
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2448825
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2448826
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2448800
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2448801
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2448802
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;

    .line 2448803
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2448804
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;

    .line 2448805
    iput-object v0, v1, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$EmployerModel;

    .line 2448806
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->j()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2448807
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->j()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;

    .line 2448808
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->j()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2448809
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;

    .line 2448810
    iput-object v0, v1, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;->f:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel$PositionModel;

    .line 2448811
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2448812
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2448797
    new-instance v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$WorkExperiencesModel$WorkExperiencesNodesModel;-><init>()V

    .line 2448798
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2448799
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2448796
    const v0, -0x199bd12c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2448795
    const v0, 0x4799e77b

    return v0
.end method
