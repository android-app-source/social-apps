.class public final Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xe88cd1b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2450567
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2450564
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2450565
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2450566
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2450568
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2450569
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2450570
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2450573
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2450574
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private k()Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2450571
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->g:Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->g:Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;

    .line 2450572
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->g:Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getIgPresenceAccountInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2450577
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2450578
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMenuInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2450575
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2450576
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageCallToAction"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2450560
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2450561
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->k:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private o()Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2450562
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->l:Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->l:Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;

    .line 2450563
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->l:Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;

    return-object v0
.end method

.method private p()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2450473
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->r:Ljava/util/List;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->r:Ljava/util/List;

    .line 2450474
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->r:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 2450475
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2450476
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2450477
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x1c373072

    invoke-static {v2, v1, v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2450478
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->k()Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2450479
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x3e12ac61

    invoke-static {v4, v3, v5}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2450480
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->m()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x66c75337

    invoke-static {v5, v4, v6}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2450481
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->n()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, -0x2ceb2c28

    invoke-static {v6, v5, v7}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2450482
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->o()Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2450483
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->p()LX/0Px;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/util/List;)I

    move-result v7

    .line 2450484
    const/16 v8, 0xe

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2450485
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 2450486
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2450487
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2450488
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2450489
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2450490
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2450491
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2450492
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2450493
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->m:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2450494
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->n:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2450495
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->o:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2450496
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->p:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2450497
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->q:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2450498
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2450499
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2450500
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2450501
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2450502
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2450503
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x1c373072

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2450504
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2450505
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;

    .line 2450506
    iput v3, v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->f:I

    move-object v1, v0

    .line 2450507
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->k()Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2450508
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->k()Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;

    .line 2450509
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->k()Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2450510
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;

    .line 2450511
    iput-object v0, v1, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->g:Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$CommerceStoreModel;

    .line 2450512
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2450513
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3e12ac61

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2450514
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2450515
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;

    .line 2450516
    iput v3, v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->h:I

    move-object v1, v0

    .line 2450517
    :cond_2
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2450518
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x66c75337

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2450519
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2450520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;

    .line 2450521
    iput v3, v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->j:I

    move-object v1, v0

    .line 2450522
    :cond_3
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2450523
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2ceb2c28

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2450524
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2450525
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;

    .line 2450526
    iput v3, v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->k:I

    move-object v1, v0

    .line 2450527
    :cond_4
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->o()Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2450528
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->o()Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;

    .line 2450529
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->o()Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2450530
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;

    .line 2450531
    iput-object v0, v1, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->l:Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel$PreferredMerchantSettingsModel;

    .line 2450532
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2450533
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    .line 2450534
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 2450535
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 2450536
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 2450537
    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    :cond_6
    move-object p0, v1

    .line 2450538
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2450539
    new-instance v0, LX/HI8;

    invoke-direct {v0, p1}, LX/HI8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2450540
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2450541
    const/4 v0, 0x1

    const v1, 0x1c373072

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->f:I

    .line 2450542
    const/4 v0, 0x3

    const v1, 0x3e12ac61

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->h:I

    .line 2450543
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->i:Z

    .line 2450544
    const/4 v0, 0x5

    const v1, 0x66c75337

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->j:I

    .line 2450545
    const/4 v0, 0x6

    const v1, -0x2ceb2c28

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->k:I

    .line 2450546
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->m:Z

    .line 2450547
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->n:Z

    .line 2450548
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->o:Z

    .line 2450549
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->p:Z

    .line 2450550
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;->q:Z

    .line 2450551
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2450552
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2450553
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2450554
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2450557
    new-instance v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$FetchPagesManagerMoreTabQueryModel;-><init>()V

    .line 2450558
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2450559
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2450555
    const v0, 0x6f2c939f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2450556
    const v0, 0x252222

    return v0
.end method
