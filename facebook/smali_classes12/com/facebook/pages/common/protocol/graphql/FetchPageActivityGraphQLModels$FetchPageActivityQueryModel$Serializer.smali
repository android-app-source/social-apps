.class public final Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2449474
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    new-instance v1, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2449475
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2449557
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2449477
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2449478
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x0

    .line 2449479
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2449480
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2449481
    if-eqz v2, :cond_0

    .line 2449482
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449483
    invoke-static {v1, v0, v4, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2449484
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2449485
    if-eqz v2, :cond_1

    .line 2449486
    const-string v3, "activity_admin_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449487
    invoke-static {v1, v2, p1, p2}, LX/HI3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2449488
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2449489
    if-eqz v2, :cond_2

    .line 2449490
    const-string v3, "activity_feeds"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449491
    invoke-static {v1, v2, p1, p2}, LX/HI4;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2449492
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2449493
    if-eqz v2, :cond_3

    .line 2449494
    const-string v3, "admin_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449495
    invoke-static {v1, v2, p1, p2}, LX/HRI;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2449496
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2449497
    if-eqz v2, :cond_4

    .line 2449498
    const-string v3, "comms_hub_config"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449499
    invoke-static {v1, v2, p1}, LX/HRJ;->a(LX/15i;ILX/0nX;)V

    .line 2449500
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2449501
    if-eqz v2, :cond_5

    .line 2449502
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449503
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2449504
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 2449505
    if-eqz v2, :cond_6

    .line 2449506
    const-string v3, "insights_badge_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449507
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 2449508
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2449509
    if-eqz v2, :cond_7

    .line 2449510
    const-string v3, "is_published"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449511
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2449512
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2449513
    if-eqz v2, :cond_b

    .line 2449514
    const-string v3, "page_call_to_action"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449515
    const/4 p0, 0x1

    .line 2449516
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2449517
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2449518
    if-eqz v3, :cond_9

    .line 2449519
    const-string v4, "cta_admin_info"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449520
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2449521
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2449522
    if-eqz v4, :cond_8

    .line 2449523
    const-string p2, "create_prompt"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449524
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2449525
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2449526
    :cond_9
    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result v3

    .line 2449527
    if-eqz v3, :cond_a

    .line 2449528
    const-string v3, "cta_type"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449529
    invoke-virtual {v1, v2, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2449530
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2449531
    :cond_b
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2449532
    if-eqz v2, :cond_c

    .line 2449533
    const-string v3, "page_likers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449534
    invoke-static {v1, v2, p1}, LX/HRK;->a(LX/15i;ILX/0nX;)V

    .line 2449535
    :cond_c
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2449536
    if-eqz v2, :cond_d

    .line 2449537
    const-string v3, "should_show_recent_activity_entry_point"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449538
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2449539
    :cond_d
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2449540
    if-eqz v2, :cond_e

    .line 2449541
    const-string v3, "should_show_recent_checkins_entry_point"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449542
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2449543
    :cond_e
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2449544
    if-eqz v2, :cond_f

    .line 2449545
    const-string v3, "should_show_recent_mentions_entry_point"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449546
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2449547
    :cond_f
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2449548
    if-eqz v2, :cond_10

    .line 2449549
    const-string v3, "should_show_recent_reviews_entry_point"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449550
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2449551
    :cond_10
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2449552
    if-eqz v2, :cond_11

    .line 2449553
    const-string v3, "should_show_recent_shares_entry_point"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2449554
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2449555
    :cond_11
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2449556
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2449476
    check-cast p1, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel$Serializer;->a(Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
