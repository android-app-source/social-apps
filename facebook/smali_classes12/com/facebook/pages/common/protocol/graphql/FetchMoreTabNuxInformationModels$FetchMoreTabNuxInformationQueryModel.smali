.class public final Lcom/facebook/pages/common/protocol/graphql/FetchMoreTabNuxInformationModels$FetchMoreTabNuxInformationQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x26bc4b01
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchMoreTabNuxInformationModels$FetchMoreTabNuxInformationQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchMoreTabNuxInformationModels$FetchMoreTabNuxInformationQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2448447
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchMoreTabNuxInformationModels$FetchMoreTabNuxInformationQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2448448
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchMoreTabNuxInformationModels$FetchMoreTabNuxInformationQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2448428
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2448429
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2448442
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2448443
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2448444
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchMoreTabNuxInformationModels$FetchMoreTabNuxInformationQueryModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2448445
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2448446
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2448439
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2448440
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2448441
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2448438
    new-instance v0, LX/HHa;

    invoke-direct {v0, p1}, LX/HHa;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2448449
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2448450
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchMoreTabNuxInformationModels$FetchMoreTabNuxInformationQueryModel;->e:Z

    .line 2448451
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2448436
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2448437
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2448435
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2448432
    new-instance v0, Lcom/facebook/pages/common/protocol/graphql/FetchMoreTabNuxInformationModels$FetchMoreTabNuxInformationQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchMoreTabNuxInformationModels$FetchMoreTabNuxInformationQueryModel;-><init>()V

    .line 2448433
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2448434
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2448431
    const v0, 0x8bef553

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2448430
    const v0, 0x25d6af

    return v0
.end method
