.class public final Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x74623920
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2448288
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2448287
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2448285
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2448286
    return-void
.end method

.method private a()Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2448283
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;

    .line 2448284
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2448277
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2448278
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2448279
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2448280
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2448281
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2448282
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2448289
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2448290
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2448291
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;

    .line 2448292
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2448293
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;

    .line 2448294
    iput-object v0, v1, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel$AdminInfoModel;

    .line 2448295
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2448296
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2448276
    new-instance v0, LX/HHU;

    invoke-direct {v0, p1}, LX/HHU;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2448274
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2448275
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2448273
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2448270
    new-instance v0, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchDraftPostsGraphQLModels$FetchDraftPostsQueryModel;-><init>()V

    .line 2448271
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2448272
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2448269
    const v0, 0x1de19593

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2448268
    const v0, 0x25d6af

    return v0
.end method
