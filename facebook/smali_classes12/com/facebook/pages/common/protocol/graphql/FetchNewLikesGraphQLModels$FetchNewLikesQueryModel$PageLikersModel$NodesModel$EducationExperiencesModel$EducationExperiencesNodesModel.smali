.class public final Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x627ea903
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2448619
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2448618
    const-class v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2448616
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2448617
    return-void
.end method

.method private a()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2448614
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;

    .line 2448615
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2448595
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2448596
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2448597
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2448598
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2448599
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2448600
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2448606
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2448607
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2448608
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;

    .line 2448609
    invoke-direct {p0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;->a()Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2448610
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;

    .line 2448611
    iput-object v0, v1, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;->e:Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel$SchoolModel;

    .line 2448612
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2448613
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2448603
    new-instance v0, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchNewLikesGraphQLModels$FetchNewLikesQueryModel$PageLikersModel$NodesModel$EducationExperiencesModel$EducationExperiencesNodesModel;-><init>()V

    .line 2448604
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2448605
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2448602
    const v0, -0x1597c96c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2448601
    const v0, 0x4b57f312    # 1.4152466E7f

    return v0
.end method
