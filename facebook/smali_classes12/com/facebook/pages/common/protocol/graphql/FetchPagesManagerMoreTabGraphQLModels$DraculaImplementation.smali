.class public final Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2450277
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2450278
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2450174
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2450175
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2450225
    if-nez p1, :cond_0

    .line 2450226
    :goto_0
    return v0

    .line 2450227
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2450228
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2450229
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2450230
    const v2, -0x5b265d15

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2450231
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 2450232
    const v3, -0x299e59f6

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2450233
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v3

    .line 2450234
    const v4, 0x14d4bfca

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 2450235
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2450236
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2450237
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2450238
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 2450239
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2450240
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2450241
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2450242
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2450243
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2450244
    :sswitch_2
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2450245
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2450246
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2450247
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2450248
    :sswitch_3
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2450249
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2450250
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2450251
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2450252
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2450253
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2450254
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2450255
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v3

    .line 2450256
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2450257
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2450258
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2450259
    invoke-virtual {p3, v6, v3}, LX/186;->a(IZ)V

    .line 2450260
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2450261
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2450262
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2450263
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2450264
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2450265
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2450266
    const v2, 0x6e270674

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2450267
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v2

    .line 2450268
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2450269
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2450270
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2450271
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2450272
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2450273
    :sswitch_7
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2450274
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2450275
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2450276
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5b265d15 -> :sswitch_1
        -0x2ceb2c28 -> :sswitch_6
        -0x299e59f6 -> :sswitch_2
        0x14d4bfca -> :sswitch_3
        0x1c373072 -> :sswitch_0
        0x3e12ac61 -> :sswitch_4
        0x66c75337 -> :sswitch_5
        0x6e270674 -> :sswitch_7
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2450224
    new-instance v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2450213
    sparse-switch p2, :sswitch_data_0

    .line 2450214
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2450215
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2450216
    const v1, -0x5b265d15

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2450217
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2450218
    const v1, -0x299e59f6

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2450219
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2450220
    const v1, 0x14d4bfca

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2450221
    :goto_0
    :sswitch_1
    return-void

    .line 2450222
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2450223
    const v1, 0x6e270674

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x5b265d15 -> :sswitch_1
        -0x2ceb2c28 -> :sswitch_2
        -0x299e59f6 -> :sswitch_1
        0x14d4bfca -> :sswitch_1
        0x1c373072 -> :sswitch_0
        0x3e12ac61 -> :sswitch_1
        0x66c75337 -> :sswitch_1
        0x6e270674 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2450207
    if-eqz p1, :cond_0

    .line 2450208
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2450209
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    .line 2450210
    if-eq v0, v1, :cond_0

    .line 2450211
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2450212
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2450206
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2450204
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2450205
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2450199
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2450200
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2450201
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2450202
    iput p2, p0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->b:I

    .line 2450203
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2450198
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2450197
    new-instance v0, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2450194
    iget v0, p0, LX/1vt;->c:I

    .line 2450195
    move v0, v0

    .line 2450196
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2450191
    iget v0, p0, LX/1vt;->c:I

    .line 2450192
    move v0, v0

    .line 2450193
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2450188
    iget v0, p0, LX/1vt;->b:I

    .line 2450189
    move v0, v0

    .line 2450190
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2450185
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2450186
    move-object v0, v0

    .line 2450187
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2450176
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2450177
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2450178
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2450179
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2450180
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2450181
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2450182
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2450183
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/protocol/graphql/FetchPagesManagerMoreTabGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2450184
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2450171
    iget v0, p0, LX/1vt;->c:I

    .line 2450172
    move v0, v0

    .line 2450173
    return v0
.end method
