.class public Lcom/facebook/pages/common/protocol/methods/PageIdParam;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/common/protocol/methods/PageIdParam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2450874
    new-instance v0, LX/HII;

    invoke-direct {v0}, LX/HII;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/protocol/methods/PageIdParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2450878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2450879
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/protocol/methods/PageIdParam;->a:Ljava/lang/String;

    .line 2450880
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2450877
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2450875
    iget-object v0, p0, Lcom/facebook/pages/common/protocol/methods/PageIdParam;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2450876
    return-void
.end method
