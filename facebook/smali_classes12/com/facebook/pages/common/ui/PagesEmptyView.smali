.class public Lcom/facebook/pages/common/ui/PagesEmptyView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;

.field private b:Lcom/facebook/resources/ui/FbButton;

.field private c:Landroid/view/ViewStub;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2464109
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2464110
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/PagesEmptyView;->a()V

    .line 2464111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2464106
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2464107
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/PagesEmptyView;->a()V

    .line 2464108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2464103
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2464104
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/PagesEmptyView;->a()V

    .line 2464105
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2464093
    const v0, 0x7f030ecf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2464094
    invoke-virtual {p0, v1}, Lcom/facebook/pages/common/ui/PagesEmptyView;->setGravity(I)V

    .line 2464095
    invoke-virtual {p0, v1}, Lcom/facebook/pages/common/ui/PagesEmptyView;->setOrientation(I)V

    .line 2464096
    const v0, 0x7f0a010a

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/ui/PagesEmptyView;->setBackgroundResource(I)V

    .line 2464097
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/PagesEmptyView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0e27

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2464098
    invoke-virtual {p0, v0, v2, v0, v2}, Lcom/facebook/pages/common/ui/PagesEmptyView;->setPadding(IIII)V

    .line 2464099
    const v0, 0x7f0d2425

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/ui/PagesEmptyView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2464100
    const v0, 0x7f0d2426

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/pages/common/ui/PagesEmptyView;->b:Lcom/facebook/resources/ui/FbButton;

    .line 2464101
    const v0, 0x7f0d2098

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/pages/common/ui/PagesEmptyView;->c:Landroid/view/ViewStub;

    .line 2464102
    return-void
.end method


# virtual methods
.method public setImageResource(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2464079
    iget-object v0, p0, Lcom/facebook/pages/common/ui/PagesEmptyView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1, p1, v1, v1}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2464080
    iget-object v0, p0, Lcom/facebook/pages/common/ui/PagesEmptyView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2464081
    return-void
.end method

.method public setMessage(I)V
    .locals 2

    .prologue
    .line 2464090
    iget-object v0, p0, Lcom/facebook/pages/common/ui/PagesEmptyView;->a:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2464091
    iget-object v0, p0, Lcom/facebook/pages/common/ui/PagesEmptyView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2464092
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2464087
    iget-object v0, p0, Lcom/facebook/pages/common/ui/PagesEmptyView;->a:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2464088
    iget-object v0, p0, Lcom/facebook/pages/common/ui/PagesEmptyView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2464089
    return-void
.end method

.method public setShowProgress(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2464082
    iget-object v3, p0, Lcom/facebook/pages/common/ui/PagesEmptyView;->c:Landroid/view/ViewStub;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 2464083
    iget-object v0, p0, Lcom/facebook/pages/common/ui/PagesEmptyView;->a:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2464084
    return-void

    :cond_0
    move v0, v2

    .line 2464085
    goto :goto_0

    :cond_1
    move v2, v1

    .line 2464086
    goto :goto_1
.end method
