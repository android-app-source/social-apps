.class public Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:F

.field private b:F

.field public c:Z

.field private d:Z

.field public e:Z

.field private final f:Z

.field private final g:Z

.field private h:I

.field public i:I

.field private j:I

.field private k:I

.field private l:I

.field private final m:Landroid/graphics/Rect;

.field private n:LX/HR5;

.field private o:Landroid/util/SparseIntArray;

.field private p:Landroid/util/SparseIntArray;

.field private final q:Landroid/view/animation/Animation$AnimationListener;

.field private final r:Landroid/view/animation/Animation$AnimationListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2464324
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2464325
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2464322
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2464323
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2464296
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2464297
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->a:F

    .line 2464298
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->b:F

    .line 2464299
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2464300
    sget-object v1, LX/03r;->ExpandableText:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2464301
    const/16 v2, 0x1

    const v3, 0x7f0a056b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->k:I

    .line 2464302
    const/16 v2, 0x0

    const v3, 0x7f0b0e41

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->j:I

    .line 2464303
    const/16 v0, 0x2

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->g:Z

    .line 2464304
    const/16 v0, 0x3

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->e:Z

    .line 2464305
    const/16 v0, 0x4

    const v2, 0x7fffffff

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->i:I

    .line 2464306
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2464307
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->i:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setMaxLines(I)V

    .line 2464308
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->d:Z

    .line 2464309
    iput-boolean v4, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c:Z

    .line 2464310
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iput-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->f:Z

    .line 2464311
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->m:Landroid/graphics/Rect;

    .line 2464312
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->p:Landroid/util/SparseIntArray;

    .line 2464313
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->o:Landroid/util/SparseIntArray;

    .line 2464314
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->l:I

    .line 2464315
    iget-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->g:Z

    if-eqz v0, :cond_0

    .line 2464316
    new-instance v0, LX/HR1;

    invoke-direct {v0, p0}, LX/HR1;-><init>(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->q:Landroid/view/animation/Animation$AnimationListener;

    .line 2464317
    new-instance v0, LX/HR2;

    invoke-direct {v0, p0}, LX/HR2;-><init>(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->r:Landroid/view/animation/Animation$AnimationListener;

    .line 2464318
    :goto_0
    invoke-virtual {p0, v4}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setFocusable(Z)V

    .line 2464319
    return-void

    .line 2464320
    :cond_0
    iput-object v5, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->q:Landroid/view/animation/Animation$AnimationListener;

    .line 2464321
    iput-object v5, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->r:Landroid/view/animation/Animation$AnimationListener;

    goto :goto_0
.end method

.method private b()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x12c

    .line 2464277
    iget-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c:Z

    if-nez v0, :cond_2

    .line 2464278
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->h:I

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->i:I

    if-ge v0, v1, :cond_0

    .line 2464279
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->h:I

    iput v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->i:I

    .line 2464280
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getExpandedStateHeight()I

    move-result v0

    .line 2464281
    new-instance v1, LX/HR4;

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getHeight()I

    move-result v2

    invoke-direct {v1, p0, p0, v2, v0}, LX/HR4;-><init>(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;Landroid/view/View;II)V

    .line 2464282
    invoke-virtual {v1, v4, v5}, LX/HR4;->setDuration(J)V

    .line 2464283
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->q:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v0}, LX/HR4;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2464284
    invoke-virtual {p0, v1}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2464285
    :cond_1
    :goto_0
    return-void

    .line 2464286
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->e:Z

    move v0, v0

    .line 2464287
    if-eqz v0, :cond_1

    .line 2464288
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->m:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2464289
    iget-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->f:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_3

    .line 2464290
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    neg-int v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F_(I)V

    .line 2464291
    :cond_3
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getCollapsedStateHeight()I

    move-result v0

    .line 2464292
    new-instance v1, LX/HR3;

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getHeight()I

    move-result v2

    invoke-direct {v1, p0, p0, v2, v0}, LX/HR3;-><init>(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;Landroid/view/View;II)V

    .line 2464293
    invoke-virtual {v1, v4, v5}, LX/HR3;->setDuration(J)V

    .line 2464294
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->r:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v1, v0}, LX/HR3;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2464295
    invoke-virtual {p0, v1}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2464259
    iget-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c:Z

    if-nez v0, :cond_2

    .line 2464260
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->h:I

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->i:I

    if-ge v0, v1, :cond_0

    .line 2464261
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->h:I

    iput v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->i:I

    .line 2464262
    :cond_0
    const/4 v0, 0x1

    .line 2464263
    iput-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c:Z

    .line 2464264
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setMaxLines(I)V

    .line 2464265
    invoke-static {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->d(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;)V

    .line 2464266
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->n:LX/HR5;

    if-eqz v0, :cond_1

    .line 2464267
    :cond_1
    :goto_0
    return-void

    .line 2464268
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->e:Z

    move v0, v0

    .line 2464269
    if-eqz v0, :cond_1

    .line 2464270
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->m:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2464271
    iget-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->f:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->m:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-lez v0, :cond_3

    .line 2464272
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->m:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    neg-int v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F_(I)V

    .line 2464273
    :cond_3
    const/4 v0, 0x0

    .line 2464274
    iput-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c:Z

    .line 2464275
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->i:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setMaxLines(I)V

    .line 2464276
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getCollapsedStateHeight()I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setFadingGradient(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;I)V

    goto :goto_0
.end method

.method public static d(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;)V
    .locals 2

    .prologue
    .line 2464201
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2464202
    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    .line 2464254
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->p:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->l:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->o:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->l:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v0

    if-lez v0, :cond_0

    .line 2464255
    :goto_0
    return-void

    .line 2464256
    :cond_0
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getCompoundPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getCompoundPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v5, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->a:F

    iget v6, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->b:F

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 2464257
    iget-object v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->o:Landroid/util/SparseIntArray;

    iget v2, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->l:I

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getCompoundPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getCompoundPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 2464258
    iget-object v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->p:Landroid/util/SparseIntArray;

    iget v2, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->l:I

    iget v3, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->i:I

    invoke-virtual {v0, v3}, Landroid/text/StaticLayout;->getLineTop(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getCompoundPaddingTop()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getCompoundPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0
.end method

.method private getCollapsedStateHeight()I
    .locals 2

    .prologue
    .line 2464252
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->f()V

    .line 2464253
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->p:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->l:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    return v0
.end method

.method private getExpandedStateHeight()I
    .locals 2

    .prologue
    .line 2464250
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->f()V

    .line 2464251
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->o:Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->l:I

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    return v0
.end method

.method public static setFadingGradient(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2464247
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v2, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->j:I

    sub-int v2, p1, v2

    int-to-float v2, v2

    int-to-float v4, p1

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getCurrentTextColor()I

    move-result v5

    iget v6, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->k:I

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 2464248
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2464249
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2464244
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2464245
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setFocusable(Z)V

    .line 2464246
    return-void
.end method

.method public getGradientEndColor()I
    .locals 1

    .prologue
    .line 2464243
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->k:I

    return v0
.end method

.method public getGradientLength()I
    .locals 1

    .prologue
    .line 2464242
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->j:I

    return v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 2464238
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->l:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    .line 2464239
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->l:I

    .line 2464240
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2464241
    :cond_0
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2464226
    iget-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->d:Z

    if-eqz v0, :cond_1

    .line 2464227
    iget-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getLineCount()I

    move-result v0

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->h:I

    if-gt v0, v1, :cond_2

    .line 2464228
    const/4 v0, 0x1

    .line 2464229
    iput-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c:Z

    .line 2464230
    iput-boolean v2, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->e:Z

    .line 2464231
    invoke-static {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->d(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;)V

    .line 2464232
    :cond_0
    :goto_0
    iput-boolean v2, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->d:Z

    .line 2464233
    :cond_1
    invoke-super/range {p0 .. p5}, Lcom/facebook/resources/ui/FbTextView;->onLayout(ZIIII)V

    .line 2464234
    return-void

    .line 2464235
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getLineCount()I

    move-result v0

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->h:I

    if-le v0, v1, :cond_0

    .line 2464236
    iput-boolean v2, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c:Z

    .line 2464237
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getHeight()I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setFadingGradient(Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;I)V

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2464217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->d:Z

    .line 2464218
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2464219
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2464220
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->o:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_1

    .line 2464221
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->o:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 2464222
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->p:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_2

    .line 2464223
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->p:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 2464224
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/resources/ui/FbTextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 2464225
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    const v0, -0x36b3af3b

    invoke-static {v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2464207
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 2464208
    const v0, 0x6a11d163

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2464209
    :goto_0
    return v3

    .line 2464210
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_1

    .line 2464211
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Spannable;

    invoke-interface {v2, p0, v0, p1}, Landroid/text/method/MovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2464212
    const v0, 0x68895ede

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2464213
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->g:Z

    if-eqz v0, :cond_2

    .line 2464214
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->b()V

    .line 2464215
    :goto_1
    const v0, -0x32822fae

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2464216
    :cond_2
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c()V

    goto :goto_1
.end method

.method public setCanCollapse(Z)V
    .locals 0

    .prologue
    .line 2464205
    iput-boolean p1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->e:Z

    .line 2464206
    return-void
.end method

.method public setGradientEndColor(I)V
    .locals 0

    .prologue
    .line 2464203
    iput p1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->k:I

    .line 2464204
    return-void
.end method

.method public setGradientLength(I)V
    .locals 0

    .prologue
    .line 2464199
    iput p1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->j:I

    .line 2464200
    return-void
.end method

.method public setIsExpanded(Z)V
    .locals 0

    .prologue
    .line 2464197
    iput-boolean p1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->c:Z

    .line 2464198
    return-void
.end method

.method public final setLineSpacing(FF)V
    .locals 0

    .prologue
    .line 2464193
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setLineSpacing(FF)V

    .line 2464194
    iput p2, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->a:F

    .line 2464195
    iput p1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->b:F

    .line 2464196
    return-void
.end method

.method public setMaxLines(I)V
    .locals 0

    .prologue
    .line 2464190
    iput p1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->h:I

    .line 2464191
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 2464192
    return-void
.end method

.method public setOnExpandCollapseListener(LX/HR5;)V
    .locals 0

    .prologue
    .line 2464188
    iput-object p1, p0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->n:LX/HR5;

    .line 2464189
    return-void
.end method
