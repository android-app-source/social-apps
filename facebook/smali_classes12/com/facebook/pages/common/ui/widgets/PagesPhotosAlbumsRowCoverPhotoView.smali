.class public Lcom/facebook/pages/common/ui/widgets/PagesPhotosAlbumsRowCoverPhotoView;
.super Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2464332
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/ui/widgets/PagesPhotosAlbumsRowCoverPhotoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2464333
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2464334
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/ui/widgets/PagesPhotosAlbumsRowCoverPhotoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2464335
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2464329
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2464330
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/ui/widgets/PagesPhotosAlbumsRowCoverPhotoView;->a(Landroid/content/Context;)V

    .line 2464331
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2464326
    const v0, 0x7f020089

    invoke-static {p1, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2464327
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2464328
    return-void
.end method
