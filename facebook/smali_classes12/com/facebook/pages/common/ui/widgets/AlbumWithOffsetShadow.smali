.class public Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# instance fields
.field private c:I

.field private d:I

.field private e:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2464150
    invoke-direct {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2464151
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->c()V

    .line 2464152
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1af;)V
    .locals 0

    .prologue
    .line 2464147
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;LX/1af;)V

    .line 2464148
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->c()V

    .line 2464149
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2464127
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2464128
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->c()V

    .line 2464129
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2464144
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2464145
    invoke-direct {p0}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->c()V

    .line 2464146
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2464153
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0e3d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->c:I

    .line 2464154
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->d:I

    .line 2464155
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2464140
    iput-object p1, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->e:Landroid/graphics/drawable/Drawable;

    .line 2464141
    iput p2, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->d:I

    .line 2464142
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->c:I

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->d:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->c:I

    iget v2, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->d:I

    mul-int/2addr v1, v2

    invoke-virtual {p0, v3, v3, v0, v1}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->setPadding(IIII)V

    .line 2464143
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 2464130
    iget-object v0, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 2464131
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 2464132
    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->getPaddingBottom()I

    move-result v2

    sub-int v2, v0, v2

    .line 2464133
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->d:I

    :goto_0
    if-ltz v0, :cond_0

    .line 2464134
    iget v3, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->c:I

    mul-int/2addr v3, v0

    .line 2464135
    iget-object v4, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->e:Landroid/graphics/drawable/Drawable;

    add-int v5, v1, v3

    add-int v6, v2, v3

    invoke-virtual {v4, v3, v3, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2464136
    iget-object v3, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2464137
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2464138
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2464139
    return-void
.end method

.method public setOffset(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2464124
    iput p1, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->c:I

    .line 2464125
    iget v0, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->c:I

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->d:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->c:I

    iget v2, p0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->d:I

    mul-int/2addr v1, v2

    invoke-virtual {p0, v3, v3, v0, v1}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->setPadding(IIII)V

    .line 2464126
    return-void
.end method

.method public setShadow(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 2464122
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2464123
    return-void
.end method
