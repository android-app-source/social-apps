.class public final Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2455996
    const-class v0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;

    new-instance v1, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2455997
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2455995
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2455977
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2455978
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2455979
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2455980
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2455981
    if-eqz v2, :cond_0

    .line 2455982
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2455983
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2455984
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2455985
    if-eqz v2, :cond_1

    .line 2455986
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2455987
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2455988
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2455989
    if-eqz v2, :cond_2

    .line 2455990
    const-string p0, "page_recommendations_based_on_category"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2455991
    invoke-static {v1, v2, p1, p2}, LX/9qw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2455992
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2455993
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2455994
    check-cast p1, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel$Serializer;->a(Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
