.class public final Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32bbe04e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2456175
    const-class v0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2456151
    const-class v0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2456152
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2456153
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2456154
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2456155
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;->a()Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2456156
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2456157
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2456158
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2456159
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2456160
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2456161
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;->a()Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2456162
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;->a()Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;

    .line 2456163
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;->a()Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2456164
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;

    .line 2456165
    iput-object v0, v1, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;->e:Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;

    .line 2456166
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2456167
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2456168
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;->e:Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;->e:Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;

    .line 2456169
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;->e:Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel$ActorModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2456170
    new-instance v0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCityGuidesGraphQLModels$PageFriendsCityActivityConnectedPagesQueryModel;-><init>()V

    .line 2456171
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2456172
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2456173
    const v0, -0x49962f5a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2456174
    const v0, -0x6747e1ce

    return v0
.end method
