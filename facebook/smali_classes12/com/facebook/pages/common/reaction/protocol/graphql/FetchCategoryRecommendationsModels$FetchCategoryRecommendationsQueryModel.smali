.class public final Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x55a5b0fa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2456035
    const-class v0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2455998
    const-class v0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2456033
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2456034
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2456031
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->e:Ljava/lang/String;

    .line 2456032
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2456029
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->f:Ljava/lang/String;

    .line 2456030
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2456027
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->g:Ljava/util/List;

    .line 2456028
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2456017
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2456018
    invoke-direct {p0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2456019
    invoke-direct {p0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2456020
    invoke-direct {p0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->l()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 2456021
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2456022
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2456023
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2456024
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2456025
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2456026
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2456009
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2456010
    invoke-direct {p0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->l()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2456011
    invoke-direct {p0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2456012
    if-eqz v1, :cond_0

    .line 2456013
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;

    .line 2456014
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->g:Ljava/util/List;

    .line 2456015
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2456016
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2456008
    new-instance v0, LX/HLY;

    invoke-direct {v0, p1}, LX/HLY;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2456007
    invoke-direct {p0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2456005
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2456006
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2456004
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2456001
    new-instance v0, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/reaction/protocol/graphql/FetchCategoryRecommendationsModels$FetchCategoryRecommendationsQueryModel;-><init>()V

    .line 2456002
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2456003
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2456000
    const v0, -0x57185471

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2455999
    const v0, 0x25d6af

    return v0
.end method
