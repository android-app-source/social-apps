.class public Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/1nu;

.field public final d:LX/17W;

.field public final e:LX/1vg;

.field public final f:LX/154;

.field public final g:LX/CYK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2451360
    const-class v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1nu;LX/17W;LX/1vg;LX/154;LX/CYK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2451352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2451353
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->b:Landroid/content/Context;

    .line 2451354
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->c:LX/1nu;

    .line 2451355
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->d:LX/17W;

    .line 2451356
    iput-object p4, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->e:LX/1vg;

    .line 2451357
    iput-object p5, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->f:LX/154;

    .line 2451358
    iput-object p6, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->g:LX/CYK;

    .line 2451359
    return-void
.end method

.method public static a(Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;LX/1De;Ljava/lang/String;I)LX/1Di;
    .locals 10

    .prologue
    const/4 v6, 0x1

    .line 2451334
    const v0, 0x7f0816a9

    invoke-virtual {p1, v0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2451335
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f00b5

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2451336
    const/16 v5, 0x3e8

    if-ge p3, v5, :cond_0

    .line 2451337
    const-string v5, "%,d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v7}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2451338
    :goto_0
    move-object v5, v5

    .line 2451339
    aput-object v5, v3, v4

    invoke-virtual {v1, v2, p3, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2451340
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a010d

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v5, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->f:LX/154;

    invoke-virtual {v5, p3}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;
    .locals 10

    .prologue
    .line 2451341
    const-class v1, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;

    monitor-enter v1

    .line 2451342
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2451343
    sput-object v2, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2451344
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2451345
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2451346
    new-instance v3, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v7

    check-cast v7, LX/1vg;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v8

    check-cast v8, LX/154;

    invoke-static {v0}, LX/CYK;->b(LX/0QB;)LX/CYK;

    move-result-object v9

    check-cast v9, LX/CYK;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;-><init>(Landroid/content/Context;LX/1nu;LX/17W;LX/1vg;LX/154;LX/CYK;)V

    .line 2451347
    move-object v0, v3

    .line 2451348
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2451349
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2451350
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2451351
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
