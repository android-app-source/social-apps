.class public Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/HLX;

.field public final c:LX/8yV;

.field public final d:LX/9XE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2452669
    const-class v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/HLX;LX/8yV;LX/9XE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2452671
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->b:LX/HLX;

    .line 2452672
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->c:LX/8yV;

    .line 2452673
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->d:LX/9XE;

    .line 2452674
    return-void
.end method

.method public static a(LX/1Pr;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;)I
    .locals 3
    .param p0    # LX/1Pr;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/HLF;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pr;",
            ">(TE;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/HLF;",
            ")I"
        }
    .end annotation

    .prologue
    .line 2452675
    invoke-interface {p0, p2, p1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HLG;

    .line 2452676
    if-eqz v0, :cond_0

    iget v1, v0, LX/HLG;->f:I

    if-ltz v1, :cond_0

    iget v1, v0, LX/HLG;->f:I

    .line 2452677
    iget-object v2, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2452678
    invoke-interface {v2}, LX/9uc;->aE()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2452679
    iget v0, v0, LX/HLG;->f:I

    .line 2452680
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;
    .locals 8

    .prologue
    .line 2452681
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;

    monitor-enter v1

    .line 2452682
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452683
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452684
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452685
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2452686
    new-instance v6, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;

    .line 2452687
    new-instance p0, LX/HLX;

    invoke-static {v0}, LX/7H7;->a(LX/0QB;)LX/7H7;

    move-result-object v3

    check-cast v3, LX/7H7;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v4

    check-cast v4, LX/0rq;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-direct {p0, v3, v4, v5, v7}, LX/HLX;-><init>(LX/7H7;LX/0rq;LX/0tX;LX/1Ck;)V

    .line 2452688
    move-object v3, p0

    .line 2452689
    check-cast v3, LX/HLX;

    invoke-static {v0}, LX/8yV;->a(LX/0QB;)LX/8yV;

    move-result-object v4

    check-cast v4, LX/8yV;

    invoke-static {v0}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v5

    check-cast v5, LX/9XE;

    invoke-direct {v6, v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;-><init>(LX/HLX;LX/8yV;LX/9XE;)V

    .line 2452690
    move-object v0, v6

    .line 2452691
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452692
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452693
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452694
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public onClick(LX/2km;LX/HL6;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;)V
    .locals 8
    .param p1    # LX/2km;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/HL6;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/HLF;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/HL6;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/HLF;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 2452695
    move-object v0, p1

    check-cast v0, LX/1Pr;

    invoke-static {v0, p3, p4}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->a(LX/1Pr;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HLF;)I

    move-result v0

    iget v1, p2, LX/HL6;->c:I

    if-ne v0, v1, :cond_0

    .line 2452696
    :goto_0
    return-void

    .line 2452697
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->d:LX/9XE;

    .line 2452698
    iget-object v1, p3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2452699
    invoke-interface {v1}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p2, LX/HL6;->a:Ljava/lang/String;

    .line 2452700
    iget-object v4, v0, LX/9XE;->a:LX/0Zb;

    sget-object v5, LX/9XI;->EVENT_TAPPED_CITY_HUB_SOCIAL_MODULE_FACEPILE:LX/9XI;

    invoke-static {v5, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "tapped_friend_id"

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2452701
    move-object v0, p1

    .line 2452702
    check-cast v0, LX/1Pr;

    invoke-interface {v0, p4, p3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HLG;

    .line 2452703
    iput-boolean v7, v0, LX/HLG;->a:Z

    .line 2452704
    iget-object v1, p3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2452705
    iget v2, p2, LX/HL6;->c:I

    .line 2452706
    iput v2, v0, LX/HLG;->f:I

    .line 2452707
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/HLG;->b:Z

    .line 2452708
    invoke-static {v0, v1}, LX/HLG;->b(LX/HLG;Ljava/lang/String;)LX/25L;

    move-result-object v3

    iput-object v3, v0, LX/HLG;->e:LX/25L;

    .line 2452709
    move-object v1, p1

    .line 2452710
    check-cast v1, LX/1Pr;

    invoke-interface {v1, p4, v0}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2452711
    iget-object v6, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;->b:LX/HLX;

    new-instance v0, LX/HJP;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/HJP;-><init>(Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityFacepileComponentSpec;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HL6;LX/2km;LX/HLF;)V

    iget-object v1, p2, LX/HL6;->a:Ljava/lang/String;

    .line 2452712
    iget-object v2, p3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2452713
    invoke-interface {v2}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v2

    .line 2452714
    iget-object v3, v6, LX/HLX;->d:LX/1Ck;

    const-string v4, "fetch_city_guides_friend_connected_pages"

    new-instance v5, LX/HLU;

    invoke-direct {v5, v6, v1, v2}, LX/HLU;-><init>(LX/HLX;Ljava/lang/String;Ljava/lang/String;)V

    new-instance p0, LX/HLV;

    invoke-direct {p0, v6, v0}, LX/HLV;-><init>(LX/HLX;LX/HJP;)V

    invoke-virtual {v3, v4, v5, p0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2452715
    check-cast p1, LX/1Pq;

    new-array v0, v7, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2452716
    iget-object v2, p3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 2452717
    aput-object v2, v0, v1

    invoke-interface {p1, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0
.end method
