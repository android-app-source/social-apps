.class public Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final d:LX/HKt;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/HKt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2455292
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2455293
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentPartDefinition;->d:LX/HKt;

    .line 2455294
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2455276
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentPartDefinition;->d:LX/HKt;

    const/4 v1, 0x0

    .line 2455277
    new-instance v2, LX/HKs;

    invoke-direct {v2, v0}, LX/HKs;-><init>(LX/HKt;)V

    .line 2455278
    iget-object p0, v0, LX/HKt;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HKr;

    .line 2455279
    if-nez p0, :cond_0

    .line 2455280
    new-instance p0, LX/HKr;

    invoke-direct {p0, v0}, LX/HKr;-><init>(LX/HKt;)V

    .line 2455281
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HKr;->a$redex0(LX/HKr;LX/1De;IILX/HKs;)V

    .line 2455282
    move-object v2, p0

    .line 2455283
    move-object v1, v2

    .line 2455284
    move-object v0, v1

    .line 2455285
    iget-object v1, v0, LX/HKr;->a:LX/HKs;

    iput-object p2, v1, LX/HKs;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455286
    iget-object v1, v0, LX/HKr;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2455287
    move-object v0, v0

    .line 2455288
    iget-object v1, v0, LX/HKr;->a:LX/HKs;

    iput-object p3, v1, LX/HKs;->b:LX/2km;

    .line 2455289
    iget-object v1, v0, LX/HKr;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2455290
    move-object v0, v0

    .line 2455291
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2455275
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2455268
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2455272
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455273
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2455274
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->o()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bp()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bp()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dq()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bo()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bo()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel$ImageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bo()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2455269
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455270
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2455271
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
