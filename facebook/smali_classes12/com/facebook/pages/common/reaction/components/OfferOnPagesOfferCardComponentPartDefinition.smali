.class public Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HId;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final c:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2451531
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2451532
    new-instance v0, LX/HIc;

    invoke-direct {v0}, LX/HIc;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;->b:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2451533
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2451534
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;->c:LX/HLT;

    .line 2451535
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;
    .locals 4

    .prologue
    .line 2451519
    const-class v1, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;

    monitor-enter v1

    .line 2451520
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2451521
    sput-object v2, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2451522
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2451523
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2451524
    new-instance p0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;-><init>(LX/HLT;)V

    .line 2451525
    move-object v0, p0

    .line 2451526
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2451527
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2451528
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2451529
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2451530
    sget-object v0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2451513
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 2451514
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v0

    .line 2451515
    new-instance v0, LX/HId;

    invoke-interface {v5}, LX/9uc;->dc()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5}, LX/9uc;->au()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5}, LX/9uc;->O()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentPartDefinition;->c:LX/HLT;

    invoke-interface {v5}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v6

    .line 2451516
    iget-object v7, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v7, v7

    .line 2451517
    iget-object v8, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v8, v8

    .line 2451518
    invoke-virtual {v4, v6, p3, v7, v8}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-interface {v5}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/HId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xdc1707

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2451509
    check-cast p2, LX/HId;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;

    .line 2451510
    iget-object v1, p2, LX/HId;->d:Ljava/lang/String;

    iget-object v2, p2, LX/HId;->a:Ljava/lang/String;

    iget-object p0, p2, LX/HId;->b:Ljava/lang/String;

    iget-object p1, p2, LX/HId;->c:Ljava/lang/String;

    invoke-virtual {p4, v1, v2, p0, p1}, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2451511
    iget-object v1, p2, LX/HId;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2451512
    const/16 v1, 0x1f

    const v2, 0x46fb8fb0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2451506
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2451507
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2451508
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->dc()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->au()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->au()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/9uc;->O()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2451503
    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;

    .line 2451504
    invoke-virtual {p4}, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->a()V

    .line 2451505
    return-void
.end method
