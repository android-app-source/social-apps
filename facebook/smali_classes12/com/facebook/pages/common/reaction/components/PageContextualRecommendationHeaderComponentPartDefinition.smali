.class public Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationHeaderComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJ8;",
        "TE;",
        "LX/HLr;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2452111
    new-instance v0, LX/HJ7;

    invoke-direct {v0}, LX/HJ7;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationHeaderComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452109
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2452110
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationHeaderComponentPartDefinition;
    .locals 3

    .prologue
    .line 2452098
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2452099
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationHeaderComponentPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452100
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationHeaderComponentPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452101
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452102
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2452103
    new-instance v0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationHeaderComponentPartDefinition;

    invoke-direct {v0}, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationHeaderComponentPartDefinition;-><init>()V

    .line 2452104
    move-object v0, v0

    .line 2452105
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452106
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452107
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452108
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2452097
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationHeaderComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2452094
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2452095
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452096
    new-instance v1, LX/HJ8;

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    invoke-direct {v1, v0}, LX/HJ8;-><init>(LX/3Ab;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x151f5272

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2452085
    check-cast p2, LX/HJ8;

    check-cast p4, LX/HLr;

    .line 2452086
    iget-object v1, p2, LX/HJ8;->a:LX/3Ab;

    .line 2452087
    :try_start_0
    iget-object v2, p4, LX/HLr;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setLinkableTextWithEntities(LX/3Ab;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 2452088
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x3729052e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2452089
    :catch_0
    move-exception v2

    .line 2452090
    const-string p0, "PageContextualRecommendationHeaderComponentView"

    invoke-virtual {v2}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-static {p0, p2, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2452091
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2452092
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452093
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
