.class public Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/HJH;

.field private final e:LX/9XE;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/HJH;LX/9XE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452430
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2452431
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->d:LX/HJH;

    .line 2452432
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->e:LX/9XE;

    .line 2452433
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2452349
    new-instance v2, LX/HLF;

    .line 2452350
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2452351
    invoke-direct {v2, v0}, LX/HLF;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 2452352
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v2, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HLG;

    .line 2452353
    iget-boolean v1, v0, LX/HLG;->c:Z

    if-nez v1, :cond_0

    .line 2452354
    invoke-direct {p0, p2}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2452355
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/HLG;->c:Z

    move-object v1, p3

    .line 2452356
    check-cast v1, LX/1Pr;

    invoke-interface {v1, v2, v0}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2452357
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->d:LX/HJH;

    const/4 v1, 0x0

    .line 2452358
    new-instance v3, LX/HJG;

    invoke-direct {v3, v0}, LX/HJG;-><init>(LX/HJH;)V

    .line 2452359
    iget-object p0, v0, LX/HJH;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HJF;

    .line 2452360
    if-nez p0, :cond_1

    .line 2452361
    new-instance p0, LX/HJF;

    invoke-direct {p0, v0}, LX/HJF;-><init>(LX/HJH;)V

    .line 2452362
    :cond_1
    invoke-static {p0, p1, v1, v1, v3}, LX/HJF;->a$redex0(LX/HJF;LX/1De;IILX/HJG;)V

    .line 2452363
    move-object v3, p0

    .line 2452364
    move-object v1, v3

    .line 2452365
    move-object v0, v1

    .line 2452366
    iget-object v1, v0, LX/HJF;->a:LX/HJG;

    iput-object p2, v1, LX/HJG;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2452367
    iget-object v1, v0, LX/HJF;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 2452368
    move-object v0, v0

    .line 2452369
    iget-object v1, v0, LX/HJF;->a:LX/HJG;

    iput-object v2, v1, LX/HJG;->b:LX/HLF;

    .line 2452370
    iget-object v1, v0, LX/HJF;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 2452371
    move-object v0, v0

    .line 2452372
    iget-object v1, v0, LX/HJF;->a:LX/HJG;

    iput-object p3, v1, LX/HJG;->c:LX/2km;

    .line 2452373
    iget-object v1, v0, LX/HJF;->e:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2452374
    move-object v0, v0

    .line 2452375
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;
    .locals 6

    .prologue
    .line 2452419
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;

    monitor-enter v1

    .line 2452420
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452421
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452422
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452423
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2452424
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HJH;->a(LX/0QB;)LX/HJH;

    move-result-object v4

    check-cast v4, LX/HJH;

    invoke-static {v0}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v5

    check-cast v5, LX/9XE;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;-><init>(Landroid/content/Context;LX/HJH;LX/9XE;)V

    .line 2452425
    move-object v0, p0

    .line 2452426
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452427
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452428
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452429
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 4

    .prologue
    .line 2452414
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->e:LX/9XE;

    .line 2452415
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2452416
    invoke-interface {v1}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2452417
    iget-object v1, v0, LX/9XE;->a:LX/0Zb;

    sget-object p0, LX/9XA;->EVENT_CITY_HUB_SOCIAL_MODULE_RENDER_ERROR:LX/9XA;

    invoke-static {p0, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2452418
    return-void
.end method

.method private d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 6

    .prologue
    .line 2452407
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->e:LX/9XE;

    .line 2452408
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2452409
    invoke-interface {v1}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2452410
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2452411
    invoke-interface {v1}, LX/9uc;->aE()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    int-to-long v4, v1

    .line 2452412
    iget-object v1, v0, LX/9XE;->a:LX/0Zb;

    sget-object p0, LX/9XB;->EVENT_CITY_HUB_SOCIAL_MODULE_RENDER_SUCCESS:LX/9XB;

    invoke-static {p0, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "friends_count"

    invoke-virtual {p0, p1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2452413
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2452406
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2452405
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    .line 2452379
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 2452380
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452381
    invoke-interface {v0}, LX/9uc;->N()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2452382
    :goto_0
    return v0

    .line 2452383
    :cond_0
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452384
    invoke-interface {v0}, LX/9uc;->aE()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2452385
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move v0, v1

    .line 2452386
    goto :goto_0

    .line 2452387
    :cond_1
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452388
    invoke-interface {v0}, LX/9uc;->aE()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;

    .line 2452389
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;->d()LX/5sY;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageFriendsCityActivityComponentFragmentModel$FriendsModel;->d()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2452390
    :cond_2
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move v0, v1

    .line 2452391
    goto :goto_0

    .line 2452392
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2452393
    :cond_4
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452394
    invoke-interface {v0}, LX/9uc;->m()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    .line 2452395
    :goto_2
    if-ge v2, v4, :cond_8

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;

    .line 2452396
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v5

    .line 2452397
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v6

    invoke-interface {v6}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    const/4 v6, 0x1

    :goto_3
    move v5, v6

    .line 2452398
    if-nez v5, :cond_5

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v5

    .line 2452399
    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;

    move-result-object v6

    if-eqz v6, :cond_a

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    const/4 v6, 0x1

    :goto_4
    move v5, v6

    .line 2452400
    if-eqz v5, :cond_6

    :cond_5
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->gP_()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->b()LX/3Ab;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->b()LX/3Ab;

    move-result-object v0

    invoke-interface {v0}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2452401
    :cond_6
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityComponentPartDefinition;->c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move v0, v1

    .line 2452402
    goto/16 :goto_0

    .line 2452403
    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_2

    .line 2452404
    :cond_8
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_9
    const/4 v6, 0x0

    goto :goto_3

    :cond_a
    const/4 v6, 0x0

    goto :goto_4
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2452376
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2452377
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2452378
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
