.class public Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/3U9;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJ6;",
        "TE;",
        "Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:LX/5up;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2452075
    new-instance v0, LX/HJ2;

    invoke-direct {v0}, LX/HJ2;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;LX/5up;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452076
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2452077
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;->b:LX/HLT;

    .line 2452078
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;->c:LX/5up;

    .line 2452079
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;
    .locals 5

    .prologue
    .line 2452063
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;

    monitor-enter v1

    .line 2452064
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452065
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452066
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452067
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2452068
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-static {v0}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v4

    check-cast v4, LX/5up;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;-><init>(LX/HLT;LX/5up;)V

    .line 2452069
    move-object v0, p0

    .line 2452070
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452071
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452072
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2452074
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2452047
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    const/4 v0, 0x0

    .line 2452048
    iget-object v1, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v1

    .line 2452049
    invoke-interface {v4}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2452050
    :goto_0
    invoke-interface {v4}, LX/9uc;->n()LX/5sY;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, LX/9uc;->n()LX/5sY;

    move-result-object v0

    invoke-interface {v0}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v3

    .line 2452051
    :goto_1
    new-instance v0, LX/HJ6;

    invoke-interface {v4}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->e()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    .line 2452052
    iget-object v6, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v7, v6

    .line 2452053
    sget-object v6, LX/HJ5;->a:[I

    invoke-interface {v7}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->ordinal()I

    move-result v8

    aget v6, v6, v8

    packed-switch v6, :pswitch_data_0

    .line 2452054
    iget-object v6, p0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v7}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v7

    .line 2452055
    iget-object v8, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v9, v8

    .line 2452056
    iget-object v8, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v10, v8

    .line 2452057
    iget-object v8, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v8, v8

    .line 2452058
    invoke-interface {v8}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v11

    move-object v8, p3

    invoke-virtual/range {v6 .. v11}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v6

    :goto_2
    move-object v5, v6

    .line 2452059
    invoke-direct/range {v0 .. v5}, LX/HJ6;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_0
    move-object v2, v0

    .line 2452060
    goto :goto_0

    :cond_1
    move-object v3, v0

    .line 2452061
    goto :goto_1

    .line 2452062
    :pswitch_0
    new-instance v6, LX/HJ4;

    invoke-direct {v6, p0, p3, v7, p2}, LX/HJ4;-><init>(Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;LX/2km;LX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x12d84154

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2452027
    check-cast p2, LX/HJ6;

    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;

    .line 2452028
    iget-object v2, p2, LX/HJ6;->a:Ljava/lang/String;

    iget-object v4, p2, LX/HJ6;->b:Ljava/lang/String;

    iget-object v5, p2, LX/HJ6;->c:Ljava/lang/String;

    iget-object v6, p2, LX/HJ6;->d:Ljava/lang/String;

    iget-object v7, p2, LX/HJ6;->e:Landroid/view/View$OnClickListener;

    move-object v1, p4

    const/16 p4, 0x8

    const/4 p2, 0x0

    .line 2452029
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 2452030
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2452031
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, p2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2452032
    :goto_0
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 2452033
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2452034
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, p2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2452035
    :goto_1
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 2452036
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    sget-object p3, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2452037
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2452038
    :goto_2
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 2452039
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0, p2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2452040
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0, v6}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2452041
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0, v7}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2452042
    :goto_3
    const/16 v1, 0x1f

    const v2, 0x2bd7040

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2452043
    :cond_0
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, p4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 2452044
    :cond_1
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, p4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1

    .line 2452045
    :cond_2
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_2

    .line 2452046
    :cond_3
    iget-object p0, v1, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0, p4}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2452024
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2452025
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452026
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2452021
    check-cast p4, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;

    .line 2452022
    iget-object p0, p4, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->n:Lcom/facebook/resources/ui/FbButton;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2452023
    return-void
.end method
