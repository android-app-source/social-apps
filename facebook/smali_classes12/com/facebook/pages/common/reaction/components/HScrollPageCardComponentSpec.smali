.class public Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/HIT;

.field public static final b:LX/HIS;

.field public static final c:LX/HIU;

.field public static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field public final e:Landroid/content/Context;

.field public final f:LX/17W;

.field public final g:LX/1DR;

.field public final h:LX/1nu;

.field public final i:LX/HIX;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2451178
    const-class v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2451179
    sput-object v1, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->a:LX/HIT;

    .line 2451180
    sput-object v1, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->b:LX/HIS;

    .line 2451181
    sput-object v1, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->c:LX/HIU;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/17W;LX/1DR;LX/1nu;LX/HIX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2451182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2451183
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->e:Landroid/content/Context;

    .line 2451184
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->f:LX/17W;

    .line 2451185
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->g:LX/1DR;

    .line 2451186
    iput-object p4, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->h:LX/1nu;

    .line 2451187
    iput-object p5, p0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->i:LX/HIX;

    .line 2451188
    return-void
.end method

.method public static a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;)LX/HIR;
    .locals 9

    .prologue
    .line 2451189
    new-instance v0, LX/HIR;

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 2451190
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->gQ_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->gQ_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->gQ_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->gQ_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->gQ_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2451191
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->gQ_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v2

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->b()Z

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->d()Z

    move-result v4

    .line 2451192
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;->a()I

    move-result v5

    if-lez v5, :cond_4

    const/4 v5, 0x1

    :goto_2
    move v5, v5

    .line 2451193
    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$PageLikersModel;->a()I

    move-result v5

    :goto_3
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->gP_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->l()Ljava/lang/String;

    move-result-object v7

    .line 2451194
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v8

    invoke-interface {v8}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    const/4 v8, 0x1

    :goto_4
    move v8, v8

    .line 2451195
    if-eqz v8, :cond_2

    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v8

    invoke-interface {v8}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    :goto_5
    invoke-direct/range {v0 .. v8}, LX/HIR;-><init>(Ljava/lang/String;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;ZZILjava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    goto :goto_5

    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    :cond_5
    const/4 v8, 0x0

    goto :goto_4
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;
    .locals 9

    .prologue
    .line 2451196
    const-class v1, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;

    monitor-enter v1

    .line 2451197
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2451198
    sput-object v2, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2451199
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2451200
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2451201
    new-instance v3, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v6

    check-cast v6, LX/1DR;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v7

    check-cast v7, LX/1nu;

    invoke-static {v0}, LX/HIX;->a(LX/0QB;)LX/HIX;

    move-result-object v8

    check-cast v8, LX/HIX;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;-><init>(Landroid/content/Context;LX/17W;LX/1DR;LX/1nu;LX/HIX;)V

    .line 2451202
    move-object v0, v3

    .line 2451203
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2451204
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2451205
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2451206
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
