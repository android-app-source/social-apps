.class public Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E1f;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/1nu;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2454897
    const-class v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;

    const-string v1, "pages_public_view"

    const-string v2, "thumbnail"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "LX/0Ot",
            "<",
            "LX/E1f;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2454909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2454910
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;->c:LX/1nu;

    .line 2454911
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;->a:LX/0Ot;

    .line 2454912
    return-void
.end method

.method public static a(LX/1De;Ljava/lang/CharSequence;II)LX/1Di;
    .locals 2

    .prologue
    .line 2454913
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ne;->o(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;
    .locals 5

    .prologue
    .line 2454898
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;

    monitor-enter v1

    .line 2454899
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2454900
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2454901
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2454902
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2454903
    new-instance v4, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    const/16 p0, 0x3098

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;-><init>(LX/1nu;LX/0Ot;)V

    .line 2454904
    move-object v0, v4

    .line 2454905
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2454906
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2454907
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2454908
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
