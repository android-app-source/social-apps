.class public Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;

.field private final b:Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2454920
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2454921
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;->a:Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;

    .line 2454922
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;->b:Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;

    .line 2454923
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;->c:LX/0Uh;

    .line 2454924
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;
    .locals 6

    .prologue
    .line 2454925
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;

    monitor-enter v1

    .line 2454926
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2454927
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2454928
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2454929
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2454930
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;-><init>(Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;LX/0Uh;)V

    .line 2454931
    move-object v0, p0

    .line 2454932
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2454933
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2454934
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2454935
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2454936
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2454937
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;->c:LX/0Uh;

    const/16 v1, 0x41d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2454938
    if-eqz v0, :cond_0

    .line 2454939
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;->b:Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2454940
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2454941
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;->a:Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2454942
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2454943
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentSelectorPartDefinition;->c:LX/0Uh;

    const/16 v1, 0x41d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2454944
    if-eqz v0, :cond_0

    .line 2454945
    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    .line 2454946
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    goto :goto_0
.end method
