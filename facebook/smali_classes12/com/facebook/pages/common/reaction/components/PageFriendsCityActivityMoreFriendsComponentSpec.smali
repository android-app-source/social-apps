.class public Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/1nu;

.field public final d:LX/17W;

.field public final e:LX/1vg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2452774
    const-class v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1nu;LX/17W;LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2452776
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->b:Landroid/content/Context;

    .line 2452777
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->c:LX/1nu;

    .line 2452778
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->d:LX/17W;

    .line 2452779
    iput-object p4, p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->e:LX/1vg;

    .line 2452780
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;
    .locals 7

    .prologue
    .line 2452781
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;

    monitor-enter v1

    .line 2452782
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452783
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452784
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452785
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2452786
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v6

    check-cast v6, LX/1vg;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;-><init>(Landroid/content/Context;LX/1nu;LX/17W;LX/1vg;)V

    .line 2452787
    move-object v0, p0

    .line 2452788
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452789
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452790
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452791
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
