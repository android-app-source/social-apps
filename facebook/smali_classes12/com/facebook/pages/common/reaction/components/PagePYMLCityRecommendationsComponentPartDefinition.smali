.class public Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/HK7;

.field private final e:LX/9XE;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/HK7;LX/9XE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453793
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2453794
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->d:LX/HK7;

    .line 2453795
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->e:LX/9XE;

    .line 2453796
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2453717
    new-instance v2, LX/HLO;

    .line 2453718
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2453719
    invoke-direct {v2, v0}, LX/HLO;-><init>(Ljava/lang/String;)V

    move-object v0, p3

    .line 2453720
    check-cast v0, LX/1Pr;

    invoke-interface {v0, v2, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HLP;

    .line 2453721
    iget-boolean v1, v0, LX/HLP;->a:Z

    if-nez v1, :cond_0

    .line 2453722
    invoke-direct {p0, p2}, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2453723
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/HLP;->a:Z

    move-object v1, p3

    .line 2453724
    check-cast v1, LX/1Pr;

    invoke-interface {v1, v2, v0}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2453725
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->d:LX/HK7;

    const/4 v1, 0x0

    .line 2453726
    new-instance v3, LX/HK6;

    invoke-direct {v3, v0}, LX/HK6;-><init>(LX/HK7;)V

    .line 2453727
    iget-object p0, v0, LX/HK7;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HK5;

    .line 2453728
    if-nez p0, :cond_1

    .line 2453729
    new-instance p0, LX/HK5;

    invoke-direct {p0, v0}, LX/HK5;-><init>(LX/HK7;)V

    .line 2453730
    :cond_1
    invoke-static {p0, p1, v1, v1, v3}, LX/HK5;->a$redex0(LX/HK5;LX/1De;IILX/HK6;)V

    .line 2453731
    move-object v3, p0

    .line 2453732
    move-object v1, v3

    .line 2453733
    move-object v0, v1

    .line 2453734
    iget-object v1, v0, LX/HK5;->a:LX/HK6;

    iput-object v2, v1, LX/HK6;->a:LX/HLO;

    .line 2453735
    iget-object v1, v0, LX/HK5;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 2453736
    move-object v0, v0

    .line 2453737
    iget-object v1, v0, LX/HK5;->a:LX/HK6;

    iput-object p3, v1, LX/HK6;->c:LX/2km;

    .line 2453738
    iget-object v1, v0, LX/HK5;->e:Ljava/util/BitSet;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2453739
    move-object v0, v0

    .line 2453740
    iget-object v1, v0, LX/HK5;->a:LX/HK6;

    iput-object p2, v1, LX/HK6;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2453741
    iget-object v1, v0, LX/HK5;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2453742
    move-object v0, v0

    .line 2453743
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;
    .locals 6

    .prologue
    .line 2453744
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;

    monitor-enter v1

    .line 2453745
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453746
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453747
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453748
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453749
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HK7;->a(LX/0QB;)LX/HK7;

    move-result-object v4

    check-cast v4, LX/HK7;

    invoke-static {v0}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v5

    check-cast v5, LX/9XE;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;-><init>(Landroid/content/Context;LX/HK7;LX/9XE;)V

    .line 2453750
    move-object v0, p0

    .line 2453751
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453752
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453753
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453754
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 4

    .prologue
    .line 2453755
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->e:LX/9XE;

    .line 2453756
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2453757
    invoke-interface {v1}, LX/9uc;->M()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2453758
    iget-object v1, v0, LX/9XE;->a:LX/0Zb;

    sget-object p0, LX/9XA;->EVENT_CITY_HUB_PYML_MODULE_RENDER_ERROR:LX/9XA;

    invoke-static {p0, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2453759
    return-void
.end method

.method private d(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 6

    .prologue
    .line 2453760
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->e:LX/9XE;

    .line 2453761
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2453762
    invoke-interface {v1}, LX/9uc;->M()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2453763
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2453764
    invoke-interface {v1}, LX/9uc;->cn()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    int-to-long v4, v1

    .line 2453765
    iget-object v1, v0, LX/9XE;->a:LX/0Zb;

    sget-object p0, LX/9XB;->EVENT_CITY_HUB_PYML_MODULE_RENDER_SUCCESS:LX/9XB;

    invoke-static {p0, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "pages_count"

    invoke-virtual {p0, p1, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2453766
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2453767
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2453768
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2453769
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v1, 0x0

    .line 2453770
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2453771
    invoke-interface {v0}, LX/9uc;->M()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2453772
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2453773
    invoke-interface {v0}, LX/9uc;->M()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$CityPageModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 2453774
    :goto_0
    return v0

    .line 2453775
    :cond_1
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2453776
    invoke-interface {v0}, LX/9uc;->cn()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2453777
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move v0, v1

    .line 2453778
    goto :goto_0

    .line 2453779
    :cond_2
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2453780
    invoke-interface {v0}, LX/9uc;->cn()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    .line 2453781
    if-eqz v0, :cond_4

    .line 2453782
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->c()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel;->a()Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/common/ReactionCommonGraphQLModels$ReactionPageFieldsModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v5

    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    const/4 v5, 0x1

    :goto_2
    move v5, v5

    .line 2453783
    if-nez v5, :cond_3

    .line 2453784
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    const/4 v5, 0x1

    :goto_3
    move v5, v5

    .line 2453785
    if-eqz v5, :cond_4

    :cond_3
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->gP_()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2453786
    :cond_4
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/reaction/components/PagePYMLCityRecommendationsComponentPartDefinition;->c(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    move v0, v1

    .line 2453787
    goto/16 :goto_0

    .line 2453788
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 2453789
    :cond_6
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_7
    const/4 v5, 0x0

    goto :goto_2

    :cond_8
    const/4 v5, 0x0

    goto :goto_3
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2453790
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2453791
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2453792
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
