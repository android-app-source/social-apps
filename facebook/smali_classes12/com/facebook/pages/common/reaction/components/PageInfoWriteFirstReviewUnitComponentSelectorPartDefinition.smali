.class public Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;

.field private final b:Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453073
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2453074
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;->a:Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;

    .line 2453075
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;->b:Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;

    .line 2453076
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;->c:LX/0Uh;

    .line 2453077
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;
    .locals 6

    .prologue
    .line 2453078
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;

    monitor-enter v1

    .line 2453079
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453080
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453081
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453082
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453083
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;-><init>(Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;LX/0Uh;)V

    .line 2453084
    move-object v0, p0

    .line 2453085
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453086
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453087
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453088
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2453089
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2453090
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;->c:LX/0Uh;

    const/16 v1, 0x41d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2453091
    if-eqz v0, :cond_0

    .line 2453092
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;->b:Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2453093
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2453094
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;->a:Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2453095
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2453096
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentSelectorPartDefinition;->c:LX/0Uh;

    const/16 v1, 0x41d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2453097
    if-eqz v0, :cond_0

    .line 2453098
    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    .line 2453099
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageInfoWriteFirstReviewUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    goto :goto_0
.end method
