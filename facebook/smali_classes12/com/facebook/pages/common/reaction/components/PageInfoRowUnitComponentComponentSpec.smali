.class public Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/E1f;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2452905
    const-class v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;

    const-string v1, "pages_public_view"

    const-string v2, "thumbnail"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/E1f;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2452907
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;->b:LX/1nu;

    .line 2452908
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;->c:LX/E1f;

    .line 2452909
    return-void
.end method

.method public static a(LX/1De;Ljava/lang/String;II)LX/1Di;
    .locals 2

    .prologue
    .line 2452910
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ne;->o(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;
    .locals 5

    .prologue
    .line 2452911
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;

    monitor-enter v1

    .line 2452912
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452913
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452914
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452915
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2452916
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v4

    check-cast v4, LX/E1f;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;-><init>(LX/1nu;LX/E1f;)V

    .line 2452917
    move-object v0, p0

    .line 2452918
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452919
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInfoRowUnitComponentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452920
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452921
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
