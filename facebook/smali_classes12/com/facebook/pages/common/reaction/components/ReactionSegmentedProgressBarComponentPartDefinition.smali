.class public Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HL1;",
        "TE;",
        "LX/HM8;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/HLT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2455397
    new-instance v0, LX/HL0;

    invoke-direct {v0}, LX/HL0;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2455398
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2455399
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;->b:LX/HLT;

    .line 2455400
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;
    .locals 4

    .prologue
    .line 2455401
    const-class v1, Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;

    monitor-enter v1

    .line 2455402
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2455403
    sput-object v2, Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2455404
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2455405
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2455406
    new-instance p0, Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;-><init>(LX/HLT;)V

    .line 2455407
    move-object v0, p0

    .line 2455408
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2455409
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2455410
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2455411
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2455412
    sget-object v0, Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2455413
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    .line 2455414
    iget-object v0, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2455415
    new-instance v1, LX/HL1;

    invoke-interface {v0}, LX/9uc;->ci()LX/0Px;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/common/reaction/components/ReactionSegmentedProgressBarComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v0}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v0

    .line 2455416
    iget-object v4, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2455417
    iget-object v5, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2455418
    invoke-virtual {v3, v0, p3, v4, v5}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/HL1;-><init>(LX/0Px;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1b475bfc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2455419
    check-cast p2, LX/HL1;

    check-cast p4, LX/HM8;

    .line 2455420
    iget-object v1, p2, LX/HL1;->a:LX/0Px;

    iget-object v2, p2, LX/HL1;->b:Landroid/view/View$OnClickListener;

    const/4 v5, 0x0

    .line 2455421
    invoke-virtual {p4}, LX/HM8;->removeAllViews()V

    .line 2455422
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    move v7, v5

    :goto_0
    if-ge v6, v8, :cond_0

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionSegmentedProgressBarComponentFragmentModel$ProgressSegmentsModel;

    .line 2455423
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionSegmentedProgressBarComponentFragmentModel$ProgressSegmentsModel;->b()I

    move-result v4

    add-int/2addr v7, v4

    .line 2455424
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 2455425
    :cond_0
    if-ltz v7, :cond_1

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2455426
    invoke-virtual {p4, v2}, LX/HM8;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2455427
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    :goto_2
    if-ge v6, v8, :cond_2

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionSegmentedProgressBarComponentFragmentModel$ProgressSegmentsModel;

    .line 2455428
    :try_start_0
    new-instance v9, Ljava/lang/StringBuilder;

    const-string p0, "#"

    invoke-direct {v9, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionSegmentedProgressBarComponentFragmentModel$ProgressSegmentsModel;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    .line 2455429
    new-instance p0, Landroid/view/View;

    invoke-virtual {p4}, LX/HM8;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2455430
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p3, -0x1

    invoke-direct {p1, v5, p3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2455431
    invoke-virtual {v4}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionSegmentedProgressBarComponentFragmentModel$ProgressSegmentsModel;->b()I

    move-result v4

    int-to-float v4, v4

    int-to-float p3, v7

    div-float/2addr v4, p3

    iput v4, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2455432
    invoke-virtual {p4}, LX/HM8;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p3, 0x7f0b0033

    invoke-virtual {v4, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p4}, LX/HM8;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const p2, 0x7f0b0033

    invoke-virtual {p3, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    invoke-virtual {p1, v5, v4, v5, p3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2455433
    invoke-virtual {p0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2455434
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v9}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v4}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2455435
    invoke-virtual {p4, p0}, LX/HM8;->addView(Landroid/view/View;)V

    .line 2455436
    :goto_3
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_2

    :cond_1
    move v4, v5

    .line 2455437
    goto :goto_1

    .line 2455438
    :catch_0
    move-exception v4

    .line 2455439
    iget-object v9, p4, LX/HM8;->a:LX/03V;

    sget-object p0, LX/HM8;->b:Ljava/lang/String;

    const-string p1, "Failed to parse segmented progress bar style background color"

    invoke-virtual {v9, p0, p1, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 2455440
    :cond_2
    const/16 v1, 0x1f

    const v2, 0x35fea58

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2455441
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455442
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2455443
    invoke-interface {v0}, LX/9uc;->ci()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->ci()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2455444
    check-cast p4, LX/HM8;

    .line 2455445
    const/4 p0, 0x0

    invoke-virtual {p4, p0}, LX/HM8;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2455446
    return-void
.end method
