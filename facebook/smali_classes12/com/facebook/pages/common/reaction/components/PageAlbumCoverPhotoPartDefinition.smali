.class public Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/HIx;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2451963
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2451964
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;
    .locals 3

    .prologue
    .line 2451965
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

    monitor-enter v1

    .line 2451966
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2451967
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2451968
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2451969
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2451970
    new-instance v0, Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;

    invoke-direct {v0}, Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;-><init>()V

    .line 2451971
    move-object v0, v0

    .line 2451972
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2451973
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageAlbumCoverPhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2451974
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2451975
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x610f1aca

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2451976
    check-cast p1, LX/HIx;

    check-cast p4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2451977
    iget-object v1, p1, LX/HIx;->a:LX/HIw;

    invoke-virtual {v1, p4}, LX/HIw;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 2451978
    const/16 v1, 0x1f

    const v2, -0x398400c6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
