.class public Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/HKf;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/HKf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2454878
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2454879
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;->d:LX/HKf;

    .line 2454880
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2454881
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;->d:LX/HKf;

    const/4 v1, 0x0

    .line 2454882
    new-instance v2, LX/HKe;

    invoke-direct {v2, v0}, LX/HKe;-><init>(LX/HKf;)V

    .line 2454883
    iget-object p0, v0, LX/HKf;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HKd;

    .line 2454884
    if-nez p0, :cond_0

    .line 2454885
    new-instance p0, LX/HKd;

    invoke-direct {p0, v0}, LX/HKd;-><init>(LX/HKf;)V

    .line 2454886
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HKd;->a$redex0(LX/HKd;LX/1De;IILX/HKe;)V

    .line 2454887
    move-object v2, p0

    .line 2454888
    move-object v1, v2

    .line 2454889
    move-object v0, v1

    .line 2454890
    iget-object v1, v0, LX/HKd;->a:LX/HKe;

    iput-object p2, v1, LX/HKe;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2454891
    iget-object v1, v0, LX/HKd;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2454892
    move-object v0, v0

    .line 2454893
    iget-object v1, v0, LX/HKd;->a:LX/HKe;

    iput-object p3, v1, LX/HKe;->b:LX/2km;

    .line 2454894
    iget-object v1, v0, LX/HKd;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2454895
    move-object v0, v0

    .line 2454896
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;
    .locals 5

    .prologue
    .line 2454865
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;

    monitor-enter v1

    .line 2454866
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2454867
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2454868
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2454869
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2454870
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HKf;->a(LX/0QB;)LX/HKf;

    move-result-object v4

    check-cast v4, LX/HKf;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;-><init>(Landroid/content/Context;LX/HKf;)V

    .line 2454871
    move-object v0, p0

    .line 2454872
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2454873
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2454874
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2454875
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2454876
    iget-object v1, p0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2454877
    invoke-interface {v1}, LX/9uc;->cw()LX/3Ab;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cw()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cw()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cw()LX/3Ab;

    move-result-object v2

    invoke-interface {v2}, LX/3Ab;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-interface {v1}, LX/9uc;->cv()LX/174;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cv()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->B()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->B()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;->a()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->B()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionPageVeryResponsiveToMessagesComponentFragmentModel$BadgeIconModel;->a()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cx()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->cx()Ljava/lang/String;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2454864
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2454863
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2454862
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageVeryResponsiveToMessagesBadgeUnitComponentComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2454859
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2454860
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2454861
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
