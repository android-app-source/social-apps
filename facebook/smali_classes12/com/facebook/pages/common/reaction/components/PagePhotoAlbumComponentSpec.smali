.class public Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/2U1;

.field private final d:LX/8I0;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2453997
    const-class v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;

    const-string v1, "pages_public_view"

    const-string v2, "album_photo"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/2U1;LX/8I0;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "LX/2U1;",
            "Lcom/facebook/photos/intent/PhotosViewIntentBuilder;",
            "LX/0Or",
            "<",
            "LX/0SI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2453893
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->b:LX/1nu;

    .line 2453894
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->c:LX/2U1;

    .line 2453895
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->d:LX/8I0;

    .line 2453896
    iput-object p4, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->e:LX/0Or;

    .line 2453897
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;
    .locals 7

    .prologue
    .line 2453898
    const-class v1, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;

    monitor-enter v1

    .line 2453899
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453900
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453901
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453902
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453903
    new-instance v6, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v4

    check-cast v4, LX/2U1;

    invoke-static {v0}, LX/8I0;->b(LX/0QB;)LX/8I0;

    move-result-object v5

    check-cast v5, LX/8I0;

    const/16 p0, 0x1a1

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;-><init>(LX/1nu;LX/2U1;LX/8I0;LX/0Or;)V

    .line 2453904
    move-object v0, v6

    .line 2453905
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453906
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453907
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453908
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static c(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 4

    .prologue
    .line 2453909
    new-instance v1, LX/89I;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v0, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v1, v2, v3, v0}, LX/89I;-><init>(JLX/2rw;)V

    .line 2453910
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->c:LX/2U1;

    invoke-virtual {v0, p1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 2453911
    if-nez v0, :cond_0

    .line 2453912
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    .line 2453913
    :goto_0
    return-object v0

    .line 2453914
    :cond_0
    iget-object v2, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v2, v2

    .line 2453915
    invoke-virtual {v2}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 2453916
    if-eqz v2, :cond_1

    .line 2453917
    iput-object v2, v1, LX/89I;->c:Ljava/lang/String;

    .line 2453918
    :cond_1
    iget-object v2, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v2, v2

    .line 2453919
    invoke-virtual {v2}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_3

    .line 2453920
    iget-object v2, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v0, v2

    .line 2453921
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2453922
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2453923
    :goto_1
    if-eqz v0, :cond_2

    .line 2453924
    iput-object v0, v1, LX/89I;->d:Ljava/lang/String;

    .line 2453925
    :cond_2
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    goto :goto_0

    .line 2453926
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onClick(LX/HKJ;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)V
    .locals 12
    .param p1    # LX/HKJ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/2km;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HKJ;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2453927
    iget-object v1, p1, LX/HKJ;->e:Ljava/lang/String;

    .line 2453928
    iget-object v0, p1, LX/HKJ;->a:Ljava/lang/String;

    .line 2453929
    iget-object v2, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2453930
    iget-object v3, p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2453931
    iget-object v4, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->c:LX/2U1;

    invoke-virtual {v4, v1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/8Dk;

    .line 2453932
    iget-object v5, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->e:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0SI;

    invoke-interface {v5}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    .line 2453933
    if-eqz v4, :cond_3

    .line 2453934
    iget-object v6, v4, LX/8Dk;->b:LX/0am;

    move-object v6, v6

    .line 2453935
    invoke-virtual {v6}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2453936
    iget-boolean v6, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v6, v6

    .line 2453937
    if-nez v6, :cond_3

    .line 2453938
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v6

    const/4 v7, 0x1

    .line 2453939
    iput-boolean v7, v6, LX/0SK;->d:Z

    .line 2453940
    move-object v6, v6

    .line 2453941
    iget-object v7, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v7, v7

    .line 2453942
    iput-object v7, v6, LX/0SK;->c:Ljava/lang/String;

    .line 2453943
    move-object v6, v6

    .line 2453944
    iget-object v7, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    move-object v7, v7

    .line 2453945
    iput-object v7, v6, LX/0SK;->f:Ljava/lang/String;

    .line 2453946
    move-object v6, v6

    .line 2453947
    iget-object v7, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    move-object v5, v7

    .line 2453948
    iput-object v5, v6, LX/0SK;->e:Ljava/lang/String;

    .line 2453949
    move-object v5, v6

    .line 2453950
    iput-object v1, v5, LX/0SK;->a:Ljava/lang/String;

    .line 2453951
    move-object v6, v5

    .line 2453952
    iget-object v5, v4, LX/8Dk;->b:LX/0am;

    move-object v5, v5

    .line 2453953
    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2453954
    iput-object v5, v6, LX/0SK;->b:Ljava/lang/String;

    .line 2453955
    move-object v5, v6

    .line 2453956
    iget-object v6, v4, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v4, v6

    .line 2453957
    invoke-virtual {v4}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v4

    .line 2453958
    iput-object v4, v5, LX/0SK;->g:Ljava/lang/String;

    .line 2453959
    move-object v4, v5

    .line 2453960
    invoke-virtual {v4}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    .line 2453961
    :goto_0
    move-object v4, v4

    .line 2453962
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2453963
    iget-object v5, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->c:LX/2U1;

    invoke-virtual {v5, v0}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/8Dk;

    .line 2453964
    if-eqz v5, :cond_0

    .line 2453965
    iget-object v6, v5, LX/8Dk;->b:LX/0am;

    move-object v6, v6

    .line 2453966
    invoke-virtual {v6}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2453967
    iget-object v6, v5, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v5, v6

    .line 2453968
    invoke-virtual {v5}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    .line 2453969
    const/4 v5, 0x0

    move v6, v5

    :goto_1
    if-ge v6, v9, :cond_0

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2453970
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2453971
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 2453972
    :cond_0
    move-object v5, v7

    .line 2453973
    invoke-static {p0, v1}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->c(Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v6

    .line 2453974
    new-instance v7, LX/4Vp;

    invoke-direct {v7}, LX/4Vp;-><init>()V

    .line 2453975
    iput-object v0, v7, LX/4Vp;->m:Ljava/lang/String;

    .line 2453976
    move-object v0, v7

    .line 2453977
    invoke-virtual {v0}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v7

    .line 2453978
    iget-object v8, p0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumComponentSpec;->d:LX/8I0;

    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v0, v9}, LX/8I0;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2453979
    const-string v8, "extra_album_selected"

    invoke-static {v0, v8, v7}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2453980
    const-string v7, "extra_photo_tab_mode_params"

    sget-object v8, LX/5SD;->VIEWING_MODE:LX/5SD;

    .line 2453981
    iget-object v9, v4, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v9, v9

    .line 2453982
    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v8, v10, v11}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a(LX/5SD;J)Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2453983
    const-string v7, "is_page"

    const/4 v8, 0x1

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2453984
    const-string v7, "owner_id"

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v0, v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2453985
    const-string v7, "pick_hc_pic"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2453986
    const-string v7, "pick_pic_lite"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2453987
    const-string v7, "disable_adding_photos_to_albums"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2453988
    iget-boolean v7, v4, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v7, v7

    .line 2453989
    if-eqz v7, :cond_2

    .line 2453990
    const-string v7, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2453991
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2453992
    const-string v4, "extra_pages_admin_permissions"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2453993
    :cond_1
    const-string v4, "extra_composer_target_data"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2453994
    :cond_2
    new-instance v4, LX/Cfl;

    sget-object v5, LX/Cfc;->ALBUM_TAP:LX/Cfc;

    invoke-direct {v4, v1, v5, v0}, LX/Cfl;-><init>(Ljava/lang/String;LX/Cfc;Landroid/content/Intent;)V

    .line 2453995
    invoke-interface {p3, v2, v3, v4}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2453996
    return-void

    :cond_3
    move-object v4, v5

    goto/16 :goto_0
.end method
