.class public Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "LX/HJA;",
        "TE;",
        "LX/HLs;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/HLT;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HLT",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2452149
    new-instance v0, LX/HJ9;

    invoke-direct {v0}, LX/HJ9;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/HLT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452146
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2452147
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;->b:LX/HLT;

    .line 2452148
    return-void
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HJA;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/HJA;"
        }
    .end annotation

    .prologue
    .line 2452144
    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v1

    .line 2452145
    new-instance v8, LX/HJA;

    invoke-interface {v1}, LX/9uc;->cr()D

    move-result-wide v10

    invoke-interface {v1}, LX/9uc;->cq()D

    move-result-wide v12

    invoke-interface {v1}, LX/9uc;->cz()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;->b:LX/HLT;

    invoke-interface {v1}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->k()LX/9uc;

    move-result-object v2

    invoke-interface {v2}, LX/9uc;->S()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v2, p2

    invoke-virtual/range {v0 .. v5}, LX/HLT;->a(Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;LX/2km;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v7

    move-object v1, v8

    move-wide v2, v10

    move-wide v4, v12

    invoke-direct/range {v1 .. v7}, LX/HJA;-><init>(DDLjava/lang/String;Landroid/view/View$OnClickListener;)V

    return-object v8
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;
    .locals 4

    .prologue
    .line 2452133
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;

    monitor-enter v1

    .line 2452134
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452135
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452136
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452137
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2452138
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;

    invoke-static {v0}, LX/HLT;->a(LX/0QB;)LX/HLT;

    move-result-object v3

    check-cast v3, LX/HLT;

    invoke-direct {p0, v3}, Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;-><init>(LX/HLT;)V

    .line 2452139
    move-object v0, p0

    .line 2452140
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452141
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452142
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452143
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2452150
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2452132
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/2km;

    invoke-direct {p0, p2, p3}, Lcom/facebook/pages/common/reaction/components/PageCrReviewComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/2km;)LX/HJA;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 10

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x426568f5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2452126
    check-cast p2, LX/HJA;

    check-cast p4, LX/HLs;

    .line 2452127
    iget-wide v4, p2, LX/HJA;->a:D

    double-to-float v4, v4

    iget-wide v6, p2, LX/HJA;->b:D

    double-to-float v5, v6

    iget-object v6, p2, LX/HJA;->c:Ljava/lang/String;

    .line 2452128
    iget-object v7, p4, LX/HLs;->a:Lcom/facebook/widget/ratingbar/FractionalRatingBar;

    div-float v8, v4, v5

    const/high16 v9, 0x40a00000    # 5.0f

    mul-float/2addr v8, v9

    invoke-virtual {v7, v8}, Lcom/facebook/widget/ratingbar/FractionalRatingBar;->setRating(F)V

    .line 2452129
    iget-object v7, p4, LX/HLs;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v7, v6}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2452130
    iget-object v4, p2, LX/HJA;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v4}, LX/HLs;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2452131
    const/16 v1, 0x1f

    const v2, -0x6d595dce

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2452123
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2452124
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 2452125
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/9uc;->cq()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cz()LX/174;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->cz()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2452120
    check-cast p4, LX/HLs;

    .line 2452121
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/HLs;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2452122
    return-void
.end method
