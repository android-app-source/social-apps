.class public Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/HIj;

.field private final e:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/HIj;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2451665
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2451666
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;->d:LX/HIj;

    .line 2451667
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;->e:LX/0Uh;

    .line 2451668
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2451652
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;->d:LX/HIj;

    const/4 v1, 0x0

    .line 2451653
    new-instance v2, LX/HIi;

    invoke-direct {v2, v0}, LX/HIi;-><init>(LX/HIj;)V

    .line 2451654
    sget-object p0, LX/HIj;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/HIh;

    .line 2451655
    if-nez p0, :cond_0

    .line 2451656
    new-instance p0, LX/HIh;

    invoke-direct {p0}, LX/HIh;-><init>()V

    .line 2451657
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/HIh;->a$redex0(LX/HIh;LX/1De;IILX/HIi;)V

    .line 2451658
    move-object v2, p0

    .line 2451659
    move-object v1, v2

    .line 2451660
    move-object v0, v1

    .line 2451661
    iget-object v1, v0, LX/HIh;->a:LX/HIi;

    iput-object p2, v1, LX/HIi;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2451662
    iget-object v1, v0, LX/HIh;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2451663
    move-object v0, v0

    .line 2451664
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;
    .locals 6

    .prologue
    .line 2451641
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;

    monitor-enter v1

    .line 2451642
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2451643
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2451644
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2451645
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2451646
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/HIj;->a(LX/0QB;)LX/HIj;

    move-result-object v4

    check-cast v4, LX/HIj;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;-><init>(Landroid/content/Context;LX/HIj;LX/0Uh;)V

    .line 2451647
    move-object v0, p0

    .line 2451648
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2451649
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2451650
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2451651
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2451669
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2451640
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;->a(LX/1De;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2451637
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v0, 0x0

    .line 2451638
    iget-object v1, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v1, v1

    .line 2451639
    iget-object v2, p0, Lcom/facebook/pages/common/reaction/components/PageAboutPaymentOptionsComponentComponentPartDefinition;->e:LX/0Uh;

    const/16 v3, 0x5af

    invoke-virtual {v2, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/9uc;->ce()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$PrimaryIconModel;->a()LX/5sY;

    move-result-object v2

    invoke-interface {v2}, LX/5sY;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1}, LX/9uc;->bM()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2451634
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2451635
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2451636
    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
