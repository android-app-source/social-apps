.class public Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;

.field private final b:Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentPartDefinition;

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453211
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2453212
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;->a:Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;

    .line 2453213
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;->b:Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentPartDefinition;

    .line 2453214
    iput-object p3, p0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;->c:LX/0Uh;

    .line 2453215
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;
    .locals 6

    .prologue
    .line 2453216
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;

    monitor-enter v1

    .line 2453217
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453218
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453219
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453220
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453221
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;-><init>(Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentPartDefinition;LX/0Uh;)V

    .line 2453222
    move-object v0, p0

    .line 2453223
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453224
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453225
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453226
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2453227
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2453228
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;->c:LX/0Uh;

    const/16 v1, 0x41c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2453229
    if-eqz v0, :cond_0

    .line 2453230
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;->b:Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2453231
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2453232
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;->a:Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2453233
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2453234
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentSelectorPartDefinition;->c:LX/0Uh;

    const/16 v1, 0x41c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2453235
    if-eqz v0, :cond_0

    .line 2453236
    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    .line 2453237
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/facebook/pages/common/reaction/components/PageInlineComposerUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;)Z

    move-result v0

    goto :goto_0
.end method
