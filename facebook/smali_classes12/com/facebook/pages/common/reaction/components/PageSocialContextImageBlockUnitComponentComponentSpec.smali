.class public Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/E1f;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2454721
    const-class v0, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;

    const-string v1, "pages_public_view"

    const-string v2, "thumbnail"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/E1f;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2454722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2454723
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;->b:LX/1nu;

    .line 2454724
    iput-object p2, p0, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;->c:LX/E1f;

    .line 2454725
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;
    .locals 5

    .prologue
    .line 2454710
    const-class v1, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;

    monitor-enter v1

    .line 2454711
    :try_start_0
    sget-object v0, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2454712
    sput-object v2, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2454713
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2454714
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2454715
    new-instance p0, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/E1f;->a(LX/0QB;)LX/E1f;

    move-result-object v4

    check-cast v4, LX/E1f;

    invoke-direct {p0, v3, v4}, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;-><init>(LX/1nu;LX/E1f;)V

    .line 2454716
    move-object v0, p0

    .line 2454717
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2454718
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageSocialContextImageBlockUnitComponentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2454719
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2454720
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
