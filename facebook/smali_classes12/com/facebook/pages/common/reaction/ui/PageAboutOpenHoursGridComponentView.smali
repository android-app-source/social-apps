.class public Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/widget/TableLayout;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2456325
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2456271
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456272
    const v0, 0x7f030e03

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456273
    const v0, 0x7f0d223a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2456274
    const v0, 0x7f0d223b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->c:Landroid/widget/TableLayout;

    .line 2456275
    const v0, 0x7f0d2239

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2456276
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2456277
    const v1, 0x7f0213ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2456278
    const v1, 0x7f0b0d5e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0b0d5f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0b0d5e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v4, 0x7f0b0d5f

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->setPadding(IIII)V

    .line 2456279
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object p0, p1, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->a:LX/0ad;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2456280
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 2456281
    :cond_0
    return-void

    .line 2456282
    :cond_1
    iget-object v2, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->c:Landroid/widget/TableLayout;

    invoke-virtual {v2}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 2456283
    iget-object v2, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2456284
    new-instance v3, Landroid/text/SpannableString;

    move-object/from16 v0, p3

    invoke-direct {v3, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2456285
    new-instance v4, Landroid/text/SpannableString;

    move-object/from16 v0, p4

    invoke-direct {v4, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2456286
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0a00ab

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 2456287
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v5, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-virtual {v3, v5, v2, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2456288
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_AVAILABLE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-object/from16 v0, p5

    if-ne v0, v2, :cond_2

    .line 2456289
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0a0092

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 2456290
    :goto_0
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v5, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2456291
    iget-object v2, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0836a3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/CharSequence;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v3, 0x1

    aput-object v4, v6, v3

    invoke-static {v5, v6}, Landroid/text/TextUtils;->expandTemplate(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456292
    iget-object v2, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    sget-object v3, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v4, LX/0xr;->REGULAR:LX/0xr;

    iget-object v5, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5}, Lcom/facebook/widget/text/BetterTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2456293
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2456294
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2456295
    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v8

    .line 2456296
    const-class v2, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;

    invoke-static {v2, p0}, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2456297
    iget-object v2, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->a:LX/0ad;

    sget-char v3, LX/8Dn;->a:C

    const-string v4, "Today"

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2456298
    const-string v3, "Sunday"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2456299
    const/4 v2, 0x1

    move v7, v2

    .line 2456300
    :goto_1
    const/4 v2, 0x0

    move v9, v2

    :goto_2
    int-to-long v2, v9

    const-wide/16 v4, 0x7

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 2456301
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 2456302
    const v3, 0x7f030e06

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TableRow;

    .line 2456303
    const v3, 0x7f0d2240

    invoke-virtual {v2, v3}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/text/BetterTextView;

    .line 2456304
    const v4, 0x7f0d2241

    invoke-virtual {v2, v4}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/text/BetterTextView;

    .line 2456305
    add-int v5, v9, v7

    add-int/lit8 v5, v5, 0x5

    int-to-long v10, v5

    const-wide/16 v12, 0x7

    rem-long/2addr v10, v12

    long-to-int v5, v10

    .line 2456306
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 2456307
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v10, 0x3

    if-ne v6, v10, :cond_0

    .line 2456308
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v3, v6}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456309
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456310
    sub-int v5, v8, v7

    int-to-long v10, v5

    const-wide/16 v12, 0x7

    add-long/2addr v10, v12

    const-wide/16 v12, 0x7

    rem-long/2addr v10, v12

    .line 2456311
    long-to-int v5, v10

    if-ne v9, v5, :cond_6

    .line 2456312
    sget-object v5, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v6, LX/0xr;->BOLD:LX/0xr;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v10

    invoke-static {v3, v5, v6, v10}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2456313
    sget-object v3, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v5, LX/0xr;->BOLD:LX/0xr;

    invoke-virtual {v4}, Lcom/facebook/widget/text/BetterTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v6

    invoke-static {v4, v3, v5, v6}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2456314
    :goto_3
    iget-object v3, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->c:Landroid/widget/TableLayout;

    invoke-virtual {v3, v2}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 2456315
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto/16 :goto_2

    .line 2456316
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-object/from16 v0, p5

    if-ne v0, v2, :cond_3

    .line 2456317
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0a0095

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto/16 :goto_0

    .line 2456318
    :cond_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->SHOW_UNDETERMINED:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-object/from16 v0, p5

    if-ne v0, v2, :cond_4

    .line 2456319
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0a014a

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto/16 :goto_0

    .line 2456320
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageAboutOpenHoursGridComponentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0a00a8

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto/16 :goto_0

    .line 2456321
    :cond_5
    const-string v3, "Monday"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2456322
    const/4 v2, 0x2

    move v7, v2

    goto/16 :goto_1

    .line 2456323
    :cond_6
    sget-object v5, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v6, LX/0xr;->REGULAR:LX/0xr;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v10

    invoke-static {v3, v5, v6, v10}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2456324
    sget-object v3, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v5, LX/0xr;->REGULAR:LX/0xr;

    invoke-virtual {v4}, Lcom/facebook/widget/text/BetterTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v6

    invoke-static {v4, v3, v5, v6}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    goto :goto_3

    :cond_7
    move v7, v8

    goto/16 :goto_1
.end method
