.class public Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Lcom/facebook/widget/text/BetterTextView;

.field private final e:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2456399
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2456400
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456401
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->setOrientation(I)V

    .line 2456402
    const v0, 0x7f030ec4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456403
    const v0, 0x7f0d240d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2456404
    const v0, 0x7f0d240e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2456405
    const v0, 0x7f0d240f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2456406
    const v0, 0x7f0d2410

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2456407
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2456397
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456398
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2456380
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2456381
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2456382
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2456383
    :goto_0
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2456384
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456385
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2456386
    :goto_1
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2456387
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456388
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2456389
    :goto_2
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2456390
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456391
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2456392
    :goto_3
    return-void

    .line 2456393
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0

    .line 2456394
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1

    .line 2456395
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_2

    .line 2456396
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageCardComponentView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_3
.end method
