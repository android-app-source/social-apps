.class public Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Landroid/view/LayoutInflater;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2456751
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2456752
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456753
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2456754
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->b:Landroid/view/LayoutInflater;

    .line 2456755
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->c:Ljava/util/List;

    .line 2456756
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->setOrientation(I)V

    .line 2456757
    return-void
.end method

.method private c(I)I
    .locals 1

    .prologue
    .line 2456758
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/HKX;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, -0x2

    const/high16 v6, 0x3fa00000    # 1.25f

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2456759
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, LX/HM6;->fromPhotoCount(I)LX/HM6;

    move-result-object v0

    .line 2456760
    sget-object v1, LX/HM5;->a:[I

    invoke-virtual {v0}, LX/HM6;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2456761
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported photo count: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2456762
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->removeAllViews()V

    .line 2456763
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2456764
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030e9f

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2456765
    const v0, 0x7f0d23cd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2456766
    invoke-virtual {v0, v6}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2456767
    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2456768
    iput v8, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2456769
    const/high16 v2, 0x40000000    # 2.0f

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2456770
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2456771
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HKX;

    iget-object v1, v1, LX/HKX;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2456772
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HKX;

    iget-object v1, v1, LX/HKX;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456773
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HKX;

    iget-object v1, v1, LX/HKX;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2456774
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2456775
    const v0, 0x7f0d23ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2456776
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2456777
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2456778
    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2456779
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v2, v3

    .line 2456780
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_5

    .line 2456781
    new-instance v5, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2456782
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v6, v1, v4, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2456783
    if-ne v2, v3, :cond_0

    move v1, v3

    .line 2456784
    :goto_1
    if-eqz v1, :cond_1

    move v1, v4

    .line 2456785
    :goto_2
    invoke-virtual {v6, v4, v1, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2456786
    invoke-virtual {v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2456787
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HKX;

    iget-object v1, v1, LX/HKX;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v6, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v1, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2456788
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HKX;

    iget-object v1, v1, LX/HKX;->e:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2456789
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HKX;

    iget-object v1, v1, LX/HKX;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456790
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->c:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2456791
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2456792
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    move v1, v4

    .line 2456793
    goto :goto_1

    .line 2456794
    :cond_1
    const v1, 0x7f0b0d6a

    invoke-direct {p0, v1}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->c(I)I

    move-result v1

    goto :goto_2

    .line 2456795
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->removeAllViews()V

    .line 2456796
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    move v1, v4

    .line 2456797
    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2456798
    new-instance v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2456799
    invoke-virtual {v2, v6}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2456800
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v4, v8, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2456801
    if-nez v1, :cond_2

    move v0, v3

    .line 2456802
    :goto_4
    if-eqz v0, :cond_3

    move v0, v4

    .line 2456803
    :goto_5
    invoke-virtual {v5, v0, v4, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2456804
    invoke-virtual {v2, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2456805
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HKX;

    iget-object v0, v0, LX/HKX;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v5, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2456806
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HKX;

    iget-object v0, v0, LX/HKX;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2456807
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HKX;

    iget-object v0, v0, LX/HKX;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456808
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2456809
    invoke-virtual {p0, v2}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->addView(Landroid/view/View;)V

    .line 2456810
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_2
    move v0, v4

    .line 2456811
    goto :goto_4

    .line 2456812
    :cond_3
    const v0, 0x7f0b0d6a

    invoke-direct {p0, v0}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->c(I)I

    move-result v0

    goto :goto_5

    .line 2456813
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    rsub-int/lit8 v1, v0, 0x3

    move v0, v4

    .line 2456814
    :goto_6
    if-ge v0, v1, :cond_5

    .line 2456815
    new-instance v2, Landroid/widget/Space;

    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    .line 2456816
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2456817
    const v5, 0x7f0b0d6a

    invoke-direct {p0, v5}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->c(I)I

    move-result v5

    invoke-virtual {v3, v5, v4, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2456818
    invoke-virtual {v2, v3}, Landroid/widget/Space;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2456819
    invoke-virtual {p0, v2}, Lcom/facebook/pages/common/reaction/ui/PagePhotosComponentView;->addView(Landroid/view/View;)V

    .line 2456820
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 2456821
    :cond_5
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
