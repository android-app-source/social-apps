.class public Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final c:I

.field private final d:I

.field public final e:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public final f:Lcom/facebook/fig/starrating/FigStarRatingBar;

.field public final g:Lcom/facebook/widget/text/BetterTextView;

.field public final h:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2456488
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2456489
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456490
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2456491
    const v1, 0x7f0b0d5f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->c:I

    .line 2456492
    const v1, 0x7f0b0d5e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->d:I

    .line 2456493
    const v1, 0x7f030e7e

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456494
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f0a00fc

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2456495
    const v1, 0x7f0b0033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2456496
    iget v1, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->c:I

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerPadding(I)V

    .line 2456497
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2456498
    const v1, 0x7f0213ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2456499
    iget v0, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->d:I

    iget v1, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->c:I

    iget v2, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->d:I

    iget v3, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->c:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->setPadding(IIII)V

    .line 2456500
    const v0, 0x7f0d2379

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2456501
    const v0, 0x7f0d2376

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->e:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2456502
    const v0, 0x7f0d2377

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2456503
    const v0, 0x7f0d2378

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2456504
    const v0, 0x7f0d2375

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/starrating/FigStarRatingBar;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoRatingsAndReviewsComponentView;->f:Lcom/facebook/fig/starrating/FigStarRatingBar;

    .line 2456505
    return-void
.end method
