.class public Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2456711
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 2456712
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456713
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2456714
    invoke-virtual {p0, v3}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->setOrientation(I)V

    .line 2456715
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->b:Ljava/util/List;

    .line 2456716
    const/4 v0, 0x3

    const/4 v8, -0x2

    const/4 v2, 0x0

    .line 2456717
    move v4, v2

    :goto_0
    if-ge v4, v0, :cond_2

    .line 2456718
    new-instance v5, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;-><init>(Landroid/content/Context;)V

    .line 2456719
    const v1, 0x7f0d23c4

    invoke-static {v5, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2456720
    const/high16 v6, 0x3fa00000    # 1.25f

    invoke-virtual {v1, v6}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2456721
    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2456722
    iput v8, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2456723
    if-nez v4, :cond_0

    const/4 v1, 0x1

    .line 2456724
    :goto_1
    if-eqz v1, :cond_1

    const v1, 0x7f0b0d69

    invoke-static {p0, v1}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->d(Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;I)I

    move-result v1

    .line 2456725
    :goto_2
    const/high16 v6, 0x3f800000    # 1.0f

    const v7, 0x7f0b0d69

    invoke-static {p0, v7}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->d(Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;I)I

    move-result v7

    const/4 p1, 0x0

    .line 2456726
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v9, v2, v8, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 2456727
    invoke-virtual {v9, v1, p1, v7, p1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2456728
    move-object v1, v9

    .line 2456729
    invoke-virtual {v5, v1}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2456730
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->b:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2456731
    invoke-virtual {p0, v5}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->addView(Landroid/view/View;)V

    .line 2456732
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_0
    move v1, v2

    .line 2456733
    goto :goto_1

    :cond_1
    move v1, v2

    .line 2456734
    goto :goto_2

    .line 2456735
    :cond_2
    const v0, 0x7f0b0d69

    invoke-static {p0, v0}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->d(Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;I)I

    move-result v0

    invoke-virtual {p0, v3, v3, v3, v0}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->setPadding(IIII)V

    .line 2456736
    return-void
.end method

.method public static d(Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;I)I
    .locals 1

    .prologue
    .line 2456710
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/HKM;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2456693
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2456694
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Albums count inconsistent between data and view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2456695
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2456696
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HKM;

    .line 2456697
    iget-object v1, v0, LX/HKM;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2456698
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;

    iget-object v3, v0, LX/HKM;->b:Ljava/lang/String;

    iget-object v4, v0, LX/HKM;->c:Ljava/lang/String;

    iget-object v5, v0, LX/HKM;->d:Ljava/lang/String;

    .line 2456699
    iget-object v6, v1, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->b:Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v7, v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2456700
    iget-object v6, v1, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v6, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456701
    iget-object v6, v1, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v6, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456702
    :goto_1
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;

    iget-object v0, v0, LX/HKM;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456703
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2456704
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumsComponentView;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;

    const v3, 0x7f020626

    iget-object v4, v0, LX/HKM;->c:Ljava/lang/String;

    iget-object v5, v0, LX/HKM;->d:Ljava/lang/String;

    .line 2456705
    iget-object v6, v1, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->b:Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;

    invoke-virtual {v6, v3}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->setImageResource(I)V

    .line 2456706
    iget-object v6, v1, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v6, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456707
    iget-object v6, v1, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v6, v5}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456708
    goto :goto_1

    .line 2456709
    :cond_2
    return-void
.end method
