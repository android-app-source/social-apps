.class public Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final g:Ljava/lang/String;

.field private static final h:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HLh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HLl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final i:Lcom/facebook/widget/text/BetterTextView;

.field public final j:Lcom/facebook/widget/text/BetterTextView;

.field public final k:Lcom/facebook/widget/text/BetterTextView;

.field public final l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final m:Landroid/widget/FrameLayout;

.field public final n:Lcom/facebook/resources/ui/FbButton;

.field public final o:Landroid/widget/ImageView;

.field public final p:Lcom/facebook/widget/text/BetterTextView;

.field public q:LX/HM0;

.field public r:LX/HLj;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2456642
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->g:Ljava/lang/String;

    .line 2456643
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->h:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 2456627
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456628
    const v0, 0x7f030e98

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456629
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->setOrientation(I)V

    .line 2456630
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->setBackgroundResource(I)V

    .line 2456631
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 2456632
    const v0, 0x7f0d23bf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 2456633
    const v0, 0x7f0d23c0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2456634
    const v0, 0x7f0d23c1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 2456635
    const v0, 0x7f0d23bd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2456636
    const v0, 0x7f0d23bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->m:Landroid/widget/FrameLayout;

    .line 2456637
    const v0, 0x7f0d23c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->n:Lcom/facebook/resources/ui/FbButton;

    .line 2456638
    const v0, 0x7f0d23be

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->o:Landroid/widget/ImageView;

    .line 2456639
    const v0, 0x7f0d23bb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 2456640
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;

    const/16 v3, 0x509

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xb83

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    new-instance v5, LX/HLh;

    invoke-direct {v5}, LX/HLh;-><init>()V

    move-object v5, v5

    move-object v5, v5

    check-cast v5, LX/HLh;

    invoke-static {v0}, LX/HLl;->a(LX/0QB;)LX/HLl;

    move-result-object v6

    check-cast v6, LX/HLl;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v3, v2, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->a:LX/0Or;

    iput-object v4, v2, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->b:LX/0Or;

    iput-object v5, v2, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->c:LX/HLh;

    iput-object v6, v2, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->d:LX/HLl;

    iput-object p1, v2, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->e:Ljava/util/concurrent/ExecutorService;

    iput-object v0, v2, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->f:LX/03V;

    .line 2456641
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;LX/174;LX/174;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/174;",
            "LX/174;",
            "LX/0Px",
            "<+",
            "LX/174;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2456613
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-interface {p1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456614
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-interface {p2}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456615
    invoke-virtual {p3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2456616
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->k:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2456617
    :goto_0
    return-void

    .line 2456618
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move v1, v2

    .line 2456619
    :goto_1
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2456620
    const-string v0, " \u2022 "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2456621
    invoke-virtual {p3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/174;

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2456622
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_1

    .line 2456623
    sget-object v0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->g:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2456624
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2456625
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2456626
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;LX/1Fb;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2456606
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->m:Landroid/widget/FrameLayout;

    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 2456607
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->h:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-interface {p1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2456608
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2456609
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2456610
    invoke-interface {p1}, LX/1Fb;->c()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2456611
    invoke-interface {p1}, LX/1Fb;->a()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2456612
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;Landroid/view/View$OnClickListener;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2456600
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456601
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2456602
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456603
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456604
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->o:Landroid/widget/ImageView;

    new-instance v1, LX/HM1;

    invoke-direct {v1, p0, p5, p6}, LX/HM1;-><init>(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456605
    return-void
.end method

.method private static getNuxViewState(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;)LX/HLk;
    .locals 3

    .prologue
    .line 2456575
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->d:LX/HLl;

    iget-object v1, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->r:LX/HLj;

    .line 2456576
    iget-object v2, v1, LX/HLj;->b:Ljava/lang/String;

    invoke-static {v0, v2}, LX/HLl;->b(LX/HLl;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    iget-object p0, v1, LX/HLj;->a:Ljava/lang/String;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HLk;

    move-object v0, v2

    .line 2456577
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/HLk;->NUX_CAN_SHOW:LX/HLk;

    goto :goto_0
.end method

.method public static setState(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;LX/HLk;)V
    .locals 5

    .prologue
    .line 2456586
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->d:LX/HLl;

    iget-object v1, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->r:LX/HLj;

    iget-object v2, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->q:LX/HM0;

    .line 2456587
    iget-object v3, v1, LX/HLj;->b:Ljava/lang/String;

    invoke-static {v0, v3}, LX/HLl;->b(LX/HLl;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    iget-object v4, v1, LX/HLj;->a:Ljava/lang/String;

    invoke-interface {v3, v4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2456588
    iget-object v3, v2, LX/HM0;->a:Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;

    const/16 v4, 0x8

    const/4 p0, 0x0

    .line 2456589
    sget-object v0, LX/HM2;->a:[I

    invoke-virtual {p1}, LX/HLk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move p0, v4

    .line 2456590
    :goto_0
    :pswitch_0
    iget-object v0, v3, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2456591
    iget-object v0, v3, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2456592
    iget-object v0, v3, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2456593
    iget-object v0, v3, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2456594
    iget-object v0, v3, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->m:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2456595
    iget-object v0, v3, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2456596
    iget-object v0, v3, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2456597
    iget-object p0, v3, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2456598
    return-void

    :pswitch_1
    move v2, p0

    move p0, v4

    move v4, v2

    .line 2456599
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/HLj;LX/174;LX/174;LX/0Px;LX/1Fb;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HLj;",
            "LX/174;",
            "LX/174;",
            "LX/0Px",
            "<+",
            "LX/174;",
            ">;",
            "LX/1Fb;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/view/View$OnClickListener;",
            "Ljava/lang/String;",
            "Landroid/view/View$OnClickListener;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2456578
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2456579
    iput-object p1, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->r:LX/HLj;

    .line 2456580
    new-instance v0, LX/HM0;

    invoke-direct {v0, p0}, LX/HM0;-><init>(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->q:LX/HM0;

    .line 2456581
    invoke-static {p0, p5, p6}, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->a(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;LX/1Fb;Ljava/lang/String;)V

    .line 2456582
    invoke-static {p0, p2, p3, p4}, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->a(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;LX/174;LX/174;LX/0Px;)V

    .line 2456583
    iget-object v5, p1, LX/HLj;->b:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p8

    move-object/from16 v2, p9

    move-object/from16 v3, p10

    move-object/from16 v4, p11

    move-object v6, p7

    invoke-static/range {v0 .. v6}, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->a(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;Landroid/view/View$OnClickListener;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2456584
    invoke-static {p0}, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->getNuxViewState(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;)LX/HLk;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;->setState(Lcom/facebook/pages/common/reaction/ui/PageNuxComponentView;LX/HLk;)V

    .line 2456585
    return-void
.end method
