.class public Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2456251
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2456252
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456253
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2456254
    const v1, 0x7f0213ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2456255
    const v1, 0x7f0b0d5e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0b0d5f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0b0d5e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v4, 0x7f0b0d5f

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;->setPadding(IIII)V

    .line 2456256
    const v0, 0x7f030e02

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456257
    const v0, 0x7f0d2238

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    .line 2456258
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    sget-object v1, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v2, LX/0xr;->REGULAR:LX/0xr;

    iget-object v3, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-virtual {v3}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2456259
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAboutDescriptionComponentView;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->a()V

    .line 2456260
    return-void
.end method
