.class public Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public k:Lcom/facebook/widget/text/BetterTextView;

.field public l:Lcom/facebook/widget/text/BetterTextView;

.field public m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public n:Lcom/facebook/resources/ui/FbButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2456442
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2456443
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2456444
    const v0, 0x7f030e43

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2456445
    const v0, 0x7f0d22c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 2456446
    const v0, 0x7f0d22c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 2456447
    const v0, 0x7f0d22c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2456448
    const v0, 0x7f0d22c6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageContextualRecommendationFooterComponentView;->n:Lcom/facebook/resources/ui/FbButton;

    .line 2456449
    return-void
.end method
