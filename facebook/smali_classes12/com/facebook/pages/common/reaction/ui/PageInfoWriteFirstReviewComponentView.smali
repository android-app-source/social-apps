.class public Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2456506
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2456507
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456508
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2456509
    const v1, 0x7f0b0d5f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2456510
    const v2, 0x7f0b0d5e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2456511
    const v3, 0x7f030e80

    invoke-virtual {p0, v3}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456512
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;->setOrientation(I)V

    .line 2456513
    const v3, 0x7f0213ab

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2456514
    invoke-virtual {p0, v2, v1, v2, v1}, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;->setPadding(IIII)V

    .line 2456515
    const v0, 0x7f0d237b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2456516
    const v0, 0x7f0d237c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageInfoWriteFirstReviewComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2456517
    return-void
.end method
