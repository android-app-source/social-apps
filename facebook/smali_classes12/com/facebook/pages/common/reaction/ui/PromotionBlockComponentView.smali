.class public Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final k:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final a:Lcom/facebook/resources/ui/FbTextView;

.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Lcom/facebook/resources/ui/FbTextView;

.field public final d:Lcom/facebook/resources/ui/FbTextView;

.field public final e:Lcom/facebook/resources/ui/FbTextView;

.field public final f:Lcom/facebook/resources/ui/FbTextView;

.field public final g:Lcom/facebook/resources/ui/FbTextView;

.field public final h:Lcom/facebook/resources/ui/FbTextView;

.field public final i:Lcom/facebook/resources/ui/FbTextView;

.field public final j:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2456823
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;

    const-string v1, "page_reaction_fragment"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->k:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2456824
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456825
    const v0, 0x7f031097

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456826
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->setOrientation(I)V

    .line 2456827
    const v0, 0x7f0213bc

    invoke-static {p1, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2456828
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0d5f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2456829
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0d5e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2456830
    invoke-virtual {p0, v1, v0, v1, v0}, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->setPadding(IIII)V

    .line 2456831
    const v0, 0x7f0d090e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2456832
    const v0, 0x7f0d278f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2456833
    const v0, 0x7f0d2790

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2456834
    const v0, 0x7f0d1913

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2456835
    const v0, 0x7f0d0432

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2456836
    const v0, 0x7f0d0433

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2456837
    const v0, 0x7f0d0434

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2456838
    const v0, 0x7f0d0435

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2456839
    const v0, 0x7f0d2792

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 2456840
    const v0, 0x7f0d2791

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PromotionBlockComponentView;->j:Landroid/view/View;

    .line 2456841
    return-void
.end method
