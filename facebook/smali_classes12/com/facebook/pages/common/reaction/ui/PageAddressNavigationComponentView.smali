.class public Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Lcom/facebook/widget/text/BetterTextView;

.field public final e:Landroid/view/View;

.field public final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field public final h:Landroid/view/View;

.field public final i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2456326
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2456327
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456328
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->setOrientation(I)V

    .line 2456329
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2456330
    const v1, 0x7f0b0d5f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->a:I

    .line 2456331
    const v1, 0x7f0b0d5e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->b:I

    .line 2456332
    const v1, 0x7f030e0f

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456333
    const v1, 0x7f0213ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2456334
    iget v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->b:I

    iget v1, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->a:I

    iget v2, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->b:I

    iget v3, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->a:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->setPadding(IIII)V

    .line 2456335
    const v0, 0x7f0d2261

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2456336
    const v0, 0x7f0d2262

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2456337
    const v0, 0x7f0d2264

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->e:Landroid/view/View;

    .line 2456338
    const v0, 0x7f0d2263

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->f:Landroid/view/View;

    .line 2456339
    const v0, 0x7f0d2265

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->g:Landroid/view/View;

    .line 2456340
    const v0, 0x7f0d2266

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->h:Landroid/view/View;

    .line 2456341
    const v0, 0x7f0d225f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2456342
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2456343
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 2456344
    :goto_0
    iget-object v3, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456345
    if-eqz v0, :cond_0

    .line 2456346
    iget-object v3, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, p3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456347
    :cond_0
    iget-object v3, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2456348
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez p1, :cond_4

    :goto_2
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2456349
    if-eqz p1, :cond_1

    .line 2456350
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2456351
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 2456352
    goto :goto_0

    :cond_3
    move v0, v2

    .line 2456353
    goto :goto_1

    :cond_4
    move v2, v1

    .line 2456354
    goto :goto_2
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2456355
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456356
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->f:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456357
    iget-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PageAddressNavigationComponentView;->g:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456358
    return-void
.end method
