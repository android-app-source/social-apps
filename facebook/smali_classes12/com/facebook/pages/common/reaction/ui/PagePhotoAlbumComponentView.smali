.class public Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;

.field public final c:Lcom/facebook/widget/text/BetterTextView;

.field public final d:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2456683
    const-class v0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2456684
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456685
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->setOrientation(I)V

    .line 2456686
    const v0, 0x7f030e9b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456687
    const v0, 0x7f0d23c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->b:Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;

    .line 2456688
    invoke-virtual {p0}, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020089

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2456689
    iget-object v1, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->b:Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/pages/common/ui/widgets/AlbumWithOffsetShadow;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2456690
    const v0, 0x7f0d23c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2456691
    const v0, 0x7f0d23c6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/reaction/ui/PagePhotoAlbumComponentView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2456692
    return-void
.end method
