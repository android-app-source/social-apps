.class public final Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2629863
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2629864
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2629861
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2629862
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2629853
    if-nez p1, :cond_0

    .line 2629854
    :goto_0
    return v0

    .line 2629855
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2629856
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2629857
    :pswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2629858
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2629859
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2629860
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x6140b1e0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2629852
    new-instance v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2629849
    packed-switch p0, :pswitch_data_0

    .line 2629850
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2629851
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x6140b1e0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2629848
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2629846
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;->b(I)V

    .line 2629847
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2629841
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2629842
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2629843
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;->a:LX/15i;

    .line 2629844
    iput p2, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;->b:I

    .line 2629845
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2629815
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2629840
    new-instance v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2629837
    iget v0, p0, LX/1vt;->c:I

    .line 2629838
    move v0, v0

    .line 2629839
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2629834
    iget v0, p0, LX/1vt;->c:I

    .line 2629835
    move v0, v0

    .line 2629836
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2629831
    iget v0, p0, LX/1vt;->b:I

    .line 2629832
    move v0, v0

    .line 2629833
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2629828
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2629829
    move-object v0, v0

    .line 2629830
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2629819
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2629820
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2629821
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2629822
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2629823
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2629824
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2629825
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2629826
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2629827
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2629816
    iget v0, p0, LX/1vt;->c:I

    .line 2629817
    move v0, v0

    .line 2629818
    return v0
.end method
