.class public final Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4554a605
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2630280
    const-class v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2630259
    const-class v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2630281
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2630282
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2630266
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2630267
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2630268
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2630269
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2630270
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2630271
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2630272
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2630273
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2630274
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    .line 2630275
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2630276
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    .line 2630277
    iput-object v0, v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    .line 2630278
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2630279
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageBrowserCategories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2630264
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    .line 2630265
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2630261
    new-instance v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    invoke-direct {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;-><init>()V

    .line 2630262
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2630263
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2630260
    const v0, -0x67a5b389

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2630258
    const v0, -0x6747e1ce

    return v0
.end method
