.class public final Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7095e97
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Z

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2630031
    const-class v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2630032
    const-class v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2630033
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2630034
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2630035
    iput-boolean p1, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->g:Z

    .line 2630036
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2630037
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2630038
    if-eqz v0, :cond_0

    .line 2630039
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2630040
    :cond_0
    return-void
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2630041
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    .line 2630042
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    return-object v0
.end method

.method private p()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2630043
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->o:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->o:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    .line 2630044
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->o:Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 2630045
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2630046
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 2630047
    invoke-direct {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->o()Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2630048
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2630049
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2630050
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2630051
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->m()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, -0x6140b1e0

    invoke-static {v6, v5, v7}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2630052
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2630053
    invoke-direct {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->p()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 2630054
    const/16 v8, 0xb

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2630055
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 2630056
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2630057
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2630058
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2630059
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2630060
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2630061
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->k:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2630062
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2630063
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2630064
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2630065
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2630066
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2630067
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2629983
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2629984
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2629985
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    .line 2629986
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2629987
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;

    .line 2629988
    iput-object v0, v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->j:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    .line 2629989
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2629990
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x6140b1e0

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2629991
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2629992
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;

    .line 2629993
    iput v3, v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->m:I

    move-object v1, v0

    .line 2629994
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2629995
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2629996
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2629997
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;

    .line 2629998
    iput-object v0, v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2629999
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2630000
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2630001
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2630002
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2630068
    new-instance v0, LX/IwJ;

    invoke-direct {v0, p1}, LX/IwJ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2630069
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2630025
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2630026
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->g:Z

    .line 2630027
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->h:Z

    .line 2630028
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->k:Z

    .line 2630029
    const/16 v0, 0x8

    const v1, -0x6140b1e0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->m:I

    .line 2630030
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2630070
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2630071
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2630072
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2630073
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 2630074
    :goto_0
    return-void

    .line 2630075
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2630022
    const-string v0, "does_viewer_like"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2630023
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->a(Z)V

    .line 2630024
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2630019
    new-instance v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;

    invoke-direct {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;-><init>()V

    .line 2630020
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2630021
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 2630017
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2630018
    iget-boolean v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->g:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2630015
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->i:Ljava/lang/String;

    .line 2630016
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2630014
    const v0, 0xeb95f3f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2630013
    const v0, 0x25d6af

    return v0
.end method

.method public final j()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2630011
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->e:Ljava/util/List;

    .line 2630012
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2630009
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->j:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->j:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    .line 2630010
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->j:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2630007
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->l:Ljava/lang/String;

    .line 2630008
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageLikers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2630005
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2630006
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->m:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2630003
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2630004
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->n:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method
