.class public final Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2629777
    const-class v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    new-instance v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2629778
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2629779
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2629780
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2629781
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2629782
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2629783
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2629784
    if-eqz v2, :cond_0

    .line 2629785
    const-string p0, "page_browser_categories"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2629786
    invoke-static {v1, v2, p1, p2}, LX/IwN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2629787
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2629788
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2629789
    check-cast p1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$Serializer;->a(Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;LX/0nX;LX/0my;)V

    return-void
.end method
