.class public final Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6722c3c3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2629806
    const-class v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2629805
    const-class v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2629803
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2629804
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2629797
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2629798
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2629799
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2629800
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2629801
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2629802
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2629807
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2629808
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2629809
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    .line 2629810
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2629811
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    .line 2629812
    iput-object v0, v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    .line 2629813
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2629814
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageBrowserCategories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2629790
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    .line 2629791
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2629792
    new-instance v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    invoke-direct {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;-><init>()V

    .line 2629793
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2629794
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2629795
    const v0, -0x6be368fe

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2629796
    const v0, -0x6747e1ce

    return v0
.end method
