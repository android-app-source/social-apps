.class public final Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x561676ec
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2629741
    const-class v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2629742
    const-class v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2629739
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2629740
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2629729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2629730
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2629731
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2629732
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2629733
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2629734
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2629735
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2629736
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2629737
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2629738
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2629721
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2629722
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2629723
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    .line 2629724
    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2629725
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    .line 2629726
    iput-object v0, v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->g:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    .line 2629727
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2629728
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2629743
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->e:Ljava/lang/String;

    .line 2629744
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2629718
    new-instance v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;-><init>()V

    .line 2629719
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2629720
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2629717
    const v0, 0x7e208dfd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2629716
    const v0, -0x2c7e591b

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2629712
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->f:Ljava/lang/String;

    .line 2629713
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSuggestedPages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2629714
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->g:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    iput-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->g:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    .line 2629715
    iget-object v0, p0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->g:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    return-object v0
.end method
