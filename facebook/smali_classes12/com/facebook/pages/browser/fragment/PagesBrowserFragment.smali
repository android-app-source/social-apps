.class public Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;
.super Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;
.source ""


# instance fields
.field public e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

.field public f:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

.field private g:Landroid/view/View;

.field private h:Lcom/facebook/widget/listview/BetterListView;

.field private i:LX/1B1;

.field public j:LX/Iwb;

.field public k:LX/0zG;

.field private l:LX/0h5;

.field public m:LX/03V;

.field public n:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

.field private o:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/IwC;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2630884
    invoke-direct {p0}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    invoke-static {p0}, LX/Iwb;->a(LX/0QB;)LX/Iwb;

    move-result-object v1

    check-cast v1, LX/Iwb;

    invoke-static {p0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v2

    check-cast v2, LX/0zG;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/IwC;->b(LX/0QB;)LX/IwC;

    move-result-object p0

    check-cast p0, LX/IwC;

    iput-object v1, p1, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->j:LX/Iwb;

    iput-object v2, p1, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->k:LX/0zG;

    iput-object v3, p1, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->m:LX/03V;

    iput-object p0, p1, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->p:LX/IwC;

    return-void
.end method

.method public static b(Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;)Z
    .locals 1

    .prologue
    .line 2630883
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2630878
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->a:LX/IwD;

    const/4 v2, 0x0

    .line 2630879
    const-string v1, "fetch_initial_recommended_pages"

    invoke-static {v0, v2, v2, v1}, LX/IwD;->a(LX/IwD;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2630880
    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2630881
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->b:LX/1Ck;

    const-string v1, "pages_browser_fetch_initial_recommended_pages"

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/Iwi;

    invoke-direct {v3, p0}, LX/Iwi;-><init>(Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2630882
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2630873
    invoke-super {p0, p1}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->a(Landroid/os/Bundle;)V

    .line 2630874
    const-class v0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2630875
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->k:LX/0zG;

    if-eqz v0, :cond_0

    .line 2630876
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->k:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->l:LX/0h5;

    .line 2630877
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2630871
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->p:LX/IwC;

    const v1, -0x4daf48f5

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2630872
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x2a72f980

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2630846
    const v0, 0x7f030251

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->g:Landroid/view/View;

    .line 2630847
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->i:LX/1B1;

    .line 2630848
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->i:LX/1B1;

    new-instance v2, LX/Iwh;

    invoke-direct {v2, p0}, LX/Iwh;-><init>(Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;)V

    invoke-virtual {v0, v2}, LX/1B1;->a(LX/0b2;)Z

    .line 2630849
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->g:Landroid/view/View;

    const v2, 0x7f0d08e3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->h:Lcom/facebook/widget/listview/BetterListView;

    .line 2630850
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->h:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->g:Landroid/view/View;

    const v3, 0x1020004

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2630851
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->h:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->b()V

    .line 2630852
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->h:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->p:LX/IwC;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2630853
    if-eqz p3, :cond_1

    const-string v0, "pages_browser_data"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2630854
    const/4 v3, 0x0

    const/4 p2, 0x1

    .line 2630855
    const-string v0, "pages_browser_data"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->n:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    .line 2630856
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->n:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->n:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2630857
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->g:Landroid/view/View;

    const v2, 0x75a62b1c

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 2630858
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->c()V

    goto :goto_0

    .line 2630859
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->n:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_5

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    .line 2630860
    invoke-static {v0}, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->b(Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2630861
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v6

    const-string p1, "invites"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2630862
    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->f:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    .line 2630863
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v6

    const-string p1, "top"

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2630864
    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    .line 2630865
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2630866
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->f:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    if-eqz v0, :cond_6

    .line 2630867
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->p:LX/IwC;

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->f:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    invoke-virtual {v0, v2, p2, p2}, LX/IwC;->a(Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;ZZ)V

    .line 2630868
    :cond_6
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    if-eqz v0, :cond_7

    .line 2630869
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->p:LX/IwC;

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    invoke-virtual {v0, v2, v3, p2}, LX/IwC;->a(Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;ZZ)V

    .line 2630870
    :cond_7
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->p:LX/IwC;

    const v2, 0x6320f578

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6fcdf3ac

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2630823
    invoke-super {p0}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->onPause()V

    .line 2630824
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->b:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2630825
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2630826
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->i:LX/1B1;

    if-eqz v1, :cond_1

    .line 2630827
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->i:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->j:LX/Iwb;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2630828
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x2acba193

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x21ec508c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2630840
    invoke-super {p0}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->onResume()V

    .line 2630841
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->o:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2630842
    invoke-direct {p0}, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->c()V

    .line 2630843
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->i:LX/1B1;

    if-eqz v1, :cond_1

    .line 2630844
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->i:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->j:LX/Iwb;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2630845
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x23e9ec6c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2630836
    invoke-super {p0, p1}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2630837
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->n:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    if-eqz v0, :cond_0

    .line 2630838
    const-string v0, "pages_browser_data"

    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->n:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2630839
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x634ea97d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2630829
    invoke-super {p0}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->onStart()V

    .line 2630830
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2630831
    if-eqz v0, :cond_1

    .line 2630832
    const v2, 0x7f0838c7

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2630833
    :cond_0
    :goto_0
    const v0, -0x16a2c37d

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2630834
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->l:LX/0h5;

    if-eqz v0, :cond_0

    .line 2630835
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->l:LX/0h5;

    const v2, 0x7f0838c7

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    goto :goto_0
.end method
