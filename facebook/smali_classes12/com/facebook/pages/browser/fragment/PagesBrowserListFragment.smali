.class public Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;
.super Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;
.source ""


# instance fields
.field private e:Landroid/view/View;

.field private f:Lcom/facebook/widget/listview/BetterListView;

.field public g:LX/Iwb;

.field private h:LX/1B1;

.field public i:LX/03V;

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;

.field public l:LX/IwC;

.field public m:LX/0zG;

.field public n:LX/0h5;

.field public o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2630922
    invoke-direct {p0}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;-><init>()V

    .line 2630923
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    invoke-static {p0}, LX/Iwb;->a(LX/0QB;)LX/Iwb;

    move-result-object v1

    check-cast v1, LX/Iwb;

    invoke-static {p0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v2

    check-cast v2, LX/0zG;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/IwC;->b(LX/0QB;)LX/IwC;

    move-result-object p0

    check-cast p0, LX/IwC;

    iput-object v1, p1, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->g:LX/Iwb;

    iput-object v2, p1, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->m:LX/0zG;

    iput-object v3, p1, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->i:LX/03V;

    iput-object p0, p1, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->l:LX/IwC;

    return-void
.end method

.method public static c(Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2630924
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2630925
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel;

    .line 2630926
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2630927
    const-class v3, LX/1ZF;

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1ZF;

    .line 2630928
    if-eqz v3, :cond_2

    .line 2630929
    invoke-interface {v3, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2630930
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->l:LX/IwC;

    .line 2630931
    new-instance v3, LX/IwA;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/IwA;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2630932
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;->a()LX/0Px;

    move-result-object v4

    iput-object v4, v3, LX/IwA;->c:Ljava/util/List;

    .line 2630933
    iput-boolean v2, v3, LX/IwA;->d:Z

    .line 2630934
    iput-boolean v2, v3, LX/IwA;->e:Z

    .line 2630935
    iget-object v4, v1, LX/IwC;->e:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2630936
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->l:LX/IwC;

    const v1, -0x4e659dd9

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2630937
    return-void

    .line 2630938
    :cond_2
    iget-object v3, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->n:LX/0h5;

    if-eqz v3, :cond_0

    .line 2630939
    iget-object v3, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->n:LX/0h5;

    invoke-interface {v3, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2630940
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->a:LX/IwD;

    new-instance v1, Lcom/facebook/pages/browser/data/methods/FetchRecommendedPagesInCategory$Params;

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->k:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/pages/browser/data/methods/FetchRecommendedPagesInCategory$Params;-><init>(Ljava/lang/String;)V

    .line 2630941
    const-string v2, "fetchRecommendedPagesInCategory"

    const-string v3, "fetch_recommended_pages_in_category"

    invoke-static {v0, v1, v2, v3}, LX/IwD;->a(LX/IwD;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2630942
    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2630943
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->b:LX/1Ck;

    const-string v1, "fetch_recommended_pages_in_category"

    new-instance v2, LX/Iwl;

    invoke-direct {v2, p0}, LX/Iwl;-><init>(Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;)V

    new-instance v3, LX/Iwm;

    invoke-direct {v3, p0}, LX/Iwm;-><init>(Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2630944
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2630945
    invoke-super {p0, p1}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->a(Landroid/os/Bundle;)V

    .line 2630946
    const-class v0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2630947
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2630948
    const-string v1, "category"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->k:Ljava/lang/String;

    .line 2630949
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Empty category"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2630950
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->h:LX/1B1;

    .line 2630951
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->h:LX/1B1;

    new-instance v1, LX/Iwk;

    invoke-direct {v1, p0}, LX/Iwk;-><init>(Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2630952
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->m:LX/0zG;

    if-eqz v0, :cond_0

    .line 2630953
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->m:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->n:LX/0h5;

    .line 2630954
    :cond_0
    return-void

    .line 2630955
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2630956
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->l:LX/IwC;

    const v1, 0x5c94006a

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2630957
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x4ce732be    # 1.21214448E8f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2630958
    const v0, 0x7f030251

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->e:Landroid/view/View;

    .line 2630959
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->e:Landroid/view/View;

    const v2, 0x7f0d08e3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 2630960
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->e:Landroid/view/View;

    const v3, 0x1020004

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2630961
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->b()V

    .line 2630962
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->l:LX/IwC;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2630963
    if-eqz p3, :cond_0

    const-string v0, "pages_browser_list_data"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2630964
    const-string v0, "pages_browser_list_data"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    iput-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    .line 2630965
    invoke-static {p0}, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->c(Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;)V

    .line 2630966
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->e:Landroid/view/View;

    const v2, 0x33de9f6

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 2630967
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->d()V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7c508bb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2630968
    invoke-super {p0}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->onPause()V

    .line 2630969
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->b:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2630970
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2630971
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->h:LX/1B1;

    if-eqz v1, :cond_1

    .line 2630972
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->h:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->g:LX/Iwb;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2630973
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x7b65c7ac

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xa3666ba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2630974
    invoke-super {p0}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->onResume()V

    .line 2630975
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2630976
    invoke-direct {p0}, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->d()V

    .line 2630977
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->h:LX/1B1;

    if-eqz v1, :cond_1

    .line 2630978
    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->h:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->g:LX/Iwb;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2630979
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x525b1d6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2630980
    invoke-super {p0, p1}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2630981
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    if-eqz v0, :cond_0

    .line 2630982
    const-string v0, "pages_browser_list_data"

    iget-object v1, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->o:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPagesInCategoryModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2630983
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x9abacf0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2630984
    invoke-super {p0}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->onStart()V

    .line 2630985
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2630986
    if-eqz v0, :cond_1

    .line 2630987
    const v2, 0x7f0838c7

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2630988
    :cond_0
    :goto_0
    const v0, 0x145dcee0

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2630989
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->n:LX/0h5;

    if-eqz v0, :cond_0

    .line 2630990
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/PagesBrowserListFragment;->n:LX/0h5;

    const v2, 0x7f0838c7

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    goto :goto_0
.end method
