.class public Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/IwD;

.field public b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/Iwr;

.field public d:LX/Iw6;

.field public e:LX/961;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2630768
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2630769
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2630770
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;

    invoke-static {v0}, LX/IwD;->a(LX/0QB;)LX/IwD;

    move-result-object v3

    check-cast v3, LX/IwD;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/Iwr;->a(LX/0QB;)LX/Iwr;

    move-result-object v5

    check-cast v5, LX/Iwr;

    invoke-static {v0}, LX/Iw6;->a(LX/0QB;)LX/Iw6;

    move-result-object p1

    check-cast p1, LX/Iw6;

    invoke-static {v0}, LX/961;->b(LX/0QB;)LX/961;

    move-result-object v0

    check-cast v0, LX/961;

    iput-object v3, v2, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->a:LX/IwD;

    iput-object v4, v2, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->b:LX/1Ck;

    iput-object v5, v2, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->c:LX/Iwr;

    iput-object p1, v2, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->d:LX/Iw6;

    iput-object v0, v2, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->e:LX/961;

    .line 2630771
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2630772
    if-eqz p2, :cond_0

    .line 2630773
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->c:LX/Iwr;

    invoke-virtual {v0, p1}, LX/Iwr;->a(Ljava/lang/String;)V

    .line 2630774
    :goto_0
    return-void

    .line 2630775
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->c:LX/Iwr;

    .line 2630776
    invoke-virtual {v0, p1}, LX/Iwr;->c(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2630777
    iget-object p0, v0, LX/Iwr;->a:Ljava/util/Set;

    invoke-interface {p0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2630778
    :cond_1
    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 2630779
    return-void
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 2630780
    iget-object v0, p0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->e:LX/961;

    const-string v4, "pages_browser"

    const-string v6, "mobile_page_browser"

    const/4 v8, 0x0

    new-instance v9, LX/Iwg;

    invoke-direct {v9, p0, p2, p1}, LX/Iwg;-><init>(Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;ZLjava/lang/String;)V

    move-object v1, p1

    move v2, p2

    move-object v5, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v9}, LX/961;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;ZLX/1L9;)V

    .line 2630781
    return-void
.end method
