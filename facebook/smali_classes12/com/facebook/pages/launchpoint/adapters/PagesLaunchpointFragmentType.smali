.class public final enum Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum PAGES_LAUNCHPOINT_DISCOVER_FRAGMENT:Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

.field public static final enum PAGES_LAUNCHPOINT_FEED_FRAGMENT:Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

.field public static final enum PAGES_LAUNCHPOINT_HOME_FRAGMENT:Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2631340
    new-instance v0, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    const-string v1, "PAGES_LAUNCHPOINT_HOME_FRAGMENT"

    invoke-direct {v0, v1, v2}, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->PAGES_LAUNCHPOINT_HOME_FRAGMENT:Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    .line 2631341
    new-instance v0, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    const-string v1, "PAGES_LAUNCHPOINT_DISCOVER_FRAGMENT"

    invoke-direct {v0, v1, v3}, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->PAGES_LAUNCHPOINT_DISCOVER_FRAGMENT:Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    .line 2631342
    new-instance v0, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    const-string v1, "PAGES_LAUNCHPOINT_FEED_FRAGMENT"

    invoke-direct {v0, v1, v4}, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->PAGES_LAUNCHPOINT_FEED_FRAGMENT:Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    .line 2631343
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    sget-object v1, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->PAGES_LAUNCHPOINT_HOME_FRAGMENT:Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->PAGES_LAUNCHPOINT_DISCOVER_FRAGMENT:Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->PAGES_LAUNCHPOINT_FEED_FRAGMENT:Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->$VALUES:[Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    .line 2631344
    new-instance v0, LX/IxD;

    invoke-direct {v0}, LX/IxD;-><init>()V

    sput-object v0, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2631339
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;
    .locals 1

    .prologue
    .line 2631345
    const-class v0, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    return-object v0
.end method

.method public static values()[Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;
    .locals 1

    .prologue
    .line 2631338
    sget-object v0, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->$VALUES:[Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2631337
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2631335
    invoke-virtual {p0}, Lcom/facebook/pages/launchpoint/adapters/PagesLaunchpointFragmentType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2631336
    return-void
.end method
