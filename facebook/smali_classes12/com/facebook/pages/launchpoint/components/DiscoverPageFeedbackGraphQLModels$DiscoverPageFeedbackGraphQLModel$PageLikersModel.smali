.class public final Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2631540
    const-class v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2631541
    const-class v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2631542
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2631543
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2631535
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2631536
    iget v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2631530
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2631531
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2631532
    iget v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 2631533
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2631534
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2631537
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2631538
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2631539
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2631527
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2631528
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel;->e:I

    .line 2631529
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2631524
    new-instance v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel;

    invoke-direct {v0}, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$PageLikersModel;-><init>()V

    .line 2631525
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2631526
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2631523
    const v0, 0x67d8a1a9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2631522
    const v0, 0x25f82de5

    return v0
.end method
