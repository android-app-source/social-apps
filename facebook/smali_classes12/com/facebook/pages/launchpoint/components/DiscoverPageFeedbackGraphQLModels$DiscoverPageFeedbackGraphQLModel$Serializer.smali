.class public final Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2631544
    const-class v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel;

    new-instance v1, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2631545
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2631546
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2631547
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2631548
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2631549
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2631550
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2631551
    if-eqz v2, :cond_0

    .line 2631552
    const-string p0, "can_viewer_like"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2631553
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2631554
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2631555
    if-eqz v2, :cond_1

    .line 2631556
    const-string p0, "does_viewer_like"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2631557
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2631558
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2631559
    if-eqz v2, :cond_2

    .line 2631560
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2631561
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2631562
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2631563
    if-eqz v2, :cond_3

    .line 2631564
    const-string p0, "page_likers"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2631565
    invoke-static {v1, v2, p1}, LX/IxO;->a(LX/15i;ILX/0nX;)V

    .line 2631566
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2631567
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2631568
    check-cast p1, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel$Serializer;->a(Lcom/facebook/pages/launchpoint/components/DiscoverPageFeedbackGraphQLModels$DiscoverPageFeedbackGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
