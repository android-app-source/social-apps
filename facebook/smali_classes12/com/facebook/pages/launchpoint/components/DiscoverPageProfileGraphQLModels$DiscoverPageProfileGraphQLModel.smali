.class public final Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/IxU;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x698ea370
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2632154
    const-class v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2632140
    const-class v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2632141
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2632142
    return-void
.end method

.method private m()Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2632143
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->h:Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->h:Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    .line 2632144
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->h:Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2632155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2632156
    invoke-virtual {p0}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 2632157
    invoke-virtual {p0}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2632158
    invoke-virtual {p0}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2632159
    invoke-direct {p0}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->m()Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2632160
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2632161
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2632162
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2632163
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2632164
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2632165
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2632166
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2632145
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2632146
    invoke-direct {p0}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->m()Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2632147
    invoke-direct {p0}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->m()Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    .line 2632148
    invoke-direct {p0}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->m()Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2632149
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;

    .line 2632150
    iput-object v0, v1, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->h:Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    .line 2632151
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2632152
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2632153
    new-instance v0, LX/Ixe;

    invoke-direct {v0, p1}, LX/Ixe;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2632137
    invoke-virtual {p0}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2632138
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2632139
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2632136
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2632124
    new-instance v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;-><init>()V

    .line 2632125
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2632126
    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2632134
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->f:Ljava/lang/String;

    .line 2632135
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2632133
    const v0, -0x4aadef01

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2632132
    const v0, 0x25d6af

    return v0
.end method

.method public final j()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2632130
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->e:Ljava/util/List;

    .line 2632131
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2632128
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->g:Ljava/lang/String;

    .line 2632129
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic l()Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2632127
    invoke-direct {p0}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;->m()Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method
