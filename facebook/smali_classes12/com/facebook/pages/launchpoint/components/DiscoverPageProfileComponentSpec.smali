.class public Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/17W;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2632004
    const-class v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;

    const-string v1, "pages_launchpoint"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2632005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2632006
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;
    .locals 5

    .prologue
    .line 2632007
    const-class v1, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;

    monitor-enter v1

    .line 2632008
    :try_start_0
    sget-object v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2632009
    sput-object v2, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2632010
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2632011
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2632012
    new-instance v4, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;

    invoke-direct {v4}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;-><init>()V

    .line 2632013
    const/16 v3, 0x509

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    .line 2632014
    iput-object p0, v4, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;->b:LX/0Or;

    iput-object v3, v4, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;->c:LX/17W;

    .line 2632015
    move-object v0, v4

    .line 2632016
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2632017
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2632018
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2632019
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
