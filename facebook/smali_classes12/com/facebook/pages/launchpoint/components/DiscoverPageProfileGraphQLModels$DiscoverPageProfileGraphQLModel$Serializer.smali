.class public final Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2632099
    const-class v0, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;

    new-instance v1, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2632100
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2632101
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2632102
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2632103
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2632104
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2632105
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2632106
    if-eqz v2, :cond_0

    .line 2632107
    const-string v2, "category_names"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2632108
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2632109
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2632110
    if-eqz v2, :cond_1

    .line 2632111
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2632112
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2632113
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2632114
    if-eqz v2, :cond_2

    .line 2632115
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2632116
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2632117
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2632118
    if-eqz v2, :cond_3

    .line 2632119
    const-string p0, "profile_picture"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2632120
    invoke-static {v1, v2, p1}, LX/Ixg;->a(LX/15i;ILX/0nX;)V

    .line 2632121
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2632122
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2632123
    check-cast p1, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel$Serializer;->a(Lcom/facebook/pages/launchpoint/components/DiscoverPageProfileGraphQLModels$DiscoverPageProfileGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
