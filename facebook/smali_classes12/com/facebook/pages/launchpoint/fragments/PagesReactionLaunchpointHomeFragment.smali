.class public Lcom/facebook/pages/launchpoint/fragments/PagesReactionLaunchpointHomeFragment;
.super Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;
.source ""


# instance fields
.field public i:LX/CfW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2632626
    invoke-direct {p0}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/pages/launchpoint/fragments/PagesReactionLaunchpointHomeFragment;

    invoke-static {p0}, LX/CfW;->b(LX/0QB;)LX/CfW;

    move-result-object v1

    check-cast v1, LX/CfW;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    iput-object v1, p1, Lcom/facebook/pages/launchpoint/fragments/PagesReactionLaunchpointHomeFragment;->i:LX/CfW;

    iput-object p0, p1, Lcom/facebook/pages/launchpoint/fragments/PagesReactionLaunchpointHomeFragment;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2632627
    const-string v0, "pages_launchpoint"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2632628
    const-class v0, Lcom/facebook/pages/launchpoint/fragments/PagesReactionLaunchpointHomeFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/launchpoint/fragments/PagesReactionLaunchpointHomeFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2632629
    invoke-super {p0, p1}, Lcom/facebook/reaction/ui/fragment/BaseFullscreenReactionFragment;->a(Landroid/os/Bundle;)V

    .line 2632630
    return-void
.end method

.method public final v()LX/2jY;
    .locals 7

    .prologue
    .line 2632631
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesReactionLaunchpointHomeFragment;->i:LX/CfW;

    const-string v1, "ANDROID_PAGE_LAUNCH_POINT_HOME"

    new-instance v2, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v2}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    iget-object v3, p0, Lcom/facebook/pages/launchpoint/fragments/PagesReactionLaunchpointHomeFragment;->j:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2632632
    iput-object v3, v2, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2632633
    move-object v2, v2

    .line 2632634
    const-wide/16 v4, 0x5

    .line 2632635
    iput-wide v4, v2, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 2632636
    move-object v2, v2

    .line 2632637
    invoke-virtual {v0, v1, v2}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    return-object v0
.end method
