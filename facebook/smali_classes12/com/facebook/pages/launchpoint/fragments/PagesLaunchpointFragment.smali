.class public Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fj;


# instance fields
.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IxC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1vi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/0h5;

.field public e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

.field public f:I

.field private g:Z

.field private final h:LX/CgM;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2632622
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2632623
    iput v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->f:I

    .line 2632624
    iput-boolean v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->g:Z

    .line 2632625
    new-instance v0, LX/Ixv;

    invoke-direct {v0, p0}, LX/Ixv;-><init>(Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->h:LX/CgM;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;

    invoke-static {p0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v1

    check-cast v1, LX/0zG;

    const-class v2, LX/IxC;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/IxC;

    invoke-static {p0}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object p0

    check-cast p0, LX/1vi;

    iput-object v1, p1, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->a:LX/0zG;

    iput-object v2, p1, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->b:LX/IxC;

    iput-object p0, p1, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->c:LX/1vi;

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2632618
    iget-object v1, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eqz v1, :cond_0

    .line 2632619
    iget-object v1, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2632620
    const/4 v0, 0x1

    .line 2632621
    :cond_0
    return v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2632617
    const-string v0, "pages_launchpoint"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2632589
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2632590
    const-class v0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2632591
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->a:LX/0zG;

    if-eqz v0, :cond_0

    .line 2632592
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->d:LX/0h5;

    .line 2632593
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x77271561

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2632616
    const v1, 0x7f030ed4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x414cf71b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5416b355

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2632613
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2632614
    iget-object v1, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->c:LX/1vi;

    iget-object v2, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->h:LX/CgM;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2632615
    const/16 v1, 0x2b

    const v2, 0x5a181d14

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x69b9e925

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2632606
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2632607
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2632608
    if-eqz v0, :cond_1

    .line 2632609
    const v2, 0x7f083101

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2632610
    :cond_0
    :goto_0
    const v0, -0x63efed5b

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2632611
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->d:LX/0h5;

    if-eqz v0, :cond_0

    .line 2632612
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->d:LX/0h5;

    const v2, 0x7f083101

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2632594
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2632595
    const v0, 0x7f0d2435

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iput-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    .line 2632596
    const v0, 0x7f0d2434

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2632597
    iget-object v1, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->b:LX/IxC;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2632598
    new-instance p2, LX/IxB;

    invoke-static {v1}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p1

    check-cast p1, LX/0ad;

    invoke-direct {p2, v4, v2, v3, p1}, LX/IxB;-><init>(Ljava/lang/String;LX/0gc;Landroid/content/Context;LX/0ad;)V

    .line 2632599
    move-object v1, p2

    .line 2632600
    iget-object v2, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v2, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2632601
    iget-object v1, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iget v2, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->f:I

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2632602
    iget-object v1, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2632603
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->e:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    new-instance v1, LX/Ixw;

    invoke-direct {v1, p0}, LX/Ixw;-><init>(Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2632604
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->c:LX/1vi;

    iget-object v1, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointFragment;->h:LX/CgM;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2632605
    return-void
.end method
