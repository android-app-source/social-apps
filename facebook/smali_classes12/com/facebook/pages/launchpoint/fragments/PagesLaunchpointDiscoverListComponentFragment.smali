.class public Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;
.super Lcom/facebook/components/list/fb/fragment/ListComponentFragment;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Ixs;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Ixm;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/1rq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/Ixk;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0if;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2632557
    invoke-direct {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;-><init>()V

    .line 2632558
    return-void
.end method


# virtual methods
.method public final a(LX/BcP;LX/BcQ;)LX/BcO;
    .locals 3

    .prologue
    .line 2632559
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->e:LX/Ixk;

    .line 2632560
    new-instance v1, LX/Ixj;

    invoke-direct {v1, v0}, LX/Ixj;-><init>(LX/Ixk;)V

    .line 2632561
    iget-object v2, v0, LX/Ixk;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ixi;

    .line 2632562
    if-nez v2, :cond_0

    .line 2632563
    new-instance v2, LX/Ixi;

    invoke-direct {v2, v0}, LX/Ixi;-><init>(LX/Ixk;)V

    .line 2632564
    :cond_0
    iput-object v1, v2, LX/BcN;->a:LX/BcO;

    .line 2632565
    iput-object v1, v2, LX/Ixi;->a:LX/Ixj;

    .line 2632566
    iget-object v0, v2, LX/Ixi;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2632567
    move-object v1, v2

    .line 2632568
    move-object v0, v1

    .line 2632569
    const/4 v1, 0x2

    .line 2632570
    iget-object v2, v0, LX/Ixi;->a:LX/Ixj;

    iput v1, v2, LX/Ixj;->b:I

    .line 2632571
    iget-object v2, v0, LX/Ixi;->e:Ljava/util/BitSet;

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Ljava/util/BitSet;->set(I)V

    .line 2632572
    move-object v0, v0

    .line 2632573
    iget-object v1, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->c:LX/2kW;

    .line 2632574
    iget-object v2, v0, LX/Ixi;->a:LX/Ixj;

    iput-object v1, v2, LX/Ixj;->c:LX/2kW;

    .line 2632575
    iget-object v2, v0, LX/Ixi;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2632576
    move-object v0, v0

    .line 2632577
    invoke-virtual {v0, p2}, LX/Ixi;->b(LX/BcQ;)LX/Ixi;

    move-result-object v0

    invoke-virtual {v0}, LX/Ixi;->b()LX/BcO;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2632540
    invoke-super {p0, p1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(Landroid/os/Bundle;)V

    .line 2632541
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;

    const/16 v3, 0x2c71

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x2c70

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const-class v5, LX/1rq;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/1rq;

    invoke-static {v0}, LX/Ixk;->a(LX/0QB;)LX/Ixk;

    move-result-object v6

    check-cast v6, LX/Ixk;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v7

    check-cast v7, LX/0if;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object p1

    check-cast p1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v3, v2, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->a:LX/0Or;

    iput-object v4, v2, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->b:LX/0Or;

    iput-object v5, v2, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->d:LX/1rq;

    iput-object v6, v2, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->e:LX/Ixk;

    iput-object v7, v2, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->f:LX/0if;

    iput-object p1, v2, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object v0, v2, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->h:LX/0ad;

    .line 2632542
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->f:LX/0if;

    sget-object v1, LX/0ig;->ai:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2632543
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->f:LX/0if;

    sget-object v1, LX/0ig;->ai:LX/0ih;

    const-string v2, "list_component"

    invoke-virtual {v0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2632544
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ixm;

    .line 2632545
    iget-object v1, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ixs;

    .line 2632546
    iget-object v2, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->d:LX/1rq;

    const-string v3, "/page/discover/"

    invoke-virtual {v2, v3, v0}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v0

    const/4 v2, 0x1

    .line 2632547
    iput-boolean v2, v0, LX/2jj;->k:Z

    .line 2632548
    move-object v0, v0

    .line 2632549
    const-wide/16 v2, 0x0

    .line 2632550
    iput-wide v2, v0, LX/2jj;->j:J

    .line 2632551
    move-object v0, v0

    .line 2632552
    invoke-virtual {v0}, LX/2jj;->a()LX/2kW;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->c:LX/2kW;

    .line 2632553
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->c:LX/2kW;

    invoke-virtual {v0, v1}, LX/2kW;->a(LX/1vq;)V

    .line 2632554
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->c:LX/2kW;

    new-instance v1, LX/Ixu;

    invoke-direct {v1, p0}, LX/Ixu;-><init>(Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;)V

    invoke-virtual {v0, v1}, LX/2kW;->a(LX/1vq;)V

    .line 2632555
    return-void
.end method

.method public final d()LX/BcG;
    .locals 1

    .prologue
    .line 2632556
    new-instance v0, LX/BdH;

    invoke-direct {v0, p0}, LX/BdH;-><init>(Lcom/facebook/components/list/fb/fragment/ListComponentFragment;)V

    return-object v0
.end method

.method public final e()LX/BdI;
    .locals 2

    .prologue
    .line 2632539
    new-instance v0, LX/Ixt;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/Ixt;-><init>(I)V

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x70c16a5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2632536
    iget-object v1, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->c:LX/2kW;

    invoke-virtual {v1}, LX/2kW;->c()V

    .line 2632537
    invoke-super {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->onDestroy()V

    .line 2632538
    const/16 v1, 0x2b

    const v2, 0x6efb0d37

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1e7431ec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2632533
    iget-object v1, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->f:LX/0if;

    sget-object v2, LX/0ig;->ai:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 2632534
    invoke-super {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->onDestroyView()V

    .line 2632535
    const/16 v1, 0x2b

    const v2, -0x61041805

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 3

    .prologue
    .line 2632527
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->f:LX/0if;

    if-eqz v0, :cond_0

    .line 2632528
    if-eqz p1, :cond_1

    .line 2632529
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->f:LX/0if;

    sget-object v1, LX/0ig;->ai:LX/0ih;

    sget-object v2, LX/IxE;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2632530
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->setUserVisibleHint(Z)V

    .line 2632531
    return-void

    .line 2632532
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/launchpoint/fragments/PagesLaunchpointDiscoverListComponentFragment;->f:LX/0if;

    sget-object v1, LX/0ig;->ai:LX/0ih;

    sget-object v2, LX/IxE;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0
.end method
