.class public Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2473274
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2473275
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2473276
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2473277
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2473278
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2473279
    const v0, 0x7f030e77

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2473280
    const v0, 0x7f0d2367

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;->a:Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;

    .line 2473281
    return-void
.end method


# virtual methods
.method public setAttribtutions(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAttributionEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2473282
    iget-object v0, p0, Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;->a:Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;->removeAllViews()V

    .line 2473283
    iget-object v0, p0, Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;->a:Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;->setAttributions(Ljava/util/List;)V

    .line 2473284
    return-void
.end method
