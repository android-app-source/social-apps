.class public Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final a:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2473253
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2473254
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2473255
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2473256
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;->setOrientation(I)V

    .line 2473257
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;->a:Landroid/view/LayoutInflater;

    .line 2473258
    return-void
.end method


# virtual methods
.method public setAttributions(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAttributionEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2473259
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2473260
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAttributionEntry;

    .line 2473261
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2473262
    iget-object v1, p0, Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f030e57

    const/4 v4, 0x0

    invoke-virtual {v1, v2, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2473263
    const v1, 0x7f0d22fd

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2473264
    const v2, 0x7f0d22fc

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2473265
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2473266
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2473267
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {v6}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2473268
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->j()Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemIcon;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    .line 2473269
    :goto_1
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2473270
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-class v1, Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2473271
    :cond_1
    invoke-virtual {p0, v4}, Lcom/facebook/pages/fb4a/vertex_attribution/PageIdentityVertexAttributionList;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2473272
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttributionEntry;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2473273
    :cond_3
    return-void
.end method
