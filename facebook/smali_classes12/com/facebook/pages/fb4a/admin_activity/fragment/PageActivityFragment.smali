.class public Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/GZf;


# static fields
.field public static final N:[I


# instance fields
.field public A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

.field private B:LX/HRS;

.field public C:Z

.field private D:Z

.field public E:Z

.field public F:LX/E8s;

.field private G:LX/E8t;

.field public H:Landroid/view/View;

.field public I:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field private J:Z

.field public K:I

.field private L:I

.field private M:I

.field public O:Z

.field public P:Z

.field private final Q:LX/HDc;

.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HRo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HDT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Sg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/FQY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/8Do;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/16I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/2U1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/widget/ScrollingAwareScrollView;

.field private r:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;

.field private s:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;

.field private t:LX/HS4;

.field private u:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;

.field private v:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;

.field private w:Landroid/widget/LinearLayout;

.field private x:Z

.field public y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2465396
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->N:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2465387
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2465388
    new-instance v0, LX/HRS;

    invoke-direct {v0, p0}, LX/HRS;-><init>(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->B:LX/HRS;

    .line 2465389
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->C:Z

    .line 2465390
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->J:Z

    .line 2465391
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->M:I

    .line 2465392
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->O:Z

    .line 2465393
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->P:Z

    .line 2465394
    new-instance v0, LX/HRN;

    invoke-direct {v0, p0}, LX/HRN;-><init>(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->Q:LX/HDc;

    .line 2465395
    return-void
.end method

.method private static a(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;LX/0Ot;LX/1Ck;LX/HRo;LX/HDT;Landroid/content/Context;LX/0Sg;LX/0tX;LX/0Ot;LX/0Ot;LX/FQY;LX/0hB;LX/8Do;LX/0Uh;LX/0ad;LX/16I;LX/2U1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/1Ck;",
            "LX/HRo;",
            "LX/HDT;",
            "Landroid/content/Context;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/0tX;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;",
            "LX/FQY;",
            "LX/0hB;",
            "LX/8Do;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/16I;",
            "LX/2U1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2465386
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->b:LX/1Ck;

    iput-object p3, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->c:LX/HRo;

    iput-object p4, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->d:LX/HDT;

    iput-object p5, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->e:Landroid/content/Context;

    iput-object p6, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->f:LX/0Sg;

    iput-object p7, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->g:LX/0tX;

    iput-object p8, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->h:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->i:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->j:LX/FQY;

    iput-object p11, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->k:LX/0hB;

    iput-object p12, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->l:LX/8Do;

    iput-object p13, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->m:LX/0Uh;

    iput-object p14, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->n:LX/0ad;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->o:LX/16I;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->p:LX/2U1;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 18

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    const/16 v2, 0x2eb

    move-object/from16 v0, v17

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static/range {v17 .. v17}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static/range {v17 .. v17}, LX/HRo;->a(LX/0QB;)LX/HRo;

    move-result-object v4

    check-cast v4, LX/HRo;

    invoke-static/range {v17 .. v17}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v5

    check-cast v5, LX/HDT;

    const-class v6, Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static/range {v17 .. v17}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v7

    check-cast v7, LX/0Sg;

    invoke-static/range {v17 .. v17}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    const/16 v9, 0x2b68

    move-object/from16 v0, v17

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xbd2

    move-object/from16 v0, v17

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {v17 .. v17}, LX/FQY;->a(LX/0QB;)LX/FQY;

    move-result-object v11

    check-cast v11, LX/FQY;

    invoke-static/range {v17 .. v17}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v12

    check-cast v12, LX/0hB;

    invoke-static/range {v17 .. v17}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v13

    check-cast v13, LX/8Do;

    invoke-static/range {v17 .. v17}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v14

    check-cast v14, LX/0Uh;

    invoke-static/range {v17 .. v17}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v15

    check-cast v15, LX/0ad;

    invoke-static/range {v17 .. v17}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v16

    check-cast v16, LX/16I;

    invoke-static/range {v17 .. v17}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v17

    check-cast v17, LX/2U1;

    invoke-static/range {v1 .. v17}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->a(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;LX/0Ot;LX/1Ck;LX/HRo;LX/HDT;Landroid/content/Context;LX/0Sg;LX/0tX;LX/0Ot;LX/0Ot;LX/FQY;LX/0hB;LX/8Do;LX/0Uh;LX/0ad;LX/16I;LX/2U1;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;Z)V
    .locals 5

    .prologue
    .line 2465383
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->n:LX/0ad;

    sget-short v1, LX/8Dn;->j:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2465384
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->b:LX/1Ck;

    sget-object v2, LX/8Dp;->FETCH_PAGE_IDENTITY_ACTIVITY_DATA:LX/8Dp;

    new-instance v3, LX/HRQ;

    invoke-direct {v3, p0, v0}, LX/HRQ;-><init>(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;Z)V

    new-instance v4, LX/HRR;

    invoke-direct {v4, p0, p1, v0}, LX/HRR;-><init>(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;ZZ)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2465385
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 2465382
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->C:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->D:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V
    .locals 8

    .prologue
    .line 2465373
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->r:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;

    if-eqz v0, :cond_0

    .line 2465374
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2465375
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->r:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;

    iget-wide v2, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->B:LX/HRS;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    .line 2465376
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->y:LX/0Px;

    invoke-static {v0}, LX/FQY;->a(LX/0Px;)Z

    move-result v0

    move v7, v0

    .line 2465377
    invoke-virtual/range {v1 .. v7}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a(JLX/15i;ILX/0am;Z)V

    .line 2465378
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->u:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;

    if-eqz v0, :cond_1

    .line 2465379
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->u:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;

    iget-wide v2, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->y:LX/0Px;

    invoke-virtual {v0, v2, v3, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->a(JLX/0Px;)V

    .line 2465380
    :cond_1
    return-void

    .line 2465381
    :cond_2
    const/4 v4, 0x0

    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V
    .locals 13

    .prologue
    const v10, -0xa168aab

    const/16 v12, 0x8

    const/4 v9, 0x3

    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 2465293
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    if-eqz v0, :cond_5

    .line 2465294
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2465295
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2465296
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 2465297
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2465298
    invoke-virtual {v2, v0, v7}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_0
    if-eqz v0, :cond_9

    .line 2465299
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2465300
    invoke-virtual {v2, v0, v7}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v2, v0, v7}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_1
    if-eqz v0, :cond_24

    .line 2465301
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v7}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v2, v0, v7}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v0

    .line 2465302
    :goto_2
    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->m()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_a

    .line 2465303
    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->m()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2465304
    invoke-virtual {v3, v2, v7}, LX/15i;->h(II)Z

    move-result v2

    :goto_3
    if-eqz v2, :cond_23

    .line 2465305
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v0

    move v2, v0

    .line 2465306
    :goto_4
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_22

    .line 2465307
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->p()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v7}, LX/15i;->j(II)I

    move-result v0

    .line 2465308
    :goto_5
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_c

    .line 2465309
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v3

    iget-object v6, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2465310
    invoke-virtual {v6, v3, v7}, LX/15i;->g(II)I

    move-result v3

    if-eqz v3, :cond_b

    move v3, v1

    :goto_6
    if-eqz v3, :cond_e

    .line 2465311
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v3

    iget-object v6, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2465312
    invoke-virtual {v6, v3, v7}, LX/15i;->g(II)I

    move-result v3

    invoke-virtual {v6, v3, v1}, LX/15i;->g(II)I

    move-result v3

    if-eqz v3, :cond_d

    move v3, v1

    :goto_7
    if-eqz v3, :cond_21

    .line 2465313
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->l()LX/1vs;

    move-result-object v3

    iget-object v6, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v6, v3, v7}, LX/15i;->g(II)I

    move-result v3

    invoke-virtual {v6, v3, v1}, LX/15i;->g(II)I

    move-result v3

    .line 2465314
    invoke-virtual {v6, v3, v7}, LX/15i;->j(II)I

    move-result v3

    .line 2465315
    :goto_8
    iget-object v6, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v6}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->n()I

    move-result v8

    .line 2465316
    iget-object v6, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->r:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;

    invoke-virtual {v6, v0, v3}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a(II)V

    .line 2465317
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->r:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;

    invoke-virtual {v0, v4, v5}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->b(LX/15i;I)V

    .line 2465318
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->r:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;

    invoke-virtual {v0, v4, v5}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a(LX/15i;I)V

    .line 2465319
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->v:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;

    if-eqz v0, :cond_0

    .line 2465320
    if-eqz v5, :cond_16

    const/4 v0, 0x7

    invoke-virtual {v4, v5, v0}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 2465321
    const/4 v6, 0x0

    .line 2465322
    invoke-virtual {v4, v5, v9}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_11

    .line 2465323
    invoke-virtual {v4, v5, v9}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v4, v0, v7, v10}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2465324
    if-eqz v0, :cond_f

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_9
    if-eqz v0, :cond_10

    move v0, v1

    :goto_a
    if-eqz v0, :cond_14

    .line 2465325
    invoke-virtual {v4, v5, v9}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v4, v0, v7, v10}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2465326
    if-eqz v0, :cond_12

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_b
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_13

    move v0, v1

    :goto_c
    if-eqz v0, :cond_20

    .line 2465327
    invoke-virtual {v4, v5, v9}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v4, v0, v7, v10}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_15

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_d
    invoke-virtual {v0, v7}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2465328
    const-class v9, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v6, v0, v7, v9, v10}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 2465329
    :goto_e
    iget-object v6, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->v:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;

    invoke-virtual {v6, v7}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->setVisibility(I)V

    .line 2465330
    iget-object v6, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->v:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;

    iget-wide v10, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    invoke-virtual {v6, v10, v11, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->a(JLcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;)V

    .line 2465331
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->v:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->setPromoteLabel(Ljava/lang/String;)V

    .line 2465332
    :cond_0
    :goto_f
    add-int/2addr v2, v3

    .line 2465333
    const-class v0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    .line 2465334
    if-eqz v0, :cond_1

    .line 2465335
    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->setActivityBadgeCount(J)V

    .line 2465336
    if-lez v8, :cond_1

    .line 2465337
    int-to-long v2, v8

    invoke-virtual {v0, v2, v3}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->setInsightsBadgeCount(J)V

    .line 2465338
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_18

    .line 2465339
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2465340
    const-class v3, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v2, v0, v1, v3, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CONTACT_US:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    if-ne v0, v2, :cond_17

    move v0, v1

    :goto_10
    if-eqz v0, :cond_1a

    .line 2465341
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2465342
    invoke-virtual {v2, v0, v7}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_19

    move v0, v1

    .line 2465343
    :goto_11
    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->y:LX/0Px;

    .line 2465344
    new-instance v3, LX/8A4;

    invoke-direct {v3, v2}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v6, LX/8A3;->MODERATE_CONTENT:LX/8A3;

    invoke-virtual {v3, v6}, LX/8A4;->a(LX/8A3;)Z

    move-result v3

    move v3, v3

    .line 2465345
    if-eqz v5, :cond_1c

    invoke-virtual {v4, v5, v12}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_1b

    move v2, v1

    :goto_12
    if-eqz v2, :cond_1e

    .line 2465346
    invoke-virtual {v4, v5, v12}, LX/15i;->g(II)I

    move-result v2

    .line 2465347
    invoke-virtual {v4, v2, v7}, LX/15i;->j(II)I

    move-result v2

    if-lez v2, :cond_1d

    .line 2465348
    :goto_13
    iget-boolean v2, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->x:Z

    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    .line 2465349
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->t:LX/HS4;

    new-instance v1, LX/HRP;

    invoke-direct {v1, p0}, LX/HRP;-><init>(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V

    invoke-virtual {v0, v1}, LX/HS4;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2465350
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->t:LX/HS4;

    invoke-virtual {v0, v7}, LX/HS4;->setVisibility(I)V

    .line 2465351
    :cond_3
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->c:LX/HRo;

    iget-wide v2, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->B:LX/HRS;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/HRo;->a(JLX/15i;ILX/0am;)V

    .line 2465352
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->s:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;

    if-eqz v0, :cond_5

    .line 2465353
    const/4 v0, 0x0

    .line 2465354
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->k()LX/2uF;

    move-result-object v1

    if-nez v1, :cond_25

    .line 2465355
    :cond_4
    :goto_14
    move v0, v0

    .line 2465356
    if-eqz v0, :cond_1f

    .line 2465357
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->s:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;

    invoke-virtual {v0, v7}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->setVisibility(I)V

    .line 2465358
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->s:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;

    iget-wide v2, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->k()LX/2uF;

    move-result-object v1

    iget-object v4, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->B:LX/HRS;

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->a(JLX/2uF;LX/0am;)V

    .line 2465359
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->D:Z

    if-eqz v0, :cond_5

    .line 2465360
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->s:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->b()V

    .line 2465361
    :cond_5
    :goto_15
    return-void

    .line 2465362
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    move v0, v7

    .line 2465363
    goto/16 :goto_0

    :cond_7
    move v0, v7

    goto/16 :goto_0

    :cond_8
    move v0, v7

    goto/16 :goto_1

    :cond_9
    move v0, v7

    goto/16 :goto_1

    :cond_a
    move v2, v7

    .line 2465364
    goto/16 :goto_3

    :cond_b
    move v3, v7

    .line 2465365
    goto/16 :goto_6

    :cond_c
    move v3, v7

    goto/16 :goto_6

    :cond_d
    move v3, v7

    goto/16 :goto_7

    :cond_e
    move v3, v7

    goto/16 :goto_7

    .line 2465366
    :cond_f
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_9

    :cond_10
    move v0, v7

    goto/16 :goto_a

    :cond_11
    move v0, v7

    goto/16 :goto_a

    :cond_12
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_b

    :cond_13
    move v0, v7

    goto/16 :goto_c

    :cond_14
    move v0, v7

    goto/16 :goto_c

    .line 2465367
    :cond_15
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_d

    .line 2465368
    :cond_16
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->v:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;

    invoke-virtual {v0, v12}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->setVisibility(I)V

    goto/16 :goto_f

    :cond_17
    move v0, v7

    .line 2465369
    goto/16 :goto_10

    :cond_18
    move v0, v7

    goto/16 :goto_10

    :cond_19
    move v0, v7

    goto/16 :goto_11

    :cond_1a
    move v0, v7

    goto/16 :goto_11

    :cond_1b
    move v2, v7

    .line 2465370
    goto/16 :goto_12

    :cond_1c
    move v2, v7

    goto/16 :goto_12

    :cond_1d
    move v1, v7

    goto/16 :goto_13

    :cond_1e
    move v1, v7

    goto/16 :goto_13

    .line 2465371
    :cond_1f
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->s:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;

    invoke-virtual {v0, v12}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->setVisibility(I)V

    .line 2465372
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->s:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->a()V

    goto :goto_15

    :cond_20
    move-object v0, v6

    goto/16 :goto_e

    :cond_21
    move v3, v7

    goto/16 :goto_8

    :cond_22
    move v0, v7

    goto/16 :goto_5

    :cond_23
    move v2, v0

    goto/16 :goto_4

    :cond_24
    move v0, v7

    goto/16 :goto_2

    :cond_25
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->A:Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;

    invoke-virtual {v1}, Lcom/facebook/pages/common/protocol/graphql/FetchPageActivityGraphQLModels$FetchPageActivityQueryModel;->k()LX/2uF;

    move-result-object v1

    invoke-virtual {v1}, LX/39O;->a()Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v0, 0x1

    goto/16 :goto_14
.end method

.method private n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2465282
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->G:LX/E8t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->w:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 2465283
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->F:LX/E8s;

    if-eqz v0, :cond_0

    .line 2465284
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->w:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->F:LX/E8s;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2465285
    :cond_0
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->G:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->F:LX/E8s;

    .line 2465286
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->F:LX/E8s;

    const/4 v1, 0x1

    .line 2465287
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2465288
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->w:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->F:LX/E8s;

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 2465289
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->G:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->F:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2465290
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->setBackgroundResource(I)V

    .line 2465291
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2465292
    :cond_1
    return-void
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2465277
    iget v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->M:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->M:I

    if-le p1, v0, :cond_1

    .line 2465278
    :cond_0
    :goto_0
    return-void

    .line 2465279
    :cond_1
    iput p1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->M:I

    .line 2465280
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->H:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2465281
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->H:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->M:I

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2465274
    iput p1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->L:I

    .line 2465275
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    iget v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->L:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2465276
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2465397
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->G:LX/E8t;

    .line 2465398
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->n()V

    .line 2465399
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2465189
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2465190
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2465191
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->m:LX/0Uh;

    sget v3, LX/8Dm;->a:I

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->x:Z

    .line 2465192
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2465193
    const-string v3, "com.facebook.katana.profile.id"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    .line 2465194
    iget-wide v4, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid page id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2465195
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->p:LX/2U1;

    iget-wide v4, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/2U1;->c(Ljava/lang/String;)LX/8Dk;

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->E:Z

    .line 2465196
    if-eqz p1, :cond_0

    .line 2465197
    const-string v0, "extra_viewer_profile_permissions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2465198
    const-string v0, "extra_viewer_profile_permissions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->y:LX/0Px;

    .line 2465199
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2465200
    const-string v1, "extra_is_inside_page_surface_tab"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->J:Z

    .line 2465201
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->d:LX/HDT;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->Q:LX/HDc;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2465202
    return-void

    :cond_1
    move v0, v2

    .line 2465203
    goto :goto_0

    :cond_2
    move v1, v2

    .line 2465204
    goto :goto_1
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2465205
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->I:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2465206
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2465207
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->a$redex0(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;Z)V

    .line 2465208
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2465209
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2465210
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->c:LX/HRo;

    .line 2465211
    iget-object p0, v0, LX/HRo;->f:LX/HRn;

    instance-of p0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    if-eqz p0, :cond_0

    .line 2465212
    iget-object p0, v0, LX/HRo;->f:LX/HRn;

    check-cast p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    invoke-virtual {p0, p1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2465213
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6f158a5c    # -9.247798E-29f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2465214
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2465215
    const v0, 0x7f0306a0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ScrollingAwareScrollView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 2465216
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v2, 0x7f0d1220

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->r:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;

    .line 2465217
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v2, 0x7f0d1221

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/HS4;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->t:LX/HS4;

    .line 2465218
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v2, 0x7f0d1222

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->s:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;

    .line 2465219
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v2, 0x7f0d1223

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->u:Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;

    .line 2465220
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v2, 0x7f0d1224

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->v:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;

    .line 2465221
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v2, 0x7f0d121e

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->w:Landroid/widget/LinearLayout;

    .line 2465222
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->c:LX/HRo;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    .line 2465223
    iput-object v2, v0, LX/HRo;->e:Landroid/widget/LinearLayout;

    .line 2465224
    iput-object v3, v0, LX/HRo;->h:Landroid/app/Activity;

    .line 2465225
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v2, 0x7f0d03c4

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->H:Landroid/view/View;

    .line 2465226
    iget v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->M:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->E_(I)V

    .line 2465227
    invoke-static {p0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->d(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V

    .line 2465228
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->C:Z

    if-nez v0, :cond_0

    .line 2465229
    invoke-static {p0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->k(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V

    .line 2465230
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/16 v2, 0x2b

    const v3, 0x8c7d644

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x17915f0c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2465231
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2465232
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->d:LX/HDT;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->Q:LX/HDc;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2465233
    const/16 v1, 0x2b

    const v2, 0x775f628d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x10be9fb7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2465234
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2465235
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->c:LX/HRo;

    .line 2465236
    iget-object v2, v1, LX/HRo;->f:LX/HRn;

    instance-of v2, v2, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    if-eqz v2, :cond_0

    iget-boolean v2, v1, LX/HRo;->g:Z

    if-eqz v2, :cond_0

    .line 2465237
    invoke-static {v1}, LX/HRo;->d(LX/HRo;)V

    .line 2465238
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x5bda516d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2465239
    :cond_0
    const/4 v2, 0x0

    iput-object v2, v1, LX/HRo;->f:LX/HRn;

    .line 2465240
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/HRo;->g:Z

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x27e8eee6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2465241
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2465242
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2465243
    const/16 v1, 0x2b

    const v2, -0x5ab4ae87

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v4, 0x6667ae0c

    invoke-static {v0, v1, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 2465244
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2465245
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->c:LX/HRo;

    .line 2465246
    iget-object v1, v0, LX/HRo;->f:LX/HRn;

    if-eqz v1, :cond_0

    iget-boolean v1, v0, LX/HRo;->g:Z

    if-nez v1, :cond_0

    .line 2465247
    invoke-static {v0}, LX/HRo;->c(LX/HRo;)V

    .line 2465248
    :cond_0
    iget v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->M:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->E_(I)V

    .line 2465249
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2465250
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->a$redex0(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;Z)V

    .line 2465251
    :cond_1
    :goto_0
    const v0, -0x5f35709a

    invoke-static {v0, v4}, LX/02F;->f(II)V

    return-void

    .line 2465252
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->C:Z

    if-eqz v0, :cond_1

    .line 2465253
    const-class v0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    .line 2465254
    if-eqz v0, :cond_4

    .line 2465255
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->getActivityBadgeCount()J

    move-result-wide v0

    .line 2465256
    :goto_1
    cmp-long v0, v0, v2

    if-gtz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->e:Landroid/content/Context;

    invoke-static {v0}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0x7dc

    if-lt v0, v1, :cond_1

    .line 2465257
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->f:LX/0Sg;

    const-string v1, "Preloading Page Activity tab"

    new-instance v2, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment$3;

    invoke-direct {v2, p0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment$3;-><init>(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_UI_IDLE:LX/0VZ;

    sget-object v5, LX/0Vm;->UI:LX/0Vm;

    invoke-virtual {v0, v1, v2, v3, v5}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    goto :goto_0

    :cond_4
    move-wide v0, v2

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2465258
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->y:LX/0Px;

    if-eqz v0, :cond_0

    .line 2465259
    const-string v0, "extra_viewer_profile_permissions"

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->y:LX/0Px;

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2465260
    :cond_0
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2465261
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2465262
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    new-instance v1, LX/HRO;

    invoke-direct {v1, p0}, LX/HRO;-><init>(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->a(LX/4oV;)V

    .line 2465263
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->J:Z

    if-eqz v0, :cond_0

    .line 2465264
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->q:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 2465265
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2465266
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->n()V

    .line 2465267
    iget v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->L:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->a(I)V

    .line 2465268
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 2465269
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2465270
    iput-boolean p1, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->D:Z

    .line 2465271
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->b:LX/1Ck;

    if-eqz v0, :cond_0

    .line 2465272
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->a$redex0(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;Z)V

    .line 2465273
    :cond_0
    return-void
.end method
