.class public Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0fv;


# instance fields
.field public A:Z

.field private final B:Ljava/lang/Runnable;

.field public a:LX/HRf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2iv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2iw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1rq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1rr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2ix;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/1ro;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/16I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/2hU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/2jO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:LX/HRe;

.field public p:LX/2kw;

.field public q:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public r:LX/4nS;

.field public s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public t:LX/0g7;

.field private u:LX/0fx;

.field public v:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public w:LX/2kW;

.field public x:LX/2kg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kg",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "LX/3DK;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/lang/String;

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2465642
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2465643
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->z:Z

    .line 2465644
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->A:Z

    .line 2465645
    new-instance v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment$1;-><init>(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->B:Ljava/lang/Runnable;

    return-void
.end method

.method private static a(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;LX/HRf;LX/2iv;LX/2iw;LX/1rq;LX/0SI;LX/1rr;LX/2ix;LX/0Ot;Ljava/lang/String;LX/1ro;Ljava/util/concurrent/Executor;LX/16I;LX/2hU;LX/2jO;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;",
            "LX/HRf;",
            "LX/2iv;",
            "LX/2iw;",
            "LX/1rq;",
            "LX/0SI;",
            "LX/1rr;",
            "LX/2ix;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;",
            "Ljava/lang/String;",
            "LX/1ro;",
            "Ljava/util/concurrent/Executor;",
            "LX/16I;",
            "LX/2hU;",
            "LX/2jO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2465646
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->a:LX/HRf;

    iput-object p2, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->b:LX/2iv;

    iput-object p3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->c:LX/2iw;

    iput-object p4, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->d:LX/1rq;

    iput-object p5, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->e:LX/0SI;

    iput-object p6, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->f:LX/1rr;

    iput-object p7, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->g:LX/2ix;

    iput-object p8, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->h:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->i:Ljava/lang/String;

    iput-object p10, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->j:LX/1ro;

    iput-object p11, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->k:Ljava/util/concurrent/Executor;

    iput-object p12, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->l:LX/16I;

    iput-object p13, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->m:LX/2hU;

    iput-object p14, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->n:LX/2jO;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;

    const-class v1, LX/HRf;

    invoke-interface {v14, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/HRf;

    invoke-static {v14}, LX/2iv;->a(LX/0QB;)LX/2iv;

    move-result-object v2

    check-cast v2, LX/2iv;

    const-class v3, LX/2iw;

    invoke-interface {v14, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2iw;

    const-class v4, LX/1rq;

    invoke-interface {v14, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1rq;

    invoke-static {v14}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v5

    check-cast v5, LX/0SI;

    invoke-static {v14}, LX/1rr;->b(LX/0QB;)LX/1rr;

    move-result-object v6

    check-cast v6, LX/1rr;

    invoke-static {v14}, LX/2ix;->a(LX/0QB;)LX/2ix;

    move-result-object v7

    check-cast v7, LX/2ix;

    const/16 v8, 0x4cd

    invoke-static {v14, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v14}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v14}, LX/1ro;->b(LX/0QB;)LX/1ro;

    move-result-object v10

    check-cast v10, LX/1ro;

    invoke-static {v14}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/Executor;

    invoke-static {v14}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v12

    check-cast v12, LX/16I;

    invoke-static {v14}, LX/2hU;->b(LX/0QB;)LX/2hU;

    move-result-object v13

    check-cast v13, LX/2hU;

    invoke-static {v14}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v14

    check-cast v14, LX/2jO;

    invoke-static/range {v0 .. v14}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->a(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;LX/HRf;LX/2iv;LX/2iw;LX/1rq;LX/0SI;LX/1rr;LX/2ix;LX/0Ot;Ljava/lang/String;LX/1ro;Ljava/util/concurrent/Executor;LX/16I;LX/2hU;LX/2jO;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;LX/2ub;)V
    .locals 8

    .prologue
    .line 2465547
    sget-object v0, LX/2uc;->c:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3DO;

    .line 2465548
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    .line 2465549
    iget-object v3, v2, LX/2kW;->o:LX/2kM;

    move-object v2, v3

    .line 2465550
    invoke-interface {v2}, LX/2kM;->a()LX/2nj;

    move-result-object v2

    sget-object v3, LX/3DP;->LAST:LX/3DP;

    const/16 v4, 0xa

    new-instance v5, LX/3DK;

    invoke-direct {p0, p1}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->b(LX/2ub;)Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v5, v6, p1, v0, v7}, LX/3DK;-><init>(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;LX/2ub;LX/3DO;Z)V

    invoke-virtual {v1, v2, v3, v4, v5}, LX/2kW;->a(LX/2nj;LX/3DP;ILjava/lang/Object;)V

    .line 2465551
    return-void
.end method

.method public static b(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;Landroid/content/Context;)LX/HRe;
    .locals 14

    .prologue
    .line 2465647
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->a:LX/HRf;

    .line 2465648
    sget-object v1, LX/2kh;->a:LX/2kh;

    move-object v2, v1

    .line 2465649
    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->B:Ljava/lang/Runnable;

    new-instance v5, LX/HRX;

    invoke-direct {v5, p0}, LX/HRX;-><init>(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V

    move-object v1, p1

    .line 2465650
    new-instance v6, LX/HRe;

    invoke-static {v0}, LX/1ro;->b(LX/0QB;)LX/1ro;

    move-result-object v12

    check-cast v12, LX/1ro;

    const-class v7, LX/HRU;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/HRU;

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v13}, LX/HRe;-><init>(Landroid/content/Context;LX/1PT;LX/Amz;Ljava/lang/Runnable;LX/1PY;LX/1ro;LX/HRU;)V

    .line 2465651
    move-object v0, v6

    .line 2465652
    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->o:LX/HRe;

    .line 2465653
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->o:LX/HRe;

    return-object v0
.end method

.method private b(LX/2ub;)Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;
    .locals 2

    .prologue
    .line 2465654
    new-instance v0, LX/3DL;

    invoke-direct {v0}, LX/3DL;-><init>()V

    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 2465655
    iput-object v1, v0, LX/3DL;->a:LX/0rS;

    .line 2465656
    move-object v0, v0

    .line 2465657
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->e:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 2465658
    iput-object v1, v0, LX/3DL;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2465659
    move-object v1, v0

    .line 2465660
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0dC;

    invoke-virtual {v0}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v0

    .line 2465661
    iput-object v0, v1, LX/3DL;->l:Ljava/lang/String;

    .line 2465662
    move-object v0, v1

    .line 2465663
    invoke-virtual {p1}, LX/2ub;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2465664
    iput-object v1, v0, LX/3DL;->g:Ljava/lang/String;

    .line 2465665
    move-object v0, v0

    .line 2465666
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    .line 2465667
    iput-object v1, v0, LX/3DL;->p:Lcom/facebook/common/callercontext/CallerContext;

    .line 2465668
    move-object v0, v0

    .line 2465669
    invoke-virtual {v0}, LX/3DL;->q()Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V
    .locals 7

    .prologue
    .line 2465670
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    const/16 v2, 0xa

    new-instance v3, LX/3DK;

    sget-object v0, LX/2ub;->SCROLL:LX/2ub;

    invoke-direct {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->b(LX/2ub;)Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;

    move-result-object v4

    sget-object v5, LX/2ub;->SCROLL:LX/2ub;

    sget-object v0, LX/2uc;->c:LX/0P1;

    sget-object v6, LX/2ub;->SCROLL:LX/2ub;

    invoke-virtual {v0, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3DO;

    invoke-direct {v3, v4, v5, v0}, LX/3DK;-><init>(Lcom/facebook/notifications/protocol/FetchGraphQLNotificationsParams;LX/2ub;LX/3DO;)V

    invoke-virtual {v1, v2, v3}, LX/2kW;->b(ILjava/lang/Object;)V

    .line 2465671
    return-void
.end method

.method public static c(LX/2nk;)Z
    .locals 1

    .prologue
    .line 2465672
    sget-object v0, LX/2nk;->AFTER:LX/2nk;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V
    .locals 7

    .prologue
    .line 2465673
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 2465674
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    if-nez v3, :cond_0

    move-object v3, v4

    .line 2465675
    :goto_0
    move-object v0, v3

    .line 2465676
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->k:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment$2;-><init>(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;Ljava/lang/Long;)V

    const v0, -0x77820c27

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2465677
    return-void

    .line 2465678
    :cond_0
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    .line 2465679
    iget-object v5, v3, LX/2kW;->o:LX/2kM;

    move-object v5, v5

    .line 2465680
    invoke-interface {v5}, LX/2kM;->c()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5, v6}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v5, v6}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    if-nez v3, :cond_2

    :cond_1
    move-object v3, v4

    .line 2465681
    goto :goto_0

    .line 2465682
    :cond_2
    invoke-interface {v5, v6}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0
.end method

.method public static l(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V
    .locals 3

    .prologue
    .line 2465683
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->l:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2465684
    sget-object v0, LX/2ub;->PULL_TO_REFRESH:LX/2ub;

    invoke-static {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->a$redex0(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;LX/2ub;)V

    .line 2465685
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->n:LX/2jO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2jO;->a(LX/DqC;)V

    .line 2465686
    return-void

    .line 2465687
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->q:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2465688
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->r:LX/4nS;

    if-eqz v0, :cond_1

    .line 2465689
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->r:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    .line 2465690
    :cond_1
    new-instance v0, LX/62h;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/62h;-><init>(Landroid/content/Context;)V

    .line 2465691
    new-instance v1, LX/HRc;

    invoke-direct {v1, p0}, LX/HRc;-><init>(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V

    invoke-virtual {v0, v1}, LX/62h;->setRetryClickListener(Landroid/view/View$OnClickListener;)V

    .line 2465692
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->m:LX/2hU;

    const/16 v2, 0x2710

    invoke-virtual {v1, v0, v2}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->r:LX/4nS;

    .line 2465693
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->r:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->a()V

    .line 2465694
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2465636
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2465637
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2465638
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2465639
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    if-eqz v0, :cond_0

    .line 2465640
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->d(I)V

    .line 2465641
    :cond_0
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 2465635
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()LX/0g8;
    .locals 1

    .prologue
    .line 2465634
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2465631
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2465632
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    invoke-static {v0}, LX/62G;->a(LX/0g8;)V

    .line 2465633
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, 0x7e8ee91d

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2465586
    const v0, 0x7f030c2e

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2465587
    const v0, 0x102000a

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2465588
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v3, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2465589
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 2465590
    new-instance v0, LX/0g7;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-direct {v0, v3}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    .line 2465591
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    invoke-virtual {v0}, LX/0g7;->k()V

    .line 2465592
    new-instance v0, LX/HRd;

    invoke-direct {v0, p0}, LX/HRd;-><init>(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V

    move-object v0, v0

    .line 2465593
    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->u:LX/0fx;

    .line 2465594
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->u:LX/0fx;

    invoke-virtual {v0, v3}, LX/0g7;->b(LX/0fx;)V

    .line 2465595
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 2465596
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v4, 0x7f0101e2

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v3, v5}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2465597
    const v0, 0x7f0d05b0

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->v:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2465598
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->v:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget v4, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v4}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setBackgroundResource(I)V

    .line 2465599
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->v:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2465600
    iget v0, v3, Landroid/util/TypedValue;->resourceId:I

    .line 2465601
    const v3, 0x7f0d0595

    invoke-static {v2, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->q:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2465602
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->q:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v3, v0}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setBackgroundResource(I)V

    .line 2465603
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->q:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v4, LX/HRb;

    invoke-direct {v4, p0}, LX/HRb;-><init>(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2465604
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->q:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 2465605
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->g:LX/2ix;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    invoke-virtual {v0, v3}, LX/2ix;->a(LX/0g8;)V

    .line 2465606
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->g:LX/2ix;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    iget-object v4, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v3, v4}, LX/2ix;->a(LX/0g8;Landroid/support/v7/widget/RecyclerView;)V

    .line 2465607
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "pages_fb4a_notifications_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->y:Ljava/lang/String;

    .line 2465608
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->d:LX/1rq;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->y:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->f:LX/1rr;

    invoke-virtual {v0, v3, v4}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v0

    sget-object v3, LX/2jq;->CHECK_SERVER_FOR_NEWDATA:LX/2jq;

    .line 2465609
    iput-object v3, v0, LX/2jj;->i:LX/2jq;

    .line 2465610
    move-object v0, v0

    .line 2465611
    new-instance v3, LX/2ju;

    invoke-direct {v3}, LX/2ju;-><init>()V

    .line 2465612
    iput-object v3, v0, LX/2jj;->m:LX/0QK;

    .line 2465613
    move-object v0, v0

    .line 2465614
    const/16 v3, 0xa

    .line 2465615
    iput v3, v0, LX/2jj;->o:I

    .line 2465616
    move-object v0, v0

    .line 2465617
    invoke-virtual {v0}, LX/2jj;->a()LX/2kW;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    .line 2465618
    new-instance v0, LX/HRY;

    invoke-direct {v0, p0}, LX/HRY;-><init>(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V

    .line 2465619
    new-instance v3, LX/HRa;

    invoke-direct {v3, p0, v0}, LX/HRa;-><init>(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;LX/2ke;)V

    move-object v0, v3

    .line 2465620
    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->x:LX/2kg;

    .line 2465621
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->x:LX/2kg;

    invoke-virtual {v0, v3}, LX/2kW;->a(LX/1vq;)V

    .line 2465622
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2465623
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->p:LX/2kw;

    if-nez v3, :cond_0

    .line 2465624
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->c:LX/2iw;

    invoke-static {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->b(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;Landroid/content/Context;)LX/HRe;

    move-result-object v4

    new-instance v5, LX/2kv;

    invoke-direct {v5}, LX/2kv;-><init>()V

    iget-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->b:LX/2iv;

    invoke-virtual {v3, v4, v5, p1}, LX/2iw;->a(LX/1PW;LX/1DZ;LX/0g1;)LX/2kw;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->p:LX/2kw;

    .line 2465625
    :cond_0
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->p:LX/2kw;

    new-instance v4, LX/HRV;

    invoke-direct {v4, p0}, LX/HRV;-><init>(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V

    .line 2465626
    iput-object v4, v3, LX/2kw;->j:Landroid/view/View$OnClickListener;

    .line 2465627
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->p:LX/2kw;

    sget-object v4, LX/2kx;->FINISHED:LX/2kx;

    invoke-virtual {v3, v4}, LX/2kw;->a(LX/2kx;)V

    .line 2465628
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->p:LX/2kw;

    invoke-virtual {v0, v3}, LX/0g7;->a(LX/1OO;)V

    .line 2465629
    sget-object v0, LX/2ub;->FRAGMENT_LOADED:LX/2ub;

    invoke-static {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->a$redex0(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;LX/2ub;)V

    .line 2465630
    const/16 v0, 0x2b

    const v3, 0x344e7073

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x55e29645

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2465567
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2465568
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->u:LX/0fx;

    invoke-virtual {v1, v2}, LX/0g7;->c(LX/0fx;)V

    .line 2465569
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    invoke-virtual {v1, v3}, LX/0g7;->a(Landroid/view/View$OnTouchListener;)V

    .line 2465570
    iput-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    .line 2465571
    iput-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2465572
    iput-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->u:LX/0fx;

    .line 2465573
    iput-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->v:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2465574
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->q:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2465575
    iput-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->q:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2465576
    iput-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->r:LX/4nS;

    .line 2465577
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->b:LX/2iv;

    if-eqz v1, :cond_0

    .line 2465578
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->b:LX/2iv;

    .line 2465579
    iput-object v3, v1, LX/2iv;->a:LX/2kM;

    .line 2465580
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    if-eqz v1, :cond_1

    .line 2465581
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->x:LX/2kg;

    invoke-virtual {v1, v2}, LX/2kW;->b(LX/1vq;)V

    .line 2465582
    iput-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->x:LX/2kg;

    .line 2465583
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    invoke-virtual {v1}, LX/2kW;->c()V

    .line 2465584
    iput-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    .line 2465585
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x61d9544d

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6929f217

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2465560
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2465561
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->A:Z

    .line 2465562
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->n:LX/2jO;

    .line 2465563
    iput-object v2, v1, LX/2jO;->j:LX/2kW;

    .line 2465564
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->n:LX/2jO;

    .line 2465565
    iput-object v2, v1, LX/2jO;->h:Ljava/lang/Runnable;

    .line 2465566
    const/16 v1, 0x2b

    const v2, -0x4879deee

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x85df7eb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2465552
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2465553
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->A:Z

    .line 2465554
    invoke-static {p0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->c$redex0(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V

    .line 2465555
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->n:LX/2jO;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->w:LX/2kW;

    .line 2465556
    iput-object v2, v1, LX/2jO;->j:LX/2kW;

    .line 2465557
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->n:LX/2jO;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->B:Ljava/lang/Runnable;

    .line 2465558
    iput-object v2, v1, LX/2jO;->h:Ljava/lang/Runnable;

    .line 2465559
    const/16 v1, 0x2b

    const v2, -0x66231e29

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x868bf50

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2465542
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2465543
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2465544
    if-eqz v0, :cond_0

    .line 2465545
    const v2, 0x7f081153

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2465546
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x6fbf2287

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6be20b4d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2465539
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2465540
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->n:LX/2jO;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/2jO;->a(LX/DqC;)V

    .line 2465541
    const/16 v1, 0x2b

    const v2, 0x6495277f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 2

    .prologue
    .line 2465527
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    .line 2465528
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2465529
    iput-boolean p1, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->A:Z

    .line 2465530
    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2465531
    :cond_0
    :goto_0
    return-void

    .line 2465532
    :cond_1
    if-eqz p1, :cond_3

    .line 2465533
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    if-eqz v0, :cond_2

    .line 2465534
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->t:LX/0g7;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0g7;->g(I)V

    .line 2465535
    :cond_2
    invoke-static {p0}, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->c$redex0(Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;)V

    goto :goto_0

    .line 2465536
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->n:LX/2jO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2jO;->a(LX/DqC;)V

    .line 2465537
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->r:LX/4nS;

    if-eqz v0, :cond_0

    .line 2465538
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/notifications/PagesNotificationsConnectionControllerFragment;->r:LX/4nS;

    invoke-virtual {v0}, LX/4nS;->b()V

    goto :goto_0
.end method
