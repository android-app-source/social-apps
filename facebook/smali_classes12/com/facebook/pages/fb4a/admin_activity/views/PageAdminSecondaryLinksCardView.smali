.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/4AB;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/dracula/runtime/jdk/DraculaMap$1$Dracula",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2466421
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2466422
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->e()V

    .line 2466423
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2466418
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466419
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->e()V

    .line 2466420
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2466415
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466416
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->e()V

    .line 2466417
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;

    const/16 v1, 0xf9a

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->a:LX/0Ot;

    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2466411
    const v0, 0x7f030e17

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2466412
    const-class v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2466413
    new-instance v0, LX/4AJ;

    invoke-direct {v0}, LX/4AJ;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->b:LX/4AB;

    .line 2466414
    return-void
.end method

.method private getItems()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/3rL",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;",
            "LX/HS1;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2466399
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2466400
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->b:LX/4AB;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->ALL:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    invoke-virtual {v1, v2}, LX/4AB;->b(Ljava/lang/Object;)LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_0

    .line 2466401
    new-instance v1, LX/3rL;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->ALL:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    sget-object v3, LX/HS1;->RECENT_ACTIVITY:LX/HS1;

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466402
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->b:LX/4AB;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->MENTION:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    invoke-virtual {v1, v2}, LX/4AB;->b(Ljava/lang/Object;)LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_1

    .line 2466403
    new-instance v1, LX/3rL;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->MENTION:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    sget-object v3, LX/HS1;->RECENT_MENTIONS:LX/HS1;

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466404
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->b:LX/4AB;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->SHARE:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    invoke-virtual {v1, v2}, LX/4AB;->b(Ljava/lang/Object;)LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_2

    .line 2466405
    new-instance v1, LX/3rL;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->SHARE:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    sget-object v3, LX/HS1;->RECENT_SHARES:LX/HS1;

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466406
    :cond_2
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->b:LX/4AB;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->REVIEW:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    invoke-virtual {v1, v2}, LX/4AB;->b(Ljava/lang/Object;)LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_3

    .line 2466407
    new-instance v1, LX/3rL;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->REVIEW:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    sget-object v3, LX/HS1;->RECENT_REVIEWS:LX/HS1;

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466408
    :cond_3
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->b:LX/4AB;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    invoke-virtual {v1, v2}, LX/4AB;->b(Ljava/lang/Object;)LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_4

    .line 2466409
    new-instance v1, LX/3rL;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    sget-object v3, LX/HS1;->RECENT_CHECK_INS:LX/HS1;

    invoke-direct {v1, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466410
    :cond_4
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2466397
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->b:LX/4AB;

    invoke-virtual {v0}, LX/4AB;->a()V

    .line 2466398
    return-void
.end method

.method public final a(JLX/2uF;LX/0am;)V
    .locals 11
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/2uF;",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2466385
    invoke-virtual {p3}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2466386
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->b:LX/4AB;

    const-class v5, Lcom/facebook/graphql/enums/GraphQLPageActivityFeedType;

    const/4 v6, 0x0

    invoke-virtual {v2, v1, v4, v5, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v5

    const v6, 0x31ead959

    invoke-virtual {v3, v5, v2, v1, v6}, LX/4AB;->a(Ljava/lang/Object;LX/15i;II)LX/1vs;

    goto :goto_0

    .line 2466387
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->getItems()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v4

    :goto_1
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 2466388
    iget-object v1, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v1, LX/HS1;

    iget v1, v1, LX/HS1;->resId:I

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/HS4;

    .line 2466389
    iget-object v2, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v2, LX/HS1;

    iget-object v2, v2, LX/HS1;->uri:Ljava/lang/String;

    invoke-virtual {v1, v2, p1, p2, p4}, LX/HS4;->a(Ljava/lang/String;JLX/0am;)V

    .line 2466390
    iget-object v2, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v2, LX/HS1;

    iget-object v2, v2, LX/HS1;->loggingEvent:LX/9X5;

    .line 2466391
    iput-object v2, v1, LX/HS4;->b:LX/9X2;

    .line 2466392
    sget-object v2, LX/HS9;->SECONDARY:LX/HS9;

    invoke-virtual {v1, v2}, LX/HS4;->setBadgeStyle(LX/HS9;)V

    .line 2466393
    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->b:LX/4AB;

    iget-object v0, v0, LX/3rL;->a:Ljava/lang/Object;

    invoke-virtual {v2, v0}, LX/4AB;->b(Ljava/lang/Object;)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v7, 0x1

    invoke-virtual {v2, v0, v7}, LX/15i;->j(II)I

    move-result v0

    int-to-long v8, v0

    invoke-virtual {v1, v8, v9}, LX/HS4;->setBadgeNumber(J)V

    .line 2466394
    invoke-virtual {v1, v4}, LX/HS4;->setVisibility(I)V

    .line 2466395
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2466396
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2466377
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3ke;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2466378
    :goto_0
    return-void

    .line 2466379
    :cond_0
    const v0, 0x7f0d2275    # 1.8760006E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2466380
    new-instance v1, LX/0hs;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2466381
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081800

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2466382
    sget-object v2, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 2466383
    invoke-virtual {v1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 2466384
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminSecondaryLinksCardView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3ke;->c:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method
