.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;
.super LX/HS4;
.source ""


# instance fields
.field public h:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2466541
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466542
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2466543
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466544
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2466545
    invoke-direct {p0, p1, p2, p3}, LX/HS4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466546
    const-class v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2466547
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v0

    check-cast v0, LX/17Y;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;->h:LX/17Y;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;JLX/0am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2466548
    new-instance v0, LX/HS6;

    invoke-direct {v0, p0, p2, p3, p1}, LX/HS6;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;JLjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityMessagesLinkView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2466549
    return-void
.end method
