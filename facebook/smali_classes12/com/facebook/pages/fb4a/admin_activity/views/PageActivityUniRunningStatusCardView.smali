.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/HRn;


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

.field private g:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

.field private h:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

.field private i:Landroid/widget/ImageView;

.field private j:Lcom/facebook/resources/ui/FbTextView;

.field private k:Landroid/widget/LinearLayout;

.field private l:Landroid/view/View;

.field private m:LX/154;

.field public n:LX/9XE;

.field public o:LX/GMm;

.field private p:LX/2Pb;

.field private q:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2466242
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2466243
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->a()V

    .line 2466244
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2466239
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466240
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->a()V

    .line 2466241
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2466236
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466237
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->a()V

    .line 2466238
    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2466233
    const/16 v0, 0x2710

    if-ge p1, v0, :cond_0

    .line 2466234
    const-string v0, "%,d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2466235
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->m:LX/154;

    invoke-virtual {v0, p1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2466217
    const v0, 0x7f030e0c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2466218
    const-class v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2466219
    const v0, 0x7f0d2251

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->a:Landroid/widget/ImageView;

    .line 2466220
    const v0, 0x7f0d2252

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2466221
    const v0, 0x7f0d2253

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2466222
    const v0, 0x7f0d2255

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2466223
    const v0, 0x7f0d2256

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2466224
    const v0, 0x7f0d2257

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->f:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    .line 2466225
    const v0, 0x7f0d2258

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->g:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    .line 2466226
    const v0, 0x7f0d2259

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->h:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    .line 2466227
    const v0, 0x7f0d225b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->k:Landroid/widget/LinearLayout;

    .line 2466228
    const v0, 0x7f0d225c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->i:Landroid/widget/ImageView;

    .line 2466229
    const v0, 0x7f0d225d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2466230
    const v0, 0x7f0d225a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->l:Landroid/view/View;

    .line 2466231
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    .line 2466232
    return-void
.end method

.method private a(LX/154;LX/9XE;LX/GMm;LX/2Pb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2466146
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->m:LX/154;

    .line 2466147
    iput-object p2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->n:LX/9XE;

    .line 2466148
    iput-object p3, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->o:LX/GMm;

    .line 2466149
    iput-object p3, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->o:LX/GMm;

    .line 2466150
    iput-object p4, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->p:LX/2Pb;

    .line 2466151
    return-void
.end method

.method private a(LX/15i;I)V
    .locals 4

    .prologue
    .line 2466211
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->j(II)I

    move-result v0

    .line 2466212
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2466213
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v3, 0x7f0f00a6

    invoke-virtual {v2, v3, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2466214
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->d:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x4

    invoke-virtual {p1, p2, v1}, LX/15i;->j(II)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2466215
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->e:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081642

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2466216
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;

    invoke-static {v3}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v0

    check-cast v0, LX/154;

    invoke-static {v3}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v1

    check-cast v1, LX/9XE;

    invoke-static {v3}, LX/GMm;->a(LX/0QB;)LX/GMm;

    move-result-object v2

    check-cast v2, LX/GMm;

    invoke-static {v3}, LX/2Pb;->a(LX/0QB;)LX/2Pb;

    move-result-object v3

    check-cast v3, LX/2Pb;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->a(LX/154;LX/9XE;LX/GMm;LX/2Pb;)V

    return-void
.end method

.method private b(LX/15i;I)V
    .locals 3

    .prologue
    .line 2466203
    const/4 v0, 0x5

    const-class v1, Lcom/facebook/graphql/enums/GraphQLAdproLimitResetPeriod;

    const/4 v2, 0x0

    invoke-virtual {p1, p2, v0, v1, v2}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdproLimitResetPeriod;

    .line 2466204
    sget-object v1, LX/HRz;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLAdproLimitResetPeriod;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2466205
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v1, 0x7f081647

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2466206
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->g:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    invoke-virtual {v1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;->setName(Ljava/lang/String;)V

    .line 2466207
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->g:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    const/4 v1, 0x1

    invoke-virtual {p1, p2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;->setValue(Ljava/lang/String;)V

    .line 2466208
    return-void

    .line 2466209
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v1, 0x7f081644

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2466210
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v1, 0x7f081645

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c(LX/15i;I)V
    .locals 3

    .prologue
    .line 2466200
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->f:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v2, 0x7f081643

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;->setName(Ljava/lang/String;)V

    .line 2466201
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->f:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    const/4 v1, 0x6

    invoke-virtual {p1, p2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;->setValue(Ljava/lang/String;)V

    .line 2466202
    return-void
.end method

.method private d(LX/15i;I)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2466192
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->h:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v2, 0x7f081648

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;->setName(Ljava/lang/String;)V

    .line 2466193
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v0

    .line 2466194
    new-instance v1, Ljava/sql/Date;

    const/4 v2, 0x7

    invoke-virtual {p1, p2, v2}, LX/15i;->k(II)J

    move-result-wide v2

    mul-long/2addr v2, v10

    invoke-direct {v1, v2, v3}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 2466195
    invoke-virtual {p1, p2, v8}, LX/15i;->k(II)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2466196
    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v3, 0x7f081649

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v6

    new-instance v1, Ljava/sql/Date;

    invoke-virtual {p1, p2, v8}, LX/15i;->k(II)J

    move-result-wide v4

    mul-long/2addr v4, v10

    invoke-direct {v1, v4, v5}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2466197
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->h:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;

    invoke-virtual {v1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;->setValue(Ljava/lang/String;)V

    .line 2466198
    return-void

    .line 2466199
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v2, 0x7f08164a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v1, v2, v6

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private e(LX/15i;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0x8

    const/4 v2, 0x0

    .line 2466169
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v3, 0x7f0a00e5

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2466170
    sget-object v4, LX/HRz;->b:[I

    const-class v0, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    invoke-virtual {p1, p2, v2, v0, v1}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    move v1, v2

    .line 2466171
    :goto_0
    if-nez v0, :cond_0

    .line 2466172
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2466173
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->l:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2466174
    :goto_1
    return-void

    .line 2466175
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v1, 0x7f081650

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v1, v2

    .line 2466176
    goto :goto_0

    .line 2466177
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v1, 0x7f08164c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2466178
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v3, 0x7f0a0580

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2466179
    const v1, 0x7f02137c

    goto :goto_0

    .line 2466180
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v1, 0x7f08164d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2466181
    const v1, 0x7f02137d

    goto :goto_0

    .line 2466182
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v1, 0x7f08164f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v1, v2

    .line 2466183
    goto :goto_0

    .line 2466184
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->q:Landroid/content/res/Resources;

    const v1, 0x7f08164e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v1, v2

    .line 2466185
    goto :goto_0

    .line 2466186
    :cond_0
    iget-object v4, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2466187
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2466188
    if-eqz v1, :cond_1

    .line 2466189
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2466190
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 2466191
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(JLX/15i;ILX/0am;)V
    .locals 7
    .param p3    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/15i;",
            "I",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x4

    .line 2466152
    if-nez p4, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 2466153
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->setVisibility(I)V

    .line 2466154
    :goto_1
    return-void

    .line 2466155
    :cond_1
    invoke-virtual {p3, p4, v5}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2466156
    :cond_2
    invoke-virtual {p3, p4, v5}, LX/15i;->g(II)I

    move-result v0

    .line 2466157
    const-class v2, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    const/4 v3, 0x0

    invoke-virtual {p3, v0, v1, v2, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    .line 2466158
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    if-ne v0, v1, :cond_4

    :cond_3
    sget-object v0, LX/8wW;->EVENT_RENDER_FAIL_ENTRY_POINT:LX/8wW;

    .line 2466159
    :goto_2
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->p:LX/2Pb;

    sget-object v2, LX/8wV;->PROMOTE_PAGE_MOBILE_MODULE:LX/8wV;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "pages_manager_activity_tab"

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2Pb;->a(LX/8wV;LX/8wW;Ljava/lang/String;Ljava/lang/String;)V

    .line 2466160
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->a:Landroid/widget/ImageView;

    new-instance v1, LX/HRy;

    invoke-direct {v1, p0, p1, p2, p5}, LX/HRy;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;JLX/0am;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2466161
    invoke-virtual {p3, p4, v5}, LX/15i;->g(II)I

    move-result v0

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2466162
    invoke-direct {p0, p3, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->a(LX/15i;I)V

    .line 2466163
    invoke-direct {p0, p3, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->b(LX/15i;I)V

    .line 2466164
    invoke-direct {p0, p3, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->c(LX/15i;I)V

    .line 2466165
    invoke-direct {p0, p3, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->d(LX/15i;I)V

    .line 2466166
    invoke-direct {p0, p3, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->e(LX/15i;I)V

    goto :goto_1

    .line 2466167
    :cond_4
    sget-object v0, LX/8wW;->EVENT_RENDER_EDIT_ENTRY_POINT:LX/8wW;

    goto :goto_2

    .line 2466168
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
