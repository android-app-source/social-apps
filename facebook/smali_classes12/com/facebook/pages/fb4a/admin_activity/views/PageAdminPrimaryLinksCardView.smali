.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2466374
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2466375
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a()V

    .line 2466376
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2466371
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466372
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a()V

    .line 2466373
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2466368
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466369
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a()V

    .line 2466370
    return-void
.end method

.method private a(Z)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/0Px",
            "<",
            "LX/HS1;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2466355
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2466356
    if-eqz p1, :cond_0

    .line 2466357
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->b:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/17g;->i:S

    invoke-interface {v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2466358
    sget-object v1, LX/HS1;->MESSAGES:LX/HS1;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466359
    :cond_0
    sget-object v1, LX/HS1;->NOTIFICATIONS:LX/HS1;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466360
    sget-object v1, LX/HS1;->PAGES_FEED:LX/HS1;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466361
    sget-object v1, LX/HS1;->NEW_LIKES:LX/HS1;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466362
    sget-object v1, LX/HS1;->SCHEDULED_POSTS:LX/HS1;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466363
    sget-object v1, LX/HS1;->DRAFTS:LX/HS1;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466364
    sget-object v1, LX/HS1;->FOLLOWERS:LX/HS1;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466365
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a:LX/0Uh;

    sget v2, LX/FQe;->i:I

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2466366
    sget-object v1, LX/HS1;->PAGE_TIPS:LX/HS1;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2466367
    :cond_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2466354
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2466350
    const v0, 0x7f030e16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2466351
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->c:Landroid/os/Handler;

    .line 2466352
    const-class v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2466353
    return-void
.end method

.method private static a(Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;LX/0Uh;LX/0ad;)V
    .locals 0

    .prologue
    .line 2466349
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a:LX/0Uh;

    iput-object p2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->b:LX/0ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;

    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a(Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;LX/0Uh;LX/0ad;)V

    return-void
.end method

.method public static synthetic b(Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2466348
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic c(Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2466347
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic d(Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2466346
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(II)V
    .locals 5

    .prologue
    .line 2466344
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView$1;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;II)V

    const-wide/16 v2, 0x64

    const v4, -0x4cc22b3

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2466345
    return-void
.end method

.method public final a(JLX/15i;ILX/0am;Z)V
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/15i;",
            "I",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2466332
    invoke-direct {p0, p6}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a(Z)LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_2

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HS1;

    .line 2466333
    iget v1, v0, LX/HS1;->resId:I

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/HS4;

    .line 2466334
    iget-object v6, v0, LX/HS1;->uri:Ljava/lang/String;

    invoke-virtual {v1, v6, p1, p2, p5}, LX/HS4;->a(Ljava/lang/String;JLX/0am;)V

    .line 2466335
    iget-object v6, v0, LX/HS1;->loggingEvent:LX/9X5;

    .line 2466336
    iput-object v6, v1, LX/HS4;->b:LX/9X2;

    .line 2466337
    invoke-virtual {v1, v3}, LX/HS4;->setVisibility(I)V

    .line 2466338
    sget-object v6, LX/HS1;->NEW_LIKES:LX/HS1;

    invoke-virtual {v0, v6}, LX/HS1;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, LX/HS1;->SCHEDULED_POSTS:LX/HS1;

    invoke-virtual {v0, v6}, LX/HS1;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    sget-object v6, LX/HS1;->DRAFTS:LX/HS1;

    invoke-virtual {v0, v6}, LX/HS1;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2466339
    :cond_0
    sget-object v0, LX/HS9;->SECONDARY:LX/HS9;

    invoke-virtual {v1, v0}, LX/HS4;->setBadgeStyle(LX/HS9;)V

    .line 2466340
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2466341
    :cond_2
    invoke-virtual {p0, p3, p4}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->a(LX/15i;I)V

    .line 2466342
    invoke-virtual {p0, p3, p4}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->b(LX/15i;I)V

    .line 2466343
    return-void
.end method

.method public final a(LX/15i;I)V
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updateScheduledPost"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2466328
    if-nez p2, :cond_0

    .line 2466329
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView$2;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;I)V

    const-wide/16 v4, 0x64

    const v0, -0x5e7ec6ca

    invoke-static {v1, v2, v4, v5, v0}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2466330
    return-void

    .line 2466331
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p1, p2, v1}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {p1, v1, v0}, LX/15i;->j(II)I

    move-result v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)V
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updateDrafts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2466324
    if-nez p2, :cond_0

    .line 2466325
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView$3;

    invoke-direct {v2, p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView$3;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminPrimaryLinksCardView;I)V

    const-wide/16 v4, 0x64

    const v0, 0x1b0cc62a

    invoke-static {v1, v2, v4, v5, v0}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2466326
    return-void

    .line 2466327
    :cond_0
    invoke-virtual {p1, p2, v0}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {p1, v1, v0}, LX/15i;->j(II)I

    move-result v0

    goto :goto_0
.end method
