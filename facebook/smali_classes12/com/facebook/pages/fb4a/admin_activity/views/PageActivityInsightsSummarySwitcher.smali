.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/9XE;

.field private b:Landroid/content/res/Resources;

.field public c:LX/HRm;

.field public d:LX/HRl;

.field private e:J

.field private f:Landroid/widget/RelativeLayout;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Landroid/widget/RelativeLayout;

.field private j:Lcom/facebook/resources/ui/FbTextView;

.field private k:Lcom/facebook/resources/ui/FbTextView;

.field private l:LX/154;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2465786
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2465787
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->e:J

    .line 2465788
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->a()V

    .line 2465789
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2465790
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2465791
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->e:J

    .line 2465792
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->a()V

    .line 2465793
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2465806
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2465807
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->e:J

    .line 2465808
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->a()V

    .line 2465809
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2465794
    const v0, 0x7f030e08

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2465795
    const-class v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2465796
    const v0, 0x7f0d2246

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->f:Landroid/widget/RelativeLayout;

    .line 2465797
    const v0, 0x7f0d2247

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2465798
    const v0, 0x7f0d2248

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2465799
    const v0, 0x7f0d224a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->i:Landroid/widget/RelativeLayout;

    .line 2465800
    const v0, 0x7f0d224b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2465801
    const v0, 0x7f0d224c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2465802
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->b:Landroid/content/res/Resources;

    .line 2465803
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->f:Landroid/widget/RelativeLayout;

    new-instance v1, LX/HRj;

    invoke-direct {v1, p0}, LX/HRj;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2465804
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->i:Landroid/widget/RelativeLayout;

    new-instance v1, LX/HRk;

    invoke-direct {v1, p0}, LX/HRk;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2465805
    return-void
.end method

.method private a(LX/154;LX/9XE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2465810
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->l:LX/154;

    .line 2465811
    iput-object p2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->a:LX/9XE;

    .line 2465812
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;

    invoke-static {v1}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v0

    check-cast v0, LX/154;

    invoke-static {v1}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v1

    check-cast v1, LX/9XE;

    invoke-direct {p0, v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->a(LX/154;LX/9XE;)V

    return-void
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 2465781
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, p1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2465782
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->b:Landroid/content/res/Resources;

    const v1, 0x7f0f00a5

    invoke-virtual {v0, v1, p1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 2465783
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2465784
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2465785
    return-void
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 2465776
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, p1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->e(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2465777
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->b:Landroid/content/res/Resources;

    const v1, 0x7f08163c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2465778
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2465779
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2465780
    return-void
.end method

.method private e(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2465773
    const/16 v0, 0x2710

    if-ge p1, v0, :cond_0

    .line 2465774
    const-string v0, "%,d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2465775
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->l:LX/154;

    invoke-virtual {v0, p1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/15i;IJ)V
    .locals 3
    .param p1    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2465767
    if-nez p2, :cond_0

    .line 2465768
    :goto_0
    return-void

    .line 2465769
    :cond_0
    iput-wide p3, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->e:J

    .line 2465770
    invoke-virtual {p1, p2, v1}, LX/15i;->j(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->c(I)V

    .line 2465771
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->j(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->d(I)V

    .line 2465772
    invoke-virtual {p0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->setVisibility(I)V

    goto :goto_0
.end method

.method public getSwitcherState()LX/HRl;
    .locals 1

    .prologue
    .line 2465766
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->d:LX/HRl;

    return-object v0
.end method

.method public setInsightsSwitcherStateListener(LX/HRm;)V
    .locals 0

    .prologue
    .line 2465764
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->c:LX/HRm;

    .line 2465765
    return-void
.end method

.method public setSwitcherState(LX/HRl;)V
    .locals 4

    .prologue
    .line 2465755
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->d:LX/HRl;

    if-eq v0, p1, :cond_2

    .line 2465756
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->c:LX/HRm;

    if-eqz v0, :cond_1

    .line 2465757
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->d:LX/HRl;

    if-eqz v0, :cond_3

    sget-object v0, LX/HRl;->WEEKLY_LIKE:LX/HRl;

    if-ne p1, v0, :cond_3

    .line 2465758
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->a:LX/9XE;

    sget-object v1, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_INSIGHTS_WEEKLY_LIKE:LX/9X5;

    iget-wide v2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->b(LX/9X2;J)V

    .line 2465759
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->c:LX/HRm;

    invoke-interface {v0, p1}, LX/HRm;->a(LX/HRl;)V

    .line 2465760
    :cond_1
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->d:LX/HRl;

    .line 2465761
    :cond_2
    return-void

    .line 2465762
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->d:LX/HRl;

    if-eqz v0, :cond_0

    sget-object v0, LX/HRl;->WEEKLY_POST_REACH:LX/HRl;

    if-ne p1, v0, :cond_0

    .line 2465763
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->a:LX/9XE;

    sget-object v1, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_INSIGHTS_WEEKLY_POST_REACH:LX/9X5;

    iget-wide v2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->b(LX/9X2;J)V

    goto :goto_0
.end method
