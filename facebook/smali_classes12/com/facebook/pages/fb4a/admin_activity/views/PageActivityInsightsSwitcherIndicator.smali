.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:LX/HRl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2465813
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2465814
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->a()V

    .line 2465815
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2465816
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2465817
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->a()V

    .line 2465818
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2465819
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2465820
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->a()V

    .line 2465821
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2465822
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->a:Landroid/graphics/Paint;

    .line 2465823
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2465824
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->a:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2465825
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a057f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2465826
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2465827
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->getMeasuredHeight()I

    move-result v0

    .line 2465828
    mul-int/lit8 v1, v0, 0x2

    .line 2465829
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->getMeasuredWidth()I

    move-result v2

    .line 2465830
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 2465831
    int-to-float v4, v0

    invoke-virtual {v3, v6, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2465832
    iget-object v4, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->b:LX/HRl;

    sget-object v5, LX/HRl;->WEEKLY_LIKE:LX/HRl;

    if-ne v4, v5, :cond_1

    .line 2465833
    div-int/lit8 v4, v2, 0x4

    div-int/lit8 v5, v1, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    int-to-float v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2465834
    div-int/lit8 v4, v2, 0x4

    int-to-float v4, v4

    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2465835
    div-int/lit8 v4, v2, 0x4

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v4

    int-to-float v1, v1

    int-to-float v4, v0

    invoke-virtual {v3, v1, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2465836
    int-to-float v1, v2

    int-to-float v0, v0

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2465837
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2465838
    return-void

    .line 2465839
    :cond_1
    iget-object v4, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->b:LX/HRl;

    sget-object v5, LX/HRl;->WEEKLY_POST_REACH:LX/HRl;

    if-ne v4, v5, :cond_0

    .line 2465840
    mul-int/lit8 v4, v2, 0x3

    div-int/lit8 v4, v4, 0x4

    div-int/lit8 v5, v1, 0x2

    sub-int/2addr v4, v5

    int-to-float v4, v4

    int-to-float v5, v0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2465841
    mul-int/lit8 v4, v2, 0x3

    div-int/lit8 v4, v4, 0x4

    int-to-float v4, v4

    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2465842
    mul-int/lit8 v4, v2, 0x3

    div-int/lit8 v4, v4, 0x4

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v4

    int-to-float v1, v1

    int-to-float v4, v0

    invoke-virtual {v3, v1, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2465843
    int-to-float v1, v2

    int-to-float v0, v0

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_0
.end method

.method public setSwitcherState(LX/HRl;)V
    .locals 1

    .prologue
    .line 2465844
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->b:LX/HRl;

    if-eq v0, p1, :cond_0

    .line 2465845
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->b:LX/HRl;

    .line 2465846
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->invalidate()V

    .line 2465847
    :cond_0
    return-void
.end method
