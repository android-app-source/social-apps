.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;

.field private b:LX/FQY;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2466434
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2466435
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->a()V

    .line 2466436
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2466450
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466451
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->a()V

    .line 2466452
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2466453
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466454
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->a()V

    .line 2466455
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2466437
    const-class v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2466438
    const v0, 0x7f030e15

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2466439
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 2466440
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->b:LX/FQY;

    invoke-virtual {v0, p1, p2}, LX/FQY;->a(J)Landroid/content/Intent;

    move-result-object v0

    .line 2466441
    if-eqz v0, :cond_0

    .line 2466442
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->setVisibility(I)V

    .line 2466443
    :goto_0
    return-void

    .line 2466444
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->setVisibility(I)V

    .line 2466445
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2466446
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/1oW;->a(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "market://details?id=com.facebook.pages.app"

    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2466447
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2466448
    new-instance v0, LX/HS2;

    invoke-direct {v0, p0, v1}, LX/HS2;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2466449
    :cond_1
    const-string v0, "https://play.google.com/store/apps/details?id=com.facebook.pages.app"

    goto :goto_1
.end method

.method private a(Lcom/facebook/content/SecureContextHelper;LX/FQY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2466427
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2466428
    iput-object p2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->b:LX/FQY;

    .line 2466429
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/FQY;->a(LX/0QB;)LX/FQY;

    move-result-object v1

    check-cast v1, LX/FQY;

    invoke-direct {p0, v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->a(Lcom/facebook/content/SecureContextHelper;LX/FQY;)V

    return-void
.end method


# virtual methods
.method public final a(JLX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2466430
    invoke-static {p3}, LX/FQY;->a(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2466431
    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->a(J)V

    .line 2466432
    :goto_0
    return-void

    .line 2466433
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageAdminUpsellCardView;->setVisibility(I)V

    goto :goto_0
.end method
