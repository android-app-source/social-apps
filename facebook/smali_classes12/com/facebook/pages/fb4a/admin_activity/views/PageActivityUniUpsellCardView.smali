.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/HRn;


# instance fields
.field private a:Lcom/facebook/resources/ui/FbButton;

.field public b:LX/9XE;

.field public c:LX/GMm;

.field private d:LX/2Pb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2466251
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2466252
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->a()V

    .line 2466253
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2466254
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466255
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->a()V

    .line 2466256
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2466257
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466258
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->a()V

    .line 2466259
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2466260
    const v0, 0x7f030e07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2466261
    const-class v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2466262
    const v0, 0x7f0d2245

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->a:Lcom/facebook/resources/ui/FbButton;

    .line 2466263
    const v0, 0x7f0d2242

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2466264
    const v1, 0x7f021381

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2466265
    const v0, 0x7f0d2243

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2466266
    const v1, 0x7f08163d

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2466267
    const v0, 0x7f0d2244

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2466268
    const v1, 0x7f08163e

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2466269
    return-void
.end method

.method private a(LX/9XE;LX/GMm;LX/2Pb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2466270
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->b:LX/9XE;

    .line 2466271
    iput-object p2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->c:LX/GMm;

    .line 2466272
    iput-object p3, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->d:LX/2Pb;

    .line 2466273
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;

    invoke-static {v2}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v0

    check-cast v0, LX/9XE;

    invoke-static {v2}, LX/GMm;->a(LX/0QB;)LX/GMm;

    move-result-object v1

    check-cast v1, LX/GMm;

    invoke-static {v2}, LX/2Pb;->a(LX/0QB;)LX/2Pb;

    move-result-object v2

    check-cast v2, LX/2Pb;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->a(LX/9XE;LX/GMm;LX/2Pb;)V

    return-void
.end method


# virtual methods
.method public final a(JLX/15i;ILX/0am;)V
    .locals 9
    .param p3    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/15i;",
            "I",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x4

    .line 2466274
    if-eqz p4, :cond_0

    const/4 v1, 0x6

    invoke-virtual {p3, p4, v1}, LX/15i;->h(II)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2466275
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->setVisibility(I)V

    .line 2466276
    :goto_0
    return-void

    .line 2466277
    :cond_1
    invoke-virtual {p3, p4, v2}, LX/15i;->g(II)I

    move-result v1

    if-eqz v1, :cond_2

    .line 2466278
    invoke-virtual {p3, p4, v2}, LX/15i;->g(II)I

    move-result v1

    .line 2466279
    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    invoke-virtual {p3, v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    .line 2466280
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    if-ne v0, v1, :cond_4

    :cond_3
    sget-object v0, LX/8wW;->EVENT_RENDER_FAIL_ENTRY_POINT:LX/8wW;

    .line 2466281
    :goto_1
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->d:LX/2Pb;

    sget-object v2, LX/8wV;->PROMOTE_PAGE_MOBILE_MODULE:LX/8wV;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "pages_manager_activity_tab"

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2Pb;->a(LX/8wV;LX/8wW;Ljava/lang/String;Ljava/lang/String;)V

    .line 2466282
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->a:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x5

    invoke-virtual {p3, p4, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2466283
    iget-object v7, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->a:Lcom/facebook/resources/ui/FbButton;

    new-instance v0, LX/HS0;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/HS0;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;JLX/15i;ILX/0am;)V

    invoke-virtual {v7, v0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2466284
    :cond_4
    sget-object v0, LX/8wW;->EVENT_RENDER_CREATE_ENTRY_POINT:LX/8wW;

    goto :goto_1
.end method
