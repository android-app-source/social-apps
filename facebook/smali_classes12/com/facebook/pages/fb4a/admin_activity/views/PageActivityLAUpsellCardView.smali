.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbButton;

.field public b:LX/17Y;

.field public c:Lcom/facebook/content/SecureContextHelper;

.field private d:LX/2Pb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2466088
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2466089
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->a()V

    .line 2466090
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2466121
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466122
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->a()V

    .line 2466123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2466118
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466119
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->a()V

    .line 2466120
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2466108
    const v0, 0x7f030e07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2466109
    const-class v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2466110
    const v0, 0x7f0d2242

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2466111
    const v1, 0x7f021380

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2466112
    const v0, 0x7f0d2243

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2466113
    const v1, 0x7f08163f

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2466114
    const v0, 0x7f0d2244

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2466115
    const v1, 0x7f081640

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2466116
    const v0, 0x7f0d2245

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->a:Lcom/facebook/resources/ui/FbButton;

    .line 2466117
    return-void
.end method

.method private a(LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/2Pb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2466104
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->b:LX/17Y;

    .line 2466105
    iput-object p2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2466106
    iput-object p3, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->d:LX/2Pb;

    .line 2466107
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;

    invoke-static {v2}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, LX/2Pb;->a(LX/0QB;)LX/2Pb;

    move-result-object v2

    check-cast v2, LX/2Pb;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->a(LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/2Pb;)V

    return-void
.end method

.method private b(JLcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;)V
    .locals 5
    .param p3    # Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2466096
    if-nez p3, :cond_0

    .line 2466097
    :goto_0
    return-void

    .line 2466098
    :cond_0
    :try_start_0
    sget-object v0, LX/HRx;->a:[I

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2466099
    sget-object v0, LX/8wW;->EVENT_RENDER_EDIT_ENTRY_POINT:LX/8wW;

    .line 2466100
    :goto_1
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->d:LX/2Pb;

    sget-object v2, LX/8wV;->BOOSTED_EVENT_MOBILE_MODULE:LX/8wV;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "pages_manager_activity_tab"

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2Pb;->a(LX/8wV;LX/8wW;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2466101
    :catch_0
    goto :goto_0

    .line 2466102
    :pswitch_0
    sget-object v0, LX/8wW;->EVENT_RENDER_FAIL_ENTRY_POINT:LX/8wW;

    goto :goto_1

    .line 2466103
    :pswitch_1
    sget-object v0, LX/8wW;->EVENT_RENDER_CREATE_ENTRY_POINT:LX/8wW;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(JLcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;)V
    .locals 3
    .param p3    # Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2466093
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->b(JLcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;)V

    .line 2466094
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->a:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/HRw;

    invoke-direct {v1, p0, p1, p2}, LX/HRw;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;J)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2466095
    return-void
.end method

.method public setPromoteLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2466091
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityLAUpsellCardView;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2466092
    return-void
.end method
