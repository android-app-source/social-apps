.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2466124
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2466130
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2466132
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466133
    const v0, 0x7f030e0d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2466134
    const v0, 0x7f0d037b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;->a:Landroid/widget/TextView;

    .line 2466135
    const v0, 0x7f0d225e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;->b:Landroid/widget/TextView;

    .line 2466136
    return-void
.end method


# virtual methods
.method public setName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2466128
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2466129
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2466126
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardRow;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2466127
    return-void
.end method
