.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;
.super LX/HS4;
.source ""


# instance fields
.field public h:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2U1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/HRh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/3iH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2466566
    invoke-direct {p0, p1}, LX/HS4;-><init>(Landroid/content/Context;)V

    .line 2466567
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->a()V

    .line 2466568
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2466592
    invoke-direct {p0, p1, p2}, LX/HS4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466593
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->a()V

    .line 2466594
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2466589
    invoke-direct {p0, p1, p2, p3}, LX/HS4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466590
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->a()V

    .line 2466591
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2466587
    const-class v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2466588
    return-void
.end method

.method private static a(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;LX/0Uh;LX/9XE;LX/2U1;LX/HRh;LX/3iH;)V
    .locals 0

    .prologue
    .line 2466586
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->h:LX/0Uh;

    iput-object p2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->i:LX/9XE;

    iput-object p3, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->j:LX/2U1;

    iput-object p4, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->k:LX/HRh;

    iput-object p5, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->l:LX/3iH;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;

    invoke-static {v5}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {v5}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v2

    check-cast v2, LX/9XE;

    invoke-static {v5}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v3

    check-cast v3, LX/2U1;

    invoke-static {v5}, LX/HRh;->a(LX/0QB;)LX/HRh;

    move-result-object v4

    check-cast v4, LX/HRh;

    invoke-static {v5}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v5

    check-cast v5, LX/3iH;

    invoke-static/range {v0 .. v5}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->a(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;LX/0Uh;LX/9XE;LX/2U1;LX/HRh;LX/3iH;)V

    return-void
.end method

.method public static b(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;J)Landroid/content/Intent;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2466571
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2466572
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->j:LX/2U1;

    invoke-virtual {v0, v2}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 2466573
    if-eqz v0, :cond_0

    .line 2466574
    iget-object v3, v0, LX/8Dk;->b:LX/0am;

    move-object v0, v3

    .line 2466575
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2466576
    :goto_0
    return-object v0

    .line 2466577
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->j:LX/2U1;

    invoke-virtual {v0, v2}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 2466578
    iget-object v3, v0, LX/8Dk;->b:LX/0am;

    move-object v0, v3

    .line 2466579
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2466580
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->l:LX/3iH;

    invoke-virtual {v3, v2, v0}, LX/3iH;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 2466581
    const-string v0, "page/%s/notifications"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2466582
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->k:LX/HRh;

    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2466583
    if-nez v0, :cond_2

    move-object v0, v1

    .line 2466584
    goto :goto_0

    .line 2466585
    :cond_2
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;JLX/0am;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2466569
    new-instance v0, LX/HS7;

    move-object v1, p0

    move-wide v2, p2

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/HS7;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;JLjava/lang/String;LX/0am;)V

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityNotificationsLinkView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2466570
    return-void
.end method
