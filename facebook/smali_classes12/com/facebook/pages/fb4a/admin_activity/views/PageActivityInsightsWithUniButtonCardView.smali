.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/HRn;


# instance fields
.field public a:Landroid/widget/LinearLayout$LayoutParams;

.field private final b:LX/HRm;

.field private final c:Landroid/webkit/WebChromeClient;

.field private d:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;

.field private e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;

.field private f:Lcom/facebook/webview/BasicWebView;

.field private g:Lcom/facebook/webview/BasicWebView;

.field private h:Landroid/widget/TextView;

.field public i:LX/9XE;

.field public j:LX/GMm;

.field private k:LX/48V;

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0WJ;

.field private n:LX/0lC;

.field private o:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private p:LX/2Pb;

.field private q:J

.field private r:LX/HRv;

.field private s:LX/HRv;

.field private t:Z

.field private u:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2466007
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2466008
    new-instance v0, LX/HRp;

    invoke-direct {v0, p0}, LX/HRp;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->b:LX/HRm;

    .line 2466009
    new-instance v0, LX/HRq;

    invoke-direct {v0, p0}, LX/HRq;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->c:Landroid/webkit/WebChromeClient;

    .line 2466010
    new-instance v0, LX/HRv;

    invoke-direct {v0, p0}, LX/HRv;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->r:LX/HRv;

    .line 2466011
    new-instance v0, LX/HRv;

    invoke-direct {v0, p0}, LX/HRv;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->s:LX/HRv;

    .line 2466012
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a:Landroid/widget/LinearLayout$LayoutParams;

    .line 2466013
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->u:Z

    .line 2466014
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a()V

    .line 2466015
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2466016
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466017
    new-instance v0, LX/HRp;

    invoke-direct {v0, p0}, LX/HRp;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->b:LX/HRm;

    .line 2466018
    new-instance v0, LX/HRq;

    invoke-direct {v0, p0}, LX/HRq;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->c:Landroid/webkit/WebChromeClient;

    .line 2466019
    new-instance v0, LX/HRv;

    invoke-direct {v0, p0}, LX/HRv;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->r:LX/HRv;

    .line 2466020
    new-instance v0, LX/HRv;

    invoke-direct {v0, p0}, LX/HRv;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->s:LX/HRv;

    .line 2466021
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a:Landroid/widget/LinearLayout$LayoutParams;

    .line 2466022
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->u:Z

    .line 2466023
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a()V

    .line 2466024
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2466025
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466026
    new-instance v0, LX/HRp;

    invoke-direct {v0, p0}, LX/HRp;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->b:LX/HRm;

    .line 2466027
    new-instance v0, LX/HRq;

    invoke-direct {v0, p0}, LX/HRq;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->c:Landroid/webkit/WebChromeClient;

    .line 2466028
    new-instance v0, LX/HRv;

    invoke-direct {v0, p0}, LX/HRv;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->r:LX/HRv;

    .line 2466029
    new-instance v0, LX/HRv;

    invoke-direct {v0, p0}, LX/HRv;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->s:LX/HRv;

    .line 2466030
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a:Landroid/widget/LinearLayout$LayoutParams;

    .line 2466031
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->u:Z

    .line 2466032
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a()V

    .line 2466033
    return-void
.end method

.method private a(I)I
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedMethod"
        }
    .end annotation

    .prologue
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    .line 2466034
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 2466035
    const/high16 v1, 0x43340000    # 180.0f

    mul-float/2addr v0, v1

    .line 2466036
    int-to-float v1, p1

    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->f:Lcom/facebook/webview/BasicWebView;

    invoke-virtual {v2}, Lcom/facebook/webview/BasicWebView;->getScale()F

    move-result v2

    mul-float/2addr v1, v2

    .line 2466037
    float-to-double v2, v1

    float-to-double v4, v0

    const-wide/high16 v6, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v4, v6

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    float-to-double v2, v1

    float-to-double v4, v0

    const-wide/high16 v6, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v4, v6

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 2466038
    float-to-double v0, v1

    add-double/2addr v0, v8

    double-to-int v0, v0

    .line 2466039
    :goto_0
    return v0

    :cond_0
    float-to-double v0, v0

    add-double/2addr v0, v8

    double-to-int v0, v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;I)I
    .locals 1

    .prologue
    .line 2466040
    invoke-direct {p0, p1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a(I)I

    move-result v0

    return v0
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2466041
    const v0, 0x7f030e0a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2466042
    const-class v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2466043
    const v0, 0x7f0d224d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;

    .line 2466044
    const v0, 0x7f0d224e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;

    .line 2466045
    const v0, 0x7f0d224f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/webview/BasicWebView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->f:Lcom/facebook/webview/BasicWebView;

    .line 2466046
    const v0, 0x7f0d2250

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/webview/BasicWebView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->g:Lcom/facebook/webview/BasicWebView;

    .line 2466047
    const v0, 0x7f0d2245

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->h:Landroid/widget/TextView;

    .line 2466048
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->m:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 2466049
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v0, v2

    .line 2466050
    if-eqz v0, :cond_2

    .line 2466051
    iget-object v2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->n:LX/0lC;

    invoke-static {v2, v0}, Lcom/facebook/auth/credentials/SessionCookie;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 2466052
    if-eqz v2, :cond_2

    .line 2466053
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://%s/"

    .line 2466054
    :goto_0
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    sget-object v4, LX/0dU;->r:LX/0Tn;

    aput-object v4, v3, v1

    invoke-static {v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2466055
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v4

    .line 2466056
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    :goto_1
    if-ge v1, v5, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/SessionCookie;

    .line 2466057
    invoke-virtual {v0}, Lcom/facebook/auth/credentials/SessionCookie;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2466058
    invoke-virtual {v4, v3, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    .line 2466059
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2466060
    :cond_0
    const-string v0, "http://%s/"

    goto :goto_0

    .line 2466061
    :cond_1
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 2466062
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->f:Lcom/facebook/webview/BasicWebView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->r:LX/HRv;

    invoke-direct {p0, v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a(Lcom/facebook/webview/BasicWebView;LX/HRv;)V

    .line 2466063
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->g:Lcom/facebook/webview/BasicWebView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->s:LX/HRv;

    invoke-direct {p0, v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a(Lcom/facebook/webview/BasicWebView;LX/HRv;)V

    .line 2466064
    return-void
.end method

.method private a(LX/9XE;LX/48V;LX/0Ot;LX/0WJ;LX/0lC;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/GMm;LX/2Pb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/9XE;",
            "LX/48V;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/auth/datastore/AuthDataStore;",
            "LX/0lC;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/GMm;",
            "LX/2Pb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2466065
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->i:LX/9XE;

    .line 2466066
    iput-object p2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->k:LX/48V;

    .line 2466067
    iput-object p3, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->l:LX/0Ot;

    .line 2466068
    iput-object p4, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->m:LX/0WJ;

    .line 2466069
    iput-object p5, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->n:LX/0lC;

    .line 2466070
    iput-object p6, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2466071
    iput-object p7, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->j:LX/GMm;

    .line 2466072
    iput-object p8, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->p:LX/2Pb;

    .line 2466073
    return-void
.end method

.method private a(Lcom/facebook/webview/BasicWebView;LX/HRv;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled",
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2466074
    invoke-virtual {p1}, Lcom/facebook/webview/BasicWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 2466075
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->c:Landroid/webkit/WebChromeClient;

    invoke-virtual {p1, v0}, Lcom/facebook/webview/BasicWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 2466076
    invoke-virtual {p1, v2}, Lcom/facebook/webview/BasicWebView;->setHorizontalScrollBarEnabled(Z)V

    .line 2466077
    invoke-virtual {p1, v2}, Lcom/facebook/webview/BasicWebView;->setVerticalScrollBarEnabled(Z)V

    .line 2466078
    new-instance v0, LX/HRu;

    invoke-direct {v0, p0, p2}, LX/HRu;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;LX/HRv;)V

    const-string v1, "JSBridge"

    invoke-virtual {p1, v0, v1}, Lcom/facebook/webview/BasicWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2466079
    new-instance v0, LX/HRr;

    invoke-direct {v0, p0, p2}, LX/HRr;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;LX/HRv;)V

    invoke-virtual {p1, v0}, Lcom/facebook/webview/BasicWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2466080
    new-instance v0, LX/HRs;

    invoke-direct {v0, p0}, LX/HRs;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V

    invoke-virtual {p1, v0}, Lcom/facebook/webview/BasicWebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2466081
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;

    invoke-static {v8}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v1

    check-cast v1, LX/9XE;

    invoke-static {v8}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object v2

    check-cast v2, LX/48V;

    const/16 v3, 0x259

    invoke-static {v8, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v8}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    invoke-static {v8}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v5

    check-cast v5, LX/0lC;

    invoke-static {v8}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v8}, LX/GMm;->a(LX/0QB;)LX/GMm;

    move-result-object v7

    check-cast v7, LX/GMm;

    invoke-static {v8}, LX/2Pb;->a(LX/0QB;)LX/2Pb;

    move-result-object v8

    check-cast v8, LX/2Pb;

    invoke-direct/range {v0 .. v8}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a(LX/9XE;LX/48V;LX/0Ot;LX/0WJ;LX/0lC;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/GMm;LX/2Pb;)V

    return-void
.end method

.method private a(LX/15i;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2466002
    if-nez p2, :cond_0

    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    .line 2466003
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->setVisibility(I)V

    .line 2466004
    :goto_1
    return v0

    .line 2466005
    :cond_0
    const/16 v2, 0x9

    invoke-virtual {p1, p2, v2}, LX/15i;->g(II)I

    move-result v2

    if-nez v2, :cond_1

    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2466006
    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;LX/HRl;)V
    .locals 1

    .prologue
    .line 2465999
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->setSwitcherState(LX/HRl;)V

    .line 2466000
    invoke-direct {p0, p1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->b(LX/HRl;)V

    .line 2466001
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2465996
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->k:LX/48V;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->f:Lcom/facebook/webview/BasicWebView;

    const-string v2, "https://m.facebook.com/pages/insights/chart/?page_id=%s&chart=weeklylike"

    new-array v3, v7, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->q:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2465997
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->k:LX/48V;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->g:Lcom/facebook/webview/BasicWebView;

    const-string v2, "https://m.facebook.com/pages/insights/chart/?page_id=%s&chart=weeklypostreach"

    new-array v3, v7, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->q:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2465998
    return-void
.end method

.method private b(LX/HRl;)V
    .locals 2

    .prologue
    .line 2465990
    sget-object v0, LX/HRl;->WEEKLY_LIKE:LX/HRl;

    if-ne p1, v0, :cond_0

    .line 2465991
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->g:Lcom/facebook/webview/BasicWebView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/facebook/webview/BasicWebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2465992
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->f:Lcom/facebook/webview/BasicWebView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->r:LX/HRv;

    iget-object v1, v1, LX/HRv;->c:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/facebook/webview/BasicWebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2465993
    :goto_0
    return-void

    .line 2465994
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->f:Lcom/facebook/webview/BasicWebView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/facebook/webview/BasicWebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2465995
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->g:Lcom/facebook/webview/BasicWebView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->s:LX/HRv;

    iget-object v1, v1, LX/HRv;->c:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/facebook/webview/BasicWebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private c(JLX/15i;ILX/0am;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/15i;",
            "I",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2465984
    const/4 v0, 0x6

    invoke-virtual {p3, p4, v0}, LX/15i;->h(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2465985
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2465986
    :goto_0
    return-void

    .line 2465987
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2465988
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->h:Landroid/widget/TextView;

    const/4 v1, 0x5

    invoke-virtual {p3, p4, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2465989
    iget-object v7, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->h:Landroid/widget/TextView;

    new-instance v0, LX/HRt;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/HRt;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;JLX/15i;ILX/0am;)V

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 2465983
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dU;->j:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method

.method public static f(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;)V
    .locals 2

    .prologue
    .line 2465972
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->r:LX/HRv;

    iget-boolean v0, v0, LX/HRv;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->s:LX/HRv;

    iget-boolean v0, v0, LX/HRv;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->t:Z

    if-nez v0, :cond_0

    .line 2465973
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->t:Z

    .line 2465974
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->b:LX/HRm;

    .line 2465975
    iput-object v1, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->c:LX/HRm;

    .line 2465976
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSwitcherIndicator;->setVisibility(I)V

    .line 2465977
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->u:Z

    if-eqz v0, :cond_1

    .line 2465978
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;

    sget-object v1, LX/HRl;->WEEKLY_LIKE:LX/HRl;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->setSwitcherState(LX/HRl;)V

    .line 2465979
    :cond_0
    :goto_0
    return-void

    .line 2465980
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;

    .line 2465981
    iget-object v1, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->d:LX/HRl;

    move-object v0, v1

    .line 2465982
    invoke-direct {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->b(LX/HRl;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(JLX/15i;ILX/0am;)V
    .locals 5
    .param p3    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/15i;",
            "I",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x4

    .line 2465960
    invoke-direct {p0, p3, p4}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a(LX/15i;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2465961
    :goto_0
    return-void

    .line 2465962
    :cond_0
    invoke-virtual {p3, p4, v2}, LX/15i;->g(II)I

    move-result v1

    if-eqz v1, :cond_1

    .line 2465963
    invoke-virtual {p3, p4, v2}, LX/15i;->g(II)I

    move-result v1

    .line 2465964
    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    invoke-virtual {p3, v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    .line 2465965
    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedActionStatus;

    if-ne v0, v1, :cond_3

    :cond_2
    sget-object v0, LX/8wW;->EVENT_RENDER_FAIL_ENTRY_POINT:LX/8wW;

    .line 2465966
    :goto_1
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->p:LX/2Pb;

    sget-object v2, LX/8wV;->PROMOTE_PAGE_MOBILE_MODULE:LX/8wV;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "pages_manager_activity_tab"

    invoke-virtual {v1, v2, v0, v3, v4}, LX/2Pb;->a(LX/8wV;LX/8wW;Ljava/lang/String;Ljava/lang/String;)V

    .line 2465967
    iput-wide p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->q:J

    .line 2465968
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;

    const/16 v1, 0x9

    invoke-virtual {p3, p4, v1}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v0, p3, v1, p1, p2}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->a(LX/15i;IJ)V

    .line 2465969
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->b()V

    .line 2465970
    invoke-direct/range {p0 .. p5}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->c(JLX/15i;ILX/0am;)V

    goto :goto_0

    .line 2465971
    :cond_3
    sget-object v0, LX/8wW;->EVENT_RENDER_CREATE_ENTRY_POINT:LX/8wW;

    goto :goto_1
.end method

.method public final b(JLX/15i;ILX/0am;)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModelWithoutReloadInsightsGraphs"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/15i;",
            "I",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2465956
    invoke-direct {p0, p3, p4}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->a(LX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2465957
    :goto_0
    return-void

    .line 2465958
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;

    const/16 v1, 0x9

    invoke-virtual {p3, p4, v1}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v0, p3, v1, p1, p2}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsSummarySwitcher;->a(LX/15i;IJ)V

    .line 2465959
    invoke-direct/range {p0 .. p5}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->c(JLX/15i;ILX/0am;)V

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2465950
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->r:LX/HRv;

    iput-boolean v1, v0, LX/HRv;->a:Z

    .line 2465951
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->s:LX/HRv;

    iput-boolean v1, v0, LX/HRv;->a:Z

    .line 2465952
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->t:Z

    .line 2465953
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->u:Z

    .line 2465954
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityInsightsWithUniButtonCardView;->b()V

    .line 2465955
    return-void
.end method
