.class public Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;
.super LX/HS4;
.source ""


# instance fields
.field public h:LX/E1d;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2466602
    invoke-direct {p0, p1}, LX/HS4;-><init>(Landroid/content/Context;)V

    .line 2466603
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;->a()V

    .line 2466604
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2466618
    invoke-direct {p0, p1, p2}, LX/HS4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466619
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;->a()V

    .line 2466620
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2466615
    invoke-direct {p0, p1, p2, p3}, LX/HS4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466616
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;->a()V

    .line 2466617
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;J)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 2466608
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2466609
    const-string v1, "reaction_session_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2466610
    const-string v1, "page_context_item_type"

    const v2, 0x7f081835

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2466611
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2466612
    const-string v1, "empty_view"

    const v2, 0x7f030e61

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2466613
    const-string v1, "source_name"

    const v2, 0x7f081835

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2466614
    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2466621
    const-class v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2466622
    return-void
.end method

.method private static a(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;LX/E1d;LX/17W;)V
    .locals 0

    .prologue
    .line 2466607
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;->h:LX/E1d;

    iput-object p2, p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;->i:LX/17W;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;

    invoke-static {v1}, LX/E1d;->b(LX/0QB;)LX/E1d;

    move-result-object v0

    check-cast v0, LX/E1d;

    invoke-static {v1}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;->a(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;LX/E1d;LX/17W;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;JLX/0am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "LX/0am",
            "<+",
            "Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityLinkView$ViewLaunchedListener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2466605
    new-instance v0, LX/HS8;

    invoke-direct {v0, p0, p2, p3}, LX/HS8;-><init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;J)V

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2466606
    return-void
.end method
