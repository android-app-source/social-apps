.class public final Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2467609
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2467610
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2467607
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2467608
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2467589
    if-nez p1, :cond_0

    .line 2467590
    :goto_0
    return v0

    .line 2467591
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2467592
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2467593
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2467594
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2467595
    invoke-virtual {p0, p1, v3}, LX/15i;->b(II)Z

    move-result v2

    .line 2467596
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2467597
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2467598
    invoke-virtual {p3, v3, v2}, LX/186;->a(IZ)V

    .line 2467599
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2467600
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2467601
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2467602
    invoke-virtual {p0, p1, v3}, LX/15i;->b(II)Z

    move-result v2

    .line 2467603
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2467604
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2467605
    invoke-virtual {p3, v3, v2}, LX/186;->a(IZ)V

    .line 2467606
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x58913c7b -> :sswitch_0
        0x72d35b49 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2467588
    new-instance v0, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2467585
    sparse-switch p0, :sswitch_data_0

    .line 2467586
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2467587
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x58913c7b -> :sswitch_0
        0x72d35b49 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2467584
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2467582
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;->b(I)V

    .line 2467583
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2467577
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2467578
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2467579
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2467580
    iput p2, p0, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;->b:I

    .line 2467581
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2467551
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2467576
    new-instance v0, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2467573
    iget v0, p0, LX/1vt;->c:I

    .line 2467574
    move v0, v0

    .line 2467575
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2467570
    iget v0, p0, LX/1vt;->c:I

    .line 2467571
    move v0, v0

    .line 2467572
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2467567
    iget v0, p0, LX/1vt;->b:I

    .line 2467568
    move v0, v0

    .line 2467569
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2467564
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2467565
    move-object v0, v0

    .line 2467566
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2467555
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2467556
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2467557
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2467558
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2467559
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2467560
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2467561
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2467562
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2467563
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2467552
    iget v0, p0, LX/1vt;->c:I

    .line 2467553
    move v0, v0

    .line 2467554
    return v0
.end method
