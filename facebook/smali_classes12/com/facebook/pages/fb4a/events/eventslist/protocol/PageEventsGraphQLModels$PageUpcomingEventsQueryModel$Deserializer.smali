.class public final Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2467740
    const-class v0, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;

    new-instance v1, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2467741
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2467742
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2467743
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2467744
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2467745
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_c

    .line 2467746
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2467747
    :goto_0
    move v1, v2

    .line 2467748
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2467749
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2467750
    new-instance v1, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;-><init>()V

    .line 2467751
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2467752
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2467753
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2467754
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2467755
    :cond_0
    return-object v1

    .line 2467756
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_8

    .line 2467757
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 2467758
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2467759
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v12, :cond_1

    .line 2467760
    const-string p0, "can_viewer_follow"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 2467761
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v5

    move v11, v5

    move v5, v3

    goto :goto_1

    .line 2467762
    :cond_2
    const-string p0, "events_calendar_can_viewer_subscribe"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2467763
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v10, v4

    move v4, v3

    goto :goto_1

    .line 2467764
    :cond_3
    const-string p0, "events_calendar_subscriber_count"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2467765
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v9, v1

    move v1, v3

    goto :goto_1

    .line 2467766
    :cond_4
    const-string p0, "events_calendar_subscription_status"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2467767
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_1

    .line 2467768
    :cond_5
    const-string p0, "owned_events"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2467769
    invoke-static {p1, v0}, LX/HSu;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2467770
    :cond_6
    const-string p0, "viewer_profile_permissions"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2467771
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2467772
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2467773
    :cond_8
    const/4 v12, 0x6

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 2467774
    if-eqz v5, :cond_9

    .line 2467775
    invoke-virtual {v0, v2, v11}, LX/186;->a(IZ)V

    .line 2467776
    :cond_9
    if-eqz v4, :cond_a

    .line 2467777
    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 2467778
    :cond_a
    if-eqz v1, :cond_b

    .line 2467779
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v9, v2}, LX/186;->a(III)V

    .line 2467780
    :cond_b
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2467781
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2467782
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2467783
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    goto/16 :goto_1
.end method
