.class public Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/GZf;


# static fields
.field public static final V:[I


# instance fields
.field public A:Z

.field private B:Landroid/view/View;

.field private C:Lcom/facebook/resources/ui/FbTextView;

.field private D:Lcom/facebook/resources/ui/FbButton;

.field private E:Landroid/content/res/Resources;

.field private F:Landroid/widget/LinearLayout;

.field public G:Lcom/facebook/widget/listview/BetterListView;

.field private H:Landroid/text/style/MetricAffectingSpan;

.field private I:Landroid/text/style/MetricAffectingSpan;

.field public J:Lcom/facebook/widget/listview/EmptyListViewItem;

.field private K:Landroid/widget/ImageView;

.field public L:Landroid/text/style/MetricAffectingSpan;

.field public M:Landroid/text/style/MetricAffectingSpan;

.field private N:LX/E8s;

.field private O:LX/E8t;

.field public P:Landroid/view/View;

.field public Q:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field private R:Z

.field public S:I

.field private T:I

.field private U:I

.field public W:Z

.field public X:I

.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HSN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HSk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/CXj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/7va;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/Bl6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final l:LX/HSV;

.field private final m:LX/HSZ;

.field private final n:LX/HSW;

.field public o:Ljava/lang/String;

.field public p:Z

.field private q:Z

.field public r:J

.field private s:Ljava/lang/String;

.field public t:Z

.field public u:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

.field public v:I

.field public w:LX/HSX;

.field public x:LX/HSM;

.field public y:Lcom/facebook/events/common/EventAnalyticsParams;

.field public z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2467279
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->V:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2467267
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2467268
    new-instance v0, LX/HSV;

    invoke-direct {v0, p0}, LX/HSV;-><init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->l:LX/HSV;

    .line 2467269
    new-instance v0, LX/HSZ;

    invoke-direct {v0, p0}, LX/HSZ;-><init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->m:LX/HSZ;

    .line 2467270
    new-instance v0, LX/HSW;

    invoke-direct {v0, p0}, LX/HSW;-><init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->n:LX/HSW;

    .line 2467271
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->p:Z

    .line 2467272
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->q:Z

    .line 2467273
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->R:Z

    .line 2467274
    iput v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->U:I

    .line 2467275
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->W:Z

    .line 2467276
    iput v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->X:I

    .line 2467277
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/16 v2, 0x11

    .line 2467280
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2467281
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2467282
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->H:Landroid/text/style/MetricAffectingSpan;

    invoke-static {v0, p1, v1, v2}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)V

    .line 2467283
    :cond_0
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2467284
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 2467285
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2467286
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->I:Landroid/text/style/MetricAffectingSpan;

    invoke-static {v0, p2, v1, v2}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)V

    .line 2467287
    :cond_2
    return-object v0
.end method

.method public static a(JLjava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;
    .locals 4
    .param p3    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;"
        }
    .end annotation

    .prologue
    .line 2467288
    new-instance v0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    invoke-direct {v0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;-><init>()V

    .line 2467289
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2467290
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2467291
    const-string v2, "profile_name"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2467292
    const-string v2, "event_id"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2467293
    const-string v2, "extra_ref_module"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2467294
    const-string v2, "event_ref_mechanism"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2467295
    const-string v2, "extra_is_inside_page_surface_tab"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2467296
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2467297
    return-object v0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)V
    .locals 2

    .prologue
    .line 2467298
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 2467299
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2467300
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p0, p2, v0, v1, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2467301
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;Ljava/util/List;LX/HSK;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;",
            "LX/HSK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2467302
    new-instance v0, LX/HSS;

    invoke-direct {v0, p0}, LX/HSS;-><init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    invoke-static {p1, v0}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v0

    .line 2467303
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->x:LX/HSM;

    .line 2467304
    sget-object v2, LX/HSK;->NEW:LX/HSK;

    if-ne p2, v2, :cond_0

    iget-object v2, v1, LX/HSM;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_0

    .line 2467305
    iget-object v2, v1, LX/HSM;->k:Ljava/util/List;

    const/4 v3, 0x0

    sget-object p0, LX/HSK;->NEW:LX/HSK;

    invoke-interface {v2, v3, p0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2467306
    :cond_0
    iget-object v2, v1, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v2, p2}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2467307
    iget-object v2, v1, LX/HSM;->e:Ljava/util/EnumMap;

    new-instance v3, LX/HSJ;

    invoke-static {v1, p2}, LX/HSM;->a(LX/HSM;LX/HSK;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HSJ;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2467308
    :cond_1
    sget-object v2, LX/HSK;->UPCOMING:LX/HSK;

    invoke-virtual {p2, v2}, LX/HSK;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2467309
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/model/Event;

    .line 2467310
    iget-object v3, v1, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v3, p2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HSJ;

    iget-object v3, v3, LX/HSJ;->b:Ljava/util/List;

    .line 2467311
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2467312
    iget-object p1, v1, LX/HSM;->h:Ljava/util/HashMap;

    .line 2467313
    iget-object v0, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v0

    .line 2467314
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2467315
    :cond_2
    sget-object v2, LX/HSK;->NEW:LX/HSK;

    invoke-virtual {p2, v2}, LX/HSK;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2467316
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/model/Event;

    .line 2467317
    iget-object v3, v1, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v3, p2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HSJ;

    iget-object v3, v3, LX/HSJ;->b:Ljava/util/List;

    .line 2467318
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2467319
    iget-object p1, v1, LX/HSM;->i:Ljava/util/HashMap;

    .line 2467320
    iget-object v0, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v0

    .line 2467321
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2467322
    :cond_3
    iget-object v2, v1, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v2, p2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HSJ;

    iget-object v2, v2, LX/HSJ;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2467323
    :cond_4
    const v2, 0x2e33199a

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2467324
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;Z)V
    .locals 2

    .prologue
    .line 2467325
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b008e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2467326
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->d:LX/HSk;

    .line 2467327
    iput v0, v1, LX/HSk;->d:I

    .line 2467328
    sget-object v0, LX/HSX;->UPCOMING:LX/HSX;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->w:LX/HSX;

    invoke-virtual {v0, v1}, LX/HSX;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2467329
    invoke-direct {p0, p1}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->b(Z)V

    .line 2467330
    :goto_0
    return-void

    .line 2467331
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->k()V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 14

    .prologue
    .line 2467332
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->d:LX/HSk;

    iget-wide v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->r:J

    const/16 v4, 0xa

    iget-object v5, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->o:Ljava/lang/String;

    new-instance v6, LX/HSQ;

    invoke-direct {v6, p0, p1}, LX/HSQ;-><init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;Z)V

    .line 2467333
    new-instance v7, LX/HSc;

    move-object v8, v1

    move-wide v9, v2

    move v11, v4

    move-object v12, v5

    invoke-direct/range {v7 .. v12}, LX/HSc;-><init>(LX/HSk;JILjava/lang/String;)V

    .line 2467334
    new-instance v8, LX/HSd;

    invoke-direct {v8, v1, v6, v4}, LX/HSd;-><init>(LX/HSk;LX/HSQ;I)V

    .line 2467335
    iget-object v9, v1, LX/HSk;->b:LX/1Ck;

    const-string v10, "fetchEventsList"

    invoke-virtual {v9, v10, v7, v8}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2467336
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2467337
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->N:LX/E8s;

    if-nez v0, :cond_2

    .line 2467338
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->O:LX/E8t;

    if-nez v0, :cond_1

    .line 2467339
    new-instance v0, LX/E8s;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->N:LX/E8s;

    .line 2467340
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->N:LX/E8s;

    const/4 v1, 0x1

    .line 2467341
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2467342
    :cond_0
    :goto_1
    return-void

    .line 2467343
    :cond_1
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->O:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->N:LX/E8s;

    .line 2467344
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->O:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->N:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    goto :goto_0

    .line 2467345
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->O:LX/E8t;

    if-eqz v0, :cond_0

    .line 2467346
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->N:LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->O:LX/E8t;

    .line 2467347
    iput-object v1, v0, LX/E8s;->a:Landroid/view/View;

    .line 2467348
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->O:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->N:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2467349
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->N:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->requestLayout()V

    goto :goto_1
.end method

.method private d(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2467350
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E:Landroid/content/res/Resources;

    const v1, 0x7f0f015f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->e:LX/154;

    invoke-virtual {v4, p1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V
    .locals 8

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2467136
    const/4 v0, 0x0

    .line 2467137
    iget-boolean v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->t:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->b:LX/0Uh;

    const/16 v4, 0x3a5

    invoke-virtual {v2, v4, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 2467138
    if-eqz v0, :cond_1

    .line 2467139
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->C:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2467140
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2467141
    :goto_0
    return-void

    .line 2467142
    :cond_1
    new-instance v0, LX/0wM;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 2467143
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->u:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->u:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->NONE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2467144
    :cond_2
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E:Landroid/content/res/Resources;

    const v2, 0x7f0830f7

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->s:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2467145
    iget v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->v:I

    invoke-direct {p0, v1}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->d(I)Ljava/lang/String;

    move-result-object v1

    .line 2467146
    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    iget-object v4, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E:Landroid/content/res/Resources;

    const v5, 0x7f0830f8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2467147
    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    iget-object v4, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E:Landroid/content/res/Resources;

    const v5, 0x7f0a09f5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 2467148
    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    const v4, 0x7f02079c

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbButton;->setBackgroundResource(I)V

    .line 2467149
    const v3, 0x7f0213a7

    const v4, -0x808081

    invoke-virtual {v0, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2467150
    :goto_1
    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->C:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2467151
    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3, v6}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2467152
    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->C:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, v2, v1}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2467153
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v0, v7, v7, v7}, Lcom/facebook/resources/ui/FbButton;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2467154
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E:Landroid/content/res/Resources;

    const v1, 0x7f0830f9

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->s:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2467155
    invoke-direct {p0, v6}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->d(I)Ljava/lang/String;

    move-result-object v1

    .line 2467156
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E:Landroid/content/res/Resources;

    const v4, 0x7f0830fa

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2467157
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E:Landroid/content/res/Resources;

    const v4, 0x7f0a09f6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 2467158
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    const v3, 0x7f02079f

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setBackgroundResource(I)V

    .line 2467159
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E:Landroid/content/res/Resources;

    const v3, 0x7f0213a7

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1
.end method

.method private k()V
    .locals 14

    .prologue
    .line 2467351
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->d:LX/HSk;

    iget-wide v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->r:J

    const/16 v4, 0xa

    iget-object v5, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->o:Ljava/lang/String;

    new-instance v6, LX/HSR;

    invoke-direct {v6, p0}, LX/HSR;-><init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    .line 2467352
    new-instance v7, LX/HSe;

    move-object v8, v1

    move-wide v9, v2

    move v11, v4

    move-object v12, v5

    invoke-direct/range {v7 .. v12}, LX/HSe;-><init>(LX/HSk;JILjava/lang/String;)V

    .line 2467353
    new-instance v8, LX/HSf;

    invoke-direct {v8, v1, v6, v4}, LX/HSf;-><init>(LX/HSk;LX/HSR;I)V

    .line 2467354
    iget-object v9, v1, LX/HSk;->b:LX/1Ck;

    const-string v10, "fetchEventsList"

    invoke-virtual {v9, v10, v7, v8}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2467355
    return-void
.end method

.method public static l(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V
    .locals 6

    .prologue
    .line 2467356
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->d:LX/HSk;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->z:Ljava/util/List;

    new-instance v2, LX/HST;

    invoke-direct {v2, p0}, LX/HST;-><init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    .line 2467357
    new-instance v3, LX/HSg;

    invoke-direct {v3, v0, v1}, LX/HSg;-><init>(LX/HSk;Ljava/util/List;)V

    .line 2467358
    new-instance v4, LX/HSh;

    invoke-direct {v4, v0, v2}, LX/HSh;-><init>(LX/HSk;LX/HST;)V

    .line 2467359
    iget-object v5, v0, LX/HSk;->b:LX/1Ck;

    const-string p0, "fetchEventsList"

    invoke-virtual {v5, p0, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2467360
    return-void
.end method

.method public static m(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2467278
    iget-boolean v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->A:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->i:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/17g;->g:S

    invoke-interface {v1, v2, v3, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static o(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 2467102
    const/4 v0, 0x0

    .line 2467103
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->x:LX/HSM;

    invoke-virtual {v1}, LX/HSM;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->i:LX/0ad;

    sget-short v2, LX/17g;->e:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 2467104
    if-eqz v0, :cond_4

    .line 2467105
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->K:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2467106
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0830fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2467107
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0830ff

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2467108
    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->C:Lcom/facebook/resources/ui/FbTextView;

    const/16 v8, 0x11

    .line 2467109
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2467110
    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0e0ae2

    invoke-direct {v5, v6, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->L:Landroid/text/style/MetricAffectingSpan;

    .line 2467111
    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0e0ae3

    invoke-direct {v5, v6, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->M:Landroid/text/style/MetricAffectingSpan;

    .line 2467112
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2467113
    iget-object v5, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->L:Landroid/text/style/MetricAffectingSpan;

    invoke-static {v4, v0, v5, v8}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)V

    .line 2467114
    :cond_1
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2467115
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 2467116
    const-string v5, "\n"

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2467117
    :cond_2
    iget-object v5, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->M:Landroid/text/style/MetricAffectingSpan;

    invoke-static {v4, v1, v5, v8}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)V

    .line 2467118
    :cond_3
    move-object v0, v4

    .line 2467119
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2467120
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->C:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2467121
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0830fd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2467122
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/HSU;

    invoke-direct {v1, p0}, LX/HSU;-><init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2467123
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E:Landroid/content/res/Resources;

    const v2, 0x7f0a09f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 2467124
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f02079f

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setBackgroundResource(I)V

    .line 2467125
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2467126
    return-void

    .line 2467127
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->K:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2467128
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0830fc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2467129
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->C:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2467130
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->C:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2467131
    iget v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->U:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->U:I

    if-le p1, v0, :cond_1

    .line 2467132
    :cond_0
    :goto_0
    return-void

    .line 2467133
    :cond_1
    iput p1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->U:I

    .line 2467134
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->P:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2467135
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->P:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->U:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2467160
    const-string v0, "page_events_list"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2467161
    iput p1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->T:I

    .line 2467162
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    iget v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->T:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2467163
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2467164
    iput-object p1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->O:LX/E8t;

    .line 2467165
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->c()V

    .line 2467166
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2467167
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2467168
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v3

    check-cast v3, LX/0zG;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    const-class v5, LX/HSN;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/HSN;

    new-instance v9, LX/HSk;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v8

    check-cast v8, LX/1My;

    invoke-direct {v9, v6, v7, v8}, LX/HSk;-><init>(LX/0tX;LX/1Ck;LX/1My;)V

    move-object v6, v9

    check-cast v6, LX/HSk;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v7

    check-cast v7, LX/154;

    invoke-static {v0}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v8

    check-cast v8, LX/CXj;

    invoke-static {v0}, LX/7va;->b(LX/0QB;)LX/7va;

    move-result-object v9

    check-cast v9, LX/7va;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v10

    check-cast v10, LX/0hB;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p1

    check-cast p1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v0

    check-cast v0, LX/Bl6;

    iput-object v3, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a:LX/0zG;

    iput-object v4, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->b:LX/0Uh;

    iput-object v5, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->c:LX/HSN;

    iput-object v6, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->d:LX/HSk;

    iput-object v7, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->e:LX/154;

    iput-object v8, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->f:LX/CXj;

    iput-object v9, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->g:LX/7va;

    iput-object v10, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->h:LX/0hB;

    iput-object v11, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->i:LX/0ad;

    iput-object p1, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->j:Lcom/facebook/content/SecureContextHelper;

    iput-object v0, v2, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->k:LX/Bl6;

    .line 2467169
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2467170
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->r:J

    .line 2467171
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2467172
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->s:Ljava/lang/String;

    .line 2467173
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2467174
    const-string v1, "event_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->z:Ljava/util/List;

    .line 2467175
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E:Landroid/content/res/Resources;

    .line 2467176
    sget-object v0, LX/HSX;->UPCOMING:LX/HSX;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->w:LX/HSX;

    .line 2467177
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2467178
    const-string v1, "extra_ref_module"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2467179
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2467180
    const-string v2, "event_ref_mechanism"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2467181
    new-instance v3, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v4, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    move-object v5, v0

    move-object v6, v1

    invoke-direct/range {v3 .. v8}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 2467182
    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->y:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2467183
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2467184
    const-string v1, "extra_is_inside_page_surface_tab"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->R:Z

    .line 2467185
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->c:LX/HSN;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->y:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2467186
    new-instance v3, LX/HSM;

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {v3, v1, v2}, LX/HSM;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;)V

    .line 2467187
    move-object v0, v3

    .line 2467188
    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->x:LX/HSM;

    .line 2467189
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->x:LX/HSM;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->z:Ljava/util/List;

    .line 2467190
    if-eqz v1, :cond_0

    .line 2467191
    iget-object v2, v0, LX/HSM;->j:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2467192
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->d:LX/HSk;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->x:LX/HSM;

    .line 2467193
    iput-object v1, v0, LX/HSk;->e:LX/HSM;

    .line 2467194
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->k:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->l:LX/HSV;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2467195
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->k:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->m:LX/HSZ;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2467196
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->k:LX/Bl6;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->n:LX/HSW;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2467197
    return-void
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2467198
    iput-object p1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->Q:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2467199
    return-void
.end method

.method public final g()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2467200
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->x:LX/HSM;

    .line 2467201
    iget-object v2, v0, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v2}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HSK;

    .line 2467202
    iget-object v4, v0, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v4, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HSJ;

    iget-object v2, v2, LX/HSJ;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 2467203
    :cond_0
    iget-object v2, v0, LX/HSM;->h:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 2467204
    iget-object v2, v0, LX/HSM;->i:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 2467205
    sget-object v0, LX/HSX;->UPCOMING:LX/HSX;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->w:LX/HSX;

    .line 2467206
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->o:Ljava/lang/String;

    .line 2467207
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->p:Z

    .line 2467208
    invoke-static {p0, v1}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a$redex0(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;Z)V

    .line 2467209
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0xe67f516

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2467210
    const v1, 0x7f0306a2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x2d5884b7

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5e86dcdd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2467211
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->d:LX/HSk;

    .line 2467212
    iget-object v2, v1, LX/HSk;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2467213
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    .line 2467214
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->k:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->l:LX/HSV;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2467215
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->k:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->m:LX/HSZ;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2467216
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->k:LX/Bl6;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->n:LX/HSW;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2467217
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2467218
    const/16 v1, 0x2b

    const v2, -0x2c7d77bd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v0, 0x2a

    const v1, -0x2a67f8a4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2467219
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2467220
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->R:Z

    if-nez v0, :cond_0

    .line 2467221
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    const v2, 0x7f0830f6

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    .line 2467222
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->q:Z

    if-nez v0, :cond_1

    .line 2467223
    invoke-static {p0, v3}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a$redex0(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;Z)V

    .line 2467224
    iput-boolean v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->q:Z

    .line 2467225
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->J:Lcom/facebook/widget/listview/EmptyListViewItem;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->p:Z

    if-nez v0, :cond_2

    .line 2467226
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->J:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 2467227
    :cond_2
    const/16 v0, 0x2b

    const v2, 0x5477220d

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 2467228
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2467229
    const v0, 0x7f0d1226

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->F:Landroid/widget/LinearLayout;

    .line 2467230
    const v0, 0x7f0d1227

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    .line 2467231
    invoke-virtual {p0, p2}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0306a5

    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->B:Landroid/view/View;

    .line 2467232
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->R:Z

    if-nez v0, :cond_1

    .line 2467233
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->F:Landroid/widget/LinearLayout;

    const v2, 0x7f0a00d5

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2467234
    :goto_0
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->c()V

    .line 2467235
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->N:LX/E8s;

    if-eqz v0, :cond_3

    .line 2467236
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->N:LX/E8s;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 2467237
    const/4 v0, 0x2

    .line 2467238
    :goto_1
    new-instance v2, Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->J:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2467239
    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E:Landroid/content/res/Resources;

    const v3, 0x7f0b0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2467240
    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->J:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v3, v4, v2, v4, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->setPadding(IIII)V

    .line 2467241
    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->J:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v2, v4}, Lcom/facebook/widget/listview/EmptyListViewItem;->setBackgroundResource(I)V

    .line 2467242
    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->J:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v2, v4}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 2467243
    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->J:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2467244
    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->J:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;)V

    .line 2467245
    new-instance v2, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->P:Landroid/view/View;

    .line 2467246
    iget v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->U:I

    invoke-virtual {p0, v2}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->E_(I)V

    .line 2467247
    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->P:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;)V

    .line 2467248
    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->B:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;)V

    .line 2467249
    iget-object v2, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2467250
    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->x:LX/HSM;

    .line 2467251
    iput v0, v1, LX/HSM;->m:I

    .line 2467252
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->x:LX/HSM;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2467253
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/HSO;

    invoke-direct {v1, p0}, LX/HSO;-><init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2467254
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->B:Landroid/view/View;

    const v1, 0x7f0d122b

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->C:Lcom/facebook/resources/ui/FbTextView;

    .line 2467255
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->B:Landroid/view/View;

    const v1, 0x7f0d122c

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    .line 2467256
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->B:Landroid/view/View;

    const v1, 0x7f0d122a

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->K:Landroid/widget/ImageView;

    .line 2467257
    invoke-static {p0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->m(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2467258
    invoke-static {p0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->o(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    .line 2467259
    :goto_2
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->R:Z

    if-eqz v0, :cond_0

    .line 2467260
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->G:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/listview/BetterListView;->setVerticalScrollBarEnabled(Z)V

    .line 2467261
    :cond_0
    iget v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->T:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->a(I)V

    .line 2467262
    return-void

    .line 2467263
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->F:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0062

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v4, v2, v4, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto/16 :goto_0

    .line 2467264
    :cond_2
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0ae0

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->H:Landroid/text/style/MetricAffectingSpan;

    .line 2467265
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0ae1

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->I:Landroid/text/style/MetricAffectingSpan;

    .line 2467266
    iget-object v0, p0, Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;->D:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/HSP;

    invoke-direct {v1, p0}, LX/HSP;-><init>(Lcom/facebook/pages/fb4a/events/eventslist/PageEventsListFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto/16 :goto_1
.end method
