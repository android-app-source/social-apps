.class public final Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2467845
    const-class v0, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;

    new-instance v1, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2467846
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2467847
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2467848
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2467849
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 2467850
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2467851
    invoke-virtual {v1, v0, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 2467852
    if-eqz v2, :cond_0

    .line 2467853
    const-string v3, "can_viewer_follow"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2467854
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2467855
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2467856
    if-eqz v2, :cond_1

    .line 2467857
    const-string v3, "events_calendar_can_viewer_subscribe"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2467858
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2467859
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 2467860
    if-eqz v2, :cond_2

    .line 2467861
    const-string v3, "events_calendar_subscriber_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2467862
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 2467863
    :cond_2
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 2467864
    if-eqz v2, :cond_3

    .line 2467865
    const-string v2, "events_calendar_subscription_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2467866
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2467867
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2467868
    if-eqz v2, :cond_4

    .line 2467869
    const-string v3, "owned_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2467870
    invoke-static {v1, v2, p1, p2}, LX/HSu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2467871
    :cond_4
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2467872
    if-eqz v2, :cond_5

    .line 2467873
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2467874
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2467875
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2467876
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2467877
    check-cast p1, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$Serializer;->a(Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
