.class public Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/HTK;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private a:Landroid/content/Context;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/HTJ;

.field private e:Landroid/view/LayoutInflater;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2469474
    const-class v0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;LX/HTJ;Landroid/content/Context;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/HTJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;",
            ">;",
            "Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter$OffersOnClickListener;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2469428
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2469429
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2469430
    iput-object p1, p0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->b:Ljava/util/List;

    .line 2469431
    iput-object p3, p0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->a:Landroid/content/Context;

    .line 2469432
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->e:Landroid/view/LayoutInflater;

    .line 2469433
    iput-object p2, p0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->d:LX/HTJ;

    .line 2469434
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2469470
    const/4 v2, 0x0

    .line 2469471
    sget-object v0, LX/HTM;->LARGE_ITEM:LX/HTM;

    invoke-virtual {v0}, LX/HTM;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f0306a9

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2469472
    :goto_0
    new-instance v1, LX/HTL;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->d:LX/HTJ;

    invoke-direct {v1, v0, v2}, LX/HTL;-><init>(Landroid/view/View;LX/HTJ;)V

    return-object v1

    .line 2469473
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f0306ab

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 13

    .prologue
    .line 2469439
    check-cast p1, LX/HTK;

    .line 2469440
    move v1, p2

    .line 2469441
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;

    .line 2469442
    check-cast p1, LX/HTL;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->a:Landroid/content/Context;

    const/16 v12, 0x8

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2469443
    iput-object v0, p1, LX/HTL;->m:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;

    .line 2469444
    iput v1, p1, LX/HTL;->l:I

    .line 2469445
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->m()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->m()Ljava/lang/String;

    move-result-object v3

    .line 2469446
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->k()J

    move-result-wide v5

    .line 2469447
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->j()I

    move-result v7

    .line 2469448
    :try_start_0
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 2469449
    iget-object v4, p1, LX/HTL;->m:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->n()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p1, LX/HTL;->m:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->n()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_0
    move-object v4, v8

    .line 2469450
    :goto_1
    move-object v4, v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2469451
    :goto_2
    if-eqz v4, :cond_2

    .line 2469452
    iget-object v8, p1, LX/HTK;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v9, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v8, v4, v9}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2469453
    iget-object v4, p1, LX/HTK;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v10}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2469454
    :goto_3
    iget-object v4, p1, LX/HTK;->p:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2469455
    iget-object v3, p1, LX/HTK;->q:Lcom/facebook/offers/views/OfferExpirationTimerView;

    invoke-virtual {v3, v5, v6}, Lcom/facebook/offers/views/OfferExpirationTimerView;->setExpiresOn(J)V

    .line 2469456
    const/16 v3, 0xa

    if-lt v7, v3, :cond_3

    .line 2469457
    iget-object v3, p1, LX/HTK;->r:Landroid/widget/TextView;

    const v4, 0x7f081cae

    new-array v5, v11, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v2, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2469458
    iget-object v3, p1, LX/HTK;->r:Landroid/widget/TextView;

    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2469459
    :goto_4
    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v3, v11}, Landroid/view/View;->setClickable(Z)V

    .line 2469460
    return-void

    .line 2469461
    :cond_1
    const-string v3, ""

    goto :goto_0

    .line 2469462
    :catch_0
    const/4 v4, 0x0

    goto :goto_2

    .line 2469463
    :cond_2
    iget-object v4, p1, LX/HTK;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v12}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_3

    .line 2469464
    :cond_3
    iget-object v3, p1, LX/HTK;->r:Landroid/widget/TextView;

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 2469465
    :cond_4
    iget-object v4, p1, LX/HTL;->m:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->n()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;->k()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    .line 2469466
    iget-object v4, p1, LX/HTL;->m:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->n()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;->k()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$PhotosModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$PhotosModel;->j()LX/1vs;

    move-result-object v4

    iget-object v8, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v8, v4, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 2469467
    :cond_5
    iget-object v4, p1, LX/HTL;->m:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->n()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;->m()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    .line 2469468
    iget-object v4, p1, LX/HTL;->m:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel;->n()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel;->m()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;->a()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel$VideoThumbnailsNodesModel;

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel$VideoThumbnailsNodesModel;->a()LX/1vs;

    move-result-object v4

    iget-object v8, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v8, v4, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    :cond_6
    move-object v4, v8

    .line 2469469
    goto/16 :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2469436
    if-nez p1, :cond_0

    .line 2469437
    sget-object v0, LX/HTM;->LARGE_ITEM:LX/HTM;

    invoke-virtual {v0}, LX/HTM;->ordinal()I

    move-result v0

    .line 2469438
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/HTM;->LIST_ITEM:LX/HTM;

    invoke-virtual {v0}, LX/HTM;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2469435
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
