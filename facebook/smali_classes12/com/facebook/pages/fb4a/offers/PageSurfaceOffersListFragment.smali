.class public Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/GZf;


# instance fields
.field public a:LX/HTN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/H6T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Landroid/support/v7/widget/RecyclerView;

.field public g:LX/1P1;

.field public h:Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;

.field public i:LX/HME;

.field private j:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public k:Z

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Z

.field private o:LX/E8s;

.field private p:LX/E8t;

.field private q:Landroid/view/View;

.field public r:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field public s:I

.field private t:I

.field private u:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2469359
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2469360
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->k:Z

    .line 2469361
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->m:Ljava/lang/String;

    .line 2469362
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->n:Z

    .line 2469363
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->u:I

    return-void
.end method

.method private static a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2469364
    if-eqz p0, :cond_0

    .line 2469365
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2469366
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2469367
    :cond_0
    return-object p0
.end method

.method public static a(J)Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;
    .locals 2

    .prologue
    .line 2469368
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2469369
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2469370
    new-instance v1, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;-><init>()V

    .line 2469371
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2469372
    return-object v1
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;Z)V
    .locals 2

    .prologue
    .line 2469386
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->j:Lcom/facebook/widget/listview/EmptyListViewItem;

    if-nez v0, :cond_0

    .line 2469387
    :goto_0
    return-void

    .line 2469388
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->j:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2469389
    iget-object v1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->j:Lcom/facebook/widget/listview/EmptyListViewItem;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2469373
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->o:LX/E8s;

    if-nez v0, :cond_2

    .line 2469374
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->p:LX/E8t;

    if-nez v0, :cond_1

    .line 2469375
    new-instance v0, LX/E8s;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->o:LX/E8s;

    .line 2469376
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->o:LX/E8s;

    const/4 v1, 0x1

    .line 2469377
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2469378
    :cond_0
    :goto_1
    return-void

    .line 2469379
    :cond_1
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->p:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->o:LX/E8s;

    .line 2469380
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->p:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->o:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    goto :goto_0

    .line 2469381
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->p:LX/E8t;

    if-eqz v0, :cond_0

    .line 2469382
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->o:LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->p:LX/E8t;

    .line 2469383
    iput-object v1, v0, LX/E8s;->a:Landroid/view/View;

    .line 2469384
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->p:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->o:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2469385
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->o:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->requestLayout()V

    goto :goto_1
.end method

.method public static d(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2469390
    invoke-static {p0, v7}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->a$redex0(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;Z)V

    .line 2469391
    new-instance v1, LX/HTH;

    invoke-direct {v1, p0}, LX/HTH;-><init>(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;)V

    .line 2469392
    new-instance v2, LX/HTI;

    invoke-direct {v2, p0}, LX/HTI;-><init>(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;)V

    .line 2469393
    iget-object v3, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->b:LX/1Ck;

    const-string v4, "fetchPagesOffersList"

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->l:Ljava/lang/String;

    aput-object v6, v5, v0

    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    aput-object v0, v5, v7

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2469394
    return-void

    .line 2469395
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->m:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2469353
    iget v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->u:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->u:I

    if-le p1, v0, :cond_1

    .line 2469354
    :cond_0
    :goto_0
    return-void

    .line 2469355
    :cond_1
    iput p1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->u:I

    .line 2469356
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->q:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2469357
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->q:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->u:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2469358
    const-string v0, "page_offers_list"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2469350
    iput p1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->t:I

    .line 2469351
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->f:Landroid/support/v7/widget/RecyclerView;

    iget v1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->t:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2469352
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2469347
    iput-object p1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->p:LX/E8t;

    .line 2469348
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->b()V

    .line 2469349
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const-wide/16 v2, -0x1

    .line 2469339
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2469340
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v4, p0

    check-cast v4, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    const-class v5, LX/HTN;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/HTN;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    const/16 p1, 0x259

    invoke-static {v0, p1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/H6T;->a(LX/0QB;)LX/H6T;

    move-result-object v0

    check-cast v0, LX/H6T;

    iput-object v5, v4, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->a:LX/HTN;

    iput-object v6, v4, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->b:LX/1Ck;

    iput-object v7, v4, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->c:LX/0tX;

    iput-object p1, v4, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->d:LX/0Ot;

    iput-object v0, v4, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->e:LX/H6T;

    .line 2469341
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2469342
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2469343
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 2469344
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid page id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2469345
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->l:Ljava/lang/String;

    .line 2469346
    return-void
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2469337
    iput-object p1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->r:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2469338
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2469333
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->m:Ljava/lang/String;

    .line 2469334
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->k:Z

    .line 2469335
    invoke-static {p0}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->d(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;)V

    .line 2469336
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/16 v0, 0x2a

    const v1, 0x13eba158

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2469310
    const v0, 0x7f0306aa

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 2469311
    const v0, 0x7f0d1237

    invoke-static {v7, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->f:Landroid/support/v7/widget/RecyclerView;

    .line 2469312
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->b()V

    .line 2469313
    new-instance v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->j:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2469314
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->j:Lcom/facebook/widget/listview/EmptyListViewItem;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2469315
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->j:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v8}, Lcom/facebook/widget/listview/EmptyListViewItem;->setBackgroundResource(I)V

    .line 2469316
    invoke-static {p0, v8}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->a$redex0(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;Z)V

    .line 2469317
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->q:Landroid/view/View;

    .line 2469318
    iget v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->u:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->E_(I)V

    .line 2469319
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->h:Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;

    if-nez v0, :cond_0

    .line 2469320
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2469321
    iget-object v1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->a:LX/HTN;

    .line 2469322
    new-instance v2, LX/HTJ;

    invoke-direct {v2, p0}, LX/HTJ;-><init>(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;)V

    move-object v2, v2

    .line 2469323
    new-instance v4, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;

    const-class v3, Landroid/content/Context;

    invoke-interface {v1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v0, v2, v3}, Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;-><init>(Ljava/util/List;LX/HTJ;Landroid/content/Context;)V

    .line 2469324
    move-object v0, v4

    .line 2469325
    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->h:Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;

    .line 2469326
    invoke-static {p0}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->d(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;)V

    .line 2469327
    :cond_0
    new-instance v0, LX/HME;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->o:LX/E8s;

    invoke-static {v2}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->h:Lcom/facebook/pages/fb4a/offers/PagesOffersListAdapter;

    iget-object v4, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->j:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-static {v4}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->q:Landroid/view/View;

    invoke-static {v5}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/HME;-><init>(Landroid/content/Context;Landroid/view/View;LX/1OM;Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->i:LX/HME;

    .line 2469328
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v8}, LX/1P1;-><init>(IZ)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->g:LX/1P1;

    .line 2469329
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->g:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2469330
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->f:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->i:LX/HME;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2469331
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->f:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/HTG;

    invoke-direct {v1, p0}, LX/HTG;-><init>(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2469332
    const/16 v0, 0x2b

    const v1, 0x9ae5b55

    invoke-static {v9, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v7
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x27fbc06c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2469305
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2469306
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2469307
    if-eqz v0, :cond_0

    .line 2469308
    const v2, 0x7f081ca9

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2469309
    :cond_0
    const/16 v0, 0x2b

    const v2, -0xa5764bf

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2469302
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2469303
    iget v0, p0, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->t:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->a(I)V

    .line 2469304
    return-void
.end method
