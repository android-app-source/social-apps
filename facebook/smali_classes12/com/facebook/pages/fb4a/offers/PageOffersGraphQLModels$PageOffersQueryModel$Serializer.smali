.class public final Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2468875
    const-class v0, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;

    new-instance v1, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2468876
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2468874
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2468857
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2468858
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2468859
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2468860
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2468861
    if-eqz v2, :cond_0

    .line 2468862
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2468863
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2468864
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2468865
    if-eqz v2, :cond_1

    .line 2468866
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2468867
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2468868
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2468869
    if-eqz v2, :cond_2

    .line 2468870
    const-string p0, "offers"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2468871
    invoke-static {v1, v2, p1, p2}, LX/HTE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2468872
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2468873
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2468856
    check-cast p1, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$Serializer;->a(Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
