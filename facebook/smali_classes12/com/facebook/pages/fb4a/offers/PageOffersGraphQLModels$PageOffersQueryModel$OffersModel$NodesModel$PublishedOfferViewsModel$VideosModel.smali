.class public final Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x577cf7df
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2468710
    const-class v0, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2468709
    const-class v0, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2468707
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2468708
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2468701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2468702
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;->a()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2468703
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2468704
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2468705
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2468706
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2468693
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2468694
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;->a()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2468695
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;->a()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    .line 2468696
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;->a()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2468697
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;

    .line 2468698
    iput-object v0, v1, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;->e:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    .line 2468699
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2468700
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVideoThumbnails"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2468686
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;->e:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;->e:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    .line 2468687
    iget-object v0, p0, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;->e:Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel$VideoThumbnailsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2468690
    new-instance v0, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;

    invoke-direct {v0}, Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel$OffersModel$NodesModel$PublishedOfferViewsModel$VideosModel;-><init>()V

    .line 2468691
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2468692
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2468689
    const v0, -0x5ed4cd62

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2468688
    const v0, 0x4ed245b

    return v0
.end method
