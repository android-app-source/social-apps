.class public final Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1d158dbe
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2470626
    const-class v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2470627
    const-class v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2470628
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2470629
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 2470630
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2470631
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2470632
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2470633
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2470634
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2470635
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->m()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 2470636
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2470637
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2470638
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->p()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2470639
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2470640
    const/16 v9, 0xa

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2470641
    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 2470642
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2470643
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->g:I

    invoke-virtual {p1, v0, v1, v10}, LX/186;->a(III)V

    .line 2470644
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2470645
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2470646
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2470647
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2470648
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2470649
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2470650
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2470651
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2470652
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2470661
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2470662
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2470663
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2470664
    if-eqz v1, :cond_0

    .line 2470665
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;

    .line 2470666
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->f:Ljava/util/List;

    .line 2470667
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->m()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2470668
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->m()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2470669
    if-eqz v1, :cond_1

    .line 2470670
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;

    .line 2470671
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->j:Ljava/util/List;

    :cond_1
    move-object v1, v0

    .line 2470672
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2470673
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    .line 2470674
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2470675
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;

    .line 2470676
    iput-object v0, v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    .line 2470677
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2470678
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2470653
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->e:Ljava/lang/String;

    .line 2470654
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2470655
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2470656
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->g:I

    .line 2470657
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2470658
    new-instance v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;

    invoke-direct {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;-><init>()V

    .line 2470659
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2470660
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2470624
    const v0, -0x68a56e6d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2470625
    const v0, 0x4d5f37aa    # 2.34060448E8f

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2470608
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->f:Ljava/util/List;

    .line 2470609
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2470610
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->h:Ljava/lang/String;

    .line 2470611
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2470612
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->i:Ljava/lang/String;

    .line 2470613
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final m()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2470614
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->j:Ljava/util/List;

    .line 2470615
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPolitician"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2470616
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    .line 2470617
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2470618
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->l:Ljava/lang/String;

    .line 2470619
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2470620
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->m:Ljava/lang/String;

    .line 2470621
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2470622
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n:Ljava/lang/String;

    .line 2470623
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n:Ljava/lang/String;

    return-object v0
.end method
