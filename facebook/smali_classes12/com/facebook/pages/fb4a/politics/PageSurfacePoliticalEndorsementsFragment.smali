.class public Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/GZf;


# instance fields
.field private A:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public B:Ljava/lang/String;

.field public C:Z

.field public D:Z

.field public E:Ljava/lang/String;

.field public F:Z

.field public G:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;

.field private H:LX/E8s;

.field private I:LX/E8t;

.field private J:Landroid/view/View;

.field public K:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field public L:I

.field private M:I

.field private N:I

.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Nq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/HTY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:Landroid/support/v7/widget/RecyclerView;

.field public j:LX/1P1;

.field public k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

.field public l:LX/1ON;

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Lcom/facebook/widget/listview/EmptyListViewItem;

.field private q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Lcom/facebook/fig/button/FigButton;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field public y:LX/HTP;

.field public z:LX/HTO;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2471929
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2471930
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->N:I

    return-void
.end method

.method private static a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2471925
    if-eqz p0, :cond_0

    .line 2471926
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2471927
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2471928
    :cond_0
    return-object p0
.end method

.method public static a(J)Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;
    .locals 2

    .prologue
    .line 2471920
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2471921
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2471922
    new-instance v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;-><init>()V

    .line 2471923
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2471924
    return-object v1
.end method

.method public static a(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;)V
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2471830
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-boolean v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->C:Z

    .line 2471831
    iput-boolean v1, v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->h:Z

    .line 2471832
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->B:Ljava/lang/String;

    .line 2471833
    iput-object v1, v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->i:Ljava/lang/String;

    .line 2471834
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->C:Z

    if-eqz v0, :cond_6

    .line 2471835
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->r:Landroid/widget/TextView;

    const v1, 0x7f0836f0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2471836
    :goto_0
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2471837
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->x:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2471838
    :cond_0
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;->k()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2471839
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;->k()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;

    move-result-object v3

    .line 2471840
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2471841
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471842
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->C:Z

    if-eqz v0, :cond_1

    .line 2471843
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2471844
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2471845
    sget-object v2, LX/21D;->PAGE_FEED:LX/21D;

    .line 2471846
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->C:Z

    if-eqz v0, :cond_7

    .line 2471847
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->u:Landroid/widget/TextView;

    const v1, 0x7f0836f4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2471848
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->v:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f0836f6

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2471849
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->v:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f02080f

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 2471850
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0836f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2471851
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->x:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f083701

    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->k()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471852
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->x:Landroid/widget/TextView;

    new-instance v1, LX/HUF;

    invoke-direct {v1, p0, v3}, LX/HUF;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2471853
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2471854
    new-instance v5, LX/89I;

    iget-object v6, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->B:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    sget-object v6, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v5, v10, v11, v6}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->a()Ljava/lang/String;

    move-result-object v6

    .line 2471855
    iput-object v6, v5, LX/89I;->c:Ljava/lang/String;

    .line 2471856
    move-object v5, v5

    .line 2471857
    invoke-virtual {v1, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2471858
    iput-object v0, v5, LX/89I;->d:Ljava/lang/String;

    .line 2471859
    move-object v0, v5

    .line 2471860
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v5

    .line 2471861
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2471862
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v6

    invoke-virtual {v1, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->g:LX/0SI;

    invoke-interface {v1}, LX/0SI;->b()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v6

    .line 2471863
    iget-object v9, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->v:Lcom/facebook/fig/button/FigButton;

    new-instance v0, LX/HUG;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, LX/HUG;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;LX/21D;Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/intent/ComposerPageData;)V

    invoke-virtual {v9, v0}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2471864
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->w:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0836ff

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v1, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471865
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_9

    .line 2471866
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2471867
    invoke-virtual {v1, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v7

    :goto_2
    if-eqz v0, :cond_2

    .line 2471868
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2471869
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2471870
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->C:Z

    if-nez v0, :cond_5

    .line 2471871
    :cond_3
    const/4 v2, 0x0

    .line 2471872
    const/4 v1, 0x0

    .line 2471873
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->C:Z

    if-eqz v0, :cond_a

    .line 2471874
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2471875
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2471876
    new-instance v0, LX/HU7;

    invoke-direct {v0, p0, v1}, LX/HU7;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Landroid/net/Uri;)V

    move-object v1, v0

    .line 2471877
    :cond_4
    iget-object v4, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->z:LX/HTO;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->m()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2471878
    iget-object v2, v4, LX/HTO;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p0

    invoke-virtual {v2, v6, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2471879
    iget-object v2, v4, LX/HTO;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471880
    iget-object v2, v4, LX/HTO;->c:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2471881
    invoke-virtual {v4, v1}, LX/HTO;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2471882
    :cond_5
    :goto_3
    return-void

    .line 2471883
    :cond_6
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->r:Landroid/widget/TextView;

    const v1, 0x7f0836ef

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 2471884
    :cond_7
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->u:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0836f3

    new-array v5, v7, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471885
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->v:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f0836f5

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2471886
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->v:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f02080f

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 2471887
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0836f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2471888
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->x:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f083700

    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->a()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471889
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->v:Lcom/facebook/fig/button/FigButton;

    new-instance v4, LX/HUH;

    invoke-direct {v4, p0, v2, v3, v0}, LX/HUH;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;LX/21D;Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    :cond_8
    move v0, v8

    .line 2471890
    goto/16 :goto_2

    :cond_9
    move v0, v8

    goto/16 :goto_2

    .line 2471891
    :cond_a
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2471892
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->y:LX/HTP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0836fb

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel$PoliticianModel;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 2471893
    iget-object v2, v0, LX/HTP;->d:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v2, v5}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2471894
    iget-object v2, v0, LX/HTP;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2471895
    iget-object v2, v0, LX/HTP;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2471896
    iget-object v2, v0, LX/HTP;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2471897
    iget-object v2, v0, LX/HTP;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2471898
    iget-object v2, v0, LX/HTP;->g:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2471899
    iget-object v2, v0, LX/HTP;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, LX/HTP;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0836fa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471900
    iget-object v2, v0, LX/HTP;->f:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471901
    goto/16 :goto_3

    .line 2471902
    :cond_b
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2471903
    :goto_4
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_c

    .line 2471904
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2471905
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 2471906
    :cond_c
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 2471907
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2471908
    new-instance v1, LX/HU8;

    invoke-direct {v1, p0, v0}, LX/HU8;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Landroid/net/Uri;)V

    .line 2471909
    :cond_d
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->y:LX/HTP;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$PoliticianEndorsementModuleModel;->p()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    .line 2471910
    iget-object v5, v0, LX/HTP;->d:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v5, v6}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2471911
    iget-object v5, v0, LX/HTP;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v5, v6}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2471912
    iget-object v5, v0, LX/HTP;->b:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2471913
    iget-object v5, v0, LX/HTP;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v5, v6}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2471914
    iget-object v5, v0, LX/HTP;->g:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2471915
    iget-object v5, v0, LX/HTP;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v5, v4}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 2471916
    iget-object v5, v0, LX/HTP;->b:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471917
    iget-object v5, v0, LX/HTP;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, LX/HTP;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const p0, 0x7f0836fa

    invoke-virtual {v6, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471918
    invoke-virtual {v0, v1}, LX/HTP;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2471919
    goto/16 :goto_3
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2471817
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2471818
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->D:Z

    .line 2471819
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->D:Z

    if-nez v0, :cond_0

    .line 2471820
    invoke-static {p0, v3}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Z)V

    .line 2471821
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->E:Ljava/lang/String;

    .line 2471822
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    .line 2471823
    if-eqz p1, :cond_4

    .line 2471824
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel;

    .line 2471825
    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 2471826
    iget-object p0, v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2471827
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2471828
    :cond_3
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2471829
    :cond_4
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Z)V
    .locals 2

    .prologue
    .line 2471813
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->p:Lcom/facebook/widget/listview/EmptyListViewItem;

    if-nez v0, :cond_0

    .line 2471814
    :goto_0
    return-void

    .line 2471815
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->p:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2471816
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->p:Lcom/facebook/widget/listview/EmptyListViewItem;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2471800
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->H:LX/E8s;

    if-nez v0, :cond_2

    .line 2471801
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->I:LX/E8t;

    if-nez v0, :cond_1

    .line 2471802
    new-instance v0, LX/E8s;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->H:LX/E8s;

    .line 2471803
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->H:LX/E8s;

    const/4 v1, 0x1

    .line 2471804
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2471805
    :cond_0
    :goto_1
    return-void

    .line 2471806
    :cond_1
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->I:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->H:LX/E8s;

    .line 2471807
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->I:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->H:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    goto :goto_0

    .line 2471808
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->I:LX/E8t;

    if-eqz v0, :cond_0

    .line 2471809
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->H:LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->I:LX/E8t;

    .line 2471810
    iput-object v1, v0, LX/E8s;->a:Landroid/view/View;

    .line 2471811
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->I:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->H:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2471812
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->H:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->requestLayout()V

    goto :goto_1
.end method

.method public static b(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2471795
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->A:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->A:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2471796
    if-eqz p1, :cond_0

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2471797
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2471798
    :cond_0
    const/16 v1, 0x8

    goto :goto_1

    .line 2471799
    :cond_1
    return-void
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 2471786
    invoke-static {p0, v4}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Z)V

    .line 2471787
    invoke-static {p0, v6}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->b(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Z)V

    .line 2471788
    iput-boolean v4, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->F:Z

    .line 2471789
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    .line 2471790
    iget-object v1, v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2471791
    new-instance v0, LX/HUB;

    invoke-direct {v0, p0}, LX/HUB;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;)V

    .line 2471792
    new-instance v1, LX/HUC;

    invoke-direct {v1, p0}, LX/HUC;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;)V

    .line 2471793
    iget-object v2, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a:LX/1Ck;

    const-string v3, "fetchPagesPoliticalEndorsements"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->B:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2471794
    return-void
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2471699
    iget v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->N:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->N:I

    if-le p1, v0, :cond_1

    .line 2471700
    :cond_0
    :goto_0
    return-void

    .line 2471701
    :cond_1
    iput p1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->N:I

    .line 2471702
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->J:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2471703
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->J:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->N:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2471785
    const-string v0, "page_political_endorsement"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2471782
    iput p1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->M:I

    .line 2471783
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->i:Landroid/support/v7/widget/RecyclerView;

    iget v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->M:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2471784
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2471779
    iput-object p1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->I:LX/E8t;

    .line 2471780
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->b()V

    .line 2471781
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const-wide/16 v2, -0x1

    .line 2471765
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2471766
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v4, p0

    check-cast v4, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v7

    check-cast v7, LX/1Kf;

    invoke-static {v0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v8

    check-cast v8, LX/1Nq;

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x455

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object p1

    check-cast p1, LX/0SI;

    invoke-static {v0}, LX/HTY;->a(LX/0QB;)LX/HTY;

    move-result-object v0

    check-cast v0, LX/HTY;

    iput-object v5, v4, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a:LX/1Ck;

    iput-object v6, v4, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->b:LX/0tX;

    iput-object v7, v4, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->c:LX/1Kf;

    iput-object v8, v4, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->d:LX/1Nq;

    iput-object v9, v4, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->e:LX/0Ot;

    iput-object v10, v4, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->f:LX/0Ot;

    iput-object p1, v4, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->g:LX/0SI;

    iput-object v0, v4, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->h:LX/HTY;

    .line 2471767
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2471768
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2471769
    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 2471770
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid page id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2471771
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->B:Ljava/lang/String;

    .line 2471772
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->C:Z

    .line 2471773
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->h:LX/HTY;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->B:Ljava/lang/String;

    .line 2471774
    iput-object v1, v0, LX/HTY;->b:Ljava/lang/String;

    .line 2471775
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->h:LX/HTY;

    .line 2471776
    const-string v1, "pages_endorsements_tab_click"

    invoke-static {v0, v1}, LX/HTY;->b(LX/HTY;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2471777
    iget-object v2, v0, LX/HTY;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2471778
    return-void
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2471763
    iput-object p1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->K:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2471764
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 2471761
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->d()V

    .line 2471762
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v6, 0x0

    const/16 v0, 0x2a

    const v1, 0x64f418dc

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2471711
    const v0, 0x7f0306a1

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2471712
    const v0, 0x7f0d1225

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->i:Landroid/support/v7/widget/RecyclerView;

    .line 2471713
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->b()V

    .line 2471714
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->A:Ljava/util/ArrayList;

    .line 2471715
    const v0, 0x7f030487

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->m:Landroid/view/View;

    .line 2471716
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->m:Landroid/view/View;

    const v3, 0x7f0d0d77

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2471717
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2471718
    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v3

    .line 2471719
    iput-object v3, v0, LX/1Uo;->u:LX/4Ab;

    .line 2471720
    move-object v0, v0

    .line 2471721
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2471722
    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2471723
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->m:Landroid/view/View;

    const v3, 0x7f0d0d78

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->r:Landroid/widget/TextView;

    .line 2471724
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->m:Landroid/view/View;

    const v3, 0x7f0d0d79

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->s:Landroid/widget/TextView;

    .line 2471725
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->m:Landroid/view/View;

    const v3, 0x7f0d0d7a

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->t:Landroid/widget/TextView;

    .line 2471726
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->t:Landroid/widget/TextView;

    new-instance v3, LX/HU9;

    invoke-direct {v3, p0}, LX/HU9;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2471727
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->A:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2471728
    const v0, 0x7f030486

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->n:Landroid/view/View;

    .line 2471729
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->n:Landroid/view/View;

    const v3, 0x7f0d0d73

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->u:Landroid/widget/TextView;

    .line 2471730
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->n:Landroid/view/View;

    const v3, 0x7f0d0d74

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->v:Lcom/facebook/fig/button/FigButton;

    .line 2471731
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->A:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->n:Landroid/view/View;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2471732
    new-instance v0, LX/HTP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/HTP;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->y:LX/HTP;

    .line 2471733
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->y:LX/HTP;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v5, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, LX/HTP;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2471734
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->A:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->y:LX/HTP;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2471735
    new-instance v0, LX/HTO;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/HTO;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->z:LX/HTO;

    .line 2471736
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->z:LX/HTO;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v5, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, LX/HTO;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2471737
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->A:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->z:LX/HTO;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2471738
    const v0, 0x7f030489

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->o:Landroid/view/View;

    .line 2471739
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->o:Landroid/view/View;

    const v3, 0x7f0d0d82

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->w:Landroid/widget/TextView;

    .line 2471740
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->o:Landroid/view/View;

    const v3, 0x7f0d0d83

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->x:Landroid/widget/TextView;

    .line 2471741
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->A:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->o:Landroid/view/View;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2471742
    new-instance v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->p:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2471743
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->p:Lcom/facebook/widget/listview/EmptyListViewItem;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2471744
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->p:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v6}, Lcom/facebook/widget/listview/EmptyListViewItem;->setBackgroundResource(I)V

    .line 2471745
    invoke-static {p0, v6}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Z)V

    .line 2471746
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->J:Landroid/view/View;

    .line 2471747
    iget v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->N:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->E_(I)V

    .line 2471748
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    if-nez v0, :cond_0

    .line 2471749
    new-instance v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    .line 2471750
    :cond_0
    new-instance v0, LX/1ON;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->k:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    invoke-direct {v0, v3}, LX/1ON;-><init>(LX/1OO;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->l:LX/1ON;

    .line 2471751
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->l:LX/1ON;

    const/4 v3, 0x6

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->H:LX/E8s;

    invoke-static {v4}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->m:Landroid/view/View;

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->n:Landroid/view/View;

    aput-object v4, v3, v8

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->y:LX/HTP;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->z:LX/HTO;

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->o:Landroid/view/View;

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1ON;->a(Ljava/util/ArrayList;)V

    .line 2471752
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->l:LX/1ON;

    new-array v3, v8, [Landroid/view/View;

    iget-object v4, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->p:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-static {v4}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->J:Landroid/view/View;

    invoke-static {v4}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1ON;->b(Ljava/util/ArrayList;)V

    .line 2471753
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    invoke-direct {v0, v7, v6}, LX/1P1;-><init>(IZ)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->j:LX/1P1;

    .line 2471754
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->j:LX/1P1;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2471755
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->l:LX/1ON;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2471756
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->i:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, LX/HUA;

    invoke-direct {v3, p0}, LX/HUA;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2471757
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->G:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;

    if-nez v0, :cond_1

    .line 2471758
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->d()V

    .line 2471759
    :goto_0
    const v0, -0x21c2fae

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 2471760
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->G:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;

    invoke-static {p0, v0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x669789c4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2471707
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2471708
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2471709
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2471710
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x26ffbcc7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2471704
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2471705
    iget v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->M:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->a(I)V

    .line 2471706
    return-void
.end method
