.class public final Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2472176
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2472177
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2472178
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2472179
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2472180
    if-nez p1, :cond_0

    move v0, v1

    .line 2472181
    :goto_0
    return v0

    .line 2472182
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2472183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2472184
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2472185
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2472186
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2472187
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2472188
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2472189
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2472190
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2472191
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2472192
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2472193
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 2472194
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2472195
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2472196
    const v2, 0x72607d7b

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2472197
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2472198
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2472199
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2472200
    :sswitch_2
    const-class v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$IssueModel$IssueEdgesModel$IssueEdgesNodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$IssueModel$IssueEdgesModel$IssueEdgesNodeModel;

    .line 2472201
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2472202
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2472203
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2472204
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2472205
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2472206
    const v2, 0x4f3d3985

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2472207
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2472208
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2472209
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2472210
    :sswitch_4
    const-class v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;

    .line 2472211
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2472212
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2472213
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2472214
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2472215
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2472216
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2472217
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 2472218
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 2472219
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2472220
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2472221
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2472222
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2472223
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 2472224
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 2472225
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 2472226
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x520345fb -> :sswitch_1
        -0x40e88b0f -> :sswitch_3
        0xda4283c -> :sswitch_0
        0x3a337041 -> :sswitch_5
        0x4f3d3985 -> :sswitch_4
        0x72607d7b -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2472160
    if-nez p0, :cond_0

    move v0, v1

    .line 2472161
    :goto_0
    return v0

    .line 2472162
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2472163
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2472164
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2472165
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2472166
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2472167
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2472168
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2472227
    const/4 v7, 0x0

    .line 2472228
    const/4 v1, 0x0

    .line 2472229
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2472230
    invoke-static {v2, v3, v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2472231
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2472232
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2472233
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2472267
    new-instance v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2472234
    if-eqz p0, :cond_0

    .line 2472235
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2472236
    if-eq v0, p0, :cond_0

    .line 2472237
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2472238
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2472239
    sparse-switch p2, :sswitch_data_0

    .line 2472240
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2472241
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2472242
    const v1, 0x72607d7b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2472243
    :goto_0
    :sswitch_1
    return-void

    .line 2472244
    :sswitch_2
    const-class v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$IssueModel$IssueEdgesModel$IssueEdgesNodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$IssueModel$IssueEdgesModel$IssueEdgesNodeModel;

    .line 2472245
    invoke-static {v0, p3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 2472246
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2472247
    const v1, 0x4f3d3985

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2472248
    :sswitch_4
    const-class v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;

    .line 2472249
    invoke-static {v0, p3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x520345fb -> :sswitch_0
        -0x40e88b0f -> :sswitch_3
        0xda4283c -> :sswitch_1
        0x3a337041 -> :sswitch_1
        0x4f3d3985 -> :sswitch_4
        0x72607d7b -> :sswitch_2
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2472250
    if-nez p1, :cond_0

    move v0, v1

    .line 2472251
    :goto_0
    return v0

    .line 2472252
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 2472253
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2472254
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2472255
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 2472256
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 2472257
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2472258
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2472259
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2472260
    if-eqz p1, :cond_0

    .line 2472261
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 2472262
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2472263
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 2472264
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2472265
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2472266
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2472169
    if-eqz p1, :cond_0

    .line 2472170
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2472171
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;

    .line 2472172
    if-eq v0, v1, :cond_0

    .line 2472173
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2472174
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2472175
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2472137
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2472138
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2472132
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2472133
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2472134
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2472135
    iput p2, p0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->b:I

    .line 2472136
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2472131
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2472130
    new-instance v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2472127
    iget v0, p0, LX/1vt;->c:I

    .line 2472128
    move v0, v0

    .line 2472129
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2472139
    iget v0, p0, LX/1vt;->c:I

    .line 2472140
    move v0, v0

    .line 2472141
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2472142
    iget v0, p0, LX/1vt;->b:I

    .line 2472143
    move v0, v0

    .line 2472144
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2472145
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2472146
    move-object v0, v0

    .line 2472147
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2472148
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2472149
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2472150
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2472151
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2472152
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2472153
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2472154
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2472155
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2472156
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2472157
    iget v0, p0, LX/1vt;->c:I

    .line 2472158
    move v0, v0

    .line 2472159
    return v0
.end method
