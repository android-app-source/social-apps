.class public final Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x145cdd32
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2469851
    const-class v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2469850
    const-class v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2469848
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2469849
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2469842
    iput-object p1, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->f:Ljava/lang/String;

    .line 2469843
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2469844
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2469845
    if-eqz v0, :cond_0

    .line 2469846
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2469847
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2469839
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2469840
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2469841
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2469831
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2469832
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2469833
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2469834
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2469835
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2469836
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2469837
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2469838
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2469811
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2469812
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2469813
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2469830
    new-instance v0, LX/HTe;

    invoke-direct {v0, p1}, LX/HTe;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2469828
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->f:Ljava/lang/String;

    .line 2469829
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2469822
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2469823
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2469824
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2469825
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2469826
    :goto_0
    return-void

    .line 2469827
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2469819
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2469820
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->a(Ljava/lang/String;)V

    .line 2469821
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2469816
    new-instance v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;

    invoke-direct {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;-><init>()V

    .line 2469817
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2469818
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2469815
    const v0, 0x115e6417

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2469814
    const v0, 0x499e8e7

    return v0
.end method
