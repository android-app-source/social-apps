.class public final Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2470209
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2470210
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2470238
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2470239
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2470212
    if-nez p1, :cond_0

    .line 2470213
    :goto_0
    return v0

    .line 2470214
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2470215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2470216
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2470217
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2470218
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2470219
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2470220
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2470221
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2470222
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2470223
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2470224
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2470225
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2470226
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2470227
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2470228
    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 2470229
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2470230
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2470231
    invoke-virtual {p3, v4, v2}, LX/186;->a(IZ)V

    .line 2470232
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2470233
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2470234
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2470235
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2470236
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2470237
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x158b1315 -> :sswitch_1
        0x54fd4485 -> :sswitch_3
        0x63c0b4d7 -> :sswitch_2
        0x69434e1d -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2470211
    new-instance v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2470206
    sparse-switch p0, :sswitch_data_0

    .line 2470207
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2470208
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x158b1315 -> :sswitch_0
        0x54fd4485 -> :sswitch_0
        0x63c0b4d7 -> :sswitch_0
        0x69434e1d -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2470205
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2470203
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;->b(I)V

    .line 2470204
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2470240
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2470241
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2470242
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2470243
    iput p2, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;->b:I

    .line 2470244
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2470177
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2470178
    new-instance v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2470179
    iget v0, p0, LX/1vt;->c:I

    .line 2470180
    move v0, v0

    .line 2470181
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2470182
    iget v0, p0, LX/1vt;->c:I

    .line 2470183
    move v0, v0

    .line 2470184
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2470200
    iget v0, p0, LX/1vt;->b:I

    .line 2470201
    move v0, v0

    .line 2470202
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2470185
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2470186
    move-object v0, v0

    .line 2470187
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2470188
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2470189
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2470190
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2470191
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2470192
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2470193
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2470194
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2470195
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2470196
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2470197
    iget v0, p0, LX/1vt;->c:I

    .line 2470198
    move v0, v0

    .line 2470199
    return v0
.end method
