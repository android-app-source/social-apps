.class public final Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x477a76a3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2470344
    const-class v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2470343
    const-class v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2470341
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2470342
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2470338
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2470339
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2470340
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2470336
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->f:Ljava/lang/String;

    .line 2470337
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2470307
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2470308
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2470309
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2470310
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2470311
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2470312
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2470313
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2470314
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2470315
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2470316
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2470328
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2470329
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2470330
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    .line 2470331
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2470332
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;

    .line 2470333
    iput-object v0, v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->g:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    .line 2470334
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2470335
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2470327
    new-instance v0, LX/HTg;

    invoke-direct {v0, p1}, LX/HTg;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2470325
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->g:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->g:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    .line 2470326
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;->g:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel$ProfilePictureModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2470323
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2470324
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2470322
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2470319
    new-instance v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;

    invoke-direct {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$EndorserProfileFragmentModel;-><init>()V

    .line 2470320
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2470321
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2470318
    const v0, 0x158dc094

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2470317
    const v0, 0x50c72189

    return v0
.end method
