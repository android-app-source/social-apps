.class public Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/D3w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HU1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private h:Landroid/view/LayoutInflater;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2471456
    const-class v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2471454
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2471455
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 2471446
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2471447
    const-class v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2471448
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->h:Landroid/view/LayoutInflater;

    .line 2471449
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->h:Landroid/view/LayoutInflater;

    const v1, 0x7f0309a1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2471450
    const v0, 0x7f0d1893

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2471451
    const v0, 0x7f0d1894

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2471452
    const v0, 0x7f0d1897

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2471453
    return-void
.end method

.method private static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2471428
    div-int/lit16 v0, p0, 0x3e8

    .line 2471429
    div-int/lit8 v1, v0, 0x3c

    .line 2471430
    rem-int/lit8 v0, v0, 0x3c

    .line 2471431
    const-string v2, "%d:%02d"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2, v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;LX/0Or;LX/D3w;LX/HU1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/D3w;",
            "LX/HU1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2471445
    iput-object p1, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->b:LX/D3w;

    iput-object p3, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->c:LX/HU1;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;

    const/16 v0, 0x509

    invoke-static {v1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v1}, LX/D3w;->b(LX/0QB;)LX/D3w;

    move-result-object v0

    check-cast v0, LX/D3w;

    invoke-static {v1}, LX/HU1;->a(LX/0QB;)LX/HU1;

    move-result-object v1

    check-cast v1, LX/HU1;

    invoke-static {p0, v2, v0, v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->a(Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;LX/0Or;LX/D3w;LX/HU1;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2471432
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 2471433
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2471434
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x3fe38e39

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2471435
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2471436
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2471437
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2471438
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->y()I

    move-result v1

    invoke-static {v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471439
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2471440
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2471441
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2471442
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/HU0;

    invoke-direct {v1, p0, p2, p1}, LX/HU0;-><init>(Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;Ljava/lang/String;Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2471443
    return-void

    .line 2471444
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method
