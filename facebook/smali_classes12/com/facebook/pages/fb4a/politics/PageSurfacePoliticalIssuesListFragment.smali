.class public Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/GZf;


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HU1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Nq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:Landroid/support/v7/widget/RecyclerView;

.field public i:LX/1P1;

.field public j:LX/HU3;

.field public k:LX/HME;

.field private l:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public m:LX/6WS;

.field public n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field public p:LX/3Sb;

.field public q:Z

.field public r:Ljava/lang/String;

.field private s:LX/E8s;

.field private t:LX/E8t;

.field private u:Landroid/view/View;

.field public v:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field public w:I

.field private x:I

.field private y:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2472101
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2472102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->r:Ljava/lang/String;

    .line 2472103
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->y:I

    return-void
.end method

.method private static a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2472097
    if-eqz p0, :cond_0

    .line 2472098
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2472099
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2472100
    :cond_0
    return-object p0
.end method

.method public static a(JLjava/lang/String;)Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;
    .locals 2

    .prologue
    .line 2471994
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2471995
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2471996
    const-string v1, "profile_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2471997
    new-instance v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;-><init>()V

    .line 2471998
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2471999
    return-object v1
.end method

.method public static a(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2472086
    new-instance v0, LX/6WS;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/6WS;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->m:LX/6WS;

    .line 2472087
    new-instance v2, LX/5OG;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, LX/5OG;-><init>(Landroid/content/Context;)V

    move v0, v1

    .line 2472088
    :goto_0
    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->p:LX/3Sb;

    invoke-interface {v3}, LX/3Sb;->c()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2472089
    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->p:LX/3Sb;

    invoke-interface {v3, v0}, LX/3Sb;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2472090
    invoke-virtual {v4, v3, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v0, v3}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    .line 2472091
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2472092
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2472093
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->m:LX/6WS;

    invoke-virtual {v0, v2}, LX/5OM;->a(LX/5OG;)V

    .line 2472094
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->m:LX/6WS;

    new-instance v1, LX/HUL;

    invoke-direct {v1, p0, p2, p1}, LX/HUL;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2472095
    iput-object v1, v0, LX/5OM;->p:LX/5OO;

    .line 2472096
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2472082
    sget-object v0, LX/21D;->PAGE_FEED:LX/21D;

    .line 2472083
    invoke-static {p1}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v1

    invoke-virtual {v1}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    invoke-static {v0, p2, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->g:LX/1Nq;

    invoke-static {p3}, Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setAllowTargetSelection(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2472084
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->f:LX/1Kf;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2472085
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Z)V
    .locals 2

    .prologue
    .line 2472078
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    if-nez v0, :cond_0

    .line 2472079
    :goto_0
    return-void

    .line 2472080
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2472081
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2472065
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->s:LX/E8s;

    if-nez v0, :cond_2

    .line 2472066
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->t:LX/E8t;

    if-nez v0, :cond_1

    .line 2472067
    new-instance v0, LX/E8s;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->s:LX/E8s;

    .line 2472068
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->s:LX/E8s;

    const/4 v1, 0x1

    .line 2472069
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2472070
    :cond_0
    :goto_1
    return-void

    .line 2472071
    :cond_1
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->t:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->s:LX/E8s;

    .line 2472072
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->t:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->s:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    goto :goto_0

    .line 2472073
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->t:LX/E8t;

    if-eqz v0, :cond_0

    .line 2472074
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->s:LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->t:LX/E8t;

    .line 2472075
    iput-object v1, v0, LX/E8s;->a:Landroid/view/View;

    .line 2472076
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->t:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->s:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2472077
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->s:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->requestLayout()V

    goto :goto_1
.end method

.method private e()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2472059
    invoke-static {p0, v7}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Z)V

    .line 2472060
    new-instance v1, LX/HUJ;

    invoke-direct {v1, p0}, LX/HUJ;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;)V

    .line 2472061
    new-instance v2, LX/HUK;

    invoke-direct {v2, p0}, LX/HUK;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;)V

    .line 2472062
    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a:LX/1Ck;

    const-string v4, "fetchPagesPoliticalIssuesList"

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->n:Ljava/lang/String;

    aput-object v6, v5, v0

    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->r:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    aput-object v0, v5, v7

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2472063
    return-void

    .line 2472064
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->r:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2472054
    iget v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->y:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->y:I

    if-le p1, v0, :cond_1

    .line 2472055
    :cond_0
    :goto_0
    return-void

    .line 2472056
    :cond_1
    iput p1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->y:I

    .line 2472057
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->u:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2472058
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->u:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->y:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2472053
    const-string v0, "page_politcal_issues_list"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2472050
    iput p1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->x:I

    .line 2472051
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->h:Landroid/support/v7/widget/RecyclerView;

    iget v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->x:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2472052
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2472047
    iput-object p1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->t:LX/E8t;

    .line 2472048
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->c()V

    .line 2472049
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const-wide/16 v4, -0x1

    .line 2472030
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2472031
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    const/16 v7, 0x259

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/HU1;->a(LX/0QB;)LX/HU1;

    move-result-object v8

    check-cast v8, LX/HU1;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object p1

    check-cast p1, LX/1Kf;

    invoke-static {v0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v0

    check-cast v0, LX/1Nq;

    iput-object v3, v2, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a:LX/1Ck;

    iput-object v6, v2, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->b:LX/0tX;

    iput-object v7, v2, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->c:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->d:LX/HU1;

    iput-object v9, v2, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->e:LX/0Uh;

    iput-object p1, v2, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->f:LX/1Kf;

    iput-object v0, v2, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->g:LX/1Nq;

    .line 2472032
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2472033
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 2472034
    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2472035
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid page id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2472036
    :cond_0
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->n:Ljava/lang/String;

    .line 2472037
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->o:Ljava/lang/String;

    .line 2472038
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->d:LX/HU1;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->o:Ljava/lang/String;

    .line 2472039
    iput-object v1, v0, LX/HU1;->b:Ljava/lang/String;

    .line 2472040
    iput-object v2, v0, LX/HU1;->c:Ljava/lang/String;

    .line 2472041
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->d:LX/HU1;

    .line 2472042
    const-string v1, "pages_issues_tab_click"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/HU1;->c(LX/HU1;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2472043
    iget-object v2, v0, LX/HU1;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2472044
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->q:Z

    .line 2472045
    new-instance v0, LX/4AD;

    invoke-direct {v0}, LX/4AD;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->p:LX/3Sb;

    .line 2472046
    return-void
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2472028
    iput-object p1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->v:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2472029
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2472025
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->r:Ljava/lang/String;

    .line 2472026
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->e()V

    .line 2472027
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/16 v0, 0x2a

    const v1, 0x3a733ab9

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2472007
    const v0, 0x7f0306a7

    invoke-virtual {p1, v0, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 2472008
    const v0, 0x7f0d122f

    invoke-static {v7, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->h:Landroid/support/v7/widget/RecyclerView;

    .line 2472009
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->c()V

    .line 2472010
    new-instance v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2472011
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2472012
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v8}, Lcom/facebook/widget/listview/EmptyListViewItem;->setBackgroundResource(I)V

    .line 2472013
    invoke-static {p0, v8}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Z)V

    .line 2472014
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->u:Landroid/view/View;

    .line 2472015
    iget v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->y:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->E_(I)V

    .line 2472016
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->j:LX/HU3;

    if-nez v0, :cond_0

    .line 2472017
    new-instance v0, LX/HU3;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->e:LX/0Uh;

    invoke-direct {v0, v1, p0, v2}, LX/HU3;-><init>(Landroid/content/Context;Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;LX/0Uh;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->j:LX/HU3;

    .line 2472018
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->e()V

    .line 2472019
    :cond_0
    new-instance v0, LX/HME;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->s:LX/E8s;

    invoke-static {v2}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->j:LX/HU3;

    iget-object v4, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->l:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-static {v4}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->u:Landroid/view/View;

    invoke-static {v5}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/HME;-><init>(Landroid/content/Context;Landroid/view/View;LX/1OM;Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->k:LX/HME;

    .line 2472020
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v8}, LX/1P1;-><init>(IZ)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->i:LX/1P1;

    .line 2472021
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->i:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2472022
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->k:LX/HME;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2472023
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->h:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/HUI;

    invoke-direct {v1, p0}, LX/HUI;-><init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 2472024
    const/16 v0, 0x2b

    const v1, 0x654eae81

    invoke-static {v9, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v7
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4dfa9356

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2472003
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2472004
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2472005
    iget-object v1, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2472006
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x178291bb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2472000
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2472001
    iget v0, p0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->x:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a(I)V

    .line 2472002
    return-void
.end method
