.class public final Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7364db50
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2472493
    const-class v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2472494
    const-class v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2472491
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2472492
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2472489
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;->e:Ljava/lang/String;

    .line 2472490
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2472481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2472482
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2472483
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2472484
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2472485
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2472486
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2472487
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2472488
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2472495
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2472496
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2472497
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2472480
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2472477
    new-instance v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;

    invoke-direct {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;-><init>()V

    .line 2472478
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2472479
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2472476
    const v0, -0x58924507

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2472473
    const v0, -0x39593fae

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2472474
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;->f:Ljava/lang/String;

    .line 2472475
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;->f:Ljava/lang/String;

    return-object v0
.end method
