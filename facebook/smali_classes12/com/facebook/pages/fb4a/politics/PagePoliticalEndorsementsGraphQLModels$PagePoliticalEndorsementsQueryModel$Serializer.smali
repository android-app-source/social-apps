.class public final Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2470679
    const-class v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;

    new-instance v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2470680
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2470681
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2470682
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2470683
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2470684
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2470685
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2470686
    if-eqz v2, :cond_0

    .line 2470687
    const-string p0, "actor"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2470688
    invoke-static {v1, v2, p1}, LX/HTu;->a(LX/15i;ILX/0nX;)V

    .line 2470689
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2470690
    if-eqz v2, :cond_1

    .line 2470691
    const-string p0, "admined_pages"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2470692
    invoke-static {v1, v2, p1, p2}, LX/HTw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2470693
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2470694
    if-eqz v2, :cond_2

    .line 2470695
    const-string p0, "approved_political_endorsements"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2470696
    invoke-static {v1, v2, p1, p2}, LX/HTr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2470697
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2470698
    if-eqz v2, :cond_3

    .line 2470699
    const-string p0, "has_endorsement_review_permission"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2470700
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2470701
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2470702
    if-eqz v2, :cond_4

    .line 2470703
    const-string p0, "politician_endorsement_module"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2470704
    invoke-static {v1, v2, p1, p2}, LX/HTy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2470705
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2470706
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2470707
    check-cast p1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel$Serializer;->a(Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PagePoliticalEndorsementsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
