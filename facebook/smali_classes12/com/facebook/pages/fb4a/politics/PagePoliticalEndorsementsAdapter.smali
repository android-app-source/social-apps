.class public Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements LX/02k;
.implements LX/1OO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/HTX;",
        ">;",
        "LX/02k;",
        "LX/1OO",
        "<",
        "LX/HTX;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/11S;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hy;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;",
            ">;"
        }
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;

.field public j:Landroid/content/Context;

.field private k:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 2469618
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2469619
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->g:Ljava/util/ArrayList;

    .line 2469620
    iput-object p1, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->j:Landroid/content/Context;

    .line 2469621
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->j:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->k:Landroid/view/LayoutInflater;

    .line 2469622
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v3

    check-cast v3, LX/11S;

    const/16 v4, 0x455

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xbe1

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p1

    check-cast p1, LX/0tX;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v0

    check-cast v0, LX/0SI;

    iput-object v3, v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->a:LX/11S;

    iput-object v4, v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->b:LX/0Ot;

    iput-object v5, v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->c:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->d:LX/1Ck;

    iput-object p1, v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->e:LX/0tX;

    iput-object v0, v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->f:LX/0SI;

    .line 2469623
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2469624
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->k:Landroid/view/LayoutInflater;

    const v1, 0x7f030488

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2469625
    new-instance v1, LX/HTX;

    invoke-direct {v1, p0, v0}, LX/HTX;-><init>(Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    .line 2469568
    check-cast p1, LX/HTX;

    .line 2469569
    invoke-virtual {p0, p2}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;

    .line 2469570
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2469571
    iput p2, p1, LX/HTX;->m:I

    .line 2469572
    iget-object v1, p1, LX/HTX;->n:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 2469573
    iget-object v1, p1, LX/HTX;->n:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2469574
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2469575
    iget-object v4, p1, LX/HTX;->o:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2469576
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;->k()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_6

    .line 2469577
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;->k()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2469578
    invoke-virtual {v4, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_0
    if-eqz v1, :cond_0

    .line 2469579
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;->k()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v4, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2469580
    iget-object v4, p1, LX/HTX;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2469581
    :cond_0
    iget-object v4, p1, LX/HTX;->o:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ActorsModel;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    const v1, 0x7f021a25

    :goto_1
    invoke-virtual {v4, v3, v3, v1, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2469582
    :cond_1
    iget-object v1, p1, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->a:LX/11S;

    sget-object v4, LX/1lB;->EXACT_STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->k()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-interface {v1, v4, v5, v6}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v5

    .line 2469583
    const/4 v1, 0x0

    .line 2469584
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->m()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2469585
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->m()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$ImplicitPlaceModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2469586
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->o()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$PrivacyScopeModel;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 2469587
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->o()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$PrivacyScopeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$PrivacyScopeModel;->a()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    .line 2469588
    if-eqz v4, :cond_8

    move v4, v2

    :goto_2
    if-eqz v4, :cond_b

    .line 2469589
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->o()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$PrivacyScopeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$PrivacyScopeModel;->a()LX/1vs;

    move-result-object v4

    iget-object v6, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2469590
    invoke-virtual {v6, v4, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    move v4, v2

    :goto_3
    if-eqz v4, :cond_3

    .line 2469591
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->o()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$PrivacyScopeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$PrivacyScopeModel;->a()LX/1vs;

    move-result-object v4

    iget-object v6, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2469592
    iget-object v7, p1, LX/HTX;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v6, v4, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    invoke-virtual {v7, v4, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2469593
    :cond_3
    if-eqz v1, :cond_c

    .line 2469594
    iget-object v4, p1, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v4, v4, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->j:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f083702

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v5, v7, v3

    aput-object v1, v7, v2

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2469595
    :goto_4
    iget-object v2, p1, LX/HTX;->p:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2469596
    iget-object v1, p1, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-boolean v1, v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->h:Z

    if-eqz v1, :cond_d

    .line 2469597
    const/4 v5, 0x0

    .line 2469598
    new-instance v1, LX/6WS;

    iget-object v2, p1, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->j:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2469599
    iget-object v2, p1, LX/HTX;->t:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v2}, LX/0ht;->c(Landroid/view/View;)V

    .line 2469600
    new-instance v2, LX/5OG;

    iget-object v3, p1, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->j:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 2469601
    invoke-virtual {v1, v2}, LX/5OM;->a(LX/5OG;)V

    .line 2469602
    new-instance v3, LX/3Ai;

    const v4, 0x7f083704

    invoke-direct {v3, v2, v5, v5, v4}, LX/3Ai;-><init>(Landroid/view/Menu;III)V

    .line 2469603
    invoke-virtual {v2, v3}, LX/5OG;->a(LX/3Ai;)V

    .line 2469604
    const v2, 0x7f020818

    invoke-virtual {v3, v2}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2469605
    new-instance v2, LX/HTS;

    invoke-direct {v2, p1}, LX/HTS;-><init>(LX/HTX;)V

    invoke-virtual {v3, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2469606
    iget-object v2, p1, LX/HTX;->t:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setClickable(Z)V

    .line 2469607
    iget-object v2, p1, LX/HTX;->t:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v3, LX/HTT;

    invoke-direct {v3, p1, v1}, LX/HTT;-><init>(LX/HTX;LX/6WS;)V

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2469608
    :goto_5
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$MessageModel;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2469609
    iget-object v1, p1, LX/HTX;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->n()Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$MessageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2469610
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$ApprovedPoliticalEndorsementsFragmentModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, LX/HTX;->u:Ljava/lang/String;

    .line 2469611
    return-void

    :cond_5
    move v1, v3

    .line 2469612
    goto/16 :goto_0

    :cond_6
    move v1, v3

    goto/16 :goto_0

    :cond_7
    move v1, v3

    .line 2469613
    goto/16 :goto_1

    :cond_8
    move v4, v3

    .line 2469614
    goto/16 :goto_2

    :cond_9
    move v4, v3

    goto/16 :goto_2

    :cond_a
    move v4, v3

    goto/16 :goto_3

    :cond_b
    move v4, v3

    goto/16 :goto_3

    .line 2469615
    :cond_c
    iget-object v1, p1, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f083703

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v5, v2, v3

    invoke-virtual {v1, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_4

    .line 2469616
    :cond_d
    iget-object v1, p1, LX/HTX;->t:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_5
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2469617
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->j:Landroid/content/Context;

    return-object v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2469567
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final ii_()I
    .locals 1

    .prologue
    .line 2469566
    const/4 v0, 0x1

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2469565
    iget-object v0, p0, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
