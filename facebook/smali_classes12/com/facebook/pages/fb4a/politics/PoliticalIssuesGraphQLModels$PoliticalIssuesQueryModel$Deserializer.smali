.class public final Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2472268
    const-class v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;

    new-instance v1, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2472269
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2472270
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2472271
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2472272
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2472273
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 2472274
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2472275
    :goto_0
    move v1, v2

    .line 2472276
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2472277
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2472278
    new-instance v1, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;-><init>()V

    .line 2472279
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2472280
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2472281
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2472282
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2472283
    :cond_0
    return-object v1

    .line 2472284
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_6

    .line 2472285
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2472286
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2472287
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 2472288
    const-string v8, "is_political_issue_interaction_supported"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2472289
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_1

    .line 2472290
    :cond_2
    const-string v8, "political_issue_interaction_configs"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2472291
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2472292
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_3

    .line 2472293
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_3

    .line 2472294
    const/4 v8, 0x0

    .line 2472295
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_e

    .line 2472296
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2472297
    :goto_3
    move v7, v8

    .line 2472298
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2472299
    :cond_3
    invoke-static {v5, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 2472300
    goto :goto_1

    .line 2472301
    :cond_4
    const-string v8, "politician_issue_views_collection"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2472302
    invoke-static {p1, v0}, LX/HUX;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2472303
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2472304
    :cond_6
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2472305
    if-eqz v1, :cond_7

    .line 2472306
    invoke-virtual {v0, v2, v6}, LX/186;->a(IZ)V

    .line 2472307
    :cond_7
    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 2472308
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2472309
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto/16 :goto_1

    .line 2472310
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2472311
    :cond_a
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_d

    .line 2472312
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2472313
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2472314
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_a

    if-eqz v11, :cond_a

    .line 2472315
    const-string p0, "action_text"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    .line 2472316
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_4

    .line 2472317
    :cond_b
    const-string p0, "interaction_type"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 2472318
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 2472319
    :cond_c
    const-string p0, "placeholder_text"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 2472320
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 2472321
    :cond_d
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 2472322
    invoke-virtual {v0, v8, v10}, LX/186;->b(II)V

    .line 2472323
    const/4 v8, 0x1

    invoke-virtual {v0, v8, v9}, LX/186;->b(II)V

    .line 2472324
    const/4 v8, 0x2

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 2472325
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto/16 :goto_3

    :cond_e
    move v7, v8

    move v9, v8

    move v10, v8

    goto :goto_4
.end method
