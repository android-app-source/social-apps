.class public final Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2472756
    const-class v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;

    new-instance v1, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2472757
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2472755
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2472758
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2472759
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2472760
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2472761
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2472762
    if-eqz v2, :cond_0

    .line 2472763
    const-string p0, "is_political_issue_interaction_supported"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2472764
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2472765
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2472766
    if-eqz v2, :cond_1

    .line 2472767
    const-string p0, "political_issue_interaction_configs"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2472768
    invoke-static {v1, v2, p1, p2}, LX/HUP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2472769
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2472770
    if-eqz v2, :cond_2

    .line 2472771
    const-string p0, "politician_issue_views_collection"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2472772
    invoke-static {v1, v2, p1, p2}, LX/HUX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2472773
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2472774
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2472754
    check-cast p1, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$Serializer;->a(Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
