.class public Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field public static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HSF;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8EC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HDT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

.field public f:Lcom/facebook/fbui/widget/text/BadgeTextView;

.field public g:Lcom/facebook/fbui/widget/text/BadgeTextView;

.field private h:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

.field private i:Landroid/os/Handler;

.field public j:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

.field public k:LX/HSE;

.field private l:Z

.field private final m:Landroid/view/View$OnClickListener;

.field private final n:LX/HDc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2466827
    invoke-static {}, LX/HSF;->values()[LX/HSF;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->d:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2466777
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2466778
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    .line 2466779
    new-instance v0, LX/HSA;

    invoke-direct {v0, p0}, LX/HSA;-><init>(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->m:Landroid/view/View$OnClickListener;

    .line 2466780
    new-instance v0, LX/HSB;

    invoke-direct {v0, p0}, LX/HSB;-><init>(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->n:LX/HDc;

    .line 2466781
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->b()V

    .line 2466782
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2466783
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2466784
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    .line 2466785
    new-instance v0, LX/HSA;

    invoke-direct {v0, p0}, LX/HSA;-><init>(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->m:Landroid/view/View$OnClickListener;

    .line 2466786
    new-instance v0, LX/HSB;

    invoke-direct {v0, p0}, LX/HSB;-><init>(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->n:LX/HDc;

    .line 2466787
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->b()V

    .line 2466788
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2466753
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2466754
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    .line 2466755
    new-instance v0, LX/HSA;

    invoke-direct {v0, p0}, LX/HSA;-><init>(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->m:Landroid/view/View$OnClickListener;

    .line 2466756
    new-instance v0, LX/HSB;

    invoke-direct {v0, p0}, LX/HSB;-><init>(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->n:LX/HDc;

    .line 2466757
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->b()V

    .line 2466758
    return-void
.end method

.method public static synthetic a(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2466789
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/Long;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 2466790
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x14

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2466791
    const v0, 0x7f08163b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2466792
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 2466793
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    if-nez v0, :cond_1

    .line 2466794
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->j:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2466795
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->j:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2466796
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->j:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-direct {v0, p1}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->a(I)V

    .line 2466797
    :cond_0
    :goto_0
    return-void

    .line 2466798
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->h:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    if-eqz v0, :cond_0

    .line 2466799
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->h:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    .line 2466800
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 2466801
    sget-object v0, LX/HSF;->PAGE:LX/HSF;

    invoke-virtual {v0}, LX/HSF;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 2466802
    sget-object v0, LX/9XH;->EVENT_FB4A_VISIT_PAGE_TAB:LX/9XH;

    .line 2466803
    :goto_1
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->a:LX/9XE;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget-wide v2, v2, LX/HSE;->a:J

    invoke-virtual {v1, v0, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    goto :goto_0

    .line 2466804
    :cond_2
    sget-object v0, LX/HSF;->ACTIVITY:LX/HSF;

    invoke-virtual {v0}, LX/HSF;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_3

    .line 2466805
    sget-object v0, LX/9XH;->EVENT_FB4A_VISIT_ACTIVITY_TAB:LX/9XH;

    goto :goto_1

    .line 2466806
    :cond_3
    sget-object v0, LX/HSF;->INSIGHTS:LX/HSF;

    invoke-virtual {v0}, LX/HSF;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2466807
    sget-object v0, LX/9XH;->EVENT_FB4A_VISIT_INSIGHTS_TAB:LX/9XH;

    goto :goto_1
.end method

.method private static a(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;LX/9XE;LX/8EC;LX/HDT;)V
    .locals 0

    .prologue
    .line 2466808
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->a:LX/9XE;

    iput-object p2, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->b:LX/8EC;

    iput-object p3, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->c:LX/HDT;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-static {v2}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v0

    check-cast v0, LX/9XE;

    invoke-static {v2}, LX/8EC;->a(LX/0QB;)LX/8EC;

    move-result-object v1

    check-cast v1, LX/8EC;

    invoke-static {v2}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v2

    check-cast v2, LX/HDT;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->a(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;LX/9XE;LX/8EC;LX/HDT;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;LX/HSF;)V
    .locals 3

    .prologue
    .line 2466809
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->h:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    if-nez v0, :cond_0

    .line 2466810
    :goto_0
    return-void

    .line 2466811
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget v0, v0, LX/HSE;->f:I

    .line 2466812
    iget-object v1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    sget-object v2, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->d:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 2466813
    iput v2, v1, LX/HSE;->f:I

    .line 2466814
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget v1, v1, LX/HSE;->f:I

    if-eq v0, v1, :cond_1

    .line 2466815
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget v0, v0, LX/HSE;->e:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->setVisibility(I)V

    .line 2466816
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget v0, v0, LX/HSE;->f:I

    invoke-direct {p0, v0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->a(I)V

    .line 2466817
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->h:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {p1}, LX/HSF;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2466818
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    .line 2466819
    iput-object p1, v0, LX/HSE;->g:LX/HSF;

    .line 2466820
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->b:LX/8EC;

    .line 2466821
    iget-object v1, v0, LX/8EC;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, p0, :cond_3

    iget-object v1, v0, LX/8EC;->c:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pq;

    .line 2466822
    iget-object p1, v0, LX/8EC;->a:LX/11i;

    invoke-interface {p1, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object p1

    .line 2466823
    if-eqz p1, :cond_2

    .line 2466824
    iget-object p1, v0, LX/8EC;->a:LX/11i;

    invoke-interface {p1, v1}, LX/11i;->d(LX/0Pq;)V

    .line 2466825
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2466826
    :cond_3
    goto :goto_0
.end method

.method private b(LX/HSF;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 2466695
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget-object v0, v0, LX/HSE;->g:LX/HSF;

    if-ne v0, p1, :cond_0

    const v0, 0x7f0800c5

    .line 2466696
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, p1, LX/HSF;->tabTextString:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->d:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->d:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2466697
    :cond_0
    const v0, 0x7f0800c6

    goto :goto_0
.end method

.method public static synthetic b(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;LX/HSF;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2466828
    invoke-direct {p0, p1}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->b(LX/HSF;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2466759
    const v0, 0x7f030e56

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2466760
    const-class v0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2466761
    new-instance v0, LX/HSE;

    invoke-direct {v0}, LX/HSE;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    .line 2466762
    const v0, 0x7f0d22f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->f:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2466763
    const v0, 0x7f0d22fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->g:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2466764
    sget-object v0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSF;

    .line 2466765
    iget v2, v0, LX/HSF;->tabButtonId:I

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v2

    .line 2466766
    iget v3, v0, LX/HSF;->tabTextViewId:I

    invoke-virtual {p0, v3}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    .line 2466767
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2466768
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2466769
    invoke-direct {p0, v0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->b(LX/HSF;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2466770
    :cond_0
    const v0, 0x7f0d22fb

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/view/ControlledView;

    .line 2466771
    iget-object v1, v0, Lcom/facebook/view/ControlledView;->a:Lcom/facebook/view/ViewController;

    move-object v0, v1

    .line 2466772
    check-cast v0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->e:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    .line 2466773
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->e:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    new-instance v1, LX/HSC;

    invoke-direct {v1, p0}, LX/HSC;-><init>(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;)V

    .line 2466774
    iput-object v1, v0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->c:LX/0hc;

    .line 2466775
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->i:Landroid/os/Handler;

    .line 2466776
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/facebook/fbui/widget/text/BadgeTextView;J)V
    .locals 2

    .prologue
    .line 2466691
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    .line 2466692
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2466693
    :goto_0
    return-void

    .line 2466694
    :cond_0
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->a(Landroid/content/Context;Ljava/lang/Long;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 2466698
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    .line 2466699
    iput-wide p1, v0, LX/HSE;->a:J

    .line 2466700
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    .line 2466701
    iput-object p3, v0, LX/HSE;->b:Ljava/lang/String;

    .line 2466702
    return-void
.end method

.method public final fe_()V
    .locals 2

    .prologue
    .line 2466703
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->fe_()V

    .line 2466704
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->c:LX/HDT;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->n:LX/HDc;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2466705
    return-void
.end method

.method public final ff_()V
    .locals 2

    .prologue
    .line 2466706
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->ff_()V

    .line 2466707
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->c:LX/HDT;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->n:LX/HDc;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2466708
    return-void
.end method

.method public getActivityBadgeCount()J
    .locals 2

    .prologue
    .line 2466709
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget-wide v0, v0, LX/HSE;->c:J

    return-wide v0
.end method

.method public getInsightsBadgeCount()J
    .locals 2

    .prologue
    .line 2466710
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget-wide v0, v0, LX/HSE;->d:J

    return-wide v0
.end method

.method public setActivityBadgeCount(J)V
    .locals 6

    .prologue
    .line 2466711
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    .line 2466712
    iput-wide p1, v0, LX/HSE;->c:J

    .line 2466713
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->i:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView$4;-><init>(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;J)V

    const-wide/16 v2, 0x64

    const v4, 0x27e65071

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2466714
    return-void
.end method

.method public setInsightsBadgeCount(J)V
    .locals 6

    .prologue
    .line 2466715
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    .line 2466716
    iput-wide p1, v0, LX/HSE;->d:J

    .line 2466717
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->i:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView$5;-><init>(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;J)V

    const-wide/16 v2, 0x64

    const v4, -0x5d8af9db

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2466718
    return-void
.end method

.method public setPrimaryTabsView(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;)V
    .locals 2

    .prologue
    .line 2466719
    if-eqz p1, :cond_0

    .line 2466720
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->j:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    .line 2466721
    iput-object p0, p1, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->j:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    .line 2466722
    iget-object v0, p1, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    .line 2466723
    iget-object v0, p1, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->h:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->h:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    .line 2466724
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->e:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iget-object v1, p1, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->h:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->a(Landroid/support/v4/view/ViewPager;)V

    .line 2466725
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    .line 2466726
    :cond_0
    return-void
.end method

.method public setViewPager(Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;)V
    .locals 2

    .prologue
    .line 2466727
    iput-object p1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->h:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    .line 2466728
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->e:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->h:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->a(Landroid/support/v4/view/ViewPager;)V

    .line 2466729
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->h:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    new-instance v1, LX/HSD;

    invoke-direct {v1, p0}, LX/HSD;-><init>(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2466730
    return-void
.end method

.method public setViewVisibility(I)V
    .locals 0

    .prologue
    .line 2466731
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 2466732
    return-void
.end method

.method public setVisibility(I)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 2466733
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    .line 2466734
    iput p1, v3, LX/HSE;->e:I

    .line 2466735
    iget-object v3, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->j:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    if-nez v3, :cond_0

    .line 2466736
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 2466737
    :goto_0
    return-void

    .line 2466738
    :cond_0
    iget-boolean v4, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    iget-object v3, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->j:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    iget-boolean v3, v3, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    if-nez v3, :cond_2

    move v3, v0

    :goto_1
    if-ne v4, v3, :cond_3

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2466739
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget v0, v0, LX/HSE;->f:I

    if-nez v0, :cond_5

    .line 2466740
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    if-eqz v0, :cond_4

    move v0, v2

    .line 2466741
    :goto_3
    invoke-super {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 2466742
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->j:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    iget-boolean v1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget v2, v1, LX/HSE;->e:I

    .line 2466743
    :cond_1
    invoke-super {v0, v2}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 2466744
    goto :goto_0

    :cond_2
    move v3, v1

    .line 2466745
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 2466746
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget v0, v0, LX/HSE;->e:I

    goto :goto_3

    .line 2466747
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    if-nez v0, :cond_7

    move v0, v2

    .line 2466748
    :goto_4
    invoke-super {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 2466749
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->j:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    iget-boolean v1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->l:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget v2, v1, LX/HSE;->e:I

    .line 2466750
    :cond_6
    invoke-super {v0, v2}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 2466751
    goto :goto_0

    .line 2466752
    :cond_7
    iget-object v0, p0, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget v0, v0, LX/HSE;->e:I

    goto :goto_4
.end method
