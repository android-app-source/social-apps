.class public Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Landroid/widget/ListView;

.field public g:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public h:J

.field public i:J

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLInterfaces$VideoDetailFragment;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field private n:LX/HUr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2473942
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2473943
    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->k:Ljava/lang/String;

    .line 2473944
    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->l:Ljava/lang/String;

    .line 2473945
    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->m:Ljava/lang/String;

    .line 2473946
    return-void
.end method

.method public static b$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V
    .locals 6

    .prologue
    .line 2473937
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2473938
    if-eqz v0, :cond_0

    .line 2473939
    invoke-static {p0}, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->c(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)Ljava/lang/String;

    move-result-object v1

    .line 2473940
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0817df

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2473941
    :cond_0
    return-void
.end method

.method public static c(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2473936
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->k:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0817e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static l(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V
    .locals 2

    .prologue
    .line 2473932
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2473933
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 2473934
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->n:LX/HUr;

    const v1, -0x7c7d6ccf

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2473935
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 2473947
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2473948
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v3, p0

    check-cast v3, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v1}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object p1

    check-cast p1, LX/17W;

    invoke-static {v1}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v1

    check-cast v1, LX/9XE;

    iput-object v4, v3, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->a:LX/1Ck;

    iput-object v5, v3, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->b:LX/03V;

    iput-object v6, v3, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->c:LX/0tX;

    iput-object p1, v3, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->d:LX/17W;

    iput-object v1, v3, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->e:LX/9XE;

    .line 2473949
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 2473950
    :goto_0
    return-void

    .line 2473951
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f0e06ea

    invoke-virtual {v1, v2, v0}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 2473952
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2473953
    if-eqz v1, :cond_1

    :goto_1
    const-string v1, "The arguments for the fragment should have a long value for user id which is missing"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2473954
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2473955
    const-string v1, "video_list_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->h:J

    .line 2473956
    const-string v1, "page_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->i:J

    .line 2473957
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->j:Ljava/util/List;

    goto :goto_0

    .line 2473958
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x60cf7b72

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2473911
    const v0, 0x7f030eee

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2473912
    const v0, 0x7f0d2461

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->f:Landroid/widget/ListView;

    .line 2473913
    const v0, 0x7f0d2462

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2473914
    new-instance v0, LX/HUr;

    invoke-direct {v0, p0}, LX/HUr;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->n:LX/HUr;

    .line 2473915
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->f:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->n:LX/HUr;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2473916
    const/16 v0, 0x2b

    const v3, 0x776d915a

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6161be32

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2473917
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2473918
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->a:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2473919
    const/16 v1, 0x2b

    const v2, 0x70ed4a64

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3a5c5b59

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2473920
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2473921
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2473922
    invoke-static {p0}, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->l(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V

    .line 2473923
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x7ed87b1e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2473924
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->j:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2473925
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->g:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2473926
    new-instance v1, LX/HUn;

    invoke-direct {v1, p0}, LX/HUn;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V

    .line 2473927
    iget-object v2, p0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->a:LX/1Ck;

    sget-object v4, LX/HUp;->FETCH_VIDEO_LISTS_WITH_VIDEOS:LX/HUp;

    new-instance v5, LX/HUo;

    invoke-direct {v5, p0}, LX/HUo;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V

    invoke-virtual {v2, v4, v1, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2473928
    goto :goto_0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3317604

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2473929
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2473930
    invoke-static {p0}, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->b$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V

    .line 2473931
    const/16 v1, 0x2b

    const v2, 0x560c9c5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
