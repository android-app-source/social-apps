.class public Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/GZf;


# static fields
.field public static final J:[I


# instance fields
.field public A:Z

.field private B:LX/E8s;

.field private C:LX/E8t;

.field public D:Landroid/view/View;

.field public E:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field public F:Z

.field public G:I

.field private H:I

.field private I:I

.field public K:Z

.field public L:I

.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/intent_builder/IPageIdentityIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1ay;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0rq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0se;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Landroid/content/Context;

.field private n:Landroid/widget/FrameLayout;

.field public o:Landroid/widget/ListView;

.field public p:Landroid/view/LayoutInflater;

.field public q:J

.field public r:Ljava/lang/String;

.field public s:Z

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;",
            ">;"
        }
    .end annotation
.end field

.field public u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLInterfaces$VideoDetailFragment;",
            ">;"
        }
    .end annotation
.end field

.field public v:I

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field private y:Z

.field public z:LX/HUl;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2473745
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->J:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 2473746
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2473747
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->s:Z

    .line 2473748
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->A:Z

    .line 2473749
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->F:Z

    .line 2473750
    iput v2, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->I:I

    .line 2473751
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->K:Z

    .line 2473752
    iput v2, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->L:I

    .line 2473753
    return-void
.end method

.method public static a(JZZ)Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;
    .locals 4

    .prologue
    .line 2473754
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2473755
    new-instance v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-direct {v0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;-><init>()V

    .line 2473756
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2473757
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2473758
    const-string v2, "extra_force_all_videos"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2473759
    const-string v2, "extra_is_inside_page_surface_tab"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2473760
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2473761
    return-object v0

    .line 2473762
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v1, p1

    check-cast v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-static {v13}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {v13}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v13}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v13}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v5

    check-cast v5, LX/9XE;

    invoke-static {v13}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v6

    check-cast v6, LX/0hB;

    const/16 v7, 0x2c1f

    invoke-static {v13, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xf07

    invoke-static {v13, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xbde

    invoke-static {v13, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x12c4

    invoke-static {v13, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v13}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v11

    check-cast v11, LX/0rq;

    invoke-static {v13}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v12

    check-cast v12, LX/0se;

    const/16 p0, 0x1430

    invoke-static {v13, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    iput-object v2, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a:LX/1Ck;

    iput-object v3, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b:LX/03V;

    iput-object v4, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->c:LX/0tX;

    iput-object v5, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->d:LX/9XE;

    iput-object v6, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->e:LX/0hB;

    iput-object v7, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->f:LX/0Ot;

    iput-object v8, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->g:LX/0Ot;

    iput-object v9, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->h:LX/0Ot;

    iput-object v10, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->i:LX/0Ot;

    iput-object v11, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->j:LX/0rq;

    iput-object v12, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->k:LX/0se;

    iput-object v13, v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->l:LX/0Ot;

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2473763
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->A:Z

    if-eqz v0, :cond_3

    .line 2473764
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    .line 2473765
    :cond_0
    invoke-static {p0, v1}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->d(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    .line 2473766
    :goto_0
    return-void

    .line 2473767
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    .line 2473768
    iget-object v1, v0, LX/HUl;->b:LX/1Cv;

    move-object v0, v1

    .line 2473769
    instance-of v0, v0, LX/HUj;

    if-nez v0, :cond_2

    .line 2473770
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    new-instance v1, LX/HUj;

    invoke-direct {v1, p0}, LX/HUj;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    invoke-virtual {v0, v1}, LX/HUl;->a(LX/1Cv;)V

    .line 2473771
    :cond_2
    invoke-static {p0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->l(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    goto :goto_0

    .line 2473772
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    if-eqz p1, :cond_5

    .line 2473773
    :cond_4
    invoke-static {p0, v1}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->e$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    goto :goto_0

    .line 2473774
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    .line 2473775
    iget-object v1, v0, LX/HUl;->b:LX/1Cv;

    move-object v0, v1

    .line 2473776
    instance-of v0, v0, LX/HUh;

    if-nez v0, :cond_6

    .line 2473777
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    new-instance v1, LX/HUh;

    invoke-direct {v1, p0}, LX/HUh;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    invoke-virtual {v0, v1}, LX/HUl;->a(LX/1Cv;)V

    .line 2473778
    :cond_6
    invoke-static {p0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->l(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)V
    .locals 1

    .prologue
    .line 2473694
    if-eqz p1, :cond_0

    .line 2473695
    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->x:Ljava/lang/String;

    .line 2473696
    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->y:Z

    .line 2473697
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V
    .locals 2

    .prologue
    .line 2473779
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    .line 2473780
    iget-boolean v1, v0, LX/HUl;->c:Z

    if-eq v1, p1, :cond_1

    const/4 v1, 0x1

    .line 2473781
    :goto_0
    iput-boolean p1, v0, LX/HUl;->c:Z

    .line 2473782
    move v0, v1

    .line 2473783
    if-eqz v0, :cond_0

    .line 2473784
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    new-instance v1, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment$4;

    invoke-direct {v1, p0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment$4;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 2473785
    :cond_0
    return-void

    .line 2473786
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2473787
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2473788
    iget-object v3, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->u:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2473789
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2473790
    :cond_0
    invoke-static {p0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->l(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    .line 2473791
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2473792
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->B:LX/E8s;

    if-nez v0, :cond_2

    .line 2473793
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->C:LX/E8t;

    if-nez v0, :cond_1

    .line 2473794
    new-instance v0, LX/E8s;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->B:LX/E8s;

    .line 2473795
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->B:LX/E8s;

    const/4 v1, 0x1

    .line 2473796
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2473797
    :cond_0
    :goto_1
    return-void

    .line 2473798
    :cond_1
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->C:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->B:LX/E8s;

    .line 2473799
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->C:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->B:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    goto :goto_0

    .line 2473800
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->C:LX/E8t;

    if-eqz v0, :cond_0

    .line 2473801
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->B:LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->C:LX/E8t;

    .line 2473802
    iput-object v1, v0, LX/E8s;->a:Landroid/view/View;

    .line 2473803
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->C:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->B:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2473804
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->B:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->requestLayout()V

    goto :goto_1
.end method

.method public static d(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2473805
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->y:Z

    if-nez v0, :cond_0

    .line 2473806
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    .line 2473807
    :goto_0
    return-void

    .line 2473808
    :cond_0
    invoke-static {p0, v1}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    .line 2473809
    if-eqz p1, :cond_1

    .line 2473810
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->y:Z

    .line 2473811
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->x:Ljava/lang/String;

    .line 2473812
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2473813
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2473814
    :cond_1
    new-instance v0, LX/HUd;

    invoke-direct {v0, p0, p1}, LX/HUd;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    .line 2473815
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a:LX/1Ck;

    sget-object v2, LX/HUi;->FETCH_VIDEO_LISTS_WITH_VIDEOS:LX/HUi;

    new-instance v3, LX/HUe;

    invoke-direct {v3, p0}, LX/HUe;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public static e$redex0(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2473732
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->y:Z

    if-nez v0, :cond_0

    .line 2473733
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    .line 2473734
    :goto_0
    return-void

    .line 2473735
    :cond_0
    invoke-static {p0, v1}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->b(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    .line 2473736
    if-eqz p1, :cond_1

    .line 2473737
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->y:Z

    .line 2473738
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->x:Ljava/lang/String;

    .line 2473739
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2473740
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2473741
    :cond_1
    new-instance v0, LX/HUf;

    invoke-direct {v0, p0}, LX/HUf;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    .line 2473742
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a:LX/1Ck;

    sget-object v2, LX/HUi;->FETCH_ALL_VIDEOS:LX/HUi;

    new-instance v3, LX/HUg;

    invoke-direct {v3, p0}, LX/HUg;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public static l(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V
    .locals 2

    .prologue
    .line 2473743
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    const v1, 0x5ffda87d

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2473744
    return-void
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2473663
    iget v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->I:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->I:I

    if-le p1, v0, :cond_1

    .line 2473664
    :cond_0
    :goto_0
    return-void

    .line 2473665
    :cond_1
    iput p1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->I:I

    .line 2473666
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->D:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2473667
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->D:Landroid/view/View;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->I:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2473668
    const-string v0, "page_video_fragment"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2473669
    iput p1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->H:I

    .line 2473670
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    iget v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->H:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2473671
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2473672
    iput-object p1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->C:LX/E8t;

    .line 2473673
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->d()V

    .line 2473674
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2473675
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2473676
    const-class v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2473677
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 2473678
    if-eqz v0, :cond_0

    .line 2473679
    invoke-virtual {v0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v3, 0x7f0e06ea

    invoke-virtual {v0, v3, v1}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 2473680
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2473681
    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "The arguments for the fragment should have a long value for user id which is missing"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2473682
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2473683
    const-string v3, "com.facebook.katana.profile.id"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->q:J

    .line 2473684
    const-string v3, "extra_force_all_videos"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2473685
    iput-boolean v2, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->A:Z

    .line 2473686
    :cond_1
    const-string v3, "extra_is_inside_page_surface_tab"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->F:Z

    .line 2473687
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->t:Ljava/util/List;

    .line 2473688
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->u:Ljava/util/List;

    .line 2473689
    iput-boolean v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->y:Z

    .line 2473690
    return-void

    :cond_2
    move v0, v2

    .line 2473691
    goto :goto_0
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2473692
    iput-object p1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->E:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2473693
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2473698
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a(Z)V

    .line 2473699
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1f2b2df9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2473700
    iput-object p1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->p:Landroid/view/LayoutInflater;

    .line 2473701
    const v1, 0x7f030eef

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x3f714566

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4146f64b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2473702
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2473703
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2473704
    const/16 v1, 0x2b

    const v2, -0x43c9092f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xec67992

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2473705
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2473706
    iget-boolean v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->F:Z

    if-eqz v1, :cond_1

    .line 2473707
    :cond_0
    :goto_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a(Z)V

    .line 2473708
    const/16 v1, 0x2b

    const v2, -0x43ed5c7c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2473709
    :cond_1
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2473710
    if-eqz v1, :cond_0

    .line 2473711
    const v2, 0x7f0817de

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2473712
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2473713
    const v0, 0x7f0d245e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->n:Landroid/widget/FrameLayout;

    .line 2473714
    const v0, 0x7f0d2463

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    .line 2473715
    new-instance v0, LX/HUl;

    invoke-direct {v0, p0}, LX/HUl;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    .line 2473716
    iget-boolean v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->F:Z

    if-eqz v0, :cond_1

    .line 2473717
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->d()V

    .line 2473718
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->B:LX/E8s;

    if-eqz v0, :cond_0

    .line 2473719
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->B:LX/E8s;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 2473720
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 2473721
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 2473722
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    const v2, 0x7f0a004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2473723
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    const v2, 0x7f0b0062

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 2473724
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->D:Landroid/view/View;

    .line 2473725
    iget v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->I:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->E_(I)V

    .line 2473726
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->D:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 2473727
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    new-instance v1, LX/HUa;

    invoke-direct {v1, p0}, LX/HUa;-><init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2473728
    iget v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->H:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->a(I)V

    .line 2473729
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->o:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->z:LX/HUl;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2473730
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->m:Landroid/content/Context;

    .line 2473731
    return-void
.end method
