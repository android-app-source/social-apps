.class public Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/D3w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:Ljava/lang/String;

.field public final g:Lcom/facebook/widget/text/BetterTextView;

.field public final h:Lcom/facebook/widget/text/BetterTextView;

.field public final i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final j:Lcom/facebook/widget/text/BetterTextView;

.field public final k:Lcom/facebook/widget/text/BetterTextView;

.field public l:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2474716
    const-class v0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2474717
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2474718
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2474719
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2474720
    const-class v0, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2474721
    const v0, 0x7f0315ae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2474722
    const v0, 0x7f081809

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->f:Ljava/lang/String;

    .line 2474723
    const v0, 0x7f0d19be

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2474724
    const v0, 0x7f0d30ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2474725
    const v0, 0x7f0d30ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2474726
    const v0, 0x7f0d30e3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 2474727
    const v0, 0x7f0d30ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 2474728
    new-instance v0, LX/HV6;

    invoke-direct {v0, p0}, LX/HV6;-><init>(Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(LX/0b2;)Z

    .line 2474729
    return-void
.end method

.method public static a(LX/174;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2474730
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;

    const/16 v1, 0x509

    invoke-static {v3, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v3}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v1

    check-cast v1, LX/154;

    invoke-static {v3}, LX/D3w;->b(LX/0QB;)LX/D3w;

    move-result-object v2

    check-cast v2, LX/D3w;

    invoke-static {v3}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v3

    check-cast v3, LX/0bH;

    iput-object p0, p1, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->a:LX/0Or;

    iput-object v1, p1, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->b:LX/154;

    iput-object v2, p1, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->c:LX/D3w;

    iput-object v3, p1, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->d:LX/0bH;

    return-void
.end method


# virtual methods
.method public bridge synthetic getEventBus()LX/0b4;
    .locals 1

    .prologue
    .line 2474731
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->d:LX/0bH;

    move-object v0, v0

    .line 2474732
    return-object v0
.end method
