.class public Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/154;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/D3w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:Ljava/lang/String;

.field private final h:Lcom/facebook/resources/ui/FbTextView;

.field private final i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final j:Lcom/facebook/resources/ui/FbTextView;

.field private final k:LX/HVB;

.field private l:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2474845
    const-class v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2474843
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2474844
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2474833
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2474834
    new-instance v0, LX/HVB;

    invoke-direct {v0, p0}, LX/HVB;-><init>(Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;)V

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->k:LX/HVB;

    .line 2474835
    const-class v0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;

    invoke-static {v0, p0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2474836
    const v0, 0x7f0315ab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2474837
    invoke-virtual {p0, v1}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->setOrientation(I)V

    .line 2474838
    const v0, 0x7f081809

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->g:Ljava/lang/String;

    .line 2474839
    const v0, 0x7f0d30e4

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2474840
    const v0, 0x7f0d30e1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2474841
    const v0, 0x7f0d30e3

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2474842
    return-void
.end method

.method private static a(LX/174;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2474832
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;LX/0Or;LX/154;LX/D3w;LX/0bH;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/154;",
            "LX/D3w;",
            "LX/0bH;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2474831
    iput-object p1, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->b:LX/154;

    iput-object p3, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->c:LX/D3w;

    iput-object p4, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->d:LX/0bH;

    iput-object p5, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->e:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;

    const/16 v1, 0x509

    invoke-static {v5, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v5}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v2

    check-cast v2, LX/154;

    invoke-static {v5}, LX/D3w;->b(LX/0QB;)LX/D3w;

    move-result-object v3

    check-cast v3, LX/D3w;

    invoke-static {v5}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    const/16 v6, 0x19e

    invoke-static {v5, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->a(Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;LX/0Or;LX/154;LX/D3w;LX/0bH;LX/0Or;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2474829
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->l:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    invoke-static {v0, p1, p2}, LX/HVE;->a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;Ljava/lang/String;Z)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->l:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2474830
    return-void
.end method

.method public static b(Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V
    .locals 0

    .prologue
    .line 2474824
    iput-object p1, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->l:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2474825
    invoke-direct {p0, p1}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->setVideoPreviewImage(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V

    .line 2474826
    invoke-direct {p0, p1}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->setVideoPreviewMeta(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V

    .line 2474827
    invoke-direct {p0, p1}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->setVideoPreviewStats(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V

    .line 2474828
    return-void
.end method

.method private c(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2474771
    const/16 v0, 0x3e8

    if-ge p1, v0, :cond_0

    .line 2474772
    const-string v0, "%,d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2474773
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->b:LX/154;

    invoke-virtual {v0, p1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setVideoPreviewImage(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V
    .locals 3

    .prologue
    .line 2474813
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x3fe38e39

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2474814
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2474815
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2474816
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2474817
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 2474818
    if-eqz v0, :cond_1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 2474819
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2474820
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2474821
    new-instance v0, LX/HVA;

    invoke-direct {v0, p0, p1}, LX/HVA;-><init>(Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V

    invoke-virtual {p0, v0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2474822
    :cond_0
    return-void

    .line 2474823
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method

.method private setVideoPreviewMeta(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V
    .locals 3

    .prologue
    .line 2474805
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->K()LX/174;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->a(LX/174;)Ljava/lang/String;

    move-result-object v0

    .line 2474806
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->v()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->a(LX/174;)Ljava/lang/String;

    move-result-object v1

    .line 2474807
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2474808
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2474809
    :goto_0
    return-void

    .line 2474810
    :cond_0
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2474811
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2474812
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->j:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setVideoPreviewStats(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 2474786
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2474787
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->bV_()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->bV_()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->d()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2474788
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->x()I

    move-result v3

    .line 2474789
    invoke-virtual {p1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->bV_()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->d()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    move-result-object v0

    .line 2474790
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;->a()I

    move-result v0

    .line 2474791
    :goto_0
    const v4, 0x7f0f00b5

    new-array v5, v7, [Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->c(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v2, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2474792
    const v5, 0x7f0f00b1

    new-array v6, v7, [Ljava/lang/Object;

    invoke-direct {p0, v3}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->c(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v2, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2474793
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 2474794
    if-lez v0, :cond_3

    if-lez v3, :cond_3

    .line 2474795
    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2474796
    const-string v0, " \u2022 "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2474797
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2474798
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2474799
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 2474800
    goto :goto_0

    .line 2474801
    :cond_3
    if-lez v0, :cond_4

    .line 2474802
    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 2474803
    :cond_4
    if-lez v3, :cond_0

    .line 2474804
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x566868ec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2474783
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 2474784
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->d:LX/0bH;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->k:LX/HVB;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2474785
    const/16 v1, 0x2d

    const v2, 0x2c52b257

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2f150809

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2474780
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 2474781
    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->d:LX/0bH;

    iget-object v2, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->k:LX/HVB;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2474782
    const/16 v1, 0x2d

    const v2, -0x5a4fb56d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 2

    .prologue
    .line 2474777
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onFinishTemporaryDetach()V

    .line 2474778
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->d:LX/0bH;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->k:LX/HVB;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2474779
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 2

    .prologue
    .line 2474774
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onStartTemporaryDetach()V

    .line 2474775
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->d:LX/0bH;

    iget-object v1, p0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->k:LX/HVB;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2474776
    return-void
.end method
