.class public final Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2474132
    const-class v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    new-instance v1, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2474133
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2474131
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2474096
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2474097
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 2474098
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2474099
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2474100
    if-eqz v2, :cond_0

    .line 2474101
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2474102
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2474103
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2474104
    if-eqz v2, :cond_1

    .line 2474105
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2474106
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2474107
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2474108
    if-eqz v2, :cond_3

    .line 2474109
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2474110
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2474111
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2474112
    if-eqz v3, :cond_2

    .line 2474113
    const-string p0, "uri"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2474114
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2474115
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2474116
    :cond_3
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2474117
    if-eqz v2, :cond_4

    .line 2474118
    const-string v3, "uploaded_videos"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2474119
    invoke-static {v1, v2, p1, p2}, LX/HUx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2474120
    :cond_4
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2474121
    if-eqz v2, :cond_5

    .line 2474122
    const-string v3, "video_collection"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2474123
    invoke-static {v1, v2, p1, p2}, LX/HV1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2474124
    :cond_5
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2474125
    if-eqz v2, :cond_6

    .line 2474126
    const-string v2, "viewer_profile_permissions"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2474127
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2474128
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2474129
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2474130
    check-cast p1, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$Serializer;->a(Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
