.class public final Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3241e7f9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2474258
    const-class v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2474257
    const-class v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2474255
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2474256
    return-void
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2474252
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2474253
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2474254
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2474236
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2474237
    invoke-direct {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2474238
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2474239
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x2a46c029

    invoke-static {v3, v2, v4}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2474240
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2474241
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2474242
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->m()LX/0Px;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/util/List;)I

    move-result v5

    .line 2474243
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2474244
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2474245
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2474246
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2474247
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2474248
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2474249
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2474250
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2474251
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2474216
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2474217
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2474218
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2a46c029

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2474219
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2474220
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    .line 2474221
    iput v3, v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->g:I

    move-object v1, v0

    .line 2474222
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2474223
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    .line 2474224
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2474225
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    .line 2474226
    iput-object v0, v1, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->h:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    .line 2474227
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2474228
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    .line 2474229
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2474230
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    .line 2474231
    iput-object v0, v1, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->i:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    .line 2474232
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2474233
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2474234
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2474235
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2474215
    new-instance v0, LX/HUv;

    invoke-direct {v0, p1}, LX/HUv;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2474213
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->f:Ljava/lang/String;

    .line 2474214
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2474210
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2474211
    const/4 v0, 0x2

    const v1, 0x2a46c029

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->g:I

    .line 2474212
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2474194
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2474195
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2474209
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2474206
    new-instance v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;-><init>()V

    .line 2474207
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2474208
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2474205
    const v0, 0x2c83c30a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2474204
    const v0, 0x252222

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2474202
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2474203
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2474200
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->h:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->h:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    .line 2474201
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->h:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$UploadedVideosModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2474198
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->i:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->i:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    .line 2474199
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->i:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$VideoCollectionFragmentModel;

    return-object v0
.end method

.method public final m()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2474196
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->j:Ljava/util/List;

    .line 2474197
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
