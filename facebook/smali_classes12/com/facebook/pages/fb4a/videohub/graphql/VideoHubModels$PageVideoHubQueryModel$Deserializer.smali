.class public final Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2474039
    const-class v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    new-instance v1, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2474040
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2474041
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2474042
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2474043
    const/4 v2, 0x0

    .line 2474044
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_a

    .line 2474045
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2474046
    :goto_0
    move v1, v2

    .line 2474047
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2474048
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2474049
    new-instance v1, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoHubQueryModel;-><init>()V

    .line 2474050
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2474051
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2474052
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2474053
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2474054
    :cond_0
    return-object v1

    .line 2474055
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2474056
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_9

    .line 2474057
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2474058
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2474059
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, p0, :cond_2

    if-eqz v8, :cond_2

    .line 2474060
    const-string v9, "__type__"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "__typename"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2474061
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    goto :goto_1

    .line 2474062
    :cond_4
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2474063
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2474064
    :cond_5
    const-string v9, "profile_picture"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 2474065
    const/4 v8, 0x0

    .line 2474066
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v9, :cond_e

    .line 2474067
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2474068
    :goto_2
    move v5, v8

    .line 2474069
    goto :goto_1

    .line 2474070
    :cond_6
    const-string v9, "uploaded_videos"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 2474071
    invoke-static {p1, v0}, LX/HUx;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2474072
    :cond_7
    const-string v9, "video_collection"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 2474073
    invoke-static {p1, v0}, LX/HV1;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2474074
    :cond_8
    const-string v9, "viewer_profile_permissions"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2474075
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 2474076
    :cond_9
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2474077
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2474078
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2474079
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2474080
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2474081
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2474082
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2474083
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto/16 :goto_1

    .line 2474084
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2474085
    :cond_c
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_d

    .line 2474086
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2474087
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2474088
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_c

    if-eqz v9, :cond_c

    .line 2474089
    const-string p0, "uri"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 2474090
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 2474091
    :cond_d
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2474092
    invoke-virtual {v0, v8, v5}, LX/186;->b(II)V

    .line 2474093
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto/16 :goto_2

    :cond_e
    move v5, v8

    goto :goto_3
.end method
