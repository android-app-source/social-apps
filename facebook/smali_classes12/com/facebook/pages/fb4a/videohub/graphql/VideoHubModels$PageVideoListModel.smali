.class public final Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2b216419
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2474376
    const-class v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2474375
    const-class v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2474373
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2474374
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2474361
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2474362
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2474363
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2474364
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2474365
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2474366
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2474367
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2474368
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2474369
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2474370
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2474371
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2474372
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2474353
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2474354
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2474355
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    .line 2474356
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2474357
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;

    .line 2474358
    iput-object v0, v1, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->h:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    .line 2474359
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2474360
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2474352
    invoke-virtual {p0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2474349
    new-instance v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;

    invoke-direct {v0}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;-><init>()V

    .line 2474350
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2474351
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2474348
    const v0, -0x22041c1b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2474339
    const v0, -0x391e5c67

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2474346
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->e:Ljava/lang/String;

    .line 2474347
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2474344
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->f:Ljava/lang/String;

    .line 2474345
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2474342
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->g:Ljava/lang/String;

    .line 2474343
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2474340
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->h:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    iput-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->h:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    .line 2474341
    iget-object v0, p0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->h:Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    return-object v0
.end method
