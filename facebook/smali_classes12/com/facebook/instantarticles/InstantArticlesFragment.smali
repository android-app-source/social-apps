.class public Lcom/facebook/instantarticles/InstantArticlesFragment;
.super Lcom/facebook/richdocument/RichDocumentFragment;
.source ""


# instance fields
.field public n:LX/Chr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/CsO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/8bL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

.field public t:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

.field private final u:LX/Chx;

.field private v:LX/IXr;

.field private w:Lcom/facebook/instantarticles/view/ShareBar;

.field private x:Landroid/support/v7/widget/RecyclerView;

.field public y:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2585776
    invoke-direct {p0}, Lcom/facebook/richdocument/RichDocumentFragment;-><init>()V

    .line 2585777
    new-instance v0, LX/IXD;

    invoke-direct {v0, p0}, LX/IXD;-><init>(Lcom/facebook/instantarticles/InstantArticlesFragment;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->u:LX/Chx;

    return-void
.end method

.method private static a(Lcom/facebook/instantarticles/InstantArticlesFragment;LX/Chr;LX/CsO;LX/Chi;LX/Chv;LX/8bL;)V
    .locals 0

    .prologue
    .line 2585775
    iput-object p1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->n:LX/Chr;

    iput-object p2, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->o:LX/CsO;

    iput-object p3, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->p:LX/Chi;

    iput-object p4, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->q:LX/Chv;

    iput-object p5, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->r:LX/8bL;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/instantarticles/InstantArticlesFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/instantarticles/InstantArticlesFragment;

    invoke-static {v5}, LX/Chr;->a(LX/0QB;)LX/Chr;

    move-result-object v1

    check-cast v1, LX/Chr;

    invoke-static {v5}, LX/CsO;->a(LX/0QB;)LX/CsO;

    move-result-object v2

    check-cast v2, LX/CsO;

    invoke-static {v5}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v3

    check-cast v3, LX/Chi;

    invoke-static {v5}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v4

    check-cast v4, LX/Chv;

    invoke-static {v5}, LX/8bL;->b(LX/0QB;)LX/8bL;

    move-result-object v5

    check-cast v5, LX/8bL;

    invoke-static/range {v0 .. v5}, Lcom/facebook/instantarticles/InstantArticlesFragment;->a(Lcom/facebook/instantarticles/InstantArticlesFragment;LX/Chr;LX/CsO;LX/Chi;LX/Chv;LX/8bL;)V

    return-void
.end method

.method private t()V
    .locals 3

    .prologue
    .line 2585758
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->v:LX/IXr;

    if-nez v0, :cond_1

    .line 2585759
    :cond_0
    :goto_0
    return-void

    .line 2585760
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    if-eqz v0, :cond_0

    .line 2585761
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->w:Lcom/facebook/instantarticles/view/ShareBar;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    iget-object v1, v1, LX/GmZ;->M:LX/Chi;

    .line 2585762
    iput-object v1, v0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2585763
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->v:LX/IXr;

    .line 2585764
    iput-object v1, v0, LX/GmZ;->ac:LX/Csi;

    .line 2585765
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->v:LX/IXr;

    instance-of v0, v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    if-eqz v0, :cond_0

    .line 2585766
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->v:LX/IXr;

    check-cast v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v1}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->S()Landroid/support/v7/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->setRecyclerView(Landroid/support/v7/widget/RecyclerView;)V

    .line 2585767
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->n:LX/Chr;

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    iget-object v0, v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ad:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    .line 2585768
    iget-object v2, v1, LX/Chr;->d:LX/Chv;

    if-eqz v2, :cond_2

    .line 2585769
    iget-object v2, v1, LX/Chr;->d:LX/Chv;

    iget-object p0, v1, LX/Chr;->b:LX/Chm;

    invoke-virtual {v2, p0}, LX/0b4;->b(LX/0b2;)Z

    .line 2585770
    iget-object v2, v1, LX/Chr;->d:LX/Chv;

    iget-object p0, v1, LX/Chr;->c:LX/Cho;

    invoke-virtual {v2, p0}, LX/0b4;->b(LX/0b2;)Z

    .line 2585771
    :cond_2
    iput-object v0, v1, LX/Chr;->d:LX/Chv;

    .line 2585772
    iget-object v2, v1, LX/Chr;->d:LX/Chv;

    iget-object p0, v1, LX/Chr;->b:LX/Chm;

    invoke-virtual {v2, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 2585773
    iget-object v2, v1, LX/Chr;->d:LX/Chv;

    iget-object p0, v1, LX/Chr;->c:LX/Cho;

    invoke-virtual {v2, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 2585774
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2585756
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->x:Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_1

    .line 2585757
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->o:LX/CsO;

    invoke-virtual {v1}, LX/CsO;->a()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->x:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-static {v1, v2, v3}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->d(Landroid/support/v7/widget/RecyclerView;II)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public k()LX/ChL;
    .locals 1

    .prologue
    .line 2585754
    new-instance v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-direct {v0}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    .line 2585755
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2585752
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2585753
    invoke-static {v0}, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->d(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/CqD;
    .locals 1

    .prologue
    .line 2585751
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->t:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    return-object v0
.end method

.method public n()V
    .locals 1

    .prologue
    .line 2585745
    invoke-super {p0}, Lcom/facebook/richdocument/RichDocumentFragment;->n()V

    .line 2585746
    invoke-direct {p0}, Lcom/facebook/instantarticles/InstantArticlesFragment;->t()V

    .line 2585747
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    if-eqz v0, :cond_0

    .line 2585748
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v0}, LX/Chc;->o()V

    .line 2585749
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->y:Z

    .line 2585750
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    .line 2585692
    invoke-super {p0}, Lcom/facebook/richdocument/RichDocumentFragment;->o()V

    .line 2585693
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    if-eqz v0, :cond_0

    .line 2585694
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    const/4 v1, 0x0

    .line 2585695
    iput-object v1, v0, LX/GmZ;->ac:LX/Csi;

    .line 2585696
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v0}, LX/Chc;->s()V

    .line 2585697
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2585725
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/richdocument/RichDocumentFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2585726
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    .line 2585727
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 2585728
    packed-switch p1, :pswitch_data_0

    .line 2585729
    :goto_0
    return-void

    .line 2585730
    :pswitch_0
    const-string v1, "block_media_type"

    const-string p3, "paragraph"

    invoke-interface {p0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585731
    const-string v1, "ia_source"

    const-string p3, "native_article_text_block"

    invoke-interface {p0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585732
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ac:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ckw;

    const-string p3, "feed_share_action"

    invoke-virtual {v1, p2, p3, p0}, LX/Ckw;->a(ILjava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 2585733
    :pswitch_1
    const-string v1, "block_media_type"

    const-string p3, "article"

    invoke-interface {p0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585734
    const-string v1, "ia_source"

    const-string p3, "native_article_text_block"

    invoke-interface {p0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585735
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ac:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ckw;

    const-string p3, "feed_share_action"

    invoke-virtual {v1, p2, p3, p0}, LX/Ckw;->a(ILjava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 2585736
    :pswitch_2
    const-string v1, "block_media_type"

    const-string p3, "article"

    invoke-interface {p0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585737
    const-string v1, "ia_source"

    const-string p3, "share_block"

    invoke-interface {p0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585738
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ac:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ckw;

    const-string p3, "feed_share_action_bottom"

    invoke-virtual {v1, p2, p3, p0}, LX/Ckw;->a(ILjava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 2585739
    :pswitch_3
    const-string v1, "block_media_type"

    const-string p3, "photo_video"

    invoke-interface {p0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585740
    const-string v1, "ia_source"

    const-string p3, "photo_video"

    invoke-interface {p0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585741
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ac:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ckw;

    const-string p3, "article_media_share"

    invoke-virtual {v1, p2, p3, p0}, LX/Ckw;->a(ILjava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    .line 2585742
    :pswitch_4
    const-string v1, "block_media_type"

    const-string p3, "article"

    invoke-interface {p0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585743
    const-string v1, "ia_source"

    const-string p3, "article_ufi_share_button"

    invoke-interface {p0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585744
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;->ac:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ckw;

    const-string p3, "feed_share_action_bottom"

    invoke-virtual {v1, p2, p3, p0}, LX/Ckw;->a(ILjava/lang/String;Ljava/util/Map;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x24c72192

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2585721
    invoke-super {p0, p1}, Lcom/facebook/richdocument/RichDocumentFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2585722
    const-class v1, Lcom/facebook/instantarticles/InstantArticlesFragment;

    invoke-static {v1, p0}, Lcom/facebook/instantarticles/InstantArticlesFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2585723
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v1, p1}, LX/Chc;->a(Landroid/os/Bundle;)V

    .line 2585724
    const/16 v1, 0x2b

    const v2, -0x3dabdf52

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3365f290    # -8.0767872E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2585717
    invoke-super {p0}, Lcom/facebook/richdocument/RichDocumentFragment;->onDestroyView()V

    .line 2585718
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->x:Landroid/support/v7/widget/RecyclerView;

    .line 2585719
    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->q:LX/Chv;

    iget-object v2, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->u:LX/Chx;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2585720
    const/16 v1, 0x2b

    const v2, 0x67aa7257    # 1.609823E24f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2585713
    invoke-super {p0, p1, p2}, Lcom/facebook/richdocument/RichDocumentFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2585714
    const v0, 0x7f0d04e7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->x:Landroid/support/v7/widget/RecyclerView;

    .line 2585715
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->q:LX/Chv;

    iget-object v1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->u:LX/Chx;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2585716
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2585712
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->p:LX/Chi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->p:LX/Chi;

    invoke-virtual {v0}, LX/Chi;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()V
    .locals 1

    .prologue
    .line 2585709
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    if-eqz v0, :cond_0

    .line 2585710
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v0}, LX/Chc;->n()V

    .line 2585711
    :cond_0
    return-void
.end method

.method public final s()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2585703
    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->t:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    .line 2585704
    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->v:LX/IXr;

    .line 2585705
    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->w:Lcom/facebook/instantarticles/view/ShareBar;

    .line 2585706
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    if-eqz v0, :cond_0

    .line 2585707
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->s:Lcom/facebook/instantarticles/InstantArticlesDelegateImpl;

    invoke-virtual {v0}, LX/Chc;->v()V

    .line 2585708
    :cond_0
    return-void
.end method

.method public final setFragmentPager(LX/CqD;)V
    .locals 2

    .prologue
    .line 2585698
    instance-of v0, p1, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    if-nez v0, :cond_0

    .line 2585699
    :goto_0
    return-void

    .line 2585700
    :cond_0
    check-cast p1, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iput-object p1, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->t:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    .line 2585701
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->t:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v0}, LX/CsS;->getHeader()LX/CsR;

    move-result-object v0

    invoke-interface {v0}, LX/CsR;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/IXr;

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->v:LX/IXr;

    .line 2585702
    iget-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->v:LX/IXr;

    const v1, 0x7f0d167d

    invoke-virtual {v0, v1}, LX/IXr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/view/ShareBar;

    iput-object v0, p0, Lcom/facebook/instantarticles/InstantArticlesFragment;->w:Lcom/facebook/instantarticles/view/ShareBar;

    goto :goto_0
.end method
