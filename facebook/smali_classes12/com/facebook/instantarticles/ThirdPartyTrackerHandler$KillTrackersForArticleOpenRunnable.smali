.class public final Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/IXQ;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/IXQ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2585878
    iput-object p1, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->a:LX/IXQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2585879
    iput-object p2, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->b:Ljava/lang/String;

    .line 2585880
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 2585881
    iget-object v0, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->a:LX/IXQ;

    iget-object v0, v0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    if-nez v0, :cond_1

    .line 2585882
    :cond_0
    :goto_0
    return-void

    .line 2585883
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->a:LX/IXQ;

    iget-object v0, v0, LX/IXQ;->o:Ljava/util/Map;

    iget-object v1, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXK;

    .line 2585884
    if-eqz v0, :cond_0

    .line 2585885
    iget-object v1, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->a:LX/IXQ;

    iget-object v2, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/IXQ;->a$redex0(LX/IXQ;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 2585886
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    .line 2585887
    iget-object v3, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->a:LX/IXQ;

    invoke-static {v3, v1}, LX/IXQ;->a$redex0(LX/IXQ;Landroid/webkit/WebView;)V

    goto :goto_1

    .line 2585888
    :cond_2
    iget-boolean v4, v0, LX/IXK;->d:Z

    if-eqz v4, :cond_4

    .line 2585889
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->a:LX/IXQ;

    iget-object v0, v0, LX/IXQ;->n:Ljava/util/Map;

    iget-object v1, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->a:LX/IXQ;

    iget-object v1, v1, LX/IXQ;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585890
    iget-object v0, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->a:LX/IXQ;

    iget-object v0, v0, LX/IXQ;->o:Ljava/util/Map;

    iget-object v1, p0, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;->a:LX/IXQ;

    iget-object v1, v1, LX/IXQ;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2585891
    :cond_4
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/IXK;->d:Z

    .line 2585892
    iget-object v4, v0, LX/IXK;->a:LX/IXQ;

    .line 2585893
    iget-object v5, v4, LX/IXQ;->h:LX/0Uh;

    const/16 v6, 0x3dc

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, LX/0Uh;->a(IZ)Z

    move-result v5

    move v4, v5

    .line 2585894
    if-eqz v4, :cond_3

    .line 2585895
    invoke-static {v0}, LX/IXK;->b(LX/IXK;)Ljava/util/Map;

    move-result-object v4

    .line 2585896
    const-string v5, "total_time_open"

    iget-object v6, v0, LX/IXK;->a:LX/IXQ;

    iget-object v6, v6, LX/IXQ;->c:LX/0So;

    invoke-interface {v6}, LX/0So;->now()J

    move-result-wide v6

    iget-wide v8, v0, LX/IXK;->b:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585897
    const-string v5, "connection_quality_at_start"

    iget-object v6, v0, LX/IXK;->a:LX/IXQ;

    iget-object v6, v6, LX/IXQ;->p:LX/0p3;

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2585898
    iget-object v5, v0, LX/IXK;->a:LX/IXQ;

    iget-object v5, v5, LX/IXQ;->k:LX/Ckw;

    const-string v6, "instant_article_tracker_perf"

    invoke-virtual {v5, v6, v4}, LX/Ckw;->c(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_2
.end method
