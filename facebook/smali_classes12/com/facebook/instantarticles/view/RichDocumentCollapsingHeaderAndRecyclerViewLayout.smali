.class public Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/0vZ;


# instance fields
.field public a:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/1uH;

.field private final c:LX/Chm;

.field private final d:LX/Cho;

.field private final e:LX/ChN;

.field private final f:Landroid/animation/ValueAnimator;

.field private final g:Landroid/animation/ValueAnimator;

.field private h:Landroid/support/v7/widget/RecyclerView;

.field private i:LX/1P1;

.field public j:LX/IXs;

.field public k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

.field private l:I

.field private m:I

.field private n:I

.field public o:I

.field public p:Z

.field public q:LX/Cqw;

.field public r:Z

.field public s:Z

.field private t:Z

.field public u:Z

.field private v:I

.field public w:I

.field private x:I

.field private y:I

.field public z:LX/CnR;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2587246
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2587247
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2587248
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2587249
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2587250
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2587251
    new-instance v0, LX/1uH;

    invoke-direct {v0, p0}, LX/1uH;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->b:LX/1uH;

    .line 2587252
    new-instance v0, LX/IXw;

    invoke-direct {v0, p0}, LX/IXw;-><init>(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->c:LX/Chm;

    .line 2587253
    new-instance v0, LX/IXx;

    invoke-direct {v0, p0}, LX/IXx;-><init>(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->d:LX/Cho;

    .line 2587254
    new-instance v0, LX/IXy;

    invoke-direct {v0, p0}, LX/IXy;-><init>(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->e:LX/ChN;

    .line 2587255
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->f:Landroid/animation/ValueAnimator;

    .line 2587256
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->g:Landroid/animation/ValueAnimator;

    .line 2587257
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->p:Z

    .line 2587258
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a()V

    .line 2587259
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2587260
    const-class v0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    invoke-static {v0, p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2587261
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->f:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2587262
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->f:Landroid/animation/ValueAnimator;

    new-instance v1, LX/IXz;

    invoke-direct {v1, p0}, LX/IXz;-><init>(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2587263
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->g:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2587264
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->g:Landroid/animation/ValueAnimator;

    new-instance v1, LX/IY0;

    invoke-direct {v1, p0}, LX/IY0;-><init>(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2587265
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b12c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->y:I

    .line 2587266
    return-void
.end method

.method public static a(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;I)V
    .locals 2

    .prologue
    .line 2587267
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    if-eqz v0, :cond_0

    .line 2587268
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0}, LX/IXs;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2587269
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2587270
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v1}, LX/IXs;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2587271
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0, p1}, LX/IXs;->e(I)V

    .line 2587272
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v0

    check-cast v0, LX/Chv;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a:LX/Chv;

    return-void
.end method

.method private static a(LX/Cqw;)Z
    .locals 1

    .prologue
    .line 2587273
    sget-object v0, LX/Cqw;->b:LX/Cqw;

    invoke-virtual {p0, v0}, LX/Cqw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/Cqw;->c:LX/Cqw;

    invoke-virtual {p0, v0}, LX/Cqw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/Cqw;->d:LX/Cqw;

    invoke-virtual {p0, v0}, LX/Cqw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;II)V
    .locals 4

    .prologue
    .line 2587274
    if-ne p1, p2, :cond_1

    .line 2587275
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2587276
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2587277
    :cond_0
    :goto_0
    return-void

    .line 2587278
    :cond_1
    sub-int v0, p2, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    .line 2587279
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->f:Landroid/animation/ValueAnimator;

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    div-int/lit8 v0, v0, 0x64

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2587280
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->f:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 2587281
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 2587282
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2587283
    iget v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->w:I

    if-nez v1, :cond_0

    .line 2587284
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v1}, LX/CnR;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2587285
    :goto_0
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v1}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v2}, LX/CnR;->getTop()I

    move-result v2

    add-int/2addr v2, v0

    sub-int/2addr v1, v2

    .line 2587286
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->v:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 2587287
    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v2, v1}, LX/CnR;->setAlpha(F)V

    .line 2587288
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-static {v1, v0}, LX/0vv;->g(Landroid/view/View;I)V

    .line 2587289
    return-void

    .line 2587290
    :cond_0
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v1}, LX/CnR;->getBottom()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private b(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2587291
    if-lez p1, :cond_1

    move v0, v1

    .line 2587292
    :goto_0
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->e()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2587293
    :cond_0
    :goto_1
    return v1

    .line 2587294
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2587295
    :cond_2
    if-nez v0, :cond_3

    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getBottom()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v3}, LX/CnR;->getTop()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v2}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v3}, LX/CnR;->getBottom()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 2587296
    :cond_3
    mul-int/lit8 v2, p1, -0x1

    .line 2587297
    if-nez v0, :cond_4

    .line 2587298
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/CnR;->setAlpha(F)V

    .line 2587299
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v1}, LX/CnR;->getBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2587300
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v0}, LX/CnR;->getTop()I

    move-result v0

    iget-object v3, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getBottom()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2587301
    :goto_2
    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-static {v2, v1}, LX/0vv;->g(Landroid/view/View;I)V

    .line 2587302
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1, v0}, LX/0vv;->g(Landroid/view/View;I)V

    move v1, v0

    .line 2587303
    goto :goto_1

    .line 2587304
    :cond_4
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v0

    iget-object v3, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getBottom()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_2
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2587305
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0}, LX/IXs;->a()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d167d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2587306
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2587307
    new-instance v1, LX/IY1;

    invoke-direct {v1, p0, v0}, LX/IY1;-><init>(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2587308
    return-void
.end method

.method private b(II)V
    .locals 2

    .prologue
    .line 2587309
    iget v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->n:I

    if-eq p2, v0, :cond_1

    .line 2587310
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->x:I

    .line 2587311
    :goto_0
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2587312
    :cond_0
    :goto_1
    return-void

    .line 2587313
    :cond_1
    iget v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->x:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->x:I

    goto :goto_0

    .line 2587314
    :cond_2
    iget v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->x:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->y:I

    if-le v0, v1, :cond_0

    .line 2587315
    iget v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->n:I

    iput v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->w:I

    .line 2587316
    iget v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->w:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v1}, LX/CnR;->getTop()I

    move-result v1

    if-le v0, v1, :cond_3

    .line 2587317
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v0}, LX/CnR;->getTop()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v1}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->c(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;II)V

    goto :goto_1

    .line 2587318
    :cond_3
    iget v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->w:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v1}, LX/CnR;->getBottom()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2587319
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v0}, LX/CnR;->getBottom()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v1}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->c(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;II)V

    goto :goto_1
.end method

.method public static synthetic b(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;LX/Cqw;)Z
    .locals 1

    .prologue
    .line 2587320
    invoke-static {p1}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a(LX/Cqw;)Z

    move-result v0

    return v0
.end method

.method public static c(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;II)V
    .locals 4

    .prologue
    .line 2587321
    if-ne p1, p2, :cond_0

    .line 2587322
    :goto_0
    return-void

    .line 2587323
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->g:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2587324
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->g:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 2587325
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->g:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method private e(I)I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2587229
    if-lez p1, :cond_2

    move v0, v1

    .line 2587230
    :goto_0
    iget-object v3, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v3}, LX/IXs;->getCurrentHeight()I

    move-result v3

    .line 2587231
    if-ne v0, v2, :cond_0

    iget v4, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->m:I

    if-eq v3, v4, :cond_1

    :cond_0
    if-nez v0, :cond_3

    iget v4, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->l:I

    if-ne v3, v4, :cond_3

    .line 2587232
    :cond_1
    :goto_1
    return v1

    :cond_2
    move v0, v2

    .line 2587233
    goto :goto_0

    .line 2587234
    :cond_3
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollOffset()I

    move-result v4

    if-eqz v4, :cond_4

    iget-boolean v4, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->t:Z

    if-eqz v4, :cond_1

    .line 2587235
    :cond_4
    iput v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->n:I

    .line 2587236
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v1}, LX/IXs;->getCurrentHeight()I

    move-result v4

    .line 2587237
    if-nez v0, :cond_5

    .line 2587238
    iget v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->l:I

    sub-int v1, v3, v1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2587239
    iput-boolean v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->t:Z

    .line 2587240
    sub-int v2, v4, v1

    .line 2587241
    :goto_2
    invoke-static {p0, v2}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;I)V

    .line 2587242
    iget-object v4, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    sub-int/2addr v2, v3

    invoke-static {v4, v2}, LX/0vv;->g(Landroid/view/View;I)V

    .line 2587243
    if-eqz v0, :cond_1

    neg-int v1, v1

    goto :goto_1

    .line 2587244
    :cond_5
    iget v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->m:I

    sub-int/2addr v1, v3

    neg-int v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2587245
    add-int v2, v4, v1

    goto :goto_2
.end method

.method private e()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2587326
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 2587327
    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v1

    iget-object v4, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    .line 2587328
    iget-object v5, v4, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v4, v5

    .line 2587329
    invoke-virtual {v4}, LX/1OM;->ij_()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_2

    move v1, v2

    .line 2587330
    :goto_0
    invoke-virtual {v0}, LX/1P1;->o()I

    move-result v0

    iget-object v4, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    .line 2587331
    iget-object v5, v4, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v4, v5

    .line 2587332
    invoke-virtual {v4}, LX/1OM;->ij_()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_3

    move v0, v2

    .line 2587333
    :goto_1
    iget-object v4, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    invoke-virtual {v5}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getBottom()I

    move-result v5

    if-ge v4, v5, :cond_4

    move v4, v2

    .line 2587334
    :goto_2
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    if-eqz v4, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    return v3

    :cond_2
    move v1, v3

    .line 2587335
    goto :goto_0

    :cond_3
    move v0, v3

    .line 2587336
    goto :goto_1

    :cond_4
    move v4, v3

    .line 2587337
    goto :goto_2
.end method

.method private f()V
    .locals 1

    .prologue
    .line 2587146
    iget v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->v:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    if-eqz v0, :cond_0

    .line 2587147
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v0}, LX/CnR;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->v:I

    .line 2587148
    :cond_0
    return-void
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 2587149
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    invoke-virtual {v0}, LX/CnR;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 2

    .prologue
    .line 2587150
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0}, LX/IXs;->getCurrentHeight()I

    move-result v0

    iget v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->l:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2587151
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->i:LX/1P1;

    if-nez v1, :cond_1

    .line 2587152
    :cond_0
    :goto_0
    return v0

    .line 2587153
    :cond_1
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->i:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->o()I

    move-result v1

    .line 2587154
    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->i:LX/1P1;

    invoke-virtual {v2}, LX/1OR;->D()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getCollapsingHeader()LX/IXs;
    .locals 1

    .prologue
    .line 2587155
    const v0, 0x7f0d1685

    invoke-virtual {p0, v0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/IXs;

    return-object v0
.end method

.method public getFooter()Lcom/facebook/instantarticles/view/InstantArticlesFooter;
    .locals 1

    .prologue
    .line 2587156
    const v0, 0x7f0d1683

    invoke-virtual {p0, v0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    return-object v0
.end method

.method public getNestedScrollAxes()I
    .locals 1

    .prologue
    .line 2587157
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->b:LX/1uH;

    .line 2587158
    iget p0, v0, LX/1uH;->b:I

    move v0, p0

    .line 2587159
    return v0
.end method

.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 2587160
    const v0, 0x7f0d04e7

    invoke-virtual {p0, v0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2c

    const v2, 0x5be998f1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2587161
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2587162
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->getRecyclerView()Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    .line 2587163
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 2587164
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    instance-of v0, v0, LX/1P1;

    if-eqz v0, :cond_0

    .line 2587165
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->i:LX/1P1;

    .line 2587166
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->getFooter()Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    .line 2587167
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    .line 2587168
    iget-object v2, v0, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->a:LX/CnR;

    move-object v0, v2

    .line 2587169
    :goto_0
    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    .line 2587170
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->getCollapsingHeader()LX/IXs;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    .line 2587171
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    if-eqz v0, :cond_1

    .line 2587172
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0}, LX/IXs;->getCollapsedHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->l:I

    .line 2587173
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0}, LX/IXs;->getExpandedHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->m:I

    .line 2587174
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->b()V

    .line 2587175
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a:LX/Chv;

    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->e:LX/ChN;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2587176
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a:LX/Chv;

    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->c:LX/Chm;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2587177
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a:LX/Chv;

    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->d:LX/Cho;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2587178
    const v0, -0x46c3151

    invoke-static {v0, v1}, LX/02F;->g(II)V

    return-void

    .line 2587179
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x43a2d47b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2587180
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2587181
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a:LX/Chv;

    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->e:LX/ChN;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2587182
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a:LX/Chv;

    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->c:LX/Chm;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2587183
    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a:LX/Chv;

    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->d:LX/Cho;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2587184
    const/16 v1, 0x2d

    const v2, 0x52874df4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 2587185
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 2587186
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 2587187
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getTop()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v2}, LX/IXs;->getCurrentBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, LX/0vv;->g(Landroid/view/View;I)V

    .line 2587188
    :cond_0
    return-void
.end method

.method public final onNestedFling(Landroid/view/View;FFZ)Z
    .locals 1

    .prologue
    .line 2587189
    const/4 v0, 0x0

    return v0
.end method

.method public final onNestedPreFling(Landroid/view/View;FF)Z
    .locals 2

    .prologue
    .line 2587190
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->r:Z

    if-nez v0, :cond_2

    .line 2587191
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->i()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const v0, -0x3ae3c000    # -2500.0f

    cmpg-float v0, p3, v0

    if-gez v0, :cond_2

    .line 2587192
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0}, LX/IXs;->getCurrentHeight()I

    move-result v0

    iget v1, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->m:I

    invoke-static {p0, v0, v1}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a$redex0(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;II)V

    .line 2587193
    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public final onNestedPreScroll(Landroid/view/View;II[I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2587194
    aput v0, p4, v0

    .line 2587195
    aput v0, p4, v1

    .line 2587196
    if-nez p3, :cond_1

    .line 2587197
    :cond_0
    :goto_0
    return-void

    .line 2587198
    :cond_1
    iget v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->n:I

    .line 2587199
    if-lez p3, :cond_2

    :goto_1
    iput v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->n:I

    .line 2587200
    invoke-direct {p0, p3}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->e(I)I

    move-result v0

    aput v0, p4, v1

    .line 2587201
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->k:Lcom/facebook/instantarticles/view/InstantArticlesFooter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->z:LX/CnR;

    if-eqz v0, :cond_0

    .line 2587202
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->f()V

    .line 2587203
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2587204
    invoke-direct {p0, p3}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->b(I)I

    move-result v0

    .line 2587205
    neg-int v0, v0

    aput v0, p4, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2587206
    goto :goto_1

    .line 2587207
    :cond_3
    invoke-direct {p0, p3, v2}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->b(II)V

    goto :goto_0
.end method

.method public final onNestedScroll(Landroid/view/View;IIII)V
    .locals 0

    .prologue
    .line 2587208
    if-gez p5, :cond_0

    .line 2587209
    invoke-direct {p0, p5}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->e(I)I

    .line 2587210
    :cond_0
    return-void
.end method

.method public final onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 2587211
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->b:LX/1uH;

    .line 2587212
    iput p3, v0, LX/1uH;->b:I

    .line 2587213
    return-void
.end method

.method public final onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2587214
    iput-boolean v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->t:Z

    .line 2587215
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_1

    move v0, v1

    .line 2587216
    :goto_0
    iget-boolean v3, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->r:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->s:Z

    if-eqz v3, :cond_0

    .line 2587217
    iput-boolean v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->r:Z

    .line 2587218
    iput-boolean v2, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->s:Z

    .line 2587219
    :cond_0
    iget-boolean v3, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->u:Z

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->r:Z

    if-nez v0, :cond_2

    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 2587220
    goto :goto_0

    :cond_2
    move v1, v2

    .line 2587221
    goto :goto_1
.end method

.method public final onStopNestedScroll(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2587222
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->b:LX/1uH;

    invoke-virtual {v0}, LX/1uH;->b()V

    .line 2587223
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0}, LX/IXs;->getCurrentHeight()I

    move-result v1

    .line 2587224
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0}, LX/IXs;->getCollapsedHeight()I

    move-result v0

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0}, LX/IXs;->getExpandedHeight()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 2587225
    iget v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->n:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0}, LX/IXs;->getCollapsedHeight()I

    move-result v0

    .line 2587226
    :goto_0
    invoke-static {p0, v1, v0}, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->a$redex0(Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;II)V

    .line 2587227
    :cond_0
    return-void

    .line 2587228
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/view/RichDocumentCollapsingHeaderAndRecyclerViewLayout;->j:LX/IXs;

    invoke-interface {v0}, LX/IXs;->getExpandedHeight()I

    move-result v0

    goto :goto_0
.end method
