.class public final Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;
.super Landroid/support/design/widget/CoordinatorLayout$Behavior;
.source ""


# instance fields
.field private a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2586751
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>()V

    .line 2586752
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2586753
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2586754
    return-void
.end method

.method private a(I)I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2586755
    if-lez p1, :cond_2

    move v0, v1

    .line 2586756
    :goto_0
    iget-object v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v3}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCurrentHeight()I

    move-result v4

    .line 2586757
    if-ne v0, v2, :cond_0

    iget-object v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v3}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getExpandedHeight()I

    move-result v3

    if-eq v4, v3, :cond_1

    :cond_0
    if-nez v0, :cond_3

    iget-object v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v3}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCollapsedHeight()I

    move-result v3

    if-ne v4, v3, :cond_3

    .line 2586758
    :cond_1
    :goto_1
    return v1

    :cond_2
    move v0, v2

    .line 2586759
    goto :goto_0

    .line 2586760
    :cond_3
    iget-object v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586761
    iget-object v5, v3, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->d:Landroid/support/v7/widget/RecyclerView;

    move-object v3, v5

    .line 2586762
    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586763
    iget-object v5, v3, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->d:Landroid/support/v7/widget/RecyclerView;

    move-object v3, v5

    .line 2586764
    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollOffset()I

    move-result v3

    if-nez v3, :cond_5

    move v3, v2

    .line 2586765
    :goto_2
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->b()Z

    move-result v5

    if-eqz v5, :cond_4

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-boolean v3, v3, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->j:Z

    if-eqz v3, :cond_1

    .line 2586766
    :cond_4
    iget-object v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586767
    iput v0, v1, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->k:I

    .line 2586768
    iget-object v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v1}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCurrentHeight()I

    move-result v3

    .line 2586769
    if-nez v0, :cond_6

    .line 2586770
    iget-object v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v1}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCollapsedHeight()I

    move-result v1

    sub-int v1, v4, v1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2586771
    iget-object v5, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586772
    iput-boolean v2, v5, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->j:Z

    .line 2586773
    sub-int v2, v3, v1

    .line 2586774
    :goto_3
    iget-object v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-static {v3, v2}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->f(Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;I)V

    .line 2586775
    sub-int/2addr v2, v4

    iput v2, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->b:I

    .line 2586776
    if-eqz v0, :cond_1

    neg-int v1, v1

    goto :goto_1

    :cond_5
    move v3, v1

    .line 2586777
    goto :goto_2

    .line 2586778
    :cond_6
    iget-object v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v1}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getExpandedHeight()I

    move-result v1

    sub-int/2addr v1, v4

    neg-int v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2586779
    add-int v2, v3, v1

    goto :goto_3
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 2586780
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCurrentHeight()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v1}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCollapsedHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2586781
    iget-object v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-object v1, v1, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->e:LX/1P1;

    if-nez v1, :cond_1

    .line 2586782
    :cond_0
    :goto_0
    return v0

    .line 2586783
    :cond_1
    iget-object v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-object v1, v1, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->e:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->o()I

    move-result v1

    .line 2586784
    iget-object v2, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-object v2, v2, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->e:LX/1P1;

    invoke-virtual {v2}, LX/1OR;->D()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2586785
    iget v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->b:I

    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public final onLayoutChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 2586786
    invoke-virtual {p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;I)V

    move-object v0, p2

    .line 2586787
    check-cast v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586788
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->b:I

    .line 2586789
    invoke-super {p0, p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout$Behavior;->onLayoutChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public final onNestedPreFling(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;FF)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2586790
    move-object v0, p2

    check-cast v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586791
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-boolean v0, v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->i:Z

    if-nez v0, :cond_1

    .line 2586792
    :cond_0
    :goto_0
    return v1

    .line 2586793
    :cond_1
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->b()Z

    move-result v0

    .line 2586794
    check-cast p2, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586795
    iget-object v2, p2, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->f:LX/Chr;

    if-eqz v2, :cond_2

    .line 2586796
    if-eqz v0, :cond_5

    iget-object v0, p2, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->f:LX/Chr;

    .line 2586797
    iget-boolean v2, v0, LX/Chr;->g:Z

    move v0, v2

    .line 2586798
    if-nez v0, :cond_5

    const/4 v0, 0x1

    .line 2586799
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 2586800
    const/4 v0, 0x0

    cmpl-float v0, p5, v0

    if-lez v0, :cond_3

    invoke-direct {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->c()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const v0, -0x3ae3c000    # -2500.0f

    cmpg-float v0, p5, v0

    if-gez v0, :cond_0

    .line 2586801
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCurrentHeight()I

    move-result v0

    invoke-virtual {p2}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getExpandedHeight()I

    move-result v2

    .line 2586802
    invoke-static {p2, v0, v2}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->a$redex0(Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;II)V

    .line 2586803
    goto :goto_0

    :cond_5
    move v0, v1

    .line 2586804
    goto :goto_1
.end method

.method public final onNestedPreScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;II[I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2586805
    aput v0, p6, v0

    .line 2586806
    aput v0, p6, v1

    .line 2586807
    check-cast p2, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iput-object p2, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586808
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-boolean v0, v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->i:Z

    if-eqz v0, :cond_0

    if-nez p5, :cond_1

    .line 2586809
    :cond_0
    :goto_0
    return-void

    .line 2586810
    :cond_1
    invoke-direct {p0, p5}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a(I)I

    move-result v0

    aput v0, p6, v1

    goto :goto_0
.end method

.method public final onNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;IIII)V
    .locals 1

    .prologue
    .line 2586811
    check-cast p2, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iput-object p2, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586812
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-boolean v0, v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->i:Z

    if-nez v0, :cond_1

    .line 2586813
    :cond_0
    :goto_0
    return-void

    .line 2586814
    :cond_1
    if-gez p7, :cond_0

    .line 2586815
    invoke-direct {p0, p7}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a(I)I

    goto :goto_0
.end method

.method public final onStartNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;Landroid/view/View;I)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2586816
    check-cast p2, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iput-object p2, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586817
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-boolean v0, v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->i:Z

    if-nez v0, :cond_0

    .line 2586818
    :goto_0
    return v2

    .line 2586819
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586820
    iput-boolean v2, v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->j:Z

    .line 2586821
    and-int/lit8 v0, p5, 0x2

    if-eqz v0, :cond_2

    move v0, v1

    .line 2586822
    :goto_1
    iget-object v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-boolean v3, v3, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->i:Z

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    move v0, v1

    .line 2586823
    :goto_2
    iget-object v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-object v3, v3, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->f:LX/Chr;

    if-eqz v3, :cond_6

    .line 2586824
    iget-object v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-object v3, v3, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->f:LX/Chr;

    .line 2586825
    iget-boolean v4, v3, LX/Chr;->g:Z

    move v4, v4

    .line 2586826
    if-eqz v4, :cond_4

    iget-object v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-object v3, v3, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->f:LX/Chr;

    .line 2586827
    iget-boolean p0, v3, LX/Chr;->h:Z

    move v3, p0

    .line 2586828
    if-eqz v3, :cond_4

    move v3, v1

    .line 2586829
    :goto_3
    if-eqz v0, :cond_5

    if-eqz v4, :cond_1

    if-eqz v3, :cond_5

    :cond_1
    :goto_4
    move v2, v1

    .line 2586830
    goto :goto_0

    :cond_2
    move v0, v2

    .line 2586831
    goto :goto_1

    :cond_3
    move v0, v2

    .line 2586832
    goto :goto_2

    :cond_4
    move v3, v2

    .line 2586833
    goto :goto_3

    :cond_5
    move v1, v2

    .line 2586834
    goto :goto_4

    :cond_6
    move v1, v0

    goto :goto_4
.end method

.method public final onStopNestedScroll(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2586835
    check-cast p2, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iput-object p2, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586836
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget-boolean v0, v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->i:Z

    if-nez v0, :cond_1

    .line 2586837
    :cond_0
    :goto_0
    return-void

    .line 2586838
    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCurrentHeight()I

    move-result v1

    .line 2586839
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCollapsedHeight()I

    move-result v0

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getExpandedHeight()I

    move-result v0

    if-eq v1, v0, :cond_0

    .line 2586840
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    iget v0, v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->k:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCollapsedHeight()I

    move-result v0

    .line 2586841
    :goto_1
    iget-object v2, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2586842
    invoke-static {v2, v1, v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->a$redex0(Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;II)V

    .line 2586843
    goto :goto_0

    .line 2586844
    :cond_2
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a:Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getExpandedHeight()I

    move-result v0

    goto :goto_1
.end method
