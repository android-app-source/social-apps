.class public Lcom/facebook/instantarticles/view/InstantArticlesFooter;
.super Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;
.source ""

# interfaces
.implements LX/CsR;


# instance fields
.field public a:LX/CnR;

.field private b:LX/CqD;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2586979
    invoke-direct {p0, p1}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2586980
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->b()V

    .line 2586981
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2586957
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2586958
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->b()V

    .line 2586959
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2586976
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2586977
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->b()V

    .line 2586978
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2586972
    iget-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a:LX/7yl;

    move-object v0, v0

    .line 2586973
    new-instance v1, LX/CqW;

    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/CqW;-><init>(Landroid/content/Context;)V

    .line 2586974
    iput-object v1, v0, LX/7yl;->g:LX/7yk;

    .line 2586975
    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 0

    .prologue
    .line 2586971
    return-void
.end method

.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 2586970
    return-object p0
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 2586969
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 2586968
    return-void
.end method

.method public getFragmentPager()LX/CqD;
    .locals 1

    .prologue
    .line 2586967
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->b:LX/CqD;

    return-object v0
.end method

.method public getUfiView()LX/CnR;
    .locals 1

    .prologue
    .line 2586966
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->a:LX/CnR;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6a6bb739

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2586964
    invoke-super {p0}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->onAttachedToWindow()V

    .line 2586965
    const/16 v1, 0x2d

    const v2, -0x6cc0a83    # -5.8400014E34f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setFragmentPager(LX/CqD;)V
    .locals 0

    .prologue
    .line 2586962
    iput-object p1, p0, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->b:LX/CqD;

    .line 2586963
    return-void
.end method

.method public setUfiView(LX/CnR;)V
    .locals 0

    .prologue
    .line 2586960
    iput-object p1, p0, Lcom/facebook/instantarticles/view/InstantArticlesFooter;->a:LX/CnR;

    .line 2586961
    return-void
.end method
