.class public Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;
.super LX/Hi9;
.source ""

# interfaces
.implements LX/Che;


# annotations
.annotation runtime Landroid/support/design/widget/CoordinatorLayout$DefaultBehavior;
    value = Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$ScrollingSiblingBehavior;
.end annotation


# instance fields
.field public a:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/IXp;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2586744
    invoke-direct {p0, p1}, LX/Hi9;-><init>(Landroid/content/Context;)V

    .line 2586745
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->c:Ljava/util/Set;

    .line 2586746
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->g()V

    .line 2586747
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2586740
    invoke-direct {p0, p1, p2}, LX/Hi9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2586741
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->c:Ljava/util/Set;

    .line 2586742
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->g()V

    .line 2586743
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v0

    check-cast v0, LX/Crz;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->a:LX/Crz;

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2586737
    const/4 v0, 0x0

    .line 2586738
    :try_start_0
    invoke-super {p0, p1}, LX/Hi9;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2586739
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method

.method public static a(Landroid/view/View;III)Z
    .locals 1

    .prologue
    .line 2586696
    check-cast p0, Landroid/view/ViewGroup;

    if-lez p1, :cond_0

    sget-object v0, LX/31M;->RIGHT:LX/31M;

    :goto_0
    invoke-static {p0, v0, p2, p3}, LX/3BA;->a(Landroid/view/ViewGroup;LX/31M;II)Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, LX/31M;->LEFT:LX/31M;

    goto :goto_0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 2586734
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXp;

    .line 2586735
    invoke-interface {v0, p1}, LX/IXp;->c(I)V

    goto :goto_0

    .line 2586736
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2586730
    const-class v0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;

    invoke-static {v0, p0}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2586731
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->a:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->d:Z

    .line 2586732
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 2586733
    return-void
.end method


# virtual methods
.method public final a(IZ)V
    .locals 0

    .prologue
    .line 2586727
    invoke-super {p0, p1, p2}, LX/Hi9;->a(IZ)V

    .line 2586728
    invoke-direct {p0, p1}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->b(I)V

    .line 2586729
    return-void
.end method

.method public final a(LX/IXp;)V
    .locals 1

    .prologue
    .line 2586724
    if-eqz p1, :cond_0

    .line 2586725
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2586726
    :cond_0
    return-void
.end method

.method public final a(LX/31M;)Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2586713
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2586714
    :cond_0
    :goto_0
    return v1

    .line 2586715
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->d:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2586716
    :goto_1
    iget-boolean v2, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->d:Z

    if-eqz v2, :cond_3

    move v2, v1

    .line 2586717
    :goto_2
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    .line 2586718
    sget-object v5, LX/31M;->RIGHT:LX/31M;

    if-ne p1, v5, :cond_5

    .line 2586719
    iget-boolean v2, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->d:Z

    if-eqz v2, :cond_4

    if-ge v4, v0, :cond_0

    move v1, v3

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2586720
    goto :goto_1

    .line 2586721
    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v2

    invoke-virtual {v2}, LX/0gG;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 2586722
    :cond_4
    if-le v4, v0, :cond_0

    move v1, v3

    goto :goto_0

    .line 2586723
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->d:Z

    if-eqz v0, :cond_6

    if-le v4, v2, :cond_0

    move v1, v3

    goto :goto_0

    :cond_6
    if-ge v4, v2, :cond_0

    move v1, v3

    goto :goto_0
.end method

.method public final b(LX/IXp;)V
    .locals 1

    .prologue
    .line 2586711
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2586712
    return-void
.end method

.method public getFragmentPager()LX/CqD;
    .locals 1

    .prologue
    .line 2586710
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->b:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    return-object v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2586703
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->b:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    if-nez v0, :cond_0

    .line 2586704
    invoke-direct {p0, p1}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2586705
    :goto_0
    return v0

    .line 2586706
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->b:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->b:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v1}, LX/CsS;->getActiveFragmentIndex()I

    move-result v1

    invoke-virtual {v0, v1}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/InstantArticlesFragment;

    .line 2586707
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/facebook/instantarticles/InstantArticlesFragment;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2586708
    const/4 v0, 0x0

    goto :goto_0

    .line 2586709
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setCurrentItem(I)V
    .locals 0

    .prologue
    .line 2586700
    invoke-super {p0, p1}, LX/Hi9;->setCurrentItem(I)V

    .line 2586701
    invoke-direct {p0, p1}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->b(I)V

    .line 2586702
    return-void
.end method

.method public setFragmentPager(LX/CqD;)V
    .locals 1

    .prologue
    .line 2586697
    instance-of v0, p1, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    if-nez v0, :cond_0

    .line 2586698
    :goto_0
    return-void

    .line 2586699
    :cond_0
    check-cast p1, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iput-object p1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->b:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    goto :goto_0
.end method
