.class public Lcom/facebook/instantarticles/view/ShareBar;
.super Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;
.source ""


# instance fields
.field public A:LX/IYL;

.field public B:LX/Chi;

.field private C:I

.field public D:I

.field public E:Z

.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/14x;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ck0;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cju;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Crz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CoV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalComposer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:I

.field public p:I

.field public q:I

.field public r:F

.field public s:I

.field public t:I

.field public u:I

.field private v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public w:Landroid/widget/ImageView;

.field public x:Lcom/facebook/widget/text/BetterTextView;

.field private y:LX/ChT;

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2587593
    invoke-direct {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2587594
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->z:Z

    .line 2587595
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->E:Z

    .line 2587596
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/ShareBar;->a()V

    .line 2587597
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2587598
    invoke-direct {p0, p1, p2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2587599
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->z:Z

    .line 2587600
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->E:Z

    .line 2587601
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/ShareBar;->a()V

    .line 2587602
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2587603
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2587604
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->z:Z

    .line 2587605
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->E:Z

    .line 2587606
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/ShareBar;->a()V

    .line 2587607
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2587608
    const-class v0, Lcom/facebook/instantarticles/view/ShareBar;

    invoke-static {v0, p0}, Lcom/facebook/instantarticles/view/ShareBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2587609
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/instantarticles/view/ShareBar;->setClickable(Z)V

    .line 2587610
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1287

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->C:I

    .line 2587611
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->D:I

    .line 2587612
    new-instance v0, LX/IYB;

    invoke-direct {v0, p0}, LX/IYB;-><init>(Lcom/facebook/instantarticles/view/ShareBar;)V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->y:LX/ChT;

    .line 2587613
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    iget-object v1, p0, Lcom/facebook/instantarticles/view/ShareBar;->y:LX/ChT;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2587614
    return-void
.end method

.method private a(LX/5OG;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2587557
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/richdocument/BaseRichDocumentActivity;

    if-nez v0, :cond_1

    .line 2587558
    :cond_0
    :goto_0
    return-void

    .line 2587559
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->z:Z

    if-eqz v0, :cond_0

    .line 2587560
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/BaseRichDocumentActivity;

    invoke-virtual {v0}, Lcom/facebook/richdocument/BaseRichDocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_instant_articles_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2587561
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2fv;->a:LX/0Tn;

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2587562
    invoke-static {v0, v1}, LX/8bJ;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2587563
    const v2, 0x7f081c5c

    invoke-virtual {p1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    .line 2587564
    const v3, 0x7f0209f2

    invoke-virtual {v2, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, LX/IYC;

    invoke-direct {v3, p0, p2, v0, v1}, LX/IYC;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 2587565
    :cond_2
    const v2, 0x7f081c5b

    invoke-virtual {p1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    .line 2587566
    const v3, 0x7f0209f0

    invoke-virtual {v2, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v2

    new-instance v3, LX/IYD;

    invoke-direct {v3, p0, p2, v0, v1}, LX/IYD;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;LX/5OG;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2587615
    invoke-virtual {p2, p3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    const v1, 0x7f021670

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, LX/IY8;

    invoke-direct {v1, p0, p4, p1}, LX/IY8;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2587616
    return-void
.end method

.method private static a(Lcom/facebook/instantarticles/view/ShareBar;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/instantarticles/view/ShareBar;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ckw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/14x;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ck0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Cju;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Crz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CoV;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalComposer;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2587617
    iput-object p1, p0, Lcom/facebook/instantarticles/view/ShareBar;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/instantarticles/view/ShareBar;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/instantarticles/view/ShareBar;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/instantarticles/view/ShareBar;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/instantarticles/view/ShareBar;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/instantarticles/view/ShareBar;->f:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/instantarticles/view/ShareBar;->g:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/instantarticles/view/ShareBar;->h:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/instantarticles/view/ShareBar;->i:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/instantarticles/view/ShareBar;->j:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/instantarticles/view/ShareBar;->k:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/instantarticles/view/ShareBar;->l:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/instantarticles/view/ShareBar;->m:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/instantarticles/view/ShareBar;->n:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/instantarticles/view/ShareBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v14

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/instantarticles/view/ShareBar;

    const/16 v1, 0xdf4

    invoke-static {v14, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x3216

    invoke-static {v14, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xdec

    invoke-static {v14, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x12c4

    invoke-static {v14, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x31e0

    invoke-static {v14, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xf9a

    invoke-static {v14, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x320f

    invoke-static {v14, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x320e

    invoke-static {v14, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3244

    invoke-static {v14, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x329d

    invoke-static {v14, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x455

    invoke-static {v14, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3239

    invoke-static {v14, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3222

    invoke-static {v14, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v15, 0x1032

    invoke-static {v14, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {v0 .. v14}, Lcom/facebook/instantarticles/view/ShareBar;->a(Lcom/facebook/instantarticles/view/ShareBar;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2587618
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2587619
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2587620
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2587621
    const-string v1, "android.intent.extra.TEXT"

    invoke-direct {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getUrlToUse()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2587622
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 2587623
    return-void
.end method

.method private b(LX/5OG;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2587624
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/2yD;->E:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2587625
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2587626
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2587627
    iget-object v1, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587628
    iget-object v2, v1, LX/Chi;->e:Ljava/lang/String;

    move-object v1, v2

    .line 2587629
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2587630
    const-string v1, "force_external_browser"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2587631
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, LX/0DQ;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 2587632
    if-nez v1, :cond_1

    .line 2587633
    invoke-direct {p0, p1, p2}, Lcom/facebook/instantarticles/view/ShareBar;->c(LX/5OG;Ljava/lang/String;)V

    .line 2587634
    :cond_0
    :goto_0
    return-void

    .line 2587635
    :cond_1
    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v2, :cond_0

    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-boolean v2, v2, Landroid/content/pm/ComponentInfo;->exported:Z

    if-eqz v2, :cond_0

    .line 2587636
    iget-object v2, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    const-string v3, "android"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2587637
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081c63

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1, p2}, Lcom/facebook/instantarticles/view/ShareBar;->a(Landroid/content/Intent;LX/5OG;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2587638
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081c64

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1, p2}, Lcom/facebook/instantarticles/view/ShareBar;->a(Landroid/content/Intent;LX/5OG;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2587639
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/facebook/instantarticles/view/ShareBar;->c(LX/5OG;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/instantarticles/view/ShareBar;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 2587640
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->v:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2587641
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->fo:J

    const-string v1, ""

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2587642
    const/16 v1, 0x2c

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->v:Ljava/util/List;

    .line 2587643
    :cond_1
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getUrlToUse()Ljava/lang/String;

    move-result-object v1

    .line 2587644
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2587645
    :goto_0
    return-void

    .line 2587646
    :cond_2
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2587647
    new-instance v3, LX/IYK;

    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, p0, v0}, LX/IYK;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Landroid/content/Context;)V

    .line 2587648
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, v0}, LX/0ht;->c(Landroid/view/View;)V

    .line 2587649
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, LX/5OM;->a(Z)V

    .line 2587650
    sget-object v0, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v3, v0}, LX/0ht;->a(LX/3AV;)V

    .line 2587651
    new-instance v0, LX/IYE;

    invoke-direct {v0, p0, v2}, LX/IYE;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;)V

    .line 2587652
    iput-object v0, v3, LX/0ht;->I:LX/2yQ;

    .line 2587653
    invoke-virtual {v3}, LX/5OM;->c()LX/5OG;

    move-result-object v4

    .line 2587654
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2587655
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K20;

    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getUrlToUse()Ljava/lang/String;

    move-result-object v6

    .line 2587656
    invoke-static {v0}, LX/K20;->a(LX/K20;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 2587657
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2587658
    const v0, 0x7f081c69

    invoke-virtual {v4, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    const v5, 0x7f02080f

    invoke-virtual {v0, v5}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v5, LX/IYF;

    invoke-direct {v5, p0, v2, v1}, LX/IYF;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2587659
    :cond_4
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14x;

    invoke-virtual {v0}, LX/14x;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/14x;

    invoke-virtual {v0}, LX/14x;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2587660
    const v0, 0x7f081c6a

    invoke-virtual {v4, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    invoke-static {}, LX/10A;->a()I

    move-result v5

    invoke-virtual {v0, v5}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v5, LX/IYG;

    invoke-direct {v5, p0, v2}, LX/IYG;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2587661
    :cond_5
    const v0, 0x7f081c6b

    invoke-virtual {v4, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    const v5, 0x7f02166f

    invoke-virtual {v0, v5}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v5, LX/IYH;

    invoke-direct {v5, p0, v2}, LX/IYH;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2587662
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587663
    iget-boolean v5, v0, LX/Chi;->h:Z

    move v0, v5

    .line 2587664
    if-eqz v0, :cond_9

    .line 2587665
    const v0, 0x7f081c6d

    invoke-virtual {v4, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    const v5, 0x7f021671

    invoke-virtual {v0, v5}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v5, LX/IYI;

    invoke-direct {v5, p0, v2}, LX/IYI;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2587666
    :goto_2
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587667
    iget-object v5, v0, LX/Chi;->e:Ljava/lang/String;

    move-object v0, v5

    .line 2587668
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2587669
    invoke-direct {p0, v4, v2}, Lcom/facebook/instantarticles/view/ShareBar;->b(LX/5OG;Ljava/lang/String;)V

    .line 2587670
    :cond_6
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587671
    iget-object v5, v0, LX/Chi;->e:Ljava/lang/String;

    move-object v0, v5

    .line 2587672
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2587673
    const v0, 0x7f081c65

    invoke-virtual {v4, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    const v5, 0x7f020850

    invoke-virtual {v0, v5}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v5, LX/IY5;

    invoke-direct {v5, p0, v2}, LX/IY5;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2587674
    :cond_7
    invoke-direct {p0, v4, v2}, Lcom/facebook/instantarticles/view/ShareBar;->a(LX/5OG;Ljava/lang/String;)V

    .line 2587675
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->v:Ljava/util/List;

    if-eqz v0, :cond_a

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 2587676
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 2587677
    new-instance v6, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2587678
    const-string v0, "text/plain"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2587679
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2587680
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_8
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2587681
    invoke-virtual {v6, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2587682
    invoke-virtual {v5, v6, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 2587683
    if-eqz v1, :cond_8

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_8

    .line 2587684
    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 2587685
    iget-object v8, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v8, v5}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v4, v8}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v8

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v1, v5}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v8, v1}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v8, LX/IY6;

    invoke-direct {v8, p0, v0, v2}, LX/IY6;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_3

    .line 2587686
    :cond_9
    const v0, 0x7f081c6e

    invoke-virtual {v4, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    const v5, 0x7f021671

    invoke-virtual {v0, v5}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v5, LX/IYJ;

    invoke-direct {v5, p0, v2}, LX/IYJ;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;)V

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 2587687
    :cond_a
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, LX/5OG;->a(Landroid/content/res/ColorStateList;)V

    .line 2587688
    invoke-virtual {v3}, LX/0ht;->d()V

    goto/16 :goto_0

    .line 2587689
    :cond_b
    const v7, 0x7f0837e8

    invoke-virtual {v4, v7}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v8

    .line 2587690
    const v7, 0x7f0209af

    invoke-virtual {v8, v7}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2587691
    new-instance v7, LX/K1y;

    invoke-direct {v7, v0, v5, v2, v6}, LX/K1y;-><init>(LX/K20;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v7}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2587692
    iget-object v7, v0, LX/K20;->f:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/2sP;

    invoke-virtual {v7}, LX/2sP;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v7

    .line 2587693
    invoke-static {v7}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v7

    .line 2587694
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v7, v10, :cond_c

    .line 2587695
    const v7, 0x7f0837ea

    invoke-virtual {v8, v7}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 2587696
    iget-object v7, v0, LX/K20;->g:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/Ckw;

    const-string v8, "quick_share_public"

    invoke-static {v7, v8, v2}, LX/CoV;->a(LX/Ckw;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2587697
    :cond_c
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v7, v10, :cond_3

    .line 2587698
    const v7, 0x7f0837e9

    invoke-virtual {v8, v7}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 2587699
    iget-object v7, v0, LX/K20;->g:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/Ckw;

    const-string v8, "quick_share_friends"

    invoke-static {v7, v8, v2}, LX/CoV;->a(LX/Ckw;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public static b$redex0(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2587700
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, LX/CoV;->a(LX/Ckw;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2587701
    return-void
.end method

.method private c(LX/5OG;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2587702
    const v0, 0x7f081c63

    invoke-virtual {p1, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    const v1, 0x7f021670

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    new-instance v1, LX/IY7;

    invoke-direct {v1, p0, p2}, LX/IY7;-><init>(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2587703
    return-void
.end method

.method public static c(Lcom/facebook/instantarticles/view/ShareBar;)V
    .locals 3

    .prologue
    .line 2587577
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587578
    iget-object v1, v0, LX/Chi;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2587579
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2587580
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2587581
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587582
    iget-object v2, v0, LX/Chi;->e:Ljava/lang/String;

    move-object v0, v2

    .line 2587583
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2587584
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2587585
    :cond_0
    return-void
.end method

.method public static c$redex0(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2587586
    invoke-static {p1, p2}, LX/8bJ;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2587587
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2587588
    :goto_0
    move-object v1, p2

    .line 2587589
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/2fv;->a:LX/0Tn;

    invoke-interface {v0, v2, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2587590
    return-void

    .line 2587591
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_1
    move-object p2, p1

    .line 2587592
    goto :goto_0
.end method

.method public static d(Lcom/facebook/instantarticles/view/ShareBar;)V
    .locals 4

    .prologue
    .line 2587497
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587498
    iget-object v1, v0, LX/Chi;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2587499
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2587500
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.SENDTO"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2587501
    const-string v0, "text/plain"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2587502
    const-string v0, "android.intent.extra.TEXT"

    iget-object v2, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587503
    iget-object v3, v2, LX/Chi;->e:Ljava/lang/String;

    move-object v2, v3

    .line 2587504
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2587505
    const-string v0, "mailto:"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2587506
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2587507
    :cond_0
    return-void
.end method

.method public static d(Lcom/facebook/instantarticles/view/ShareBar;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2587508
    const/16 v0, 0x2c

    invoke-static {p1, v0}, LX/0YN;->a(Ljava/lang/String;C)Ljava/util/List;

    move-result-object v0

    .line 2587509
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2587510
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2587511
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2587512
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_1

    .line 2587513
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2587514
    :cond_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2587515
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2587516
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, LX/2fv;->a:LX/0Tn;

    invoke-interface {v0, v2, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2587517
    return-void
.end method

.method public static e$redex0(Lcom/facebook/instantarticles/view/ShareBar;)V
    .locals 5

    .prologue
    .line 2587518
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5up;

    invoke-direct {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getUrlToUse()Ljava/lang/String;

    move-result-object v1

    const-string v2, "native_web_view"

    const-string v3, "saved_add"

    new-instance v4, LX/IY9;

    invoke-direct {v4, p0}, LX/IY9;-><init>(Lcom/facebook/instantarticles/view/ShareBar;)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/5up;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2587519
    return-void
.end method

.method public static f(Lcom/facebook/instantarticles/view/ShareBar;)V
    .locals 5

    .prologue
    .line 2587520
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5up;

    invoke-direct {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getUrlToUse()Ljava/lang/String;

    move-result-object v1

    const-string v2, "native_web_view"

    const-string v3, "saved_add"

    new-instance v4, LX/IYA;

    invoke-direct {v4, p0}, LX/IYA;-><init>(Lcom/facebook/instantarticles/view/ShareBar;)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/5up;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2587521
    return-void
.end method

.method public static g$redex0(Lcom/facebook/instantarticles/view/ShareBar;)V
    .locals 9

    .prologue
    .line 2587522
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2587523
    :goto_0
    return-void

    .line 2587524
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K20;

    invoke-direct {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getUrlToUse()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v6, 0x1

    .line 2587525
    iget-object v3, v0, LX/K20;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/B9y;

    const-string v8, "browser"

    move-object v4, v2

    move-object v5, v1

    move v7, v6

    invoke-virtual/range {v3 .. v8}, LX/B9y;->a(Landroid/content/Context;Ljava/lang/String;ZZLjava/lang/String;)V

    .line 2587526
    goto :goto_0
.end method

.method private getUrlToUse()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2587527
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    if-nez v0, :cond_0

    .line 2587528
    const/4 v0, 0x0

    .line 2587529
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587530
    iget-object v1, v0, LX/Chi;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2587531
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587532
    iget-object v1, v0, LX/Chi;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2587533
    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587534
    iget-object v1, v0, LX/Chi;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2587535
    goto :goto_0
.end method

.method public static h$redex0(Lcom/facebook/instantarticles/view/ShareBar;)V
    .locals 3

    .prologue
    .line 2587536
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/instantarticles/view/ShareBar;->getUrlToUse()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/47Y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2587537
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081c6c

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2587538
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2587539
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 2587540
    :cond_0
    :goto_0
    return-void

    .line 2587541
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/facebook/instantarticles/view/ShareBar;->C:I

    sub-int v1, p1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/instantarticles/view/ShareBar;->D:I

    iget v3, p0, Lcom/facebook/instantarticles/view/ShareBar;->C:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 2587542
    iget v1, p0, Lcom/facebook/instantarticles/view/ShareBar;->o:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/instantarticles/view/ShareBar;->p:I

    int-to-float v2, v2

    invoke-static {v1, v2, v0}, LX/8bS;->a(FFF)F

    move-result v1

    .line 2587543
    iget v2, p0, Lcom/facebook/instantarticles/view/ShareBar;->q:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/instantarticles/view/ShareBar;->s:I

    int-to-float v3, v3

    invoke-static {v2, v3, v0}, LX/8bS;->a(FFF)F

    move-result v2

    .line 2587544
    iget v3, p0, Lcom/facebook/instantarticles/view/ShareBar;->t:I

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/instantarticles/view/ShareBar;->u:I

    int-to-float v4, v4

    invoke-static {v3, v4, v0}, LX/8bS;->a(FFF)F

    move-result v0

    .line 2587545
    iget-object v3, p0, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, v5, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(IF)V

    .line 2587546
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 2587547
    int-to-float v2, v1

    iget v3, p0, Lcom/facebook/instantarticles/view/ShareBar;->r:F

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 2587548
    iget-object v3, p0, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2587549
    if-eqz v3, :cond_2

    .line 2587550
    iput v1, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2587551
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2587552
    :cond_2
    iget-object v1, p0, Lcom/facebook/instantarticles/view/ShareBar;->w:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2587553
    float-to-int v1, v0

    float-to-int v0, v0

    invoke-virtual {p0, v1, v5, v0, v5}, Lcom/facebook/instantarticles/view/ShareBar;->setPadding(IIII)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2587554
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ckw;

    invoke-static {v0, p1, p2}, LX/CoV;->a(LX/Ckw;Ljava/lang/String;Ljava/lang/String;)V

    .line 2587555
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2587556
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "setOnClickListener not allowed"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnCloseClickedListener(LX/IYL;)V
    .locals 0

    .prologue
    .line 2587567
    iput-object p1, p0, Lcom/facebook/instantarticles/view/ShareBar;->A:LX/IYL;

    .line 2587568
    return-void
.end method

.method public setRichDocumentInfo(LX/Chi;)V
    .locals 0

    .prologue
    .line 2587569
    iput-object p1, p0, Lcom/facebook/instantarticles/view/ShareBar;->B:LX/Chi;

    .line 2587570
    return-void
.end method

.method public setShowShareButton(Z)V
    .locals 2

    .prologue
    .line 2587571
    iput-boolean p1, p0, Lcom/facebook/instantarticles/view/ShareBar;->E:Z

    .line 2587572
    iget-boolean v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->E:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_0

    .line 2587573
    iget-object v0, p0, Lcom/facebook/instantarticles/view/ShareBar;->x:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2587574
    :cond_0
    return-void
.end method

.method public setWasCalledBySampleApp(Z)V
    .locals 0

    .prologue
    .line 2587575
    iput-boolean p1, p0, Lcom/facebook/instantarticles/view/ShareBar;->z:Z

    .line 2587576
    return-void
.end method
