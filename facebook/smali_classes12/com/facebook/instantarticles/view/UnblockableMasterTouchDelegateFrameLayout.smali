.class public Lcom/facebook/instantarticles/view/UnblockableMasterTouchDelegateFrameLayout;
.super Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2587716
    invoke-direct {p0, p1}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2587717
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/UnblockableMasterTouchDelegateFrameLayout;->a()V

    .line 2587718
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2587704
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2587705
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/UnblockableMasterTouchDelegateFrameLayout;->a()V

    .line 2587706
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2587714
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2587715
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2587710
    iget-object v0, p0, Lcom/facebook/fbui/tinyclicks/widget/MasterTouchDelegateFrameLayout;->a:LX/7yl;

    move-object v0, v0

    .line 2587711
    new-instance v1, LX/CqW;

    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/UnblockableMasterTouchDelegateFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/CqW;-><init>(Landroid/content/Context;)V

    .line 2587712
    iput-object v1, v0, LX/7yl;->g:LX/7yk;

    .line 2587713
    return-void
.end method


# virtual methods
.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 1

    .prologue
    .line 2587707
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/UnblockableMasterTouchDelegateFrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2587708
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/UnblockableMasterTouchDelegateFrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2587709
    :cond_0
    return-void
.end method
