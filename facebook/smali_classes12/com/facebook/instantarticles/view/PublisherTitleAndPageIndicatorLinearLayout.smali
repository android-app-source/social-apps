.class public Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "LX/IXv;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

.field private c:Landroid/widget/TextView;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2587050
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2587051
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->a:Ljava/util/Map;

    .line 2587052
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2587053
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2587054
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->a:Ljava/util/Map;

    .line 2587055
    return-void
.end method


# virtual methods
.method public final onFinishInflate()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/16 v0, 0x2c

    const v1, 0x4f0c81c0

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2587056
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onFinishInflate()V

    .line 2587057
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1287

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->d:I

    .line 2587058
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1288

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->e:I

    .line 2587059
    const v0, 0x7f0d1802

    invoke-virtual {p0, v0}, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->c:Landroid/widget/TextView;

    .line 2587060
    const v0, 0x7f0d1803

    invoke-virtual {p0, v0}, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    .line 2587061
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 2587062
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 2587063
    iget-object v3, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v2}, Landroid/widget/TextView;->measure(II)V

    .line 2587064
    iget-object v3, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-virtual {v3, v0, v2}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->measure(II)V

    .line 2587065
    iget-object v0, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->getMeasuredHeight()I

    move-result v0

    .line 2587066
    iget-object v2, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    .line 2587067
    iget v3, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->d:I

    div-int/lit8 v3, v3, 0x2

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    .line 2587068
    iget-object v3, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->a:Ljava/util/Map;

    iget-object v4, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    new-instance v5, LX/IXv;

    invoke-direct {v5, v0, v2}, LX/IXv;-><init>(II)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2587069
    iget-object v3, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->a:Ljava/util/Map;

    iget-object v4, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->c:Landroid/widget/TextView;

    new-instance v5, LX/IXv;

    sub-int v0, v2, v0

    neg-int v0, v0

    invoke-direct {v5, v0, v6}, LX/IXv;-><init>(II)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2587070
    const/16 v0, 0x2d

    const v2, -0x6b7d9b0b

    invoke-static {v7, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 2587071
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomViewGroup;->onLayout(ZIIII)V

    .line 2587072
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->getHeight()I

    move-result v0

    .line 2587073
    iget v1, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->d:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->e:I

    iget v2, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->d:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 2587074
    iget-object v0, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->a:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXv;

    iget v0, v0, LX/IXv;->a:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->a:Ljava/util/Map;

    iget-object v3, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXv;

    iget v0, v0, LX/IXv;->b:I

    int-to-float v0, v0

    invoke-static {v2, v0, v1}, LX/8bS;->a(FFF)F

    move-result v0

    .line 2587075
    iget-object v2, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->b:Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v2, v0}, LX/0vv;->g(Landroid/view/View;I)V

    .line 2587076
    iget-object v0, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->a:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->c:Landroid/widget/TextView;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXv;

    iget v0, v0, LX/IXv;->a:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->a:Ljava/util/Map;

    iget-object v3, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->c:Landroid/widget/TextView;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXv;

    iget v0, v0, LX/IXv;->b:I

    int-to-float v0, v0

    invoke-static {v2, v0, v1}, LX/8bS;->a(FFF)F

    move-result v0

    .line 2587077
    iget-object v2, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->c:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v2, v0}, LX/0vv;->g(Landroid/view/View;I)V

    .line 2587078
    const/4 v0, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v2, v1}, LX/8bS;->a(FFF)F

    move-result v0

    .line 2587079
    iget-object v1, p0, Lcom/facebook/instantarticles/view/PublisherTitleAndPageIndicatorLinearLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setAlpha(F)V

    .line 2587080
    return-void
.end method
