.class public final Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$ScrollingSiblingBehavior;
.super Landroid/support/design/widget/CoordinatorLayout$Behavior;
.source ""


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2586845
    invoke-direct {p0}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>()V

    .line 2586846
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2586865
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/CoordinatorLayout$Behavior;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2586866
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2586859
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1uK;

    .line 2586860
    iget-object v1, v0, LX/1uK;->a:Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-object v0, v1

    .line 2586861
    check-cast v0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;

    .line 2586862
    invoke-virtual {v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;->a()I

    move-result v0

    .line 2586863
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    iget v2, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$ScrollingSiblingBehavior;->a:I

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    invoke-static {p1, v0}, LX/0vv;->g(Landroid/view/View;I)V

    .line 2586864
    return-void
.end method


# virtual methods
.method public final layoutDependsOn(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 2586858
    instance-of v0, p3, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    return v0
.end method

.method public final onDependentViewChanged(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 2586855
    instance-of v0, p3, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    if-eqz v0, :cond_0

    .line 2586856
    invoke-direct {p0, p2, p3}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$ScrollingSiblingBehavior;->a(Landroid/view/View;Landroid/view/View;)V

    .line 2586857
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final onLayoutChild(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;I)Z
    .locals 4

    .prologue
    .line 2586847
    invoke-virtual {p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;->a(Landroid/view/View;I)V

    .line 2586848
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$ScrollingSiblingBehavior;->a:I

    .line 2586849
    invoke-virtual {p1, p2}, Landroid/support/design/widget/CoordinatorLayout;->c(Landroid/view/View;)Ljava/util/List;

    move-result-object v1

    .line 2586850
    const/4 v0, 0x0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 2586851
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    if-eqz v3, :cond_1

    .line 2586852
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, p2, v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$ScrollingSiblingBehavior;->a(Landroid/view/View;Landroid/view/View;)V

    .line 2586853
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 2586854
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
