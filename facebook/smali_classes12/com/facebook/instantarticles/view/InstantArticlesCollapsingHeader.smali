.class public Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;
.super LX/IXr;
.source ""

# interfaces
.implements LX/IXs;
.implements LX/Chq;


# annotations
.annotation runtime Landroid/support/design/widget/CoordinatorLayout$DefaultBehavior;
    value = Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader$Behavior;
.end annotation


# instance fields
.field private final c:Landroid/animation/ValueAnimator;

.field public d:Landroid/support/v7/widget/RecyclerView;

.field public e:LX/1P1;

.field public f:LX/Chr;

.field private g:I

.field private h:I

.field public i:Z

.field public j:Z

.field public k:I

.field private l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2586915
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2586916
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2586944
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2586945
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2586936
    invoke-direct {p0, p1, p2, p3}, LX/IXr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2586937
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->c:Landroid/animation/ValueAnimator;

    .line 2586938
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->i:Z

    .line 2586939
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1287

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->g:I

    .line 2586940
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1288

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->h:I

    .line 2586941
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->c:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2586942
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->c:Landroid/animation/ValueAnimator;

    new-instance v1, LX/IXq;

    invoke-direct {v1, p0}, LX/IXq;-><init>(Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2586943
    return-void
.end method

.method public static a$redex0(Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;II)V
    .locals 4

    .prologue
    .line 2586928
    if-ne p1, p2, :cond_1

    .line 2586929
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2586930
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2586931
    :cond_0
    :goto_0
    return-void

    .line 2586932
    :cond_1
    sub-int v0, p2, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    .line 2586933
    iget-object v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->c:Landroid/animation/ValueAnimator;

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    div-int/lit8 v0, v0, 0x64

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2586934
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->c:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    .line 2586935
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->c:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public static f(Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;I)V
    .locals 1

    .prologue
    .line 2586923
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2586924
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2586925
    invoke-virtual {p0, v0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2586926
    invoke-virtual {p0, p1}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->e(I)V

    .line 2586927
    return-void
.end method


# virtual methods
.method public final a(LX/Chr;)V
    .locals 2

    .prologue
    .line 2586917
    iput-object p1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->f:LX/Chr;

    .line 2586918
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCurrentHeight()I

    move-result v0

    .line 2586919
    if-eqz v0, :cond_0

    .line 2586920
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCurrentHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->l:I

    .line 2586921
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCurrentHeight()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->a$redex0(Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;II)V

    .line 2586922
    return-void
.end method

.method public final b(LX/Chr;)V
    .locals 2

    .prologue
    .line 2586912
    iput-object p1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->f:LX/Chr;

    .line 2586913
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getCurrentHeight()I

    move-result v0

    iget v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->l:I

    invoke-static {p0, v0, v1}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->a$redex0(Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;II)V

    .line 2586914
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 2586946
    iget-object v0, p0, LX/IXr;->a:Lcom/facebook/instantarticles/view/ShareBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IXr;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-static {v0}, LX/0vv;->E(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2586947
    :cond_0
    :goto_0
    return-void

    .line 2586948
    :cond_1
    iget-object v0, p0, LX/IXr;->a:Lcom/facebook/instantarticles/view/ShareBar;

    invoke-virtual {v0, p1}, Lcom/facebook/instantarticles/view/ShareBar;->a(I)V

    goto :goto_0
.end method

.method public getCollapsedHeight()I
    .locals 1

    .prologue
    .line 2586911
    iget v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->g:I

    return v0
.end method

.method public getCurrentBottom()I
    .locals 1

    .prologue
    .line 2586901
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getBottom()I

    move-result v0

    return v0
.end method

.method public getCurrentHeight()I
    .locals 1

    .prologue
    .line 2586910
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->getHeight()I

    move-result v0

    return v0
.end method

.method public getExpandedHeight()I
    .locals 1

    .prologue
    .line 2586909
    iget v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->h:I

    return v0
.end method

.method public getRecyclerView()Landroid/support/v7/widget/RecyclerView;
    .locals 1

    .prologue
    .line 2586908
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->d:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method public setEnableExpansion(Z)V
    .locals 0

    .prologue
    .line 2586906
    iput-boolean p1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->i:Z

    .line 2586907
    return-void
.end method

.method public setRecyclerView(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 2586902
    iput-object p1, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->d:Landroid/support/v7/widget/RecyclerView;

    .line 2586903
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    instance-of v0, v0, LX/1P1;

    if-eqz v0, :cond_0

    .line 2586904
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;->e:LX/1P1;

    .line 2586905
    :cond_0
    return-void
.end method
