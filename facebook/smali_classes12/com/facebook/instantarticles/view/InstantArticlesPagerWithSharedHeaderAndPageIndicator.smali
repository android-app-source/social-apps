.class public Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;
.super LX/CsS;
.source ""

# interfaces
.implements LX/CtT;


# instance fields
.field public h:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:I

.field private j:I

.field private k:I

.field private l:LX/IXu;

.field private m:Z

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2587041
    invoke-direct {p0, p1}, LX/CsS;-><init>(Landroid/content/Context;)V

    .line 2587042
    sget-object v0, LX/IXu;->WAITING_FOR_DOWN:LX/IXu;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->l:LX/IXu;

    .line 2587043
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->m:Z

    .line 2587044
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->a()V

    .line 2587045
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2587036
    invoke-direct {p0, p1, p2}, LX/CsS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2587037
    sget-object v0, LX/IXu;->WAITING_FOR_DOWN:LX/IXu;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->l:LX/IXu;

    .line 2587038
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->m:Z

    .line 2587039
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->a()V

    .line 2587040
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2587031
    invoke-direct {p0, p1, p2, p3}, LX/CsS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2587032
    sget-object v0, LX/IXu;->WAITING_FOR_DOWN:LX/IXu;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->l:LX/IXu;

    .line 2587033
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->m:Z

    .line 2587034
    invoke-direct {p0}, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->a()V

    .line 2587035
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2587026
    const-class v0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-static {v0, p0}, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2587027
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->h:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->n:Z

    .line 2587028
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1283

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    iput v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->i:I

    .line 2587029
    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b12c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->k:I

    .line 2587030
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v0

    check-cast v0, LX/Crz;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->h:LX/Crz;

    return-void
.end method

.method private b(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2587025
    iget-boolean v2, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->n:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->k:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->k:I

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(IZ)V
    .locals 3

    .prologue
    .line 2587019
    if-nez p2, :cond_1

    .line 2587020
    :cond_0
    :goto_0
    return-void

    .line 2587021
    :cond_1
    invoke-virtual {p0, p1}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    .line 2587022
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 2587023
    if-eqz v0, :cond_0

    .line 2587024
    const-string v1, "open_action"

    const-string v2, "swiped"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2586992
    invoke-virtual {p0}, LX/CsS;->getActiveFragmentIndex()I

    move-result v0

    .line 2586993
    invoke-direct {p0, p1}, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->b(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 2586994
    iget-boolean v4, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->m:Z

    if-nez v4, :cond_0

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    move v0, v2

    .line 2586995
    :goto_0
    return v0

    .line 2586996
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 2586997
    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    move v0, v3

    .line 2586998
    goto :goto_0

    .line 2586999
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->j:I

    .line 2587000
    sget-object v0, LX/IXu;->WAITING_FOR_CONSIDERABLE_MOVE:LX/IXu;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->l:LX/IXu;

    .line 2587001
    iput-boolean v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->m:Z

    move v0, v3

    .line 2587002
    goto :goto_0

    .line 2587003
    :pswitch_1
    iget-boolean v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->m:Z

    if-nez v0, :cond_1

    .line 2587004
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    .line 2587005
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v5, v0

    .line 2587006
    iget v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->j:I

    sub-int v6, v4, v0

    .line 2587007
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->l:LX/IXu;

    sget-object v1, LX/IXu;->WAITING_FOR_CONSIDERABLE_MOVE:LX/IXu;

    if-ne v0, v1, :cond_2

    .line 2587008
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->i:I

    if-le v0, v1, :cond_2

    .line 2587009
    sget-object v0, LX/IXu;->SILENTLY_WATCHING_MOVE_EVENTS:LX/IXu;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->l:LX/IXu;

    .line 2587010
    :cond_2
    iget-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->l:LX/IXu;

    sget-object v1, LX/IXu;->SILENTLY_WATCHING_MOVE_EVENTS:LX/IXu;

    if-ne v0, v1, :cond_1

    .line 2587011
    iget-object v0, p0, LX/CsS;->g:Landroid/support/v4/view/ViewPager;

    check-cast v0, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;

    .line 2587012
    if-gez v6, :cond_4

    sget-object v1, LX/31M;->LEFT:LX/31M;

    :goto_2
    invoke-virtual {v0, v1}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->a(LX/31M;)Z

    move-result v1

    .line 2587013
    invoke-static {v0, v6, v4, v5}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->a(Landroid/view/View;III)Z

    move-result v0

    .line 2587014
    if-nez v1, :cond_3

    if-eqz v0, :cond_5

    :cond_3
    move v0, v2

    goto :goto_0

    .line 2587015
    :cond_4
    sget-object v1, LX/31M;->RIGHT:LX/31M;

    goto :goto_2

    :cond_5
    move v0, v3

    .line 2587016
    goto :goto_0

    .line 2587017
    :pswitch_2
    sget-object v0, LX/IXu;->WAITING_FOR_DOWN:LX/IXu;

    iput-object v0, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->l:LX/IXu;

    .line 2587018
    iput-boolean v3, p0, Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;->m:Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getHeaderResourceId()I
    .locals 1

    .prologue
    .line 2586991
    const v0, 0x7f0d1800

    return v0
.end method

.method public getPageIndicatorResourceId()I
    .locals 1

    .prologue
    .line 2586989
    const v0, 0x7f0d1803

    return v0
.end method

.method public getViewPagerResourceId()I
    .locals 1

    .prologue
    .line 2586990
    const v0, 0x7f0d1804

    return v0
.end method
