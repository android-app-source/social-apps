.class public Lcom/facebook/rapidfeedback/debug/DebugRapidFeedbackActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/3Bz;


# instance fields
.field public p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2480617
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/rapidfeedback/debug/DebugRapidFeedbackActivity;

    const/16 v1, 0x122d

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rapidfeedback/debug/DebugRapidFeedbackActivity;->p:LX/0Or;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2480618
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2480619
    invoke-static {p0, p0}, Lcom/facebook/rapidfeedback/debug/DebugRapidFeedbackActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2480620
    if-nez p1, :cond_0

    .line 2480621
    invoke-virtual {p0}, Lcom/facebook/rapidfeedback/debug/DebugRapidFeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "args_survey_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2480622
    invoke-virtual {p0}, Lcom/facebook/rapidfeedback/debug/DebugRapidFeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "args_integration_point_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2480623
    if-eqz v1, :cond_1

    const-string v0, "UNKNOWN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2480624
    iget-object v0, p0, Lcom/facebook/rapidfeedback/debug/DebugRapidFeedbackActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 2480625
    iget-object v2, v0, LX/0gt;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/rapidfeedback/RapidFeedbackController;

    .line 2480626
    iget-object v3, v2, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    .line 2480627
    iput-object p0, v3, Lcom/facebook/structuredsurvey/StructuredSurveyController;->C:Landroid/content/Context;

    .line 2480628
    invoke-static {p0}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v5

    .line 2480629
    iget-object v3, v2, Lcom/facebook/rapidfeedback/RapidFeedbackController;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1x7;

    iget-object v4, v2, Lcom/facebook/rapidfeedback/RapidFeedbackController;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/structuredsurvey/StructuredSurveyController;

    new-instance p1, Lcom/facebook/rapidfeedback/RapidFeedbackController$2;

    invoke-direct {p1, v2, v5, v1}, Lcom/facebook/rapidfeedback/RapidFeedbackController$2;-><init>(Lcom/facebook/rapidfeedback/RapidFeedbackController;LX/0ew;Ljava/lang/String;)V

    .line 2480630
    iget-object v5, v3, LX/1x7;->e:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0tX;

    .line 2480631
    new-instance v0, LX/7Ed;

    invoke-direct {v0}, LX/7Ed;-><init>()V

    move-object v0, v0

    .line 2480632
    const-string v2, "survey_id"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7Ed;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    iput-object v5, v3, LX/1x7;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2480633
    iget-object v5, v3, LX/1x7;->q:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v0, LX/7EM;

    invoke-direct {v0, v3, v4, p1, p0}, LX/7EM;-><init>(LX/1x7;Lcom/facebook/structuredsurvey/StructuredSurveyController;Ljava/lang/Runnable;Landroid/content/Context;)V

    iget-object v2, v3, LX/1x7;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2480634
    :cond_0
    :goto_0
    return-void

    .line 2480635
    :cond_1
    if-eqz v2, :cond_0

    const-string v0, "UNKNOWN"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2480636
    iget-object v0, p0, Lcom/facebook/rapidfeedback/debug/DebugRapidFeedbackActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 2480637
    iput-object v2, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2480638
    move-object v0, v0

    .line 2480639
    iget-object v1, v0, LX/0gt;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/rapidfeedback/RapidFeedbackController;

    iget-object v2, v0, LX/0gt;->a:Ljava/lang/String;

    iget-object v3, v0, LX/0gt;->b:LX/1ww;

    invoke-virtual {v3}, LX/1ww;->k()LX/1x4;

    move-result-object v3

    .line 2480640
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/facebook/rapidfeedback/RapidFeedbackController;->f:Z

    .line 2480641
    invoke-virtual {v1, v2, p0, v3}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(Ljava/lang/String;Landroid/content/Context;LX/1x4;)V

    .line 2480642
    goto :goto_0
.end method
