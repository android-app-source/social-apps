.class public Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:Lcom/facebook/rapidfeedback/RapidFeedbackController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2480736
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private a()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2480730
    new-instance v0, LX/4oi;

    invoke-direct {v0, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 2480731
    sget-object v1, LX/1x7;->a:LX/0Tn;

    invoke-virtual {v0, v1}, LX/4oi;->a(LX/0Tn;)V

    .line 2480732
    const-string v1, "Developer Mode"

    invoke-virtual {v0, v1}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 2480733
    const-string v1, "Disables timeout between surveys"

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 2480734
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 2480735
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2480726
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2480727
    invoke-virtual {v0, p2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2480728
    invoke-direct {p0, p1, p2}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->c(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 2480729
    return-object v0
.end method

.method private a(Landroid/preference/PreferenceScreen;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2480718
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2480719
    invoke-virtual {v0, p2}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2480720
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2480721
    iget-object v0, p0, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->a:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-virtual {v0, p3}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->a(LX/0Tn;)Ljava/util/List;

    move-result-object v0

    .line 2480722
    if-eqz v0, :cond_0

    .line 2480723
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2480724
    invoke-direct {p0, p4, v0}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/preference/Preference;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    .line 2480725
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;Lcom/facebook/rapidfeedback/RapidFeedbackController;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 2480717
    iput-object p1, p0, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->a:Lcom/facebook/rapidfeedback/RapidFeedbackController;

    iput-object p2, p0, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->b:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;

    invoke-static {v1}, Lcom/facebook/rapidfeedback/RapidFeedbackController;->b(LX/0QB;)Lcom/facebook/rapidfeedback/RapidFeedbackController;

    move-result-object v0

    check-cast v0, Lcom/facebook/rapidfeedback/RapidFeedbackController;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->a(Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;Lcom/facebook/rapidfeedback/RapidFeedbackController;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private b()Landroid/preference/Preference;
    .locals 2

    .prologue
    .line 2480714
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2480715
    const-string v1, "Still don\'t see a survey? Are you whitelisted in your Tessa config?"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 2480716
    return-object v0
.end method

.method public static b(Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2480710
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2480711
    iget-object v0, p0, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, p1, p2}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->c(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2480712
    const/4 v0, 0x1

    .line 2480713
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2480671
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/rapidfeedback/debug/DebugRapidFeedbackActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2480672
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2480673
    return-object v0
.end method

.method private d()Landroid/preference/Preference;
    .locals 3

    .prologue
    .line 2480702
    new-instance v0, LX/4om;

    invoke-direct {v0, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 2480703
    invoke-virtual {v0}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 2480704
    const-string v1, "With Integration Point ID"

    invoke-virtual {v0, v1}, LX/4om;->setTitle(Ljava/lang/CharSequence;)V

    .line 2480705
    const-string v1, "Display a survey from Intern/Tessa"

    invoke-virtual {v0, v1}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    .line 2480706
    invoke-virtual {v0}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 2480707
    invoke-virtual {v0}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    const-string v2, "Integration Point ID"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2480708
    new-instance v1, LX/HYR;

    invoke-direct {v1, p0}, LX/HYR;-><init>(Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;)V

    invoke-virtual {v0, v1}, LX/4om;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2480709
    return-object v0
.end method

.method private e()Landroid/preference/Preference;
    .locals 3

    .prologue
    .line 2480694
    new-instance v0, LX/4om;

    invoke-direct {v0, p0}, LX/4om;-><init>(Landroid/content/Context;)V

    .line 2480695
    invoke-virtual {v0}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 2480696
    const-string v1, "With Survey ID"

    invoke-virtual {v0, v1}, LX/4om;->setTitle(Ljava/lang/CharSequence;)V

    .line 2480697
    const-string v1, "Display a survey from Intern/SimonX"

    invoke-virtual {v0, v1}, LX/4om;->setSummary(Ljava/lang/CharSequence;)V

    .line 2480698
    invoke-virtual {v0}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 2480699
    invoke-virtual {v0}, LX/4om;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    const-string v2, "Survey ID"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2480700
    new-instance v1, LX/HYS;

    invoke-direct {v1, p0}, LX/HYS;-><init>(Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;)V

    invoke-virtual {v0, v1}, LX/4om;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2480701
    return-object v0
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2480690
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 2480691
    invoke-static {p0, p0}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2480692
    const-string v0, "Rapid Feedback Settings"

    invoke-virtual {p0, v0}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 2480693
    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x22

    const v1, 0x45ba5453

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2480674
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onResume()V

    .line 2480675
    invoke-virtual {p0}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 2480676
    invoke-virtual {p0, v1}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 2480677
    new-instance v2, Landroid/preference/PreferenceCategory;

    invoke-direct {v2, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2480678
    const-string v3, "TEST / DEBUG"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2480679
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2480680
    invoke-direct {p0}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->e()Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2480681
    invoke-direct {p0}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->d()Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2480682
    new-instance v2, Landroid/preference/PreferenceCategory;

    invoke-direct {v2, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2480683
    const-string v3, "DON\'T SEE A SURVEY?"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 2480684
    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2480685
    invoke-direct {p0}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->a()Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2480686
    invoke-direct {p0}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->b()Landroid/preference/Preference;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 2480687
    const-string v2, "RECENT SURVEY IDS"

    sget-object v3, Lcom/facebook/structuredsurvey/StructuredSurveyController;->a:LX/0Tn;

    const-string v4, "args_survey_id"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->a(Landroid/preference/PreferenceScreen;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V

    .line 2480688
    const-string v2, "RECENT INTEGRATION POINT IDS"

    sget-object v3, Lcom/facebook/structuredsurvey/StructuredSurveyController;->b:LX/0Tn;

    const-string v4, "args_integration_point_id"

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/facebook/rapidfeedback/debug/RapidFeedbackPreferencesActivity;->a(Landroid/preference/PreferenceScreen;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V

    .line 2480689
    const/16 v1, 0x23

    const v2, 0x50276f45

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
