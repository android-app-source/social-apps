.class public Lcom/facebook/feed/history/StoryEditHistoryFragment;
.super Lcom/facebook/ui/edithistory/EditHistoryFragment;
.source ""


# instance fields
.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JMf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2684487
    invoke-direct {p0}, Lcom/facebook/ui/edithistory/EditHistoryFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/feed/history/StoryEditHistoryFragment;

    const/16 p0, 0x1c96

    invoke-static {v1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    iput-object v1, p1, Lcom/facebook/feed/history/StoryEditHistoryFragment;->c:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2684484
    const-class v0, Lcom/facebook/feed/history/StoryEditHistoryFragment;

    invoke-static {v0, p0}, Lcom/facebook/feed/history/StoryEditHistoryFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2684485
    invoke-super {p0, p1}, Lcom/facebook/ui/edithistory/EditHistoryFragment;->a(Landroid/os/Bundle;)V

    .line 2684486
    return-void
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 2684483
    const v0, 0x7f030647

    return v0
.end method

.method public final n()LX/A7F;
    .locals 1

    .prologue
    .line 2684476
    iget-object v0, p0, Lcom/facebook/feed/history/StoryEditHistoryFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JMf;

    .line 2684477
    return-object v0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x45c697f2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2684478
    invoke-super {p0}, Lcom/facebook/ui/edithistory/EditHistoryFragment;->onStart()V

    .line 2684479
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2684480
    if-eqz v0, :cond_0

    .line 2684481
    const v2, 0x7f080fe5

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2684482
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x3ce909b

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
