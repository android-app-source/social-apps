.class public Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements LX/0hF;
.implements LX/0fg;


# static fields
.field public static final D:LX/J8x;

.field private static final E:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public A:Z

.field public B:Z

.field private C:I

.field public F:LX/9lP;

.field public G:Ljava/lang/String;

.field public H:Ljava/lang/String;

.field public I:LX/B0O;

.field public final J:I

.field private K:I

.field public a:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/J9Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/J8y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/J8u;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/J9y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

.field public g:LX/J8z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/J9L;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/J90;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/J9X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/J9b;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/J9R;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/J9T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/B0P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:Lcom/facebook/widget/listview/BetterListView;

.field public t:LX/J9W;

.field public u:LX/J9a;

.field public v:LX/62k;

.field private w:LX/1B1;

.field public x:Lcom/facebook/feed/banner/GenericNotificationBanner;

.field private y:Ljava/lang/String;

.field public z:LX/1PF;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2653361
    sget-object v0, LX/J8x;->COLLECTION:LX/J8x;

    sput-object v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->D:LX/J8x;

    .line 2653362
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->E:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2653363
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2653364
    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->A:Z

    .line 2653365
    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->B:Z

    .line 2653366
    iput v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->C:I

    .line 2653367
    iput v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->K:I

    .line 2653368
    sget-object v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->E:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    .line 2653369
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2653370
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2653371
    const v1, 0x7f0b248e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0b249a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 2653372
    invoke-static {p0, v0}, LX/JDA;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    .line 2653373
    return-object v0
.end method

.method private static a(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;Ljava/lang/String;LX/J9Y;LX/J8y;LX/J8u;LX/J9y;LX/J8z;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0kb;LX/03V;LX/J9L;LX/J90;LX/1Ck;LX/J9X;LX/J9b;LX/J9R;LX/J9T;LX/B0P;)V
    .locals 1

    .prologue
    .line 2653374
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->b:LX/J9Y;

    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->c:LX/J8y;

    iput-object p4, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->d:LX/J8u;

    iput-object p5, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->e:LX/J9y;

    iput-object p6, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->g:LX/J8z;

    iput-object p7, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p8, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->i:LX/0kb;

    iput-object p9, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->j:LX/03V;

    iput-object p10, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->k:LX/J9L;

    iput-object p11, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->l:LX/J90;

    iput-object p12, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->m:LX/1Ck;

    iput-object p13, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n:LX/J9X;

    iput-object p14, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->o:LX/J9b;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->p:LX/J9R;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->q:LX/J9T;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->r:LX/B0P;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 21

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v19

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-static/range {v19 .. v19}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static/range {v19 .. v19}, LX/J9Y;->a(LX/0QB;)LX/J9Y;

    move-result-object v4

    check-cast v4, LX/J9Y;

    invoke-static/range {v19 .. v19}, LX/J8y;->a(LX/0QB;)LX/J8y;

    move-result-object v5

    check-cast v5, LX/J8y;

    invoke-static/range {v19 .. v19}, LX/J8u;->a(LX/0QB;)LX/J8u;

    move-result-object v6

    check-cast v6, LX/J8u;

    invoke-static/range {v19 .. v19}, LX/J9y;->a(LX/0QB;)LX/J9y;

    move-result-object v7

    check-cast v7, LX/J9y;

    invoke-static/range {v19 .. v19}, LX/J8z;->a(LX/0QB;)LX/J8z;

    move-result-object v8

    check-cast v8, LX/J8z;

    invoke-static/range {v19 .. v19}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v9

    check-cast v9, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {v19 .. v19}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v10

    check-cast v10, LX/0kb;

    invoke-static/range {v19 .. v19}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static/range {v19 .. v19}, LX/J9L;->a(LX/0QB;)LX/J9L;

    move-result-object v12

    check-cast v12, LX/J9L;

    invoke-static/range {v19 .. v19}, LX/J90;->a(LX/0QB;)LX/J90;

    move-result-object v13

    check-cast v13, LX/J90;

    invoke-static/range {v19 .. v19}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v14

    check-cast v14, LX/1Ck;

    const-class v15, LX/J9X;

    move-object/from16 v0, v19

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/J9X;

    const-class v16, LX/J9b;

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/J9b;

    const-class v17, LX/J9R;

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/J9R;

    const-class v18, LX/J9T;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/J9T;

    const-class v20, LX/B0P;

    invoke-interface/range {v19 .. v20}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/B0P;

    invoke-static/range {v2 .. v19}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;Ljava/lang/String;LX/J9Y;LX/J8y;LX/J8u;LX/J9y;LX/J8z;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0kb;LX/03V;LX/J9L;LX/J90;LX/1Ck;LX/J9X;LX/J9b;LX/J9R;LX/J9T;LX/B0P;)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 2653375
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2653376
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->k:LX/J9L;

    invoke-virtual {v0}, LX/J9L;->h()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    .line 2653377
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->m:LX/1Ck;

    const-string v2, "initial fetch"

    new-instance v3, LX/J9d;

    invoke-direct {v3, p0, p1, v0}, LX/J9d;-><init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;ZI)V

    new-instance v0, LX/J9e;

    invoke-direct {v0, p0}, LX/J9e;-><init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2653378
    return-void

    .line 2653379
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(III)Z
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2653380
    if-gtz p1, :cond_1

    .line 2653381
    :cond_0
    :goto_0
    return v0

    .line 2653382
    :cond_1
    sub-int v1, p2, p0

    div-int/2addr v1, p1

    .line 2653383
    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 2653384
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653385
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2653386
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->j:LX/03V;

    const-string v1, "timeline_invalid_meuser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "logged in user: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2653387
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653388
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a:Ljava/lang/String;

    .line 2653389
    const-string v1, "collection_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->H:Ljava/lang/String;

    .line 2653390
    const-string v1, "profile_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2653391
    const-string v2, "friendship_status"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    .line 2653392
    const-string v3, "subscribe_status"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    .line 2653393
    new-instance v4, LX/9lP;

    invoke-direct {v4, v1, v0, v2, v3}, LX/9lP;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    iput-object v4, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->F:LX/9lP;

    .line 2653394
    const-string v1, "section_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->G:Ljava/lang/String;

    .line 2653395
    invoke-static {p0, p1}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->c(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;Landroid/os/Bundle;)Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    .line 2653396
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2653397
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->G:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->H:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/J93;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->y:Ljava/lang/String;

    .line 2653398
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->o:LX/J9b;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->F:LX/9lP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->b:LX/J9Y;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/J9b;->a(LX/9lP;Landroid/content/Context;Landroid/view/LayoutInflater;LX/J8p;)LX/J9a;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    .line 2653399
    const/4 v0, 0x0

    .line 2653400
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    if-eqz v1, :cond_1

    .line 2653401
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, LX/B0d;->a(J)LX/B0d;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    invoke-static {v0, v1}, LX/J9Q;->a(LX/B0d;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;)LX/B0N;

    move-result-object v0

    .line 2653402
    :cond_1
    iget-object v9, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->r:LX/B0P;

    iget-object v10, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->y:Ljava/lang/String;

    new-instance v1, LX/B0a;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->p:LX/J9R;

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->G:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->H:Ljava/lang/String;

    .line 2653403
    new-instance v7, LX/J9Q;

    invoke-static {v2}, LX/J90;->a(LX/0QB;)LX/J90;

    move-result-object v5

    check-cast v5, LX/J90;

    invoke-static {v2}, LX/J9L;->a(LX/0QB;)LX/J9L;

    move-result-object v6

    check-cast v6, LX/J9L;

    invoke-direct {v7, v3, v4, v5, v6}, LX/J9Q;-><init>(Ljava/lang/String;Ljava/lang/String;LX/J90;LX/J9L;)V

    .line 2653404
    move-object v2, v7

    .line 2653405
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->q:LX/J9T;

    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->H:Ljava/lang/String;

    .line 2653406
    new-instance v7, LX/J9S;

    invoke-static {v3}, LX/J90;->a(LX/0QB;)LX/J90;

    move-result-object v5

    check-cast v5, LX/J90;

    invoke-static {v3}, LX/J9L;->a(LX/0QB;)LX/J9L;

    move-result-object v6

    check-cast v6, LX/J9L;

    invoke-direct {v7, v4, v5, v6}, LX/J9S;-><init>(Ljava/lang/String;LX/J90;LX/J9L;)V

    .line 2653407
    move-object v3, v7

    .line 2653408
    const-wide/32 v4, 0x15180

    .line 2653409
    const/4 v6, 0x1

    move v6, v6

    .line 2653410
    iget-object v7, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->g:LX/J8z;

    invoke-virtual {v7}, LX/J8z;->e()Z

    move-result v7

    invoke-direct/range {v1 .. v7}, LX/B0a;-><init>(LX/B0V;LX/B0V;JZZ)V

    new-instance v2, LX/J9c;

    invoke-direct {v2, p0}, LX/J9c;-><init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V

    invoke-virtual {v9, v10, v1, v2, v0}, LX/B0P;->a(Ljava/lang/String;LX/B0U;LX/B0L;LX/B0N;)LX/B0O;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->I:LX/B0O;

    .line 2653411
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->I:LX/B0O;

    invoke-virtual {v0}, LX/B0O;->a()V

    .line 2653412
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n:LX/J9X;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->F:LX/9lP;

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->b:LX/J9Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/J9X;->a(Landroid/content/Context;LX/9lP;LX/J8p;Landroid/view/LayoutInflater;)LX/J9W;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    .line 2653413
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    if-eqz v0, :cond_3

    .line 2653414
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    invoke-virtual {v0, v1}, LX/J9W;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;)V

    .line 2653415
    :cond_3
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->c:LX/J8y;

    sget-object v2, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->D:LX/J8x;

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/J8y;->a(LX/J8x;Z)V

    .line 2653416
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2653417
    invoke-direct {p0, v8}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a(Z)V

    .line 2653418
    :cond_4
    return-void

    :cond_5
    move v0, v8

    .line 2653419
    goto :goto_0
.end method

.method public static c(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;Landroid/os/Bundle;)Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 2653420
    const-string v0, "collection"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JBL;

    .line 2653421
    if-eqz v0, :cond_0

    .line 2653422
    invoke-interface {v0}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-interface {v0}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-interface {v0}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2653423
    if-nez v1, :cond_1

    :cond_0
    move-object v0, v2

    .line 2653424
    :goto_1
    return-object v0

    .line 2653425
    :cond_1
    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->a(LX/JBL;)Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2653426
    new-instance v1, LX/JBw;

    invoke-direct {v1}, LX/JBw;-><init>()V

    .line 2653427
    iput-object v0, v1, LX/JBw;->a:LX/0Px;

    .line 2653428
    move-object v0, v1

    .line 2653429
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 2653430
    new-instance v6, LX/186;

    const/16 v7, 0x80

    invoke-direct {v6, v7}, LX/186;-><init>(I)V

    .line 2653431
    iget-object v7, v0, LX/JBw;->a:LX/0Px;

    invoke-static {v6, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 2653432
    invoke-virtual {v6, v10}, LX/186;->c(I)V

    .line 2653433
    invoke-virtual {v6, v9, v7}, LX/186;->b(II)V

    .line 2653434
    invoke-virtual {v6}, LX/186;->d()I

    move-result v7

    .line 2653435
    invoke-virtual {v6, v7}, LX/186;->d(I)V

    .line 2653436
    invoke-virtual {v6}, LX/186;->e()[B

    move-result-object v6

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 2653437
    invoke-virtual {v7, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2653438
    new-instance v6, LX/15i;

    move-object v9, v8

    move-object v11, v8

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2653439
    new-instance v7, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;

    invoke-direct {v7, v6}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;-><init>(LX/15i;)V

    .line 2653440
    move-object v0, v7

    .line 2653441
    new-instance v3, LX/JBv;

    invoke-direct {v3}, LX/JBv;-><init>()V

    .line 2653442
    iput-object v0, v3, LX/JBv;->b:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;

    .line 2653443
    const-string v0, "view_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2653444
    const-string v0, "section_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2653445
    const-string v0, "collections_section_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    .line 2653446
    const-string v0, "collections_icon"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2653447
    if-eqz v4, :cond_2

    if-eqz v5, :cond_2

    if-eqz v1, :cond_2

    if-nez v0, :cond_5

    .line 2653448
    :cond_2
    const-string v3, "CollectionCollectionFragment was passed a preliminary collection but was missing: "

    .line 2653449
    if-nez v4, :cond_3

    .line 2653450
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "name, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2653451
    :cond_3
    if-nez v5, :cond_4

    .line 2653452
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "sectionId, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2653453
    :cond_4
    if-nez v1, :cond_7

    .line 2653454
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "sectionTypeArg, "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2653455
    :goto_2
    if-nez v0, :cond_6

    .line 2653456
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "icon, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2653457
    :goto_3
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->j:LX/03V;

    const-string v3, "collections_collection_prelim"

    invoke-virtual {v1, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 2653458
    goto/16 :goto_1

    .line 2653459
    :cond_5
    iput-object v5, v3, LX/JBv;->d:Ljava/lang/String;

    .line 2653460
    iput-object v4, v3, LX/JBv;->e:Ljava/lang/String;

    .line 2653461
    iput-object v0, v3, LX/JBv;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2653462
    move-object v0, v1

    .line 2653463
    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2653464
    iput-object v0, v3, LX/JBv;->f:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2653465
    invoke-virtual {v3}, LX/JBv;->a()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_3

    :cond_7
    move-object v1, v3

    goto :goto_2

    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static d(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2653351
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "cursor"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "not_cursor"

    goto :goto_0
.end method

.method public static e$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V
    .locals 3

    .prologue
    .line 2653466
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->b:LX/J9Y;

    .line 2653467
    iget-boolean v1, v0, LX/J8p;->b:Z

    move v0, v1

    .line 2653468
    if-nez v0, :cond_0

    .line 2653469
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->b:LX/J9Y;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->F:LX/9lP;

    .line 2653470
    iget-object v2, v1, LX/9lP;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2653471
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->F:LX/9lP;

    invoke-static {v2}, LX/J8p;->a(LX/9lP;)LX/9lQ;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/J8p;->a(Ljava/lang/String;LX/9lQ;)V

    .line 2653472
    :cond_0
    return-void
.end method

.method public static k(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2653473
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2653474
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->e$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V

    .line 2653475
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    invoke-virtual {v0, v1}, LX/J9W;->a(Z)V

    .line 2653476
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    if-eqz v0, :cond_0

    .line 2653477
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    invoke-interface {v0}, LX/62k;->f()V

    .line 2653478
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->x:Lcom/facebook/feed/banner/GenericNotificationBanner;

    if-eqz v0, :cond_1

    .line 2653479
    iput-boolean v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->A:Z

    .line 2653480
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->x:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-virtual {v0}, LX/4nk;->a()V

    .line 2653481
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    const v1, -0x1926a2f2

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2653482
    return-void

    :cond_2
    move v0, v1

    .line 2653483
    goto :goto_0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 2653352
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    if-eqz v0, :cond_0

    .line 2653353
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    invoke-interface {v0}, LX/62k;->c()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isFocusable()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2653354
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    invoke-interface {v0}, LX/62k;->c()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isFocusableInTouchMode()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2653355
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    invoke-interface {v0}, LX/62k;->c()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 2653356
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v0

    .line 2653357
    if-eqz v1, :cond_1

    .line 2653358
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2653359
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2653360
    :cond_1
    return-void
.end method

.method public static n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z
    .locals 1

    .prologue
    .line 2653484
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->g:LX/J8z;

    invoke-virtual {v0}, LX/J8z;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2653194
    const-string v0, "collections_collection"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const v4, 0xbf0001

    .line 2653195
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2653196
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2653197
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-interface {v0, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2653198
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->d(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v4, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2653199
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->c:LX/J8y;

    sget-object v1, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->D:LX/J8x;

    invoke-virtual {v0, v1}, LX/J8y;->a(LX/J8x;)V

    .line 2653200
    if-eqz p1, :cond_0

    .line 2653201
    invoke-direct {p0, p1}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->b(Landroid/os/Bundle;)V

    .line 2653202
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    const/16 v2, 0xdd

    invoke-interface {v0, v4, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2653203
    :goto_0
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->w:LX/1B1;

    .line 2653204
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->d:LX/J8u;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->F:LX/9lP;

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->w:LX/1B1;

    invoke-virtual {v0, v1, v2, v3}, LX/J8u;->a(Landroid/content/Context;LX/9lP;LX/1B1;)V

    .line 2653205
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    const/16 v2, 0x2c

    invoke-interface {v0, v4, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2653206
    return-void

    .line 2653207
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2653208
    invoke-direct {p0, v0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->b(Landroid/os/Bundle;)V

    .line 2653209
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    const/16 v2, 0xde

    invoke-interface {v0, v4, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2653210
    const-string v0, "CollectionsCollectionFragment"

    const-string v1, "onFetchFailed"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2653211
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    if-eqz v0, :cond_0

    .line 2653212
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    invoke-virtual {v0, v3}, LX/J9W;->a(Z)V

    .line 2653213
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->c:LX/J8y;

    if-eqz v0, :cond_1

    .line 2653214
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->c:LX/J8y;

    sget-object v1, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->D:LX/J8x;

    invoke-virtual {v0, v1}, LX/J8y;->b(LX/J8x;)V

    .line 2653215
    :cond_1
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_3

    .line 2653216
    :cond_2
    :goto_0
    return-void

    .line 2653217
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->A:Z

    .line 2653218
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    if-eqz v0, :cond_4

    .line 2653219
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    invoke-interface {v0}, LX/62k;->f()V

    .line 2653220
    :cond_4
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->x:Lcom/facebook/feed/banner/GenericNotificationBanner;

    if-eqz v0, :cond_2

    .line 2653221
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->x:Lcom/facebook/feed/banner/GenericNotificationBanner;

    sget-object v1, LX/DBa;->FETCH_TIMELINE_FAILED:LX/DBa;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    goto :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2653222
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 2653223
    const-string v1, "profile_id"

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->F:LX/9lP;

    .line 2653224
    iget-object p0, v2, LX/9lP;->a:Ljava/lang/String;

    move-object v2, p0

    .line 2653225
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653226
    return-object v0
.end method

.method public final mJ_()V
    .locals 5

    .prologue
    const v4, 0xbf0002

    const/4 v3, 0x2

    .line 2653227
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-interface {v0, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2653228
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->d(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v4, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2653229
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->l()V

    .line 2653230
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2653231
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->I:LX/B0O;

    if-eqz v0, :cond_0

    .line 2653232
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->I:LX/B0O;

    invoke-virtual {v0}, LX/B0O;->c()V

    .line 2653233
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    if-eqz v0, :cond_1

    .line 2653234
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    invoke-interface {v0}, LX/62k;->d()V

    .line 2653235
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-interface {v0, v4, v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2653236
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xbf0005

    iget v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2653237
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xbf0006

    iget v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2653238
    return-void

    .line 2653239
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->l:LX/J90;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/J90;->a(Ljava/lang/String;)V

    .line 2653240
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a(Z)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x703d2722

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2653241
    const v0, 0x7f0302b7

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2653242
    const v0, 0x7f0d09a1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/62k;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    .line 2653243
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    new-instance v3, LX/J9h;

    invoke-direct {v3, p0}, LX/J9h;-><init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V

    invoke-interface {v0, v3}, LX/62k;->setOnRefreshListener(LX/62n;)V

    .line 2653244
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    invoke-interface {v0}, LX/62k;->c()Landroid/view/ViewGroup;

    move-result-object v0

    const v3, 0x102000a

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    .line 2653245
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    const v3, 0x1020004

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2653246
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2653247
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    .line 2653248
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v3

    .line 2653249
    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v4, v0, v6, v5}, Lcom/facebook/widget/listview/BetterListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2653250
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v3, v6, v5}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2653251
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2653252
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2653253
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    const/high16 v3, 0x40000

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setDescendantFocusability(I)V

    .line 2653254
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/J9W;->a(Z)V

    .line 2653255
    const v0, 0x7f0d08bd

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/banner/GenericNotificationBanner;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->x:Lcom/facebook/feed/banner/GenericNotificationBanner;

    .line 2653256
    new-instance v0, LX/J9i;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, p0, v3}, LX/J9i;-><init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->z:LX/1PF;

    .line 2653257
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0xbf0001

    iget v4, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    const/16 v5, 0x2d

    invoke-interface {v0, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2653258
    const v0, -0x51f7a4f8

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-object v2

    .line 2653259
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x16727d15

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2653260
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->m:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2653261
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->m:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2653262
    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->m:LX/1Ck;

    .line 2653263
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->I:LX/B0O;

    if-eqz v1, :cond_1

    .line 2653264
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->I:LX/B0O;

    invoke-virtual {v1}, LX/B0O;->b()V

    .line 2653265
    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->I:LX/B0O;

    .line 2653266
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    if-eqz v1, :cond_2

    .line 2653267
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    invoke-virtual {v1, v2}, LX/3tK;->a(Landroid/database/Cursor;)V

    .line 2653268
    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    .line 2653269
    :cond_2
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v1, :cond_3

    .line 2653270
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0xbf0001

    iget v3, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    const/4 v4, 0x4

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2653271
    :cond_3
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2653272
    const/16 v1, 0x2b

    const v2, 0x1cca5ac3    # 1.33907E-21f

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x43dd302c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2653273
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2653274
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    if-eqz v1, :cond_0

    .line 2653275
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    invoke-interface {v1, v2}, LX/62k;->setOnRefreshListener(LX/62n;)V

    .line 2653276
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v1, :cond_1

    .line 2653277
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2653278
    :cond_1
    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    .line 2653279
    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    .line 2653280
    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->x:Lcom/facebook/feed/banner/GenericNotificationBanner;

    .line 2653281
    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->z:LX/1PF;

    .line 2653282
    const/16 v1, 0x2b

    const v2, -0x7dcaaf72

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7333ae43

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2653283
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->z:LX/1PF;

    if-eqz v1, :cond_0

    .line 2653284
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->z:LX/1PF;

    invoke-virtual {v1}, LX/14l;->b()V

    .line 2653285
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->w:LX/1B1;

    if-eqz v1, :cond_1

    .line 2653286
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->w:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->e:LX/J9y;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2653287
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->c:LX/J8y;

    sget-object v2, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->D:LX/J8x;

    invoke-virtual {v1, v2}, LX/J8y;->b(LX/J8x;)V

    .line 2653288
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2653289
    const/16 v1, 0x2b

    const v2, -0x24c8bd95

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5d122da8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2653290
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->z:LX/1PF;

    if-eqz v1, :cond_0

    .line 2653291
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->z:LX/1PF;

    invoke-virtual {v1}, LX/14l;->a()V

    .line 2653292
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->w:LX/1B1;

    if-eqz v1, :cond_1

    .line 2653293
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->w:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->e:LX/J9y;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2653294
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2653295
    const/16 v1, 0x2b

    const v2, 0x2c9c4f6b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2653296
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2653297
    const-string v0, "profile_id"

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->F:LX/9lP;

    .line 2653298
    iget-object v2, v1, LX/9lP;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2653299
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2653300
    const-string v0, "section_id"

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->G:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2653301
    const-string v0, "collection_id"

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->H:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2653302
    const-string v0, "friendship_status"

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->F:LX/9lP;

    .line 2653303
    iget-object v2, v1, LX/9lP;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v1, v2

    .line 2653304
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2653305
    const-string v0, "subscribe_status"

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->F:LX/9lP;

    .line 2653306
    iget-object v2, v1, LX/9lP;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object v1, v2

    .line 2653307
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2653308
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2653309
    const-string v0, "view_name"

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2653310
    const-string v0, "collections_icon"

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2653311
    const-string v0, "collections_section_type"

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2653312
    const-string v0, "collection"

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;->a()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2653313
    :cond_0
    return-void
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const v4, 0xbf0003

    .line 2653314
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-nez v0, :cond_1

    .line 2653315
    :cond_0
    :goto_0
    return-void

    .line 2653316
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-interface {v0, v4, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2653317
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->d(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v4, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2653318
    iget v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->K:I

    if-eqz v0, :cond_0

    .line 2653319
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result v2

    .line 2653320
    invoke-static {p2, p3, p4}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a(III)Z

    move-result v0

    .line 2653321
    if-eqz v0, :cond_6

    if-nez v2, :cond_3

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    .line 2653322
    iget-boolean v3, v0, LX/J9W;->n:Z

    move v0, v3

    .line 2653323
    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    .line 2653324
    iget-object v3, v0, LX/J9W;->s:LX/0us;

    if-eqz v3, :cond_2

    iget-object v3, v0, LX/J9W;->s:LX/0us;

    invoke-interface {v3}, LX/0us;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, LX/J9W;->s:LX/0us;

    invoke-interface {v3}, LX/0us;->a()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_8

    .line 2653325
    :cond_2
    const/4 v3, 0x0

    .line 2653326
    :goto_1
    move v0, v3

    .line 2653327
    if-eqz v0, :cond_6

    :cond_3
    move v0, v1

    .line 2653328
    :goto_2
    if-eqz v0, :cond_4

    .line 2653329
    if-eqz v2, :cond_7

    .line 2653330
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->I:LX/B0O;

    if-eqz v0, :cond_4

    .line 2653331
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->I:LX/B0O;

    invoke-virtual {v0}, LX/B0O;->d()Z

    move-result v0

    .line 2653332
    if-eqz v0, :cond_4

    .line 2653333
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-interface {v0, v4, v1, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2653334
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-interface {v0, v4, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    .line 2653335
    iget v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->C:I

    if-eq v0, p2, :cond_5

    .line 2653336
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->l()V

    .line 2653337
    :cond_5
    iput p2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->C:I

    goto :goto_0

    .line 2653338
    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 2653339
    :cond_7
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    invoke-interface {v0, v4, v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2653340
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    invoke-virtual {v0, v1}, LX/J9W;->a(Z)V

    .line 2653341
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    .line 2653342
    iget-object v1, v0, LX/J9W;->s:LX/0us;

    if-nez v1, :cond_9

    .line 2653343
    const/4 v1, 0x0

    .line 2653344
    :goto_4
    move-object v0, v1

    .line 2653345
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result v1

    if-nez v1, :cond_a

    const/4 v1, 0x1

    :goto_5
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2653346
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->m:LX/1Ck;

    const-string v2, "items page"

    new-instance v3, LX/J9f;

    invoke-direct {v3, p0, v0}, LX/J9f;-><init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;Ljava/lang/String;)V

    new-instance v5, LX/J9g;

    invoke-direct {v5, p0}, LX/J9g;-><init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V

    invoke-virtual {v1, v2, v3, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2653347
    goto :goto_3

    :cond_8
    const/4 v3, 0x1

    goto :goto_1

    :cond_9
    iget-object v1, v0, LX/J9W;->s:LX/0us;

    invoke-interface {v1}, LX/0us;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 2653348
    :cond_a
    const/4 v1, 0x0

    goto :goto_5
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2653349
    iput p2, p0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->K:I

    .line 2653350
    return-void
.end method
