.class public Lcom/facebook/timeline/aboutpage/ProfileExperienceDeleteParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/aboutpage/ProfileExperienceDeleteParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2652686
    new-instance v0, LX/J9H;

    invoke-direct {v0}, LX/J9H;-><init>()V

    sput-object v0, Lcom/facebook/timeline/aboutpage/ProfileExperienceDeleteParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2652687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652688
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/ProfileExperienceDeleteParams;->a:Ljava/lang/String;

    .line 2652689
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2652690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652691
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/ProfileExperienceDeleteParams;->a:Ljava/lang/String;

    .line 2652692
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2652693
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2652694
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/ProfileExperienceDeleteParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2652695
    return-void
.end method
