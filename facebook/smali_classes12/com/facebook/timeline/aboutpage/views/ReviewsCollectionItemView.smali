.class public Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/JCi;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/intent/feed/IFeedIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/RatingBar;

.field private f:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2664245
    const-class v0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;

    const-string v1, "collections_collection"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2664246
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2664247
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->a()V

    .line 2664248
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2664242
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2664243
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->a()V

    .line 2664244
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2664235
    const-class v0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2664236
    const v0, 0x7f0302b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2664237
    const v0, 0x7f0d0993

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->d:Landroid/widget/TextView;

    .line 2664238
    const v0, 0x7f0d0994

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->e:Landroid/widget/RatingBar;

    .line 2664239
    const v0, 0x7f0d0995

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->f:Landroid/widget/TextView;

    .line 2664240
    const v0, 0x7f0d0992

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2664241
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindCollectionItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2664216
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->e()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->e()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2664217
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v5, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2664218
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2664219
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2664220
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_5

    .line 2664221
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->e:Landroid/widget/RatingBar;

    invoke-virtual {v0, v4}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 2664222
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2664223
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2664224
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2664225
    invoke-virtual {p0, v5}, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2664226
    :goto_4
    return-void

    .line 2664227
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->e()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0

    .line 2664228
    :cond_4
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2664229
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->d:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2664230
    :cond_5
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->e:Landroid/widget/RatingBar;

    invoke-virtual {v0, v3}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 2664231
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->n()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->e:Landroid/widget/RatingBar;

    invoke-virtual {v1, v0, v3}, LX/15i;->l(II)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {v2, v0}, Landroid/widget/RatingBar;->setRating(F)V

    goto :goto_2

    .line 2664232
    :cond_6
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->f:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2664233
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 2664234
    :cond_7
    new-instance v0, LX/JDZ;

    invoke-direct {v0, p0, p1}, LX/JDZ;-><init>(Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/ReviewsCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V
    .locals 0

    .prologue
    .line 2664215
    return-void
.end method
