.class public Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/J9y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/ImageView;

.field private final h:Landroid/view/View;

.field private final i:Landroid/view/View;

.field private final j:Landroid/widget/ImageView;

.field private final k:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2663170
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2663171
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2663156
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2663157
    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->b:Z

    .line 2663158
    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->c:Z

    .line 2663159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->d:Z

    .line 2663160
    const-class v0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2663161
    const v0, 0x7f0302b4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2663162
    const v0, 0x7f0d099a

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->e:Landroid/widget/TextView;

    .line 2663163
    const v0, 0x7f0d099b

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->f:Landroid/widget/TextView;

    .line 2663164
    const v0, 0x7f0d099c

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->g:Landroid/widget/ImageView;

    .line 2663165
    const v0, 0x7f0d0999

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->h:Landroid/view/View;

    .line 2663166
    const v0, 0x7f0d099d

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->i:Landroid/view/View;

    .line 2663167
    const v0, 0x7f0d099e

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->j:Landroid/widget/ImageView;

    .line 2663168
    const v0, 0x7f0d099f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2663169
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;

    invoke-static {v0}, LX/J9y;->a(LX/0QB;)LX/J9y;

    move-result-object v0

    check-cast v0, LX/J9y;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a:LX/J9y;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/1Fb;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/JBL;)V
    .locals 11

    .prologue
    .line 2663111
    if-eqz p1, :cond_0

    .line 2663112
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->e:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663113
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2663114
    :goto_0
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    if-eqz p9, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2663115
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-static {v2, v3}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 2663116
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->e:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2663117
    :goto_1
    if-eqz p2, :cond_2

    .line 2663118
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->f:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663119
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->f:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2663120
    :goto_2
    iget-boolean v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->d:Z

    if-eqz v1, :cond_3

    if-eqz p5, :cond_3

    .line 2663121
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->g:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2663122
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->h:Landroid/view/View;

    const v2, 0x7f021943

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2663123
    iget-object v10, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->h:Landroid/view/View;

    new-instance v1, LX/JD5;

    move-object v2, p0

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object v5, p1

    move-object/from16 v6, p10

    move-object/from16 v7, p9

    move-object/from16 v8, p8

    move-object v9, p4

    invoke-direct/range {v1 .. v9}, LX/JD5;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/1Fb;Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663124
    :goto_3
    iget-boolean v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->b:Z

    if-eqz v1, :cond_4

    if-eqz p7, :cond_4

    .line 2663125
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->i:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2663126
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->j:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2663127
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->j:Landroid/widget/ImageView;

    new-instance v2, LX/JD6;

    move-object/from16 v0, p7

    invoke-direct {v2, p0, v0}, LX/JD6;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663128
    :goto_4
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->g:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2663129
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b2496

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 2663130
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->j:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_5

    .line 2663131
    invoke-static {v1, v2}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 2663132
    :goto_5
    iget-boolean v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->c:Z

    if-eqz v1, :cond_7

    .line 2663133
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->k:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2663134
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->k:Lcom/facebook/resources/ui/FbTextView;

    new-instance v2, LX/JD7;

    invoke-direct {v2, p0, p3}, LX/JD7;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663135
    :goto_6
    return-void

    .line 2663136
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->e:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663137
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->e:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2663138
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0052

    invoke-static {v2, v3}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 2663139
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->e:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_1

    .line 2663140
    :cond_2
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->f:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663141
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->f:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2663142
    :cond_3
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->g:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2663143
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->h:Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0048

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2663144
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->h:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 2663145
    :cond_4
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->i:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2663146
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->j:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2663147
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->j:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 2663148
    :cond_5
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->f:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_6

    .line 2663149
    invoke-static {v1, v2}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    goto/16 :goto_5

    .line 2663150
    :cond_6
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto/16 :goto_5

    .line 2663151
    :cond_7
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->k:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2663152
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->k:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_6
.end method


# virtual methods
.method public final a(LX/JBL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/1Fb;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V
    .locals 11
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModelWithCollection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2663104
    if-nez p2, :cond_1

    invoke-interface {p1}, LX/JBK;->mW_()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2663105
    invoke-interface {p1}, LX/JBK;->mW_()Ljava/lang/String;

    move-result-object p2

    move-object v1, p2

    .line 2663106
    :goto_0
    const/4 v3, 0x0

    .line 2663107
    iget-boolean v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->c:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/JBK;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2663108
    invoke-interface {p1}, LX/JBK;->p()Ljava/lang/String;

    move-result-object v3

    :cond_0
    move-object v0, p0

    move-object v2, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object v10, p1

    .line 2663109
    invoke-direct/range {v0 .. v10}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/1Fb;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/JBL;)V

    .line 2663110
    return-void

    :cond_1
    move-object v1, p2

    goto :goto_0
.end method

.method public final a(LX/JD9;)V
    .locals 11

    .prologue
    .line 2663153
    iget-object v0, p1, LX/JD9;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p1, LX/JD9;->c:Ljava/lang/String;

    :goto_0
    iget-object v2, p1, LX/JD9;->d:Ljava/lang/String;

    iget-object v3, p1, LX/JD9;->f:Ljava/lang/String;

    iget-object v4, p1, LX/JD9;->j:Ljava/lang/String;

    iget-object v5, p1, LX/JD9;->k:Ljava/lang/String;

    iget-object v6, p1, LX/JD9;->l:Landroid/os/Bundle;

    iget-object v7, p1, LX/JD9;->m:Ljava/lang/String;

    iget-object v8, p1, LX/JD9;->e:LX/1Fb;

    iget-object v9, p1, LX/JD9;->n:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    iget-object v10, p1, LX/JD9;->g:LX/JBL;

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/1Fb;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/JBL;)V

    .line 2663154
    return-void

    .line 2663155
    :cond_0
    iget-object v1, p1, LX/JD9;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;)V
    .locals 11
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2663099
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_0

    move-object v1, v2

    .line 2663100
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;->a()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    move-object v10, v2

    invoke-direct/range {v0 .. v10}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/1Fb;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/JBL;)V

    .line 2663101
    return-void

    .line 2663102
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionInfoModel;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2663103
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;)V
    .locals 11
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2663096
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;

    .line 2663097
    invoke-virtual {v10}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->mW_()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/JAb;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/JAb;->mU_()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v9

    move-object v0, p0

    move-object v4, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-direct/range {v0 .. v10}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/1Fb;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/JBL;)V

    .line 2663098
    return-void
.end method

.method public final b(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;)V
    .locals 11
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindMoreAboutModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2663087
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel$CollectionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;

    .line 2663088
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_0

    move-object v1, v4

    .line 2663089
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_1

    move-object v2, v4

    .line 2663090
    :goto_1
    invoke-interface {p1}, LX/JAb;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, LX/JAb;->mU_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionWithItemsOrRequestablesModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v9

    move-object v0, p0

    move-object v6, v4

    move-object v8, v4

    invoke-direct/range {v0 .. v10}, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/1Fb;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/JBL;)V

    .line 2663091
    return-void

    .line 2663092
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->o()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2663093
    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2663094
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2663095
    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public setHasCurateButton(Z)V
    .locals 0

    .prologue
    .line 2663081
    iput-boolean p1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->b:Z

    .line 2663082
    return-void
.end method

.method public setHasEditButton(Z)V
    .locals 0

    .prologue
    .line 2663085
    iput-boolean p1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->c:Z

    .line 2663086
    return-void
.end method

.method public setTitleIsLink(Z)V
    .locals 0

    .prologue
    .line 2663083
    iput-boolean p1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionTitleBarView;->d:Z

    .line 2663084
    return-void
.end method
