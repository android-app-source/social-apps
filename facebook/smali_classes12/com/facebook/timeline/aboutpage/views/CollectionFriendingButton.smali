.class public Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;
.super LX/JCq;
.source ""


# instance fields
.field private a:LX/JDK;

.field public b:LX/JCj;

.field private c:LX/2dj;

.field public d:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2662857
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2662858
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2662853
    invoke-direct {p0, p1, p2}, LX/JCq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2662854
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->setClickable(Z)V

    .line 2662855
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->setVisibility(I)V

    .line 2662856
    return-void
.end method

.method public static a(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;)V
    .locals 2

    .prologue
    .line 2662848
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->a:LX/JDK;

    iget-object v0, v0, LX/JDK;->h:LX/JA0;

    invoke-virtual {v0}, LX/JA0;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 2662849
    invoke-virtual {p0, v0}, LX/JCq;->setFriendshipStatus(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2662850
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v1, :cond_0

    .line 2662851
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->b:LX/JCj;

    const v1, 0x7f08159b

    invoke-interface {v0, v1}, LX/JCj;->H_(I)V

    .line 2662852
    :cond_0
    return-void
.end method

.method public static b$redex0(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2662843
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->c:LX/2dj;

    iget-object v0, p1, LX/JA0;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2h8;->TIMELINE_FRIENDS_COLLECTION:LX/2h8;

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, LX/2dj;->b(JLX/2h8;LX/2hC;LX/5P2;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2662844
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p1, v1}, LX/JA0;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2662845
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->a(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;)V

    .line 2662846
    new-instance v1, LX/JCl;

    invoke-direct {v1, p0, p1}, LX/JCl;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2662847
    return-void
.end method

.method public static c$redex0(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V
    .locals 4

    .prologue
    .line 2662837
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->c:LX/2dj;

    iget-object v1, p1, LX/JA0;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v1, LX/2h9;->TIMELINE_FRIENDS_COLLECTION:LX/2h9;

    invoke-virtual {v0, v2, v3, v1}, LX/2dj;->a(JLX/2h9;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2662838
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p1, v1}, LX/JA0;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2662839
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->a(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;)V

    .line 2662840
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->b:LX/JCj;

    const v2, 0x7f08159c

    invoke-interface {v1, v2}, LX/JCj;->H_(I)V

    .line 2662841
    new-instance v1, LX/JCm;

    invoke-direct {v1, p0, p1}, LX/JCm;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2662842
    return-void
.end method

.method public static d(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V
    .locals 4

    .prologue
    .line 2662815
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080fa1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->a:LX/JDK;

    iget-object v3, v3, LX/JDK;->b:LX/175;

    invoke-interface {v3}, LX/175;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2662816
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f081596

    new-instance v2, LX/JCn;

    invoke-direct {v2, p0, p1}, LX/JCn;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0815b3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2662817
    return-void
.end method


# virtual methods
.method public final a(LX/9lP;LX/JDK;LX/JCj;LX/2dj;LX/03V;)V
    .locals 2

    .prologue
    .line 2662824
    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->a:LX/JDK;

    .line 2662825
    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->b:LX/JCj;

    .line 2662826
    iput-object p4, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->c:LX/2dj;

    .line 2662827
    iput-object p5, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->d:LX/03V;

    .line 2662828
    iget-object v0, p2, LX/JDK;->h:LX/JA0;

    if-eqz v0, :cond_0

    .line 2662829
    iget-object v0, p1, LX/9lP;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2662830
    iget-object v1, p2, LX/JDK;->h:LX/JA0;

    iget-object v1, v1, LX/JA0;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2662831
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->setVisibility(I)V

    .line 2662832
    :goto_0
    return-void

    .line 2662833
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->setVisibility(I)V

    .line 2662834
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->a:LX/JDK;

    iget-object v0, v0, LX/JDK;->h:LX/JA0;

    .line 2662835
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->a(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;)V

    .line 2662836
    new-instance v1, LX/JCk;

    invoke-direct {v1, p0, v0}, LX/JCk;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(LX/JA0;)V
    .locals 4

    .prologue
    .line 2662818
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {p1, v0}, LX/JA0;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2662819
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->a(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;)V

    .line 2662820
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->b:LX/JCj;

    const v1, 0x7f08159d    # 1.8088723E38f

    invoke-interface {v0, v1}, LX/JCj;->H_(I)V

    .line 2662821
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->c:LX/2dj;

    iget-object v1, p1, LX/JA0;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v1, LX/2hB;->TIMELINE_FRIENDS_COLLECTION:LX/2hB;

    invoke-virtual {v0, v2, v3, v1}, LX/2dj;->a(JLX/2hB;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2662822
    new-instance v1, LX/JCo;

    invoke-direct {v1, p0, p1}, LX/JCo;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2662823
    return-void
.end method
