.class public Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field private a:LX/JDK;

.field private b:LX/9lP;

.field public c:LX/JCj;

.field private d:LX/3mF;

.field public e:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2662947
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2662948
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2662940
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2662941
    const v0, 0x7f020a33

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setImageResource(I)V

    .line 2662942
    const v0, 0x7f020a36

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setBackgroundResource(I)V

    .line 2662943
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2662944
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setClickable(Z)V

    .line 2662945
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setVisibility(I)V

    .line 2662946
    return-void
.end method

.method public static a(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;)V
    .locals 2

    .prologue
    .line 2662929
    sget-object v0, LX/JCw;->a:[I

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->a:LX/JDK;

    iget-object v1, v1, LX/JDK;->h:LX/JA0;

    invoke-virtual {v1}, LX/JA0;->i()LX/J9z;

    move-result-object v1

    invoke-virtual {v1}, LX/J9z;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2662930
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid group membership status"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2662931
    :pswitch_0
    const v0, 0x7f020a30

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setImageResource(I)V

    .line 2662932
    const v0, 0x7f0815dd

    invoke-direct {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setContentDescription(I)V

    .line 2662933
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->c:LX/JCj;

    const v1, 0x7f0815a1

    invoke-interface {v0, v1}, LX/JCj;->H_(I)V

    .line 2662934
    :goto_0
    return-void

    .line 2662935
    :pswitch_1
    const v0, 0x7f020a30

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setImageResource(I)V

    .line 2662936
    const v0, 0x7f0815e1

    invoke-direct {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setContentDescription(I)V

    goto :goto_0

    .line 2662937
    :pswitch_2
    const v0, 0x7f020a33

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setImageResource(I)V

    .line 2662938
    const v0, 0x7f0815de

    invoke-direct {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setContentDescription(I)V

    goto :goto_0

    .line 2662939
    :pswitch_3
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V
    .locals 3

    .prologue
    .line 2662924
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->d:LX/3mF;

    iget-object v1, p1, LX/JA0;->b:Ljava/lang/String;

    const-string v2, "mobile_collection"

    invoke-virtual {v0, v1, v2}, LX/3mF;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2662925
    sget-object v1, LX/J9z;->REQUESTED:LX/J9z;

    invoke-virtual {p1, v1}, LX/JA0;->a(LX/J9z;)V

    .line 2662926
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->a(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;)V

    .line 2662927
    new-instance v1, LX/JCs;

    invoke-direct {v1, p0, p1}, LX/JCs;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2662928
    return-void
.end method

.method public static b$redex0(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V
    .locals 4

    .prologue
    .line 2662918
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->d:LX/3mF;

    iget-object v1, p1, LX/JA0;->b:Ljava/lang/String;

    const-string v2, "mobile_collection"

    const-string v3, "ALLOW_READD"

    invoke-virtual {v0, v1, v2, v3}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2662919
    sget-object v1, LX/J9z;->CAN_REQUEST:LX/J9z;

    invoke-virtual {p1, v1}, LX/JA0;->a(LX/J9z;)V

    .line 2662920
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->a(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;)V

    .line 2662921
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->c:LX/JCj;

    const v2, 0x7f0815a2

    invoke-interface {v1, v2}, LX/JCj;->H_(I)V

    .line 2662922
    new-instance v1, LX/JCt;

    invoke-direct {v1, p0, p1}, LX/JCt;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2662923
    return-void
.end method

.method public static c$redex0(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V
    .locals 4

    .prologue
    .line 2662895
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0815a5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->a:LX/JDK;

    iget-object v3, v3, LX/JDK;->b:LX/175;

    invoke-interface {v3}, LX/175;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2662896
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0815a6

    new-instance v2, LX/JCu;

    invoke-direct {v2, p0, p1}, LX/JCu;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0815b3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2662897
    return-void
.end method

.method public static d$redex0(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V
    .locals 4

    .prologue
    .line 2662912
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->d:LX/3mF;

    iget-object v1, p1, LX/JA0;->b:Ljava/lang/String;

    const-string v2, "mobile_collection"

    const-string v3, "ALLOW_READD"

    invoke-virtual {v0, v1, v2, v3}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2662913
    sget-object v1, LX/J9z;->CAN_REQUEST:LX/J9z;

    invoke-virtual {p1, v1}, LX/JA0;->a(LX/J9z;)V

    .line 2662914
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->a(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;)V

    .line 2662915
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->c:LX/JCj;

    const v2, 0x7f0815a7

    invoke-interface {v1, v2}, LX/JCj;->H_(I)V

    .line 2662916
    new-instance v1, LX/JCv;

    invoke-direct {v1, p0, p1}, LX/JCv;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2662917
    return-void
.end method

.method private setContentDescription(I)V
    .locals 1

    .prologue
    .line 2662910
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2662911
    return-void
.end method


# virtual methods
.method public final a(LX/9lP;LX/JDK;LX/JCj;LX/3mF;LX/03V;)V
    .locals 2

    .prologue
    .line 2662898
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->b:LX/9lP;

    .line 2662899
    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->a:LX/JDK;

    .line 2662900
    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->c:LX/JCj;

    .line 2662901
    iput-object p4, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->d:LX/3mF;

    .line 2662902
    iput-object p5, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->e:LX/03V;

    .line 2662903
    iget-object v0, p2, LX/JDK;->h:LX/JA0;

    if-nez v0, :cond_0

    .line 2662904
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setVisibility(I)V

    .line 2662905
    :goto_0
    return-void

    .line 2662906
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setVisibility(I)V

    .line 2662907
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->a:LX/JDK;

    iget-object v0, v0, LX/JDK;->h:LX/JA0;

    .line 2662908
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->a(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;)V

    .line 2662909
    new-instance v1, LX/JCr;

    invoke-direct {v1, p0, v0}, LX/JCr;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
