.class public Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/2fp;


# instance fields
.field private final a:Landroid/widget/ImageButton;

.field private final b:Landroid/widget/ImageView;

.field private c:LX/JA0;

.field private d:LX/2fi;

.field private e:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
    .end annotation
.end field

.field public f:LX/03V;

.field private g:LX/1dy;

.field public h:Landroid/app/AlertDialog;

.field private final i:Landroid/graphics/drawable/Drawable;

.field private final j:Landroid/graphics/drawable/Drawable;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private final n:I

.field private final o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2664471
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2664472
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2664456
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2664457
    const v0, 0x7f0314c9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2664458
    const-class v0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2664459
    const v0, 0x7f0d2f23

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    .line 2664460
    const v0, 0x7f0d2f24

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->b:Landroid/widget/ImageView;

    .line 2664461
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2664462
    const v1, 0x7f020a35

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->i:Landroid/graphics/drawable/Drawable;

    .line 2664463
    const v1, 0x7f020a32

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->j:Landroid/graphics/drawable/Drawable;

    .line 2664464
    const v1, 0x7f0810f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->k:Ljava/lang/String;

    .line 2664465
    const v1, 0x7f0810f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->l:Ljava/lang/String;

    .line 2664466
    const v1, 0x7f0810d9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->m:Ljava/lang/String;

    .line 2664467
    const v1, 0x7f0b099e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->n:I

    .line 2664468
    const v1, 0x7f0b099d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->o:I

    .line 2664469
    sget-object v0, LX/1vY;->COLLECTION_ADD_BUTTON:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2664470
    return-void
.end method

.method private a(LX/03V;LX/1dy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2664453
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->f:LX/03V;

    .line 2664454
    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->g:LX/1dy;

    .line 2664455
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {v1}, LX/1dy;->b(LX/0QB;)LX/1dy;

    move-result-object v1

    check-cast v1, LX/1dy;

    invoke-direct {p0, v0, v1}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a(LX/03V;LX/1dy;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2664430
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    invoke-virtual {v0}, LX/JA0;->b()LX/1oP;

    move-result-object v0

    .line 2664431
    if-nez v0, :cond_0

    .line 2664432
    :goto_0
    return-void

    .line 2664433
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/JA0;->a(LX/1oP;)V

    .line 2664434
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->g()V

    .line 2664435
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2664436
    new-instance v1, LX/5vM;

    invoke-direct {v1}, LX/5vM;-><init>()V

    invoke-interface {v0}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v2

    .line 2664437
    iput-object v2, v1, LX/5vM;->a:Ljava/lang/String;

    .line 2664438
    move-object v1, v1

    .line 2664439
    iput-object p2, v1, LX/5vM;->b:Ljava/lang/String;

    .line 2664440
    move-object v1, v1

    .line 2664441
    iput-boolean v3, v1, LX/5vM;->j:Z

    .line 2664442
    move-object v1, v1

    .line 2664443
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/5vM;->c(Ljava/lang/String;)LX/5vM;

    move-result-object v1

    const-string v2, "add_button"

    invoke-virtual {v1, v2}, LX/5vM;->d(Ljava/lang/String;)LX/5vM;

    move-result-object v1

    invoke-virtual {v1}, LX/5vM;->a()Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    move-result-object v1

    .line 2664444
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->d:LX/2fi;

    invoke-interface {v2, p0, v0, v1}, LX/2fi;->b(LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2664445
    new-instance v1, LX/JAe;

    invoke-direct {v1}, LX/JAe;-><init>()V

    .line 2664446
    iput-object p1, v1, LX/JAe;->a:Ljava/lang/String;

    .line 2664447
    move-object v1, v1

    .line 2664448
    const-string v2, ""

    .line 2664449
    iput-object v2, v1, LX/JAe;->b:Ljava/lang/String;

    .line 2664450
    move-object v1, v1

    .line 2664451
    invoke-virtual {v1}, LX/JAe;->a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemContainingMutationFieldsModel;

    move-result-object v1

    .line 2664452
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->g:LX/1dy;

    invoke-virtual {v2, v0, v1}, LX/1dy;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0jT;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;LX/1oP;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2664403
    invoke-interface {p1}, LX/1oP;->d()LX/1oQ;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/1oP;->d()LX/1oQ;

    move-result-object v0

    invoke-interface {v0}, LX/1oQ;->c()Ljava/lang/String;

    move-result-object v0

    .line 2664404
    :goto_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2664405
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->f:LX/03V;

    const-string v1, "TimelineCollectionPlusButton"

    const-string v2, "Could not add item to collection as collection does not support newItemDefaultPrivacy."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2664406
    :goto_1
    return-void

    .line 2664407
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2664408
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    invoke-virtual {v1, p1}, LX/JA0;->a(LX/1oP;)V

    .line 2664409
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->g()V

    .line 2664410
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2664411
    new-instance v1, LX/5vM;

    invoke-direct {v1}, LX/5vM;-><init>()V

    invoke-interface {p1}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v2

    .line 2664412
    iput-object v2, v1, LX/5vM;->a:Ljava/lang/String;

    .line 2664413
    move-object v1, v1

    .line 2664414
    iput-object p3, v1, LX/5vM;->b:Ljava/lang/String;

    .line 2664415
    move-object v1, v1

    .line 2664416
    iput-object v0, v1, LX/5vM;->g:Ljava/lang/String;

    .line 2664417
    move-object v0, v1

    .line 2664418
    iput-boolean v3, v0, LX/5vM;->j:Z

    .line 2664419
    move-object v0, v0

    .line 2664420
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/5vM;->c(Ljava/lang/String;)LX/5vM;

    move-result-object v0

    const-string v1, "add_button"

    invoke-virtual {v0, v1}, LX/5vM;->d(Ljava/lang/String;)LX/5vM;

    move-result-object v0

    invoke-virtual {v0}, LX/5vM;->a()Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    move-result-object v0

    .line 2664421
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->d:LX/2fi;

    invoke-interface {v1, p0, p1, v0}, LX/2fi;->a(LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2664422
    new-instance v1, LX/JAe;

    invoke-direct {v1}, LX/JAe;-><init>()V

    .line 2664423
    iput-object p2, v1, LX/JAe;->a:Ljava/lang/String;

    .line 2664424
    move-object v1, v1

    .line 2664425
    invoke-interface {p1}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v2

    .line 2664426
    iput-object v2, v1, LX/JAe;->b:Ljava/lang/String;

    .line 2664427
    move-object v1, v1

    .line 2664428
    invoke-virtual {v1}, LX/JAe;->a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemContainingMutationFieldsModel;

    move-result-object v1

    .line 2664429
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->g:LX/1dy;

    invoke-virtual {v2, v0, v1}, LX/1dy;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0jT;)V

    goto :goto_1
.end method

.method private b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2664399
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    if-nez v1, :cond_1

    .line 2664400
    :cond_0
    :goto_0
    return v0

    .line 2664401
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    invoke-virtual {v1}, LX/JA0;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2664402
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->e()Z

    move-result v0

    goto :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2664473
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    if-nez v1, :cond_1

    .line 2664474
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    invoke-virtual {v1}, LX/JA0;->b()LX/1oP;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private f()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2664384
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    if-nez v0, :cond_0

    move v0, v1

    .line 2664385
    :goto_0
    return v0

    .line 2664386
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    .line 2664387
    iget-object v2, v0, LX/JA0;->n:LX/0Px;

    move-object v3, v2

    .line 2664388
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 2664389
    goto :goto_0

    .line 2664390
    :cond_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_5

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oP;

    .line 2664391
    invoke-interface {v0}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2664392
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->f:LX/03V;

    const-string v2, "TimelineCollectionPlusButton"

    const-string v3, "Tried to render plus button for collection with no id"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 2664393
    goto :goto_0

    .line 2664394
    :cond_3
    invoke-interface {v0}, LX/1oP;->d()LX/1oQ;

    move-result-object v5

    if-nez v5, :cond_4

    .line 2664395
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->f:LX/03V;

    const-string v3, "TimelineCollectionPlusButton"

    const-string v4, "Tried to render plus button for collection %s with no default privacy"

    invoke-interface {v0}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 2664396
    goto :goto_0

    .line 2664397
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2664398
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2664369
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2664370
    invoke-virtual {p0, v3}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->setVisibility(I)V

    .line 2664371
    :goto_0
    return-void

    .line 2664372
    :cond_0
    invoke-virtual {p0, v2}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->setVisibility(I)V

    .line 2664373
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2664374
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2664375
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2664376
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->h()V

    goto :goto_0

    .line 2664377
    :cond_1
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->i()V

    .line 2664378
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->l:Ljava/lang/String;

    .line 2664379
    :goto_1
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2664380
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2664381
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2664382
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->h()V

    goto :goto_0

    .line 2664383
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->k:Ljava/lang/String;

    goto :goto_1
.end method

.method private h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2664363
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 2664364
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2664365
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->n:I

    invoke-static {v0, v1}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object v0

    .line 2664366
    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 2664367
    :goto_0
    return-void

    .line 2664368
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2664355
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    const v1, 0x7f020a36

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 2664356
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->j:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2664357
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2664358
    iget v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->o:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2664359
    iget v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->o:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2664360
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2664361
    return-void

    .line 2664362
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->i:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public static j(Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;)V
    .locals 8

    .prologue
    .line 2664338
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    .line 2664339
    iget-object v1, v0, LX/JA0;->a:Ljava/lang/String;

    move-object v2, v1

    .line 2664340
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    .line 2664341
    iget-object v1, v0, LX/JA0;->b:Ljava/lang/String;

    move-object v3, v1

    .line 2664342
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2664343
    :cond_0
    :goto_0
    return-void

    .line 2664344
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    .line 2664345
    iget-object v1, v0, LX/JA0;->n:LX/0Px;

    move-object v4, v1

    .line 2664346
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2664347
    invoke-direct {p0, v2, v3}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2664348
    :cond_2
    if-eqz v4, :cond_0

    .line 2664349
    new-instance v5, LX/4mb;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v5, v0}, LX/4mb;-><init>(Landroid/content/Context;)V

    .line 2664350
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v6, :cond_3

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oP;

    .line 2664351
    invoke-interface {v0}, LX/1oP;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v7, LX/JDc;

    invoke-direct {v7, p0, v4, v2, v3}, LX/JDc;-><init>(Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;LX/0Px;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v0, v7}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    .line 2664352
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2664353
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->m:Ljava/lang/String;

    new-instance v1, LX/JDd;

    invoke-direct {v1, p0}, LX/JDd;-><init>(Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;)V

    invoke-virtual {v5, v0, v1}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    .line 2664354
    invoke-virtual {v5}, LX/4mb;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->h:Landroid/app/AlertDialog;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2664336
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2664337
    return-void
.end method

.method public final a(LX/1oP;LX/5vL;)V
    .locals 2

    .prologue
    .line 2664328
    sget-object v0, LX/5vL;->ADD:LX/5vL;

    if-ne p2, v0, :cond_1

    .line 2664329
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/JA0;->a(LX/1oP;)V

    .line 2664330
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->g()V

    .line 2664331
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2664332
    return-void

    .line 2664333
    :cond_1
    sget-object v0, LX/5vL;->REMOVE:LX/5vL;

    if-ne p2, v0, :cond_0

    .line 2664334
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    invoke-virtual {v0, p1}, LX/JA0;->a(LX/1oP;)V

    .line 2664335
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->g()V

    goto :goto_0
.end method

.method public final a(LX/JA0;LX/2fi;Ljava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionsDisplaySurfaceValue;
        .end annotation
    .end param

    .prologue
    .line 2664318
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->h:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    if-eq v0, p1, :cond_0

    .line 2664319
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->h:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 2664320
    :cond_0
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->c:LX/JA0;

    .line 2664321
    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->d:LX/2fi;

    .line 2664322
    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->e:Ljava/lang/String;

    .line 2664323
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->g()V

    .line 2664324
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 2664325
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    new-instance v1, LX/JDb;

    invoke-direct {v1, p0}, LX/JDb;-><init>(Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2664326
    :goto_0
    return-void

    .line 2664327
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 2664315
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 2664316
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->h()V

    .line 2664317
    return-void
.end method
