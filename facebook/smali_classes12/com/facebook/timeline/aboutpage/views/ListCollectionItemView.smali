.class public Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/2fi;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/JCj;


# static fields
.field private static final i:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/2dj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3mF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/timeline/services/ProfileActionClient;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1nG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/intent/feed/IFeedIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/JDK;

.field private k:LX/9lP;

.field public l:Landroid/widget/TextView;

.field public m:Landroid/widget/TextView;

.field public n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public o:Landroid/widget/FrameLayout;

.field public p:Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

.field public q:Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

.field public r:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

.field public s:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

.field public t:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

.field public u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

.field public v:Landroid/widget/FrameLayout;

.field public w:Ljava/lang/String;

.field private final x:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2663907
    const-class v0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    const-string v1, "collections_collection"

    const-string v2, "thumbnail"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->i:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 2663908
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2663909
    new-instance v0, LX/JDM;

    invoke-direct {v0, p0}, LX/JDM;-><init>(Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->x:Landroid/view/View$OnClickListener;

    .line 2663910
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    invoke-static {v0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v3

    check-cast v3, LX/2dj;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v4

    check-cast v4, LX/3mF;

    invoke-static {v0}, Lcom/facebook/timeline/services/ProfileActionClient;->b(LX/0QB;)Lcom/facebook/timeline/services/ProfileActionClient;

    move-result-object v5

    check-cast v5, Lcom/facebook/timeline/services/ProfileActionClient;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v7

    check-cast v7, LX/1nG;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v8

    check-cast v8, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    iput-object v3, v2, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a:LX/2dj;

    iput-object v4, v2, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->b:LX/3mF;

    iput-object v5, v2, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->c:Lcom/facebook/timeline/services/ProfileActionClient;

    iput-object v6, v2, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->d:LX/03V;

    iput-object v7, v2, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->e:LX/1nG;

    iput-object v8, v2, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object p1, v2, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->g:Ljava/util/concurrent/Executor;

    iput-object v0, v2, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->h:LX/0aG;

    .line 2663911
    const v0, 0x7f0302a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2663912
    const v0, 0x7f0d0981

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->l:Landroid/widget/TextView;

    .line 2663913
    const v0, 0x7f0d0982

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->m:Landroid/widget/TextView;

    .line 2663914
    const v0, 0x7f0d097a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2663915
    const v0, 0x7f0d0979

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->o:Landroid/widget/FrameLayout;

    .line 2663916
    const v0, 0x7f0d0983

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->q:Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

    .line 2663917
    const v0, 0x7f0d097d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->r:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    .line 2663918
    const v0, 0x7f0d097e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    .line 2663919
    const v0, 0x7f0d097f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->t:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    .line 2663920
    const v0, 0x7f0d097c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    .line 2663921
    const v0, 0x7f0d097b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->v:Landroid/widget/FrameLayout;

    .line 2663922
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->w:Ljava/lang/String;

    .line 2663923
    return-void
.end method

.method private a(LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1oP;",
            "Lcom/facebook/story/UpdateTimelineAppCollectionParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2663898
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2663899
    const-string v1, "timelineAppCollectionParamsKey"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2663900
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->h:LX/0aG;

    const-string v2, "update_timeline_app_collection_in_timeline"

    const v3, -0x57b20a5c

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startTryNotToUseMainThread()LX/1ML;

    move-result-object v0

    .line 2663901
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    .line 2663902
    iget-object v2, p2, Lcom/facebook/story/UpdateTimelineAppCollectionParams;->c:LX/5vL;

    move-object v2, v2

    .line 2663903
    iput-object v2, v1, LX/JDK;->k:LX/5vL;

    .line 2663904
    sget-object v3, LX/JDJ;->REQUEST_PENDING:LX/JDJ;

    iput-object v3, v1, LX/JDK;->l:LX/JDJ;

    .line 2663905
    new-instance v1, LX/JDN;

    invoke-direct {v1, p0, p1, p2}, LX/JDN;-><init>(Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2663906
    return-object v0
.end method

.method private a(LX/1oP;LX/5vL;)V
    .locals 2

    .prologue
    .line 2663895
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->q:Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->a(LX/1oP;LX/5vL;)V

    .line 2663896
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->m:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663897
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2663889
    if-eqz p0, :cond_0

    .line 2663890
    if-eqz p1, :cond_1

    .line 2663891
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663892
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2663893
    :cond_0
    :goto_0
    return-void

    .line 2663894
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2663882
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v0, v0, LX/JDK;->h:LX/JA0;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v0, v0, LX/JDK;->h:LX/JA0;

    invoke-virtual {v0}, LX/JA0;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2663883
    const v0, 0x7f020274

    .line 2663884
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->setBackgroundResource(I)V

    .line 2663885
    return-void

    .line 2663886
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    .line 2663887
    iget-boolean v1, v0, LX/JDK;->i:Z

    move v0, v1

    .line 2663888
    if-eqz v0, :cond_1

    const v0, 0x7f020276

    goto :goto_0

    :cond_1
    const v0, 0x7f020275

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2663924
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    .line 2663925
    iget-object v1, v0, LX/JDK;->h:LX/JA0;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/JDK;->h:LX/JA0;

    invoke-virtual {v1}, LX/JA0;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2663926
    const v1, 0x7f0a0970

    .line 2663927
    :goto_0
    move v0, v1

    .line 2663928
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->setBackgroundColor(I)V

    .line 2663929
    return-void

    .line 2663930
    :cond_0
    iget-boolean v1, v0, LX/JDK;->i:Z

    move v1, v1

    .line 2663931
    if-eqz v1, :cond_1

    const v1, 0x7f0a0971

    goto :goto_0

    :cond_1
    const v1, 0x7f0a0048

    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2663878
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/StateListDrawable;

    if-eqz v0, :cond_0

    .line 2663879
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->c()V

    .line 2663880
    :goto_0
    return-void

    .line 2663881
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->d()V

    goto :goto_0
.end method

.method private setupButton(LX/JDK;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    .line 2663848
    iget-object v0, p1, LX/JDK;->g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, LX/JDK;->h:LX/JA0;

    if-eqz v0, :cond_0

    .line 2663849
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->r:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->k:LX/9lP;

    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a:LX/2dj;

    iget-object v5, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->d:LX/03V;

    move-object v2, p1

    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->a(LX/9lP;LX/JDK;LX/JCj;LX/2dj;LX/03V;)V

    .line 2663850
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->t:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setVisibility(I)V

    .line 2663851
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->setVisibility(I)V

    .line 2663852
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setVisibility(I)V

    .line 2663853
    :goto_0
    return-void

    .line 2663854
    :cond_0
    iget-object v0, p1, LX/JDK;->g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SUBSCRIPTIONS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-eq v0, v1, :cond_1

    iget-object v0, p1, LX/JDK;->g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SUBSCRIBERS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p1, LX/JDK;->h:LX/JA0;

    if-eqz v0, :cond_2

    .line 2663855
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->k:LX/9lP;

    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a:LX/2dj;

    iget-object v5, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->c:Lcom/facebook/timeline/services/ProfileActionClient;

    iget-object v6, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->d:LX/03V;

    iget-object v7, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->g:Ljava/util/concurrent/Executor;

    move-object v2, p1

    move-object v3, p0

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->a(LX/9lP;LX/JDK;LX/JCj;LX/2dj;Lcom/facebook/timeline/services/ProfileActionClient;LX/03V;Ljava/util/concurrent/Executor;)V

    .line 2663856
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->setVisibility(I)V

    .line 2663857
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->r:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->setVisibility(I)V

    .line 2663858
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->t:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setVisibility(I)V

    goto :goto_0

    .line 2663859
    :cond_2
    iget-object v0, p1, LX/JDK;->g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->GROUPS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne v0, v1, :cond_3

    iget-object v0, p1, LX/JDK;->h:LX/JA0;

    if-eqz v0, :cond_3

    .line 2663860
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->t:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->k:LX/9lP;

    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->b:LX/3mF;

    iget-object v5, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->d:LX/03V;

    move-object v2, p1

    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->a(LX/9lP;LX/JDK;LX/JCj;LX/3mF;LX/03V;)V

    .line 2663861
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->setVisibility(I)V

    .line 2663862
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->r:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->setVisibility(I)V

    .line 2663863
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setVisibility(I)V

    goto :goto_0

    .line 2663864
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->k:LX/9lP;

    invoke-virtual {v0}, LX/9lP;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2663865
    iget-boolean v0, p1, LX/JDK;->i:Z

    move v0, v0

    .line 2663866
    if-nez v0, :cond_4

    .line 2663867
    iget-boolean v0, p1, LX/JDK;->j:Z

    move v0, v0

    .line 2663868
    if-nez v0, :cond_4

    .line 2663869
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->setVisibility(I)V

    .line 2663870
    :goto_1
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->r:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->setVisibility(I)V

    .line 2663871
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->t:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->setVisibility(I)V

    .line 2663872
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    invoke-virtual {v0, v8}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setVisibility(I)V

    goto :goto_0

    .line 2663873
    :cond_4
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    .line 2663874
    iget-boolean v1, v0, LX/JDK;->i:Z

    move v0, v1

    .line 2663875
    if-eqz v0, :cond_5

    const-string v0, "native_self_about_recommendations"

    .line 2663876
    :goto_2
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    iget-object v2, p1, LX/JDK;->h:LX/JA0;

    invoke-virtual {v1, v2, p0, v0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->a(LX/JA0;LX/2fi;Ljava/lang/String;)V

    goto :goto_1

    .line 2663877
    :cond_5
    const-string v0, "native_friend_about_page"

    goto :goto_2
.end method


# virtual methods
.method public final H_(I)V
    .locals 2

    .prologue
    .line 2663844
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2663845
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->m:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 2663846
    :cond_0
    :goto_0
    return-void

    .line 2663847
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2fp;",
            "LX/1oP;",
            "Lcom/facebook/story/UpdateTimelineAppCollectionParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2663836
    sget-object v0, LX/5vL;->ADD:LX/5vL;

    invoke-direct {p0, p2, v0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a(LX/1oP;LX/5vL;)V

    .line 2663837
    new-instance v0, LX/5vM;

    invoke-direct {v0, p3}, LX/5vM;-><init>(Lcom/facebook/story/UpdateTimelineAppCollectionParams;)V

    sget-object v1, LX/5vL;->ADD:LX/5vL;

    .line 2663838
    iput-object v1, v0, LX/5vM;->c:LX/5vL;

    .line 2663839
    move-object v0, v0

    .line 2663840
    invoke-virtual {v0}, LX/5vM;->a()Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    move-result-object v0

    .line 2663841
    invoke-direct {p0, p2, v0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a(LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2663842
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->e()V

    .line 2663843
    return-object v0
.end method

.method public final a(LX/JDK;LX/9lP;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 2663775
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    .line 2663776
    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->k:LX/9lP;

    .line 2663777
    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->l:Landroid/widget/TextView;

    iget-object v0, p1, LX/JDK;->b:LX/175;

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_0
    invoke-static {v4, v0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 2663778
    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->m:Landroid/widget/TextView;

    iget-object v0, p1, LX/JDK;->c:LX/174;

    if-nez v0, :cond_5

    move-object v0, v1

    :goto_1
    invoke-static {v4, v0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 2663779
    iget-object v0, p1, LX/JDK;->g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->GROUPS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne v0, v4, :cond_6

    .line 2663780
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->p:Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

    if-nez v0, :cond_0

    .line 2663781
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b0dc6

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2663782
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2663783
    const v5, 0x7f0302a7

    const/4 p2, 0x0

    invoke-virtual {v0, v5, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->p:Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

    .line 2663784
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->o:Landroid/widget/FrameLayout;

    iget-object v5, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->p:Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

    new-instance p2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2663785
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->p:Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

    move-object v0, v0

    .line 2663786
    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->p:Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

    .line 2663787
    :cond_0
    const/4 v0, 0x0

    .line 2663788
    if-eqz p1, :cond_1

    iget-object v4, p1, LX/JDK;->h:LX/JA0;

    if-eqz v4, :cond_1

    iget-object v4, p1, LX/JDK;->h:LX/JA0;

    iget-object v4, v4, LX/JA0;->d:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    if-eqz v4, :cond_1

    iget-object v4, p1, LX/JDK;->h:LX/JA0;

    iget-object v4, v4, LX/JA0;->d:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2663789
    iget-object v0, p1, LX/JDK;->h:LX/JA0;

    iget-object v0, v0, LX/JA0;->d:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;->a()LX/0Px;

    move-result-object v0

    .line 2663790
    :cond_1
    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->p:Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

    invoke-virtual {v4, v0}, Lcom/facebook/timeline/aboutpage/views/IconFacepileView;->b(Ljava/util/List;)V

    .line 2663791
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 2663792
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2663793
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->p:Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/aboutpage/views/IconFacepileView;->setVisibility(I)V

    .line 2663794
    :cond_2
    :goto_2
    invoke-direct {p0, p1}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->setupButton(LX/JDK;)V

    .line 2663795
    iget-object v0, p1, LX/JDK;->l:LX/JDJ;

    move-object v4, v0

    .line 2663796
    iget-object v0, p1, LX/JDK;->k:LX/5vL;

    move-object v0, v0

    .line 2663797
    sget-object v5, LX/JDJ;->REQUEST_FAILED:LX/JDJ;

    invoke-virtual {v4, v5}, LX/JDJ;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2663798
    iget-object v5, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->q:Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

    invoke-virtual {v5, v0}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->a(LX/5vL;)V

    .line 2663799
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->q:Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->setVisibility(I)V

    .line 2663800
    :goto_3
    sget-object v0, LX/JDJ;->REQUEST_PENDING:LX/JDJ;

    invoke-virtual {v4, v0}, LX/JDJ;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2663801
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->setEnabled(Z)V

    .line 2663802
    :goto_4
    const/4 v6, 0x0

    .line 2663803
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v0, v0, LX/JDK;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v0, v0, LX/JDK;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x4c808d5

    if-ne v0, v2, :cond_14

    .line 2663804
    sget-object v0, LX/0ax;->bz:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v2, v2, LX/JDK;->a:Ljava/lang/String;

    invoke-static {v0, v2, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2663805
    :cond_3
    :goto_5
    move-object v0, v0

    .line 2663806
    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->w:Ljava/lang/String;

    .line 2663807
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->w:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 2663808
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->x:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663809
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->c()V

    .line 2663810
    :goto_6
    return-void

    .line 2663811
    :cond_4
    iget-object v0, p1, LX/JDK;->b:LX/175;

    invoke-interface {v0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2663812
    :cond_5
    iget-object v0, p1, LX/JDK;->c:LX/174;

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2663813
    :cond_6
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_7

    .line 2663814
    iget-object v0, p1, LX/JDK;->d:LX/1Fb;

    if-eqz v0, :cond_8

    .line 2663815
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v4, p1, LX/JDK;->d:LX/1Fb;

    invoke-interface {v4}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2663816
    :cond_7
    :goto_7
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2663817
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020272

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 2663818
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2663819
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->p:Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

    if-eqz v0, :cond_2

    .line 2663820
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->p:Lcom/facebook/timeline/aboutpage/views/IconFacepileView;

    invoke-virtual {v0, v6}, Lcom/facebook/timeline/aboutpage/views/IconFacepileView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2663821
    :cond_8
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v4, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_7

    .line 2663822
    :cond_9
    iget-object v5, p1, LX/JDK;->h:LX/JA0;

    if-eqz v5, :cond_a

    iget-object v5, p1, LX/JDK;->h:LX/JA0;

    invoke-virtual {v5}, LX/JA0;->c()Z

    move-result v5

    if-nez v5, :cond_e

    .line 2663823
    :cond_a
    sget-object v5, LX/JDJ;->REQUEST_NONE:LX/JDJ;

    invoke-virtual {v4, v5}, LX/JDJ;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    if-eqz v0, :cond_c

    :cond_b
    sget-object v5, LX/JDJ;->REQUEST_PENDING:LX/JDJ;

    invoke-virtual {v4, v5}, LX/JDJ;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    sget-object v5, LX/5vL;->REMOVE:LX/5vL;

    invoke-virtual {v0, v5}, LX/5vL;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_c
    move v0, v3

    :goto_8
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2663824
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->q:Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

    invoke-virtual {v0, v6}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_d
    move v0, v2

    .line 2663825
    goto :goto_8

    .line 2663826
    :cond_e
    sget-object v5, LX/JDJ;->REQUEST_NONE:LX/JDJ;

    invoke-virtual {v4, v5}, LX/JDJ;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    if-eqz v0, :cond_10

    :cond_f
    sget-object v5, LX/JDJ;->REQUEST_PENDING:LX/JDJ;

    invoke-virtual {v4, v5}, LX/JDJ;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    sget-object v5, LX/5vL;->ADD:LX/5vL;

    invoke-virtual {v0, v5}, LX/5vL;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    move v0, v3

    :goto_9
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2663827
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->q:Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

    iget-object v5, p1, LX/JDK;->h:LX/JA0;

    invoke-virtual {v5}, LX/JA0;->b()LX/1oP;

    move-result-object v5

    sget-object v6, LX/5vL;->ADD:LX/5vL;

    invoke-virtual {v0, v5, v6}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->a(LX/1oP;LX/5vL;)V

    .line 2663828
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->q:Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_11
    move v0, v2

    .line 2663829
    goto :goto_9

    .line 2663830
    :cond_12
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    invoke-virtual {v0, v3}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->setEnabled(Z)V

    goto/16 :goto_4

    .line 2663831
    :cond_13
    invoke-virtual {p0, v1}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663832
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->d()V

    goto/16 :goto_6

    .line 2663833
    :cond_14
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v2, v2, LX/JDK;->f:Lcom/facebook/graphql/enums/GraphQLObjectType;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v5, v5, LX/JDK;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v6, v3, v4

    invoke-static {v2, v3}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2663834
    if-nez v0, :cond_3

    .line 2663835
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v0, v0, LX/JDK;->e:Ljava/lang/String;

    goto/16 :goto_5
.end method

.method public final b(LX/2fp;LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2fp;",
            "LX/1oP;",
            "Lcom/facebook/story/UpdateTimelineAppCollectionParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2663752
    sget-object v0, LX/5vL;->REMOVE:LX/5vL;

    invoke-direct {p0, p2, v0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a(LX/1oP;LX/5vL;)V

    .line 2663753
    new-instance v0, LX/5vM;

    invoke-direct {v0, p3}, LX/5vM;-><init>(Lcom/facebook/story/UpdateTimelineAppCollectionParams;)V

    sget-object v1, LX/5vL;->REMOVE:LX/5vL;

    .line 2663754
    iput-object v1, v0, LX/5vM;->c:LX/5vL;

    .line 2663755
    move-object v0, v0

    .line 2663756
    invoke-virtual {v0}, LX/5vM;->a()Lcom/facebook/story/UpdateTimelineAppCollectionParams;

    move-result-object v0

    .line 2663757
    invoke-direct {p0, p2, v0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a(LX/1oP;Lcom/facebook/story/UpdateTimelineAppCollectionParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2663758
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->e()V

    .line 2663759
    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 2663760
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomRelativeLayout;->onLayout(ZIIII)V

    .line 2663761
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->r:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2663762
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->r:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b099e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object v0

    .line 2663763
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->v:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 2663764
    :goto_0
    return-void

    .line 2663765
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 2663766
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b099e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object v0

    .line 2663767
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->v:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto :goto_0

    .line 2663768
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->t:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 2663769
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->t:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b099e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object v0

    .line 2663770
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->v:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto :goto_0

    .line 2663771
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 2663772
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->u:Lcom/facebook/timeline/aboutpage/views/TimelineCollectionPlusButtonView;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b099e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object v0

    .line 2663773
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->v:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto :goto_0

    .line 2663774
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->v:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto :goto_0
.end method
