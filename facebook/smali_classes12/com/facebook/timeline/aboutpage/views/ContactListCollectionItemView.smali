.class public Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/JCi;


# instance fields
.field public a:LX/J7A;

.field private b:Lcom/facebook/content/SecureContextHelper;

.field private c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0kL;

.field private e:Ljava/util/concurrent/Executor;

.field private f:LX/J7R;

.field public g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

.field public h:LX/9lP;

.field private i:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field private final j:Landroid/view/View$OnClickListener;

.field private final k:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2663471
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2663472
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2663570
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2663571
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2663565
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2663566
    new-instance v0, LX/JDB;

    invoke-direct {v0, p0}, LX/JDB;-><init>(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->j:Landroid/view/View$OnClickListener;

    .line 2663567
    new-instance v0, LX/JDC;

    invoke-direct {v0, p0}, LX/JDC;-><init>(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->k:Landroid/view/View$OnClickListener;

    .line 2663568
    const-class v0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2663569
    return-void
.end method

.method public static synthetic a(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2663564
    invoke-static {p1}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2663563
    const/16 v0, 0xa

    const/16 v1, 0x2c

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/J7A;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0kL;Ljava/util/concurrent/Executor;LX/J7R;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/J7A;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0kL;",
            "Ljava/util/concurrent/Executor;",
            "LX/J7R;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2663556
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->a:LX/J7A;

    .line 2663557
    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2663558
    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->c:LX/0Or;

    .line 2663559
    iput-object p4, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->d:LX/0kL;

    .line 2663560
    iput-object p5, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->e:Ljava/util/concurrent/Executor;

    .line 2663561
    iput-object p6, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->f:LX/J7R;

    .line 2663562
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;

    invoke-static {v6}, LX/J7A;->a(LX/0QB;)LX/J7A;

    move-result-object v1

    check-cast v1, LX/J7A;

    invoke-static {v6}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x542

    invoke-static {v6, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v6}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    invoke-static {v6}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {v6}, LX/J7R;->b(LX/0QB;)LX/J7R;

    move-result-object v6

    check-cast v6, LX/J7R;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->a(LX/J7A;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/0kL;Ljava/util/concurrent/Executor;LX/J7R;)V

    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 2663552
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.intent.action.DIAL"

    .line 2663553
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2663554
    invoke-static {v0, p0}, LX/2A3;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p0

    move v0, p0

    .line 2663555
    return v0
.end method

.method private b()Z
    .locals 3

    .prologue
    .line 2663550
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "geo:0,0"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2663551
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, LX/2A3;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method private static b(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)Z
    .locals 1

    .prologue
    .line 2663549
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2663544
    :try_start_0
    const-string v0, "ISO-8859-1"

    invoke-static {p1, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 2663545
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "geo:0,0?q="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2663546
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2663547
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2663548
    return-void

    :catch_0
    goto :goto_0
.end method

.method public static c(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;)V
    .locals 7

    .prologue
    .line 2663539
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->h:LX/9lP;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->i:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V

    .line 2663540
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->f:LX/J7R;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->c()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->h:LX/9lP;

    .line 2663541
    iget-object v4, v3, LX/9lP;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2663542
    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0aG;

    new-instance v5, LX/JDG;

    invoke-direct {v5, p0}, LX/JDG;-><init>(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;)V

    iget-object v6, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->e:Ljava/util/concurrent/Executor;

    invoke-virtual/range {v0 .. v6}, LX/J7R;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;Ljava/lang/String;LX/0aG;LX/J73;Ljava/util/concurrent/Executor;)V

    .line 2663543
    return-void
.end method

.method private c(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2663534
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2663535
    :cond_0
    :goto_0
    return v0

    .line 2663536
    :cond_1
    invoke-static {p1}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->b(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2663537
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    move-result-object v1

    .line 2663538
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    invoke-virtual {v2, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->b()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->PHONE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    invoke-virtual {v2, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->a()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    invoke-virtual {v2, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2663528
    :try_start_0
    const-string v0, "ISO-8859-1"

    invoke-static {p1, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 2663529
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tel:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2663530
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2663531
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2663532
    return-void

    .line 2663533
    :catch_0
    goto :goto_0
.end method

.method public static d$redex0(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2663522
    :try_start_0
    const-string v0, "ISO-8859-1"

    invoke-static {p1, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 2663523
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mailto:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2663524
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SENDTO"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2663525
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2663526
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->b:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2663527
    return-void

    :catch_0
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindCollectionItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 2663499
    const v0, 0x7f0d0964

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2663500
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2663501
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2663502
    const v0, 0x7f0d0967

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2663503
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2663504
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663505
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2663506
    :cond_0
    :goto_0
    const v0, 0x7f0d0968

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2663507
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2663508
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663509
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2663510
    :goto_1
    invoke-direct {p0, p1}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->c(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2663511
    new-instance v0, LX/JDD;

    invoke-direct {v0, p0, p1}, LX/JDD;-><init>(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663512
    const v0, 0x7f021943

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->setBackgroundResource(I)V

    .line 2663513
    :goto_2
    invoke-static {p1}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->b(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2663514
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081538

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    .line 2663515
    new-instance v1, LX/JDF;

    invoke-direct {v1, p0, p1, v0}, LX/JDF;-><init>(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;[Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2663516
    :cond_1
    return-void

    .line 2663517
    :cond_2
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663518
    invoke-virtual {p0, v4}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2663519
    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 2663520
    :cond_4
    invoke-virtual {p0, v4}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663521
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->setBackgroundColor(I)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/16 v5, 0x8

    .line 2663473
    invoke-static {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;)Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    .line 2663474
    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->h:LX/9lP;

    .line 2663475
    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->i:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2663476
    invoke-virtual {p0, v7}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663477
    const v0, 0x7f0d0967

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2663478
    const v1, 0x7f0d0968

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2663479
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663480
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663481
    const v0, 0x7f0d0964

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2663482
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2663483
    const v1, 0x7f0d096b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2663484
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2663485
    const v0, 0x7f0d096c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2663486
    const v1, 0x7f0d096d

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v2

    .line 2663487
    const v1, 0x7f0d096f

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2663488
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    invoke-virtual {v3}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->e()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v3

    .line 2663489
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->REQUESTABLE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2663490
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 2663491
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663492
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2663493
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663494
    :goto_0
    return-void

    .line 2663495
    :cond_0
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 2663496
    invoke-virtual {v0, v7}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663497
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2663498
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
