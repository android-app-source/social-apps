.class public Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/JCi;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1nG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final f:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2664028
    const-class v0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;

    const-string v1, "collections_collection"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2664031
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2664032
    new-instance v0, LX/JDR;

    invoke-direct {v0, p0}, LX/JDR;-><init>(Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->f:Landroid/view/View$OnClickListener;

    .line 2664033
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->a()V

    .line 2664034
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2664008
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2664009
    new-instance v0, LX/JDR;

    invoke-direct {v0, p0}, LX/JDR;-><init>(Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->f:Landroid/view/View$OnClickListener;

    .line 2664010
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->a()V

    .line 2664011
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2664029
    const-class v0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2664030
    return-void
.end method

.method private static a(Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;LX/1nG;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;",
            "LX/1nG;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2664027
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->a:LX/1nG;

    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->c:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;

    invoke-static {v1}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v0

    check-cast v0, LX/1nG;

    const/16 v2, 0x509

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0xf2f

    invoke-static {v1, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->a(Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;LX/1nG;LX/0Or;LX/0Or;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindCollectionItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2664017
    const/4 v0, 0x0

    .line 2664018
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->j()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->j()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2664019
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->setVisibility(I)V

    move-object v1, v0

    .line 2664020
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2664021
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2664022
    return-void

    .line 2664023
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2664024
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->setVisibility(I)V

    .line 2664025
    invoke-virtual {p0, p1}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->setTag(Ljava/lang/Object;)V

    .line 2664026
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;II)V
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "setContentDescription"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2664012
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2664013
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081192

    new-array v2, v7, [Ljava/lang/Object;

    add-int/lit8 v3, p2, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2664014
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2664015
    return-void

    .line 2664016
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081193

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    add-int/lit8 v4, p2, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V
    .locals 0

    .prologue
    .line 2664007
    return-void
.end method

.method public final onFinishInflate()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2c

    const v1, 0x5bcf7156

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2663999
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishInflate()V

    .line 2664000
    const v0, 0x7f0d098b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2664001
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v2, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0168

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2664002
    iput-object v3, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2664003
    move-object v2, v2

    .line 2664004
    invoke-virtual {v2}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2664005
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2664006
    const/16 v0, 0x2d

    const v2, 0x21ef2b2

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
