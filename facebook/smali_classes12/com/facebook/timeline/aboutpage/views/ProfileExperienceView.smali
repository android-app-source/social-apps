.class public Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Uf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/JCa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/J9y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/D9E;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1nG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/fbui/widget/contentview/ContentView;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Lcom/facebook/resources/ui/FbTextView;

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/widget/ImageView;

.field private l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2664211
    const-class v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2664100
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2664101
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a()V

    .line 2664102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2664208
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2664209
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a()V

    .line 2664210
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2664205
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2664206
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a()V

    .line 2664207
    return-void
.end method

.method private a(LX/0Px;Landroid/content/Context;)LX/5OM;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;",
            ">;",
            "Landroid/content/Context;",
            ")",
            "LX/5OM;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2664187
    new-instance v5, LX/5OM;

    invoke-direct {v5, p2}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 2664188
    invoke-virtual {v5}, LX/5OM;->c()LX/5OG;

    move-result-object v6

    .line 2664189
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_5

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;

    .line 2664190
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;->k()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_0

    .line 2664191
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;->k()LX/1vs;

    move-result-object v1

    iget-object v8, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2664192
    iget-object v9, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->d:LX/D9E;

    invoke-virtual {v8, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v6, v1}, LX/D9E;->a(LX/5OG;Ljava/lang/CharSequence;)Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;

    move-result-object v8

    .line 2664193
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2664194
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel$NodeToDeleteModel;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel$NodeToDeleteModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel$NodeToDeleteModel;->j()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 2664195
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel$NodeToDeleteModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel$NodeToDeleteModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2664196
    new-instance v9, LX/JDW;

    invoke-direct {v9, p0, v1}, LX/JDW;-><init>(Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2664197
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    if-eqz v1, :cond_4

    .line 2664198
    const-string v0, ""

    invoke-virtual {v8, v0}, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->a(Ljava/lang/String;)Landroid/view/MenuItem;

    .line 2664199
    :goto_3
    invoke-virtual {v6, v8}, LX/5OG;->a(LX/3Ai;)V

    .line 2664200
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 2664201
    :cond_1
    new-instance v9, LX/JDX;

    invoke-direct {v9, p0, v1}, LX/JDX;-><init>(Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1

    .line 2664202
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;->a()LX/1vs;

    move-result-object v1

    iget-object v9, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v9, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    move v1, v2

    goto :goto_2

    :cond_3
    move v1, v3

    goto :goto_2

    .line 2664203
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->a(Ljava/lang/String;)Landroid/view/MenuItem;

    goto :goto_3

    .line 2664204
    :cond_5
    return-object v5
.end method

.method public static synthetic a(Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;LX/0Px;Landroid/content/Context;)LX/5OM;
    .locals 1

    .prologue
    .line 2664186
    invoke-direct {p0, p1, p2}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a(LX/0Px;Landroid/content/Context;)LX/5OM;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2664173
    const-class v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2664174
    const v0, 0x7f031053

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2664175
    const v0, 0x7f0d2725

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->g:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2664176
    const v0, 0x7f0d2726

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2664177
    const v0, 0x7f0d2727

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 2664178
    const v0, 0x7f0d2728

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->j:Landroid/widget/LinearLayout;

    .line 2664179
    const v0, 0x7f0d2729

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->k:Landroid/widget/ImageView;

    .line 2664180
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2664181
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->g:Lcom/facebook/fbui/widget/contentview/ContentView;

    sget-object v1, LX/6VF;->LARGE:LX/6VF;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2664182
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->g:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 2664183
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->g:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailView(Landroid/view/View;)V

    .line 2664184
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->h:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->b:LX/JCa;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2664185
    return-void
.end method

.method private a(Landroid/text/SpannableString;LX/15i;I)V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x2

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 2664166
    const v0, -0x46b8ff84

    invoke-static {p2, p3, v5, v0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 2664167
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$TextWithEntitiesInfoModel$RangesModel$EntityModel;

    invoke-virtual {v3, v4, v5, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$TextWithEntitiesInfoModel$RangesModel$EntityModel;

    .line 2664168
    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$TextWithEntitiesInfoModel$RangesModel$EntityModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$TextWithEntitiesInfoModel$RangesModel$EntityModel;->k()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v5

    aput-object v8, v6, v7

    invoke-static {v2, v6}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2664169
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a:LX/1Uf;

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$TextWithEntitiesInfoModel$RangesModel$EntityModel;->l()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v3, v4, v11}, LX/15i;->j(II)I

    move-result v2

    invoke-virtual {v3, v4, v11}, LX/15i;->j(II)I

    move-result v6

    invoke-virtual {v3, v4, v7}, LX/15i;->j(II)I

    move-result v3

    add-int/2addr v3, v6

    const v6, 0x7f0a096d

    move-object v4, p1

    move-object v9, v8

    invoke-virtual/range {v0 .. v9}, LX/1Uf;->a(Ljava/lang/String;IILandroid/text/Spannable;ZIZLX/0lF;LX/1y5;)V

    goto :goto_1

    .line 2664170
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 2664171
    goto :goto_2

    .line 2664172
    :cond_2
    return-void
.end method

.method private static a(Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;LX/1Uf;LX/JCa;LX/J9y;LX/D9E;LX/1nG;)V
    .locals 0

    .prologue
    .line 2664165
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a:LX/1Uf;

    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->b:LX/JCa;

    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->c:LX/J9y;

    iput-object p4, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->d:LX/D9E;

    iput-object p5, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->e:LX/1nG;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;

    invoke-static {v5}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v1

    check-cast v1, LX/1Uf;

    invoke-static {v5}, LX/JCa;->a(LX/0QB;)LX/JCa;

    move-result-object v2

    check-cast v2, LX/JCa;

    invoke-static {v5}, LX/J9y;->a(LX/0QB;)LX/J9y;

    move-result-object v3

    check-cast v3, LX/J9y;

    const-class v4, LX/D9E;

    invoke-interface {v5, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/D9E;

    invoke-static {v5}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v5

    check-cast v5, LX/1nG;

    invoke-static/range {v0 .. v5}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a(Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;LX/1Uf;LX/JCa;LX/J9y;LX/D9E;LX/1nG;)V

    return-void
.end method

.method private static a(II)Z
    .locals 1

    .prologue
    .line 2664164
    add-int/lit8 v0, p1, -0x1

    if-ge p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getHorizontalDivider()Landroid/view/View;
    .locals 4

    .prologue
    .line 2664163
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031052

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->j:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;)V
    .locals 17

    .prologue
    .line 2664103
    new-instance v12, LX/JDU;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v12, v0, v1}, LX/JDU;-><init>(Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;)V

    .line 2664104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->g:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v2, v12}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2664105
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->w()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v6

    .line 2664106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a:LX/1Uf;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->o()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v6}, Landroid/text/SpannableString;->length()I

    move-result v5

    const/4 v7, 0x0

    const v8, 0x7f0a096d

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, LX/1Uf;->a(Ljava/lang/String;IILandroid/text/Spannable;ZIZLX/0lF;LX/1y5;)V

    .line 2664107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->h:Lcom/facebook/resources/ui/FbTextView;

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v6, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 2664108
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->n()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-nez v2, :cond_3

    .line 2664109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2664110
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2664111
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2664112
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->q()LX/2uF;

    move-result-object v2

    invoke-virtual {v2}, LX/39O;->a()Z

    move-result v2

    if-nez v2, :cond_b

    .line 2664113
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->q()LX/2uF;

    move-result-object v2

    invoke-virtual {v2}, LX/39O;->c()I

    move-result v6

    .line 2664114
    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v6, :cond_b

    .line 2664115
    new-instance v7, Lcom/facebook/resources/ui/FbTextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->g:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v7, v2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2664116
    new-instance v8, Landroid/text/SpannableStringBuilder;

    const-string v2, "\n"

    invoke-direct {v8, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2664117
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->q()LX/2uF;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v9, 0x0

    const v10, -0x49d6164e

    invoke-static {v3, v2, v9, v10}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_2
    invoke-virtual {v2}, LX/39O;->c()I

    move-result v9

    .line 2664118
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    if-ge v3, v9, :cond_9

    .line 2664119
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->q()LX/2uF;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v10, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v11, 0x0

    const v13, -0x49d6164e

    invoke-static {v10, v2, v11, v13}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_4
    invoke-virtual {v2, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v10, v2, LX/1vs;->a:LX/15i;

    iget v11, v2, LX/1vs;->b:I

    .line 2664120
    sget-object v13, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v13

    :try_start_0
    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2664121
    const/4 v2, 0x1

    invoke-virtual {v10, v11, v2}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_2

    .line 2664122
    const/4 v2, 0x1

    invoke-virtual {v10, v11, v2}, LX/15i;->g(II)I

    move-result v2

    const/4 v13, 0x1

    invoke-virtual {v10, v2, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v13

    .line 2664123
    sget-object v14, LX/JDY;->a:[I

    const/4 v2, 0x0

    const-class v15, Lcom/facebook/graphql/enums/GraphQLProfileFieldTextListItemHeadingType;

    sget-object v16, Lcom/facebook/graphql/enums/GraphQLProfileFieldTextListItemHeadingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLProfileFieldTextListItemHeadingType;

    move-object/from16 v0, v16

    invoke-virtual {v10, v11, v2, v15, v0}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldTextListItemHeadingType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLProfileFieldTextListItemHeadingType;->ordinal()I

    move-result v2

    aget v2, v14, v2

    packed-switch v2, :pswitch_data_0

    .line 2664124
    invoke-virtual {v8, v13}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2664125
    :cond_0
    :goto_5
    invoke-static {v4, v6}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a(II)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v3, v9}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a(II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2664126
    :cond_1
    const-string v2, "\n"

    invoke-virtual {v8, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2664127
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 2664128
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2664129
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->n()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2664130
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_0

    .line 2664131
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto/16 :goto_2

    .line 2664132
    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_4

    .line 2664133
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 2664134
    :pswitch_0
    if-nez v4, :cond_6

    .line 2664135
    const/4 v2, 0x1

    invoke-virtual {v10, v11, v2}, LX/15i;->g(II)I

    move-result v2

    const/4 v14, 0x1

    invoke-virtual {v10, v2, v14}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2664136
    invoke-static {v3, v9}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a(II)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2664137
    const-string v2, "\n"

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2664138
    :cond_6
    new-instance v14, Landroid/text/SpannableString;

    invoke-direct {v14, v13}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2664139
    const/4 v2, 0x1

    invoke-virtual {v10, v11, v2}, LX/15i;->g(II)I

    move-result v2

    const/4 v15, 0x0

    const v16, -0x46b8ff84

    move/from16 v0, v16

    invoke-static {v10, v2, v15, v0}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_6
    invoke-virtual {v2}, LX/39O;->a()Z

    move-result v2

    if-nez v2, :cond_8

    .line 2664140
    const/4 v2, 0x1

    invoke-virtual {v10, v11, v2}, LX/15i;->g(II)I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v10, v2}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a(Landroid/text/SpannableString;LX/15i;I)V

    .line 2664141
    invoke-virtual {v8, v14}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2664142
    invoke-static {v3, v9}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2664143
    const-string v2, "\n"

    invoke-virtual {v8, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_5

    .line 2664144
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_6

    .line 2664145
    :cond_8
    invoke-virtual {v8, v13}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_5

    .line 2664146
    :pswitch_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_5

    .line 2664147
    :cond_9
    if-lez v4, :cond_a

    .line 2664148
    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v7, v8, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 2664149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->b:LX/JCa;

    invoke-virtual {v7, v2}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2664150
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v7, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2664151
    invoke-virtual {v7, v12}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2664152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2664153
    invoke-static {v4, v6}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->a(II)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2664154
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->j:Landroid/widget/LinearLayout;

    invoke-direct/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->getHorizontalDivider()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2664155
    :cond_a
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_1

    .line 2664156
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2664157
    const v2, 0x7f0d2729

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 2664158
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->r()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_c

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->r()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_c

    .line 2664159
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;->k:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2664160
    new-instance v3, LX/JDV;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1, v2}, LX/JDV;-><init>(Lcom/facebook/timeline/aboutpage/views/ProfileExperienceView;Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;Landroid/widget/ImageView;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2664161
    :goto_7
    return-void

    .line 2664162
    :cond_c
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
