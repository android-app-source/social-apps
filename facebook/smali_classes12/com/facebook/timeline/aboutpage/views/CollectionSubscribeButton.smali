.class public Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field private a:LX/JDK;

.field public b:LX/JCj;

.field private c:LX/2dj;

.field private d:Lcom/facebook/timeline/services/ProfileActionClient;

.field public e:LX/03V;

.field private f:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2663001
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2663002
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2663003
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2663004
    const v0, 0x7f020277

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setImageResource(I)V

    .line 2663005
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2663006
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setClickable(Z)V

    .line 2663007
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setVisibility(I)V

    .line 2663008
    return-void
.end method

.method public static a(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2663009
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->a:LX/JDK;

    iget-object v0, v0, LX/JDK;->h:LX/JA0;

    iget-object v0, v0, LX/JA0;->i:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->a:LX/JDK;

    iget-object v0, v0, LX/JDK;->h:LX/JA0;

    iget-object v0, v0, LX/JA0;->i:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x285feb

    if-eq v0, v1, :cond_1

    .line 2663010
    :cond_0
    invoke-virtual {p0, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setVisibility(I)V

    .line 2663011
    :goto_0
    return-void

    .line 2663012
    :cond_1
    sget-object v0, LX/JD4;->a:[I

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->a:LX/JDK;

    iget-object v1, v1, LX/JDK;->h:LX/JA0;

    invoke-virtual {v1}, LX/JA0;->g()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2663013
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid subscribe status"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2663014
    :pswitch_0
    const v0, 0x7f020278

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setImageResource(I)V

    .line 2663015
    const v0, 0x7f0815e0

    invoke-direct {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setContentDescription(I)V

    goto :goto_0

    .line 2663016
    :pswitch_1
    const v0, 0x7f020277

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setImageResource(I)V

    .line 2663017
    const v0, 0x7f0815df

    invoke-direct {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setContentDescription(I)V

    goto :goto_0

    .line 2663018
    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static c(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;LX/JA0;)V
    .locals 3

    .prologue
    .line 2663019
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->c:LX/2dj;

    iget-object v1, p1, LX/JA0;->b:Ljava/lang/String;

    const-string v2, "TIMELINE_COLLECTION"

    invoke-virtual {v0, v1, v2}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2663020
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {p1, v1}, LX/JA0;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2663021
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->b:LX/JCj;

    const v2, 0x7f081580

    invoke-interface {v1, v2}, LX/JCj;->H_(I)V

    .line 2663022
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->a(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;)V

    .line 2663023
    new-instance v1, LX/JCz;

    invoke-direct {v1, p0, p1}, LX/JCz;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;LX/JA0;)V

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2663024
    return-void
.end method

.method public static d(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;LX/JA0;)V
    .locals 4

    .prologue
    .line 2663025
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2663026
    iget-boolean v0, p1, LX/JA0;->m:Z

    move v0, v0

    .line 2663027
    if-eqz v0, :cond_0

    const v0, 0x7f08158d

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2663028
    new-instance v1, LX/4mb;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/4mb;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08157d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/JD1;

    invoke-direct {v3, p0, p1}, LX/JD1;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;LX/JA0;)V

    invoke-virtual {v1, v2, v3}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    move-result-object v1

    new-instance v2, LX/JD0;

    invoke-direct {v2, p0, p1}, LX/JD0;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;LX/JA0;)V

    invoke-virtual {v1, v0, v2}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    move-result-object v0

    .line 2663029
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0815b3

    .line 2663030
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance p0, LX/4mZ;

    invoke-direct {p0, v0}, LX/4mZ;-><init>(LX/4mb;)V

    invoke-virtual {v0, v3, p0}, LX/4mb;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/4mb;

    .line 2663031
    invoke-virtual {v0}, LX/4mb;->show()Landroid/app/AlertDialog;

    .line 2663032
    return-void

    .line 2663033
    :cond_0
    const v0, 0x7f08158c

    goto :goto_0
.end method

.method private setContentDescription(I)V
    .locals 1

    .prologue
    .line 2663034
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2663035
    return-void
.end method


# virtual methods
.method public final a(LX/9lP;LX/JDK;LX/JCj;LX/2dj;Lcom/facebook/timeline/services/ProfileActionClient;LX/03V;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 2663036
    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->a:LX/JDK;

    .line 2663037
    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->b:LX/JCj;

    .line 2663038
    iput-object p4, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->c:LX/2dj;

    .line 2663039
    iput-object p5, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->d:Lcom/facebook/timeline/services/ProfileActionClient;

    .line 2663040
    iput-object p6, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->e:LX/03V;

    .line 2663041
    iput-object p7, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->f:Ljava/util/concurrent/Executor;

    .line 2663042
    iget-object v0, p2, LX/JDK;->h:LX/JA0;

    if-eqz v0, :cond_0

    .line 2663043
    iget-object v0, p1, LX/9lP;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2663044
    iget-object v1, p2, LX/JDK;->h:LX/JA0;

    iget-object v1, v1, LX/JA0;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2663045
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setVisibility(I)V

    .line 2663046
    :goto_0
    return-void

    .line 2663047
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setVisibility(I)V

    .line 2663048
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->a:LX/JDK;

    iget-object v0, v0, LX/JDK;->h:LX/JA0;

    .line 2663049
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->a(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;)V

    .line 2663050
    new-instance v1, LX/JCy;

    invoke-direct {v1, p0, v0}, LX/JCy;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;LX/JA0;)V

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(LX/JA0;)V
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2663051
    iget-boolean v0, p1, LX/JA0;->m:Z

    move v3, v0

    .line 2663052
    if-nez v3, :cond_0

    move v0, v1

    .line 2663053
    :goto_0
    iput-boolean v0, p1, LX/JA0;->m:Z

    .line 2663054
    if-eqz v3, :cond_1

    .line 2663055
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->b:LX/JCj;

    const v4, 0x7f08158f

    invoke-interface {v0, v4}, LX/JCj;->H_(I)V

    .line 2663056
    :goto_1
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->d:Lcom/facebook/timeline/services/ProfileActionClient;

    iget-object v4, p1, LX/JA0;->b:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    if-nez v3, :cond_2

    .line 2663057
    :goto_2
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2663058
    const-string v6, "setNotificationPreference"

    new-instance v7, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;

    invoke-direct {v7, v4, v5, v1}, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;-><init>(JZ)V

    invoke-virtual {v8, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2663059
    iget-object v6, v0, Lcom/facebook/timeline/services/ProfileActionClient;->b:LX/0aG;

    const-string v7, "profile_set_notification_preference"

    sget-object v9, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v10, Lcom/facebook/timeline/services/ProfileActionClient;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v11, 0x641bb151

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    move-result-object v6

    .line 2663060
    const/4 v7, 0x0

    invoke-static {v7}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object v7

    iget-object v8, v0, Lcom/facebook/timeline/services/ProfileActionClient;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 2663061
    new-instance v1, LX/JD2;

    invoke-direct {v1, p0, p1, v3}, LX/JD2;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;LX/JA0;Z)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2663062
    return-void

    :cond_0
    move v0, v2

    .line 2663063
    goto :goto_0

    .line 2663064
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->b:LX/JCj;

    const v4, 0x7f08158e

    invoke-interface {v0, v4}, LX/JCj;->H_(I)V

    goto :goto_1

    :cond_2
    move v1, v2

    .line 2663065
    goto :goto_2
.end method

.method public final b(LX/JA0;)V
    .locals 3

    .prologue
    .line 2663066
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {p1, v0}, LX/JA0;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2663067
    invoke-static {p0}, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->a(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;)V

    .line 2663068
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->b:LX/JCj;

    const v1, 0x7f081581

    invoke-interface {v0, v1}, LX/JCj;->H_(I)V

    .line 2663069
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->c:LX/2dj;

    iget-object v1, p1, LX/JA0;->b:Ljava/lang/String;

    const-string v2, "TIMELINE_COLLECTION"

    invoke-virtual {v0, v1, v2}, LX/2dj;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2663070
    new-instance v1, LX/JD3;

    invoke-direct {v1, p0, p1}, LX/JD3;-><init>(Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;LX/JA0;)V

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/CollectionSubscribeButton;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2663071
    return-void
.end method
