.class public Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/1e4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2664509
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2664510
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2664498
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2664499
    const-class v0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2664500
    const v0, 0x7f030621

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2664501
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->setOrientation(I)V

    .line 2664502
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->setVisibility(I)V

    .line 2664503
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->setGravity(I)V

    .line 2664504
    const v0, 0x7f0d10fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->c:Landroid/widget/TextView;

    .line 2664505
    const v0, 0x7f0d10fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->d:Landroid/widget/TextView;

    .line 2664506
    const v0, 0x7f0d10ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->e:Landroid/widget/ImageView;

    .line 2664507
    sget-object v0, LX/1vY;->COLLECTION_ACTION_LINK:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2664508
    return-void
.end method

.method private static a(Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;LX/1e4;LX/0wM;)V
    .locals 0

    .prologue
    .line 2664475
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->a:LX/1e4;

    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->b:LX/0wM;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;

    invoke-static {v1}, LX/1e4;->a(LX/0QB;)LX/1e4;

    move-result-object v0

    check-cast v0, LX/1e4;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->a(Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;LX/1e4;LX/0wM;)V

    return-void
.end method


# virtual methods
.method public final a(LX/1oP;LX/5vL;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2664483
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/1oP;->d()LX/1oQ;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/1oP;->d()LX/1oQ;

    move-result-object v0

    invoke-interface {v0}, LX/1oQ;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2664484
    :cond_0
    :goto_0
    return-void

    .line 2664485
    :cond_1
    sget-object v0, LX/5vL;->ADD:LX/5vL;

    if-ne p2, v0, :cond_2

    .line 2664486
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0810f3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-interface {p1}, LX/1oP;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2664487
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->a:LX/1e4;

    invoke-interface {p1}, LX/1oP;->d()LX/1oQ;

    move-result-object v2

    invoke-interface {v2}, LX/1oQ;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v1

    .line 2664488
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->b:LX/0wM;

    const v3, -0x6e685d

    invoke-virtual {v2, v1, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2664489
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2664490
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2664491
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2664492
    :goto_1
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2664493
    invoke-virtual {p0, v4}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->setVisibility(I)V

    goto :goto_0

    .line 2664494
    :cond_2
    sget-object v0, LX/5vL;->REMOVE:LX/5vL;

    if-ne p2, v0, :cond_0

    .line 2664495
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0810f4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-interface {p1}, LX/1oP;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2664496
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2664497
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(LX/5vL;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2664476
    sget-object v0, LX/5vL;->ADD:LX/5vL;

    if-ne p1, v0, :cond_0

    const v0, 0x7f0810f5

    .line 2664477
    :goto_0
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2664478
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2664479
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2664480
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/UpdateTimelineCollectionConfirmationView;->setVisibility(I)V

    .line 2664481
    return-void

    .line 2664482
    :cond_0
    const v0, 0x7f0810f6

    goto :goto_0
.end method
