.class public Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/JCi;


# instance fields
.field public a:Lcom/facebook/intent/feed/IFeedIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Uk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2663979
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2663980
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->a()V

    .line 2663981
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2663976
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2663977
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->a()V

    .line 2663978
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2663966
    const-class v0, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2663967
    return-void
.end method

.method private a(ILjava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2663969
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2663970
    if-eqz v0, :cond_0

    .line 2663971
    if-eqz p2, :cond_1

    .line 2663972
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663973
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2663974
    :cond_0
    :goto_0
    return-void

    .line 2663975
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1Uk;)V
    .locals 0

    .prologue
    .line 2663968
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->b:LX/1Uk;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;

    invoke-static {v1}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v1}, LX/1Uk;->b(LX/0QB;)LX/1Uk;

    move-result-object v1

    check-cast v1, LX/1Uk;

    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->a(Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1Uk;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindCollectionItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2663954
    const v2, 0x7f0d0985

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-direct {p0, v2, v0}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->a(ILjava/lang/CharSequence;)V

    .line 2663955
    const v2, 0x7f0d0986

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-direct {p0, v2, v0}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->a(ILjava/lang/CharSequence;)V

    .line 2663956
    const v2, 0x7f0d0987

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_2
    invoke-direct {p0, v2, v0}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->a(ILjava/lang/CharSequence;)V

    .line 2663957
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->mS_()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2663958
    new-instance v0, LX/JDQ;

    invoke-direct {v0, p0, p1}, LX/JDQ;-><init>(Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663959
    const v0, 0x7f021943

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->setBackgroundResource(I)V

    .line 2663960
    :goto_3
    return-void

    .line 2663961
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2663962
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2663963
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2663964
    :cond_3
    invoke-virtual {p0, v1}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663965
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/NotesCollectionItemView;->setBackgroundColor(I)V

    goto :goto_3
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V
    .locals 0

    .prologue
    .line 2663953
    return-void
.end method
