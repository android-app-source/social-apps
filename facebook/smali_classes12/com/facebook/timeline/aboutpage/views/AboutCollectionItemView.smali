.class public Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/JCi;


# static fields
.field private static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Uf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/JCa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/J7A;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/J93;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/J7R;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private n:Landroid/view/View;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/Button;

.field private q:Landroid/view/View;

.field private r:Landroid/widget/ImageView;

.field public s:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

.field public t:LX/9lP;

.field private u:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field private final v:Landroid/view/View$OnClickListener;

.field private final w:Landroid/view/View$OnClickListener;

.field private final x:LX/J73;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2662686
    const-class v0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;

    const-string v1, "collections_collection"

    const-string v2, "about"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2662753
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2662754
    new-instance v0, LX/JCd;

    invoke-direct {v0, p0}, LX/JCd;-><init>(Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->v:Landroid/view/View$OnClickListener;

    .line 2662755
    new-instance v0, LX/JCe;

    invoke-direct {v0, p0}, LX/JCe;-><init>(Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->w:Landroid/view/View$OnClickListener;

    .line 2662756
    new-instance v0, LX/JCf;

    invoke-direct {v0, p0}, LX/JCf;-><init>(Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->x:LX/J73;

    .line 2662757
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->a()V

    .line 2662758
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2662761
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2662762
    new-instance v0, LX/JCd;

    invoke-direct {v0, p0}, LX/JCd;-><init>(Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->v:Landroid/view/View$OnClickListener;

    .line 2662763
    new-instance v0, LX/JCe;

    invoke-direct {v0, p0}, LX/JCe;-><init>(Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->w:Landroid/view/View$OnClickListener;

    .line 2662764
    new-instance v0, LX/JCf;

    invoke-direct {v0, p0}, LX/JCf;-><init>(Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->x:LX/J73;

    .line 2662765
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->a()V

    .line 2662766
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2662759
    const-class v0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2662760
    return-void
.end method

.method private static a(Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;LX/1Uf;LX/JCa;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/J7R;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;",
            "LX/1Uf;",
            "LX/JCa;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Or",
            "<",
            "LX/J7A;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Or",
            "<",
            "LX/J93;",
            ">;",
            "LX/J7R;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2662752
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->a:LX/1Uf;

    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->b:LX/JCa;

    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->d:LX/0Or;

    iput-object p5, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->e:LX/0Or;

    iput-object p6, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->f:LX/0Or;

    iput-object p7, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->g:LX/0Or;

    iput-object p8, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->h:LX/0Or;

    iput-object p9, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->i:LX/J7R;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;

    invoke-static {v9}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v1

    check-cast v1, LX/1Uf;

    invoke-static {v9}, LX/JCa;->a(LX/0QB;)LX/JCa;

    move-result-object v2

    check-cast v2, LX/JCa;

    const/16 v3, 0xbc6

    invoke-static {v9, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x3010

    invoke-static {v9, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x542

    invoke-static {v9, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x1430

    invoke-static {v9, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x12c4

    invoke-static {v9, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x3606

    invoke-static {v9, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v9}, LX/J7R;->b(LX/0QB;)LX/J7R;

    move-result-object v9

    check-cast v9, LX/J7R;

    invoke-static/range {v0 .. v9}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->a(Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;LX/1Uf;LX/JCa;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/J7R;)V

    return-void
.end method

.method public static b(Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;)V
    .locals 7

    .prologue
    .line 2662748
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->i:LX/J7R;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->c()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->t:LX/9lP;

    .line 2662749
    iget-object v4, v3, LX/9lP;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2662750
    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->e:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0aG;

    iget-object v5, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->x:LX/J73;

    iget-object v6, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->f:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-virtual/range {v0 .. v6}, LX/J7R;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;Ljava/lang/String;LX/0aG;LX/J73;Ljava/util/concurrent/Executor;)V

    .line 2662751
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindCollectionItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 2662717
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->n:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2662718
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2662719
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;->b()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2662720
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 2662721
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->k:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a096f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2662722
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->a:LX/1Uf;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v3

    invoke-virtual {v2, v3, v5, v1}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v2

    .line 2662723
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->k:Landroid/widget/TextView;

    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v3, v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 2662724
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->k:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 2662725
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->k:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->b:LX/JCa;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2662726
    :goto_0
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->k:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2662727
    :goto_1
    if-eqz v0, :cond_3

    .line 2662728
    new-instance v2, LX/JCg;

    invoke-direct {v2, p0, v0}, LX/JCg;-><init>(Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2662729
    const v0, 0x7f021943

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->setBackgroundResource(I)V

    .line 2662730
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2662731
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2662732
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->l:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2662733
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->c()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->c()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2662734
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->c()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2662735
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2662736
    :goto_4
    return-void

    .line 2662737
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->l()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a096d

    .line 2662738
    :goto_5
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->k:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2662739
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->k:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2662740
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->l()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2662741
    :cond_1
    const v0, 0x7f0a096f

    goto :goto_5

    .line 2662742
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    move-object v0, v1

    goto/16 :goto_1

    .line 2662743
    :cond_3
    invoke-virtual {p0, v1}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2662744
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0048

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->setBackgroundColor(I)V

    goto/16 :goto_2

    .line 2662745
    :cond_4
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 2662746
    :cond_5
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2662747
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_4
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2662697
    invoke-static {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;)Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    .line 2662698
    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->t:LX/9lP;

    .line 2662699
    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->u:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2662700
    invoke-virtual {p0, v4}, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2662701
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2662702
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2662703
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2662704
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->n:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2662705
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->o:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2662706
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->e()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v0

    .line 2662707
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->REQUESTABLE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2662708
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->p:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 2662709
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->p:Landroid/widget/Button;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->v:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2662710
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->q:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2662711
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2662712
    :goto_0
    return-void

    .line 2662713
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->p:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2662714
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->p:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2662715
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2662716
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->r:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->w:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1a94ca9b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2662687
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onFinishInflate()V

    .line 2662688
    const v0, 0x7f0d0961

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->k:Landroid/widget/TextView;

    .line 2662689
    const v0, 0x7f0d0962

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->l:Landroid/widget/TextView;

    .line 2662690
    const v0, 0x7f0d095f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2662691
    const v0, 0x7f0d0964

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->n:Landroid/view/View;

    .line 2662692
    const v0, 0x7f0d096b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->o:Landroid/widget/TextView;

    .line 2662693
    const v0, 0x7f0d096c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->p:Landroid/widget/Button;

    .line 2662694
    const v0, 0x7f0d096d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->q:Landroid/view/View;

    .line 2662695
    const v0, 0x7f0d096f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->r:Landroid/widget/ImageView;

    .line 2662696
    const/16 v0, 0x2d

    const v2, -0x1756571d

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
