.class public Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final c:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2664525
    const-class v0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;

    const-string v1, "collections_collection"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2664526
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2664527
    const v0, 0x7f0302b2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2664528
    const v0, 0x7f0a0974

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->setBackgroundResource(I)V

    .line 2664529
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->setOrientation(I)V

    .line 2664530
    const v0, 0x7f0d0998

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->c:Landroid/widget/TextView;

    .line 2664531
    const v0, 0x7f0d0997

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2664532
    return-void
.end method


# virtual methods
.method public final a(LX/JAc;)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2664533
    if-nez p1, :cond_0

    .line 2664534
    :goto_0
    return-void

    .line 2664535
    :cond_0
    invoke-interface {p1}, LX/JAb;->b()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/JAb;->b()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2664536
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-interface {p1}, LX/JAb;->b()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2664537
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2664538
    :goto_1
    const-string v0, ""

    .line 2664539
    invoke-interface {p1}, LX/JAb;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2664540
    invoke-interface {p1}, LX/JAb;->d()Ljava/lang/String;

    move-result-object v0

    .line 2664541
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2664542
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2664543
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_1
.end method
