.class public Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/JCi;


# static fields
.field private static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/intent/feed/IFeedIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/WindowManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/J9L;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1nG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Landroid/widget/TextView;

.field private h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private i:Landroid/view/Display;

.field private j:I

.field private final k:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2663579
    const-class v0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2663622
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2663623
    new-instance v0, LX/JDH;

    invoke-direct {v0, p0}, LX/JDH;-><init>(Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->k:Landroid/view/View$OnClickListener;

    .line 2663624
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->a()V

    .line 2663625
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2663618
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2663619
    new-instance v0, LX/JDH;

    invoke-direct {v0, p0}, LX/JDH;-><init>(Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->k:Landroid/view/View$OnClickListener;

    .line 2663620
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->a()V

    .line 2663621
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2663612
    const-class v0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2663613
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->b:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->i:Landroid/view/Display;

    .line 2663614
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->c:LX/J9L;

    .line 2663615
    iget v1, v0, LX/J9L;->a:I

    move v0, v1

    .line 2663616
    iput v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->j:I

    .line 2663617
    return-void
.end method

.method private static a(Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;Lcom/facebook/intent/feed/IFeedIntentBuilder;Landroid/view/WindowManager;LX/J9L;LX/1nG;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;)V
    .locals 0

    .prologue
    .line 2663611
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->b:Landroid/view/WindowManager;

    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->c:LX/J9L;

    iput-object p4, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->d:LX/1nG;

    iput-object p5, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->e:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;

    invoke-static {v5}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v1

    check-cast v1, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v5}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-static {v5}, LX/J9L;->a(LX/0QB;)LX/J9L;

    move-result-object v3

    check-cast v3, LX/J9L;

    invoke-static {v5}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v4

    check-cast v4, LX/1nG;

    invoke-static {v5}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v5

    check-cast v5, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static/range {v0 .. v5}, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->a(Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;Lcom/facebook/intent/feed/IFeedIntentBuilder;Landroid/view/WindowManager;LX/J9L;LX/1nG;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;)V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2663602
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2663603
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2663604
    const v2, 0x7f0b2492

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2663605
    mul-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->j:I

    mul-int/2addr v2, v3

    .line 2663606
    const v3, 0x7f0b248d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    .line 2663607
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->i:Landroid/view/Display;

    invoke-virtual {v3}, Landroid/view/Display;->getWidth()I

    move-result v3

    sub-int v2, v3, v2

    sub-int v1, v2, v1

    iget v2, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->j:I

    div-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2663608
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2663609
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2663610
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindCollectionItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2663590
    const/4 v0, 0x0

    .line 2663591
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->j()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2663592
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->j()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2663593
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->d:LX/1nG;

    invoke-static {p1, v2}, LX/JDA;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;LX/1nG;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setTag(Ljava/lang/Object;)V

    .line 2663594
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2663595
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2663596
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2663597
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->g:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2663598
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2663599
    :goto_0
    return-void

    .line 2663600
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->g:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2663601
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V
    .locals 0

    .prologue
    .line 2663589
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 2663586
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2663587
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->b()V

    .line 2663588
    return-void
.end method

.method public final onFinishInflate()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2c

    const v1, -0x3d52b365

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2663580
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onFinishInflate()V

    .line 2663581
    const v0, 0x7f0d096a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->g:Landroid/widget/TextView;

    .line 2663582
    const v0, 0x7f0d0969

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2663583
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-string v3, "collections_collection"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/Class;)V

    .line 2663584
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/views/GridCollectionItemView;->b()V

    .line 2663585
    const/16 v0, 0x2d

    const v2, 0x6a263d8f

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
