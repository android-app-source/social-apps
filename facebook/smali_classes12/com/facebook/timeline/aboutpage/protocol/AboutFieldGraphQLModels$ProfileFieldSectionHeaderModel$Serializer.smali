.class public final Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionHeaderModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionHeaderModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2654628
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionHeaderModel;

    new-instance v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionHeaderModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionHeaderModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2654629
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2654613
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionHeaderModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2654615
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2654616
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2654617
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2654618
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2654619
    if-eqz v2, :cond_0

    .line 2654620
    const-string p0, "edit_uri"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2654621
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2654622
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2654623
    if-eqz v2, :cond_1

    .line 2654624
    const-string p0, "title"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2654625
    invoke-static {v1, v2, p1}, LX/JAN;->a(LX/15i;ILX/0nX;)V

    .line 2654626
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2654627
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2654614
    check-cast p1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionHeaderModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionHeaderModel$Serializer;->a(Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionHeaderModel;LX/0nX;LX/0my;)V

    return-void
.end method
