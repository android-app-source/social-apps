.class public final Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/JAa;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5827d2b7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2657541
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2657540
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2657538
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2657539
    return-void
.end method

.method private a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2657536
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    .line 2657537
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2657530
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2657531
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2657532
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2657533
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2657534
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2657535
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2657542
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2657543
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2657544
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    .line 2657545
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2657546
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;

    .line 2657547
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    .line 2657548
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2657549
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2657529
    new-instance v0, LX/JAx;

    invoke-direct {v0, p1}, LX/JAx;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2657527
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2657528
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2657526
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2657523
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel;-><init>()V

    .line 2657524
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2657525
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2657522
    const v0, -0x105d6d07

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2657521
    const v0, 0x41e065f

    return v0
.end method
