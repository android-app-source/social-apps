.class public final Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2653825
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;

    new-instance v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2653826
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2653792
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2653793
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2653794
    const/4 v2, 0x0

    .line 2653795
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 2653796
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2653797
    :goto_0
    move v1, v2

    .line 2653798
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2653799
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2653800
    new-instance v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;

    invoke-direct {v1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;-><init>()V

    .line 2653801
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2653802
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2653803
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2653804
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2653805
    :cond_0
    return-object v1

    .line 2653806
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2653807
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_6

    .line 2653808
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2653809
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2653810
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v6, :cond_2

    .line 2653811
    const-string p0, "mutual_friends"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2653812
    invoke-static {p1, v0}, LX/JBf;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2653813
    :cond_3
    const-string p0, "name"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2653814
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2653815
    :cond_4
    const-string p0, "profile_field_sections"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2653816
    invoke-static {p1, v0}, LX/JAQ;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2653817
    :cond_5
    const-string p0, "structured_name"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2653818
    invoke-static {p1, v0}, LX/JBc;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 2653819
    :cond_6
    const/4 v6, 0x4

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2653820
    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2653821
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2653822
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2653823
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2653824
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    goto :goto_1
.end method
