.class public final Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/JAc;
.implements LX/JAb;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2bea1fc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2660976
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2660967
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2660968
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2660969
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2660970
    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2660971
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2660972
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660973
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2660974
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2660975
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2661034
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2661035
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSubtitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660977
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2660978
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660979
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2660980
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->m:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 12

    .prologue
    .line 2660981
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2660982
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->l()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2660983
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2660984
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2660985
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2660986
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2660987
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2660988
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->mU_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2660989
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->n()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, 0x6f8fc4b2

    invoke-static {v8, v7, v9}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2660990
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->o()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    const v10, 0x76f20bf7

    invoke-static {v9, v8, v10}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2660991
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->mV_()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2660992
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2660993
    const/16 v11, 0xb

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2660994
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v0}, LX/186;->b(II)V

    .line 2660995
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2660996
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2660997
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2660998
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2660999
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2661000
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2661001
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2661002
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2661003
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2661004
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2661005
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2661006
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2661007
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2661008
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2661009
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    .line 2661010
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2661011
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;

    .line 2661012
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    .line 2661013
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2661014
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2661015
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2661016
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;

    .line 2661017
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2661018
    :cond_1
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2661019
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6f8fc4b2

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2661020
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2661021
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;

    .line 2661022
    iput v3, v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->l:I

    move-object v1, v0

    .line 2661023
    :cond_2
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2661024
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x76f20bf7

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2661025
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2661026
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;

    .line 2661027
    iput v3, v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->m:I

    move-object v1, v0

    .line 2661028
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2661029
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 2661030
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2661031
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_4
    move-object p0, v1

    .line 2661032
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2661033
    new-instance v0, LX/JBu;

    invoke-direct {v0, p1}, LX/JBu;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660962
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2660963
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2660964
    const/4 v0, 0x7

    const v1, 0x6f8fc4b2

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->l:I

    .line 2660965
    const/16 v0, 0x8

    const v1, 0x76f20bf7

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->m:I

    .line 2660966
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2660941
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2660942
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2660943
    return-void
.end method

.method public final synthetic b()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660944
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2660945
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;

    invoke-direct {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;-><init>()V

    .line 2660946
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2660947
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660948
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->h:Ljava/lang/String;

    .line 2660949
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660950
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->i:Ljava/lang/String;

    .line 2660951
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2660952
    const v0, 0x1c2b774e

    return v0
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660953
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->j:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->j:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2660954
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->j:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2660961
    const v0, 0x252222

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660955
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->o:Ljava/lang/String;

    .line 2660956
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCollections"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660939
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    .line 2660940
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->f:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    return-object v0
.end method

.method public final mU_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660957
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->k:Ljava/lang/String;

    .line 2660958
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final mV_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660959
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->n:Ljava/lang/String;

    .line 2660960
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;->n:Ljava/lang/String;

    return-object v0
.end method
