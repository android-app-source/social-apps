.class public final Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x95538f0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2657406
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2657342
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2657404
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2657405
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2657401
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2657402
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2657403
    return-void
.end method

.method public static a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;
    .locals 11

    .prologue
    .line 2657374
    if-nez p0, :cond_0

    .line 2657375
    const/4 p0, 0x0

    .line 2657376
    :goto_0
    return-object p0

    .line 2657377
    :cond_0
    instance-of v0, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    if-eqz v0, :cond_1

    .line 2657378
    check-cast p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    goto :goto_0

    .line 2657379
    :cond_1
    new-instance v2, LX/JAv;

    invoke-direct {v2}, LX/JAv;-><init>()V

    .line 2657380
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2657381
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2657382
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;

    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2657383
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2657384
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/JAv;->a:LX/0Px;

    .line 2657385
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->b()LX/0us;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a(LX/0us;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, v2, LX/JAv;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2657386
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 2657387
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2657388
    iget-object v5, v2, LX/JAv;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2657389
    iget-object v7, v2, LX/JAv;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-static {v4, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2657390
    const/4 v9, 0x2

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 2657391
    invoke-virtual {v4, v10, v5}, LX/186;->b(II)V

    .line 2657392
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 2657393
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2657394
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2657395
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2657396
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2657397
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2657398
    new-instance v5, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    invoke-direct {v5, v4}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;-><init>(LX/15i;)V

    .line 2657399
    move-object p0, v5

    .line 2657400
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2657372
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2657373
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2657364
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2657365
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2657366
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2657367
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2657368
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2657369
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2657370
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2657371
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2657362
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->e:Ljava/util/List;

    .line 2657363
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2657349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2657350
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2657351
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2657352
    if-eqz v1, :cond_2

    .line 2657353
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    .line 2657354
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2657355
    :goto_0
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2657356
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2657357
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2657358
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    .line 2657359
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2657360
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2657361
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final synthetic b()LX/0us;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2657348
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2657345
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;-><init>()V

    .line 2657346
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2657347
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2657344
    const v0, -0x7d11d983

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2657343
    const v0, -0x28a366f1

    return v0
.end method
