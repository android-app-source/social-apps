.class public final Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/JA3;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6efacfec
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2658745
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2658744
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2658742
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2658743
    return-void
.end method

.method private j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2658740
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    .line 2658741
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2658738
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    .line 2658739
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2658728
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2658729
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2658730
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2658731
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2658732
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2658733
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2658734
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2658735
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2658736
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2658737
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2658715
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2658716
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2658717
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    .line 2658718
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2658719
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;

    .line 2658720
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    .line 2658721
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2658722
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    .line 2658723
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2658724
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;

    .line 2658725
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    .line 2658726
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2658727
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2658714
    new-instance v0, LX/JBN;

    invoke-direct {v0, p1}, LX/JBN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2658712
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->f:Ljava/lang/String;

    .line 2658713
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2658703
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2658704
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2658711
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2658708
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;-><init>()V

    .line 2658709
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2658710
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2658707
    const v0, 0x5ceae195

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2658706
    const v0, 0x285feb

    return v0
.end method

.method public final synthetic k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2658705
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AboutPageUserFieldsModel;->j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    return-object v0
.end method
