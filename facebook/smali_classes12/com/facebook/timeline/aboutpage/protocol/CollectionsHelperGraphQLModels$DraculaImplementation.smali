.class public final Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2657750
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2657751
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2657748
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2657749
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2657725
    if-nez p1, :cond_0

    .line 2657726
    :goto_0
    return v1

    .line 2657727
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2657728
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2657729
    :sswitch_0
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2657730
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    move-object v0, p3

    .line 2657731
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2657732
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2657733
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2657734
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2657735
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2657736
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2657737
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2657738
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2657739
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2657740
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2657741
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2657742
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2657743
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2657744
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2657745
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2657746
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2657747
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1b2b9a67 -> :sswitch_1
        0x6f8fc4b2 -> :sswitch_2
        0x76f20bf7 -> :sswitch_3
        0x775ec459 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2657724
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2657721
    sparse-switch p0, :sswitch_data_0

    .line 2657722
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2657723
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x1b2b9a67 -> :sswitch_0
        0x6f8fc4b2 -> :sswitch_0
        0x76f20bf7 -> :sswitch_0
        0x775ec459 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2657720
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2657718
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;->b(I)V

    .line 2657719
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2657713
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2657714
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2657715
    :cond_0
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2657716
    iput p2, p0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;->b:I

    .line 2657717
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2657687
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2657712
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2657709
    iget v0, p0, LX/1vt;->c:I

    .line 2657710
    move v0, v0

    .line 2657711
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2657706
    iget v0, p0, LX/1vt;->c:I

    .line 2657707
    move v0, v0

    .line 2657708
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2657703
    iget v0, p0, LX/1vt;->b:I

    .line 2657704
    move v0, v0

    .line 2657705
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2657700
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2657701
    move-object v0, v0

    .line 2657702
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2657691
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2657692
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2657693
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2657694
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2657695
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2657696
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2657697
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2657698
    invoke-static {v3, v9, v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2657699
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2657688
    iget v0, p0, LX/1vt;->c:I

    .line 2657689
    move v0, v0

    .line 2657690
    return v0
.end method
