.class public final Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2659044
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    new-instance v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2659045
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2659039
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2659041
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2659042
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1}, LX/JBW;->a(LX/15i;ILX/0nX;)V

    .line 2659043
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2659040
    check-cast p1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel$Serializer;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;LX/0nX;LX/0my;)V

    return-void
.end method
