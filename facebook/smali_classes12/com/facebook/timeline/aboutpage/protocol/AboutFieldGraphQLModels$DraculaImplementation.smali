.class public final Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2653968
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2653969
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2654091
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2654092
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 2653992
    if-nez p1, :cond_0

    move v0, v1

    .line 2653993
    :goto_0
    return v0

    .line 2653994
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2653995
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2653996
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2653997
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2653998
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2653999
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654000
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2654001
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2654002
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2654003
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2654004
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654005
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2654006
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2654007
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2654008
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2654009
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654010
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2654011
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2654012
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2654013
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2654014
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654015
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2654016
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2654017
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2654018
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2654019
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654020
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2654021
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2654022
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2654023
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2654024
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2654025
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2654026
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654027
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2654028
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2654029
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2654030
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2654031
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2654032
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654033
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2654034
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2654035
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2654036
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2654037
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654038
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2654039
    :sswitch_8
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2654040
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2654041
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2654042
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654043
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2654044
    :sswitch_9
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2654045
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2654046
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2654047
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654048
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2654049
    :sswitch_a
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2654050
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2654051
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2654052
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654053
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2654054
    :sswitch_b
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2654055
    const v2, -0x49d6164e

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2654056
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2654057
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654058
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2654059
    :sswitch_c
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLProfileFieldTextListItemHeadingType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileFieldTextListItemHeadingType;

    move-result-object v0

    .line 2654060
    invoke-virtual {p3, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2654061
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 2654062
    const v3, -0x726fe14e

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2654063
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2654064
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654065
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2654066
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2654067
    :sswitch_d
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2654068
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2654069
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 2654070
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2654071
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654072
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 2654073
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2654074
    :sswitch_e
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2654075
    const v2, -0x46b8ff84

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2654076
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2654077
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2654078
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2654079
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654080
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2654081
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2654082
    :sswitch_f
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$TextWithEntitiesInfoModel$RangesModel$EntityModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$TextWithEntitiesInfoModel$RangesModel$EntityModel;

    .line 2654083
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2654084
    invoke-virtual {p0, p1, v5, v1}, LX/15i;->a(III)I

    move-result v2

    .line 2654085
    invoke-virtual {p0, p1, v6, v1}, LX/15i;->a(III)I

    move-result v3

    .line 2654086
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2654087
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2654088
    invoke-virtual {p3, v5, v2, v1}, LX/186;->a(III)V

    .line 2654089
    invoke-virtual {p3, v6, v3, v1}, LX/186;->a(III)V

    .line 2654090
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7b516522 -> :sswitch_4
        -0x7358774e -> :sswitch_8
        -0x726fe14e -> :sswitch_e
        -0x57280b6e -> :sswitch_b
        -0x49d6164e -> :sswitch_c
        -0x46b8ff84 -> :sswitch_f
        -0x469b2c93 -> :sswitch_2
        -0x45ac2a89 -> :sswitch_1
        -0x235f45ed -> :sswitch_5
        -0x1b7707a1 -> :sswitch_0
        -0xd3da70d -> :sswitch_a
        0x7aaadf5 -> :sswitch_9
        0x15de8280 -> :sswitch_6
        0x4b19ee1c -> :sswitch_3
        0x5911dec3 -> :sswitch_7
        0x6da358be -> :sswitch_d
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2653983
    if-nez p0, :cond_0

    move v0, v1

    .line 2653984
    :goto_0
    return v0

    .line 2653985
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2653986
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2653987
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2653988
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2653989
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2653990
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2653991
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2653976
    const/4 v7, 0x0

    .line 2653977
    const/4 v1, 0x0

    .line 2653978
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2653979
    invoke-static {v2, v3, v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2653980
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2653981
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2653982
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2653975
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2653970
    if-eqz p0, :cond_0

    .line 2653971
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2653972
    if-eq v0, p0, :cond_0

    .line 2653973
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2653974
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2653957
    sparse-switch p2, :sswitch_data_0

    .line 2653958
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2653959
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2653960
    const v1, -0x49d6164e

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2653961
    :goto_0
    :sswitch_1
    return-void

    .line 2653962
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2653963
    const v1, -0x726fe14e

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2653964
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2653965
    const v1, -0x46b8ff84

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2653966
    :sswitch_4
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$TextWithEntitiesInfoModel$RangesModel$EntityModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$TextWithEntitiesInfoModel$RangesModel$EntityModel;

    .line 2653967
    invoke-static {v0, p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7b516522 -> :sswitch_1
        -0x7358774e -> :sswitch_1
        -0x726fe14e -> :sswitch_3
        -0x57280b6e -> :sswitch_0
        -0x49d6164e -> :sswitch_2
        -0x46b8ff84 -> :sswitch_4
        -0x469b2c93 -> :sswitch_1
        -0x45ac2a89 -> :sswitch_1
        -0x235f45ed -> :sswitch_1
        -0x1b7707a1 -> :sswitch_1
        -0xd3da70d -> :sswitch_1
        0x7aaadf5 -> :sswitch_1
        0x15de8280 -> :sswitch_1
        0x4b19ee1c -> :sswitch_1
        0x5911dec3 -> :sswitch_1
        0x6da358be -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2653947
    if-nez p1, :cond_0

    move v0, v1

    .line 2653948
    :goto_0
    return v0

    .line 2653949
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 2653950
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2653951
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2653952
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 2653953
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 2653954
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2653955
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2653956
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2653940
    if-eqz p1, :cond_0

    .line 2653941
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 2653942
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2653943
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 2653944
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2653945
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2653946
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2654093
    if-eqz p1, :cond_0

    .line 2654094
    invoke-static {p0, p1, p2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2654095
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    .line 2654096
    if-eq v0, v1, :cond_0

    .line 2654097
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2654098
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2653906
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2653908
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2653909
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2653910
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2653911
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2653912
    :cond_0
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2653913
    iput p2, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->b:I

    .line 2653914
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2653915
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2653907
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2653916
    iget v0, p0, LX/1vt;->c:I

    .line 2653917
    move v0, v0

    .line 2653918
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2653919
    iget v0, p0, LX/1vt;->c:I

    .line 2653920
    move v0, v0

    .line 2653921
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2653922
    iget v0, p0, LX/1vt;->b:I

    .line 2653923
    move v0, v0

    .line 2653924
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2653925
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2653926
    move-object v0, v0

    .line 2653927
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2653928
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2653929
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2653930
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2653931
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2653932
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2653933
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2653934
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2653935
    invoke-static {v3, v9, v2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2653936
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2653937
    iget v0, p0, LX/1vt;->c:I

    .line 2653938
    move v0, v0

    .line 2653939
    return v0
.end method
