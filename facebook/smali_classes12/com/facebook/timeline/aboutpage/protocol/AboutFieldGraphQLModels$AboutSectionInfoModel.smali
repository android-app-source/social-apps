.class public final Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/JA3;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x55c9ea63
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2653905
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2653904
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2653902
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2653903
    return-void
.end method

.method private l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2653900
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    .line 2653901
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2653898
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    .line 2653899
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2653886
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2653887
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2653888
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2653889
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2653890
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2653891
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2653892
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2653893
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2653894
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2653895
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2653896
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2653897
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2653868
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2653869
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2653870
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    .line 2653871
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2653872
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;

    .line 2653873
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    .line 2653874
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2653875
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    .line 2653876
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2653877
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;

    .line 2653878
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->g:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    .line 2653879
    :cond_1
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2653880
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    .line 2653881
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2653882
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;

    .line 2653883
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    .line 2653884
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2653885
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2653867
    new-instance v0, LX/JA5;

    invoke-direct {v0, p1}, LX/JA5;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2653854
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->f:Ljava/lang/String;

    .line 2653855
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2653865
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2653866
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2653864
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2653861
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;

    invoke-direct {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;-><init>()V

    .line 2653862
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2653863
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2653860
    const v0, -0x318cb55a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2653859
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfileFieldSections"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2653857
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->g:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->g:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    .line 2653858
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->g:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2653856
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$AboutSectionInfoModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    return-object v0
.end method
