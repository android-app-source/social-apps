.class public final Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5465bc8e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2654577
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2654576
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2654574
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2654575
    return-void
.end method

.method private a()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfileFields"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654572
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    .line 2654573
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2654578
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2654579
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2654580
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2654581
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2654582
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2654583
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2654564
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2654565
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2654566
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    .line 2654567
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2654568
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;

    .line 2654569
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel$ProfileFieldsModel;

    .line 2654570
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2654571
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2654561
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldSectionFieldsModel;-><init>()V

    .line 2654562
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2654563
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2654560
    const v0, -0x1b057d7b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2654559
    const v0, 0x6d12b8b4

    return v0
.end method
