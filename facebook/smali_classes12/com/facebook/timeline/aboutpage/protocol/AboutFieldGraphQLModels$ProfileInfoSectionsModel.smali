.class public final Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x11873cb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2654846
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2654847
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2654854
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2654855
    return-void
.end method

.method private a()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfileFieldSections"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654835
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    .line 2654836
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2654848
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2654849
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2654850
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2654851
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2654852
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2654853
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2654837
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2654838
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2654839
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    .line 2654840
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2654841
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;

    .line 2654842
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;->e:Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel$ProfileFieldSectionsModel;

    .line 2654843
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2654844
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2654845
    new-instance v0, LX/JA8;

    invoke-direct {v0, p1}, LX/JA8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2654828
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2654829
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2654827
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2654830
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileInfoSectionsModel;-><init>()V

    .line 2654831
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2654832
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2654833
    const v0, -0x18e4c9b9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2654834
    const v0, 0x285feb

    return v0
.end method
