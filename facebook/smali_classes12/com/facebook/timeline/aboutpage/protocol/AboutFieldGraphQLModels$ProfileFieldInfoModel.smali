.class public final Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x768cf651
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$AssociatedPagesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Z

.field private q:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2654342
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2654343
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2654344
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2654345
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 21

    .prologue
    .line 2654346
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2654347
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 2654348
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->k()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x1b7707a1

    invoke-static {v5, v4, v6}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2654349
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->a()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2654350
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->l()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, -0x45ac2a89

    invoke-static {v7, v6, v8}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2654351
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->n()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, -0x469b2c93

    invoke-static {v8, v7, v9}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2654352
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2654353
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->p()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2654354
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->q()LX/2uF;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v10, v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v10

    .line 2654355
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->r()LX/0Px;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v11

    .line 2654356
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->s()LX/1vs;

    move-result-object v12

    iget-object v13, v12, LX/1vs;->a:LX/15i;

    iget v12, v12, LX/1vs;->b:I

    const v14, -0x235f45ed

    invoke-static {v13, v12, v14}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2654357
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->u()LX/2uF;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v13

    .line 2654358
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->b()LX/0Px;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(Ljava/util/List;)I

    move-result v14

    .line 2654359
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v15

    iget-object v0, v15, LX/1vs;->a:LX/15i;

    move-object/from16 v16, v0

    iget v15, v15, LX/1vs;->b:I

    const v17, 0x5911dec3

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-static {v0, v15, v1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v0, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 2654360
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->v()LX/0Px;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v16

    .line 2654361
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->w()LX/1vs;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, LX/1vs;->b:I

    move/from16 v17, v0

    const v19, -0x7358774e

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 2654362
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->d()LX/1vs;

    move-result-object v18

    move-object/from16 v0, v18

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    iget v0, v0, LX/1vs;->b:I

    move/from16 v18, v0

    const v20, 0x7aaadf5    # 2.5681E-34f

    move-object/from16 v0, v19

    move/from16 v1, v18

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 2654363
    const/16 v19, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2654364
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2654365
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 2654366
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 2654367
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2654368
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 2654369
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2654370
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 2654371
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 2654372
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 2654373
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 2654374
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 2654375
    const/16 v3, 0xb

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 2654376
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 2654377
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 2654378
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 2654379
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2654380
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2654381
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2654382
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2654383
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2654384
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2654385
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 2654386
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2654387
    if-eqz v1, :cond_b

    .line 2654388
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2654389
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2654390
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2654391
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1b7707a1

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2654392
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2654393
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2654394
    iput v3, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->f:I

    move-object v1, v0

    .line 2654395
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2654396
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x45ac2a89

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2654397
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2654398
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2654399
    iput v3, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->h:I

    move-object v1, v0

    .line 2654400
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2654401
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x469b2c93

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2654402
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2654403
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2654404
    iput v3, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j:I

    move-object v1, v0

    .line 2654405
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->q()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2654406
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->q()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 2654407
    if-eqz v2, :cond_3

    .line 2654408
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2654409
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->m:LX/3Sb;

    move-object v1, v0

    .line 2654410
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2654411
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->r()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 2654412
    if-eqz v2, :cond_4

    .line 2654413
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2654414
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->n:Ljava/util/List;

    move-object v1, v0

    .line 2654415
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->s()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 2654416
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->s()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x235f45ed

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2654417
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->s()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2654418
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2654419
    iput v3, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->o:I

    move-object v1, v0

    .line 2654420
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->u()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2654421
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->u()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 2654422
    if-eqz v2, :cond_6

    .line 2654423
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2654424
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->q:LX/3Sb;

    move-object v1, v0

    .line 2654425
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 2654426
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5911dec3

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 2654427
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->c()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2654428
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2654429
    iput v3, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->s:I

    move-object v1, v0

    .line 2654430
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->w()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 2654431
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->w()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7358774e

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 2654432
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->w()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2654433
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2654434
    iput v3, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->u:I

    move-object v1, v0

    .line 2654435
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->d()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_9

    .line 2654436
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->d()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7aaadf5    # 2.5681E-34f

    invoke-static {v2, v0, v3}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 2654437
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->d()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2654438
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    .line 2654439
    iput v3, v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->v:I

    move-object v1, v0

    .line 2654440
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2654441
    if-nez v1, :cond_a

    :goto_1
    return-object p0

    .line 2654442
    :catchall_0
    move-exception v0

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    throw v0

    .line 2654443
    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    throw v0

    .line 2654444
    :catchall_2
    move-exception v0

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v0

    .line 2654445
    :catchall_3
    move-exception v0

    :try_start_a
    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v0

    .line 2654446
    :catchall_4
    move-exception v0

    :try_start_b
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    throw v0

    .line 2654447
    :catchall_5
    move-exception v0

    :try_start_c
    monitor-exit v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    throw v0

    .line 2654448
    :catchall_6
    move-exception v0

    :try_start_d
    monitor-exit v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_6

    throw v0

    :cond_a
    move-object p0, v1

    .line 2654449
    goto :goto_1

    :cond_b
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654450
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->g:Ljava/lang/String;

    .line 2654451
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2654452
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2654453
    const/4 v0, 0x1

    const v1, -0x1b7707a1

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->f:I

    .line 2654454
    const/4 v0, 0x3

    const v1, -0x45ac2a89

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->h:I

    .line 2654455
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->i:Z

    .line 2654456
    const/4 v0, 0x5

    const v1, -0x469b2c93

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j:I

    .line 2654457
    const/16 v0, 0xa

    const v1, -0x235f45ed

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->o:I

    .line 2654458
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->p:Z

    .line 2654459
    const/16 v0, 0xe

    const v1, 0x5911dec3

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->s:I

    .line 2654460
    const/16 v0, 0x10

    const v1, -0x7358774e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->u:I

    .line 2654461
    const/16 v0, 0x11

    const v1, 0x7aaadf5    # 2.5681E-34f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->v:I

    .line 2654462
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2654463
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->r:Ljava/util/List;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/graphql/enums/GraphQLProfileFieldStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->r:Ljava/util/List;

    .line 2654464
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->r:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2654465
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;

    invoke-direct {v0}, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;-><init>()V

    .line 2654466
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2654467
    return-object v0
.end method

.method public final c()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTextContent"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654468
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2654469
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->s:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getUpsellText"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654470
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2654471
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->v:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2654472
    const v0, 0x1a27f8c4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2654341
    const v0, 0x51591cf1

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$AssociatedPagesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2654473
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$AssociatedPagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->e:Ljava/util/List;

    .line 2654474
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDateContent"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654315
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2654316
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEmail"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654317
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2654318
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 2654319
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2654320
    iget-boolean v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->i:Z

    return v0
.end method

.method public final n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getIcon"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654321
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2654322
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654323
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->k:Ljava/lang/String;

    .line 2654324
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654325
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->l:Ljava/lang/String;

    .line 2654326
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final q()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getListItemGroups"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2654327
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->m:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x8

    const v4, -0x57280b6e

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->m:LX/3Sb;

    .line 2654328
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->m:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final r()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMenuOptions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2654329
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->n:Ljava/util/List;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel$MenuOptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->n:Ljava/util/List;

    .line 2654330
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->n:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final s()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhoneNumber"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654331
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2654332
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->o:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 2654333
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2654334
    iget-boolean v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->p:Z

    return v0
.end method

.method public final u()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStringsListContent"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2654335
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->q:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0xc

    const v4, 0x15de8280

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->q:LX/3Sb;

    .line 2654336
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->q:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final v()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2654337
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->t:Ljava/util/List;

    const/16 v1, 0xf

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->t:Ljava/util/List;

    .line 2654338
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->t:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final w()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2654339
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2654340
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/timeline/aboutpage/protocol/AboutFieldGraphQLModels$ProfileFieldInfoModel;->u:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
