.class public final Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/JA3;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x58f27415
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2658859
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2658858
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2658856
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2658857
    return-void
.end method

.method private l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2658854
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    .line 2658855
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2658852
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    .line 2658853
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2658840
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2658841
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2658842
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2658843
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2658844
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2658845
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2658846
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2658847
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2658848
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2658849
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2658850
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2658851
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2658822
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2658823
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2658824
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    .line 2658825
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2658826
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;

    .line 2658827
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->e:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    .line 2658828
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2658829
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    .line 2658830
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->m()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2658831
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;

    .line 2658832
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->g:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineAppSectionsStructuredNameModel;

    .line 2658833
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2658834
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    .line 2658835
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2658836
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;

    .line 2658837
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    .line 2658838
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2658839
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2658821
    new-instance v0, LX/JBO;

    invoke-direct {v0, p1}, LX/JBO;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2658808
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->f:Ljava/lang/String;

    .line 2658809
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2658819
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2658820
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2658818
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2658815
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;

    invoke-direct {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;-><init>()V

    .line 2658816
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2658817
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2658814
    const v0, 0x47fd2ba7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2658813
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimelineCollectionAppSections"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2658811
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    .line 2658812
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2658810
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$AppSectionInfoModel;->l()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineMutualFriendsModel;

    move-result-object v0

    return-object v0
.end method
