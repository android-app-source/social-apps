.class public final Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/JBL;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5db855fe
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2660551
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2660550
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2660548
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2660549
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2660545
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2660546
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2660547
    return-void
.end method

.method public static a(LX/JBL;)Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2660524
    if-nez p0, :cond_0

    .line 2660525
    const/4 p0, 0x0

    .line 2660526
    :goto_0
    return-object p0

    .line 2660527
    :cond_0
    instance-of v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    if-eqz v0, :cond_1

    .line 2660528
    check-cast p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    goto :goto_0

    .line 2660529
    :cond_1
    new-instance v1, LX/JBk;

    invoke-direct {v1}, LX/JBk;-><init>()V

    .line 2660530
    invoke-interface {p0}, LX/JAX;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/JBk;->a:Ljava/lang/String;

    .line 2660531
    invoke-interface {p0}, LX/JBK;->r()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v0

    iput-object v0, v1, LX/JBk;->b:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    .line 2660532
    invoke-interface {p0}, LX/JAV;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/JBk;->c:Ljava/lang/String;

    .line 2660533
    invoke-interface {p0}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    iput-object v0, v1, LX/JBk;->d:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    .line 2660534
    invoke-interface {p0}, LX/JBK;->mW_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/JBk;->e:Ljava/lang/String;

    .line 2660535
    invoke-interface {p0}, LX/JBK;->q()LX/174;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a(LX/174;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    iput-object v0, v1, LX/JBk;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2660536
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2660537
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p0}, LX/JAW;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 2660538
    invoke-interface {p0}, LX/JAW;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2660539
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2660540
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v1, LX/JBk;->g:LX/0Px;

    .line 2660541
    invoke-interface {p0}, LX/JBK;->o()Z

    move-result v0

    iput-boolean v0, v1, LX/JBk;->h:Z

    .line 2660542
    invoke-interface {p0}, LX/JBK;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/JBk;->i:Ljava/lang/String;

    .line 2660543
    invoke-interface {p0}, LX/JBK;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/JBk;->j:Ljava/lang/String;

    .line 2660544
    invoke-virtual {v1}, LX/JBk;->a()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    move-result-object p0

    goto :goto_0
.end method

.method private m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660522
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->f:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->f:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    .line 2660523
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->f:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660520
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2660521
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 2660497
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2660498
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2660499
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2660500
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2660501
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2660502
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->mW_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2660503
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2660504
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->d()LX/0Px;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->c(Ljava/util/List;)I

    move-result v6

    .line 2660505
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2660506
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->p()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2660507
    const/16 v9, 0xa

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2660508
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 2660509
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2660510
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2660511
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2660512
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2660513
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2660514
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2660515
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2660516
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2660517
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2660518
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2660519
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2660479
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2660480
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2660481
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    .line 2660482
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2660483
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    .line 2660484
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->f:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    .line 2660485
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2660486
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    .line 2660487
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2660488
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    .line 2660489
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->h:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    .line 2660490
    :cond_1
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2660491
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2660492
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2660493
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    .line 2660494
    iput-object v0, v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->j:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2660495
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2660496
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660478
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2660475
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2660476
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l:Z

    .line 2660477
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2660472
    new-instance v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    invoke-direct {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;-><init>()V

    .line 2660473
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2660474
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660552
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->e:Ljava/lang/String;

    .line 2660553
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660470
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->g:Ljava/lang/String;

    .line 2660471
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2660468
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->k:Ljava/util/List;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->k:Ljava/util/List;

    .line 2660469
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->k:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2660467
    const v0, 0x38514256

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2660466
    const v0, -0x57fc1342

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660464
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->m:Ljava/lang/String;

    .line 2660465
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660463
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660461
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->h:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->h:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    .line 2660462
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->h:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    return-object v0
.end method

.method public final mW_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660459
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->i:Ljava/lang/String;

    .line 2660460
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 2660453
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2660454
    iget-boolean v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660457
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->n:Ljava/lang/String;

    .line 2660458
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic q()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660456
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->n()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic r()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2660455
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v0

    return-object v0
.end method
