.class public final Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2661240
    const-class v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    new-instance v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2661241
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2661189
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2661191
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2661192
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x5

    const/4 v3, 0x0

    .line 2661193
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2661194
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2661195
    if-eqz v2, :cond_0

    .line 2661196
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2661197
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2661198
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2661199
    if-eqz v2, :cond_1

    .line 2661200
    const-string v3, "collections"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2661201
    invoke-static {v1, v2, p1, p2}, LX/JC0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2661202
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2661203
    if-eqz v2, :cond_2

    .line 2661204
    const-string v3, "icon_image"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2661205
    invoke-static {v1, v2, p1}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2661206
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2661207
    if-eqz v2, :cond_3

    .line 2661208
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2661209
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2661210
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2661211
    if-eqz v2, :cond_4

    .line 2661212
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2661213
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2661214
    :cond_4
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2661215
    if-eqz v2, :cond_5

    .line 2661216
    const-string v2, "section_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2661217
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2661218
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2661219
    if-eqz v2, :cond_6

    .line 2661220
    const-string v3, "standalone_url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2661221
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2661222
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2661223
    if-eqz v2, :cond_7

    .line 2661224
    const-string v3, "subtitle"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2661225
    invoke-static {v1, v2, p1}, LX/JB9;->a(LX/15i;ILX/0nX;)V

    .line 2661226
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2661227
    if-eqz v2, :cond_8

    .line 2661228
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2661229
    invoke-static {v1, v2, p1}, LX/JBA;->a(LX/15i;ILX/0nX;)V

    .line 2661230
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2661231
    if-eqz v2, :cond_9

    .line 2661232
    const-string v3, "tracking"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2661233
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2661234
    :cond_9
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2661235
    if-eqz v2, :cond_a

    .line 2661236
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2661237
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2661238
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2661239
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2661190
    check-cast p1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$Serializer;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
