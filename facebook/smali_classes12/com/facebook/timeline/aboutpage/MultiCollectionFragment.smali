.class public abstract Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;
.super Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.source ""

# interfaces
.implements LX/0hF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;",
        "LX/0hF;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/View;

.field public c:Landroid/os/Bundle;

.field public d:LX/62k;

.field public e:LX/0g8;

.field public f:LX/9lP;

.field public g:LX/J8y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/B0P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/feed/banner/GenericNotificationBanner;

.field public k:Landroid/view/View;

.field public l:Z

.field public m:Z

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/1PF;

.field public p:LX/B0O;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2652633
    invoke-direct {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;-><init>()V

    .line 2652634
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2652635
    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->i:LX/0Ot;

    .line 2652636
    iput-boolean v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->l:Z

    .line 2652637
    iput-boolean v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->m:Z

    .line 2652638
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2652639
    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->n:LX/0Ot;

    .line 2652640
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2652641
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2652642
    const v1, 0x7f0b248e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2652643
    invoke-static {p0, v0}, LX/JDA;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    .line 2652644
    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2652645
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2652646
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;

    invoke-static {p1}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/16 v4, 0x259

    invoke-static {p1, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p1}, LX/J8y;->a(LX/0QB;)LX/J8y;

    move-result-object v5

    check-cast v5, LX/J8y;

    const/16 v6, 0x2ca

    invoke-static {p1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v0, LX/B0P;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/B0P;

    iput-object v3, v2, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->a:Ljava/lang/String;

    iput-object v4, v2, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->i:LX/0Ot;

    iput-object v5, v2, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->g:LX/J8y;

    iput-object v6, v2, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->n:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->h:LX/B0P;

    .line 2652647
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->g:LX/J8y;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o()LX/J8x;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/J8y;->a(LX/J8x;)V

    .line 2652648
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2652649
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "timeline_invalid_meuser"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "logged in user: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652650
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2652651
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->a:Ljava/lang/String;

    const/4 v4, 0x0

    .line 2652652
    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->c:Landroid/os/Bundle;

    .line 2652653
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->c:Landroid/os/Bundle;

    const-string v3, "profile_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2652654
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v2, v1

    .line 2652655
    :cond_1
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2652656
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->c:Landroid/os/Bundle;

    if-eqz v3, :cond_4

    .line 2652657
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->c:Landroid/os/Bundle;

    const-string v4, "friendship_status"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    .line 2652658
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->c:Landroid/os/Bundle;

    const-string v5, "subscribe_status"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    .line 2652659
    :goto_1
    new-instance v5, LX/9lP;

    invoke-direct {v5, v2, v1, v4, v3}, LX/9lP;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    iput-object v5, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    .line 2652660
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->n()Ljava/lang/Object;

    move-result-object v0

    .line 2652661
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->g:LX/J8y;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o()LX/J8x;

    move-result-object v2

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v2, v0}, LX/J8y;->a(LX/J8x;Z)V

    .line 2652662
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->h:LX/B0P;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e()LX/B0U;

    move-result-object v2

    new-instance v3, LX/J9A;

    invoke-direct {v3, p0}, LX/J9A;-><init>(Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;)V

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->k()LX/B0N;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/B0P;->a(Ljava/lang/String;LX/B0U;LX/B0L;LX/B0N;)LX/B0O;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->p:LX/B0O;

    .line 2652663
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->p:LX/B0O;

    invoke-virtual {v0}, LX/B0O;->a()V

    .line 2652664
    return-void

    .line 2652665
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 2652666
    :cond_3
    const/4 v3, 0x0

    goto :goto_0

    :cond_4
    move-object v3, v4

    goto :goto_1
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract e()LX/B0U;
.end method

.method public abstract k()LX/B0N;
.end method

.method public abstract l()LX/3tK;
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 2652667
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->p:LX/B0O;

    if-eqz v0, :cond_0

    .line 2652668
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->p:LX/B0O;

    invoke-virtual {v0}, LX/B0O;->d()Z

    move-result v0

    .line 2652669
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final mJ_()V
    .locals 1

    .prologue
    .line 2652670
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->p:LX/B0O;

    if-eqz v0, :cond_0

    .line 2652671
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->p:LX/B0O;

    invoke-virtual {v0}, LX/B0O;->c()V

    .line 2652672
    :cond_0
    return-void
.end method

.method public abstract n()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract o()LX/J8x;
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x8c6b6c5

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2652616
    const v0, 0x7f0302b5

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->b:Landroid/view/View;

    .line 2652617
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->b:Landroid/view/View;

    const v2, 0x7f0d09a0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/62k;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    .line 2652618
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    new-instance v2, LX/J9B;

    invoke-direct {v2, p0}, LX/J9B;-><init>(Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;)V

    invoke-interface {v0, v2}, LX/62k;->setOnRefreshListener(LX/62n;)V

    .line 2652619
    new-instance v2, LX/2iI;

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    invoke-interface {v0}, LX/62k;->c()Landroid/view/ViewGroup;

    move-result-object v0

    const v3, 0x102000a

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {v2, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    .line 2652620
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->b:Landroid/view/View;

    const v3, 0x1020004

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0g8;->f(Landroid/view/View;)V

    .line 2652621
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/0g8;->d(Z)V

    .line 2652622
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    .line 2652623
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v2

    .line 2652624
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    invoke-interface {v3, v0, v6, v4}, LX/0g8;->a(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2652625
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    invoke-interface {v0, v2, v6, v4}, LX/0g8;->b(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2652626
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->b:Landroid/view/View;

    .line 2652627
    const v2, 0x7f0d08b9

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->k:Landroid/view/View;

    .line 2652628
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->k:Landroid/view/View;

    const v3, 0x7f0d1106

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2652629
    new-instance v3, LX/J9C;

    invoke-direct {v3, p0}, LX/J9C;-><init>(Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2652630
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->b:Landroid/view/View;

    const v3, 0x7f0d08bd

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/banner/GenericNotificationBanner;

    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->j:Lcom/facebook/feed/banner/GenericNotificationBanner;

    .line 2652631
    new-instance v2, LX/J9D;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3}, LX/J9D;-><init>(Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o:LX/1PF;

    .line 2652632
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->b:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x6d90a7e9

    invoke-static {v5, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1e82d668

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2652611
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->p:LX/B0O;

    if-eqz v1, :cond_0

    .line 2652612
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->p:LX/B0O;

    invoke-virtual {v1}, LX/B0O;->b()V

    .line 2652613
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->p:LX/B0O;

    .line 2652614
    :cond_0
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroy()V

    .line 2652615
    const/16 v1, 0x2b

    const v2, 0x29788714

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x50471146

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2652598
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroyView()V

    .line 2652599
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    if-eqz v1, :cond_0

    .line 2652600
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    invoke-interface {v1, v3}, LX/62k;->setOnRefreshListener(LX/62n;)V

    .line 2652601
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    if-eqz v1, :cond_1

    .line 2652602
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    invoke-interface {v1, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2652603
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->g:LX/J8y;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o()LX/J8x;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/J8y;->b(LX/J8x;)V

    .line 2652604
    iput-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->b:Landroid/view/View;

    .line 2652605
    iput-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    .line 2652606
    iput-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    .line 2652607
    iput-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->j:Lcom/facebook/feed/banner/GenericNotificationBanner;

    .line 2652608
    iput-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->k:Landroid/view/View;

    .line 2652609
    iput-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o:LX/1PF;

    .line 2652610
    const/16 v1, 0x2b

    const v2, 0x1807c10a

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x740ced4d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2652593
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->g:LX/J8y;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o()LX/J8x;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/J8y;->b(LX/J8x;)V

    .line 2652594
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o:LX/1PF;

    if-eqz v1, :cond_0

    .line 2652595
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o:LX/1PF;

    invoke-virtual {v1}, LX/14l;->b()V

    .line 2652596
    :cond_0
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onPause()V

    .line 2652597
    const/16 v1, 0x2b

    const v2, -0x68f83fea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1472674e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2652589
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o:LX/1PF;

    if-eqz v1, :cond_0

    .line 2652590
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->o:LX/1PF;

    invoke-virtual {v1}, LX/14l;->a()V

    .line 2652591
    :cond_0
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onResume()V

    .line 2652592
    const/16 v1, 0x2b

    const v2, -0x4091c647

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final s()LX/03V;
    .locals 1

    .prologue
    .line 2652588
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    return-object v0
.end method

.method public abstract u()LX/J8p;
.end method

.method public v()V
    .locals 0

    .prologue
    .line 2652587
    return-void
.end method

.method public w()V
    .locals 3

    .prologue
    .line 2652574
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    if-nez v0, :cond_1

    .line 2652575
    :cond_0
    :goto_0
    return-void

    .line 2652576
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->u()LX/J8p;

    move-result-object v0

    .line 2652577
    iget-boolean v1, v0, LX/J8p;->b:Z

    move v0, v1

    .line 2652578
    if-nez v0, :cond_2

    .line 2652579
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->u()LX/J8p;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    .line 2652580
    iget-object v2, v1, LX/9lP;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2652581
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    invoke-static {v2}, LX/J8p;->a(LX/9lP;)LX/9lQ;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/J8p;->a(Ljava/lang/String;LX/9lQ;)V

    .line 2652582
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->j:Lcom/facebook/feed/banner/GenericNotificationBanner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2652583
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2652584
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->j:Lcom/facebook/feed/banner/GenericNotificationBanner;

    sget-object v1, LX/DBa;->NO_CONNECTION:LX/DBa;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    goto :goto_0

    .line 2652585
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->l:Z

    .line 2652586
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->j:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-virtual {v0}, LX/4nk;->a()V

    goto :goto_0
.end method
