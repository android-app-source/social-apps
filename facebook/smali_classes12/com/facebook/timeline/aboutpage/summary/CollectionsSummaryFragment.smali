.class public Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;
.super Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fx;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->COLLECTIONS_SUMMARY_FRAGMENT:LX/0cQ;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/timeline/aboutpage/MultiCollectionFragment",
        "<",
        "LX/JA3;",
        ">;",
        "LX/0hF;",
        "Lcom/facebook/timeline/aboutpage/DeleteExperienceController$DeleteExperienceListener;",
        "LX/0fx;"
    }
.end annotation


# instance fields
.field public A:Landroid/os/ParcelUuid;

.field public B:LX/JCP;

.field public i:LX/J7A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/J9y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/JCJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/JCM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1Kt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/JCC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/J8z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/JCQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/JCX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/JCZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/1B1;

.field private v:LX/1B1;

.field private w:LX/JCL;

.field private x:LX/JCB;

.field public y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2662342
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;-><init>()V

    .line 2662343
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2662344
    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->y:LX/0Ot;

    .line 2662345
    return-void
.end method

.method private static a(Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;LX/J7A;LX/J9y;LX/0Or;LX/JCJ;LX/JCM;LX/1Kt;LX/JCC;LX/0Ot;LX/J8z;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/JCQ;LX/JCX;LX/JCZ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;",
            "LX/J7A;",
            "LX/J9y;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "LX/JCJ;",
            "LX/JCM;",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            "LX/JCC;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/J8z;",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            "LX/JCQ;",
            "LX/JCX;",
            "LX/JCZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2662341
    iput-object p1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->i:LX/J7A;

    iput-object p2, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->j:LX/J9y;

    iput-object p3, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->k:LX/0Or;

    iput-object p4, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->l:LX/JCJ;

    iput-object p5, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->m:LX/JCM;

    iput-object p6, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->n:LX/1Kt;

    iput-object p7, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->o:LX/JCC;

    iput-object p8, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->y:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->p:LX/J8z;

    iput-object p10, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->q:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p11, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->r:LX/JCQ;

    iput-object p12, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->s:LX/JCX;

    iput-object p13, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->t:LX/JCZ;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 15

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;

    invoke-static {v13}, LX/J7A;->a(LX/0QB;)LX/J7A;

    move-result-object v1

    check-cast v1, LX/J7A;

    invoke-static {v13}, LX/J9y;->a(LX/0QB;)LX/J9y;

    move-result-object v2

    check-cast v2, LX/J9y;

    const/16 v3, 0x2eb

    invoke-static {v13, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v13}, LX/JCJ;->b(LX/0QB;)LX/JCJ;

    move-result-object v4

    check-cast v4, LX/JCJ;

    const-class v5, LX/JCM;

    invoke-interface {v13, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/JCM;

    invoke-static {v13}, LX/23N;->b(LX/0QB;)LX/23N;

    move-result-object v6

    check-cast v6, LX/1Kt;

    const-class v7, LX/JCC;

    invoke-interface {v13, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/JCC;

    const/16 v8, 0x1032

    invoke-static {v13, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v13}, LX/J8z;->b(LX/0QB;)LX/J8z;

    move-result-object v9

    check-cast v9, LX/J8z;

    invoke-static {v13}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v10

    check-cast v10, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    const-class v11, LX/JCQ;

    invoke-interface {v13, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/JCQ;

    const-class v12, LX/JCX;

    invoke-interface {v13, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/JCX;

    const-class v14, LX/JCZ;

    invoke-interface {v13, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/JCZ;

    invoke-static/range {v0 .. v13}, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->a(Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;LX/J7A;LX/J9y;LX/0Or;LX/JCJ;LX/JCM;LX/1Kt;LX/JCC;LX/0Ot;LX/J8z;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/JCQ;LX/JCX;LX/JCZ;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2662340
    const-string v0, "collections_overview"

    return-object v0
.end method

.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 2662337
    if-nez p2, :cond_0

    .line 2662338
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->n:LX/1Kt;

    invoke-virtual {v0, p1}, LX/1Kt;->b(LX/0g8;)V

    .line 2662339
    :cond_0
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 2662334
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->n:LX/1Kt;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0fx;->a(LX/0g8;III)V

    .line 2662335
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->m()Z

    .line 2662336
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 2662301
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2662302
    invoke-super {p0, p1}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->a(Landroid/os/Bundle;)V

    .line 2662303
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->r:LX/JCQ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->l:LX/JCJ;

    invoke-virtual {v0, v1, v2, v3}, LX/JCQ;->a(Landroid/content/Context;LX/9lP;LX/J8p;)LX/JCP;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->B:LX/JCP;

    .line 2662304
    if-eqz p1, :cond_0

    .line 2662305
    const-string v0, "refresh_next_resume"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->z:Z

    .line 2662306
    const-string v0, "fragment_uuid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->A:Landroid/os/ParcelUuid;

    .line 2662307
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->A:Landroid/os/ParcelUuid;

    .line 2662308
    iput-object v1, v0, LX/9lP;->e:Landroid/os/ParcelUuid;

    .line 2662309
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->v:LX/1B1;

    .line 2662310
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->l:LX/JCJ;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    .line 2662311
    new-instance v2, LX/JCB;

    invoke-direct {v2, v0, v1}, LX/JCB;-><init>(LX/JCJ;LX/9lP;)V

    .line 2662312
    move-object v0, v2

    .line 2662313
    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->x:LX/JCB;

    .line 2662314
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->m:LX/JCM;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->l:LX/JCJ;

    .line 2662315
    new-instance v4, LX/JCL;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v8

    check-cast v8, LX/17W;

    invoke-static {v0}, LX/J8u;->a(LX/0QB;)LX/J8u;

    move-result-object v9

    check-cast v9, LX/J8u;

    invoke-static {v0}, LX/J9K;->a(LX/0QB;)LX/J9K;

    move-result-object v10

    check-cast v10, LX/J9K;

    .line 2662316
    new-instance v12, LX/J99;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v5

    check-cast v5, LX/11H;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, LX/0TD;

    invoke-direct {v12, v5, v6, v7, v11}, LX/J99;-><init>(LX/11H;Landroid/content/Context;LX/0TD;LX/0TD;)V

    .line 2662317
    move-object v11, v12

    .line 2662318
    check-cast v11, LX/J99;

    move-object v5, v1

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v11}, LX/JCL;-><init>(Landroid/content/Context;LX/9lP;LX/JCJ;LX/17W;LX/J8u;LX/J9K;LX/J99;)V

    .line 2662319
    move-object v0, v4

    .line 2662320
    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->w:LX/JCL;

    .line 2662321
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->w:LX/JCL;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->v:LX/1B1;

    .line 2662322
    new-instance v2, LX/JCK;

    invoke-direct {v2, v0}, LX/JCK;-><init>(LX/JCL;)V

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b2;)Z

    .line 2662323
    iget-object v2, v0, LX/JCL;->e:LX/J8u;

    iget-object v3, v0, LX/JCL;->a:Landroid/content/Context;

    iget-object v4, v0, LX/JCL;->b:LX/9lP;

    invoke-virtual {v2, v3, v4, v1}, LX/J8u;->a(Landroid/content/Context;LX/9lP;LX/1B1;)V

    .line 2662324
    iget-object v2, v0, LX/JCL;->f:LX/J9K;

    iget-object v3, v0, LX/JCL;->a:Landroid/content/Context;

    .line 2662325
    new-instance v4, LX/J9J;

    invoke-direct {v4, v2, v3}, LX/J9J;-><init>(LX/J9K;Landroid/content/Context;)V

    invoke-virtual {v1, v4}, LX/1B1;->a(LX/0b2;)Z

    .line 2662326
    iget-object v2, v0, LX/JCL;->g:LX/J99;

    .line 2662327
    new-instance v3, LX/J98;

    invoke-direct {v3, v2, p0}, LX/J98;-><init>(LX/J99;Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;)V

    invoke-virtual {v1, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 2662328
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->u:LX/1B1;

    .line 2662329
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->u:LX/1B1;

    new-instance v1, LX/JCR;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->A:Landroid/os/ParcelUuid;

    invoke-direct {v1, p0, v2}, LX/JCR;-><init>(Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;Landroid/os/ParcelUuid;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2662330
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->u:LX/1B1;

    new-instance v1, LX/JCS;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->A:Landroid/os/ParcelUuid;

    invoke-direct {v1, p0, v2}, LX/JCS;-><init>(Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;Landroid/os/ParcelUuid;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2662331
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->u:LX/1B1;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->i:LX/J7A;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 2662332
    return-void

    .line 2662333
    :cond_0
    new-instance v0, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->A:Landroid/os/ParcelUuid;

    goto/16 :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2662296
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 2662297
    const-string v1, "profile_id"

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    .line 2662298
    iget-object p0, v2, LX/9lP;->a:Ljava/lang/String;

    move-object v2, p0

    .line 2662299
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2662300
    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2662291
    const-string v0, "profile/%s/aboutpage?is_combined=%b&v=7"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    .line 2662292
    iget-object v4, v3, LX/9lP;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2662293
    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 2662294
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->y:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    sget-short v4, LX/0wf;->aj:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    move v3, v3

    .line 2662295
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/B0U;
    .locals 15

    .prologue
    .line 2662276
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->s:LX/JCX;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    .line 2662277
    iget-object v2, v1, LX/9lP;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2662278
    invoke-virtual {v0, v1}, LX/JCX;->a(Ljava/lang/String;)LX/JCW;

    move-result-object v1

    .line 2662279
    new-instance v0, LX/B0a;

    .line 2662280
    iget-object v2, v1, LX/JCW;->b:LX/0ad;

    sget-short v3, LX/0wf;->aj:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 2662281
    if-nez v2, :cond_0

    iget-object v2, v1, LX/JCW;->g:LX/J8z;

    invoke-virtual {v2}, LX/J8z;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2662282
    const-string v2, "collections_sections_end_cursor"

    .line 2662283
    :goto_0
    move-object v2, v2

    .line 2662284
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->t:LX/JCZ;

    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    .line 2662285
    iget-object v5, v4, LX/9lP;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2662286
    new-instance v8, LX/JCY;

    invoke-static {v3}, LX/J8o;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-static {v3}, LX/J90;->a(LX/0QB;)LX/J90;

    move-result-object v11

    check-cast v11, LX/J90;

    invoke-static {v3}, LX/JCx;->a(LX/0QB;)LX/JCx;

    move-result-object v12

    check-cast v12, LX/JCx;

    invoke-static {v3}, LX/J8z;->b(LX/0QB;)LX/J8z;

    move-result-object v13

    check-cast v13, LX/J8z;

    invoke-static {v3}, LX/JCV;->b(LX/0QB;)LX/JCV;

    move-result-object v14

    check-cast v14, LX/JCV;

    move-object v9, v4

    invoke-direct/range {v8 .. v14}, LX/JCY;-><init>(Ljava/lang/String;Ljava/lang/Boolean;LX/J90;LX/JCx;LX/J8z;LX/JCV;)V

    .line 2662287
    move-object v3, v8

    .line 2662288
    const-wide/32 v4, 0x15180

    .line 2662289
    const/4 v6, 0x1

    move v6, v6

    .line 2662290
    iget-object v7, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->p:LX/J8z;

    invoke-virtual {v7}, LX/J8z;->e()Z

    move-result v7

    invoke-direct/range {v0 .. v7}, LX/B0a;-><init>(LX/B0V;Ljava/lang/String;LX/B0Z;JZZ)V

    return-object v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final k()LX/B0N;
    .locals 1

    .prologue
    .line 2662275
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()LX/3tK;
    .locals 1

    .prologue
    .line 2662274
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->B:LX/JCP;

    return-object v0
.end method

.method public final n()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2662346
    const/4 v0, 0x0

    return-object v0
.end method

.method public final o()LX/J8x;
    .locals 1

    .prologue
    .line 2662273
    sget-object v0, LX/J8x;->SUMMARY:LX/J8x;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x40fb4e56

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2662268
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 2662269
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->b:Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 2662270
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->B:LX/JCP;

    invoke-interface {v2, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2662271
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->n:LX/1Kt;

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->x:LX/JCB;

    invoke-virtual {v2, v3}, LX/1Kt;->a(LX/1Ce;)V

    .line 2662272
    const/16 v2, 0x2b

    const v3, -0x66faf802

    invoke-static {v5, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x63c5fc6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2662262
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->u:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->i:LX/J7A;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2662263
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->B:LX/JCP;

    if-eqz v1, :cond_0

    .line 2662264
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->B:LX/JCP;

    invoke-virtual {v1, v4}, LX/3tK;->a(Landroid/database/Cursor;)V

    .line 2662265
    iput-object v4, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->B:LX/JCP;

    .line 2662266
    :cond_0
    invoke-super {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onDestroy()V

    .line 2662267
    const/16 v1, 0x2b

    const v2, -0x7984cbb1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6504ba3f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2662259
    invoke-super {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onDestroyView()V

    .line 2662260
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->n:LX/1Kt;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->x:LX/JCB;

    invoke-virtual {v1, v2}, LX/1Kt;->b(LX/1Ce;)V

    .line 2662261
    const/16 v1, 0x2b

    const v2, 0x3bdf62a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x242e5e50

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2662255
    invoke-super {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onPause()V

    .line 2662256
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->v:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->j:LX/J9y;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2662257
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->n:LX/1Kt;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    invoke-virtual {v1, v2}, LX/1Kt;->c(LX/0g8;)V

    .line 2662258
    const/16 v1, 0x2b

    const v2, -0x516554d0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x78eb4d5e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2662246
    invoke-super {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onResume()V

    .line 2662247
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2662248
    if-eqz v0, :cond_0

    .line 2662249
    const v2, 0x7f08156d

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2662250
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->v:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->j:LX/J9y;

    invoke-virtual {v0, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2662251
    iget-boolean v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->z:Z

    if-eqz v0, :cond_1

    .line 2662252
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->mJ_()V

    .line 2662253
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->z:Z

    .line 2662254
    :cond_1
    const/16 v0, 0x2b

    const v2, -0xf685349

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2662242
    invoke-super {p0, p1}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2662243
    const-string v0, "refresh_next_resume"

    iget-boolean v1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2662244
    const-string v0, "fragment_uuid"

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->A:Landroid/os/ParcelUuid;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2662245
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3812c91e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2662239
    invoke-super {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onStart()V

    .line 2662240
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    invoke-interface {v1, p0}, LX/0g8;->a(LX/0fx;)V

    .line 2662241
    const/16 v1, 0x2b

    const v2, 0x617c725

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3bec750c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2662235
    invoke-super {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onStop()V

    .line 2662236
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    if-eqz v1, :cond_0

    .line 2662237
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/0g8;->a(LX/0fx;)V

    .line 2662238
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x3f4c6e95

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final u()LX/J8p;
    .locals 1

    .prologue
    .line 2662234
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/summary/CollectionsSummaryFragment;->l:LX/JCJ;

    return-object v0
.end method

.method public final v()V
    .locals 0

    .prologue
    .line 2662232
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->m()Z

    .line 2662233
    return-void
.end method

.method public final w()V
    .locals 1

    .prologue
    .line 2662228
    invoke-super {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->w()V

    .line 2662229
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->g:LX/J8y;

    move-object v0, v0

    .line 2662230
    invoke-virtual {v0}, LX/J8y;->b()V

    .line 2662231
    return-void
.end method
