.class public Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;
.super Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;
.source ""

# interfaces
.implements LX/0hF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/timeline/aboutpage/MultiCollectionFragment",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;",
        ">;",
        "LX/0hF;"
    }
.end annotation


# instance fields
.field public i:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/JC4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/JC7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/J9y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/JC3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/JC9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field public q:LX/JC6;

.field public r:LX/1B1;

.field private s:LX/JC2;

.field private t:LX/JC8;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2661515
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;-><init>()V

    return-void
.end method

.method private x()LX/JC8;
    .locals 6

    .prologue
    .line 2661516
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->t:LX/JC8;

    if-nez v0, :cond_0

    .line 2661517
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->n:LX/JC9;

    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2661518
    new-instance v5, LX/JC8;

    invoke-static {v1}, LX/J90;->a(LX/0QB;)LX/J90;

    move-result-object v2

    check-cast v2, LX/J90;

    invoke-static {v1}, LX/J9L;->a(LX/0QB;)LX/J9L;

    move-result-object v3

    check-cast v3, LX/J9L;

    invoke-static {v1}, LX/JCx;->a(LX/0QB;)LX/JCx;

    move-result-object v4

    check-cast v4, LX/JCx;

    invoke-direct {v5, v0, v2, v3, v4}, LX/JC8;-><init>(Ljava/lang/String;LX/J90;LX/J9L;LX/JCx;)V

    .line 2661519
    move-object v0, v5

    .line 2661520
    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->t:LX/JC8;

    .line 2661521
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->t:LX/JC8;

    return-object v0
.end method

.method private y()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;
    .locals 11

    .prologue
    .line 2661522
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->s()LX/03V;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2661523
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->c:Landroid/os/Bundle;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2661524
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->c:Landroid/os/Bundle;

    const-string v1, "collection"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JBL;

    .line 2661525
    new-instance v1, LX/JBs;

    invoke-direct {v1}, LX/JBs;-><init>()V

    .line 2661526
    if-eqz v0, :cond_0

    .line 2661527
    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->a(LX/JBL;)Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    move-result-object v2

    .line 2661528
    new-instance v3, LX/JBt;

    invoke-direct {v3}, LX/JBt;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2661529
    iput-object v2, v3, LX/JBt;->b:LX/0Px;

    .line 2661530
    move-object v2, v3

    .line 2661531
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v10, 0x0

    .line 2661532
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2661533
    iget-object v5, v2, LX/JBt;->b:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 2661534
    iget-object v7, v2, LX/JBt;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-static {v4, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2661535
    const/4 v9, 0x3

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 2661536
    iget v9, v2, LX/JBt;->a:I

    invoke-virtual {v4, v10, v9, v10}, LX/186;->a(III)V

    .line 2661537
    invoke-virtual {v4, v8, v5}, LX/186;->b(II)V

    .line 2661538
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v7}, LX/186;->b(II)V

    .line 2661539
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2661540
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2661541
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2661542
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2661543
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2661544
    new-instance v5, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    invoke-direct {v5, v4}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;-><init>(LX/15i;)V

    .line 2661545
    move-object v2, v5

    .line 2661546
    iput-object v2, v1, LX/JBs;->b:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel$CollectionsModel;

    .line 2661547
    :cond_0
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->c:Landroid/os/Bundle;

    const-string v3, "view_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2661548
    if-eqz v2, :cond_4

    .line 2661549
    iput-object v2, v1, LX/JBs;->e:Ljava/lang/String;

    .line 2661550
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->c:Landroid/os/Bundle;

    const-string v2, "collections_icon"

    invoke-static {v0, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Fb;

    .line 2661551
    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2661552
    iput-object v0, v1, LX/JBs;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2661553
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->o:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2661554
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->o:Ljava/lang/String;

    .line 2661555
    iput-object v0, v1, LX/JBs;->d:Ljava/lang/String;

    .line 2661556
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->p:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2661557
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->p:Ljava/lang/String;

    .line 2661558
    iput-object v0, v1, LX/JBs;->l:Ljava/lang/String;

    .line 2661559
    :cond_3
    invoke-virtual {v1}, LX/JBs;->a()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;

    move-result-object v0

    .line 2661560
    return-object v0

    .line 2661561
    :cond_4
    if-eqz v0, :cond_1

    .line 2661562
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->s()LX/03V;

    move-result-object v0

    const-string v2, "collections_section_prelim"

    const-string v3, "Provided COLLECTION but no VIEW_NAME"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2661591
    const-string v0, "collections_section"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2661563
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v2, p0

    check-cast v2, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;

    invoke-static {v8}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v3

    check-cast v3, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    new-instance v5, LX/JC4;

    invoke-static {v8}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {v5, v4}, LX/JC4;-><init>(LX/0Zb;)V

    move-object v4, v5

    check-cast v4, LX/JC4;

    const-class v5, LX/JC7;

    invoke-interface {v8, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/JC7;

    invoke-static {v8}, LX/J9y;->a(LX/0QB;)LX/J9y;

    move-result-object v6

    check-cast v6, LX/J9y;

    const-class v7, LX/JC3;

    invoke-interface {v8, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/JC3;

    const-class v0, LX/JC9;

    invoke-interface {v8, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/JC9;

    iput-object v3, v2, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->i:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object v4, v2, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->j:LX/JC4;

    iput-object v5, v2, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->k:LX/JC7;

    iput-object v6, v2, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->l:LX/J9y;

    iput-object v7, v2, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->m:LX/JC3;

    iput-object v8, v2, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->n:LX/JC9;

    .line 2661564
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2661565
    const-string v1, "section_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->o:Ljava/lang/String;

    .line 2661566
    const-string v1, "section_tracking"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->p:Ljava/lang/String;

    .line 2661567
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2661568
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2661569
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->a(Landroid/os/Bundle;)V

    .line 2661570
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->m:LX/JC3;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->j:LX/JC4;

    .line 2661571
    new-instance v5, LX/JC2;

    invoke-static {v0}, LX/J94;->a(LX/0QB;)LX/J94;

    move-result-object v10

    check-cast v10, LX/J94;

    invoke-static {v0}, LX/JDA;->a(LX/0QB;)LX/JDA;

    move-result-object v11

    check-cast v11, LX/JDA;

    invoke-static {v0}, LX/JCx;->a(LX/0QB;)LX/JCx;

    move-result-object v12

    check-cast v12, LX/JCx;

    invoke-static {v0}, LX/JDL;->a(LX/0QB;)LX/JDL;

    move-result-object v13

    check-cast v13, LX/JDL;

    move-object v6, v1

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    invoke-direct/range {v5 .. v13}, LX/JC2;-><init>(Landroid/content/Context;LX/9lP;Landroid/view/LayoutInflater;LX/J8p;LX/J94;LX/JDA;LX/JCx;LX/JDL;)V

    .line 2661572
    move-object v0, v5

    .line 2661573
    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->s:LX/JC2;

    .line 2661574
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->k:LX/JC7;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    .line 2661575
    new-instance v5, LX/JC6;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {v0}, LX/J8u;->a(LX/0QB;)LX/J8u;

    move-result-object v4

    check-cast v4, LX/J8u;

    invoke-direct {v5, v1, v2, v3, v4}, LX/JC6;-><init>(Landroid/content/Context;LX/9lP;LX/17W;LX/J8u;)V

    .line 2661576
    move-object v0, v5

    .line 2661577
    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->q:LX/JC6;

    .line 2661578
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->r:LX/1B1;

    .line 2661579
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->q:LX/JC6;

    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->r:LX/1B1;

    .line 2661580
    new-instance v2, LX/JC5;

    invoke-direct {v2, v0}, LX/JC5;-><init>(LX/JC6;)V

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b2;)Z

    .line 2661581
    iget-object v2, v0, LX/JC6;->b:LX/J8u;

    iget-object v3, v0, LX/JC6;->c:Landroid/content/Context;

    iget-object p0, v0, LX/JC6;->d:LX/9lP;

    invoke-virtual {v2, v3, p0, v1}, LX/J8u;->a(Landroid/content/Context;LX/9lP;LX/1B1;)V

    .line 2661582
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2661583
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 2661584
    const-string v1, "profile_id"

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    .line 2661585
    iget-object p0, v2, LX/9lP;->a:Ljava/lang/String;

    move-object v2, p0

    .line 2661586
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2661587
    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2661588
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->f:LX/9lP;

    .line 2661589
    iget-object v1, v0, LX/9lP;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2661590
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->o:Ljava/lang/String;

    invoke-static {v0, v1}, LX/JCc;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()LX/B0U;
    .locals 4

    .prologue
    .line 2661510
    new-instance v0, LX/B0c;

    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->x()LX/JC8;

    move-result-object v1

    const-wide/32 v2, 0x15180

    invoke-direct {v0, v1, v2, v3}, LX/B0c;-><init>(LX/B0V;J)V

    return-object v0
.end method

.method public final k()LX/B0N;
    .locals 4

    .prologue
    .line 2661511
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->y()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;

    move-result-object v0

    .line 2661512
    if-nez v0, :cond_0

    .line 2661513
    const/4 v0, 0x0

    .line 2661514
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->x()LX/JC8;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, LX/B0d;->a(J)LX/B0d;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/JC8;->a(LX/B0d;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;)LX/B0N;

    move-result-object v0

    goto :goto_0
.end method

.method public final l()LX/3tK;
    .locals 1

    .prologue
    .line 2661509
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->s:LX/JC2;

    return-object v0
.end method

.method public final synthetic n()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2661508
    invoke-direct {p0}, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->y()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineCollectionsSectionViewQueryModel;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/J8x;
    .locals 1

    .prologue
    .line 2661507
    sget-object v0, LX/J8x;->SECTION:LX/J8x;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x45af7675

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2661503
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 2661504
    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->b:Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 2661505
    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->e:LX/0g8;

    iget-object v3, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->s:LX/JC2;

    invoke-interface {v2, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2661506
    const/16 v2, 0x2b

    const v3, 0x2814e03e

    invoke-static {v5, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5af3df8d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2661498
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->s:LX/JC2;

    if-eqz v1, :cond_0

    .line 2661499
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->s:LX/JC2;

    invoke-virtual {v1, v2}, LX/3tK;->a(Landroid/database/Cursor;)V

    .line 2661500
    iput-object v2, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->s:LX/JC2;

    .line 2661501
    :cond_0
    invoke-super {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onDestroy()V

    .line 2661502
    const/16 v1, 0x2b

    const v2, -0x31c10d6f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x924ce3c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2661495
    invoke-super {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onPause()V

    .line 2661496
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->r:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->l:LX/J9y;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2661497
    const/16 v1, 0x2b

    const v2, 0x7bd7296

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x128cec96

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2661492
    invoke-super {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->onResume()V

    .line 2661493
    iget-object v1, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->r:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->l:LX/J9y;

    invoke-virtual {v1, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2661494
    const/16 v1, 0x2b

    const v2, -0x69dff8bd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final u()LX/J8p;
    .locals 1

    .prologue
    .line 2661491
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->j:LX/JC4;

    return-object v0
.end method

.method public final w()V
    .locals 2

    .prologue
    .line 2661485
    invoke-super {p0}, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->w()V

    .line 2661486
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    if-eqz v0, :cond_0

    .line 2661487
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->d:LX/62k;

    invoke-interface {v0}, LX/62k;->f()V

    .line 2661488
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/aboutpage/MultiCollectionFragment;->g:LX/J8y;

    move-object v0, v0

    .line 2661489
    invoke-virtual {p0}, Lcom/facebook/timeline/aboutpage/sections/CollectionsSectionFragment;->o()LX/J8x;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/J8y;->b(LX/J8x;)V

    .line 2661490
    return-void
.end method
