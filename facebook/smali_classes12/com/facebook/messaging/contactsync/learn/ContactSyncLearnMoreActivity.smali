.class public Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/63V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/67X;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/contactsync/learn/ForContactsLearnMore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/48V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public v:Lcom/facebook/webview/FacebookWebView;

.field private w:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2598974
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;LX/63V;LX/0Or;LX/0Or;LX/48V;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;",
            "LX/63V;",
            "LX/0Or",
            "<",
            "LX/67X;",
            ">;",
            "LX/0Or",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "LX/48V;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2598973
    iput-object p1, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->p:LX/63V;

    iput-object p2, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->q:LX/0Or;

    iput-object p3, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->r:LX/0Or;

    iput-object p4, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->s:LX/48V;

    iput-object p5, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->t:LX/03V;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;

    invoke-static {v5}, LX/63V;->a(LX/0QB;)LX/63V;

    move-result-object v1

    check-cast v1, LX/63V;

    const/16 v2, 0x1677

    invoke-static {v5, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x24

    invoke-static {v5, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v5}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object v4

    check-cast v4, LX/48V;

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static/range {v0 .. v5}, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->a(Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;LX/63V;LX/0Or;LX/0Or;LX/48V;LX/03V;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2598967
    invoke-static {p0, p0}, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2598968
    iget-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->p:LX/63V;

    invoke-virtual {v0}, LX/63V;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->w:Z

    .line 2598969
    iget-boolean v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->w:Z

    if-eqz v0, :cond_0

    .line 2598970
    iget-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/67X;

    .line 2598971
    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2598972
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2598942
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2598943
    iget-boolean v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->w:Z

    if-nez v0, :cond_0

    .line 2598944
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2598945
    :cond_0
    const v0, 0x7f03036e

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->setContentView(I)V

    .line 2598946
    const v0, 0x7f0d0b2b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->u:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 2598947
    iget-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->u:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 2598948
    iget-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->u:Lcom/facebook/widget/listview/EmptyListViewItem;

    const v1, 0x7f080024

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setMessage(I)V

    .line 2598949
    const v0, 0x7f0d0b2a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/webview/FacebookWebView;

    iput-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->v:Lcom/facebook/webview/FacebookWebView;

    .line 2598950
    iget-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->v:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v0, v2}, Lcom/facebook/webview/FacebookWebView;->setFocusableInTouchMode(Z)V

    .line 2598951
    iget-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->v:Lcom/facebook/webview/FacebookWebView;

    new-instance v1, LX/Iel;

    invoke-direct {v1, p0}, LX/Iel;-><init>(Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/webview/FacebookWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2598952
    iget-object v1, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->s:LX/48V;

    iget-object v2, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->v:Lcom/facebook/webview/FacebookWebView;

    iget-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2598953
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 2598962
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 2598963
    packed-switch v0, :pswitch_data_0

    .line 2598964
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 2598965
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->finish()V

    .line 2598966
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2598958
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2598959
    iget-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->v:Lcom/facebook/webview/FacebookWebView;

    if-eqz v0, :cond_0

    .line 2598960
    iget-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->v:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v0, p1}, Lcom/facebook/webview/FacebookWebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 2598961
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2598954
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2598955
    iget-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->v:Lcom/facebook/webview/FacebookWebView;

    if-eqz v0, :cond_0

    .line 2598956
    iget-object v0, p0, Lcom/facebook/messaging/contactsync/learn/ContactSyncLearnMoreActivity;->v:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v0, p1}, Lcom/facebook/webview/FacebookWebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 2598957
    :cond_0
    return-void
.end method
