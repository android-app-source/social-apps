.class public Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2591672
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2591673
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2591635
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2591636
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2591659
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2591660
    const v0, 0x7f030bac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2591661
    const v0, 0x7f0d1cfa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->a:Landroid/widget/ImageView;

    .line 2591662
    const v0, 0x7f0d1cfb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2591663
    const v0, 0x7f0d1cfc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2591664
    const v0, 0x7f0d1cfd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2591665
    sget-object v0, LX/03r;->NativeSignUpRow:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 2591666
    const/16 v0, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 2591667
    if-lez v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2591668
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2591669
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 2591670
    return-void

    :cond_0
    move v0, v1

    .line 2591671
    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2591653
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2591654
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2591655
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2591656
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 2591657
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->a:Landroid/widget/ImageView;

    const v1, 0x3f07ae14    # 0.53f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 2591658
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2591647
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2591648
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2591649
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2591650
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0265

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 2591651
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->a:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 2591652
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/high16 v1, -0x10000

    .line 2591644
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2591645
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 2591646
    return-void
.end method

.method public setViewParams(LX/Iax;)V
    .locals 2

    .prologue
    .line 2591637
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/Iax;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2591638
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->d:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/Iax;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2591639
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/Iax;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2591640
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2591641
    invoke-direct {p0}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->c()V

    .line 2591642
    :goto_0
    return-void

    .line 2591643
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->b()V

    goto :goto_0
.end method
