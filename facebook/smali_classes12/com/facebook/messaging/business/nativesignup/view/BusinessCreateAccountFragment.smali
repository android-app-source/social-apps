.class public final Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public A:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

.field public B:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

.field private C:Lcom/facebook/widget/text/BetterTextView;

.field private D:Landroid/widget/LinearLayout;

.field private E:Landroid/widget/ProgressBar;

.field public F:Landroid/widget/CheckBox;

.field public G:Lcom/facebook/widget/text/BetterButton;

.field public H:Lcom/facebook/fbservice/ops/BlueServiceFragment;

.field public I:Landroid/widget/ImageView;

.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IZv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/IoA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/IZK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/IZw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final j:LX/0T0;

.field public k:LX/Io9;

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/payments/p2p/model/PaymentCard;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

.field private r:Z

.field private s:Z

.field public t:Z

.field private u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private w:Lcom/facebook/widget/text/BetterTextView;

.field private x:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

.field private y:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

.field public z:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2591083
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2591084
    new-instance v0, LX/IaT;

    invoke-direct {v0, p0}, LX/IaT;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->j:LX/0T0;

    .line 2591085
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2591086
    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->l:LX/0Px;

    .line 2591087
    return-void
.end method

.method public static a(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 2591196
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2591197
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->h:LX/03V;

    const-string v1, "BusinessCreateAccountFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "null error msg in CreateAccount response for subcode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591198
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->g:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2591199
    :goto_0
    return-void

    .line 2591200
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 2591201
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->g:LX/IZK;

    invoke-virtual {v0, p2}, LX/IZK;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2591202
    :sswitch_0
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083a48

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/IaN;

    invoke-direct {v2, p0}, LX/IaN;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/IaM;

    invoke-direct {v2, p0}, LX/IaM;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2591203
    goto :goto_0

    .line 2591204
    :sswitch_1
    invoke-static {p0, p2}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->d(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x149629 -> :sswitch_0
        0x14962a -> :sswitch_0
        0x14962b -> :sswitch_0
        0x14962f -> :sswitch_0
        0x149638 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 2591156
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->G:Lcom/facebook/widget/text/BetterButton;

    new-instance v1, LX/IaK;

    invoke-direct {v1, p0, p1}, LX/IaK;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591157
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->w:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->ne_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2591158
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->n()LX/0Px;

    move-result-object v0

    const-string v1, "public_profile"

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2591159
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->x:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-static {}, LX/Iax;->newBuilder()LX/Iay;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2591160
    iput-object v2, v0, LX/Iay;->b:Ljava/lang/String;

    .line 2591161
    move-object v2, v0

    .line 2591162
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 2591163
    iput-object v0, v2, LX/Iay;->c:Ljava/lang/CharSequence;

    .line 2591164
    move-object v0, v2

    .line 2591165
    invoke-virtual {v0}, LX/Iay;->d()LX/Iax;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setViewParams(LX/Iax;)V

    .line 2591166
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->n()LX/0Px;

    move-result-object v0

    const-string v1, "user_friends"

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2591167
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->y:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-static {}, LX/Iax;->newBuilder()LX/Iay;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 2591168
    iput-object v2, v0, LX/Iay;->b:Ljava/lang/String;

    .line 2591169
    move-object v2, v0

    .line 2591170
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->nd_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 2591171
    iput-object v0, v2, LX/Iay;->c:Ljava/lang/CharSequence;

    .line 2591172
    move-object v0, v2

    .line 2591173
    invoke-virtual {v0}, LX/Iay;->d()LX/Iax;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setViewParams(LX/Iax;)V

    .line 2591174
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->n()LX/0Px;

    move-result-object v0

    const-string v1, "user_payment_tokens"

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2591175
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->B:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setVisibility(I)V

    .line 2591176
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->n()LX/0Px;

    move-result-object v0

    const-string v1, "email"

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2591177
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    :goto_2
    invoke-direct {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->b(Ljava/lang/String;)V

    .line 2591178
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->n()LX/0Px;

    move-result-object v0

    const-string v1, "user_mobile_phone"

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2591179
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->A:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setVisibility(I)V

    .line 2591180
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->C:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->d:LX/8tu;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2591181
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->C:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->w()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->m()Ljava/lang/String;

    move-result-object v3

    const/16 v11, 0x21

    .line 2591182
    new-instance v5, LX/IaX;

    invoke-direct {v5, p0, v2}, LX/IaX;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Ljava/lang/String;)V

    .line 2591183
    new-instance v6, LX/IaY;

    invoke-direct {v6, p0, v3}, LX/IaY;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Ljava/lang/String;)V

    .line 2591184
    new-instance v7, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-direct {v7, v8}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    const v8, 0x7f083a2f

    invoke-virtual {v7, v8}, LX/47x;->a(I)LX/47x;

    move-result-object v7

    const-string v8, "%1$s"

    invoke-virtual {v7, v8, v1}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;

    move-result-object v7

    const-string v8, "%2$s"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f083a30

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9, v5, v11}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v5

    const-string v7, "%3$s"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f083a31

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8, v6, v11}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v5

    .line 2591185
    invoke-virtual {v5}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v5

    move-object v1, v5

    .line 2591186
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2591187
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->y()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2591188
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->y()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2591189
    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-class v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2591190
    :cond_3
    invoke-direct {p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->e()V

    .line 2591191
    return-void

    .line 2591192
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->x:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2591193
    :cond_5
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->y:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2591194
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_2

    .line 2591195
    :cond_7
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->z:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setVisibility(I)V

    goto/16 :goto_3
.end method

.method public static a(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Z)V
    .locals 0

    .prologue
    .line 2591153
    iput-boolean p1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->r:Z

    .line 2591154
    invoke-direct {p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->k()V

    .line 2591155
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;)V
    .locals 3
    .param p0    # Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2591138
    iput-object p1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    .line 2591139
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    if-nez v0, :cond_0

    .line 2591140
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->A:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-static {}, LX/Iax;->newBuilder()LX/Iay;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f083a33

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2591141
    iput-object v2, v1, LX/Iay;->a:Ljava/lang/String;

    .line 2591142
    move-object v1, v1

    .line 2591143
    invoke-virtual {v1}, LX/Iay;->d()LX/Iax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setViewParams(LX/Iax;)V

    .line 2591144
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->A:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    new-instance v1, LX/Iac;

    invoke-direct {v1, p0}, LX/Iac;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591145
    return-void

    .line 2591146
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->A:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-static {}, LX/Iax;->newBuilder()LX/Iay;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f083a34

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2591147
    iput-object v2, v1, LX/Iay;->b:Ljava/lang/String;

    .line 2591148
    move-object v1, v1

    .line 2591149
    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2591150
    iput-object v2, v1, LX/Iay;->c:Ljava/lang/CharSequence;

    .line 2591151
    move-object v1, v1

    .line 2591152
    invoke-virtual {v1}, LX/Iay;->d()LX/Iax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setViewParams(LX/Iax;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/payments/p2p/model/PaymentCard;)V
    .locals 0
    .param p0    # Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2591135
    iput-object p1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2591136
    invoke-direct {p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->e()V

    .line 2591137
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2591133
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CMq;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/CMq;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 2591134
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2591113
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2591114
    const/4 v0, 0x0

    .line 2591115
    :goto_0
    move v0, v0

    .line 2591116
    if-eqz v0, :cond_0

    :goto_1
    iput-object p1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->p:Ljava/lang/String;

    .line 2591117
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->p:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2591118
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->z:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-static {}, LX/Iax;->newBuilder()LX/Iay;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083a35

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2591119
    iput-object v2, v1, LX/Iay;->a:Ljava/lang/String;

    .line 2591120
    move-object v1, v1

    .line 2591121
    invoke-virtual {v1}, LX/Iay;->d()LX/Iax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setViewParams(LX/Iax;)V

    .line 2591122
    :goto_2
    return-void

    .line 2591123
    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    .line 2591124
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->z:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-static {}, LX/Iax;->newBuilder()LX/Iay;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083a36

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2591125
    iput-object v2, v1, LX/Iay;->b:Ljava/lang/String;

    .line 2591126
    move-object v1, v1

    .line 2591127
    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->p:Ljava/lang/String;

    .line 2591128
    iput-object v2, v1, LX/Iay;->c:Ljava/lang/CharSequence;

    .line 2591129
    move-object v1, v1

    .line 2591130
    invoke-virtual {v1}, LX/Iay;->d()LX/Iax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setViewParams(LX/Iax;)V

    goto :goto_2

    .line 2591131
    :cond_2
    sget-object v0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 2591132
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Z)V
    .locals 0

    .prologue
    .line 2591110
    iput-boolean p1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->s:Z

    .line 2591111
    invoke-direct {p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->k()V

    .line 2591112
    return-void
.end method

.method public static d(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2591108
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f083a4d

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083a53

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/IaU;

    invoke-direct {v2, p0}, LX/IaU;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/IaS;

    invoke-direct {v2, p0}, LX/IaS;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2591109
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    .line 2591094
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->o:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->o:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->o:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2591095
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->B:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-static {}, LX/Iax;->newBuilder()LX/Iay;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083a37

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2591096
    iput-object v2, v1, LX/Iay;->a:Ljava/lang/String;

    .line 2591097
    move-object v1, v1

    .line 2591098
    invoke-virtual {v1}, LX/Iay;->d()LX/Iax;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setViewParams(LX/Iax;)V

    .line 2591099
    :goto_0
    return-void

    .line 2591100
    :cond_1
    new-instance v0, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    const-string v1, "%1$s %2$s\n%3$s"

    invoke-virtual {v0, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    const-string v1, "%1$s"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-static {v2, v3}, LX/Dud;->a(Landroid/content/Context;Lcom/facebook/payments/p2p/model/PaymentCard;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    const-string v1, "%2$s"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083a49

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0265

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v1

    const-string v2, "%3$s"

    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->o:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->j()LX/0Px;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v0}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    .line 2591101
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->B:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    invoke-static {}, LX/Iax;->newBuilder()LX/Iay;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f083a38

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2591102
    iput-object v3, v2, LX/Iay;->b:Ljava/lang/String;

    .line 2591103
    move-object v2, v2

    .line 2591104
    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    .line 2591105
    iput-object v0, v2, LX/Iay;->c:Ljava/lang/CharSequence;

    .line 2591106
    move-object v0, v2

    .line 2591107
    invoke-virtual {v0}, LX/Iay;->d()LX/Iax;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setViewParams(LX/Iax;)V

    goto :goto_0
.end method

.method private k()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 2591088
    iget-boolean v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->r:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->s:Z

    if-eqz v0, :cond_1

    .line 2591089
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->D:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2591090
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2591091
    :goto_0
    return-void

    .line 2591092
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->D:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2591093
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->E:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public static v(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V
    .locals 4

    .prologue
    .line 2591205
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2591206
    const-string v2, "BusinessRequestCodeFragment"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    move-object v1, v2

    .line 2591207
    const/4 v2, 0x1

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2591208
    return-void
.end method

.method public static w(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V
    .locals 4

    .prologue
    .line 2590988
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2590989
    const-string v2, "BusinessEmailInputFragment"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    move-object v1, v2

    .line 2590990
    const/4 v2, 0x2

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2590991
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2590992
    const-string v0, ""

    return-object v0
.end method

.method public final a(LX/IZ9;)V
    .locals 0

    .prologue
    .line 2590993
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 2590994
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2590995
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    const/16 v3, 0x12cc

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x27c5

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/IZv;->b(LX/0QB;)LX/IZv;

    move-result-object v5

    check-cast v5, LX/IZv;

    invoke-static {v0}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v6

    check-cast v6, LX/8tu;

    const-class v7, LX/IoA;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/IoA;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object v9

    check-cast v9, LX/IZK;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-static {v0}, LX/IZw;->b(LX/0QB;)LX/IZw;

    move-result-object v0

    check-cast v0, LX/IZw;

    iput-object v3, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a:LX/0Or;

    iput-object v4, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->b:LX/0Ot;

    iput-object v5, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->c:LX/IZv;

    iput-object v6, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->d:LX/8tu;

    iput-object v7, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->e:LX/IoA;

    iput-object v8, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->f:Lcom/facebook/content/SecureContextHelper;

    iput-object v9, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->g:LX/IZK;

    iput-object p1, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->h:LX/03V;

    iput-object v0, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->i:LX/IZw;

    .line 2590996
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->e:LX/IoA;

    const/4 v1, 0x1

    .line 2590997
    new-instance v2, LX/Io9;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v0}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->a(LX/0QB;)Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-static {v0}, LX/Ijy;->b(LX/0QB;)LX/Ijy;

    move-result-object v10

    check-cast v10, LX/Ijy;

    move-object v3, p0

    move v4, v1

    invoke-direct/range {v2 .. v10}, LX/Io9;-><init>(Landroid/support/v4/app/Fragment;ZLjava/util/concurrent/Executor;LX/03V;Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;Landroid/content/Context;Landroid/content/res/Resources;LX/Ijy;)V

    .line 2590998
    move-object v0, v2

    .line 2590999
    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->k:LX/Io9;

    .line 2591000
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "create_account"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->create(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)Lcom/facebook/fbservice/ops/BlueServiceFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->H:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    .line 2591001
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->H:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    new-instance v1, LX/IaJ;

    invoke-direct {v1, p0}, LX/IaJ;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    .line 2591002
    iput-object v1, v0, Lcom/facebook/fbservice/ops/BlueServiceFragment;->onCompletedListener:LX/4Ae;

    .line 2591003
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->H:Lcom/facebook/fbservice/ops/BlueServiceFragment;

    new-instance v1, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f083a44

    invoke-direct {v1, v2, v3}, LX/4At;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbservice/ops/BlueServiceFragment;->setOperationProgressIndicator(LX/4At;)V

    .line 2591004
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 2591005
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "native_signup_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->q:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    .line 2591006
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 2591007
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2591008
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->k:LX/Io9;

    .line 2591009
    packed-switch p1, :pswitch_data_0

    .line 2591010
    :goto_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    if-ne p2, v1, :cond_2

    .line 2591011
    const-string v0, "confirmed_phone_number"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    .line 2591012
    if-eqz v0, :cond_1

    .line 2591013
    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;)V

    .line 2591014
    :cond_0
    :goto_1
    return-void

    .line 2591015
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->g:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    goto :goto_1

    .line 2591016
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_0

    .line 2591017
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->i:LX/IZw;

    const-string v1, "success_update_email"

    invoke-virtual {v0, v1}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2591018
    const-string v0, "updated_email"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 2591019
    :pswitch_0
    iget-object v2, v0, LX/Io9;->i:LX/Ijy;

    invoke-virtual {v2, p1, p2, p3}, LX/Ijy;->a(IILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x511ea004

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591020
    const v1, 0x7f03020e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6b2a47f6

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1aa7eae9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2591021
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onPause()V

    .line 2591022
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/base/activity/FbFragmentActivity;

    if-eqz v0, :cond_0

    .line 2591023
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/activity/FbFragmentActivity;

    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->j:LX/0T0;

    invoke-virtual {v0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->b(LX/0T2;)V

    .line 2591024
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->c:LX/IZv;

    invoke-virtual {v0}, LX/IZv;->a()V

    .line 2591025
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->k:LX/Io9;

    .line 2591026
    iget-object v2, v0, LX/Io9;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v2, :cond_1

    .line 2591027
    iget-object v2, v0, LX/Io9;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 p0, 0x1

    invoke-interface {v2, p0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2591028
    const/4 v2, 0x0

    iput-object v2, v0, LX/Io9;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2591029
    :cond_1
    const/16 v0, 0x2b

    const v2, -0x2545f9d5

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x23604ebf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2591030
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onResume()V

    .line 2591031
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/base/activity/FbFragmentActivity;

    if-eqz v0, :cond_0

    .line 2591032
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/activity/FbFragmentActivity;

    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->j:LX/0T0;

    invoke-virtual {v0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2591033
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x66aaba77

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2591034
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2591035
    const-string v0, "native_signup_params"

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->q:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2591036
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->o:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    if-eqz v0, :cond_0

    .line 2591037
    const-string v0, "new_user_signup"

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->o:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2591038
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    if-eqz v0, :cond_1

    .line 2591039
    const-string v0, "last_confirmed_number"

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2591040
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    if-eqz v0, :cond_2

    .line 2591041
    const-string v0, "last_selected_card"

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2591042
    :cond_2
    const-string v0, "last_updated_email"

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591043
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x74c34e72

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591044
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2591045
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onStart()V

    .line 2591046
    const/16 v1, 0x2b

    const v2, -0x7086e454

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2591047
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2591048
    const v0, 0x7f0d1cf9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2591049
    const v0, 0x7f0d1cf8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2591050
    const v0, 0x7f0d0817

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->w:Lcom/facebook/widget/text/BetterTextView;

    .line 2591051
    const v0, 0x7f0d0818

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->x:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    .line 2591052
    const v0, 0x7f0d0819

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->y:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    .line 2591053
    const v0, 0x7f0d081b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->z:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    .line 2591054
    const v0, 0x7f0d081c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->A:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    .line 2591055
    const v0, 0x7f0d081a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->B:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    .line 2591056
    const v0, 0x7f0d081e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->C:Lcom/facebook/widget/text/BetterTextView;

    .line 2591057
    const v0, 0x7f0d0816

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->D:Landroid/widget/LinearLayout;

    .line 2591058
    const v0, 0x7f0d0820

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->E:Landroid/widget/ProgressBar;

    .line 2591059
    const v0, 0x7f0d081d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->F:Landroid/widget/CheckBox;

    .line 2591060
    const v0, 0x7f0d081f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->G:Lcom/facebook/widget/text/BetterButton;

    .line 2591061
    const v0, 0x7f0d0815

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->I:Landroid/widget/ImageView;

    .line 2591062
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->k:LX/Io9;

    new-instance v1, LX/Iab;

    invoke-direct {v1, p0}, LX/Iab;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    .line 2591063
    iput-object v1, v0, LX/Io9;->b:LX/Iab;

    .line 2591064
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Z)V

    .line 2591065
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->k:LX/Io9;

    invoke-virtual {v0}, LX/Io9;->a()V

    .line 2591066
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->B:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    new-instance v1, LX/Iaa;

    invoke-direct {v1, p0}, LX/Iaa;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591067
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-class v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2591068
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->F:Landroid/widget/CheckBox;

    new-instance v1, LX/Iae;

    invoke-direct {v1, p0}, LX/Iae;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591069
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->z:Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;

    new-instance v1, LX/Iad;

    invoke-direct {v1, p0}, LX/Iad;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/NativeSignUpRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591070
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->I:Landroid/widget/ImageView;

    new-instance v1, LX/IaL;

    invoke-direct {v1, p0}, LX/IaL;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591071
    if-eqz p2, :cond_0

    .line 2591072
    const-string v0, "new_user_signup"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->o:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    .line 2591073
    const-string v0, "last_confirmed_number"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    .line 2591074
    const-string v0, "last_selected_card"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2591075
    const-string v0, "last_updated_email"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->p:Ljava/lang/String;

    .line 2591076
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->m:Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/payments/p2p/model/PaymentCard;)V

    .line 2591077
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->n:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;)V

    .line 2591078
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->o:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    if-nez v0, :cond_1

    .line 2591079
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->b$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Z)V

    .line 2591080
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->c:LX/IZv;

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->q:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    new-instance v2, LX/IaZ;

    invoke-direct {v2, p0}, LX/IaZ;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V

    invoke-virtual {v0, v1, v2}, LX/IZv;->a(Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;LX/IZu;)V

    .line 2591081
    :goto_0
    return-void

    .line 2591082
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->o:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->a(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V

    goto :goto_0
.end method
