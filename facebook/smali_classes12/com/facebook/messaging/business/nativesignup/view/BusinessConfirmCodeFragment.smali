.class public final Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""


# instance fields
.field public a:LX/Ia0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ia3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/IZK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/IZw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:Ljava/util/regex/Pattern;

.field public g:Landroid/widget/EditText;

.field public h:Lcom/facebook/widget/text/BetterButton;

.field public i:LX/4At;

.field public j:LX/4At;

.field public k:Landroid/widget/TextView;

.field public l:Landroid/widget/ImageView;

.field public m:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2590756
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2590757
    const-string v0, "\\d{6}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->f:Ljava/util/regex/Pattern;

    .line 2590758
    return-void
.end method

.method public static e$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;)V
    .locals 1

    .prologue
    .line 2590802
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->i:LX/4At;

    if-eqz v0, :cond_0

    .line 2590803
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->i:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2590804
    :cond_0
    return-void
.end method

.method public static m(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;)V
    .locals 1

    .prologue
    .line 2590799
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->j:LX/4At;

    if-eqz v0, :cond_0

    .line 2590800
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->j:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2590801
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2590798
    const-string v0, ""

    return-object v0
.end method

.method public final a(LX/IZ9;)V
    .locals 0

    .prologue
    .line 2590797
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2590794
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2590795
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    new-instance p0, LX/Ia0;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    const-class p1, Landroid/content/Context;

    invoke-interface {v0, p1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5, p1}, LX/Ia0;-><init>(LX/03V;LX/0tX;LX/1Ck;Landroid/content/Context;)V

    move-object v3, p0

    check-cast v3, LX/Ia0;

    invoke-static {v0}, LX/Ia3;->b(LX/0QB;)LX/Ia3;

    move-result-object v4

    check-cast v4, LX/Ia3;

    invoke-static {v0}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v5

    check-cast v5, LX/8tu;

    invoke-static {v0}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object p1

    check-cast p1, LX/IZK;

    invoke-static {v0}, LX/IZw;->b(LX/0QB;)LX/IZw;

    move-result-object v0

    check-cast v0, LX/IZw;

    iput-object v3, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->a:LX/Ia0;

    iput-object v4, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->b:LX/Ia3;

    iput-object v5, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->c:LX/8tu;

    iput-object p1, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->d:LX/IZK;

    iput-object v0, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->e:LX/IZw;

    .line 2590796
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 2590792
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "phone_number_to_confirm"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->m:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    .line 2590793
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3b3f9019

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2590791
    const v1, 0x7f03020d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x43b7c2cd

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x519ef4c6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2590783
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onPause()V

    .line 2590784
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->b:LX/Ia3;

    invoke-virtual {v1}, LX/Ia3;->a()V

    .line 2590785
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->a:LX/Ia0;

    .line 2590786
    iget-object v2, v1, LX/Ia0;->c:LX/1Ck;

    const-string v4, "ConfirmPhoneCodeKey"

    invoke-virtual {v2, v4}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2590787
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2590788
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2590789
    invoke-static {v1, v2}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2590790
    const/16 v1, 0x2b

    const v2, 0x57d12139

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3faeb7e3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2590780
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onResume()V

    .line 2590781
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->g:Landroid/widget/EditText;

    new-instance v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment$1;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment$1;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2590782
    const/16 v1, 0x2b

    const v2, 0xffd66b0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x534137e8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2590777
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2590778
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onStart()V

    .line 2590779
    const/16 v1, 0x2b

    const v2, -0x2f9b2475

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2590759
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2590760
    const v0, 0x7f0d0813

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->g:Landroid/widget/EditText;

    .line 2590761
    const v0, 0x7f0d0814

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->h:Lcom/facebook/widget/text/BetterButton;

    .line 2590762
    const v0, 0x7f0d0812

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->k:Landroid/widget/TextView;

    .line 2590763
    const v0, 0x7f0d0815

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->l:Landroid/widget/ImageView;

    .line 2590764
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->g:Landroid/widget/EditText;

    new-instance p1, LX/IaC;

    invoke-direct {p1, p0}, LX/IaC;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2590765
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->h:Lcom/facebook/widget/text/BetterButton;

    new-instance p1, LX/IaE;

    invoke-direct {p1, p0}, LX/IaE;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2590766
    const/16 p2, 0x21

    .line 2590767
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0265

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2590768
    new-instance v1, LX/IaG;

    invoke-direct {v1, p0, v0}, LX/IaG;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;I)V

    .line 2590769
    new-instance v0, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2590770
    const v2, 0x7f083a45

    invoke-virtual {v0, v2}, LX/47x;->a(I)LX/47x;

    .line 2590771
    const-string v2, "[[phone_number]]"

    iget-object v3, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->m:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    invoke-virtual {v3}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;->a()Ljava/lang/String;

    move-result-object v3

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3, p2, p1}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/Object;)LX/47x;

    .line 2590772
    const-string v2, "[[resend_code_link]]"

    const v3, 0x7f083a46

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1, p2}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2590773
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->k:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->c:LX/8tu;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2590774
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2590775
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->l:Landroid/widget/ImageView;

    new-instance v1, LX/IaH;

    invoke-direct {v1, p0}, LX/IaH;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2590776
    return-void
.end method
