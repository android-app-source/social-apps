.class public final Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""


# instance fields
.field public a:LX/7Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ia3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3Lz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/IZK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/IZw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/7Tj;

.field public h:Landroid/widget/EditText;

.field public i:Landroid/widget/TextView;

.field public j:Lcom/facebook/widget/text/BetterButton;

.field private k:LX/A8g;

.field public l:LX/4At;

.field public m:Landroid/widget/ImageView;

.field public n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2591413
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2591414
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->n:Ljava/lang/String;

    .line 2591415
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;)V
    .locals 3

    .prologue
    .line 2591408
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2591409
    const-string v1, "confirmed_phone_number"

    invoke-static {v0, v1, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2591410
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2591411
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2591412
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2591399
    iput-object p1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->n:Ljava/lang/String;

    .line 2591400
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2591401
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->k:LX/A8g;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2591402
    new-instance v0, LX/A8g;

    .line 2591403
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->n:Ljava/lang/String;

    move-object v1, v1

    .line 2591404
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/A8g;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->k:LX/A8g;

    .line 2591405
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->k:LX/A8g;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2591406
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->h:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2591407
    return-void
.end method

.method public static k(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2591398
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->c:LX/3Lz;

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static l(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2591397
    invoke-static {p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->n(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/3Lz;->normalizeDigitsOnly(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static n(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2591396
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static p(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)V
    .locals 1

    .prologue
    .line 2591393
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->l:LX/4At;

    if-eqz v0, :cond_0

    .line 2591394
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->l:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2591395
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2591392
    const-string v0, ""

    return-object v0
.end method

.method public final a(LX/IZ9;)V
    .locals 0

    .prologue
    .line 2591391
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2591358
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2591359
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    const-class v3, LX/7Tk;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/7Tk;

    invoke-static {v0}, LX/Ia3;->b(LX/0QB;)LX/Ia3;

    move-result-object v4

    check-cast v4, LX/Ia3;

    invoke-static {v0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v5

    check-cast v5, LX/3Lz;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object p1

    check-cast p1, LX/IZK;

    invoke-static {v0}, LX/IZw;->b(LX/0QB;)LX/IZw;

    move-result-object v0

    check-cast v0, LX/IZw;

    iput-object v3, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->a:LX/7Tk;

    iput-object v4, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->b:LX/Ia3;

    iput-object v5, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->c:LX/3Lz;

    iput-object v6, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object p1, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->e:LX/IZK;

    iput-object v0, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->f:LX/IZw;

    .line 2591360
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 2591390
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2591385
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2591386
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2591387
    const-string v0, "confirmed_phone_number"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    .line 2591388
    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;)V

    .line 2591389
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4a8be768    # 4584372.0f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591384
    const v1, 0x7f030216

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x57c7b157

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5b811f25

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591378
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onPause()V

    .line 2591379
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->b:LX/Ia3;

    invoke-virtual {v1}, LX/Ia3;->a()V

    .line 2591380
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2591381
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2591382
    invoke-static {v1, v2}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2591383
    const/16 v1, 0x2b

    const v2, -0x66b2147e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5140323d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591375
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onResume()V

    .line 2591376
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->h:Landroid/widget/EditText;

    new-instance v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment$1;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment$1;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2591377
    const/16 v1, 0x2b

    const v2, -0x3b9ebcf9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5ff4c180

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591372
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2591373
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onStart()V

    .line 2591374
    const/16 v1, 0x2b

    const v2, 0x23ec7ce6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2591361
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2591362
    const v0, 0x7f0d0832

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->h:Landroid/widget/EditText;

    .line 2591363
    const v0, 0x7f0d0831

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->i:Landroid/widget/TextView;

    .line 2591364
    const v0, 0x7f0d0833

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->j:Lcom/facebook/widget/text/BetterButton;

    .line 2591365
    const v0, 0x7f0d0815

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->m:Landroid/widget/ImageView;

    .line 2591366
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->h:Landroid/widget/EditText;

    new-instance p1, LX/Iam;

    invoke-direct {p1, p0}, LX/Iam;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2591367
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->n:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;Ljava/lang/String;)V

    .line 2591368
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->i:Landroid/widget/TextView;

    new-instance p1, LX/Ial;

    invoke-direct {p1, p0}, LX/Ial;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591369
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->j:Lcom/facebook/widget/text/BetterButton;

    new-instance p1, LX/Iao;

    invoke-direct {p1, p0}, LX/Iao;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591370
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->m:Landroid/widget/ImageView;

    new-instance p1, LX/Iap;

    invoke-direct {p1, p0}, LX/Iap;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591371
    return-void
.end method
