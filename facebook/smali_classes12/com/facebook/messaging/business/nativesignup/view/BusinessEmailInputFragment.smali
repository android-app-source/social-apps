.class public final Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""


# instance fields
.field public a:LX/IZw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IZK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/widget/EditText;

.field public d:Lcom/facebook/widget/text/BetterButton;

.field public e:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2591258
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2591259
    return-void
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2591260
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2591261
    const/4 v0, 0x0

    .line 2591262
    :goto_0
    return v0

    .line 2591263
    :cond_0
    sget-object v0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 2591264
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2591266
    const-string v0, ""

    return-object v0
.end method

.method public final a(LX/IZ9;)V
    .locals 0

    .prologue
    .line 2591265
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2591254
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2591255
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;

    invoke-static {v0}, LX/IZw;->b(LX/0QB;)LX/IZw;

    move-result-object p1

    check-cast p1, LX/IZw;

    invoke-static {v0}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object v0

    check-cast v0, LX/IZK;

    iput-object p1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->a:LX/IZw;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->b:LX/IZK;

    .line 2591256
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 2591257
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x572f26a7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591253
    const v1, 0x7f03020f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x68a17694

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x62031538

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591248
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onPause()V

    .line 2591249
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2591250
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2591251
    invoke-static {v1, v2}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2591252
    const/16 v1, 0x2b

    const v2, -0x7c78e40e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1efb2cea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591245
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onResume()V

    .line 2591246
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->c:Landroid/widget/EditText;

    new-instance v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment$1;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment$1;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2591247
    const/16 v1, 0x2b

    const v2, -0x68d7cb6a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xd3a323d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591242
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2591243
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onStart()V

    .line 2591244
    const/16 v1, 0x2b

    const v2, 0x33730886

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2591234
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2591235
    const v0, 0x7f0d0821

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->c:Landroid/widget/EditText;

    .line 2591236
    const v0, 0x7f0d0822

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->d:Lcom/facebook/widget/text/BetterButton;

    .line 2591237
    const v0, 0x7f0d0815

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->e:Landroid/widget/ImageView;

    .line 2591238
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->c:Landroid/widget/EditText;

    new-instance p1, LX/Iag;

    invoke-direct {p1, p0}, LX/Iag;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2591239
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->d:Lcom/facebook/widget/text/BetterButton;

    new-instance p1, LX/Iah;

    invoke-direct {p1, p0}, LX/Iah;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591240
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->e:Landroid/widget/ImageView;

    new-instance p1, LX/Iai;

    invoke-direct {p1, p0}, LX/Iai;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591241
    return-void
.end method
