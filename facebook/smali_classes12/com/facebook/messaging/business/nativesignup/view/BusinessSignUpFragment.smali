.class public final Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/IZv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IZK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/IZp;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/IZw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0s6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final h:LX/0T0;

.field public i:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

.field public j:Z

.field private k:Z

.field private l:Z

.field private m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private n:Lcom/facebook/widget/text/BetterTextView;

.field private o:Lcom/facebook/widget/text/BetterTextView;

.field private p:Lcom/facebook/widget/text/BetterTextView;

.field private q:Lcom/facebook/widget/text/BetterButton;

.field public r:Landroid/widget/ImageView;

.field private s:Landroid/widget/ProgressBar;

.field private t:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field private u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public v:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2591627
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2591628
    new-instance v0, LX/Iar;

    invoke-direct {v0, p0}, LX/Iar;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->h:LX/0T0;

    .line 2591629
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2591624
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2591625
    const-string v1, "native_signup_params"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2591626
    const-string v1, "BusinessSignUpFragment"

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V
    .locals 4

    .prologue
    .line 2591619
    iget-boolean v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->k:Z

    if-nez v0, :cond_0

    .line 2591620
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->k:Z

    .line 2591621
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->e:LX/IZw;

    .line 2591622
    iget-object v1, v0, LX/IZw;->a:LX/0if;

    sget-object v2, LX/0ig;->n:LX/0ih;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string p0, "IS_OPTIONAL_PAYMENT_ENABLED:"

    invoke-direct {v3, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->c()Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2591623
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2591614
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->e:LX/IZw;

    const-string v1, "BusinessSignUpFragment"

    .line 2591615
    invoke-virtual {v0, p1, v1}, LX/IZw;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591616
    iget-object v2, v0, LX/IZw;->a:LX/0if;

    sget-object v3, LX/0ig;->n:LX/0ih;

    invoke-virtual {v2, v3}, LX/0if;->c(LX/0ih;)V

    .line 2591617
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->j:Z

    .line 2591618
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 2591606
    if-eqz p1, :cond_0

    .line 2591607
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->t:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2591608
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2591609
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->s:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2591610
    :goto_0
    return-void

    .line 2591611
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->t:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2591612
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2591613
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->s:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2591574
    invoke-direct {p0, p1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a(Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V

    .line 2591575
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->z()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2591576
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->z()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    .line 2591577
    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v1, v0, v4}, LX/15i;->j(II)I

    move-result v4

    if-lt v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v0, v2

    .line 2591578
    if-eqz v0, :cond_4

    .line 2591579
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->z()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2591580
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->z()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2591581
    const/4 v4, 0x2

    invoke-virtual {v1, v0, v4}, LX/15i;->j(II)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v3, v2, v6}, LX/15i;->j(II)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 2591582
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2591583
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->n:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2591584
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v7}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2591585
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->z()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2591586
    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-class v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2591587
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->A()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2591588
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->A()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2591589
    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-class v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2591590
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->b$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2591591
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->q:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 2591592
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2591593
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->p:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/Iat;

    invoke-direct {v1, p0, p1}, LX/Iat;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591594
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->q:Lcom/facebook/widget/text/BetterButton;

    new-instance v1, LX/Iau;

    invoke-direct {v1, p0, p1}, LX/Iau;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591595
    iget-boolean v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->l:Z

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2591596
    iput-boolean v5, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->l:Z

    .line 2591597
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->c:LX/IZK;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/IZK;->a(Ljava/lang/String;)V

    .line 2591598
    :cond_3
    return-void

    .line 2591599
    :cond_4
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2591600
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b2530

    invoke-virtual {v1, v2, v0, v5}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 2591601
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2591602
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->n:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2591603
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2591604
    :cond_5
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->q:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 2591605
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static b$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2591573
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->g:LX/0s6;

    invoke-virtual {v0, p1}, LX/01H;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;)V
    .locals 2

    .prologue
    .line 2591570
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->e:LX/IZw;

    const-string v1, "click_log_in_button"

    invoke-virtual {v0, v1}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2591571
    invoke-direct {p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->k()V

    .line 2591572
    return-void
.end method

.method public static e(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;)V
    .locals 4

    .prologue
    .line 2591563
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->e:LX/IZw;

    const-string v1, "click_sign_up_button"

    invoke-virtual {v0, v1}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2591564
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->i:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    .line 2591565
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2591566
    const-string v3, "native_signup_params"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2591567
    const-string v3, "BusinessCreateAccountFragment"

    invoke-static {v0, v3, v2}, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    move-object v0, v2

    .line 2591568
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->b:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2591569
    return-void
.end method

.method private k()V
    .locals 7

    .prologue
    .line 2591552
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->v:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    if-nez v0, :cond_0

    .line 2591553
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->f:LX/03V;

    const-string v1, "BusinessSignUpFragment"

    const-string v2, "mNewUserSignup object is null in openOauthFragment()"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591554
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->c:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2591555
    :goto_0
    return-void

    .line 2591556
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->v:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083a47

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->v:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-virtual {v6}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->u()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->v:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-virtual {v3}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;->v()Ljava/lang/String;

    move-result-object v3

    .line 2591557
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2591558
    const-string v5, "oauth_url"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591559
    const-string v5, "fragment_title"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591560
    const-string v5, "dismiss_url"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591561
    const-string v5, "PlatformAccountLinkFragment"

    invoke-static {v0, v5, v4}, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v4

    move-object v0, v4

    .line 2591562
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->b:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x2

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 2591630
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IZp;

    .line 2591631
    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->i:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    .line 2591632
    iget-object v3, v2, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2591633
    invoke-interface {v0}, LX/IZp;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->i:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    invoke-interface {v0, v2}, LX/IZp;->a(Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2591634
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2591452
    const-string v0, ""

    return-object v0
.end method

.method public final a(LX/IZ9;)V
    .locals 0

    .prologue
    .line 2591461
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2591453
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2591454
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    invoke-static {v0}, LX/IZv;->b(LX/0QB;)LX/IZv;

    move-result-object v4

    check-cast v4, LX/IZv;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object v6

    check-cast v6, LX/IZK;

    new-instance v7, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v8

    new-instance v9, LX/IZr;

    invoke-direct {v9, v0}, LX/IZr;-><init>(LX/0QB;)V

    invoke-direct {v7, v8, v9}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v7, v7

    invoke-static {v0}, LX/IZw;->b(LX/0QB;)LX/IZw;

    move-result-object v8

    check-cast v8, LX/IZw;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/0s6;->a(LX/0QB;)LX/0s6;

    move-result-object v0

    check-cast v0, LX/0s6;

    iput-object v4, v3, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a:LX/IZv;

    iput-object v5, v3, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v3, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->c:LX/IZK;

    iput-object v7, v3, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->d:Ljava/util/Set;

    iput-object v8, v3, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->e:LX/IZw;

    iput-object v9, v3, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->f:LX/03V;

    iput-object v0, v3, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->g:LX/0s6;

    .line 2591455
    if-eqz p1, :cond_0

    .line 2591456
    const-string v0, "new_user_signup"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->v:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    .line 2591457
    const-string v0, "is_funnel_logging_started"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->j:Z

    .line 2591458
    const-string v0, "is_new_sign_up_logged"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->k:Z

    .line 2591459
    const-string v0, "is_warning_message_showed"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->l:Z

    .line 2591460
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 2591462
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "native_signup_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->i:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    .line 2591463
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2591464
    packed-switch p1, :pswitch_data_0

    .line 2591465
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2591466
    :cond_0
    :goto_0
    return-void

    .line 2591467
    :pswitch_0
    if-ne p2, v1, :cond_0

    .line 2591468
    const-string v0, "next_step"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2591469
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2591470
    invoke-direct {p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->k()V

    goto :goto_0

    .line 2591471
    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2591472
    const-string v0, "complete_create_account"

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Ljava/lang/String;)V

    .line 2591473
    invoke-direct {p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->l()V

    .line 2591474
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 2591475
    :pswitch_1
    if-ne p2, v1, :cond_0

    .line 2591476
    const-string v0, "complete_oauth"

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Ljava/lang/String;)V

    .line 2591477
    invoke-direct {p0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->l()V

    .line 2591478
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6d371a7a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591479
    const v1, 0x7f030217

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0xb60f617

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x598696cc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2591480
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onPause()V

    .line 2591481
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/base/activity/FbFragmentActivity;

    if-eqz v0, :cond_0

    .line 2591482
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/activity/FbFragmentActivity;

    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->h:LX/0T0;

    invoke-virtual {v0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->b(LX/0T2;)V

    .line 2591483
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a:LX/IZv;

    invoke-virtual {v0}, LX/IZv;->a()V

    .line 2591484
    const/16 v0, 0x2b

    const v2, -0x3d8a011d

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6343c6f5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2591485
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onResume()V

    .line 2591486
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/base/activity/FbFragmentActivity;

    if-eqz v0, :cond_0

    .line 2591487
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/activity/FbFragmentActivity;

    iget-object v2, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->h:LX/0T0;

    invoke-virtual {v0, v2}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2591488
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x2c8b6db1

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2591489
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2591490
    const-string v0, "new_user_signup"

    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->v:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2591491
    const-string v0, "is_funnel_logging_started"

    iget-boolean v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2591492
    const-string v0, "is_new_sign_up_logged"

    iget-boolean v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2591493
    const-string v0, "is_warning_message_showed"

    iget-boolean v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2591494
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x693431e1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591495
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2591496
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onStart()V

    .line 2591497
    const/16 v1, 0x2b

    const v2, -0x1ff1d93c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2591498
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2591499
    const v0, 0x7f0d0837

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2591500
    const v0, 0x7f0d0838

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->n:Lcom/facebook/widget/text/BetterTextView;

    .line 2591501
    const v0, 0x7f0d0839

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 2591502
    const v0, 0x7f0d083a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 2591503
    const v0, 0x7f0d0815

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->r:Landroid/widget/ImageView;

    .line 2591504
    const v0, 0x7f0d083b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->q:Lcom/facebook/widget/text/BetterButton;

    .line 2591505
    const v0, 0x7f0d083c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->s:Landroid/widget/ProgressBar;

    .line 2591506
    const v0, 0x7f0d0834

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->t:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2591507
    const v0, 0x7f0d0836

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2591508
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->r:Landroid/widget/ImageView;

    new-instance p1, LX/Iav;

    invoke-direct {p1, p0}, LX/Iav;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2591509
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->v:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    if-nez v0, :cond_6

    .line 2591510
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Z)V

    .line 2591511
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a:LX/IZv;

    iget-object p1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->i:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    new-instance p2, LX/Ias;

    invoke-direct {p2, p0}, LX/Ias;-><init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;)V

    invoke-virtual {v0, p1, p2}, LX/IZv;->a(Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;LX/IZu;)V

    .line 2591512
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->i:Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    .line 2591513
    iget-boolean v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->j:Z

    if-nez v1, :cond_5

    .line 2591514
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->j:Z

    .line 2591515
    iget-object v1, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->e:LX/IZw;

    .line 2591516
    iget-object v2, v1, LX/IZw;->a:LX/0if;

    sget-object v3, LX/0ig;->n:LX/0ih;

    invoke-virtual {v2, v3}, LX/0if;->a(LX/0ih;)V

    .line 2591517
    iget-object v2, v1, LX/IZw;->a:LX/0if;

    sget-object v3, LX/0ig;->n:LX/0ih;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PROVIDER_ID:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2591518
    iget-object v5, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2591519
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2591520
    iget-object v2, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2591521
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2591522
    iget-object v2, v1, LX/IZw;->a:LX/0if;

    sget-object v3, LX/0ig;->n:LX/0ih;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TRIGGER_SOURCE:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2591523
    iget-object v5, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->c:Ljava/lang/String;

    move-object v5, v5

    .line 2591524
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2591525
    :cond_0
    iget-object v2, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2591526
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2591527
    iget-object v2, v1, LX/IZw;->a:LX/0if;

    sget-object v3, LX/0ig;->n:LX/0ih;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TRIGGER_SOURCE_TAG:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2591528
    iget-object v5, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2591529
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2591530
    :cond_1
    iget-object v2, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v2, v2

    .line 2591531
    if-eqz v2, :cond_2

    .line 2591532
    iget-object v2, v1, LX/IZw;->a:LX/0if;

    sget-object v3, LX/0ig;->n:LX/0ih;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "THREAD_ID:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2591533
    iget-object v5, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v5, v5

    .line 2591534
    invoke-virtual {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2591535
    iget-object v2, v1, LX/IZw;->a:LX/0if;

    sget-object v3, LX/0ig;->n:LX/0ih;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "THREAD_TYPE:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2591536
    iget-object v5, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v5, v5

    .line 2591537
    iget-object v5, v5, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v5}, LX/5e9;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2591538
    :cond_2
    iget-object v2, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->f:Ljava/lang/String;

    move-object v2, v2

    .line 2591539
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2591540
    iget-object v2, v1, LX/IZw;->a:LX/0if;

    sget-object v3, LX/0ig;->n:LX/0ih;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MESSAGE_ID:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2591541
    iget-object v5, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->f:Ljava/lang/String;

    move-object v5, v5

    .line 2591542
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2591543
    :cond_3
    iget-object v2, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->h:Ljava/lang/String;

    move-object v2, v2

    .line 2591544
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2591545
    iget-object v2, v1, LX/IZw;->a:LX/0if;

    sget-object v3, LX/0ig;->n:LX/0ih;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "TRIGGER_PROMO_DATA:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2591546
    iget-object v5, v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->h:Ljava/lang/String;

    move-object v5, v5

    .line 2591547
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2591548
    :cond_4
    iget-object v2, v1, LX/IZw;->a:LX/0if;

    sget-object v3, LX/0ig;->n:LX/0ih;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "INTRO_IMAGE_QE_GROUP:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v1, LX/IZw;->b:LX/0ad;

    sget-char v6, LX/Ib1;->a:C

    const-string v7, "default"

    invoke-interface {v5, v6, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2591549
    :cond_5
    return-void

    .line 2591550
    :cond_6
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->v:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->b$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V

    .line 2591551
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Z)V

    goto/16 :goto_0
.end method
