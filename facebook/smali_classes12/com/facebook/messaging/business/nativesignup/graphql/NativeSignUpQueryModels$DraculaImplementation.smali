.class public final Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2589610
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2589611
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2589654
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2589655
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 2589626
    if-nez p1, :cond_0

    move v0, v1

    .line 2589627
    :goto_0
    return v0

    .line 2589628
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2589629
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2589630
    :sswitch_0
    const-class v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    .line 2589631
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2589632
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2589633
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2589634
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2589635
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2589636
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2589637
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2589638
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2589639
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2589640
    :sswitch_2
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2589641
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2589642
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2589643
    invoke-virtual {p0, p1, v6, v1}, LX/15i;->a(III)I

    move-result v3

    .line 2589644
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2589645
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2589646
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2589647
    invoke-virtual {p3, v6, v3, v1}, LX/186;->a(III)V

    .line 2589648
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2589649
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2589650
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2589651
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2589652
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2589653
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6e42c02f -> :sswitch_0
        -0x20e8b28b -> :sswitch_1
        -0x12bdf779 -> :sswitch_2
        0x14a4837c -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2589625
    new-instance v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2589620
    if-eqz p0, :cond_0

    .line 2589621
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2589622
    if-eq v0, p0, :cond_0

    .line 2589623
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2589624
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2589615
    sparse-switch p2, :sswitch_data_0

    .line 2589616
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2589617
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    .line 2589618
    invoke-static {v0, p3}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2589619
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x6e42c02f -> :sswitch_0
        -0x20e8b28b -> :sswitch_1
        -0x12bdf779 -> :sswitch_1
        0x14a4837c -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2589614
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2589612
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2589613
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2589656
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2589657
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2589658
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2589659
    iput p2, p0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;->b:I

    .line 2589660
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2589584
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2589585
    new-instance v0, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2589586
    iget v0, p0, LX/1vt;->c:I

    .line 2589587
    move v0, v0

    .line 2589588
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2589589
    iget v0, p0, LX/1vt;->c:I

    .line 2589590
    move v0, v0

    .line 2589591
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2589592
    iget v0, p0, LX/1vt;->b:I

    .line 2589593
    move v0, v0

    .line 2589594
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2589595
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2589596
    move-object v0, v0

    .line 2589597
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2589598
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2589599
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2589600
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2589601
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2589602
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2589603
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2589604
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2589605
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2589606
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2589607
    iget v0, p0, LX/1vt;->c:I

    .line 2589608
    move v0, v0

    .line 2589609
    return v0
.end method
