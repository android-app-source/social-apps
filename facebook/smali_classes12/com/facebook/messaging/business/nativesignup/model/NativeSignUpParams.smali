.class public Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2590487
    new-instance v0, LX/IZx;

    invoke-direct {v0}, LX/IZx;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/IZy;)V
    .locals 1

    .prologue
    .line 2590466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2590467
    iget-object v0, p1, LX/IZy;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->a:Ljava/lang/String;

    .line 2590468
    iget-object v0, p1, LX/IZy;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->b:Ljava/lang/String;

    .line 2590469
    iget-object v0, p1, LX/IZy;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->c:Ljava/lang/String;

    .line 2590470
    iget-object v0, p1, LX/IZy;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->d:Ljava/lang/String;

    .line 2590471
    iget-object v0, p1, LX/IZy;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2590472
    iget-object v0, p1, LX/IZy;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->f:Ljava/lang/String;

    .line 2590473
    iget-object v0, p1, LX/IZy;->g:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->g:Landroid/os/Bundle;

    .line 2590474
    iget-object v0, p1, LX/IZy;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->h:Ljava/lang/String;

    .line 2590475
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2590476
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2590477
    const-class v0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 2590478
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->a:Ljava/lang/String;

    .line 2590479
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->b:Ljava/lang/String;

    .line 2590480
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->c:Ljava/lang/String;

    .line 2590481
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->d:Ljava/lang/String;

    .line 2590482
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2590483
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->f:Ljava/lang/String;

    .line 2590484
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->g:Landroid/os/Bundle;

    .line 2590485
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->h:Ljava/lang/String;

    .line 2590486
    return-void
.end method

.method public static newBuilder()LX/IZy;
    .locals 1

    .prologue
    .line 2590465
    new-instance v0, LX/IZy;

    invoke-direct {v0}, LX/IZy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2590464
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2590455
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2590456
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2590457
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2590458
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2590459
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2590460
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2590461
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->g:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 2590462
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2590463
    return-void
.end method
