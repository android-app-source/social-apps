.class public Lcom/facebook/messaging/business/nativesignup/protocol/NativeSignUpServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# instance fields
.field public final a:LX/18V;

.field public final b:LX/Ia5;

.field public final c:LX/Ia7;


# direct methods
.method public constructor <init>(LX/18V;LX/Ia5;LX/Ia7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2590545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2590546
    iput-object p1, p0, Lcom/facebook/messaging/business/nativesignup/protocol/NativeSignUpServiceHandler;->a:LX/18V;

    .line 2590547
    iput-object p2, p0, Lcom/facebook/messaging/business/nativesignup/protocol/NativeSignUpServiceHandler;->b:LX/Ia5;

    .line 2590548
    iput-object p3, p0, Lcom/facebook/messaging/business/nativesignup/protocol/NativeSignUpServiceHandler;->c:LX/Ia7;

    .line 2590549
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2590550
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2590551
    const-string v1, "create_account"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2590552
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/protocol/NativeSignUpServiceHandler;->a:LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v2

    .line 2590553
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2590554
    const-string v1, "proxy_login_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;

    .line 2590555
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 2590556
    const-string v3, "third_party_registration_params"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ThirdPartyRegistrationMethod$Params;

    .line 2590557
    iget-object v3, p0, Lcom/facebook/messaging/business/nativesignup/protocol/NativeSignUpServiceHandler;->b:LX/Ia5;

    invoke-static {v3, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v3, "proxy_login"

    .line 2590558
    iput-object v3, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2590559
    move-object v0, v0

    .line 2590560
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2590561
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/protocol/NativeSignUpServiceHandler;->c:LX/Ia7;

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "third_party_registration"

    .line 2590562
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2590563
    move-object v0, v0

    .line 2590564
    const-string v1, "proxy_login"

    .line 2590565
    iput-object v1, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2590566
    move-object v0, v0

    .line 2590567
    const-string v1, "?access_token={result=proxy_login:$.access_token}"

    .line 2590568
    iput-object v1, v0, LX/2Vk;->g:Ljava/lang/String;

    .line 2590569
    move-object v0, v0

    .line 2590570
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v2, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2590571
    const-string v0, "nativeSignUpCreateAccount"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-interface {v2, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2590572
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2590573
    move-object v0, v0

    .line 2590574
    return-object v0

    .line 2590575
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
