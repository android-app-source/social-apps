.class public final Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2590587
    new-instance v0, LX/Ia4;

    invoke-direct {v0}, LX/Ia4;-><init>()V

    sput-object v0, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2590588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2590589
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;->a:Ljava/lang/String;

    .line 2590590
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;->b:Ljava/lang/String;

    .line 2590591
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2590583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2590584
    iput-object p1, p0, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;->a:Ljava/lang/String;

    .line 2590585
    iput-object p2, p0, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;->b:Ljava/lang/String;

    .line 2590586
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2590582
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2590579
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2590580
    iget-object v0, p0, Lcom/facebook/messaging/business/nativesignup/protocol/methods/ProxyLoginMethod$Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2590581
    return-void
.end method
