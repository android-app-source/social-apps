.class public final Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""


# instance fields
.field public a:LX/CJg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2588461
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2588462
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    .line 2588463
    const v0, 0x7f0d0378

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 2588464
    iget-object v1, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->a:LX/CJg;

    .line 2588465
    iput-object v0, v1, LX/CJg;->e:Landroid/widget/ProgressBar;

    .line 2588466
    iget-object v0, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->a:LX/CJg;

    iget-object v1, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->d:Ljava/lang/String;

    new-instance v2, LX/IZ6;

    invoke-direct {v2, p0}, LX/IZ6;-><init>(Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;)V

    const/4 v3, 0x0

    .line 2588467
    iget-object v4, v0, LX/CJg;->d:Ljava/util/List;

    new-instance v5, LX/CJd;

    invoke-direct {v5, v1, v2, v3}, LX/CJd;-><init>(Ljava/lang/String;LX/IZ6;Z)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2588468
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2588469
    iget-object v0, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f083a55

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(LX/IZ9;)V
    .locals 0

    .prologue
    .line 2588473
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2588470
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2588471
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;

    new-instance v1, LX/CJg;

    const-class p1, Landroid/content/Context;

    invoke-interface {v2, p1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v1, p1, v0}, LX/CJg;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    move-object v2, v1

    check-cast v2, LX/CJg;

    iput-object v2, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->a:LX/CJg;

    .line 2588472
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 2588457
    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "oauth_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->b:Ljava/lang/String;

    move-object v0, p1

    .line 2588458
    check-cast v0, Landroid/os/Bundle;

    const-string v1, "fragment_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->c:Ljava/lang/String;

    .line 2588459
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "dismiss_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->d:Ljava/lang/String;

    .line 2588460
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6ebb5f55

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2588433
    const v1, 0x7f03001c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1f8e1ae0

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x32fe3d22    # -1.3606448E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2588449
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onDestroyView()V

    .line 2588450
    iget-object v1, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->a:LX/CJg;

    .line 2588451
    :goto_0
    iget-object v2, v1, LX/CJg;->c:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->empty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2588452
    iget-object v2, v1, LX/CJg;->c:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/webkit/WebView;

    .line 2588453
    invoke-static {v2}, LX/CJg;->c(Landroid/webkit/WebView;)V

    goto :goto_0

    .line 2588454
    :cond_0
    const/4 v2, 0x0

    iput-object v2, v1, LX/CJg;->e:Landroid/widget/ProgressBar;

    .line 2588455
    iget-object v2, v1, LX/CJg;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2588456
    const/16 v1, 0x2b

    const v2, -0x44367699

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x723c4cb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2588444
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onPause()V

    .line 2588445
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2588446
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 2588447
    invoke-static {v1, v2}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2588448
    const/16 v1, 0x2b

    const v2, 0x630a52e9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x69b71ff8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2588441
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2588442
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onStart()V

    .line 2588443
    const/16 v1, 0x2b

    const v2, 0x55b54690

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.webkit.WebView.loadUrl"
        }
    .end annotation

    .prologue
    .line 2588434
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2588435
    const v0, 0x7f0d0377

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->e:Landroid/widget/FrameLayout;

    .line 2588436
    invoke-direct {p0}, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->b()V

    .line 2588437
    iget-object v0, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->a:LX/CJg;

    invoke-virtual {v0}, LX/CJg;->a()Landroid/webkit/WebView;

    move-result-object v0

    .line 2588438
    iget-object v1, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->e:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2588439
    iget-object v1, p0, Lcom/facebook/messaging/business/accountlink/view/PlatformAccountLinkFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 2588440
    return-void
.end method
