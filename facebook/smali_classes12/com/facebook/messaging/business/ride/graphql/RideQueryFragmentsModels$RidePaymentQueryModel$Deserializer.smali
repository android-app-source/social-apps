.class public final Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2592891
    const-class v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2592892
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2592893
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2592894
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2592895
    const/4 v2, 0x0

    .line 2592896
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 2592897
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592898
    :goto_0
    move v1, v2

    .line 2592899
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2592900
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2592901
    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;-><init>()V

    .line 2592902
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2592903
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2592904
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2592905
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2592906
    :cond_0
    return-object v1

    .line 2592907
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592908
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 2592909
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2592910
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2592911
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 2592912
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2592913
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 2592914
    :cond_4
    const-string v5, "messenger_commerce"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2592915
    const/4 v4, 0x0

    .line 2592916
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_b

    .line 2592917
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592918
    :goto_2
    move v1, v4

    .line 2592919
    goto :goto_1

    .line 2592920
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2592921
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2592922
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2592923
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 2592924
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592925
    :cond_8
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_a

    .line 2592926
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2592927
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2592928
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_8

    if-eqz v5, :cond_8

    .line 2592929
    const-string v6, "ride_providers"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2592930
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2592931
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_9

    .line 2592932
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_9

    .line 2592933
    const/4 v6, 0x0

    .line 2592934
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_f

    .line 2592935
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592936
    :goto_5
    move v5, v6

    .line 2592937
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2592938
    :cond_9
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2592939
    goto :goto_3

    .line 2592940
    :cond_a
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2592941
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2592942
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_b
    move v1, v4

    goto :goto_3

    .line 2592943
    :cond_c
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592944
    :cond_d
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_e

    .line 2592945
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2592946
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2592947
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_d

    if-eqz v7, :cond_d

    .line 2592948
    const-string p0, "payment_row_display_text"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 2592949
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_6

    .line 2592950
    :cond_e
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2592951
    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 2592952
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_5

    :cond_f
    move v5, v6

    goto :goto_6
.end method
