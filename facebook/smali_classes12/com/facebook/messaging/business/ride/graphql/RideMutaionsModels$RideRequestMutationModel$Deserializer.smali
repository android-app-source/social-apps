.class public final Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2592010
    const-class v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2592011
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2591939
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2591940
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2591941
    const/4 v2, 0x0

    .line 2591942
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 2591943
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2591944
    :goto_0
    move v1, v2

    .line 2591945
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2591946
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2591947
    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideRequestMutationModel;-><init>()V

    .line 2591948
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2591949
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2591950
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2591951
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2591952
    :cond_0
    return-object v1

    .line 2591953
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2591954
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 2591955
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2591956
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2591957
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_2

    if-eqz v7, :cond_2

    .line 2591958
    const-string v8, "error_message"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2591959
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2591960
    :cond_3
    const-string v8, "preset_price"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2591961
    const/4 v7, 0x0

    .line 2591962
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_c

    .line 2591963
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2591964
    :goto_2
    move v5, v7

    .line 2591965
    goto :goto_1

    .line 2591966
    :cond_4
    const-string v8, "result"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2591967
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 2591968
    :cond_5
    const-string v8, "ride_request"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2591969
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2591970
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v9, :cond_13

    .line 2591971
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2591972
    :goto_3
    move v3, v7

    .line 2591973
    goto :goto_1

    .line 2591974
    :cond_6
    const-string v8, "surge_estimate"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2591975
    invoke-static {p1, v0}, LX/IbC;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 2591976
    :cond_7
    const/4 v7, 0x5

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2591977
    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2591978
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2591979
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2591980
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2591981
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2591982
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto/16 :goto_1

    .line 2591983
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2591984
    :cond_a
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_b

    .line 2591985
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2591986
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2591987
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_a

    if-eqz v8, :cond_a

    .line 2591988
    const-string v9, "cost_token"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 2591989
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_4

    .line 2591990
    :cond_b
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2591991
    invoke-virtual {v0, v7, v5}, LX/186;->b(II)V

    .line 2591992
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto/16 :goto_2

    :cond_c
    move v5, v7

    goto :goto_4

    .line 2591993
    :cond_d
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_11

    .line 2591994
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 2591995
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2591996
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_d

    if-eqz v12, :cond_d

    .line 2591997
    const-string p0, "eta_in_minutes"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_e

    .line 2591998
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v3

    move v11, v3

    move v3, v8

    goto :goto_5

    .line 2591999
    :cond_e
    const-string p0, "request_id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_f

    .line 2592000
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_5

    .line 2592001
    :cond_f
    const-string p0, "status"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_10

    .line 2592002
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_5

    .line 2592003
    :cond_10
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 2592004
    :cond_11
    const/4 v12, 0x3

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 2592005
    if-eqz v3, :cond_12

    .line 2592006
    invoke-virtual {v0, v7, v11, v7}, LX/186;->a(III)V

    .line 2592007
    :cond_12
    invoke-virtual {v0, v8, v10}, LX/186;->b(II)V

    .line 2592008
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 2592009
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto/16 :goto_3

    :cond_13
    move v3, v7

    move v9, v7

    move v10, v7

    move v11, v7

    goto :goto_5
.end method
