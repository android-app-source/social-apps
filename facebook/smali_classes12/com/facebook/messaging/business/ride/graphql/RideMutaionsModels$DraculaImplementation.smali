.class public final Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2591867
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2591868
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2591795
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2591796
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    .line 2591835
    if-nez p1, :cond_0

    .line 2591836
    const/4 v0, 0x0

    .line 2591837
    :goto_0
    return v0

    .line 2591838
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2591839
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2591840
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2591841
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2591842
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2591843
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2591844
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2591845
    :sswitch_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2591846
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2591847
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2591848
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2591849
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2591850
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2591851
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p3, v3, v0, v4}, LX/186;->a(III)V

    .line 2591852
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2591853
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 2591854
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2591855
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2591856
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2591857
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2591858
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2591859
    const/4 v2, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2591860
    const/4 v4, 0x3

    const-wide/16 v6, 0x0

    invoke-virtual {p0, p1, v4, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2591861
    const/4 v4, 0x4

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2591862
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 2591863
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2591864
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2591865
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2591866
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x743698d7 -> :sswitch_0
        0x320bc761 -> :sswitch_2
        0x79ada3d7 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2591834
    new-instance v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2591831
    sparse-switch p0, :sswitch_data_0

    .line 2591832
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2591833
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x743698d7 -> :sswitch_0
        0x320bc761 -> :sswitch_0
        0x79ada3d7 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2591830
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2591828
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;->b(I)V

    .line 2591829
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2591823
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2591824
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2591825
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;->a:LX/15i;

    .line 2591826
    iput p2, p0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;->b:I

    .line 2591827
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2591822
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2591821
    new-instance v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2591818
    iget v0, p0, LX/1vt;->c:I

    .line 2591819
    move v0, v0

    .line 2591820
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2591815
    iget v0, p0, LX/1vt;->c:I

    .line 2591816
    move v0, v0

    .line 2591817
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2591812
    iget v0, p0, LX/1vt;->b:I

    .line 2591813
    move v0, v0

    .line 2591814
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2591809
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2591810
    move-object v0, v0

    .line 2591811
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2591800
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2591801
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2591802
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2591803
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2591804
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2591805
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2591806
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2591807
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2591808
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2591797
    iget v0, p0, LX/1vt;->c:I

    .line 2591798
    move v0, v0

    .line 2591799
    return v0
.end method
