.class public final Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2592228
    const-class v0, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel;

    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2592229
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2592227
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2592230
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2592231
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x1

    .line 2592232
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2592233
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2592234
    if-eqz v2, :cond_0

    .line 2592235
    const-string p0, "error_message"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592236
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592237
    :cond_0
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 2592238
    if-eqz v2, :cond_1

    .line 2592239
    const-string v2, "result"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592240
    invoke-virtual {v1, v0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592241
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2592242
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2592226
    check-cast p1, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel$Serializer;->a(Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
