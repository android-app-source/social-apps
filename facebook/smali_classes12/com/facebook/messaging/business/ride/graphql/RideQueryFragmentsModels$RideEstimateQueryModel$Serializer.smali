.class public final Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2592806
    const-class v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2592807
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2592808
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2592809
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2592810
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 2592811
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2592812
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2592813
    if-eqz v2, :cond_0

    .line 2592814
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592815
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2592816
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2592817
    if-eqz v2, :cond_7

    .line 2592818
    const-string v3, "messenger_commerce"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592819
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2592820
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2592821
    if-eqz v3, :cond_6

    .line 2592822
    const-string v4, "ride_providers"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592823
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2592824
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_5

    .line 2592825
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 2592826
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2592827
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2592828
    if-eqz v0, :cond_4

    .line 2592829
    const-string v2, "ride_estimate_information"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592830
    const/4 v2, 0x0

    .line 2592831
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2592832
    invoke-virtual {v1, v0, v2, v2}, LX/15i;->a(III)I

    move-result v2

    .line 2592833
    if-eqz v2, :cond_1

    .line 2592834
    const-string p0, "eta_in_seconds"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592835
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 2592836
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2592837
    if-eqz v2, :cond_2

    .line 2592838
    const-string p0, "formatted_high_fare"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592839
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592840
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2592841
    if-eqz v2, :cond_3

    .line 2592842
    const-string p0, "formatted_low_fare"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592843
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592844
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2592845
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2592846
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2592847
    :cond_5
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2592848
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2592849
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2592850
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2592851
    check-cast p1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel$Serializer;->a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
