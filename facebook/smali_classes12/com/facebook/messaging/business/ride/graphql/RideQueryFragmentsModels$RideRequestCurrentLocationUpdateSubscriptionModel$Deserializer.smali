.class public final Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2593538
    const-class v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;

    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2593539
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2593584
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2593540
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2593541
    const/4 v2, 0x0

    .line 2593542
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2593543
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2593544
    :goto_0
    move v1, v2

    .line 2593545
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2593546
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2593547
    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;-><init>()V

    .line 2593548
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2593549
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2593550
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2593551
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2593552
    :cond_0
    return-object v1

    .line 2593553
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2593554
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2593555
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2593556
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2593557
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2593558
    const-string v4, "ride_request"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2593559
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2593560
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_b

    .line 2593561
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2593562
    :goto_2
    move v1, v3

    .line 2593563
    goto :goto_1

    .line 2593564
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2593565
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2593566
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2593567
    :cond_5
    const-string p0, "eta_in_minutes"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2593568
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v4

    .line 2593569
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_9

    .line 2593570
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2593571
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2593572
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v8, :cond_6

    .line 2593573
    const-string p0, "current_location"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2593574
    invoke-static {p1, v0}, LX/Ibd;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_3

    .line 2593575
    :cond_7
    const-string p0, "ride_status"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2593576
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_3

    .line 2593577
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2593578
    :cond_9
    const/4 v8, 0x3

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2593579
    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2593580
    if-eqz v1, :cond_a

    .line 2593581
    invoke-virtual {v0, v4, v6, v3}, LX/186;->a(III)V

    .line 2593582
    :cond_a
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2593583
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_b
    move v1, v3

    move v5, v3

    move v6, v3

    move v7, v3

    goto :goto_3
.end method
