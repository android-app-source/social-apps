.class public final Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2592802
    const-class v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2592803
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2592801
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2592721
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2592722
    const/4 v2, 0x0

    .line 2592723
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 2592724
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592725
    :goto_0
    move v1, v2

    .line 2592726
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2592727
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2592728
    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideEstimateQueryModel;-><init>()V

    .line 2592729
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2592730
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2592731
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2592732
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2592733
    :cond_0
    return-object v1

    .line 2592734
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592735
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 2592736
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2592737
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2592738
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 2592739
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2592740
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 2592741
    :cond_4
    const-string v5, "messenger_commerce"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2592742
    const/4 v4, 0x0

    .line 2592743
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_b

    .line 2592744
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592745
    :goto_2
    move v1, v4

    .line 2592746
    goto :goto_1

    .line 2592747
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2592748
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2592749
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2592750
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 2592751
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592752
    :cond_8
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_a

    .line 2592753
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2592754
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2592755
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_8

    if-eqz v5, :cond_8

    .line 2592756
    const-string v6, "ride_providers"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2592757
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2592758
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_9

    .line 2592759
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_9

    .line 2592760
    const/4 v6, 0x0

    .line 2592761
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_f

    .line 2592762
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592763
    :goto_5
    move v5, v6

    .line 2592764
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2592765
    :cond_9
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2592766
    goto :goto_3

    .line 2592767
    :cond_a
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2592768
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2592769
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_b
    move v1, v4

    goto :goto_3

    .line 2592770
    :cond_c
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592771
    :cond_d
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_e

    .line 2592772
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2592773
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2592774
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_d

    if-eqz v7, :cond_d

    .line 2592775
    const-string v8, "ride_estimate_information"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 2592776
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2592777
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v9, :cond_16

    .line 2592778
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2592779
    :goto_7
    move v5, v7

    .line 2592780
    goto :goto_6

    .line 2592781
    :cond_e
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2592782
    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 2592783
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_5

    :cond_f
    move v5, v6

    goto :goto_6

    .line 2592784
    :cond_10
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_14

    .line 2592785
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 2592786
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2592787
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_10

    if-eqz v12, :cond_10

    .line 2592788
    const-string p0, "eta_in_seconds"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_11

    .line 2592789
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v5

    move v11, v5

    move v5, v8

    goto :goto_8

    .line 2592790
    :cond_11
    const-string p0, "formatted_high_fare"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_12

    .line 2592791
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_8

    .line 2592792
    :cond_12
    const-string p0, "formatted_low_fare"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_13

    .line 2592793
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_8

    .line 2592794
    :cond_13
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_8

    .line 2592795
    :cond_14
    const/4 v12, 0x3

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 2592796
    if-eqz v5, :cond_15

    .line 2592797
    invoke-virtual {v0, v7, v11, v7}, LX/186;->a(III)V

    .line 2592798
    :cond_15
    invoke-virtual {v0, v8, v10}, LX/186;->b(II)V

    .line 2592799
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v9}, LX/186;->b(II)V

    .line 2592800
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto :goto_7

    :cond_16
    move v5, v7

    move v9, v7

    move v10, v7

    move v11, v7

    goto :goto_8
.end method
