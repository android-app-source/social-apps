.class public final Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2593585
    const-class v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;

    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2593586
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2593587
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;LX/0nX;LX/0my;)V
    .locals 13

    .prologue
    .line 2593588
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2593589
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2593590
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2593591
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2593592
    if-eqz v2, :cond_5

    .line 2593593
    const-string v3, "ride_request"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593594
    const/4 v6, 0x0

    .line 2593595
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2593596
    invoke-virtual {v1, v2, v6}, LX/15i;->g(II)I

    move-result v4

    .line 2593597
    if-eqz v4, :cond_2

    .line 2593598
    const-string v5, "current_location"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593599
    const-wide/16 v11, 0x0

    .line 2593600
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2593601
    const/4 v7, 0x0

    invoke-virtual {v1, v4, v7, v11, v12}, LX/15i;->a(IID)D

    move-result-wide v7

    .line 2593602
    cmpl-double v9, v7, v11

    if-eqz v9, :cond_0

    .line 2593603
    const-string v9, "latitude"

    invoke-virtual {p1, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593604
    invoke-virtual {p1, v7, v8}, LX/0nX;->a(D)V

    .line 2593605
    :cond_0
    const/4 v7, 0x1

    invoke-virtual {v1, v4, v7, v11, v12}, LX/15i;->a(IID)D

    move-result-wide v7

    .line 2593606
    cmpl-double v9, v7, v11

    if-eqz v9, :cond_1

    .line 2593607
    const-string v9, "longitude"

    invoke-virtual {p1, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593608
    invoke-virtual {p1, v7, v8}, LX/0nX;->a(D)V

    .line 2593609
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2593610
    :cond_2
    const/4 v4, 0x1

    invoke-virtual {v1, v2, v4, v6}, LX/15i;->a(III)I

    move-result v4

    .line 2593611
    if-eqz v4, :cond_3

    .line 2593612
    const-string v5, "eta_in_minutes"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593613
    invoke-virtual {p1, v4}, LX/0nX;->b(I)V

    .line 2593614
    :cond_3
    const/4 v4, 0x2

    invoke-virtual {v1, v2, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2593615
    if-eqz v4, :cond_4

    .line 2593616
    const-string v5, "ride_status"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593617
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2593618
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2593619
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2593620
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2593621
    check-cast p1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel$Serializer;->a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideRequestCurrentLocationUpdateSubscriptionModel;LX/0nX;LX/0my;)V

    return-void
.end method
