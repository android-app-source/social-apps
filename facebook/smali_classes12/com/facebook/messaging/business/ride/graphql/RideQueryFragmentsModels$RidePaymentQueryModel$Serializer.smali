.class public final Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2592955
    const-class v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2592956
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2592986
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2592958
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2592959
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 2592960
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2592961
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2592962
    if-eqz v2, :cond_0

    .line 2592963
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592964
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2592965
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2592966
    if-eqz v2, :cond_4

    .line 2592967
    const-string v3, "messenger_commerce"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592968
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2592969
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2592970
    if-eqz v3, :cond_3

    .line 2592971
    const-string p0, "ride_providers"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592972
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2592973
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_2

    .line 2592974
    invoke-virtual {v1, v3, p0}, LX/15i;->q(II)I

    move-result v0

    .line 2592975
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2592976
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2592977
    if-eqz v2, :cond_1

    .line 2592978
    const-string p2, "payment_row_display_text"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2592979
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2592980
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2592981
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 2592982
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2592983
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2592984
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2592985
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2592957
    check-cast p1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel$Serializer;->a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePaymentQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
