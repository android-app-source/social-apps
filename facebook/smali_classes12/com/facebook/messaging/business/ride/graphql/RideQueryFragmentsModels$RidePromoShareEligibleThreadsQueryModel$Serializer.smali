.class public final Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareEligibleThreadsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareEligibleThreadsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2593060
    const-class v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareEligibleThreadsQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareEligibleThreadsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareEligibleThreadsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2593061
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2593124
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareEligibleThreadsQueryModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 2593063
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2593064
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 2593065
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2593066
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2593067
    if-eqz v2, :cond_0

    .line 2593068
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593069
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2593070
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2593071
    if-eqz v2, :cond_1

    .line 2593072
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593073
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2593074
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2593075
    if-eqz v2, :cond_b

    .line 2593076
    const-string v3, "messenger_commerce"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593077
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2593078
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2593079
    if-eqz v3, :cond_a

    .line 2593080
    const-string v4, "ride_providers"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593081
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2593082
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_9

    .line 2593083
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 2593084
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2593085
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 2593086
    if-eqz v6, :cond_8

    .line 2593087
    const-string p0, "ride_invite"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593088
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2593089
    const/4 p0, 0x0

    invoke-virtual {v1, v6, p0}, LX/15i;->g(II)I

    move-result p0

    .line 2593090
    if-eqz p0, :cond_7

    .line 2593091
    const-string v0, "eligible_threads"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593092
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2593093
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2593094
    if-eqz v0, :cond_6

    .line 2593095
    const-string v2, "nodes"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593096
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2593097
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v0}, LX/15i;->c(I)I

    move-result v5

    if-ge v2, v5, :cond_5

    .line 2593098
    invoke-virtual {v1, v0, v2}, LX/15i;->q(II)I

    move-result v5

    .line 2593099
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2593100
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 2593101
    if-eqz v6, :cond_4

    .line 2593102
    const-string p0, "thread_key"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593103
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2593104
    const/4 p0, 0x0

    invoke-virtual {v1, v6, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2593105
    if-eqz p0, :cond_2

    .line 2593106
    const-string v5, "other_user_id"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593107
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2593108
    :cond_2
    const/4 p0, 0x1

    invoke-virtual {v1, v6, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2593109
    if-eqz p0, :cond_3

    .line 2593110
    const-string v5, "thread_fbid"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2593111
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2593112
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2593113
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2593114
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2593115
    :cond_5
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2593116
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2593117
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2593118
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2593119
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 2593120
    :cond_9
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2593121
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2593122
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2593123
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2593062
    check-cast p1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareEligibleThreadsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareEligibleThreadsQueryModel$Serializer;->a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RidePromoShareEligibleThreadsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
