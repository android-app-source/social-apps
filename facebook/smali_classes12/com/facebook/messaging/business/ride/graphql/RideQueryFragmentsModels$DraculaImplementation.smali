.class public final Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2592713
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2592714
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2592711
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2592712
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v6, 0x3

    const/4 v9, 0x2

    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 2592543
    if-nez p1, :cond_0

    .line 2592544
    :goto_0
    return v1

    .line 2592545
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2592546
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2592547
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592548
    const v2, -0xa51e0a3

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2592549
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592550
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592551
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2592552
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592553
    const v2, -0x69c00475

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2592554
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592555
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592556
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2592557
    :sswitch_2
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2592558
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2592559
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2592560
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2592561
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2592562
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2592563
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2592564
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2592565
    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    .line 2592566
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2592567
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592568
    const v2, 0x1a9dacae

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2592569
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592570
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592571
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 2592572
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2592573
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2592574
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592575
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592576
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592577
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592578
    const v2, -0x6066c8fc

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2592579
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592580
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592581
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592582
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592583
    const v2, 0xbeee3ba

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2592584
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592585
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592586
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592587
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592588
    const v2, -0x7ec06fdc

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2592589
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592590
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592591
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592592
    :sswitch_8
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592593
    const v2, 0xdbd3105

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2592594
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592595
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592596
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592597
    :sswitch_9
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592598
    const v2, 0x6b576d72

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2592599
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592600
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592601
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592602
    :sswitch_a
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2592603
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2592604
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2592605
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2592606
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2592607
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592608
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2592609
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592610
    :sswitch_b
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592611
    const v2, -0x62cf0c15

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2592612
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592613
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592614
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592615
    :sswitch_c
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2592616
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2592617
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v2

    .line 2592618
    const v3, -0x3041c0a7

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2592619
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2592620
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592621
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2592622
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592623
    :sswitch_d
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2592624
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2592625
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2592626
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2592627
    invoke-virtual {p0, p1, v9}, LX/15i;->p(II)I

    move-result v3

    .line 2592628
    const v4, -0x776b30d3

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 2592629
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2592630
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2592631
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2592632
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592633
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2592634
    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    .line 2592635
    invoke-virtual {p3, v6, v4}, LX/186;->b(II)V

    .line 2592636
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592637
    :sswitch_e
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2592638
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2592639
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592640
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592641
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592642
    :sswitch_f
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2592643
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v2

    .line 2592644
    const v3, -0x29540505

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2592645
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2592646
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 2592647
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2592648
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592649
    :sswitch_10
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2592650
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2592651
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2592652
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2592653
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 2592654
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592655
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2592656
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592657
    :sswitch_11
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2592658
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2592659
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592660
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592661
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592662
    :sswitch_12
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592663
    const v2, 0x212f9855

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2592664
    invoke-virtual {p0, p1, v8, v1}, LX/15i;->a(III)I

    move-result v2

    .line 2592665
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2592666
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2592667
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2592668
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592669
    invoke-virtual {p3, v8, v2, v1}, LX/186;->a(III)V

    .line 2592670
    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    .line 2592671
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592672
    :sswitch_13
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2592673
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2592674
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 2592675
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2592676
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2592677
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592678
    :sswitch_14
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2592679
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2592680
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 2592681
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2592682
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2592683
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592684
    :sswitch_15
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2592685
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2592686
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 2592687
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2592688
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2592689
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592690
    :sswitch_16
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2592691
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 2592692
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 2592693
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 2592694
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2592695
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592696
    :sswitch_17
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592697
    const v2, -0x5ab7b37b

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2592698
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592699
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592700
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592701
    :sswitch_18
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592702
    const v2, 0x2acf7ada

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 2592703
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592704
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592705
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    .line 2592706
    :sswitch_19
    const-class v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    .line 2592707
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2592708
    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2592709
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2592710
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ec06fdc -> :sswitch_8
        -0x776b30d3 -> :sswitch_e
        -0x69c00475 -> :sswitch_2
        -0x6780a343 -> :sswitch_f
        -0x62cf0c15 -> :sswitch_c
        -0x6066c8fc -> :sswitch_6
        -0x5ab7b37b -> :sswitch_18
        -0x531e37ab -> :sswitch_b
        -0x3041c0a7 -> :sswitch_d
        -0x29540505 -> :sswitch_10
        -0x24bc1186 -> :sswitch_15
        -0x1efecd1f -> :sswitch_16
        -0xa51e0a3 -> :sswitch_1
        0x5d40476 -> :sswitch_12
        0xbeee3ba -> :sswitch_7
        0xdbd3105 -> :sswitch_9
        0x1a9dacae -> :sswitch_4
        0x212f9855 -> :sswitch_13
        0x22d292b2 -> :sswitch_3
        0x2acf7ada -> :sswitch_19
        0x2c709872 -> :sswitch_5
        0x484a492f -> :sswitch_0
        0x6b576d72 -> :sswitch_a
        0x6c8f3994 -> :sswitch_17
        0x719f562c -> :sswitch_14
        0x7c3dba78 -> :sswitch_11
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2592534
    if-nez p0, :cond_0

    move v0, v1

    .line 2592535
    :goto_0
    return v0

    .line 2592536
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2592537
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2592538
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2592539
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2592540
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2592541
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2592542
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2592527
    const/4 v7, 0x0

    .line 2592528
    const/4 v1, 0x0

    .line 2592529
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2592530
    invoke-static {v2, v3, v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2592531
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2592532
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2592533
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2592526
    new-instance v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2592521
    if-eqz p0, :cond_0

    .line 2592522
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2592523
    if-eq v0, p0, :cond_0

    .line 2592524
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2592525
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2592486
    sparse-switch p2, :sswitch_data_0

    .line 2592487
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2592488
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592489
    const v1, -0xa51e0a3

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2592490
    :goto_0
    :sswitch_1
    return-void

    .line 2592491
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592492
    const v1, -0x69c00475

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2592493
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592494
    const v1, 0x1a9dacae

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2592495
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592496
    const v1, -0x6066c8fc

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2592497
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592498
    const v1, 0xbeee3ba

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2592499
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592500
    const v1, -0x7ec06fdc

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2592501
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592502
    const v1, 0xdbd3105

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2592503
    :sswitch_8
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592504
    const v1, 0x6b576d72

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2592505
    :sswitch_9
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592506
    const v1, -0x62cf0c15

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2592507
    :sswitch_a
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2592508
    const v1, -0x3041c0a7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2592509
    :sswitch_b
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2592510
    const v1, -0x776b30d3

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2592511
    :sswitch_c
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2592512
    const v1, -0x29540505

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2592513
    :sswitch_d
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592514
    const v1, 0x212f9855

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 2592515
    :sswitch_e
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592516
    const v1, -0x5ab7b37b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 2592517
    :sswitch_f
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2592518
    const v1, 0x2acf7ada

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 2592519
    :sswitch_10
    const-class v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    .line 2592520
    invoke-static {v0, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ec06fdc -> :sswitch_7
        -0x776b30d3 -> :sswitch_1
        -0x69c00475 -> :sswitch_1
        -0x6780a343 -> :sswitch_c
        -0x62cf0c15 -> :sswitch_a
        -0x6066c8fc -> :sswitch_5
        -0x5ab7b37b -> :sswitch_f
        -0x531e37ab -> :sswitch_9
        -0x3041c0a7 -> :sswitch_b
        -0x29540505 -> :sswitch_1
        -0x24bc1186 -> :sswitch_1
        -0x1efecd1f -> :sswitch_1
        -0xa51e0a3 -> :sswitch_2
        0x5d40476 -> :sswitch_d
        0xbeee3ba -> :sswitch_6
        0xdbd3105 -> :sswitch_8
        0x1a9dacae -> :sswitch_1
        0x212f9855 -> :sswitch_1
        0x22d292b2 -> :sswitch_3
        0x2acf7ada -> :sswitch_10
        0x2c709872 -> :sswitch_4
        0x484a492f -> :sswitch_0
        0x6b576d72 -> :sswitch_1
        0x6c8f3994 -> :sswitch_e
        0x719f562c -> :sswitch_1
        0x7c3dba78 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2592476
    if-nez p1, :cond_0

    move v0, v1

    .line 2592477
    :goto_0
    return v0

    .line 2592478
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 2592479
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2592480
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2592481
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 2592482
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 2592483
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2592484
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2592485
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2592469
    if-eqz p1, :cond_0

    .line 2592470
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 2592471
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2592472
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 2592473
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2592474
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2592475
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2592715
    if-eqz p1, :cond_0

    .line 2592716
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;

    move-result-object v1

    .line 2592717
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;

    .line 2592718
    if-eq v0, v1, :cond_0

    .line 2592719
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2592720
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2592435
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2592441
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2592442
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2592436
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2592437
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2592438
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a:LX/15i;

    .line 2592439
    iput p2, p0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->b:I

    .line 2592440
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2592443
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2592444
    new-instance v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2592445
    iget v0, p0, LX/1vt;->c:I

    .line 2592446
    move v0, v0

    .line 2592447
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2592448
    iget v0, p0, LX/1vt;->c:I

    .line 2592449
    move v0, v0

    .line 2592450
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2592451
    iget v0, p0, LX/1vt;->b:I

    .line 2592452
    move v0, v0

    .line 2592453
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2592454
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2592455
    move-object v0, v0

    .line 2592456
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2592457
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2592458
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2592459
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2592460
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2592461
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2592462
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2592463
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2592464
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2592465
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2592466
    iget v0, p0, LX/1vt;->c:I

    .line 2592467
    move v0, v0

    .line 2592468
    return v0
.end method
