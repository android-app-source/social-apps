.class public Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Lcom/facebook/location/FbLocationOperationParams;

.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final d:LX/03V;

.field public final e:LX/0tX;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/IcH;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 2595371
    const-class v0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->a:Ljava/lang/String;

    .line 2595372
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0x493e0

    .line 2595373
    iput-wide v2, v0, LX/1S7;->b:J

    .line 2595374
    move-object v0, v0

    .line 2595375
    const/high16 v1, 0x44960000    # 1200.0f

    .line 2595376
    iput v1, v0, LX/1S7;->c:F

    .line 2595377
    move-object v0, v0

    .line 2595378
    const-wide/16 v2, 0x12c

    .line 2595379
    iput-wide v2, v0, LX/1S7;->d:J

    .line 2595380
    move-object v0, v0

    .line 2595381
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->b:Lcom/facebook/location/FbLocationOperationParams;

    .line 2595382
    const-class v0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    const-string v1, "ride_requests"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0tX;LX/0Or;LX/1Ck;Landroid/content/res/Resources;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;",
            "LX/1Ck;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2595383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2595384
    iput-object p1, p0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->d:LX/03V;

    .line 2595385
    iput-object p2, p0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->e:LX/0tX;

    .line 2595386
    iput-object p3, p0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->f:LX/0Or;

    .line 2595387
    iput-object p4, p0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->g:LX/1Ck;

    .line 2595388
    iput-object p5, p0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->h:Landroid/content/res/Resources;

    .line 2595389
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;
    .locals 6

    .prologue
    .line 2595390
    new-instance v0, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    const/16 v3, 0xc81

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;-><init>(LX/03V;LX/0tX;LX/0Or;LX/1Ck;Landroid/content/res/Resources;)V

    .line 2595391
    return-object v0
.end method
