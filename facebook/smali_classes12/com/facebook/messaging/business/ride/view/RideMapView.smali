.class public Lcom/facebook/messaging/business/ride/view/RideMapView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/Ibv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/business/ride/gating/IsRideGoogleMapEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/uicontrib/fab/FabView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/maps/FbMapViewDelegate;

.field public f:Landroid/widget/ImageView;

.field private g:LX/Icr;

.field public h:LX/Ics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/android/maps/model/LatLng;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2595893
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2595894
    sget-object v0, LX/Icr;->USER:LX/Icr;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->g:LX/Icr;

    .line 2595895
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->h()V

    .line 2595896
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2595889
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2595890
    sget-object v0, LX/Icr;->USER:LX/Icr;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->g:LX/Icr;

    .line 2595891
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->h()V

    .line 2595892
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2595885
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2595886
    sget-object v0, LX/Icr;->USER:LX/Icr;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->g:LX/Icr;

    .line 2595887
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->h()V

    .line 2595888
    return-void
.end method

.method private a(Lcom/facebook/android/maps/model/LatLng;)V
    .locals 2

    .prologue
    .line 2595881
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a(Z)V

    .line 2595882
    sget-object v0, LX/Icr;->PROGRAMATIC:LX/Icr;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->g:LX/Icr;

    .line 2595883
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v1, LX/Icp;

    invoke-direct {v1, p0, p1}, LX/Icp;-><init>(Lcom/facebook/messaging/business/ride/view/RideMapView;Lcom/facebook/android/maps/model/LatLng;)V

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2595884
    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/ride/view/RideMapView;LX/Ibv;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/business/ride/view/RideMapView;",
            "LX/Ibv;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2595880
    iput-object p1, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->a:LX/Ibv;

    iput-object p2, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->b:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/business/ride/view/RideMapView;

    invoke-static {v1}, LX/Ibv;->a(LX/0QB;)LX/Ibv;

    move-result-object v0

    check-cast v0, LX/Ibv;

    const/16 v2, 0x14fe

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a(Lcom/facebook/messaging/business/ride/view/RideMapView;LX/Ibv;LX/0Or;)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2595874
    if-eqz p1, :cond_0

    .line 2595875
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2595876
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->c:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    .line 2595877
    :goto_0
    return-void

    .line 2595878
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2595879
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->c:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/ride/view/RideMapView;LX/692;)V
    .locals 2

    .prologue
    .line 2595854
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->h:LX/Ics;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->g:LX/Icr;

    sget-object v1, LX/Icr;->PROGRAMATIC:LX/Icr;

    if-eq v0, v1, :cond_0

    iget-object v0, p1, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->i:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2595855
    :cond_0
    :goto_0
    return-void

    .line 2595856
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->h:LX/Ics;

    iget-object v1, p1, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    invoke-interface {v0, v1}, LX/Ics;->a(Lcom/facebook/android/maps/model/LatLng;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/ride/view/RideMapView;LX/6al;)V
    .locals 1

    .prologue
    .line 2595869
    invoke-virtual {p1}, LX/6al;->e()LX/692;

    move-result-object v0

    .line 2595870
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->i:Lcom/facebook/android/maps/model/LatLng;

    .line 2595871
    sget-object v0, LX/Icr;->USER:LX/Icr;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->g:LX/Icr;

    .line 2595872
    return-void

    .line 2595873
    :cond_0
    iget-object v0, v0, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    goto :goto_0
.end method

.method public static c(Landroid/location/Location;)Lcom/facebook/android/maps/model/LatLng;
    .locals 6

    .prologue
    .line 2595868
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2595857
    const v0, 0x7f03121f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2595858
    const-class v0, Lcom/facebook/messaging/business/ride/view/RideMapView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2595859
    const v0, 0x7f0d2a5d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2595860
    const v0, 0x7f0d2a5c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->f:Landroid/widget/ImageView;

    .line 2595861
    const v0, 0x7f0d2a5b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbMapViewDelegate;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    .line 2595862
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2595863
    :goto_0
    iput v0, v1, Lcom/facebook/maps/FbMapViewDelegate;->a:I

    .line 2595864
    const v0, 0x7f0d2a5e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->c:Lcom/facebook/uicontrib/fab/FabView;

    .line 2595865
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->c:Lcom/facebook/uicontrib/fab/FabView;

    new-instance v1, LX/Ici;

    invoke-direct {v1, p0}, LX/Ici;-><init>(Lcom/facebook/messaging/business/ride/view/RideMapView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2595866
    return-void

    .line 2595867
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2595897
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v1, LX/Icm;

    invoke-direct {v1, p0}, LX/Icm;-><init>(Lcom/facebook/messaging/business/ride/view/RideMapView;)V

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2595898
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 2595839
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v1, LX/Icn;

    invoke-direct {v1, p0}, LX/Icn;-><init>(Lcom/facebook/messaging/business/ride/view/RideMapView;)V

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2595840
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2595837
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v1, LX/Ick;

    invoke-direct {v1, p0}, LX/Ick;-><init>(Lcom/facebook/messaging/business/ride/view/RideMapView;)V

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2595838
    return-void
.end method

.method public final a(DD)V
    .locals 1

    .prologue
    .line 2595834
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->j()V

    .line 2595835
    new-instance v0, Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-direct {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 2595836
    return-void
.end method

.method public final a(Landroid/location/Location;)V
    .locals 1

    .prologue
    .line 2595832
    invoke-static {p1}, Lcom/facebook/messaging/business/ride/view/RideMapView;->c(Landroid/location/Location;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a(Lcom/facebook/android/maps/model/LatLng;)V

    .line 2595833
    return-void
.end method

.method public final a(Landroid/location/Location;Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 2595827
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->f:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2595828
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v1, LX/Icq;

    invoke-direct {v1, p0, p1, p2}, LX/Icq;-><init>(Lcom/facebook/messaging/business/ride/view/RideMapView;Landroid/location/Location;Landroid/location/Location;)V

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2595829
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2595841
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p1}, LX/6Zn;->a(Landroid/os/Bundle;)V

    .line 2595842
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->i()V

    .line 2595843
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2595844
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->d()V

    .line 2595845
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2595846
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p1}, LX/6Zn;->b(Landroid/os/Bundle;)V

    .line 2595847
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2595848
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->c()V

    .line 2595849
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2595850
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->a()V

    .line 2595851
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2595852
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0}, LX/6Zn;->b()V

    .line 2595853
    return-void
.end method

.method public setMapDisplayListener(LX/Ics;)V
    .locals 0

    .prologue
    .line 2595830
    iput-object p1, p0, Lcom/facebook/messaging/business/ride/view/RideMapView;->h:LX/Ics;

    .line 2595831
    return-void
.end method
