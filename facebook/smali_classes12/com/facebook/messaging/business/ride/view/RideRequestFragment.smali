.class public final Lcom/facebook/messaging/business/ride/view/RideRequestFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""


# static fields
.field public static final s:[Ljava/lang/String;


# instance fields
.field public A:Z

.field public B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

.field public C:Lcom/facebook/widget/text/BetterButton;

.field public D:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

.field public E:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

.field public F:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

.field public G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

.field public H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

.field public I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Landroid/location/Location;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public K:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public L:Lcom/facebook/payments/paymentmethods/model/CreditCard;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:LX/0y2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ic9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ibs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/IcM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Ic4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ibu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Ib2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Ibm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/Ich;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/6Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/business/ride/gating/IsRideGoogleMapEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/IZK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Ib3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/IcC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Dde;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final t:LX/IdA;

.field public u:Z

.field public v:Z

.field public w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

.field private x:Landroid/os/Handler;

.field public y:Lcom/facebook/messaging/business/ride/view/RideMapView;

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2596478
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->s:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2596479
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2596480
    new-instance v0, LX/IdA;

    invoke-direct {v0, p0}, LX/IdA;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->t:LX/IdA;

    .line 2596481
    new-instance v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;

    invoke-direct {v0}, Lcom/facebook/messaging/business/ride/utils/LocationParams;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    .line 2596482
    new-instance v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;

    invoke-direct {v0}, Lcom/facebook/messaging/business/ride/utils/LocationParams;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    .line 2596483
    return-void
.end method

.method public static A(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z
    .locals 1

    .prologue
    .line 2596457
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static B(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V
    .locals 11

    .prologue
    .line 2596484
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    .line 2596485
    iget-object v1, v0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    move-object v3, v1

    .line 2596486
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->g:LX/Ib2;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    iget-boolean v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->v:Z

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    iget-object v4, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v4, v4, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v5, v5, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    .line 2596487
    const-string v6, "android_messenger_ride_request"

    .line 2596488
    iget-object v7, v0, LX/Ib2;->a:LX/0Zb;

    const/4 v8, 0x0

    invoke-interface {v7, v6, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v7

    move-object v6, v7

    .line 2596489
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-nez v7, :cond_1

    .line 2596490
    :goto_1
    return-void

    .line 2596491
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->k()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 2596492
    :cond_1
    const-string v7, "ride"

    invoke-virtual {v6, v7}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "provider_id"

    .line 2596493
    iget-object v8, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    move-object v8, v8

    .line 2596494
    invoke-virtual {v6, v7, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "thread_id"

    .line 2596495
    iget-object v8, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v8, v8

    .line 2596496
    if-nez v8, :cond_2

    const/4 v9, 0x0

    :goto_2
    move-object v8, v9

    .line 2596497
    invoke-virtual {v6, v7, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "message_id"

    .line 2596498
    iget-object v8, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->c:Ljava/lang/String;

    move-object v8, v8

    .line 2596499
    invoke-virtual {v6, v7, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "entry_point"

    .line 2596500
    iget-object v8, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->a:Ljava/lang/String;

    move-object v8, v8

    .line 2596501
    invoke-virtual {v6, v7, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "is_ride_requested"

    invoke-virtual {v6, v7, v2}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    const-string v7, "ride_type_id"

    invoke-virtual {v6, v7, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "origin_address"

    invoke-virtual {v6, v7, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "dest_address"

    invoke-virtual {v6, v7, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "request_tag"

    .line 2596502
    iget-object v8, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->g:Ljava/lang/String;

    move-object v8, v8

    .line 2596503
    invoke-virtual {v6, v7, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    invoke-virtual {v6}, LX/0oG;->d()V

    goto :goto_1

    :cond_2
    invoke-virtual {v8}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    goto :goto_2
.end method

.method public static C(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V
    .locals 3

    .prologue
    .line 2596504
    sget-object v0, LX/6xZ;->SELECT_PAYMENT_METHOD:LX/6xZ;

    sget-object v1, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v1}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v1

    invoke-virtual {v1}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a(LX/6xZ;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/718;

    move-result-object v0

    sget-object v1, LX/5g0;->MESSENGER_COMMERCE:LX/5g0;

    iget-object v1, v1, LX/5g0;->analyticsModule:Ljava/lang/String;

    .line 2596505
    iput-object v1, v0, LX/718;->c:Ljava/lang/String;

    .line 2596506
    move-object v0, v0

    .line 2596507
    invoke-virtual {v0}, LX/718;->a()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v0

    .line 2596508
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->newBuilder()LX/71A;

    move-result-object v1

    .line 2596509
    iput-object v0, v1, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 2596510
    move-object v0, v1

    .line 2596511
    sget-object v1, LX/71C;->MESSENGER_COMMERCE:LX/71C;

    .line 2596512
    iput-object v1, v0, LX/71A;->b:LX/71C;

    .line 2596513
    move-object v0, v0

    .line 2596514
    sget-object v1, LX/6xg;->MOR_MESSENGER_COMMERCE:LX/6xg;

    .line 2596515
    iput-object v1, v0, LX/71A;->c:LX/6xg;

    .line 2596516
    move-object v0, v0

    .line 2596517
    invoke-static {}, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->newBuilder()LX/707;

    move-result-object v1

    const/4 v2, 0x1

    .line 2596518
    iput-boolean v2, v1, LX/707;->a:Z

    .line 2596519
    move-object v1, v1

    .line 2596520
    invoke-virtual {v1}, LX/707;->e()Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    move-result-object v1

    .line 2596521
    iput-object v1, v0, LX/71A;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 2596522
    move-object v0, v0

    .line 2596523
    const v1, 0x7f080c7a

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2596524
    iput-object v1, v0, LX/71A;->d:Ljava/lang/String;

    .line 2596525
    move-object v0, v0

    .line 2596526
    invoke-virtual {v0}, LX/71A;->h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    .line 2596527
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->newBuilder()LX/6zf;

    move-result-object v2

    .line 2596528
    iput-object v0, v2, LX/6zf;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 2596529
    move-object v0, v2

    .line 2596530
    const/4 v2, 0x0

    .line 2596531
    iput-boolean v2, v0, LX/6zf;->c:Z

    .line 2596532
    move-object v0, v0

    .line 2596533
    sget-object v2, LX/6zQ;->NEW_PAYPAL:LX/6zQ;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2596534
    iput-object v2, v0, LX/6zf;->b:LX/0Px;

    .line 2596535
    move-object v0, v0

    .line 2596536
    invoke-virtual {v0}, LX/6zf;->e()Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v0

    .line 2596537
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->h:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x4

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2596538
    return-void
.end method

.method public static D(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2596539
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    .line 2596540
    iget-object v2, v1, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    move-object v1, v2

    .line 2596541
    if-eqz v1, :cond_2

    .line 2596542
    invoke-virtual {v1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->j()Z

    move-result v1

    .line 2596543
    :goto_0
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;LX/0y2;LX/Ic9;LX/Ibs;LX/IcM;LX/Ic4;LX/Ibu;LX/Ib2;Lcom/facebook/content/SecureContextHelper;LX/1Ml;LX/Ibm;LX/Ich;LX/6Zb;LX/0Or;LX/IZK;LX/Ib3;LX/0Uh;LX/IcC;LX/Dde;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/business/ride/view/RideRequestFragment;",
            "LX/0y2;",
            "LX/Ic9;",
            "LX/Ibs;",
            "LX/IcM;",
            "LX/Ic4;",
            "LX/Ibu;",
            "LX/Ib2;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1Ml;",
            "LX/Ibm;",
            "LX/Ich;",
            "LX/6Zb;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/IZK;",
            "LX/Ib3;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/IcC;",
            "LX/Dde;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2596544
    iput-object p1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a:LX/0y2;

    iput-object p2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->b:LX/Ic9;

    iput-object p3, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->c:LX/Ibs;

    iput-object p4, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->d:LX/IcM;

    iput-object p5, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->e:LX/Ic4;

    iput-object p6, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->f:LX/Ibu;

    iput-object p7, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->g:LX/Ib2;

    iput-object p8, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->h:Lcom/facebook/content/SecureContextHelper;

    iput-object p9, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->i:LX/1Ml;

    iput-object p10, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->j:LX/Ibm;

    iput-object p11, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->k:LX/Ich;

    iput-object p12, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->l:LX/6Zb;

    iput-object p13, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->m:LX/0Or;

    iput-object p14, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->n:LX/IZK;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->o:LX/Ib3;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->p:LX/0Uh;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->q:LX/IcC;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->r:LX/Dde;

    return-void
.end method

.method public static a(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Lcom/facebook/payments/paymentmethods/model/CreditCard;)V
    .locals 5

    .prologue
    .line 2596545
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2596546
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;)V

    .line 2596547
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->F:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    const v1, 0x7f082d8f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596548
    iget-object p1, v4, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->f:Ljava/lang/String;

    move-object v4, p1

    .line 2596549
    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setSelectedSubText(Ljava/lang/String;)V

    .line 2596550
    return-void
.end method

.method public static a(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Z)V
    .locals 2

    .prologue
    .line 2596551
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596552
    iget-object v1, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->j:Ljava/lang/String;

    move-object v0, v1

    .line 2596553
    if-eqz v0, :cond_0

    .line 2596554
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->r:LX/Dde;

    invoke-virtual {v1, v0, p1}, LX/Dde;->a(Ljava/lang/String;Z)V

    .line 2596555
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 20

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v19

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static/range {v19 .. v19}, LX/0y2;->a(LX/0QB;)LX/0y2;

    move-result-object v2

    check-cast v2, LX/0y2;

    invoke-static/range {v19 .. v19}, LX/Ic9;->a(LX/0QB;)LX/Ic9;

    move-result-object v3

    check-cast v3, LX/Ic9;

    invoke-static/range {v19 .. v19}, LX/Ibs;->a(LX/0QB;)LX/Ibs;

    move-result-object v4

    check-cast v4, LX/Ibs;

    invoke-static/range {v19 .. v19}, LX/IcM;->a(LX/0QB;)LX/IcM;

    move-result-object v5

    check-cast v5, LX/IcM;

    invoke-static/range {v19 .. v19}, LX/Ic4;->a(LX/0QB;)LX/Ic4;

    move-result-object v6

    check-cast v6, LX/Ic4;

    invoke-static/range {v19 .. v19}, LX/Ibu;->a(LX/0QB;)LX/Ibu;

    move-result-object v7

    check-cast v7, LX/Ibu;

    invoke-static/range {v19 .. v19}, LX/Ib2;->a(LX/0QB;)LX/Ib2;

    move-result-object v8

    check-cast v8, LX/Ib2;

    invoke-static/range {v19 .. v19}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v19 .. v19}, LX/1Ml;->a(LX/0QB;)LX/1Ml;

    move-result-object v10

    check-cast v10, LX/1Ml;

    invoke-static/range {v19 .. v19}, LX/Ibm;->a(LX/0QB;)LX/Ibm;

    move-result-object v11

    check-cast v11, LX/Ibm;

    invoke-static/range {v19 .. v19}, LX/Ich;->a(LX/0QB;)LX/Ich;

    move-result-object v12

    check-cast v12, LX/Ich;

    invoke-static/range {v19 .. v19}, LX/6Zb;->a(LX/0QB;)LX/6Zb;

    move-result-object v13

    check-cast v13, LX/6Zb;

    const/16 v14, 0x14fe

    move-object/from16 v0, v19

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    invoke-static/range {v19 .. v19}, LX/IZK;->a(LX/0QB;)LX/IZK;

    move-result-object v15

    check-cast v15, LX/IZK;

    invoke-static/range {v19 .. v19}, LX/Ib3;->a(LX/0QB;)LX/Ib3;

    move-result-object v16

    check-cast v16, LX/Ib3;

    invoke-static/range {v19 .. v19}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v17

    check-cast v17, LX/0Uh;

    invoke-static/range {v19 .. v19}, LX/IcC;->a(LX/0QB;)LX/IcC;

    move-result-object v18

    check-cast v18, LX/IcC;

    invoke-static/range {v19 .. v19}, LX/Dde;->a(LX/0QB;)LX/Dde;

    move-result-object v19

    check-cast v19, LX/Dde;

    invoke-static/range {v1 .. v19}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;LX/0y2;LX/Ic9;LX/Ibs;LX/IcM;LX/Ic4;LX/Ibu;LX/Ib2;Lcom/facebook/content/SecureContextHelper;LX/1Ml;LX/Ibm;LX/Ich;LX/6Zb;LX/0Or;LX/IZK;LX/Ib3;LX/0Uh;LX/IcC;LX/Dde;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DD)V
    .locals 7

    .prologue
    .line 2596556
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w()V

    .line 2596557
    new-instance v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment$14;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment$14;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DD)V

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->K:Ljava/lang/Runnable;

    .line 2596558
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->x:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->K:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    const v4, 0x43568f30

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2596559
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DDLjava/lang/String;)V
    .locals 3
    .param p3    # D
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2596560
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 2596561
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    new-instance v1, Landroid/location/Location;

    const-string v2, ""

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    .line 2596562
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v0, p1, p2}, Landroid/location/Location;->setLatitude(D)V

    .line 2596563
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v0, p3, p4}, Landroid/location/Location;->setLongitude(D)V

    .line 2596564
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iput-object p5, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->c:Ljava/lang/String;

    .line 2596565
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->x(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596566
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;I)V
    .locals 7

    .prologue
    .line 2596567
    const/4 v0, 0x0

    .line 2596568
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->J:Landroid/location/Location;

    if-eqz v1, :cond_0

    .line 2596569
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->J:Landroid/location/Location;

    move-object v1, v0

    .line 2596570
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    const-string v0, "rideshare_destination"

    :goto_1
    sget-object v4, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;->a:Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;

    .line 2596571
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/facebook/addresstypeahead/AddressTypeAheadActivity;

    invoke-direct {v5, v2, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2596572
    const-string v6, "current_location"

    invoke-virtual {v5, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2596573
    const-string v6, "is_using_google_api"

    invoke-virtual {v5, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2596574
    const-string v6, "product_tag"

    invoke-virtual {v5, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2596575
    const-string v6, "result_handler_params"

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2596576
    move-object v0, v5

    .line 2596577
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->h:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2596578
    return-void

    .line 2596579
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-eqz v1, :cond_1

    .line 2596580
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    move-object v1, v0

    goto :goto_0

    .line 2596581
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-eqz v1, :cond_2

    .line 2596582
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    move-object v1, v0

    goto :goto_0

    .line 2596583
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a:LX/0y2;

    invoke-virtual {v1}, LX/0y2;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v1

    .line 2596584
    if-eqz v1, :cond_4

    .line 2596585
    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2596586
    :cond_3
    const-string v0, "rideshare_origin"

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;)V
    .locals 1
    .param p0    # Lcom/facebook/messaging/business/ride/view/RideRequestFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2596587
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2596588
    :goto_0
    return-void

    .line 2596589
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->F:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setSelectedItem(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;LX/Ib6;)V
    .locals 1
    .param p0    # Lcom/facebook/messaging/business/ride/view/RideRequestFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2596590
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2596591
    :cond_0
    :goto_0
    return-void

    .line 2596592
    :cond_1
    sget-object v0, LX/Ib6;->ORIGIN:LX/Ib6;

    if-ne p2, v0, :cond_2

    .line 2596593
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iput-object p1, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    .line 2596594
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->D:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setSelectedItem(Ljava/lang/String;)V

    goto :goto_0

    .line 2596595
    :cond_2
    sget-object v0, LX/Ib6;->DESTINATION:LX/Ib6;

    if-ne p2, v0, :cond_0

    .line 2596596
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iput-object p1, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    .line 2596597
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->E:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setSelectedItem(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Landroid/location/Address;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/location/Address;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2596598
    if-nez p0, :cond_1

    .line 2596599
    :cond_0
    :goto_0
    return-object v0

    .line 2596600
    :cond_1
    invoke-virtual {p0}, Landroid/location/Address;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2596601
    if-eqz v1, :cond_0

    const-string v0, "google_place_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DDLjava/lang/String;)V
    .locals 3
    .param p3    # D
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2596602
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 2596603
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    new-instance v1, Landroid/location/Location;

    const-string v2, ""

    invoke-direct {v1, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    .line 2596604
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v0, p1, p2}, Landroid/location/Location;->setLatitude(D)V

    .line 2596605
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v0, p3, p4}, Landroid/location/Location;->setLongitude(D)V

    .line 2596606
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iput-object p5, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->c:Ljava/lang/String;

    .line 2596607
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->x(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596608
    return-void
.end method

.method private e()V
    .locals 7

    .prologue
    .line 2596609
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596610
    iget-object v1, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2596611
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2596612
    :goto_0
    return-void

    .line 2596613
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->E:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->b()V

    .line 2596614
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->j:LX/Ibm;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596615
    iget-object v2, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2596616
    new-instance v2, LX/IdB;

    invoke-direct {v2, p0}, LX/IdB;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    const/4 v4, 0x1

    .line 2596617
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v4

    :goto_1
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 2596618
    invoke-virtual {v0}, LX/Ibm;->a()V

    .line 2596619
    iget-object v3, v0, LX/Ibm;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IJn;

    .line 2596620
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2596621
    new-instance v5, LX/4FN;

    invoke-direct {v5}, LX/4FN;-><init>()V

    new-instance v6, LX/4FL;

    invoke-direct {v6}, LX/4FL;-><init>()V

    invoke-virtual {v6, v1}, LX/4FL;->a(Ljava/lang/String;)LX/4FL;

    move-result-object v6

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/4FN;->a(Ljava/util/List;)LX/4FN;

    move-result-object v5

    .line 2596622
    new-instance v6, LX/IJo;

    invoke-direct {v6}, LX/IJo;-><init>()V

    move-object v6, v6

    .line 2596623
    const-string p0, "addresses"

    invoke-virtual {v6, p0, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v5

    const-string v6, "limit"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v5, v6, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/IJo;

    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    .line 2596624
    iget-object v6, v3, LX/IJn;->b:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    .line 2596625
    new-instance v6, LX/IJm;

    invoke-direct {v6, v3}, LX/IJm;-><init>(LX/IJn;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p0

    invoke-static {v5, v6, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v3, v5

    .line 2596626
    iput-object v3, v0, LX/Ibm;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2596627
    iget-object v3, v0, LX/Ibm;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, LX/Ibl;

    invoke-direct {v4, v0, v1, v2}, LX/Ibl;-><init>(LX/Ibm;Ljava/lang/String;LX/IdB;)V

    iget-object v5, v0, LX/Ibm;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2596628
    goto :goto_0

    .line 2596629
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static k(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V
    .locals 7

    .prologue
    .line 2596630
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-nez v0, :cond_1

    .line 2596631
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    const v1, 0x7f082d7a

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->a(Ljava/lang/String;)V

    .line 2596632
    :cond_0
    :goto_0
    return-void

    .line 2596633
    :cond_1
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->A(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2596634
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a(Landroid/location/Location;Landroid/location/Location;)V

    .line 2596635
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->d:LX/IcM;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596636
    iget-object v2, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    move-object v1, v2

    .line 2596637
    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v2, v2, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v4, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v4, v4, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    iget-object v6, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->t:LX/IdA;

    invoke-virtual/range {v0 .. v6}, LX/IcM;->a(Ljava/lang/String;DDLX/IdA;)V

    .line 2596638
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2596639
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->f:LX/Ibu;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, LX/Ibu;->a(DD)V

    goto :goto_0

    .line 2596640
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a(DD)V

    goto :goto_1
.end method

.method private l()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2596641
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->p:LX/0Uh;

    const/16 v2, 0x1bb

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596642
    iget-object v2, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2596643
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private u()V
    .locals 6

    .prologue
    .line 2596458
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->F:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0816

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setIconTintColor(I)V

    .line 2596459
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->F:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->c()V

    .line 2596460
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->F:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    new-instance v1, LX/Id3;

    invoke-direct {v1, p0}, LX/Id3;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596461
    iput-object v1, v0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->k:LX/Icw;

    .line 2596462
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->L:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    if-eqz v0, :cond_0

    .line 2596463
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->L:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Lcom/facebook/payments/paymentmethods/model/CreditCard;)V

    .line 2596464
    :goto_0
    return-void

    .line 2596465
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->I:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2596466
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->I:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 2596467
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->F:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->b()V

    .line 2596468
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->b:LX/Ic9;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596469
    iget-object v2, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    move-object v1, v2

    .line 2596470
    new-instance v2, LX/Id5;

    invoke-direct {v2, p0}, LX/Id5;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596471
    invoke-virtual {v0}, LX/Ic9;->a()V

    .line 2596472
    new-instance v3, LX/IbG;

    invoke-direct {v3}, LX/IbG;-><init>()V

    move-object v3, v3

    .line 2596473
    const-string v4, "provider"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/IbG;

    .line 2596474
    iget-object v4, v0, LX/Ic9;->d:LX/1Ck;

    const-string v5, "task_key_fetch_payment_info"

    iget-object p0, v0, LX/Ic9;->b:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2596475
    new-instance p0, LX/Ic8;

    invoke-direct {p0, v0, v2}, LX/Ic8;-><init>(LX/Ic9;LX/Id5;)V

    move-object p0, p0

    .line 2596476
    invoke-virtual {v4, v5, v3, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2596477
    goto :goto_0
.end method

.method public static v(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2596644
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->J:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 2596645
    :goto_0
    return-void

    .line 2596646
    :cond_0
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->A(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2596647
    const v0, 0x7f082d74

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/Ib6;->ORIGIN:LX/Ib6;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;LX/Ib6;)V

    .line 2596648
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iput-object v6, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    .line 2596649
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->J:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->J:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DDLjava/lang/String;)V

    .line 2596650
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->J:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->J:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DD)V

    .line 2596651
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->z:Z

    .line 2596652
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->J:Landroid/location/Location;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a(Landroid/location/Location;)V

    goto :goto_0
.end method

.method private w()V
    .locals 2

    .prologue
    .line 2596272
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->K:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2596273
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->x:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->K:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2596274
    :cond_0
    return-void
.end method

.method public static x(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V
    .locals 2

    .prologue
    .line 2596269
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->C:Lcom/facebook/widget/text/BetterButton;

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->D(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    .line 2596270
    return-void

    .line 2596271
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static z(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z
    .locals 2

    .prologue
    .line 2596268
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->i:LX/1Ml;

    sget-object v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->s:[Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2596267
    const v0, 0x7f082d65

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/IZ9;)V
    .locals 0

    .prologue
    .line 2596266
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2596259
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2596260
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2596261
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2596262
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2596263
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->x:Landroid/os/Handler;

    .line 2596264
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->l:LX/6Zb;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->k:LX/Ich;

    invoke-virtual {v0, p0, v1}, LX/6Zb;->a(Lcom/facebook/base/fragment/FbFragment;LX/6ZZ;)V

    .line 2596265
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2596246
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "ride_service_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596247
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596248
    iget-object p1, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    move-object v0, p1

    .line 2596249
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2596250
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596251
    iget-object p1, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->f:Ljava/lang/String;

    move-object v0, p1

    .line 2596252
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2596253
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596254
    iget-object v1, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v1

    .line 2596255
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2596256
    return-void

    :cond_0
    move v0, v2

    .line 2596257
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2596258
    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2596239
    iget-boolean v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->A:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->z(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2596240
    :cond_0
    :goto_0
    return-void

    .line 2596241
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a()V

    .line 2596242
    iput-boolean v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->A:Z

    .line 2596243
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->A(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2596244
    iput-boolean v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->z:Z

    goto :goto_0

    .line 2596245
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b()V

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 2596238
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->p:LX/0Uh;

    const/16 v1, 0x112

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v0, -0x1

    .line 2596217
    packed-switch p1, :pswitch_data_0

    .line 2596218
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2596219
    :cond_0
    :goto_0
    return-void

    .line 2596220
    :pswitch_1
    if-ne p2, v0, :cond_0

    .line 2596221
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->g:LX/Ib2;

    const-string v1, "success_update_pickup_location"

    invoke-virtual {v0, v1}, LX/Ib2;->b(Ljava/lang/String;)V

    .line 2596222
    const-string v0, "selected_address"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 2596223
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->f:LX/Ibu;

    invoke-virtual {v1, v0}, LX/Ibu;->a(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/Ib6;->ORIGIN:LX/Ib6;

    invoke-static {p0, v1, v2}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;LX/Ib6;)V

    .line 2596224
    invoke-virtual {v0}, Landroid/location/Address;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Address;->getLongitude()D

    move-result-wide v4

    invoke-static {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->b(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DDLjava/lang/String;)V

    .line 2596225
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->z:Z

    goto :goto_0

    .line 2596226
    :pswitch_2
    if-ne p2, v0, :cond_0

    .line 2596227
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->g:LX/Ib2;

    const-string v1, "success_update_dropoff_location"

    invoke-virtual {v0, v1}, LX/Ib2;->b(Ljava/lang/String;)V

    .line 2596228
    const-string v0, "selected_address"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 2596229
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->f:LX/Ibu;

    invoke-virtual {v1, v0}, LX/Ibu;->a(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/Ib6;->DESTINATION:LX/Ib6;

    invoke-static {p0, v1, v2}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;LX/Ib6;)V

    .line 2596230
    invoke-virtual {v0}, Landroid/location/Address;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Address;->getLongitude()D

    move-result-wide v4

    invoke-static {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->b(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->b$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DDLjava/lang/String;)V

    goto :goto_0

    .line 2596231
    :pswitch_3
    if-ne p2, v0, :cond_1

    if-eqz p3, :cond_1

    .line 2596232
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->g:LX/Ib2;

    const-string v1, "success_update_payment_info"

    invoke-virtual {v0, v1}, LX/Ib2;->b(Ljava/lang/String;)V

    .line 2596233
    const-string v0, "selected_payment_method"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 2596234
    sget-object v1, LX/6zU;->CREDIT_CARD:LX/6zU;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 2596235
    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->L:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 2596236
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->L:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    invoke-static {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Lcom/facebook/payments/paymentmethods/model/CreditCard;)V

    .line 2596237
    :cond_1
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->x(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 2596214
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2596215
    const v0, 0x7f11002c

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2596216
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x14f738e4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2596275
    const v1, 0x7f031224

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x3fc6926a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x151b1e87

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2596276
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onDestroy()V

    .line 2596277
    iget-boolean v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->v:Z

    if-nez v1, :cond_0

    .line 2596278
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596279
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->d:LX/IcM;

    .line 2596280
    iget-object v2, v1, LX/IcM;->c:LX/1Ck;

    const-string v4, "task_key_fetch_ride_type"

    invoke-virtual {v2, v4}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2596281
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->b:LX/Ic9;

    invoke-virtual {v1}, LX/Ic9;->a()V

    .line 2596282
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/ride/view/RideMapView;->f()V

    .line 2596283
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->l:LX/6Zb;

    invoke-virtual {v1}, LX/6Zb;->a()V

    .line 2596284
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596285
    iget-object v2, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->j:Ljava/lang/String;

    move-object v1, v2

    .line 2596286
    if-eqz v1, :cond_1

    .line 2596287
    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->r:LX/Dde;

    invoke-virtual {v2, v1}, LX/Dde;->a(Ljava/lang/String;)V

    .line 2596288
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x5b725004

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 2596289
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onLowMemory()V

    .line 2596290
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->g()V

    .line 2596291
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 2596292
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0d3254

    if-ne v0, v1, :cond_0

    .line 2596293
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->q:LX/IcC;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596294
    iget-object v2, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    move-object v1, v2

    .line 2596295
    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596296
    iget-object p0, v2, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->i:Ljava/lang/String;

    move-object v2, p0

    .line 2596297
    invoke-virtual {v0, v1, v2}, LX/IcC;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2596298
    const/4 v0, 0x1

    .line 2596299
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x18f77d99

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2596300
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onPause()V

    .line 2596301
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/ride/view/RideMapView;->e()V

    .line 2596302
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w()V

    .line 2596303
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->f:LX/Ibu;

    invoke-virtual {v1}, LX/Ibu;->a()V

    .line 2596304
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->c:LX/Ibs;

    invoke-virtual {v1}, LX/Ibs;->a()V

    .line 2596305
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->j:LX/Ibm;

    invoke-virtual {v1}, LX/Ibm;->a()V

    .line 2596306
    const/16 v1, 0x2b

    const v2, -0x167428e3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5

    .prologue
    .line 2596307
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2596308
    const v0, 0x7f0d3254

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2596309
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020fea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2596310
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, -0x1

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2596311
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2596312
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2596313
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x42551b92

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2596314
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onResume()V

    .line 2596315
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/ride/view/RideMapView;->b()V

    .line 2596316
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->k:LX/Ich;

    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->l:LX/6Zb;

    .line 2596317
    iget-object v4, v1, LX/Ich;->a:LX/0y3;

    invoke-virtual {v4}, LX/0y3;->a()LX/0yG;

    move-result-object v4

    sget-object v5, LX/0yG;->OKAY:LX/0yG;

    if-eq v4, v5, :cond_0

    .line 2596318
    iput-object p0, v1, LX/Ich;->b:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    .line 2596319
    new-instance v4, LX/2si;

    invoke-direct {v4}, LX/2si;-><init>()V

    const-string v5, "surface_messenger_ride_service"

    const-string v6, "mechanism_messenger_ride_service_button"

    invoke-virtual {v2, v4, v5, v6}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    .line 2596320
    :cond_0
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->z(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2596321
    invoke-virtual {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->b()V

    .line 2596322
    :cond_1
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->k(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596323
    const/16 v1, 0x2b

    const v2, 0x550cee9e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2596324
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2596325
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/business/ride/view/RideMapView;->b(Landroid/os/Bundle;)V

    .line 2596326
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->a(Landroid/os/Bundle;)V

    .line 2596327
    const-string v0, "payment_row_display_text"

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->I:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2596328
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2596329
    const-string v1, "location_params"

    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2596330
    const-string v1, "origin_location_params"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2596331
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2596332
    const-string v1, "location_params"

    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2596333
    const-string v1, "destination_location_params"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2596334
    const-string v0, "last_picked_payment_card"

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->L:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2596335
    const-string v0, "is_funnel_logging_started"

    iget-boolean v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2596336
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4853fb51

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2596337
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2596338
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onStart()V

    .line 2596339
    const/16 v1, 0x2b

    const v2, 0x769c70e4    # 1.5865E33f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2596340
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2596341
    const v0, 0x7f0d2a74

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/view/RideMapView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    .line 2596342
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    invoke-virtual {v0, p2}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a(Landroid/os/Bundle;)V

    .line 2596343
    const v0, 0x7f0d2a75

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    .line 2596344
    const v0, 0x7f0d2a79

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->C:Lcom/facebook/widget/text/BetterButton;

    .line 2596345
    const v0, 0x7f0d2a76

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->D:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    .line 2596346
    const v0, 0x7f0d2a77

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->E:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    .line 2596347
    const v0, 0x7f0d2a78

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->F:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    .line 2596348
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    invoke-virtual {v0, p2}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b(Landroid/os/Bundle;)V

    .line 2596349
    if-eqz p2, :cond_2

    .line 2596350
    const-string v0, "payment_row_display_text"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->I:Ljava/lang/String;

    .line 2596351
    const-string v0, "origin_location_params"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2596352
    if-eqz v0, :cond_0

    .line 2596353
    const-string v1, "location_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    .line 2596354
    :cond_0
    const-string v0, "destination_location_params"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2596355
    if-eqz v0, :cond_1

    .line 2596356
    const-string v1, "location_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    .line 2596357
    :cond_1
    const-string v0, "last_picked_payment_card"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->L:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 2596358
    const-string v0, "is_funnel_logging_started"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->u:Z

    .line 2596359
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->c:LX/Ibs;

    new-instance v1, LX/IdF;

    invoke-direct {v1, p0}, LX/IdF;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596360
    iput-object v1, v0, LX/Ibs;->f:LX/IdF;

    .line 2596361
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    new-instance v1, LX/IdE;

    invoke-direct {v1, p0}, LX/IdE;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596362
    iput-object v1, v0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->g:LX/IdD;

    .line 2596363
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->e:LX/Ic4;

    new-instance v1, LX/IdG;

    invoke-direct {v1, p0}, LX/IdG;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596364
    iput-object v1, v0, LX/Ic4;->m:LX/IdG;

    .line 2596365
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->f:LX/Ibu;

    new-instance v1, LX/IdH;

    invoke-direct {v1, p0}, LX/IdH;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596366
    iput-object v1, v0, LX/Ibu;->g:LX/IdH;

    .line 2596367
    const/4 v0, 0x0

    .line 2596368
    iput-boolean v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->A:Z

    .line 2596369
    iput-boolean v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->z:Z

    .line 2596370
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->z(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2596371
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideMapView;->a()V

    .line 2596372
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->A:Z

    .line 2596373
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->y:Lcom/facebook/messaging/business/ride/view/RideMapView;

    new-instance v1, LX/IdC;

    invoke-direct {v1, p0}, LX/IdC;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596374
    iput-object v1, v0, Lcom/facebook/messaging/business/ride/view/RideMapView;->h:LX/Ics;

    .line 2596375
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->J:Landroid/location/Location;

    if-nez v0, :cond_4

    .line 2596376
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    const v1, 0x7f082d7a

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->a(Ljava/lang/String;)V

    .line 2596377
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 2596378
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    sget-object v1, LX/Ib6;->ORIGIN:LX/Ib6;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;LX/Ib6;)V

    .line 2596379
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->D:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    new-instance v1, LX/IdJ;

    invoke-direct {v1, p0}, LX/IdJ;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596380
    iput-object v1, v0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->k:LX/Icw;

    .line 2596381
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 2596382
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->b:Ljava/lang/String;

    sget-object v1, LX/Ib6;->DESTINATION:LX/Ib6;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;LX/Ib6;)V

    .line 2596383
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->E:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    new-instance v1, LX/Id1;

    invoke-direct {v1, p0}, LX/Id1;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setupCancelButton(Landroid/view/View$OnClickListener;)V

    .line 2596384
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->E:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    new-instance v1, LX/Id2;

    invoke-direct {v1, p0}, LX/Id2;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596385
    iput-object v1, v0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->k:LX/Icw;

    .line 2596386
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->u()V

    .line 2596387
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->C:Lcom/facebook/widget/text/BetterButton;

    new-instance v1, LX/IdI;

    invoke-direct {v1, p0}, LX/IdI;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2596388
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596389
    iget-boolean v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->u:Z

    if-nez v2, :cond_9

    .line 2596390
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->u:Z

    .line 2596391
    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->g:LX/Ib2;

    .line 2596392
    iget-object v3, v2, LX/Ib2;->b:LX/0if;

    sget-object v4, LX/0ig;->o:LX/0ih;

    invoke-virtual {v3, v4}, LX/0if;->a(LX/0ih;)V

    .line 2596393
    iget-object v3, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2596394
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2596395
    iget-object v3, v2, LX/Ib2;->b:LX/0if;

    sget-object v4, LX/0ig;->o:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "PROVIDER_ID:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2596396
    iget-object v6, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    move-object v6, v6

    .line 2596397
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2596398
    :cond_5
    iget-object v3, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2596399
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 2596400
    iget-object v3, v2, LX/Ib2;->b:LX/0if;

    sget-object v4, LX/0ig;->o:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TRIGGER_SOURCE:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2596401
    iget-object v6, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2596402
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2596403
    :cond_6
    iget-object v3, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->g:Ljava/lang/String;

    move-object v3, v3

    .line 2596404
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 2596405
    iget-object v3, v2, LX/Ib2;->b:LX/0if;

    sget-object v4, LX/0ig;->o:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TRIGGER_SOURCE_TAG:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2596406
    iget-object v6, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->g:Ljava/lang/String;

    move-object v6, v6

    .line 2596407
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2596408
    :cond_7
    iget-object v3, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v3, v3

    .line 2596409
    if-eqz v3, :cond_8

    .line 2596410
    iget-object v3, v2, LX/Ib2;->b:LX/0if;

    sget-object v4, LX/0ig;->o:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "THREAD_ID:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2596411
    iget-object v6, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v6, v6

    .line 2596412
    invoke-virtual {v6}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2596413
    iget-object v3, v2, LX/Ib2;->b:LX/0if;

    sget-object v4, LX/0ig;->o:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "THREAD_TYPE:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2596414
    iget-object v6, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v6, v6

    .line 2596415
    iget-object v6, v6, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v6}, LX/5e9;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2596416
    :cond_8
    iget-object v3, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2596417
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 2596418
    iget-object v3, v2, LX/Ib2;->b:LX/0if;

    sget-object v4, LX/0ig;->o:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "MESSAGE_ID:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2596419
    iget-object v6, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2596420
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2596421
    :cond_9
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->z(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2596422
    :goto_2
    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596423
    iget-object v3, v2, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->h:Lcom/facebook/location/Coordinates;

    move-object v2, v3

    .line 2596424
    if-nez v2, :cond_f

    .line 2596425
    const/4 v2, 0x0

    .line 2596426
    :goto_3
    move v0, v2

    .line 2596427
    if-nez v0, :cond_a

    .line 2596428
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->e()V

    .line 2596429
    :cond_a
    const/4 v5, 0x0

    .line 2596430
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->p:LX/0Uh;

    const/16 v1, 0x161

    invoke-virtual {v0, v1, v5}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596431
    iget-object v1, v0, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->i:Ljava/lang/String;

    move-object v0, v1

    .line 2596432
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "native_sign_up"

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596433
    iget-object v2, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2596434
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 2596435
    :cond_b
    :goto_4
    return-void

    .line 2596436
    :cond_c
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->D:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    const v1, 0x7f082d7d

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setActionText(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2596437
    :cond_d
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->E:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    const v1, 0x7f082d7e

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setActionText(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2596438
    :cond_e
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2596439
    const-string v1, "extra_permissions"

    sget-object v2, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->s:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2596440
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->h:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x3

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_2

    .line 2596441
    :cond_f
    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596442
    iget-object v3, v2, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2596443
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_10

    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596444
    iget-object v3, v2, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2596445
    :goto_5
    sget-object v3, LX/Ib6;->DESTINATION:LX/Ib6;

    invoke-static {p0, v2, v3}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;LX/Ib6;)V

    .line 2596446
    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596447
    iget-object v3, v2, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->h:Lcom/facebook/location/Coordinates;

    move-object v2, v3

    .line 2596448
    iget-wide v4, v2, Lcom/facebook/location/Coordinates;->a:D

    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596449
    iget-object v3, v2, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->h:Lcom/facebook/location/Coordinates;

    move-object v2, v3

    .line 2596450
    iget-wide v6, v2, Lcom/facebook/location/Coordinates;->b:D

    const/4 v8, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->b$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DDLjava/lang/String;)V

    .line 2596451
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->k(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596452
    const/4 v2, 0x1

    goto/16 :goto_3

    .line 2596453
    :cond_10
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082d8c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 2596454
    :cond_11
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082d62

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082d63

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596455
    iget-object v6, v4, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->f:Ljava/lang/String;

    move-object v4, v6

    .line 2596456
    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082d64

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Id9;

    invoke-direct {v2, p0}, LX/Id9;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080017

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Id8;

    invoke-direct {v2, p0}, LX/Id8;-><init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto/16 :goto_4
.end method
