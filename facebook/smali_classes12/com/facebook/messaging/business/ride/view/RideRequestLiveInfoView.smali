.class public Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/widget/ProgressBar;

.field private b:Landroid/widget/LinearLayout;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2596674
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2596675
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->e()V

    .line 2596676
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2596677
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2596678
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->e()V

    .line 2596679
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2596680
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2596681
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->e()V

    .line 2596682
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2596653
    const v0, 0x7f031225

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2596654
    const v0, 0x7f0d2a7a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->a:Landroid/widget/ProgressBar;

    .line 2596655
    const v0, 0x7f0d2a7b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->b:Landroid/widget/LinearLayout;

    .line 2596656
    const v0, 0x7f0d2a7c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2596657
    const v0, 0x7f0d2a7d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2596658
    const v0, 0x7f0d2a7e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2596659
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2596668
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2596669
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->b:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2596670
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2596671
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2596672
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->b:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2596673
    return-void
.end method

.method public setDriverInfoText(Landroid/text/SpannableStringBuilder;)V
    .locals 1

    .prologue
    .line 2596666
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2596667
    return-void
.end method

.method public setDriverInfoText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2596664
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2596665
    return-void
.end method

.method public setEtaText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2596662
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2596663
    return-void
.end method

.method public setVehicleInfoText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2596660
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2596661
    return-void
.end method
