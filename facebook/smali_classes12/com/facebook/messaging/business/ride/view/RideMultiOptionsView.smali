.class public Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field private final e:Landroid/widget/LinearLayout;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/widget/ImageView;

.field private final h:Lcom/facebook/widget/text/BetterTextView;

.field private final i:Lcom/facebook/widget/text/BetterTextView;

.field private final j:Landroid/widget/ProgressBar;

.field public k:LX/Icw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2595910
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2595911
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2595975
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2595976
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2595956
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2595957
    const v0, 0x7f031220

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2595958
    const v0, 0x7f0d2a5f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->a:Landroid/widget/ImageView;

    .line 2595959
    const v0, 0x7f0d2a60

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2595960
    const v0, 0x7f0d2a61

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2595961
    const v0, 0x7f0d2a62

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->d:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2595962
    const v0, 0x7f0d2a63

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->e:Landroid/widget/LinearLayout;

    .line 2595963
    const v0, 0x7f0d2a67

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->f:Landroid/widget/ImageView;

    .line 2595964
    const v0, 0x7f0d2a68

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->g:Landroid/widget/ImageView;

    .line 2595965
    const v0, 0x7f0d2a65

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2595966
    const v0, 0x7f0d2a66

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2595967
    const v0, 0x7f0d2a69

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->j:Landroid/widget/ProgressBar;

    .line 2595968
    sget-object v0, LX/03r;->RideView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2595969
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2595970
    if-lez v1, :cond_0

    .line 2595971
    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->a:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2595972
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2595973
    return-void

    .line 2595974
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->a:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2595951
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->j:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2595952
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2595953
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2595954
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->d:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2595955
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2595949
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->i:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2595950
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2595946
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->d()V

    .line 2595947
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->j:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2595948
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2595944
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->f:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2595945
    return-void
.end method

.method public setActionText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2595938
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2595939
    :goto_0
    return-void

    .line 2595940
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->d()V

    .line 2595941
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2595942
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->b:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/Ict;

    invoke-direct {v1, p0}, LX/Ict;-><init>(Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2595943
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setIconTintColor(I)V
    .locals 1

    .prologue
    .line 2595936
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 2595937
    return-void
.end method

.method public setListener(LX/Icw;)V
    .locals 0

    .prologue
    .line 2595934
    iput-object p1, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->k:LX/Icw;

    .line 2595935
    return-void
.end method

.method public setRequirementText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2595929
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2595930
    :goto_0
    return-void

    .line 2595931
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->d()V

    .line 2595932
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2595933
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->c:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSelectedItem(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2595920
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2595921
    :goto_0
    return-void

    .line 2595922
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->d()V

    .line 2595923
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->i:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2595924
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2595925
    new-instance v0, LX/Icu;

    invoke-direct {v0, p0}, LX/Icu;-><init>(Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;)V

    .line 2595926
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2595927
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2595928
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->d:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSelectedSubText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2595916
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2595917
    :goto_0
    return-void

    .line 2595918
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->i:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2595919
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setupCancelButton(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2595912
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->f:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2595913
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->g:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2595914
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->g:Landroid/widget/ImageView;

    new-instance v1, LX/Icv;

    invoke-direct {v1, p0, p1}, LX/Icv;-><init>(Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2595915
    return-void
.end method
