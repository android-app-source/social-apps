.class public Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2596683
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2596684
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->e()V

    .line 2596685
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2596686
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2596687
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->e()V

    .line 2596688
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2596689
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2596690
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->e()V

    .line 2596691
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 2596692
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->setVisibility(I)V

    .line 2596693
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2596694
    const v0, 0x7f031226

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2596695
    const v0, 0x7f0d2a7f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2596696
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2596697
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2596698
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2596699
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2596700
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2596701
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->setVisibility(I)V

    .line 2596702
    return-void
.end method

.method public setDialogText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2596703
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2596704
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;->d()V

    .line 2596705
    return-void
.end method
