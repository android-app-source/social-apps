.class public final Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""

# interfaces
.implements LX/6ZZ;


# instance fields
.field public a:LX/IcY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0y3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Ljava/lang/String;

.field public f:Lcom/facebook/maps/FbMapViewDelegate;

.field public g:Lcom/facebook/uicontrib/fab/FabView;

.field private h:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

.field private i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

.field private j:LX/IcX;

.field public k:LX/0i5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2595744
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2595745
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;

    const-class v1, LX/IcY;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/IcY;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v2

    check-cast v2, LX/0y3;

    const-class v3, LX/0i4;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/0i4;

    invoke-static {p0}, LX/6Zb;->b(LX/0QB;)LX/6Zb;

    move-result-object p0

    check-cast p0, LX/6Zb;

    iput-object v1, p1, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->a:LX/IcY;

    iput-object v2, p1, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->b:LX/0y3;

    iput-object v3, p1, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->c:LX/0i4;

    iput-object p0, p1, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->d:LX/6Zb;

    return-void
.end method

.method public static b$redex0(Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;)V
    .locals 2

    .prologue
    .line 2595747
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->f:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v1, LX/Ica;

    invoke-direct {v1, p0}, LX/Ica;-><init>(Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;)V

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2595748
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2595746
    const v0, 0x7f082d92

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/6ZY;)V
    .locals 2

    .prologue
    .line 2595737
    sget-object v0, LX/Ice;->a:[I

    invoke-virtual {p1}, LX/6ZY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2595738
    :goto_0
    return-void

    .line 2595739
    :pswitch_0
    invoke-static {p0}, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->b$redex0(Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/IZ9;)V
    .locals 0

    .prologue
    .line 2595732
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2595740
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2595741
    const-class v0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2595742
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->d:LX/6Zb;

    invoke-virtual {v0, p0, p0}, LX/6Zb;->a(Lcom/facebook/base/fragment/FbFragment;LX/6ZZ;)V

    .line 2595743
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 2595733
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "ride_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->e:Ljava/lang/String;

    .line 2595734
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2595735
    return-void

    .line 2595736
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x46c993f3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2595731
    const v1, 0x7f03121e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1dc93b77

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x58ff1e2a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2595724
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onDestroy()V

    .line 2595725
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->j:LX/IcX;

    .line 2595726
    iget-object v2, v1, LX/IcX;->b:LX/IcJ;

    .line 2595727
    iget-object v4, v2, LX/IcJ;->c:LX/1Ck;

    const-string v1, "task_key_fetch_ride_request_info"

    invoke-virtual {v4, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2595728
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->f:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v1}, LX/6Zn;->a()V

    .line 2595729
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->d:LX/6Zb;

    invoke-virtual {v1}, LX/6Zb;->a()V

    .line 2595730
    const/16 v1, 0x2b

    const v2, 0x7ffefbab

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4f946daf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2595714
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onPause()V

    .line 2595715
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->f:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v1}, LX/6Zn;->c()V

    .line 2595716
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->j:LX/IcX;

    .line 2595717
    iget-object v2, v1, LX/IcX;->a:LX/Ibn;

    .line 2595718
    iget-object p0, v2, LX/Ibn;->b:LX/0gM;

    if-eqz p0, :cond_0

    .line 2595719
    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    .line 2595720
    iget-object v1, v2, LX/Ibn;->b:LX/0gM;

    invoke-interface {p0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2595721
    iget-object v1, v2, LX/Ibn;->a:LX/0gX;

    invoke-virtual {v1, p0}, LX/0gX;->a(Ljava/util/Set;)V

    .line 2595722
    const/4 p0, 0x0

    iput-object p0, v2, LX/Ibn;->b:LX/0gM;

    .line 2595723
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x3c2a86b2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1e674606

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2595703
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onResume()V

    .line 2595704
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->f:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v1}, LX/6Zn;->d()V

    .line 2595705
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->j:LX/IcX;

    .line 2595706
    iget-object v2, v1, LX/IcX;->a:LX/Ibn;

    iget-object v4, v1, LX/IcX;->l:Ljava/lang/String;

    iget-object v5, v1, LX/IcX;->f:LX/0TF;

    .line 2595707
    new-instance v6, LX/4JE;

    invoke-direct {v6}, LX/4JE;-><init>()V

    .line 2595708
    const-string p0, "ride_request_id"

    invoke-virtual {v6, p0, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595709
    move-object v6, v6

    .line 2595710
    new-instance p0, LX/IbJ;

    invoke-direct {p0}, LX/IbJ;-><init>()V

    move-object p0, p0

    .line 2595711
    const-string v1, "input"

    invoke-virtual {p0, v1, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2595712
    :try_start_0
    iget-object v6, v2, LX/Ibn;->a:LX/0gX;

    invoke-virtual {v6, p0, v5}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v6

    iput-object v6, v2, LX/Ibn;->b:LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    .line 2595713
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x40edbb97

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :catch_0
    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 2595673
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2595674
    const v0, 0x7f0d2a59

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->h:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    .line 2595675
    const v0, 0x7f0d2a58

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->g:Lcom/facebook/uicontrib/fab/FabView;

    .line 2595676
    const v0, 0x7f0d2a5a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    .line 2595677
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->c:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->k:LX/0i5;

    .line 2595678
    const v0, 0x7f0d2a57

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbMapViewDelegate;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->f:Lcom/facebook/maps/FbMapViewDelegate;

    .line 2595679
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->f:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, p2}, LX/6Zn;->a(Landroid/os/Bundle;)V

    .line 2595680
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->f:Lcom/facebook/maps/FbMapViewDelegate;

    new-instance v1, LX/IcZ;

    invoke-direct {v1, p0}, LX/IcZ;-><init>(Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;)V

    invoke-virtual {v0, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2595681
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->k:LX/0i5;

    sget-object v1, LX/3KE;->a:[Ljava/lang/String;

    new-instance v2, LX/Icb;

    invoke-direct {v2, p0}, LX/Icb;-><init>(Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2595682
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->a:LX/IcY;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->f:Lcom/facebook/maps/FbMapViewDelegate;

    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->h:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    iget-object v3, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;

    iget-object v4, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->e:Ljava/lang/String;

    .line 2595683
    new-instance v5, LX/IcX;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    move-object v9, v1

    move-object v10, v2

    move-object v11, v3

    move-object v12, v4

    invoke-direct/range {v5 .. v12}, LX/IcX;-><init>(Landroid/content/Context;LX/03V;Landroid/os/Handler;Lcom/facebook/maps/FbMapViewDelegate;Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;Lcom/facebook/messaging/business/ride/view/RideRequestLiveStatusDialogView;Ljava/lang/String;)V

    .line 2595684
    new-instance v7, LX/Ibn;

    invoke-static {v0}, LX/0gX;->a(LX/0QB;)LX/0gX;

    move-result-object v6

    check-cast v6, LX/0gX;

    invoke-direct {v7, v6}, LX/Ibn;-><init>(LX/0gX;)V

    .line 2595685
    move-object v6, v7

    .line 2595686
    check-cast v6, LX/Ibn;

    .line 2595687
    new-instance v10, LX/IcJ;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-direct {v10, v7, v8, v9}, LX/IcJ;-><init>(LX/03V;LX/0tX;LX/1Ck;)V

    .line 2595688
    move-object v7, v10

    .line 2595689
    check-cast v7, LX/IcJ;

    invoke-static {v0}, LX/6aE;->b(LX/0QB;)LX/6aE;

    move-result-object v8

    check-cast v8, LX/6aE;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v9

    check-cast v9, LX/0W3;

    .line 2595690
    iput-object v6, v5, LX/IcX;->a:LX/Ibn;

    iput-object v7, v5, LX/IcX;->b:LX/IcJ;

    iput-object v8, v5, LX/IcX;->c:LX/6aE;

    iput-object v9, v5, LX/IcX;->d:LX/0W3;

    .line 2595691
    move-object v0, v5

    .line 2595692
    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->j:LX/IcX;

    .line 2595693
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationMapFragment;->j:LX/IcX;

    .line 2595694
    iget-object v1, v0, LX/IcX;->b:LX/IcJ;

    iget-object v2, v0, LX/IcX;->l:Ljava/lang/String;

    iget-object v3, v0, LX/IcX;->o:LX/IcT;

    .line 2595695
    if-nez v3, :cond_0

    .line 2595696
    :goto_0
    return-void

    .line 2595697
    :cond_0
    iget-object v4, v3, LX/IcT;->a:LX/IcX;

    iget-object v4, v4, LX/IcX;->i:Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;

    invoke-virtual {v4}, Lcom/facebook/messaging/business/ride/view/RideRequestLiveInfoView;->a()V

    .line 2595698
    new-instance v4, LX/IbK;

    invoke-direct {v4}, LX/IbK;-><init>()V

    move-object v4, v4

    .line 2595699
    const-string v5, "id"

    invoke-virtual {v4, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/IbK;

    .line 2595700
    iget-object v5, v1, LX/IcJ;->c:LX/1Ck;

    const-string v6, "task_key_fetch_ride_request_info"

    iget-object v0, v1, LX/IcJ;->b:LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2595701
    new-instance v0, LX/IcI;

    invoke-direct {v0, v1, v3}, LX/IcI;-><init>(LX/IcJ;LX/IcT;)V

    move-object v0, v0

    .line 2595702
    invoke-virtual {v5, v6, v4, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
