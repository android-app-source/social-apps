.class public Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/widget/HorizontalScrollView;

.field public b:Landroid/widget/LinearLayout;

.field private c:Landroid/widget/ProgressBar;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field public e:I

.field public f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/IdD;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2596822
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2596823
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->e()V

    .line 2596824
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2596825
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2596826
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->e()V

    .line 2596827
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2596828
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2596829
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->e()V

    .line 2596830
    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;LX/0Px;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2596856
    move v1, v2

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2596857
    invoke-virtual {p0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->l()I

    move-result v3

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->l()I

    move-result v0

    if-ne v3, v0, :cond_0

    .line 2596858
    :goto_1
    return v1

    .line 2596859
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 2596860
    goto :goto_1
.end method

.method private b(Ljava/lang/String;LX/0Px;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2596831
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    if-nez v0, :cond_0

    .line 2596832
    invoke-static {p1, p2}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->c(Ljava/lang/String;LX/0Px;)Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    .line 2596833
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->e:I

    .line 2596834
    iget v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->e:I

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    .line 2596835
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->g:LX/IdD;

    if-eqz v0, :cond_1

    .line 2596836
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/IdM;

    .line 2596837
    invoke-virtual {v0}, LX/IdM;->b()V

    .line 2596838
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->g:LX/IdD;

    .line 2596839
    iget-object p0, v0, LX/IdM;->c:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    move-object v0, p0

    .line 2596840
    invoke-interface {v1, v0}, LX/IdD;->onClick(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;)V

    .line 2596841
    :cond_1
    return-void

    .line 2596842
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    invoke-static {v0, p2}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->a(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;LX/0Px;)I

    move-result v0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;LX/0Px;)Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;",
            ">;)",
            "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2596843
    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v2

    .line 2596844
    :goto_0
    return-object v0

    .line 2596845
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2596846
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2596847
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    goto :goto_0

    .line 2596848
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 2596849
    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2596850
    const v0, 0x7f03122a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2596851
    const v0, 0x7f0d2a88

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->a:Landroid/widget/HorizontalScrollView;

    .line 2596852
    const v0, 0x7f0d2a89

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b:Landroid/widget/LinearLayout;

    .line 2596853
    const v0, 0x7f0d2a8b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->c:Landroid/widget/ProgressBar;

    .line 2596854
    const v0, 0x7f0d2a8a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2596855
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2596803
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2596804
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->c:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2596805
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2596806
    return-void
.end method

.method private setUpTabs(LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2596807
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    .line 2596808
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 2596809
    if-ge v1, v0, :cond_0

    .line 2596810
    :goto_0
    if-ge v1, v0, :cond_1

    .line 2596811
    new-instance v2, LX/IdM;

    invoke-virtual {p0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3}, LX/IdM;-><init>(Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;Landroid/content/Context;)V

    .line 2596812
    iget-object v3, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2596813
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2596814
    :cond_0
    if-le v1, v0, :cond_1

    .line 2596815
    :goto_1
    if-ge v0, v1, :cond_1

    .line 2596816
    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2596817
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2596818
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2596819
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/IdM;

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    invoke-virtual {v0, v2, v1}, LX/IdM;->a(ILcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;)V

    .line 2596820
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2596821
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2596801
    new-instance v0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView$1;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView$1;-><init>(Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->post(Ljava/lang/Runnable;)Z

    .line 2596802
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2596798
    const-string v0, "last_selected_ride_type"

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2596799
    const-string v0, "last_selected_index"

    iget v1, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2596800
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 2596793
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2596794
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2596795
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2596796
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2596797
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Px;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2596787
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2596788
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082d79

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->a(Ljava/lang/String;)V

    .line 2596789
    :goto_0
    return-void

    .line 2596790
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f()V

    .line 2596791
    invoke-direct {p0, p2}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->setUpTabs(LX/0Px;)V

    .line 2596792
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b(Ljava/lang/String;LX/0Px;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2596783
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2596784
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->d:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2596785
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->c:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2596786
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2596779
    if-eqz p1, :cond_0

    .line 2596780
    const-string v0, "last_selected_ride_type"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    .line 2596781
    const-string v0, "last_selected_index"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->e:I

    .line 2596782
    :cond_0
    return-void
.end method

.method public getSelectedRideTypeModel()Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2596778
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    return-object v0
.end method

.method public setOnTabClickListener(LX/IdD;)V
    .locals 0

    .prologue
    .line 2596776
    iput-object p1, p0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->g:LX/IdD;

    .line 2596777
    return-void
.end method
