.class public final Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/android/maps/model/LatLng;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/6al;

.field public final synthetic d:Lcom/facebook/android/maps/model/LatLng;

.field public final synthetic e:Lcom/facebook/android/maps/model/LatLng;

.field public final synthetic f:LX/IcX;


# direct methods
.method public constructor <init>(LX/IcX;Lcom/facebook/android/maps/model/LatLng;Ljava/lang/String;LX/6al;Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;)V
    .locals 0

    .prologue
    .line 2595587
    iput-object p1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iput-object p2, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->a:Lcom/facebook/android/maps/model/LatLng;

    iput-object p3, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->c:LX/6al;

    iput-object p5, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->d:Lcom/facebook/android/maps/model/LatLng;

    iput-object p6, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->e:Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const-wide/16 v8, 0x0

    .line 2595570
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->a:Lcom/facebook/android/maps/model/LatLng;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->b:Ljava/lang/String;

    invoke-static {v0}, LX/IcX;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2595571
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v0, v0, LX/IcX;->e:LX/6ax;

    if-nez v0, :cond_4

    .line 2595572
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->c:LX/6al;

    const v3, 0x7f0207bb

    iget-object v4, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->a:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v1, v2, v3, v4}, LX/IcX;->a$redex0(LX/IcX;LX/6al;ILcom/facebook/android/maps/model/LatLng;)LX/6ax;

    move-result-object v1

    iput-object v1, v0, LX/IcX;->e:LX/6ax;

    .line 2595573
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->d:Lcom/facebook/android/maps/model/LatLng;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->d:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    cmpl-double v0, v0, v8

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->d:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v0, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    cmpl-double v0, v0, v8

    if-eqz v0, :cond_2

    .line 2595574
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->c:LX/6al;

    const v2, 0x7f021023

    iget-object v3, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->d:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v0, v1, v2, v3}, LX/IcX;->a$redex0(LX/IcX;LX/6al;ILcom/facebook/android/maps/model/LatLng;)LX/6ax;

    .line 2595575
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->e:Lcom/facebook/android/maps/model/LatLng;

    if-eqz v0, :cond_3

    .line 2595576
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->c:LX/6al;

    const v2, 0x7f020ff0

    iget-object v3, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->e:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v0, v1, v2, v3}, LX/IcX;->a$redex0(LX/IcX;LX/6al;ILcom/facebook/android/maps/model/LatLng;)LX/6ax;

    .line 2595577
    :cond_3
    return-void

    .line 2595578
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v0, v0, LX/IcX;->e:LX/6ax;

    invoke-virtual {v0}, LX/6ax;->a()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v0

    .line 2595579
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->a:Lcom/facebook/android/maps/model/LatLng;

    invoke-static {v0, v1}, LX/IcX;->b(Lcom/facebook/android/maps/model/LatLng;Lcom/facebook/android/maps/model/LatLng;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2595580
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v0, v0, LX/IcX;->e:LX/6ax;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->a:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0, v1}, LX/6ax;->a(Lcom/facebook/android/maps/model/LatLng;)V

    goto :goto_0

    .line 2595581
    :cond_5
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v1, v1, LX/IcX;->e:LX/6ax;

    invoke-virtual {v1}, LX/6ax;->c()V

    .line 2595582
    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v3, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->c:LX/6al;

    const v4, 0x7f0207bb

    invoke-static {v2, v3, v4, v0}, LX/IcX;->a$redex0(LX/IcX;LX/6al;ILcom/facebook/android/maps/model/LatLng;)LX/6ax;

    move-result-object v0

    iput-object v0, v1, LX/IcX;->e:LX/6ax;

    .line 2595583
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v0, v0, LX/IcX;->c:LX/6aE;

    iget-object v1, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v1, v1, LX/IcX;->e:LX/6ax;

    iget-object v2, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->a:Lcom/facebook/android/maps/model/LatLng;

    iget-object v3, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v3, v3, LX/IcX;->d:LX/0W3;

    sget-wide v6, LX/0X5;->id:J

    const/16 v4, 0x2ee0

    invoke-interface {v3, v6, v7, v4}, LX/0W4;->a(JI)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->c:LX/6al;

    invoke-virtual/range {v0 .. v5}, LX/6aE;->a(LX/6ax;Lcom/facebook/android/maps/model/LatLng;ILX/6al;LX/IJO;)V

    goto :goto_0

    .line 2595584
    :cond_6
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v0, v0, LX/IcX;->e:LX/6ax;

    if-eqz v0, :cond_0

    .line 2595585
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iget-object v0, v0, LX/IcX;->e:LX/6ax;

    invoke-virtual {v0}, LX/6ax;->c()V

    .line 2595586
    iget-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideCurrentLocationController$3;->f:LX/IcX;

    iput-object v5, v0, LX/IcX;->e:LX/6ax;

    goto/16 :goto_0
.end method
