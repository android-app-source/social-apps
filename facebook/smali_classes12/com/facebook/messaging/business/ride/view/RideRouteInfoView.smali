.class public Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2596706
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2596707
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;->a()V

    .line 2596708
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2596709
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2596710
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;->a()V

    .line 2596711
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2596712
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2596713
    invoke-direct {p0}, Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;->a()V

    .line 2596714
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2596715
    const v0, 0x7f031227

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2596716
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;->setOrientation(I)V

    .line 2596717
    const v0, 0x7f0d2a80

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2596718
    const v0, 0x7f0d2a81

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2596719
    return-void
.end method
