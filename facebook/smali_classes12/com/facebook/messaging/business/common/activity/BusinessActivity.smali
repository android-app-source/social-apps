.class public final Lcom/facebook/messaging/business/common/activity/BusinessActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/67X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/6LN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/8Br;
    .annotation runtime Lcom/facebook/messaging/connectivity/annotations/ConnectionStatusForBusinessActivity;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/IZC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/6LO;
    .annotation runtime Lcom/facebook/messaging/business/common/activity/ForBusinessBannerNotification;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/IZ7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/63L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

.field public x:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2588557
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    .locals 2
    .param p2    # Landroid/os/Parcelable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2588549
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/messaging/business/common/activity/BusinessActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2588550
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2588551
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2588552
    const-string v1, "fragment_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2588553
    if-eqz p2, :cond_0

    .line 2588554
    const-string v1, "fragment_params"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2588555
    :cond_0
    move-object v0, v0

    .line 2588556
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;

    invoke-static {v6}, LX/67X;->b(LX/0QB;)LX/67X;

    move-result-object v1

    check-cast v1, LX/67X;

    invoke-static {v6}, LX/6LN;->b(LX/0QB;)LX/6LN;

    move-result-object v2

    check-cast v2, LX/6LN;

    const-class v3, LX/8Bs;

    invoke-interface {v6, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/8Bs;

    invoke-static {v3}, LX/2MC;->c(LX/8Bs;)LX/8Br;

    move-result-object v3

    move-object v3, v3

    check-cast v3, LX/8Br;

    new-instance p1, LX/IZC;

    invoke-static {v6}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    invoke-static {v6}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    const-class p0, Landroid/content/Context;

    invoke-interface {v6, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-direct {p1, v4, v5, p0}, LX/IZC;-><init>(Landroid/view/LayoutInflater;Landroid/content/res/Resources;Landroid/content/Context;)V

    move-object v4, p1

    check-cast v4, LX/IZC;

    invoke-static {v6}, LX/IZB;->a(LX/0QB;)LX/IZB;

    move-result-object v5

    check-cast v5, LX/6LO;

    new-instance v7, LX/0U8;

    invoke-interface {v6}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object p0

    new-instance p1, LX/IZD;

    invoke-direct {p1, v6}, LX/IZD;-><init>(LX/0QB;)V

    invoke-direct {v7, p0, p1}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v7

    iput-object v1, v0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->p:LX/67X;

    iput-object v2, v0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->q:LX/6LN;

    iput-object v3, v0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->r:LX/8Br;

    iput-object v4, v0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->s:LX/IZC;

    iput-object v5, v0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->t:LX/6LO;

    iput-object v6, v0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->u:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2588545
    invoke-static {p0, p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2588546
    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->q:LX/6LN;

    iget-object v1, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->r:LX/8Br;

    iget-object v2, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->s:LX/IZC;

    invoke-static {v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->t:LX/6LO;

    invoke-virtual {v0, v1, v2}, LX/6LN;->a(Ljava/util/Set;LX/6LO;)V

    .line 2588547
    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->p:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2588548
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2588502
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2588503
    invoke-virtual {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2588504
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2588505
    const-string v1, "fragment_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2588506
    const-string v2, "fragment_params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 2588507
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2588508
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v3

    .line 2588509
    invoke-virtual {v3, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    iput-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    .line 2588510
    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 2588511
    :goto_0
    if-nez v0, :cond_1

    .line 2588512
    iget-object v4, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->u:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/IZ7;

    .line 2588513
    invoke-interface {v4}, LX/IZ7;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2588514
    invoke-interface {v4}, LX/IZ7;->b()Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    move-result-object v4

    .line 2588515
    :goto_1
    move-object v4, v4

    .line 2588516
    iput-object v4, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    .line 2588517
    :cond_1
    iget-object v4, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2588518
    iget-object v4, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    invoke-virtual {v4, p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->b(Landroid/app/Activity;)V

    .line 2588519
    const v4, 0x7f030209

    invoke-virtual {p0, v4}, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->setContentView(I)V

    .line 2588520
    if-nez v0, :cond_2

    .line 2588521
    invoke-virtual {v3}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v3, 0x7f0d0808

    iget-object v4, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    invoke-virtual {v0, v3, v4, v1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2588522
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    new-instance v1, LX/IZA;

    invoke-direct {v1, p0}, LX/IZA;-><init>(Lcom/facebook/messaging/business/common/activity/BusinessActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(LX/IZ9;)V

    .line 2588523
    if-eqz v2, :cond_3

    .line 2588524
    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Parcelable;)V

    .line 2588525
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    invoke-virtual {v0, p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2588526
    iput-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->x:Ljava/lang/String;

    .line 2588527
    iget-object v1, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->x:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2588528
    iget-object v1, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->p:LX/67X;

    invoke-virtual {v1}, LX/67X;->g()LX/3u1;

    move-result-object v1

    .line 2588529
    if-eqz v1, :cond_4

    .line 2588530
    invoke-virtual {v1}, LX/3u1;->d()V

    .line 2588531
    :cond_4
    :goto_2
    const v0, 0x7f0d0807

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2588532
    iget-object v1, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->q:LX/6LN;

    .line 2588533
    iput-object v0, v1, LX/6LN;->h:Landroid/view/ViewGroup;

    .line 2588534
    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    invoke-virtual {v0, p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->c(Landroid/app/Activity;)V

    .line 2588535
    return-void

    .line 2588536
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    .line 2588537
    :cond_7
    iget-object v1, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->p:LX/67X;

    invoke-virtual {v1}, LX/67X;->g()LX/3u1;

    move-result-object v1

    .line 2588538
    if-eqz v1, :cond_8

    .line 2588539
    invoke-virtual {v1}, LX/3u1;->c()V

    .line 2588540
    new-instance v2, LX/63L;

    invoke-direct {v2, p0, v1}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    iput-object v2, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->v:LX/63L;

    .line 2588541
    iget-object v2, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->v:LX/63L;

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, LX/63L;->setHasBackButton(Z)V

    .line 2588542
    iget-object v2, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->v:LX/63L;

    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->x:Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/63L;->setTitle(Ljava/lang/String;)V

    .line 2588543
    iget-object v2, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    invoke-virtual {v2, v1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(LX/3u1;)V

    .line 2588544
    :cond_8
    goto :goto_2
.end method

.method public final finish()V
    .locals 1

    .prologue
    .line 2588499
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2588500
    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    invoke-virtual {v0, p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->d(Landroid/app/Activity;)V

    .line 2588501
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2588495
    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    instance-of v0, v0, LX/0fj;

    if-eqz v0, :cond_0

    .line 2588496
    iget-object v0, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    .line 2588497
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2588498
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 2588490
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 2588491
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 2588492
    iget-object v1, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->v:LX/63L;

    if-eqz v1, :cond_0

    .line 2588493
    iget-object v1, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->v:LX/63L;

    invoke-virtual {v1, p1}, LX/63L;->a(Landroid/view/Menu;)V

    .line 2588494
    :cond_0
    return v0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2588485
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x102002c

    if-ne v2, v3, :cond_2

    .line 2588486
    iget-object v2, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->w:Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;

    invoke-virtual {v2, p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/app/Activity;)V

    move v2, v1

    .line 2588487
    :goto_0
    if-nez v2, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    .line 2588488
    :cond_2
    iget-object v2, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->v:LX/63L;

    if-eqz v2, :cond_3

    .line 2588489
    iget-object v2, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->v:LX/63L;

    invoke-virtual {v2, p1}, LX/63L;->a(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x7da547d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2588482
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2588483
    iget-object v1, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->q:LX/6LN;

    invoke-virtual {v1}, LX/6LN;->b()V

    .line 2588484
    const/16 v1, 0x23

    const v2, -0x5ba8a9d1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x3d4f28a2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2588479
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2588480
    iget-object v1, p0, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->q:LX/6LN;

    invoke-virtual {v1}, LX/6LN;->a()V

    .line 2588481
    const/16 v1, 0x23

    const v2, -0x63e17b99

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
