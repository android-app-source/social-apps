.class public final Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2589139
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2589140
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2589141
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2589142
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2589143
    if-nez p1, :cond_0

    .line 2589144
    :goto_0
    return v0

    .line 2589145
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2589146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2589147
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2589148
    const v2, -0x6e67195a

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 2589149
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2589150
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2589151
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2589152
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2589153
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2589154
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2589155
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2589156
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2589157
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2589158
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 2589159
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2589160
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2589161
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2589162
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2589163
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 2589164
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 2589165
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2589166
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2589167
    const v2, -0x2eb9792c

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 2589168
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 2589169
    const v3, -0xec24f9d

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2589170
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2589171
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2589172
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2589173
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2589174
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2589175
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2589176
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2589177
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2589178
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2589179
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2589180
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2589181
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2589182
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2589183
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6e67195a -> :sswitch_1
        -0x3d1c3943 -> :sswitch_3
        -0x2eb9792c -> :sswitch_4
        -0xec24f9d -> :sswitch_5
        0x26f8adf1 -> :sswitch_0
        0x72611992 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2589184
    new-instance v0, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2589185
    sparse-switch p2, :sswitch_data_0

    .line 2589186
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2589187
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2589188
    const v1, -0x6e67195a

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2589189
    :goto_0
    :sswitch_1
    return-void

    .line 2589190
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2589191
    const v1, -0x2eb9792c

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2589192
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2589193
    const v1, -0xec24f9d

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6e67195a -> :sswitch_1
        -0x3d1c3943 -> :sswitch_2
        -0x2eb9792c -> :sswitch_1
        -0xec24f9d -> :sswitch_1
        0x26f8adf1 -> :sswitch_0
        0x72611992 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2589194
    if-nez p1, :cond_0

    move v0, v1

    .line 2589195
    :goto_0
    return v0

    .line 2589196
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 2589197
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2589198
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2589199
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 2589200
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 2589201
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2589202
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2589203
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2589204
    if-eqz p1, :cond_0

    .line 2589205
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 2589206
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2589207
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 2589208
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2589209
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2589210
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2589211
    if-eqz p1, :cond_0

    .line 2589212
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;

    move-result-object v1

    .line 2589213
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;

    .line 2589214
    if-eq v0, v1, :cond_0

    .line 2589215
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2589216
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2589136
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2589137
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2589138
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2589108
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2589109
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2589110
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->a:LX/15i;

    .line 2589111
    iput p2, p0, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->b:I

    .line 2589112
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2589113
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2589114
    new-instance v0, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2589115
    iget v0, p0, LX/1vt;->c:I

    .line 2589116
    move v0, v0

    .line 2589117
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2589118
    iget v0, p0, LX/1vt;->c:I

    .line 2589119
    move v0, v0

    .line 2589120
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2589121
    iget v0, p0, LX/1vt;->b:I

    .line 2589122
    move v0, v0

    .line 2589123
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2589124
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2589125
    move-object v0, v0

    .line 2589126
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2589127
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2589128
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2589129
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2589130
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2589131
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2589132
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2589133
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2589134
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2589135
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2589105
    iget v0, p0, LX/1vt;->c:I

    .line 2589106
    move v0, v0

    .line 2589107
    return v0
.end method
