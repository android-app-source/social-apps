.class public final Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessGreetingContentsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2588798
    const-class v0, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessGreetingContentsQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessGreetingContentsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessGreetingContentsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2588799
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2588800
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2588801
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2588802
    const/4 v2, 0x0

    .line 2588803
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 2588804
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2588805
    :goto_0
    move v1, v2

    .line 2588806
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2588807
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2588808
    new-instance v1, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessGreetingContentsQueryModel;

    invoke-direct {v1}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessGreetingContentsQueryModel;-><init>()V

    .line 2588809
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2588810
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2588811
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2588812
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2588813
    :cond_0
    return-object v1

    .line 2588814
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2588815
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 2588816
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2588817
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2588818
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_2

    if-eqz v5, :cond_2

    .line 2588819
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2588820
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_1

    .line 2588821
    :cond_4
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2588822
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2588823
    :cond_5
    const-string v6, "pages_greeting"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2588824
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2588825
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v7, :cond_f

    .line 2588826
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2588827
    :goto_2
    move v1, v5

    .line 2588828
    goto :goto_1

    .line 2588829
    :cond_6
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2588830
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2588831
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2588832
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2588833
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_1

    .line 2588834
    :cond_8
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_c

    .line 2588835
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2588836
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2588837
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_8

    if-eqz v11, :cond_8

    .line 2588838
    const-string p0, "is_enabled"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2588839
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v7

    move v10, v7

    move v7, v6

    goto :goto_3

    .line 2588840
    :cond_9
    const-string p0, "is_feature_visible"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 2588841
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v9, v1

    move v1, v6

    goto :goto_3

    .line 2588842
    :cond_a
    const-string p0, "processed_message"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 2588843
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_3

    .line 2588844
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2588845
    :cond_c
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 2588846
    if-eqz v7, :cond_d

    .line 2588847
    invoke-virtual {v0, v5, v10}, LX/186;->a(IZ)V

    .line 2588848
    :cond_d
    if-eqz v1, :cond_e

    .line 2588849
    invoke-virtual {v0, v6, v9}, LX/186;->a(IZ)V

    .line 2588850
    :cond_e
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2588851
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_2

    :cond_f
    move v1, v5

    move v7, v5

    move v8, v5

    move v9, v5

    move v10, v5

    goto :goto_3
.end method
