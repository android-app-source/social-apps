.class public final Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessFAQContentsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessFAQContentsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2588752
    const-class v0, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessFAQContentsQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessFAQContentsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessFAQContentsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2588753
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2588718
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessFAQContentsQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2588720
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2588721
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 2588722
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2588723
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2588724
    if-eqz v2, :cond_0

    .line 2588725
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2588726
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2588727
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2588728
    if-eqz v2, :cond_4

    .line 2588729
    const-string v3, "commerce_page_faq_contents"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2588730
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2588731
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2588732
    if-eqz v3, :cond_3

    .line 2588733
    const-string v4, "content"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2588734
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2588735
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_2

    .line 2588736
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 2588737
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2588738
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2588739
    if-eqz v2, :cond_1

    .line 2588740
    const-string p2, "text"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2588741
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2588742
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2588743
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2588744
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2588745
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2588746
    :cond_4
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2588747
    if-eqz v2, :cond_5

    .line 2588748
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2588749
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2588750
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2588751
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2588719
    check-cast p1, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessFAQContentsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessFAQContentsQueryModel$Serializer;->a(Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessFAQContentsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
