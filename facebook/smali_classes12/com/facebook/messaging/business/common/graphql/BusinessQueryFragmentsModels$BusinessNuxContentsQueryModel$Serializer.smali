.class public final Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessNuxContentsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessNuxContentsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2589059
    const-class v0, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessNuxContentsQueryModel;

    new-instance v1, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessNuxContentsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessNuxContentsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2589060
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2589058
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessNuxContentsQueryModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2589016
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2589017
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x0

    .line 2589018
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2589019
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 2589020
    if-eqz v2, :cond_0

    .line 2589021
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2589022
    invoke-static {v1, v0, v3, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2589023
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2589024
    if-eqz v2, :cond_6

    .line 2589025
    const-string v3, "commerce_page_nux_contents"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2589026
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2589027
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2589028
    if-eqz v3, :cond_3

    .line 2589029
    const-string v4, "nux_content_items"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2589030
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2589031
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 2589032
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 2589033
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2589034
    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2589035
    if-eqz p0, :cond_1

    .line 2589036
    const-string p2, "text"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2589037
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2589038
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2589039
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2589040
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2589041
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2589042
    if-eqz v3, :cond_5

    .line 2589043
    const-string v4, "title"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2589044
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2589045
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2589046
    if-eqz v4, :cond_4

    .line 2589047
    const-string v5, "text"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2589048
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2589049
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2589050
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2589051
    :cond_6
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2589052
    if-eqz v2, :cond_7

    .line 2589053
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2589054
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2589055
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2589056
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2589057
    check-cast p1, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessNuxContentsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessNuxContentsQueryModel$Serializer;->a(Lcom/facebook/messaging/business/common/graphql/BusinessQueryFragmentsModels$BusinessNuxContentsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
