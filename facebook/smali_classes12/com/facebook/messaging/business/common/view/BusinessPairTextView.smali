.class public Lcom/facebook/messaging/business/common/view/BusinessPairTextView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private d:LX/IZT;

.field private e:Z

.field private f:Z

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2589339
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2589340
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2589337
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2589338
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2589320
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2589321
    const v0, 0x7f030215

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2589322
    iput-object p1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->a:Landroid/content/Context;

    .line 2589323
    const v0, 0x7f0d082d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2589324
    const v0, 0x7f0d082e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2589325
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2589326
    sget-object v0, LX/03r;->BusinessView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2589327
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2589328
    if-eqz v1, :cond_0

    .line 2589329
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->setTitle(Ljava/lang/String;)V

    .line 2589330
    :cond_0
    invoke-static {}, LX/IZT;->values()[LX/IZT;

    move-result-object v1

    const/16 v2, 0x3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->d:LX/IZT;

    .line 2589331
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->g:I

    .line 2589332
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->e:Z

    .line 2589333
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->f:Z

    .line 2589334
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2589335
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->a()V

    .line 2589336
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2589310
    iget-boolean v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->e:Z

    if-nez v0, :cond_0

    const v0, 0x7f0e07dd

    :goto_0
    iput v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->h:I

    .line 2589311
    iget-boolean v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->e:Z

    if-nez v0, :cond_1

    const v0, 0x7f0e07de

    :goto_1
    iput v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->i:I

    .line 2589312
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->d:LX/IZT;

    sget-object v1, LX/IZT;->VERTICAL:LX/IZT;

    if-ne v0, v1, :cond_2

    .line 2589313
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->c()V

    .line 2589314
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->g:I

    invoke-virtual {v0, v2, v1, v2, v2}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2589315
    :goto_2
    return-void

    .line 2589316
    :cond_0
    const v0, 0x7f0e07df

    goto :goto_0

    .line 2589317
    :cond_1
    const v0, 0x7f0e07e0

    goto :goto_1

    .line 2589318
    :cond_2
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->d()V

    .line 2589319
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->g:I

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    goto :goto_2
.end method

.method private b()V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 2589305
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2589306
    iget-object v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2589307
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2589308
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2589309
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2589298
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->a:Landroid/content/Context;

    iget v2, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->h:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2589299
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->a:Landroid/content/Context;

    iget v2, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2589300
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2589301
    const/4 v1, 0x3

    const v2, 0x7f0d082d

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2589302
    iget-boolean v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->f:Z

    if-eqz v0, :cond_0

    .line 2589303
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->b()V

    .line 2589304
    :cond_0
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2589285
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->a:Landroid/content/Context;

    iget v2, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2589286
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->a:Landroid/content/Context;

    iget v2, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2589287
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2589288
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2589289
    return-void
.end method


# virtual methods
.method public setText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2589296
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p1, "-"

    :cond_0
    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2589297
    return-void
.end method

.method public setTextStyle(I)V
    .locals 2

    .prologue
    .line 2589294
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2589295
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2589292
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2589293
    return-void
.end method

.method public setTitleStyle(I)V
    .locals 2

    .prologue
    .line 2589290
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2589291
    return-void
.end method
