.class public Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/IZR;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/IZV;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2589399
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2589400
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->b:Ljava/util/ArrayList;

    .line 2589401
    iput-object p1, p0, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->a:Landroid/content/Context;

    .line 2589402
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2589403
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2589404
    if-nez p2, :cond_0

    .line 2589405
    const v1, 0x7f03020a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2589406
    new-instance v0, LX/IZW;

    invoke-direct {v0, v1}, LX/IZW;-><init>(Landroid/view/View;)V

    .line 2589407
    :goto_0
    return-object v0

    .line 2589408
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 2589409
    new-instance v0, LX/IZX;

    .line 2589410
    new-instance v1, Landroid/widget/Space;

    iget-object v2, p0, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    .line 2589411
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    iget-object p1, p0, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0b12fa

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    invoke-direct {v2, v3, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/Space;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2589412
    move-object v1, v1

    .line 2589413
    invoke-direct {v0, v1}, LX/IZX;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2589414
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view type for creating view holder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2589371
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2589372
    packed-switch v0, :pswitch_data_0

    .line 2589373
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid view type for binding view holder."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2589374
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->b:Ljava/util/ArrayList;

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IZR;

    .line 2589375
    check-cast p1, LX/IZW;

    .line 2589376
    iget-object v1, v0, LX/IZR;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2589377
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2589378
    iget-object v2, p1, LX/IZW;->m:Lcom/facebook/widget/text/BetterTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2589379
    iget-object v2, p1, LX/IZW;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2589380
    :goto_0
    iget-object v1, v0, LX/IZR;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2589381
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2589382
    iget-object v2, p1, LX/IZW;->n:Lcom/facebook/widget/text/BetterTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2589383
    iget-object v2, p1, LX/IZW;->n:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2589384
    :goto_1
    iget-object v1, v0, LX/IZR;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2589385
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2589386
    iget-object v2, p1, LX/IZW;->o:Lcom/facebook/widget/text/BetterTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2589387
    iget-object v2, p1, LX/IZW;->o:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2589388
    :goto_2
    iget-object v1, v0, LX/IZR;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2589389
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2589390
    iget-object v2, p1, LX/IZW;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-class p2, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;

    invoke-static {p2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p2

    invoke-virtual {v2, v3, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2589391
    :cond_0
    iget-object v1, v0, LX/IZR;->b:Landroid/graphics/drawable/Drawable;

    move-object v1, v1

    .line 2589392
    if-eqz v1, :cond_1

    .line 2589393
    iget-object v2, p1, LX/IZW;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v2

    check-cast v2, LX/1af;

    invoke-virtual {v2, v1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2589394
    :cond_1
    iget-object v1, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v2, LX/IZU;

    invoke-direct {v2, p0, v0}, LX/IZU;-><init>(Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;LX/IZR;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2589395
    :pswitch_1
    return-void

    .line 2589396
    :cond_2
    iget-object v2, p1, LX/IZW;->m:Lcom/facebook/widget/text/BetterTextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 2589397
    :cond_3
    iget-object v2, p1, LX/IZW;->n:Lcom/facebook/widget/text/BetterTextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1

    .line 2589398
    :cond_4
    iget-object v2, p1, LX/IZW;->o:Lcom/facebook/widget/text/BetterTextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2589367
    if-eqz p1, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2589368
    if-eqz v0, :cond_1

    .line 2589369
    const/4 v0, 0x1

    .line 2589370
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2589366
    iget-object v0, p0, Lcom/facebook/messaging/business/common/view/adapters/BusinessBottomSheetAdapter;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method
