.class public final Lcom/facebook/messaging/photos/editing/StickerLayerPresenter$2$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/Irz;


# direct methods
.method public constructor <init>(LX/Irz;I)V
    .locals 0

    .prologue
    .line 2619122
    iput-object p1, p0, Lcom/facebook/messaging/photos/editing/StickerLayerPresenter$2$1;->b:LX/Irz;

    iput p2, p0, Lcom/facebook/messaging/photos/editing/StickerLayerPresenter$2$1;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2619123
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerLayerPresenter$2$1;->b:LX/Irz;

    iget-object v0, v0, LX/Irz;->b:LX/Is1;

    iget-object v0, v0, LX/Is1;->i:LX/Irl;

    if-eqz v0, :cond_2

    .line 2619124
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerLayerPresenter$2$1;->b:LX/Irz;

    iget-object v0, v0, LX/Irz;->b:LX/Is1;

    iget-object v0, v0, LX/Is1;->i:LX/Irl;

    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/StickerLayerPresenter$2$1;->b:LX/Irz;

    iget-object v1, v1, LX/Irz;->b:LX/Is1;

    iget-object v1, v1, LX/Is1;->d:LX/Irx;

    iget v2, p0, Lcom/facebook/messaging/photos/editing/StickerLayerPresenter$2$1;->a:I

    .line 2619125
    const/4 v3, 0x0

    :goto_0
    iget-object v4, v0, LX/Irl;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v4, v4, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v4}, LX/Ird;->h()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 2619126
    iget-object v4, v0, LX/Irl;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v4, v4, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v4, v3}, LX/Ird;->a(I)LX/Iqg;

    move-result-object v4

    .line 2619127
    instance-of v5, v4, LX/IsC;

    if-nez v5, :cond_0

    instance-of v5, v4, LX/Iqh;

    if-eqz v5, :cond_1

    .line 2619128
    :cond_0
    iget-object v5, v4, LX/Iqg;->j:Lcom/facebook/messaging/montage/model/art/ArtItem;

    move-object v5, v5

    .line 2619129
    iget-object p0, v1, LX/Iqg;->j:Lcom/facebook/messaging/montage/model/art/ArtItem;

    move-object p0, p0

    .line 2619130
    if-ne v5, p0, :cond_1

    .line 2619131
    iput v2, v4, LX/Iqg;->n:I

    .line 2619132
    sget-object v5, LX/Iqn;->DYNAMIC_COLOR_CHANGED:LX/Iqn;

    invoke-virtual {v4, v5}, LX/Iqg;->a(Ljava/lang/Object;)V

    .line 2619133
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2619134
    :cond_2
    return-void
.end method
