.class public Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/widget/ImageButton;

.field private c:Landroid/widget/ImageButton;

.field private d:Landroid/widget/ImageButton;

.field private e:Landroid/widget/ImageButton;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageButton;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/graphics/drawable/Drawable;

.field public h:LX/IrE;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2618073
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2618074
    invoke-direct {p0}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->a()V

    .line 2618075
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2618070
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2618071
    invoke-direct {p0}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->a()V

    .line 2618072
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2618067
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2618068
    invoke-direct {p0}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->a()V

    .line 2618069
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2618052
    const-class v0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    invoke-static {v0, p0}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2618053
    const v0, 0x7f030394

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2618054
    const v0, 0x7f0d0b7a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->b:Landroid/widget/ImageButton;

    .line 2618055
    const v0, 0x7f0d0b79

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->c:Landroid/widget/ImageButton;

    .line 2618056
    const v0, 0x7f0d0b78

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->d:Landroid/widget/ImageButton;

    .line 2618057
    const v0, 0x7f0d0b77

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->e:Landroid/widget/ImageButton;

    .line 2618058
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->e:Landroid/widget/ImageButton;

    new-instance v1, LX/IrR;

    invoke-direct {v1, p0}, LX/IrR;-><init>(Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2618059
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->e:Landroid/widget/ImageButton;

    new-instance v1, LX/IrS;

    invoke-direct {v1, p0}, LX/IrS;-><init>(Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2618060
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    const v1, 0x7f0d31e8

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->g:Landroid/graphics/drawable/Drawable;

    .line 2618061
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->a:LX/0wM;

    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->g:Landroid/graphics/drawable/Drawable;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 2618062
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/ImageButton;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->b:Landroid/widget/ImageButton;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->c:Landroid/widget/ImageButton;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->d:Landroid/widget/ImageButton;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->f:Ljava/util/List;

    .line 2618063
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->b:Landroid/widget/ImageButton;

    new-instance v1, LX/IrT;

    invoke-direct {v1, p0}, LX/IrT;-><init>(Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2618064
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->c:Landroid/widget/ImageButton;

    new-instance v1, LX/IrU;

    invoke-direct {v1, p0}, LX/IrU;-><init>(Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2618065
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->d:Landroid/widget/ImageButton;

    new-instance v1, LX/IrV;

    invoke-direct {v1, p0}, LX/IrV;-><init>(Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2618066
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->a:LX/0wM;

    return-void
.end method

.method private setAddDoodleLayerButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 2618049
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->d:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2618050
    return-void

    .line 2618051
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setAddStickerLayerButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 2618046
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->c:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2618047
    return-void

    .line 2618048
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setAddTextLayerButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 2618043
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->b:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2618044
    return-void

    .line 2618045
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/IrD;)V
    .locals 1

    .prologue
    .line 2618039
    iget-boolean v0, p1, LX/IrD;->a:Z

    invoke-direct {p0, v0}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->setAddTextLayerButtonVisibility(Z)V

    .line 2618040
    iget-boolean v0, p1, LX/IrD;->b:Z

    invoke-direct {p0, v0}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->setAddStickerLayerButtonVisibility(Z)V

    .line 2618041
    iget-boolean v0, p1, LX/IrD;->c:Z

    invoke-direct {p0, v0}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->setAddDoodleLayerButtonVisibility(Z)V

    .line 2618042
    return-void
.end method

.method public setAddDoodleLayerButtonBrushTipColour(I)V
    .locals 2

    .prologue
    .line 2618036
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->g:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->a:LX/0wM;

    invoke-virtual {v1, p1}, LX/0wM;->a(I)Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2618037
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    .line 2618038
    return-void
.end method

.method public setListener(LX/IrE;)V
    .locals 0

    .prologue
    .line 2618034
    iput-object p1, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->h:LX/IrE;

    .line 2618035
    return-void
.end method

.method public setUndoodleButtonVisibility(Z)V
    .locals 2

    .prologue
    .line 2618031
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->e:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 2618032
    return-void

    .line 2618033
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
