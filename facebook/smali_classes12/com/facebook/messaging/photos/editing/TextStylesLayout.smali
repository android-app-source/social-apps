.class public Lcom/facebook/messaging/photos/editing/TextStylesLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0w3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IrI;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2619647
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2619648
    invoke-direct {p0}, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->a()V

    .line 2619649
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2619644
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2619645
    invoke-direct {p0}, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->a()V

    .line 2619646
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2619641
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2619642
    invoke-direct {p0}, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->a()V

    .line 2619643
    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    const/high16 v6, -0x1000000

    const/4 v5, -0x1

    .line 2619619
    const-class v0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;

    invoke-static {v0, p0}, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2619620
    const v0, 0x7f031493

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2619621
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->a:LX/0wM;

    const v1, 0x7f020079

    invoke-virtual {v0, v1, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2619622
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->a:LX/0wM;

    const v2, 0x7f020079

    invoke-virtual {v0, v2, v6}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2619623
    const v0, 0x7f0d2ec7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->d:Landroid/widget/ImageView;

    .line 2619624
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->d:Landroid/widget/ImageView;

    new-instance v3, LX/IsK;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v5, v4}, LX/IsK;-><init>(Lcom/facebook/messaging/photos/editing/TextStylesLayout;II)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2619625
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2619626
    const v0, 0x7f0d2ec8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->e:Landroid/widget/ImageView;

    .line 2619627
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->e:Landroid/widget/ImageView;

    new-instance v3, LX/IsK;

    invoke-direct {v3, p0, v5, v6}, LX/IsK;-><init>(Lcom/facebook/messaging/photos/editing/TextStylesLayout;II)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2619628
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2619629
    const v0, 0x7f0d2ec9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->f:Landroid/widget/ImageView;

    .line 2619630
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->f:Landroid/widget/ImageView;

    new-instance v1, LX/IsK;

    invoke-direct {v1, p0, v6, v5}, LX/IsK;-><init>(Lcom/facebook/messaging/photos/editing/TextStylesLayout;II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2619631
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2619632
    return-void
.end method

.method private static a(Lcom/facebook/messaging/photos/editing/TextStylesLayout;LX/0wM;LX/0w3;)V
    .locals 0

    .prologue
    .line 2619640
    iput-object p1, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->a:LX/0wM;

    iput-object p2, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->b:LX/0w3;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v1}, LX/0w3;->a(LX/0QB;)LX/0w3;

    move-result-object v1

    check-cast v1, LX/0w3;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->a(Lcom/facebook/messaging/photos/editing/TextStylesLayout;LX/0wM;LX/0w3;)V

    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 2619635
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->b:LX/0w3;

    .line 2619636
    iget v1, v0, LX/0w3;->e:I

    move v0, v1

    .line 2619637
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 2619638
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 2619639
    return-void
.end method

.method public setListener(LX/IrI;)V
    .locals 0

    .prologue
    .line 2619633
    iput-object p1, p0, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->c:LX/IrI;

    .line 2619634
    return-void
.end method
