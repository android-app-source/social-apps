.class public Lcom/facebook/messaging/photos/editing/LayerEditText;
.super Landroid/widget/EditText;
.source ""


# instance fields
.field public a:LX/Iqp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2617281
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 2617282
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2617291
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2617292
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2617289
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2617290
    return-void
.end method


# virtual methods
.method public final onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2c

    const v1, 0x3deee88d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2617293
    invoke-super {p0, p1, p2, p3}, Landroid/widget/EditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 2617294
    if-nez p1, :cond_0

    .line 2617295
    invoke-virtual {p0, v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setTextIsSelectable(Z)V

    .line 2617296
    invoke-virtual {p0, v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setFocusable(Z)V

    .line 2617297
    invoke-virtual {p0, v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setFocusableInTouchMode(Z)V

    .line 2617298
    invoke-virtual {p0, v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setEnabled(Z)V

    .line 2617299
    invoke-virtual {p0, v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setClickable(Z)V

    .line 2617300
    invoke-virtual {p0, v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->setLongClickable(Z)V

    .line 2617301
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x72de155e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p2    # Landroid/view/KeyEvent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 2617285
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 2617286
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/LayerEditText;->a:LX/Iqp;

    if-eqz v1, :cond_0

    .line 2617287
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/LayerEditText;->a:LX/Iqp;

    invoke-interface {v1}, LX/Iqp;->a()V

    .line 2617288
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setOnBackPressedListener(LX/Iqp;)V
    .locals 0

    .prologue
    .line 2617283
    iput-object p1, p0, Lcom/facebook/messaging/photos/editing/LayerEditText;->a:LX/Iqp;

    .line 2617284
    return-void
.end method
