.class public Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;
.super Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;
.source ""


# static fields
.field public static final m:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Landroid/view/View;

.field private B:LX/475;

.field private C:Landroid/animation/ValueAnimator;

.field public D:LX/IrP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:LX/IrD;

.field public F:Landroid/view/View;

.field public G:Landroid/widget/ImageButton;

.field public H:Landroid/widget/ImageButton;

.field public I:Lcom/facebook/fbui/glyph/GlyphView;

.field public J:Landroid/view/View;

.field public K:Lcom/facebook/fbui/glyph/GlyphView;

.field public L:Landroid/view/View;

.field public M:Lcom/facebook/messaging/doodle/ColourIndicator;

.field public N:Lcom/facebook/messaging/doodle/ColourPicker;

.field public O:Landroid/graphics/Bitmap;

.field public P:Landroid/view/View;

.field public Q:Landroid/view/View;

.field public R:Z

.field public n:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/IrQ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/1FZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/1Er;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/0kL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/476;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:Landroid/net/Uri;

.field private v:Lcom/facebook/ui/media/attachments/MediaResource;

.field public w:LX/Igg;

.field public x:Landroid/widget/ImageView;

.field public y:Lcom/facebook/drawingview/DrawingView;

.field public z:Lcom/facebook/messaging/doodle/CaptionEditorView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2617704
    const-class v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    sput-object v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->m:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2617629
    invoke-direct {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;-><init>()V

    .line 2617630
    return-void
.end method

.method private a(LX/1FJ;)Landroid/graphics/Bitmap;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 2617631
    iget-boolean v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-eqz v0, :cond_2

    .line 2617632
    const/4 v9, 0x0

    .line 2617633
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->p:LX/1FZ;

    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v2

    .line 2617634
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 2617635
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2617636
    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->draw(Landroid/graphics/Canvas;)V

    .line 2617637
    const/16 v1, 0x9

    new-array v1, v1, [F

    .line 2617638
    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 2617639
    const/4 v3, 0x2

    aget v3, v1, v3

    float-to-int v3, v3

    .line 2617640
    const/4 v4, 0x5

    aget v1, v1, v4

    float-to-int v4, v1

    .line 2617641
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 2617642
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2617643
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    add-int/2addr v8, v4

    invoke-direct {v6, v3, v4, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2617644
    new-instance v3, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v3, v9, v9, v4, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2617645
    iget-boolean v4, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    .line 2617646
    iget-object v7, v4, LX/IrP;->t:LX/Iqd;

    if-eqz v7, :cond_0

    iget-object v7, v4, LX/IrP;->t:LX/Iqd;

    invoke-virtual {v7}, LX/Iqd;->e()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2617647
    iget-object v7, v4, LX/IrP;->t:LX/Iqd;

    .line 2617648
    iget-object v8, v7, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    move-object v7, v8

    .line 2617649
    if-eqz v7, :cond_0

    .line 2617650
    iget-object v8, v4, LX/IrP;->l:Landroid/view/ViewGroup;

    invoke-virtual {v8, v7}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2617651
    iget-object v8, v4, LX/IrP;->i:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v8, v7}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->indexOfChild(Landroid/view/View;)I

    move-result v8

    if-gez v8, :cond_0

    .line 2617652
    iget-object v8, v4, LX/IrP;->i:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    const/4 v9, 0x0

    invoke-virtual {v8, v7, v9}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->addView(Landroid/view/View;I)V

    .line 2617653
    :cond_0
    iget-object v7, v4, LX/IrP;->c:LX/IqN;

    iget-object v8, v4, LX/IrP;->i:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    .line 2617654
    new-instance v9, LX/IqO;

    invoke-direct {v9, v0}, LX/IqO;-><init>(Landroid/graphics/Bitmap;)V

    move-object v9, v9

    .line 2617655
    iget-object p0, v4, LX/IrP;->F:Ljava/lang/Integer;

    .line 2617656
    iput-object p0, v9, LX/IqO;->e:Ljava/lang/Integer;

    .line 2617657
    move-object v9, v9

    .line 2617658
    new-instance p0, LX/IqP;

    invoke-direct {p0, v9}, LX/IqP;-><init>(LX/IqO;)V

    move-object v9, p0

    .line 2617659
    invoke-virtual {v7, v8, v9}, LX/IqN;->a(Landroid/view/ViewGroup;LX/IqP;)Landroid/graphics/Bitmap;

    move-result-object v7

    move-object v0, v7

    .line 2617660
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v5, v0, v6, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2617661
    invoke-static {v2}, LX/1FJ;->c(LX/1FJ;)V

    .line 2617662
    move-object v0, v1

    .line 2617663
    :goto_0
    return-object v0

    .line 2617664
    :cond_2
    invoke-virtual {p1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 2617665
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2617666
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->A:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 2617667
    move-object v0, v0

    .line 2617668
    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 2617669
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->r:LX/1Er;

    const-string v1, "orca-image-"

    const-string v2, ".jpg"

    sget-object v3, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v1, v2, v3}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 2617670
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 2617671
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 2617672
    :try_start_0
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x55

    invoke-virtual {p1, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2617673
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 2617674
    return-object v0

    .line 2617675
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;
    .locals 4

    .prologue
    .line 2617676
    invoke-static {}, LX/6eE;->a()LX/6eE;

    move-result-object v0

    .line 2617677
    new-instance v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;-><init>()V

    .line 2617678
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2617679
    const-string v3, "arg_media_resource"

    invoke-virtual {v2, v3, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2617680
    invoke-virtual {v0, v2}, LX/6eE;->a(Landroid/os/Bundle;)V

    .line 2617681
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2617682
    move-object v0, v1

    .line 2617683
    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;ILandroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2617692
    if-nez p1, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    .line 2617693
    :goto_0
    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->C:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_0

    .line 2617694
    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2617695
    :cond_0
    const/4 v3, 0x2

    new-array v3, v3, [F

    invoke-virtual {p2}, Landroid/view/View;->getAlpha()F

    move-result v4

    aput v4, v3, v2

    aput v0, v3, v1

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->C:Landroid/animation/ValueAnimator;

    .line 2617696
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->C:Landroid/animation/ValueAnimator;

    new-instance v3, LX/Ir0;

    invoke-direct {v3, p0, p2}, LX/Ir0;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;Landroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2617697
    if-ne p1, v1, :cond_1

    .line 2617698
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->C:Landroid/animation/ValueAnimator;

    const-wide/16 v4, 0xc8

    invoke-virtual {v0, v4, v5}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 2617699
    :cond_1
    if-nez p1, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p2, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 2617700
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->C:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2617701
    return-void

    .line 2617702
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2617703
    goto :goto_1
.end method

.method public static l(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2617684
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/doodle/CaptionEditorView;->setEnabled(Z)V

    .line 2617685
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 2617686
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->H:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2617687
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->G:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2617688
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->J:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2617689
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->N:Lcom/facebook/messaging/doodle/ColourPicker;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/doodle/ColourPicker;->setVisibility(I)V

    .line 2617690
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->M:Lcom/facebook/messaging/doodle/ColourIndicator;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/doodle/ColourIndicator;->setVisibility(I)V

    .line 2617691
    return-void
.end method

.method public static m(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)Z
    .locals 1

    .prologue
    .line 2617705
    iget-boolean v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    if-eqz v0, :cond_0

    .line 2617706
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    invoke-virtual {v0}, LX/IrP;->b()Z

    move-result v0

    .line 2617707
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->f()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V
    .locals 5

    .prologue
    .line 2617565
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "back_button_dialog_photo_edit_shown"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2617566
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 2617567
    iget-boolean v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-nez v2, :cond_0

    .line 2617568
    const-string v2, "has_caption"

    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v3}, Lcom/facebook/messaging/doodle/CaptionEditorView;->f()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2617569
    const-string v2, "has_drawing"

    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v3}, Lcom/facebook/drawingview/DrawingView;->c()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2617570
    :cond_0
    new-instance v2, LX/31Y;

    invoke-virtual {v1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v3, 0x7f080831

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f080832

    invoke-virtual {v2, v3}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f080833

    new-instance v4, LX/Ir2;

    invoke-direct {v4, p0, v0}, LX/Ir2;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const v3, 0x7f080834

    new-instance v4, LX/Ir1;

    invoke-direct {v4, p0, v0, v1}, LX/Ir1;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/app/Dialog;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2617571
    return-void
.end method

.method public static o$redex0(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V
    .locals 5

    .prologue
    .line 2617572
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->w:LX/Igg;

    if-nez v0, :cond_0

    .line 2617573
    :goto_0
    return-void

    .line 2617574
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-nez v0, :cond_1

    .line 2617575
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->l(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    .line 2617576
    :cond_1
    const/4 v1, 0x0

    .line 2617577
    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-eqz v0, :cond_5

    .line 2617578
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 2617579
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 2617580
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->O:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x0

    aget v3, v0, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 2617581
    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->O:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    const/4 v4, 0x4

    aget v0, v0, v4

    mul-float/2addr v0, v3

    float-to-int v0, v0

    .line 2617582
    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->p:LX/1FZ;

    invoke-virtual {v3, v2, v0}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v0

    .line 2617583
    :goto_1
    move-object v1, v0

    .line 2617584
    invoke-direct {p0, v1}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->a(LX/1FJ;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->a(Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->u:Landroid/net/Uri;

    .line 2617585
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->v:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz v0, :cond_3

    .line 2617586
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->v:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v2}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->v:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2617587
    iput-object v2, v0, LX/5zn;->g:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2617588
    move-object v0, v0

    .line 2617589
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->u:Landroid/net/Uri;

    .line 2617590
    iput-object v2, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2617591
    move-object v0, v0

    .line 2617592
    const/4 v2, 0x1

    .line 2617593
    iput-boolean v2, v0, LX/5zn;->x:Z

    .line 2617594
    move-object v0, v0

    .line 2617595
    iget-boolean v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-nez v2, :cond_2

    .line 2617596
    const-string v2, "has_caption"

    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v3}, Lcom/facebook/messaging/doodle/CaptionEditorView;->f()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/5zn;->a(Ljava/lang/String;Ljava/lang/String;)LX/5zn;

    move-result-object v2

    const-string v3, "has_drawing"

    iget-object v4, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v4}, Lcom/facebook/drawingview/DrawingView;->c()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/5zn;->a(Ljava/lang/String;Ljava/lang/String;)LX/5zn;

    .line 2617597
    :cond_2
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2617598
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->w:LX/Igg;

    invoke-interface {v2, v0}, LX/Igg;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2617599
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->n:LX/0Zb;

    const-string v2, "send_from_photo_edit_clicked"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2617600
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-nez v2, :cond_3

    .line 2617601
    const-string v2, "has_caption"

    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v3}, Lcom/facebook/messaging/doodle/CaptionEditorView;->f()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 2617602
    const-string v2, "has_drawing"

    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v3}, Lcom/facebook/drawingview/DrawingView;->c()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 2617603
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2617604
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    if-eqz v0, :cond_4

    .line 2617605
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    invoke-virtual {v0}, LX/IrP;->k()V

    .line 2617606
    :cond_4
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/4eL; {:try_start_0 .. :try_end_0} :catch_2
    .catch LX/1FM; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2617607
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto/16 :goto_0

    .line 2617608
    :catch_0
    move-exception v0

    .line 2617609
    :try_start_1
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->s:LX/0kL;

    new-instance v3, LX/27k;

    const v4, 0x7f080039

    invoke-direct {v3, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v2, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2617610
    sget-object v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->m:Ljava/lang/Class;

    const-string v3, "Saving the bitmap failed, could not generate Uri."

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2617611
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto/16 :goto_0

    .line 2617612
    :catch_1
    move-exception v0

    .line 2617613
    :try_start_2
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->s:LX/0kL;

    new-instance v3, LX/27k;

    const v4, 0x7f080039

    invoke-direct {v3, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v2, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2617614
    sget-object v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->m:Ljava/lang/Class;

    const-string v3, "Not enough memory to create new bitmap."

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2617615
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto/16 :goto_0

    .line 2617616
    :catch_2
    move-exception v0

    .line 2617617
    :try_start_3
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->s:LX/0kL;

    new-instance v3, LX/27k;

    const v4, 0x7f080039

    invoke-direct {v3, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v2, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2617618
    sget-object v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->m:Ljava/lang/Class;

    const-string v3, "Too much memory being used by other bitmaps to create new bitmap."

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2617619
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto/16 :goto_0

    .line 2617620
    :catch_3
    move-exception v0

    .line 2617621
    :try_start_4
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->s:LX/0kL;

    new-instance v3, LX/27k;

    const v4, 0x7f080039

    invoke-direct {v3, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v2, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2617622
    sget-object v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->m:Ljava/lang/Class;

    const-string v3, "Too much memory being used by other bitmaps to create new bitmap."

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2617623
    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    :cond_5
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->p:LX/1FZ;

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->A:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->A:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/1FZ;->a(II)LX/1FJ;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private s()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2617624
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->O:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 2617625
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2617626
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->O:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2617627
    iput-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->O:Landroid/graphics/Bitmap;

    .line 2617628
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 2617456
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 2617457
    new-instance v1, LX/Ir3;

    invoke-direct {v1, p0}, LX/Ir3;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 2617458
    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2617459
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->c()V

    .line 2617460
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->I:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2617461
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    if-eqz v0, :cond_0

    .line 2617462
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->e()V

    .line 2617463
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->B:LX/475;

    invoke-virtual {v0}, LX/475;->b()V

    .line 2617464
    invoke-direct {p0}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->s()V

    .line 2617465
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x5ba2532a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2617466
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2617467
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2617468
    if-eqz v0, :cond_0

    .line 2617469
    const-string v2, "arg_media_resource"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->v:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2617470
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->v:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2617471
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->v:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->u:Landroid/net/Uri;

    .line 2617472
    const v0, 0x7f0d212e

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->x:Landroid/widget/ImageView;

    .line 2617473
    new-instance v0, LX/2Qx;

    invoke-direct {v0}, LX/2Qx;-><init>()V

    .line 2617474
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->u:Landroid/net/Uri;

    const/4 p1, 0x1

    invoke-virtual {v0, v2, v3, p1}, LX/2Qx;->a(Landroid/content/Context;Landroid/net/Uri;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2617475
    :goto_0
    move-object v0, v0

    .line 2617476
    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->O:Landroid/graphics/Bitmap;

    .line 2617477
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->x:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->O:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2617478
    iget-boolean v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-eqz v0, :cond_1

    .line 2617479
    const v0, 0x7f0d118a

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->I:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2617480
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->I:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/Ir6;

    invoke-direct {v2, p0}, LX/Ir6;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2617481
    const v0, 0x7f0d09a9

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->K:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2617482
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->K:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/Ir7;

    invoke-direct {v2, p0}, LX/Ir7;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2617483
    const v0, 0x7f0d212f

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->L:Landroid/view/View;

    .line 2617484
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->L:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v2, 0x64

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2617485
    :goto_1
    const v0, -0xfad4501

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2617486
    :cond_1
    const v0, 0x7f0d24e8

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->A:Landroid/view/View;

    .line 2617487
    const v0, 0x7f0d24ea

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->P:Landroid/view/View;

    .line 2617488
    const v0, 0x7f0d24ee

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->F:Landroid/view/View;

    .line 2617489
    const v0, 0x7f0d24e9

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/CaptionEditorView;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    .line 2617490
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    new-instance v2, LX/Ir8;

    invoke-direct {v2, p0}, LX/Ir8;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/doodle/CaptionEditorView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2617491
    const v0, 0x7f0d0803

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawingview/DrawingView;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    .line 2617492
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    new-instance v2, LX/Ir9;

    invoke-direct {v2, p0}, LX/Ir9;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    .line 2617493
    iput-object v2, v0, Lcom/facebook/drawingview/DrawingView;->d:LX/5Ne;

    .line 2617494
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->y:Lcom/facebook/drawingview/DrawingView;

    new-instance v2, LX/IrA;

    invoke-direct {v2, p0}, LX/IrA;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    .line 2617495
    iput-object v2, v0, Lcom/facebook/drawingview/DrawingView;->n:LX/5Ng;

    .line 2617496
    const v0, 0x7f0d13f0

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->J:Landroid/view/View;

    .line 2617497
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->J:Landroid/view/View;

    new-instance v2, LX/IrB;

    invoke-direct {v2, p0}, LX/IrB;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2617498
    const v0, 0x7f0d24f0

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->I:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2617499
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->I:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/Iqv;

    invoke-direct {v2, p0}, LX/Iqv;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2617500
    const v0, 0x7f0d24ef

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->K:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2617501
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->K:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/Iqw;

    invoke-direct {v2, p0}, LX/Iqw;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2617502
    const v0, 0x7f0d0cd1

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourIndicator;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->M:Lcom/facebook/messaging/doodle/ColourIndicator;

    .line 2617503
    const v0, 0x7f0d0cd2

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/doodle/ColourPicker;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->N:Lcom/facebook/messaging/doodle/ColourPicker;

    .line 2617504
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->N:Lcom/facebook/messaging/doodle/ColourPicker;

    new-instance v2, LX/Iqx;

    invoke-direct {v2, p0}, LX/Iqx;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    .line 2617505
    iput-object v2, v0, Lcom/facebook/messaging/doodle/ColourPicker;->c:LX/8CF;

    .line 2617506
    const v0, 0x7f0d24ec

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->G:Landroid/widget/ImageButton;

    .line 2617507
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->G:Landroid/widget/ImageButton;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2617508
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->G:Landroid/widget/ImageButton;

    new-instance v2, LX/Iqy;

    invoke-direct {v2, p0}, LX/Iqy;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2617509
    const v0, 0x7f0d24ed

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->H:Landroid/widget/ImageButton;

    .line 2617510
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->H:Landroid/widget/ImageButton;

    new-instance v2, LX/Iqz;

    invoke-direct {v2, p0}, LX/Iqz;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2617511
    goto/16 :goto_1

    .line 2617512
    :catch_0
    move-exception v0

    .line 2617513
    sget-object v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->m:Ljava/lang/Class;

    const-string v3, "Could not scale image down."

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2617514
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/16 v0, 0x2a

    const v1, 0x3db485a6

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2617515
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2617516
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v3, p0

    check-cast v3, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    const-class v5, LX/IrQ;

    invoke-interface {p1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/IrQ;

    invoke-static {p1}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v8

    check-cast v8, LX/1FZ;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {p1}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v10

    check-cast v10, LX/1Er;

    invoke-static {p1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v11

    check-cast v11, LX/0kL;

    const-class v1, LX/476;

    invoke-interface {p1, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/476;

    iput-object v4, v3, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->n:LX/0Zb;

    iput-object v5, v3, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->o:LX/IrQ;

    iput-object v8, v3, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->p:LX/1FZ;

    iput-object v9, v3, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->q:LX/0ad;

    iput-object v10, v3, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->r:LX/1Er;

    iput-object v11, v3, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->s:LX/0kL;

    iput-object p1, v3, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->t:LX/476;

    .line 2617517
    new-instance v1, LX/IrD;

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->q:LX/0ad;

    sget-short v3, LX/IsL;->c:S

    invoke-interface {v2, v3, v6}, LX/0ad;->a(SZ)Z

    move-result v2

    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->q:LX/0ad;

    sget-short v4, LX/IsL;->b:S

    invoke-interface {v3, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    iget-object v4, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->q:LX/0ad;

    sget-short v5, LX/IsL;->a:S

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-direct {v1, v2, v3, v4}, LX/IrD;-><init>(ZZZ)V

    iput-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->E:LX/IrD;

    .line 2617518
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->E:LX/IrD;

    invoke-virtual {v1}, LX/IrD;->a()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    .line 2617519
    const/16 v1, 0x2b

    const v2, -0x50642428

    invoke-static {v7, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5798018b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2617520
    iget-boolean v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-eqz v0, :cond_0

    .line 2617521
    const v0, 0x7f030d77

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->Q:Landroid/view/View;

    .line 2617522
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 2617523
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v2, 0x30

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2617524
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->Q:Landroid/view/View;

    move-object v0, v0

    .line 2617525
    const/16 v2, 0x2b

    const v3, -0x65faf286

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2617526
    :goto_0
    return-object v0

    .line 2617527
    :cond_0
    const v0, 0x7f030f3e

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2617528
    move-object v0, v0

    .line 2617529
    const v2, 0x5dbba6fd

    invoke-static {v2, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2e5ca5ec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2617530
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onDestroy()V

    .line 2617531
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->I:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v1, :cond_0

    .line 2617532
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->I:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2617533
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    if-eqz v1, :cond_1

    .line 2617534
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v1}, Lcom/facebook/messaging/doodle/CaptionEditorView;->e()V

    .line 2617535
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->s()V

    .line 2617536
    const/16 v1, 0x2b

    const v2, -0x217c0167

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x653a02e4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2617537
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onResume()V

    .line 2617538
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v1}, Lcom/facebook/messaging/doodle/CaptionEditorView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2617539
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v1}, Lcom/facebook/messaging/doodle/CaptionEditorView;->requestFocus()Z

    .line 2617540
    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->u:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2617541
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2617542
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2617543
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x52d789f6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xd1cb0f0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2617544
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onStart()V

    .line 2617545
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->B:LX/475;

    invoke-virtual {v1}, LX/475;->a()V

    .line 2617546
    const/16 v1, 0x2b

    const v2, -0x4a5226d4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x215eff1c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2617547
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->B:LX/475;

    invoke-virtual {v1}, LX/475;->b()V

    .line 2617548
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onStop()V

    .line 2617549
    const/16 v1, 0x2b

    const v2, -0x3e952d4c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2617550
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2617551
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->t:LX/476;

    invoke-virtual {v0, p1}, LX/476;->a(Landroid/view/View;)LX/475;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->B:LX/475;

    .line 2617552
    iget-boolean v0, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->R:Z

    if-eqz v0, :cond_1

    .line 2617553
    const/4 v3, 0x0

    .line 2617554
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->o:LX/IrQ;

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->E:LX/IrD;

    iget-object v4, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->Q:Landroid/view/View;

    check-cast v4, Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->Q:Landroid/view/View;

    const v6, 0x7f0d1c7c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    iget-object v6, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->Q:Landroid/view/View;

    const v7, 0x7f0d2130

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    const v7, 0x7f0d2131

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/photos/editing/TextStylesLayout;

    const v8, 0x7f0d1c7e

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-object v9, v3

    invoke-virtual/range {v1 .. v9}, LX/IrQ;->a(LX/IrD;LX/Is9;Landroid/view/ViewGroup;Lcom/facebook/messaging/photos/editing/LayerGroupLayout;Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;Lcom/facebook/messaging/photos/editing/TextStylesLayout;Landroid/view/View;LX/4ob;)LX/IrP;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    .line 2617555
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    .line 2617556
    iget-object v2, v1, LX/IrP;->w:LX/Iqe;

    sget-object v3, LX/Iqe;->DISABLED:LX/Iqe;

    if-ne v2, v3, :cond_0

    .line 2617557
    sget-object v2, LX/Iqe;->IDLE:LX/Iqe;

    invoke-static {v1, v2}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    .line 2617558
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    .line 2617559
    iget-object v2, v1, LX/IrP;->i:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->setVisibility(I)V

    .line 2617560
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    new-instance v2, LX/Ir4;

    invoke-direct {v2, p0}, LX/Ir4;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    .line 2617561
    iput-object v2, v1, LX/IrP;->v:LX/Ir4;

    .line 2617562
    :goto_0
    return-void

    .line 2617563
    :cond_1
    new-instance v0, LX/Ir5;

    invoke-direct {v0, p0}, LX/Ir5;-><init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2617564
    goto :goto_0
.end method
