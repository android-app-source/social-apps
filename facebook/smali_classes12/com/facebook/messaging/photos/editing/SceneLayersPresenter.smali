.class public Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private A:[F

.field public B:LX/Iqk;

.field public final a:LX/1zC;

.field private final b:LX/1Ad;

.field public final c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DeK;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final f:LX/Ird;

.field public final g:LX/Irg;

.field public final h:LX/0wW;

.field public final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Iqg;",
            "LX/Iqk;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "LX/Iqg;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/Iqq;

.field public l:LX/Iqm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/Is2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/IrL;

.field public o:LX/IrM;

.field public p:Landroid/view/GestureDetector;

.field public q:LX/Irc;

.field public r:LX/IrY;

.field private s:F

.field private t:I

.field private u:I

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:Z

.field private final z:I


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/LayerGroupLayout;LX/Ird;LX/1zC;LX/1Ad;LX/0Ot;LX/0TD;LX/0wW;)V
    .locals 2
    .param p1    # Lcom/facebook/messaging/photos/editing/LayerGroupLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Ird;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/photos/editing/LayerGroupLayout;",
            "LX/Ird;",
            "LX/1zC;",
            "LX/1Ad;",
            "LX/0Ot",
            "<",
            "LX/DeK;",
            ">;",
            "LX/0TD;",
            "LX/0wW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2619010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2619011
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    .line 2619012
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->j:Ljava/util/LinkedHashMap;

    .line 2619013
    new-instance v0, LX/Irh;

    invoke-direct {v0, p0}, LX/Irh;-><init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->k:LX/Iqq;

    .line 2619014
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->x:Z

    .line 2619015
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->A:[F

    .line 2619016
    iput-object p3, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a:LX/1zC;

    .line 2619017
    iput-object p4, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->b:LX/1Ad;

    .line 2619018
    iput-object p5, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->d:LX/0Ot;

    .line 2619019
    iput-object p1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    .line 2619020
    iput-object p2, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    .line 2619021
    iput-object p7, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->h:LX/0wW;

    .line 2619022
    iput-object p6, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->e:LX/0TD;

    .line 2619023
    new-instance v0, LX/Irg;

    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-direct {v0, v1}, LX/Irg;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->g:LX/Irg;

    .line 2619024
    const/16 v0, 0x30

    iput v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->z:I

    .line 2619025
    return-void
.end method

.method public static a(LX/Iqg;Landroid/view/View;)LX/Iqo;
    .locals 2

    .prologue
    .line 2619004
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 2619005
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2619006
    iget-object v0, p0, LX/Iqg;->i:LX/Iqo;

    move-object v0, v0

    .line 2619007
    :goto_0
    return-object v0

    .line 2619008
    :cond_0
    iget-object v0, p0, LX/Iqg;->h:LX/Iqo;

    move-object v0, v0

    .line 2619009
    goto :goto_0
.end method

.method public static a(LX/Iqg;Landroid/view/View;II)V
    .locals 3

    .prologue
    .line 2618997
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 2618998
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 2618999
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 2619000
    int-to-float v2, p2

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 2619001
    int-to-float v2, p3

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 2619002
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-virtual {p0, v0}, LX/Iqg;->a(F)V

    .line 2619003
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;LX/Iqg;I)V
    .locals 12

    .prologue
    const/4 v6, -0x1

    const/4 v11, 0x0

    const/4 v10, -0x2

    .line 2618938
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->j(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v0

    if-nez v0, :cond_1

    .line 2618939
    :cond_0
    iget-boolean v0, p1, LX/Iqg;->k:Z

    move v0, v0

    .line 2618940
    if-eqz v0, :cond_1

    .line 2618941
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->j:Ljava/util/LinkedHashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2618942
    :goto_0
    return-void

    .line 2618943
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c(LX/Iqg;)LX/Iqk;

    move-result-object v1

    .line 2618944
    new-instance v0, LX/Irk;

    invoke-direct {v0, p0}, LX/Irk;-><init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V

    .line 2618945
    iput-object v0, v1, LX/Iqk;->l:LX/Irk;

    .line 2618946
    instance-of v0, v1, LX/Is1;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 2618947
    check-cast v0, LX/Is1;

    new-instance v2, LX/Irl;

    invoke-direct {v2, p0}, LX/Irl;-><init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V

    .line 2618948
    iput-object v2, v0, LX/Is1;->i:LX/Irl;

    .line 2618949
    :cond_2
    invoke-virtual {v1}, LX/Iqk;->a()V

    .line 2618950
    iget v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->s:F

    .line 2618951
    iget v2, v1, LX/Iqk;->i:F

    cmpl-float v2, v2, v0

    if-nez v2, :cond_a

    .line 2618952
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2618953
    iget-object v0, v1, LX/Iqk;->c:Landroid/view/View;

    move-object v7, v0

    .line 2618954
    iget-boolean v0, p1, LX/Iqg;->k:Z

    move v0, v0

    .line 2618955
    if-eqz v0, :cond_8

    .line 2618956
    invoke-static {p1, v7}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(LX/Iqg;Landroid/view/View;)LX/Iqo;

    move-result-object v8

    .line 2618957
    instance-of v0, p1, LX/IsC;

    if-eqz v0, :cond_7

    move-object v0, p1

    .line 2618958
    check-cast v0, LX/IsC;

    .line 2618959
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->j(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v2

    invoke-static {v8, v2}, LX/Irf;->b(LX/Iqo;I)I

    move-result v3

    .line 2618960
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v2

    invoke-static {v8, v2}, LX/Irf;->a(LX/Iqo;I)I

    move-result v5

    .line 2618961
    iget-boolean v2, v0, LX/IsC;->d:Z

    move v2, v2

    .line 2618962
    if-eqz v2, :cond_4

    .line 2618963
    const/high16 v2, -0x80000000

    invoke-static {v5, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 2618964
    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 2618965
    invoke-virtual {v7, v2, v4}, Landroid/view/View;->measure(II)V

    .line 2618966
    :goto_2
    invoke-static {p1, v7, v5, v3}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(LX/Iqg;Landroid/view/View;II)V

    .line 2618967
    iget-boolean v2, v0, LX/IsC;->d:Z

    move v2, v2

    .line 2618968
    if-eqz v2, :cond_5

    move v2, v3

    move v4, v5

    .line 2618969
    :goto_3
    iget-boolean v9, v0, LX/IsC;->c:Z

    move v0, v9

    .line 2618970
    if-eqz v0, :cond_6

    .line 2618971
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    invoke-direct {v0, v9, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2618972
    :goto_4
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v9

    invoke-static {v8, v4, v5, v9}, LX/Irf;->a(LX/Iqo;III)I

    move-result v4

    int-to-float v4, v4

    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->j(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v5

    invoke-static {v8, v2, v3, v5}, LX/Irf;->b(LX/Iqo;III)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v4, v2}, LX/Iqg;->a(FF)V

    .line 2618973
    :goto_5
    const/16 v2, 0x11

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2618974
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v3}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getChildCount()I

    move-result v3

    if-lt p2, v3, :cond_3

    move p2, v6

    :cond_3
    invoke-virtual {v2, v7, p2, v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2618975
    :goto_6
    new-instance v0, LX/Irq;

    invoke-direct {v0, p0}, LX/Irq;-><init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2618976
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->y:Z

    .line 2618977
    invoke-virtual {v1}, LX/Iqk;->f()V

    goto/16 :goto_0

    .line 2618978
    :cond_4
    invoke-virtual {v7, v11, v11}, Landroid/view/View;->measure(II)V

    goto :goto_2

    .line 2618979
    :cond_5
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    .line 2618980
    iget v4, p1, LX/Iqg;->d:F

    move v4, v4

    .line 2618981
    mul-float/2addr v2, v4

    float-to-int v4, v2

    .line 2618982
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    .line 2618983
    iget v9, p1, LX/Iqg;->d:F

    move v9, v9

    .line 2618984
    mul-float/2addr v2, v9

    float-to-int v2, v2

    goto :goto_3

    .line 2618985
    :cond_6
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v10, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    goto :goto_4

    .line 2618986
    :cond_7
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v0

    invoke-static {v8, v0}, LX/Irf;->a(LX/Iqo;I)I

    move-result v2

    .line 2618987
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->j(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v0

    invoke-static {v8, v0}, LX/Irf;->b(LX/Iqo;I)I

    move-result v3

    .line 2618988
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2618989
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v4

    .line 2618990
    invoke-static {v8, v2, v2, v4}, LX/Irf;->a(LX/Iqo;III)I

    move-result v5

    move v2, v5

    .line 2618991
    int-to-float v2, v2

    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->j(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v4

    .line 2618992
    invoke-static {v8, v3, v3, v4}, LX/Irf;->b(LX/Iqo;III)I

    move-result v5

    move v3, v5

    .line 2618993
    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, LX/Iqg;->a(FF)V

    goto :goto_5

    .line 2618994
    :cond_8
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v2}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getChildCount()I

    move-result v2

    if-lt p2, v2, :cond_9

    :goto_7
    invoke-virtual {v0, v7, v6}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->addView(Landroid/view/View;I)V

    goto :goto_6

    :cond_9
    move v6, p2

    goto :goto_7

    .line 2618995
    :cond_a
    iput v0, v1, LX/Iqk;->i:F

    .line 2618996
    invoke-virtual {v1}, LX/Iqk;->q()V

    goto/16 :goto_1
.end method

.method public static a(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;LX/Iqg;LX/Iqg;)V
    .locals 2

    .prologue
    .line 2618926
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqk;

    .line 2618927
    if-eqz v0, :cond_0

    .line 2618928
    invoke-virtual {v0}, LX/Iqk;->g()V

    .line 2618929
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->y:Z

    .line 2618930
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqk;

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    .line 2618931
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    if-eqz v0, :cond_1

    .line 2618932
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    .line 2618933
    iget-object p1, v1, LX/Iqk;->c:Landroid/view/View;

    move-object v1, p1

    .line 2618934
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->setTopView(Landroid/view/View;)V

    .line 2618935
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    if-eqz v0, :cond_2

    .line 2618936
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    invoke-virtual {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/IrL;->b(Z)V

    .line 2618937
    :cond_2
    return-void
.end method

.method private a(IILX/Iqg;)Z
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2618896
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqk;

    .line 2618897
    if-nez v0, :cond_0

    move v0, v2

    .line 2618898
    :goto_0
    return v0

    .line 2618899
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v1}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 2618900
    iget-object v4, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v4}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    .line 2618901
    iget-boolean v5, v0, LX/Iqk;->j:Z

    if-eqz v5, :cond_1

    .line 2618902
    const/4 v5, 0x0

    iput-boolean v5, v0, LX/Iqk;->j:Z

    .line 2618903
    iget-object v5, v0, LX/Iqk;->d:Landroid/graphics/Matrix;

    invoke-virtual {v5}, Landroid/graphics/Matrix;->reset()V

    .line 2618904
    iget-object v5, v0, LX/Iqk;->d:Landroid/graphics/Matrix;

    iget-object v6, v0, LX/Iqk;->c:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getRotation()F

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 2618905
    iget-object v5, v0, LX/Iqk;->d:Landroid/graphics/Matrix;

    iget-object v6, v0, LX/Iqk;->c:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getScaleX()F

    move-result v6

    iget-object v7, v0, LX/Iqk;->c:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getScaleY()F

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 2618906
    iget-object v5, v0, LX/Iqk;->d:Landroid/graphics/Matrix;

    iget-object v6, v0, LX/Iqk;->c:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getTranslationX()F

    move-result v6

    iget-object v7, v0, LX/Iqk;->c:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getTranslationY()F

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 2618907
    iget-object v5, v0, LX/Iqk;->d:Landroid/graphics/Matrix;

    iget-object v6, v0, LX/Iqk;->d:Landroid/graphics/Matrix;

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 2618908
    :cond_1
    iget-object v5, v0, LX/Iqk;->d:Landroid/graphics/Matrix;

    move-object v5, v5

    .line 2618909
    iget-object v6, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->A:[F

    sub-int v7, p1, v1

    int-to-float v7, v7

    aput v7, v6, v2

    .line 2618910
    iget-object v6, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->A:[F

    sub-int v7, p2, v4

    int-to-float v7, v7

    aput v7, v6, v3

    .line 2618911
    iget-object v6, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->A:[F

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 2618912
    iget-object v5, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->A:[F

    aget v5, v5, v2

    float-to-int v5, v5

    add-int v6, v5, v1

    .line 2618913
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->A:[F

    aget v1, v1, v3

    float-to-int v1, v1

    add-int v7, v1, v4

    .line 2618914
    iget-object v1, v0, LX/Iqk;->c:Landroid/view/View;

    move-object v4, v1

    .line 2618915
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 2618916
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v0

    .line 2618917
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v5

    .line 2618918
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    .line 2618919
    sub-int v8, v0, v1

    iget v9, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->z:I

    if-ge v8, v9, :cond_2

    .line 2618920
    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->z:I

    sub-int/2addr v0, v1

    div-int/lit8 v1, v0, 0x2

    .line 2618921
    iget v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->z:I

    add-int/2addr v0, v1

    .line 2618922
    :cond_2
    sub-int v8, v4, v5

    iget v9, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->z:I

    if-ge v8, v9, :cond_3

    .line 2618923
    add-int/2addr v4, v5

    iget v5, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->z:I

    sub-int/2addr v4, v5

    div-int/lit8 v5, v4, 0x2

    .line 2618924
    iget v4, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->z:I

    add-int/2addr v4, v5

    .line 2618925
    :cond_3
    if-lt v6, v1, :cond_4

    if-ge v6, v0, :cond_4

    if-lt v7, v5, :cond_4

    if-ge v7, v4, :cond_4

    move v0, v3

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;III)Z
    .locals 1

    .prologue
    .line 2618892
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v0, p3}, LX/Ird;->a(I)LX/Iqg;

    move-result-object v0

    .line 2618893
    if-nez v0, :cond_0

    .line 2618894
    const/4 v0, 0x0

    .line 2618895
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(IILX/Iqg;)Z

    move-result v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;II)LX/Iqg;
    .locals 4

    .prologue
    .line 2618875
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    .line 2618876
    iget-object v1, v0, LX/Ird;->c:LX/Iqg;

    move-object v1, v1

    .line 2618877
    iget-boolean v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->v:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->q:LX/Irc;

    .line 2618878
    iget-boolean v2, v0, LX/Irc;->q:Z

    move v0, v2

    .line 2618879
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->r:LX/IrY;

    .line 2618880
    iget-boolean v2, v0, LX/IrY;->w:Z

    move v0, v2

    .line 2618881
    if-eqz v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2618882
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1, p2, v1}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(IILX/Iqg;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move-object v0, v1

    .line 2618883
    :goto_1
    return-object v0

    .line 2618884
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-ltz v2, :cond_4

    .line 2618885
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v0, v2}, LX/Ird;->a(I)LX/Iqg;

    move-result-object v0

    .line 2618886
    if-eq v0, v1, :cond_3

    .line 2618887
    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v3, v2}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2618888
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_3

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(IILX/Iqg;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2618889
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v1, v0}, LX/Ird;->b(LX/Iqg;)V

    goto :goto_1

    .line 2618890
    :cond_3
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 2618891
    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(LX/Iqg;)LX/Iqk;
    .locals 14

    .prologue
    .line 2619026
    instance-of v0, p1, LX/IsC;

    if-eqz v0, :cond_0

    .line 2619027
    check-cast p1, LX/IsC;

    .line 2619028
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->g:LX/Irg;

    const/4 v3, 0x0

    .line 2619029
    iget-object v2, v1, LX/Irg;->d:LX/0Zk;

    invoke-interface {v2}, LX/0Zk;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/photos/editing/LayerEditText;

    .line 2619030
    if-nez v2, :cond_3

    .line 2619031
    iget-object v2, v1, LX/Irg;->a:Landroid/view/LayoutInflater;

    const v4, 0x7f030464

    iget-object v5, v1, LX/Irg;->b:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/photos/editing/LayerEditText;

    .line 2619032
    :goto_0
    move-object v3, v2

    .line 2619033
    new-instance v1, LX/IsJ;

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a:LX/1zC;

    iget-object v4, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->d:LX/0Ot;

    iget-object v5, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->e:LX/0TD;

    iget-object v6, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->h:LX/0wW;

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, LX/IsJ;-><init>(LX/1zC;Lcom/facebook/messaging/photos/editing/LayerEditText;LX/0Ot;LX/0TD;LX/0wW;LX/IsC;)V

    .line 2619034
    new-instance v2, LX/Irm;

    invoke-direct {v2, p0}, LX/Irm;-><init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V

    .line 2619035
    iput-object v2, v1, LX/IsJ;->j:LX/Irm;

    .line 2619036
    move-object v0, v1

    .line 2619037
    :goto_1
    return-object v0

    .line 2619038
    :cond_0
    instance-of v0, p1, LX/Irx;

    if-eqz v0, :cond_1

    .line 2619039
    check-cast p1, LX/Irx;

    .line 2619040
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->g:LX/Irg;

    invoke-virtual {v1}, LX/Irg;->b()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v1

    .line 2619041
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->m:LX/Is2;

    const-class v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    .line 2619042
    new-instance v4, LX/Is1;

    invoke-static {v2}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {v2}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v9

    check-cast v9, LX/1Ad;

    invoke-static {v2}, LX/IiT;->a(LX/0QB;)LX/IiT;

    move-result-object v10

    check-cast v10, LX/IiT;

    invoke-static {v2}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v11

    check-cast v11, LX/0wW;

    invoke-static {v2}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v12

    check-cast v12, LX/1HI;

    invoke-static {v2}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/ExecutorService;

    move-object v5, p1

    move-object v6, v1

    move-object v7, v3

    invoke-direct/range {v4 .. v13}, LX/Is1;-><init>(LX/Irx;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/common/callercontext/CallerContext;LX/0Sh;LX/1Ad;LX/IiT;LX/0wW;LX/1HI;Ljava/util/concurrent/ExecutorService;)V

    .line 2619043
    move-object v1, v4

    .line 2619044
    move-object v0, v1

    .line 2619045
    goto :goto_1

    .line 2619046
    :cond_1
    instance-of v0, p1, LX/Iqh;

    if-eqz v0, :cond_2

    .line 2619047
    check-cast p1, LX/Iqh;

    .line 2619048
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->g:LX/Irg;

    invoke-virtual {v1}, LX/Irg;->b()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v2

    .line 2619049
    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    sget-object v3, LX/1Up;->a:LX/1Up;

    invoke-virtual {v1, v3}, LX/1af;->a(LX/1Up;)V

    .line 2619050
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->l:LX/Iqm;

    const-class v3, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    .line 2619051
    new-instance v4, LX/Iql;

    invoke-static {v1}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v8

    check-cast v8, LX/0wW;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v9

    check-cast v9, LX/1Ad;

    move-object v5, p1

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v9}, LX/Iql;-><init>(LX/Iqh;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/common/callercontext/CallerContext;LX/0wW;LX/1Ad;)V

    .line 2619052
    move-object v1, v4

    .line 2619053
    move-object v0, v1

    .line 2619054
    goto :goto_1

    .line 2619055
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2619056
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/messaging/photos/editing/LayerEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-nez v4, :cond_4

    const/4 v3, 0x1

    :cond_4
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    goto/16 :goto_0
.end method

.method public static e(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)F
    .locals 3

    .prologue
    .line 2618865
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    .line 2618866
    iget-object v1, v0, LX/Ird;->c:LX/Iqg;

    move-object v0, v1

    .line 2618867
    if-eqz v0, :cond_0

    .line 2618868
    if-nez v0, :cond_1

    .line 2618869
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v1}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 2618870
    :goto_0
    move v0, v1

    .line 2618871
    int-to-float v0, v0

    .line 2618872
    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v1}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 2618873
    iget v2, v0, LX/Iqg;->b:F

    move v2, v2

    .line 2618874
    float-to-int v2, v2

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method public static f(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)F
    .locals 3

    .prologue
    .line 2618855
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    .line 2618856
    iget-object v1, v0, LX/Ird;->c:LX/Iqg;

    move-object v0, v1

    .line 2618857
    if-eqz v0, :cond_0

    .line 2618858
    if-nez v0, :cond_1

    .line 2618859
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v1}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 2618860
    :goto_0
    move v0, v1

    .line 2618861
    int-to-float v0, v0

    .line 2618862
    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v1}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    .line 2618863
    iget v2, v0, LX/Iqg;->c:F

    move v2, v2

    .line 2618864
    float-to-int v2, v2

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method public static i(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I
    .locals 1

    .prologue
    .line 2618852
    iget v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->u:I

    if-eqz v0, :cond_0

    .line 2618853
    iget v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->u:I

    .line 2618854
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public static j(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I
    .locals 1

    .prologue
    .line 2618849
    iget v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->t:I

    if-eqz v0, :cond_0

    .line 2618850
    iget v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->t:I

    .line 2618851
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getHeight()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 2618822
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->k:LX/Iqq;

    .line 2618823
    iget-object v3, v0, LX/Ird;->b:LX/IrC;

    invoke-virtual {v3, v2}, LX/IrC;->a(LX/Iqq;)V

    .line 2618824
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getChildCount()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    .line 2618825
    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->g:LX/Irg;

    iget-object v4, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v4, v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Irg;->a(Landroid/view/View;)V

    .line 2618826
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2618827
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->removeAllViews()V

    .line 2618828
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v0}, LX/Ird;->h()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_1

    .line 2618829
    iget-object v3, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    invoke-virtual {v3, v0}, LX/Ird;->a(I)LX/Iqg;

    move-result-object v3

    invoke-static {p0, v3, v0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;LX/Iqg;I)V

    .line 2618830
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2618831
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    .line 2618832
    iget-object v1, v0, LX/Ird;->c:LX/Iqg;

    move-object v0, v1

    .line 2618833
    if-eqz v0, :cond_2

    .line 2618834
    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;LX/Iqg;LX/Iqg;)V

    .line 2618835
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    new-instance v2, LX/Irr;

    invoke-direct {v2, p0}, LX/Irr;-><init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2618836
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2618837
    new-instance v2, Landroid/view/GestureDetector;

    new-instance v3, LX/Irn;

    invoke-direct {v3, p0}, LX/Irn;-><init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V

    invoke-direct {v2, v0, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v2, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->p:Landroid/view/GestureDetector;

    .line 2618838
    new-instance v2, LX/Irc;

    new-instance v3, LX/Irp;

    invoke-direct {v3, p0}, LX/Irp;-><init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V

    invoke-direct {v2, v0, v3}, LX/Irc;-><init>(Landroid/content/Context;LX/Irb;)V

    iput-object v2, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->q:LX/Irc;

    .line 2618839
    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->q:LX/Irc;

    new-instance v3, LX/Iri;

    invoke-direct {v3, p0}, LX/Iri;-><init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V

    invoke-virtual {v2, v5, v5, v3}, LX/Irc;->a(ZZLX/Iri;)V

    .line 2618840
    new-instance v2, LX/IrY;

    new-instance v3, LX/Iro;

    invoke-direct {v3, p0}, LX/Iro;-><init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V

    invoke-direct {v2, v0, v3}, LX/IrY;-><init>(Landroid/content/Context;LX/IrX;)V

    iput-object v2, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->r:LX/IrY;

    .line 2618841
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->r:LX/IrY;

    new-instance v1, LX/Irj;

    invoke-direct {v1, p0}, LX/Irj;-><init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V

    .line 2618842
    iput-object v1, v0, LX/IrY;->c:LX/Irj;

    .line 2618843
    iput-boolean v5, v0, LX/IrY;->f:Z

    .line 2618844
    iput-boolean v5, v0, LX/IrY;->g:Z

    .line 2618845
    iget-boolean v2, v0, LX/IrY;->f:Z

    if-eqz v2, :cond_3

    iget-object v2, v0, LX/IrY;->L:Landroid/view/GestureDetector;

    if-nez v2, :cond_3

    .line 2618846
    new-instance v2, LX/IrW;

    invoke-direct {v2, v0}, LX/IrW;-><init>(LX/IrY;)V

    .line 2618847
    new-instance v3, Landroid/view/GestureDetector;

    iget-object v4, v0, LX/IrY;->a:Landroid/content/Context;

    iget-object p0, v0, LX/IrY;->K:Landroid/os/Handler;

    invoke-direct {v3, v4, v2, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    iput-object v3, v0, LX/IrY;->L:Landroid/view/GestureDetector;

    .line 2618848
    :cond_3
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 2618807
    iput p1, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->u:I

    .line 2618808
    iput p2, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->t:I

    .line 2618809
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->j(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2618810
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->j:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2618811
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2618812
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2618813
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Iqg;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;LX/Iqg;I)V

    .line 2618814
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2618815
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2618819
    invoke-virtual {p0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2618820
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    check-cast v0, LX/IsJ;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/IsJ;->d(Z)V

    .line 2618821
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2618816
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    instance-of v0, v0, LX/IsJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    check-cast v0, LX/IsJ;

    .line 2618817
    iget-boolean p0, v0, LX/IsJ;->k:Z

    move v0, p0

    .line 2618818
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
