.class public Lcom/facebook/messaging/photos/editing/StickerPackAdapter;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/IqQ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2619317
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2619318
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2619319
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2619320
    const v1, 0x7f0313c4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2619321
    new-instance v1, LX/Is8;

    invoke-direct {v1, v0}, LX/Is8;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2619322
    check-cast p1, LX/Is8;

    .line 2619323
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerPackAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/StickerPack;

    .line 2619324
    iget-object v1, p1, LX/Is8;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2619325
    iget-object v2, v0, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 2619326
    const-class v3, LX/Is8;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2619327
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v1, LX/Is5;

    invoke-direct {v1, p0, p2}, LX/Is5;-><init>(Lcom/facebook/messaging/photos/editing/StickerPackAdapter;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2619328
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/StickerPack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2619329
    iput-object p1, p0, Lcom/facebook/messaging/photos/editing/StickerPackAdapter;->a:Ljava/util/List;

    .line 2619330
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2619331
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2619332
    const/4 v0, 0x2

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2619333
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerPackAdapter;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerPackAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
