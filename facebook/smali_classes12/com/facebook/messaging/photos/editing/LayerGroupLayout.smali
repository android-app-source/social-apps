.class public Lcom/facebook/messaging/photos/editing/LayerGroupLayout;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# instance fields
.field private a:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2617326
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2617327
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2617324
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2617325
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2617320
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2617321
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->setChildrenDrawingOrderEnabled(Z)V

    .line 2617322
    invoke-virtual {p0, p0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 2617323
    return-void
.end method


# virtual methods
.method public final bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 2617319
    invoke-virtual {p0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 2617318
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x11

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    return-object v0
.end method

.method public final getChildDrawingOrder(II)I
    .locals 2

    .prologue
    .line 2617311
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->a:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2617312
    :cond_0
    :goto_0
    return p2

    .line 2617313
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 2617314
    add-int/lit8 v1, p1, -0x1

    if-ne p2, v1, :cond_2

    move p2, v0

    .line 2617315
    goto :goto_0

    .line 2617316
    :cond_2
    if-lt p2, v0, :cond_0

    .line 2617317
    add-int/lit8 p2, p2, 0x1

    goto :goto_0
.end method

.method public final onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2617310
    return-void
.end method

.method public final onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2617302
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->a:Landroid/view/View;

    if-ne p2, v0, :cond_0

    .line 2617303
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->a:Landroid/view/View;

    .line 2617304
    :cond_0
    return-void
.end method

.method public setTopView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2617305
    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2617306
    iput-object p1, p0, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->a:Landroid/view/View;

    .line 2617307
    invoke-virtual {p0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->invalidate()V

    .line 2617308
    return-void

    .line 2617309
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
