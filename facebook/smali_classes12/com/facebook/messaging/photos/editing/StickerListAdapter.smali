.class public Lcom/facebook/messaging/photos/editing/StickerListAdapter;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/stickers/model/StickerPack;

.field private b:LX/8jY;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/stickers/model/Sticker;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/IqR;


# direct methods
.method public constructor <init>(LX/8jY;)V
    .locals 0

    .prologue
    .line 2619213
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2619214
    iput-object p1, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->b:LX/8jY;

    .line 2619215
    return-void
.end method

.method public static d(Lcom/facebook/messaging/photos/editing/StickerListAdapter;)V
    .locals 3

    .prologue
    .line 2619216
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->b:LX/8jY;

    invoke-virtual {v0}, LX/8jY;->a()V

    .line 2619217
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->b:LX/8jY;

    new-instance v1, LX/Is3;

    invoke-direct {v1, p0}, LX/Is3;-><init>(Lcom/facebook/messaging/photos/editing/StickerListAdapter;)V

    .line 2619218
    iput-object v1, v0, LX/8jY;->e:LX/3Mb;

    .line 2619219
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->b:LX/8jY;

    new-instance v1, LX/8jW;

    iget-object v2, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 2619220
    iget-object p0, v2, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object v2, p0

    .line 2619221
    invoke-direct {v1, v2}, LX/8jW;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, LX/8jY;->a(LX/8jW;)V

    .line 2619222
    return-void
.end method

.method private e()I
    .locals 1

    .prologue
    .line 2619223
    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->f(Lcom/facebook/messaging/photos/editing/StickerListAdapter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2619224
    const/4 v0, 0x2

    .line 2619225
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static f(Lcom/facebook/messaging/photos/editing/StickerListAdapter;)Z
    .locals 1

    .prologue
    .line 2619226
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->a:Lcom/facebook/stickers/model/StickerPack;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 2619227
    iget-object p0, v0, Lcom/facebook/stickers/model/StickerPack;->p:LX/0Px;

    move-object v0, p0

    .line 2619228
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2619229
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2619230
    if-nez p2, :cond_0

    .line 2619231
    const v1, 0x7f0313c5

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2619232
    new-instance v0, LX/Is7;

    invoke-direct {v0, v1}, LX/Is7;-><init>(Landroid/view/View;)V

    .line 2619233
    :goto_0
    return-object v0

    .line 2619234
    :cond_0
    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    .line 2619235
    const v1, 0x7f0313c1

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2619236
    new-instance v0, LX/IsA;

    invoke-direct {v0, v1}, LX/IsA;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2619237
    :cond_1
    const/4 v1, 0x2

    if-ne p2, v1, :cond_2

    .line 2619238
    const v1, 0x7f0313c3

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2619239
    new-instance v0, LX/Is6;

    invoke-direct {v0, v1}, LX/Is6;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2619240
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 7

    .prologue
    .line 2619241
    instance-of v0, p1, LX/Is7;

    if-eqz v0, :cond_2

    .line 2619242
    check-cast p1, LX/Is7;

    .line 2619243
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 2619244
    if-eqz v0, :cond_0

    .line 2619245
    iget-object v1, v0, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    move-object v1, v1

    .line 2619246
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2619247
    :cond_0
    iget-object v1, p1, LX/Is7;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2619248
    :goto_0
    iget-object v1, p1, LX/Is7;->m:Landroid/widget/TextView;

    .line 2619249
    iget-object v2, v0, Lcom/facebook/stickers/model/StickerPack;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2619250
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2619251
    iget-object v1, p1, LX/Is7;->n:Landroid/widget/TextView;

    .line 2619252
    iget-object v2, v0, Lcom/facebook/stickers/model/StickerPack;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2619253
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2619254
    iget-object v1, p1, LX/Is7;->o:Landroid/widget/TextView;

    .line 2619255
    iget-object v2, v0, Lcom/facebook/stickers/model/StickerPack;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2619256
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2619257
    :cond_1
    :goto_1
    return-void

    .line 2619258
    :cond_2
    instance-of v0, p1, LX/Is6;

    if-eqz v0, :cond_5

    .line 2619259
    check-cast p1, LX/Is6;

    .line 2619260
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->a:Lcom/facebook/stickers/model/StickerPack;

    const/4 v3, 0x0

    .line 2619261
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2619262
    const/4 v2, 0x1

    .line 2619263
    iget-object v1, v0, Lcom/facebook/stickers/model/StickerPack;->p:LX/0Px;

    move-object v6, v1

    .line 2619264
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p0

    move v4, v3

    :goto_2
    if-ge v4, p0, :cond_4

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2619265
    if-nez v2, :cond_3

    .line 2619266
    const/16 p2, 0xa

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2619267
    :goto_3
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2619268
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_3
    move v2, v3

    .line 2619269
    goto :goto_3

    .line 2619270
    :cond_4
    iget-object v1, p1, LX/Is6;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2619271
    goto :goto_1

    .line 2619272
    :cond_5
    instance-of v0, p1, LX/IsA;

    if-eqz v0, :cond_1

    .line 2619273
    add-int/lit8 v1, p2, -0x1

    .line 2619274
    check-cast p1, LX/IsA;

    .line 2619275
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->c:Ljava/util/List;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2619276
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/stickers/model/Sticker;

    .line 2619277
    iget-object v2, p1, LX/IsA;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, v0, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    const-class v4, LX/IsA;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2619278
    :goto_4
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v2, LX/Is4;

    invoke-direct {v2, p0, v1}, LX/Is4;-><init>(Lcom/facebook/messaging/photos/editing/StickerListAdapter;I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2619279
    :cond_6
    iget-object v0, p1, LX/IsA;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v2, 0x7f020e4e

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 2619280
    goto :goto_4

    .line 2619281
    :cond_7
    iget-object v1, p1, LX/Is7;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2619282
    iget-object v2, v0, Lcom/facebook/stickers/model/StickerPack;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 2619283
    const-class p0, LX/Is7;

    invoke-static {p0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p0

    invoke-virtual {v1, v2, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2619284
    if-nez p1, :cond_0

    .line 2619285
    const/4 v0, 0x0

    .line 2619286
    :goto_0
    return v0

    .line 2619287
    :cond_0
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    invoke-static {p0}, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->f(Lcom/facebook/messaging/photos/editing/StickerListAdapter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2619288
    const/4 v0, 0x2

    goto :goto_0

    .line 2619289
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2619290
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2619291
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->e()I

    move-result v1

    add-int/2addr v0, v1

    .line 2619292
    :goto_0
    return v0

    .line 2619293
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->a:Lcom/facebook/stickers/model/StickerPack;

    if-eqz v0, :cond_1

    .line 2619294
    iget-object v0, p0, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->a:Lcom/facebook/stickers/model/StickerPack;

    .line 2619295
    iget-object v1, v0, Lcom/facebook/stickers/model/StickerPack;->q:LX/0Px;

    move-object v0, v1

    .line 2619296
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/facebook/messaging/photos/editing/StickerListAdapter;->e()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 2619297
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
