.class public Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile A:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0aG;

.field public final c:LX/2Ox;

.field public final d:LX/2P3;

.field private final e:LX/FDp;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/2Ow;

.field private final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Landroid/content/res/Resources;

.field public final k:LX/2P4;

.field public final l:LX/IuH;

.field private final m:LX/IuU;

.field private final n:LX/3QU;

.field private final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/2P0;

.field private final q:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

.field private final r:LX/2PA;

.field private final s:LX/IuT;

.field private final t:Lcom/facebook/messaging/tincan/outbound/Sender;

.field private final u:LX/IuF;

.field public final v:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

.field private final w:LX/2PM;

.field public final x:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/2PE;

.field public final z:LX/0qK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2625570
    const-class v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    sput-object v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/2Ox;LX/2P3;LX/FDp;LX/0Or;LX/0Or;LX/2Ow;LX/0Or;Landroid/content/res/Resources;LX/2P4;LX/IuH;LX/34G;LX/3QU;LX/0Or;LX/2P0;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/2PA;Lcom/facebook/messaging/tincan/outbound/Sender;LX/IuF;Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;LX/2PM;LX/0Or;LX/2PE;LX/0SG;LX/1Hr;)V
    .locals 6
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/cache/TincanMessages;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/2Ox;",
            "LX/2P3;",
            "LX/FDp;",
            "LX/0Or",
            "<",
            "LX/2Oe;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/messenger/MessageExpirationHelper;",
            ">;",
            "LX/2Ow;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/res/Resources;",
            "LX/2P4;",
            "LX/IuH;",
            "LX/34G;",
            "LX/3QU;",
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;",
            "LX/2P0;",
            "Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;",
            "LX/2PA;",
            "Lcom/facebook/messaging/tincan/outbound/Sender;",
            "Lcom/facebook/messaging/tincan/inbound/PacketIdFactory;",
            "Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;",
            "LX/2PM;",
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;",
            ">;",
            "LX/2PE;",
            "LX/0SG;",
            "LX/1Hr;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2625619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625620
    iput-object p1, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b:LX/0aG;

    .line 2625621
    iput-object p2, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    .line 2625622
    iput-object p3, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->d:LX/2P3;

    .line 2625623
    iput-object p4, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->e:LX/FDp;

    .line 2625624
    iput-object p5, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->f:LX/0Or;

    .line 2625625
    iput-object p6, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->g:LX/0Or;

    .line 2625626
    iput-object p7, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->h:LX/2Ow;

    .line 2625627
    iput-object p8, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->i:LX/0Or;

    .line 2625628
    iput-object p9, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->j:Landroid/content/res/Resources;

    .line 2625629
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->k:LX/2P4;

    .line 2625630
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->l:LX/IuH;

    .line 2625631
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->n:LX/3QU;

    .line 2625632
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->o:LX/0Or;

    .line 2625633
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->p:LX/2P0;

    .line 2625634
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->q:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    .line 2625635
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->r:LX/2PA;

    .line 2625636
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->t:Lcom/facebook/messaging/tincan/outbound/Sender;

    .line 2625637
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->u:LX/IuF;

    .line 2625638
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->v:Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    .line 2625639
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->w:LX/2PM;

    .line 2625640
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->x:LX/0Or;

    .line 2625641
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->y:LX/2PE;

    .line 2625642
    new-instance v2, LX/IuU;

    move-object/from16 v0, p12

    invoke-direct {v2, p2, v0}, LX/IuU;-><init>(LX/2Ox;LX/34G;)V

    iput-object v2, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->m:LX/IuU;

    .line 2625643
    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->t:Lcom/facebook/messaging/tincan/outbound/Sender;

    invoke-virtual {v2, p0}, LX/2PI;->a(Ljava/lang/Object;)V

    .line 2625644
    new-instance v2, LX/IuT;

    move-object/from16 v0, p25

    invoke-direct {v2, v0}, LX/IuT;-><init>(LX/1Hr;)V

    iput-object v2, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->s:LX/IuT;

    .line 2625645
    new-instance v2, LX/0qK;

    const/4 v3, 0x2

    const-wide/32 v4, 0x36ee80

    move-object/from16 v0, p24

    invoke-direct {v2, v0, v3, v4, v5}, LX/0qK;-><init>(LX/0SG;IJ)V

    iput-object v2, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->z:LX/0qK;

    .line 2625646
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;
    .locals 3

    .prologue
    .line 2625609
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->A:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    if-nez v0, :cond_1

    .line 2625610
    const-class v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    monitor-enter v1

    .line 2625611
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->A:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2625612
    if-eqz v2, :cond_0

    .line 2625613
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->A:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2625614
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2625615
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2625616
    :cond_1
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->A:Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    return-object v0

    .line 2625617
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2625618
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/DpY;JJLjava/lang/String;)V
    .locals 10

    .prologue
    .line 2625597
    iget-object v0, p1, LX/DpY;->body:LX/DpZ;

    invoke-virtual {v0}, LX/DpZ;->g()LX/Dpl;

    move-result-object v0

    .line 2625598
    :try_start_0
    new-instance v6, LX/EbB;

    iget-object v0, v0, LX/Dpl;->sender_key:[B

    invoke-direct {v6, v0}, LX/EbB;-><init>([B)V

    .line 2625599
    new-instance v7, LX/Eax;

    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->y:LX/2PE;

    invoke-direct {v7, v0}, LX/Eax;-><init>(LX/2PE;)V

    .line 2625600
    invoke-static/range {p4 .. p6}, LX/DoM;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2625601
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-wide v0, p2

    move-wide v4, p4

    .line 2625602
    invoke-static/range {v0 .. v5}, LX/Dpq;->a(JJJ)J

    move-result-wide v0

    .line 2625603
    new-instance v2, LX/Eay;

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/Eap;

    const/4 v3, 0x0

    invoke-direct {v1, v8, v3}, LX/Eap;-><init>(Ljava/lang/String;I)V

    invoke-direct {v2, v0, v1}, LX/Eay;-><init>(Ljava/lang/String;LX/Eap;)V

    .line 2625604
    invoke-virtual {v7, v2, v6}, LX/Eax;->a(LX/Eay;LX/EbB;)V
    :try_end_0
    .catch LX/Eai; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/Eak; {:try_start_0 .. :try_end_0} :catch_0

    .line 2625605
    :goto_0
    return-void

    .line 2625606
    :catch_0
    move-exception v0

    .line 2625607
    :goto_1
    sget-object v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a:Ljava/lang/Class;

    const-string v2, "Unable to decode SenderKeyMessage salamander"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2625608
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dp5;Ljava/lang/String;LX/DpY;)V
    .locals 10

    .prologue
    .line 2625580
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v9

    .line 2625581
    :try_start_0
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->d:LX/2P3;

    iget-object v0, p2, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->msg_from:LX/DpM;

    iget-object v0, v0, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p2, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->date_micros:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v2, p1

    move-object v3, p3

    move-object v8, p4

    invoke-virtual/range {v1 .. v9}, LX/2P3;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JJLX/DpY;LX/6f7;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2625582
    :goto_0
    invoke-virtual {v9}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2625583
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->K:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 2625584
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->K:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/2Ox;->a(Ljava/lang/String;J)V

    .line 2625585
    :cond_0
    invoke-static {p0, v0}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2625586
    iget-object v1, p2, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->body:LX/DpO;

    invoke-virtual {v1}, LX/DpO;->g()LX/Dpa;

    move-result-object v1

    iget-object v1, v1, LX/Dpa;->has_prekey_material:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2625587
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2625588
    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->k:LX/2P4;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/2P4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v3

    .line 2625589
    sget-object v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->a:Lcom/facebook/messaging/service/model/FetchThreadResult;

    if-eq v3, v2, :cond_1

    .line 2625590
    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->f:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Oe;

    invoke-virtual {v2, v3}, LX/2Oe;->b(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2625591
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->h:LX/2Ow;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v0}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2625592
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->h:LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->a()V

    .line 2625593
    :cond_2
    return-void

    .line 2625594
    :catch_0
    move-exception v0

    .line 2625595
    sget-object v1, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a:Ljava/lang/Class;

    const-string v2, "Received salamander decoded with invalid body"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2625596
    const/16 v0, 0x1996

    invoke-static {p0, v0, p2}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;ILX/Dp5;)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;ILX/Dp5;)V
    .locals 6

    .prologue
    .line 2625578
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->t:Lcom/facebook/messaging/tincan/outbound/Sender;

    iget-object v1, p2, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->msg_from:LX/DpM;

    iget-object v2, p2, LX/Dp5;->b:LX/DpN;

    iget-object v2, v2, LX/DpN;->version:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v2, p2, LX/Dp5;->b:LX/DpN;

    iget-object v4, v2, LX/DpN;->nonce:[B

    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->u:LX/IuF;

    invoke-virtual {v2}, LX/IuF;->a()[B

    move-result-object v5

    move v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(LX/DpM;II[B[B)V

    .line 2625579
    return-void
.end method

.method private static a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2625571
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->m:LX/IuU;

    invoke-virtual {v0, p1}, LX/IuU;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2625572
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-wide v6, p1, Lcom/facebook/messaging/model/messages/Message;->c:J

    move-object v3, p1

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2625573
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    invoke-virtual {v0, v1}, LX/2Oe;->b(Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2625574
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->h:LX/2Ow;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2625575
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->n:LX/3QU;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v3, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    new-instance v4, Lcom/facebook/push/PushProperty;

    sget-object v1, LX/3B4;->TINCAN:LX/3B4;

    invoke-direct {v4, v1}, Lcom/facebook/push/PushProperty;-><init>(LX/3B4;)V

    sget-object v5, LX/03R;->YES:LX/03R;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/3QU;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadCustomization;Lcom/facebook/push/PushProperty;LX/03R;)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v1

    .line 2625576
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Di5;

    invoke-virtual {v0, v1}, LX/Di5;->a(Lcom/facebook/messaging/notify/NewMessageNotification;)V

    .line 2625577
    return-void
.end method

.method private static a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 2625567
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->j:Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Ljava/lang/String;Ljava/lang/String;)V

    .line 2625568
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->j:Landroid/content/res/Resources;

    const v1, 0x7f0806c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Ljava/lang/String;Ljava/lang/String;)V

    .line 2625569
    return-void
.end method

.method public static a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2625647
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2625648
    const-string v0, "message_id"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2625649
    const-string v0, "error_text"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2625650
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b:LX/0aG;

    const-string v1, "TincanSetRetryableSendError"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x35cdde46

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2625651
    return-void
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;
    .locals 28

    .prologue
    .line 2625565
    new-instance v2, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static/range {p0 .. p0}, LX/2Ox;->a(LX/0QB;)LX/2Ox;

    move-result-object v4

    check-cast v4, LX/2Ox;

    invoke-static/range {p0 .. p0}, LX/2P3;->a(LX/0QB;)LX/2P3;

    move-result-object v5

    check-cast v5, LX/2P3;

    invoke-static/range {p0 .. p0}, LX/FDp;->a(LX/0QB;)LX/FDp;

    move-result-object v6

    check-cast v6, LX/FDp;

    const/16 v7, 0xce7

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0xdd1

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v9

    check-cast v9, LX/2Ow;

    const/16 v10, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v11

    check-cast v11, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/2P4;->a(LX/0QB;)LX/2P4;

    move-result-object v12

    check-cast v12, LX/2P4;

    invoke-static/range {p0 .. p0}, LX/IuH;->a(LX/0QB;)LX/IuH;

    move-result-object v13

    check-cast v13, LX/IuH;

    invoke-static/range {p0 .. p0}, LX/34G;->a(LX/0QB;)LX/34G;

    move-result-object v14

    check-cast v14, LX/34G;

    invoke-static/range {p0 .. p0}, LX/3QU;->a(LX/0QB;)LX/3QU;

    move-result-object v15

    check-cast v15, LX/3QU;

    const/16 v16, 0x2847

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    invoke-static/range {p0 .. p0}, LX/2P0;->a(LX/0QB;)LX/2P0;

    move-result-object v17

    check-cast v17, LX/2P0;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    move-result-object v18

    check-cast v18, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-static/range {p0 .. p0}, LX/2PA;->a(LX/0QB;)LX/2PA;

    move-result-object v19

    check-cast v19, LX/2PA;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/tincan/outbound/Sender;->a(LX/0QB;)Lcom/facebook/messaging/tincan/outbound/Sender;

    move-result-object v20

    check-cast v20, Lcom/facebook/messaging/tincan/outbound/Sender;

    invoke-static/range {p0 .. p0}, LX/IuF;->a(LX/0QB;)LX/IuF;

    move-result-object v21

    check-cast v21, LX/IuF;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;->a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    move-result-object v22

    check-cast v22, Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;

    invoke-static/range {p0 .. p0}, LX/2PM;->a(LX/0QB;)LX/2PM;

    move-result-object v23

    check-cast v23, LX/2PM;

    const/16 v24, 0x2a31

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v24

    invoke-static/range {p0 .. p0}, LX/2PE;->a(LX/0QB;)LX/2PE;

    move-result-object v25

    check-cast v25, LX/2PE;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v26

    check-cast v26, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/1Hr;->a(LX/0QB;)LX/1Hr;

    move-result-object v27

    check-cast v27, LX/1Hr;

    invoke-direct/range {v2 .. v27}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;-><init>(LX/0aG;LX/2Ox;LX/2P3;LX/FDp;LX/0Or;LX/0Or;LX/2Ow;LX/0Or;Landroid/content/res/Resources;LX/2P4;LX/IuH;LX/34G;LX/3QU;LX/0Or;LX/2P0;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/2PA;Lcom/facebook/messaging/tincan/outbound/Sender;LX/IuF;Lcom/facebook/messaging/tincan/messenger/TincanMessengerErrorGenerator;LX/2PM;LX/0Or;LX/2PE;LX/0SG;LX/1Hr;)V

    .line 2625566
    return-object v2
.end method

.method public static b(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2625459
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2625460
    const-string v0, "message_id"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2625461
    const-string v0, "message"

    invoke-virtual {v2, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2625462
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b:LX/0aG;

    const-string v1, "TincanAdminMessageForMessage"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x1a715d3e

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2625463
    return-void
.end method

.method public static d([B)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2625558
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;

    move-result-object v0

    .line 2625559
    array-length v1, p0

    invoke-static {v1}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v1

    .line 2625560
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v3}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;Ljava/nio/CharBuffer;Z)Ljava/nio/charset/CoderResult;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/charset/CoderResult;->isError()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2625561
    const/4 v0, 0x0

    .line 2625562
    :goto_0
    return-object v0

    .line 2625563
    :cond_0
    invoke-virtual {v0, v1}, Ljava/nio/charset/CharsetDecoder;->flush(Ljava/nio/CharBuffer;)Ljava/nio/charset/CoderResult;

    .line 2625564
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    goto :goto_0
.end method


# virtual methods
.method public final a(JJ)V
    .locals 7

    .prologue
    .line 2625553
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2625554
    const-string v0, "from_user_id"

    invoke-virtual {v2, v0, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2625555
    const-string v0, "timestamp_us"

    const-wide/16 v4, 0x3e8

    sub-long v4, p3, v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2625556
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b:LX/0aG;

    const-string v1, "TincanOtherDeviceSwitched"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x20038e74

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2625557
    return-void
.end method

.method public final a(LX/Dp5;[B)V
    .locals 10

    .prologue
    .line 2625525
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2625526
    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v6, v0, LX/DpN;->msg_from:LX/DpM;

    .line 2625527
    iget-object v7, p1, LX/Dp5;->a:Ljava/lang/String;

    .line 2625528
    invoke-static {p2}, LX/Dpn;->b([B)LX/DpY;

    move-result-object v8

    .line 2625529
    if-nez v8, :cond_1

    .line 2625530
    const/16 v0, 0x1982

    invoke-static {p0, v0, p1}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;ILX/Dp5;)V

    .line 2625531
    :goto_1
    return-void

    .line 2625532
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2625533
    :cond_1
    iget-object v0, v8, LX/DpY;->type:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    sget-object v0, LX/Dpc;->a:LX/1sn;

    iget-object v1, v8, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2625534
    :cond_2
    const/16 v0, 0x198c

    invoke-static {p0, v0, p1}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;ILX/Dp5;)V

    goto :goto_1

    .line 2625535
    :cond_3
    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->body:LX/DpO;

    invoke-virtual {v0}, LX/DpO;->g()LX/Dpa;

    move-result-object v9

    .line 2625536
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2625537
    iget-object v0, v9, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2625538
    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->thread_fbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v4, v6, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, LX/Dpq;->a(JJJ)J

    move-result-wide v0

    .line 2625539
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2625540
    :goto_2
    iget-object v1, p1, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_5

    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->s:LX/IuT;

    iget-object v4, v8, LX/DpY;->sender_hmac_key:[B

    iget-object v5, v9, LX/Dpa;->sender_hmac:[B

    invoke-virtual {v1, p2, v4, v5}, LX/IuT;->a([B[B[B)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2625541
    const/16 v1, 0x1978

    invoke-static {p0, v1, p1}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;ILX/Dp5;)V

    .line 2625542
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    invoke-static {v7}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;)V

    goto :goto_1

    .line 2625543
    :cond_4
    iget-object v0, v6, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_2

    .line 2625544
    :cond_5
    if-eqz v8, :cond_6

    iget-object v1, v8, LX/DpY;->type:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 2625545
    iget-object v1, v8, LX/DpY;->type:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 2625546
    :cond_6
    iget-object v1, v9, LX/Dpa;->is_multi:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2625547
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    iget-wide v4, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5, v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2625548
    :cond_7
    invoke-direct {p0, v0, p1, v7, v8}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/Dp5;Ljava/lang/String;LX/DpY;)V

    goto/16 :goto_1

    .line 2625549
    :sswitch_0
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    invoke-virtual {v1, v0}, LX/2Ox;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2625550
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    invoke-static {v7}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;)V

    .line 2625551
    goto/16 :goto_1

    .line 2625552
    :sswitch_1
    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->thread_fbid:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, v6, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, v6, LX/DpM;->instance_id:Ljava/lang/String;

    move-object v0, p0

    move-object v1, v8

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(LX/DpY;JJLjava/lang/String;)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x7 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 2625517
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->l:LX/IuH;

    invoke-virtual {v0, p1}, LX/IuH;->a(Ljava/lang/String;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2625518
    if-nez p2, :cond_0

    .line 2625519
    invoke-static {p1}, LX/IuH;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2625520
    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    iget-wide v4, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-virtual {v2, v4, v5, v0}, LX/2Ox;->a(JLjava/lang/String;)V

    .line 2625521
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    invoke-virtual {v0, v1, v3}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Z)V

    .line 2625522
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    invoke-virtual {v0, v1, v3}, LX/2Oe;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Z)V

    .line 2625523
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->h:LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->a()V

    .line 2625524
    return-void
.end method

.method public final b(LX/Dp5;)V
    .locals 14

    .prologue
    .line 2625482
    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x12c

    if-ne v0, v1, :cond_1

    .line 2625483
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->q:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-virtual {v0}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->d()Z

    .line 2625484
    :cond_0
    :goto_0
    return-void

    .line 2625485
    :cond_1
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->body:LX/DpO;

    invoke-virtual {v1}, LX/DpO;->d()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 2625486
    iget-object v1, p1, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x1b58

    if-ne v1, v2, :cond_2

    .line 2625487
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->d:LX/2P3;

    iget-object v2, p1, LX/Dp5;->b:LX/DpN;

    iget-object v2, v2, LX/DpN;->msg_from:LX/DpM;

    iget-object v2, v2, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/2P3;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2625488
    iget-object v4, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->c:LX/2Ox;

    invoke-virtual {v4, v1}, LX/2Ox;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2625489
    iget-object v4, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->z:LX/0qK;

    invoke-virtual {v4}, LX/0qK;->a()Z

    move-result v4

    move v4, v4

    .line 2625490
    if-eqz v4, :cond_5

    .line 2625491
    iget-object v6, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->p:LX/2P0;

    invoke-virtual {v6, v0}, LX/2P0;->c(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v6

    .line 2625492
    if-nez v6, :cond_6

    .line 2625493
    :goto_1
    goto :goto_0

    .line 2625494
    :cond_2
    iget-object v1, p1, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x1770

    if-lt v1, v2, :cond_3

    iget-object v1, p1, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x1b57

    if-gt v1, v2, :cond_3

    .line 2625495
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2625496
    const-string v4, "message_id"

    invoke-virtual {v6, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2625497
    iget-object v4, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b:LX/0aG;

    const-string v5, "TincanSetSalamanderError"

    sget-object v7, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v8, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, -0x3d2dedbb

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    .line 2625498
    goto :goto_0

    .line 2625499
    :cond_3
    iget-object v1, p1, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x1c5

    if-ne v1, v2, :cond_4

    .line 2625500
    const v1, 0x7f0806c2

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 2625501
    :cond_4
    iget-object v1, p1, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->type:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x1c4

    if-ne v1, v2, :cond_0

    .line 2625502
    const v1, 0x7f0806c3

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 2625503
    :cond_5
    iget-object v4, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->j:Landroid/content/res/Resources;

    const v5, 0x7f0806c0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v0, v4}, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a(Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2625504
    :cond_6
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v6

    sget-object v7, LX/2uW;->PENDING_SEND:LX/2uW;

    .line 2625505
    iput-object v7, v6, LX/6f7;->l:LX/2uW;

    .line 2625506
    move-object v6, v6

    .line 2625507
    sget-object v7, Lcom/facebook/messaging/model/send/SendError;->a:Lcom/facebook/messaging/model/send/SendError;

    .line 2625508
    iput-object v7, v6, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 2625509
    move-object v6, v6

    .line 2625510
    invoke-virtual {v6}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v7

    .line 2625511
    iget-object v6, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->x:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;

    .line 2625512
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 2625513
    const-string v8, "message"

    invoke-virtual {v10, v8, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2625514
    iget-object v8, v6, Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;->a:LX/0aG;

    const-string v9, "TincanRetrySendMessage"

    sget-object v11, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v12, Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;

    invoke-static {v12}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v12

    const v13, -0x2b6a3bfa

    invoke-static/range {v8 .. v13}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v8

    invoke-interface {v8}, LX/1MF;->start()LX/1ML;

    move-result-object v8

    .line 2625515
    invoke-static {v6, v8}, Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;->a(Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;LX/1ML;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2625516
    goto/16 :goto_1
.end method

.method public final e(LX/Dp5;)V
    .locals 8

    .prologue
    .line 2625464
    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->body:LX/DpO;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/Dp5;->b:LX/DpN;

    iget-object v0, v0, LX/DpN;->body:LX/DpO;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, LX/6kT;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2625465
    :cond_0
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->a:Ljava/lang/Class;

    const-string v1, "Malformed primary device change payload"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2625466
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->w:LX/2PM;

    const-string v1, "Bad primary device change payload"

    invoke-virtual {v0, v1}, LX/2PM;->a(Ljava/lang/String;)V

    .line 2625467
    :cond_1
    :goto_0
    return-void

    .line 2625468
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->r:LX/2PA;

    const/4 v5, 0x0

    .line 2625469
    iget-object v1, v0, LX/2PA;->f:LX/03R;

    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v1, v2, :cond_3

    .line 2625470
    :try_start_0
    iget-object v1, v0, LX/2PA;->s:LX/0TD;

    new-instance v2, Lcom/facebook/messaging/tincan/messenger/TincanDeviceManager$3;

    invoke-direct {v2, v0}, Lcom/facebook/messaging/tincan/messenger/TincanDeviceManager$3;-><init>(LX/2PA;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    const v2, -0x1cb256c2

    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2625471
    :cond_3
    :goto_1
    iget-object v1, v0, LX/2PA;->f:LX/03R;

    invoke-virtual {v1, v5}, LX/03R;->asBoolean(Z)Z

    move-result v1

    move v0, v1

    .line 2625472
    iget-object v1, p1, LX/Dp5;->b:LX/DpN;

    iget-object v1, v1, LX/DpN;->body:LX/DpO;

    invoke-virtual {v1}, LX/DpO;->f()LX/DpT;

    move-result-object v1

    .line 2625473
    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->r:LX/2PA;

    iget-object v3, v1, LX/DpT;->msg_to:LX/DpM;

    iget-object v3, v3, LX/DpM;->user_id:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v3, v1, LX/DpT;->msg_to:LX/DpM;

    iget-object v3, v3, LX/DpM;->instance_id:Ljava/lang/String;

    .line 2625474
    iget-object v6, v2, LX/2PA;->p:LX/2PM;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string p1, "User "

    invoke-direct {v7, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string p1, " changed primary device"

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2PM;->a(Ljava/lang/String;)V

    .line 2625475
    iget-object v6, v2, LX/2PA;->q:LX/2PJ;

    invoke-virtual {v6}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2625476
    iget-object v6, v2, LX/2PA;->g:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Dof;

    sget-object v7, LX/2PA;->e:LX/2PC;

    const/4 p1, 0x0

    invoke-virtual {v6, v7, p1}, LX/Dod;->b(LX/2PC;Z)V

    .line 2625477
    sget-object v6, LX/03R;->NO:LX/03R;

    iput-object v6, v2, LX/2PA;->f:LX/03R;

    .line 2625478
    :cond_4
    if-eqz v0, :cond_1

    .line 2625479
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;->b:LX/0aG;

    const-string v1, "TincanDeviceBecameNonPrimary"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/tincan/messenger/IncomingMessageHandler;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x45d3ebc

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    goto/16 :goto_0

    .line 2625480
    :catch_0
    move-exception v1

    .line 2625481
    sget-object v2, LX/2PA;->a:Ljava/lang/String;

    const-string v3, "Failed to fetch primary device info from DB."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
