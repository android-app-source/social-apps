.class public Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:LX/0aG;

.field private final b:LX/0Xl;

.field public final c:LX/Iu9;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2625302
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0Xl;LX/Iu9;)V
    .locals 0
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2625297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625298
    iput-object p1, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->a:LX/0aG;

    .line 2625299
    iput-object p2, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->b:LX/0Xl;

    .line 2625300
    iput-object p3, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->c:LX/Iu9;

    .line 2625301
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;
    .locals 13

    .prologue
    .line 2625303
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2625304
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2625305
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2625306
    if-nez v1, :cond_0

    .line 2625307
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2625308
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2625309
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2625310
    sget-object v1, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2625311
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2625312
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2625313
    :cond_1
    if-nez v1, :cond_4

    .line 2625314
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2625315
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2625316
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2625317
    new-instance v9, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    .line 2625318
    new-instance p0, LX/Iu9;

    invoke-static {v0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v8

    check-cast v8, LX/16I;

    invoke-static {v0}, LX/3R7;->a(LX/0QB;)LX/3R7;

    move-result-object v10

    check-cast v10, LX/3R7;

    invoke-static {v0}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v12

    check-cast v12, LX/0So;

    invoke-direct {p0, v8, v10, v11, v12}, LX/Iu9;-><init>(LX/16I;LX/3R7;Ljava/util/concurrent/ScheduledExecutorService;LX/0So;)V

    .line 2625319
    move-object v8, p0

    .line 2625320
    check-cast v8, LX/Iu9;

    invoke-direct {v9, v1, v7, v8}, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;-><init>(LX/0aG;LX/0Xl;LX/Iu9;)V

    .line 2625321
    move-object v1, v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2625322
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2625323
    if-nez v1, :cond_2

    .line 2625324
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2625325
    :goto_1
    if-eqz v0, :cond_3

    .line 2625326
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2625327
    :goto_3
    check-cast v0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2625328
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2625329
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2625330
    :catchall_1
    move-exception v0

    .line 2625331
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2625332
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2625333
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2625334
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static b(Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;Lcom/facebook/messaging/media/upload/EncryptedPhotoUploadResult;)V
    .locals 6

    .prologue
    .line 2625293
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2625294
    const-string v0, "upload_status"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2625295
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->a:LX/0aG;

    const-string v1, "UpdateUploadStatus"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x17669ebd

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2625296
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2625291
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "EncryptedPhotoUploadStatusAction"

    new-instance v2, LX/Iu6;

    invoke-direct {v2, p0}, LX/Iu6;-><init>(Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2625292
    return-void
.end method
