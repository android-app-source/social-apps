.class public Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/2PC;

.field private static volatile f:Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;


# instance fields
.field private final b:LX/0aG;

.field public final c:LX/0TD;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Dof;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Sh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2625933
    new-instance v0, LX/2PC;

    const-string v1, "omnistore_global_version_id"

    invoke-direct {v0, v1}, LX/2PC;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->a:LX/2PC;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0TD;LX/0Or;LX/0Sh;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0TD;",
            "LX/0Or",
            "<",
            "LX/Dof;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2625955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625956
    iput-object p1, p0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->b:LX/0aG;

    .line 2625957
    iput-object p2, p0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->c:LX/0TD;

    .line 2625958
    iput-object p3, p0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->d:LX/0Or;

    .line 2625959
    iput-object p4, p0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->e:LX/0Sh;

    .line 2625960
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;
    .locals 7

    .prologue
    .line 2625942
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->f:Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;

    if-nez v0, :cond_1

    .line 2625943
    const-class v1, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;

    monitor-enter v1

    .line 2625944
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->f:Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2625945
    if-eqz v2, :cond_0

    .line 2625946
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2625947
    new-instance v6, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    const/16 v5, 0x2a14

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-direct {v6, v3, v4, p0, v5}, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;-><init>(LX/0aG;LX/0TD;LX/0Or;LX/0Sh;)V

    .line 2625948
    move-object v0, v6

    .line 2625949
    sput-object v0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->f:Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2625950
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2625951
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2625952
    :cond_1
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->f:Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;

    return-object v0

    .line 2625953
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2625954
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/messaging/tincan/omnistore/TincanMessage;)V
    .locals 6

    .prologue
    .line 2625934
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;->b:LX/0aG;

    const-string v1, "TincanNewMessage"

    .line 2625935
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2625936
    const-string v3, "packet_key"

    iget-object v4, p1, Lcom/facebook/messaging/tincan/omnistore/TincanMessage;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2625937
    const-string v3, "message_data"

    iget-object v4, p1, Lcom/facebook/messaging/tincan/omnistore/TincanMessage;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 2625938
    move-object v2, v2

    .line 2625939
    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/tincan/messenger/TincanIncomingDispatcher;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x29cbd4a4

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625940
    monitor-exit p0

    return-void

    .line 2625941
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
