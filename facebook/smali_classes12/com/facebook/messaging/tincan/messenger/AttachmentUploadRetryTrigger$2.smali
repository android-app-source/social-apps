.class public final Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Iu7;

.field public final synthetic b:LX/Iu9;


# direct methods
.method public constructor <init>(LX/Iu9;LX/Iu7;)V
    .locals 0

    .prologue
    .line 2625341
    iput-object p1, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$2;->b:LX/Iu9;

    iput-object p2, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$2;->a:LX/Iu7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2625342
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$2;->b:LX/Iu9;

    iget-object v0, v0, LX/Iu9;->b:LX/3R7;

    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$2;->a:LX/Iu7;

    iget-object v1, v1, LX/Iu7;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadRetryTrigger$2;->a:LX/Iu7;

    iget-object v2, v2, LX/Iu7;->b:Landroid/net/Uri;

    .line 2625343
    iget-object v3, v0, LX/3R7;->c:LX/2P0;

    .line 2625344
    invoke-static {v3, v1}, LX/2P0;->f(LX/2P0;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v6

    .line 2625345
    if-nez v6, :cond_2

    .line 2625346
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2625347
    :goto_0
    move-object v3, v4

    .line 2625348
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2625349
    iget-object v5, v3, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v2, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2625350
    invoke-static {v0, v3}, LX/3R7;->a(LX/3R7;Lcom/facebook/ui/media/attachments/MediaResource;)V

    goto :goto_1

    .line 2625351
    :cond_1
    return-void

    .line 2625352
    :cond_2
    iget-object v4, v3, LX/2P0;->i:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Dom;

    iget-object v5, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4, v5}, LX/Dom;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)[B

    move-result-object v5

    .line 2625353
    iget-object p0, v3, LX/2P0;->h:LX/2Oy;

    iget-object v4, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, [B

    invoke-virtual {p0, v5, v4}, LX/2Oy;->c([B[B)Ljava/lang/String;

    move-result-object v4

    .line 2625354
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2625355
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 2625356
    :cond_3
    iget-object v5, v3, LX/2P0;->c:LX/2NB;

    invoke-virtual {v5, v4}, LX/2NB;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    goto :goto_0
.end method
