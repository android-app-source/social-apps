.class public final Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final synthetic b:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 0

    .prologue
    .line 2625980
    iput-object p1, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;->b:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iput-object p2, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 2625981
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->TINCAN_MULTI_ENDPOINT:LX/5e9;

    if-ne v0, v1, :cond_1

    .line 2625982
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;->b:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iget-object v0, v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->h:LX/2PE;

    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2PE;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 2625983
    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;->b:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iget-object v1, v1, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->o:LX/2PL;

    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/2PL;->a([BLjava/util/List;)Z

    .line 2625984
    :cond_0
    :goto_0
    return-void

    .line 2625985
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->TINCAN:LX/5e9;

    if-ne v0, v1, :cond_0

    .line 2625986
    iget-object v0, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;->b:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    iget-object v0, v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->n:LX/2PK;

    iget-object v1, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager$3;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    const/4 v5, 0x0

    .line 2625987
    invoke-virtual {v0}, LX/2PI;->b()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2625988
    sget-object v4, LX/2PK;->b:Ljava/lang/Class;

    const-string v5, "No stored procedure sender for lookupPreKey"

    invoke-static {v4, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2625989
    :goto_1
    goto :goto_0

    .line 2625990
    :cond_2
    new-instance v6, LX/DpJ;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-direct {v6, v4}, LX/DpJ;-><init>(Ljava/lang/Long;)V

    .line 2625991
    new-instance v7, LX/DpM;

    iget-object v4, v0, LX/2PK;->e:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v8, v0, LX/2PK;->c:LX/2PJ;

    invoke-virtual {v8}, LX/2PJ;->a()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v4, v8}, LX/DpM;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    iget-object v4, v0, LX/2PK;->d:LX/0SF;

    invoke-virtual {v4}, LX/0SF;->a()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    const/16 v10, 0xa

    .line 2625992
    new-instance v4, LX/DpO;

    invoke-direct {v4}, LX/DpO;-><init>()V

    .line 2625993
    invoke-static {v4, v6}, LX/DpO;->b(LX/DpO;LX/DpJ;)V

    .line 2625994
    move-object v11, v4

    .line 2625995
    move-object v6, v5

    move-object v12, v1

    invoke-static/range {v5 .. v12}, LX/Dpm;->a(LX/Dpe;LX/DpM;LX/DpM;JILX/DpO;[B)LX/DpN;

    move-result-object v4

    .line 2625996
    invoke-static {v4}, LX/Dpn;->a(LX/1u2;)[B

    move-result-object v4

    invoke-virtual {v0, v4}, LX/2PI;->a([B)V

    .line 2625997
    goto :goto_1
.end method
