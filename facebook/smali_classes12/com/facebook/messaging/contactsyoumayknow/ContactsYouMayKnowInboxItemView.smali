.class public Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;
.super Landroid/support/v7/widget/CardView;
.source ""


# instance fields
.field public a:Landroid/view/LayoutInflater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Iez;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/facebook/resources/ui/FbImageButton;

.field private h:Landroid/view/View;

.field private i:LX/If6;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2599349
    invoke-direct {p0, p1}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;)V

    .line 2599350
    sget-object v0, LX/If6;->NEW_CONTACT:LX/If6;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->i:LX/If6;

    .line 2599351
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->a()V

    .line 2599352
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2599353
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2599354
    sget-object v0, LX/If6;->NEW_CONTACT:LX/If6;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->i:LX/If6;

    .line 2599355
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->a()V

    .line 2599356
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2599357
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2599358
    sget-object v0, LX/If6;->NEW_CONTACT:LX/If6;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->i:LX/If6;

    .line 2599359
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->a()V

    .line 2599360
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2599368
    const-class v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2599369
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f03036f

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2599370
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/CardView;->setUseCompatPadding(Z)V

    .line 2599371
    const v0, 0x7f0d037b

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->c:Landroid/widget/TextView;

    .line 2599372
    const v0, 0x7f0d0b2d

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->d:Landroid/widget/TextView;

    .line 2599373
    const v0, 0x7f0d0575

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2599374
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/If2;

    invoke-direct {v1, p0}, LX/If2;-><init>(Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2599375
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->setLongClickListener(Landroid/view/View;)V

    .line 2599376
    const v0, 0x7f0d0b2e

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->f:Landroid/widget/TextView;

    .line 2599377
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->f:Landroid/widget/TextView;

    new-instance v1, LX/If3;

    invoke-direct {v1, p0}, LX/If3;-><init>(Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2599378
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->f:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->setLongClickListener(Landroid/view/View;)V

    .line 2599379
    const v0, 0x7f0d0b2c

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    .line 2599380
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    new-instance v1, LX/If4;

    invoke-direct {v1, p0}, LX/If4;-><init>(Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2599381
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->setLongClickListener(Landroid/view/View;)V

    .line 2599382
    const v0, 0x7f0d0b2f

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->h:Landroid/view/View;

    .line 2599383
    return-void
.end method

.method private a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2599361
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2599362
    iget v0, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->b:I

    if-lez v0, :cond_0

    .line 2599363
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2599364
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0182

    iget v3, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->b:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2599365
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2599366
    return-void

    .line 2599367
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->a:Landroid/view/LayoutInflater;

    return-void
.end method

.method public static b$redex0(Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;)V
    .locals 1

    .prologue
    .line 2599346
    sget-object v0, LX/If6;->ADDING_CONTACT:LX/If6;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->i:LX/If6;

    .line 2599347
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->c()V

    .line 2599348
    return-void
.end method

.method private c()V
    .locals 0

    .prologue
    .line 2599342
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->e()V

    .line 2599343
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->f()V

    .line 2599344
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->g()V

    .line 2599345
    return-void
.end method

.method public static d(Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;)Z
    .locals 1

    .prologue
    .line 2599339
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->b:LX/Iez;

    if-eqz v0, :cond_0

    .line 2599340
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->b:LX/Iez;

    invoke-interface {v0, p0}, LX/Iez;->a(Landroid/view/View;)Z

    move-result v0

    .line 2599341
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2599335
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->i:LX/If6;

    sget-object v1, LX/If6;->NEW_CONTACT:LX/If6;

    if-ne v0, v1, :cond_0

    .line 2599336
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2599337
    :goto_0
    return-void

    .line 2599338
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->f:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2599331
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->i:LX/If6;

    sget-object v1, LX/If6;->NEW_CONTACT:LX/If6;

    if-ne v0, v1, :cond_0

    .line 2599332
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setVisibility(I)V

    .line 2599333
    :goto_0
    return-void

    .line 2599334
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2599327
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->i:LX/If6;

    sget-object v1, LX/If6;->ADDING_CONTACT:LX/If6;

    if-ne v0, v1, :cond_0

    .line 2599328
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2599329
    :goto_0
    return-void

    .line 2599330
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->h:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setLongClickListener(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2599325
    new-instance v0, LX/If5;

    invoke-direct {v0, p0}, LX/If5;-><init>(Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2599326
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V
    .locals 1

    .prologue
    .line 2599320
    if-eqz p2, :cond_0

    sget-object v0, LX/If6;->ADDING_CONTACT:LX/If6;

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->i:LX/If6;

    .line 2599321
    invoke-direct {p0, p1}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599322
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->c()V

    .line 2599323
    return-void

    .line 2599324
    :cond_0
    sget-object v0, LX/If6;->NEW_CONTACT:LX/If6;

    goto :goto_0
.end method

.method public setListener(LX/Iez;)V
    .locals 0

    .prologue
    .line 2599318
    iput-object p1, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowInboxItemView;->b:LX/Iez;

    .line 2599319
    return-void
.end method
