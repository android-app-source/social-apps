.class public Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;
.super Landroid/support/v7/widget/CardView;
.source ""


# instance fields
.field public a:Landroid/view/LayoutInflater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ieq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private f:Landroid/widget/TextView;

.field private g:Lcom/facebook/resources/ui/FbImageButton;

.field private h:Landroid/view/View;

.field private i:LX/4ob;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/IfK;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2599610
    invoke-direct {p0, p1}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;)V

    .line 2599611
    sget-object v0, LX/IfK;->NEW_CONTACT:LX/IfK;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->j:LX/IfK;

    .line 2599612
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->a()V

    .line 2599613
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2599606
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2599607
    sget-object v0, LX/IfK;->NEW_CONTACT:LX/IfK;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->j:LX/IfK;

    .line 2599608
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->a()V

    .line 2599609
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2599602
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2599603
    sget-object v0, LX/IfK;->NEW_CONTACT:LX/IfK;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->j:LX/IfK;

    .line 2599604
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->a()V

    .line 2599605
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2599587
    const-class v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2599588
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030f24

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2599589
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/CardView;->setUseCompatPadding(Z)V

    .line 2599590
    const v0, 0x7f0d037b

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->c:Landroid/widget/TextView;

    .line 2599591
    const v0, 0x7f0d0b2d

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->d:Landroid/widget/TextView;

    .line 2599592
    const v0, 0x7f0d0575

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2599593
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/IfG;

    invoke-direct {v1, p0}, LX/IfG;-><init>(Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2599594
    const v0, 0x7f0d0b2e

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->f:Landroid/widget/TextView;

    .line 2599595
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->f:Landroid/widget/TextView;

    new-instance v1, LX/IfH;

    invoke-direct {v1, p0}, LX/IfH;-><init>(Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2599596
    const v0, 0x7f0d0b2c

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    .line 2599597
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    new-instance v1, LX/IfI;

    invoke-direct {v1, p0}, LX/IfI;-><init>(Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2599598
    const v0, 0x7f0d0b2f

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->h:Landroid/view/View;

    .line 2599599
    const v0, 0x7f0d24bb

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->i:LX/4ob;

    .line 2599600
    new-instance v0, LX/IfJ;

    invoke-direct {v0, p0}, LX/IfJ;-><init>(Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2599601
    return-void
.end method

.method private a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2599580
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2599581
    iget v0, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->b:I

    if-lez v0, :cond_0

    .line 2599582
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2599583
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0182

    iget v3, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->b:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2599584
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2599585
    return-void

    .line 2599586
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->d:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->a:Landroid/view/LayoutInflater;

    return-void
.end method

.method public static b$redex0(Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;)V
    .locals 1

    .prologue
    .line 2599549
    sget-object v0, LX/IfK;->ADDING_CONTACT:LX/IfK;

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->j:LX/IfK;

    .line 2599550
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->c()V

    .line 2599551
    return-void
.end method

.method private c()V
    .locals 0

    .prologue
    .line 2599575
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->d()V

    .line 2599576
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->e()V

    .line 2599577
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->f()V

    .line 2599578
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->g()V

    .line 2599579
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2599571
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->j:LX/IfK;

    sget-object v1, LX/IfK;->NEW_CONTACT:LX/IfK;

    if-ne v0, v1, :cond_0

    .line 2599572
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2599573
    :goto_0
    return-void

    .line 2599574
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->f:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2599567
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->j:LX/IfK;

    sget-object v1, LX/IfK;->NEW_CONTACT:LX/IfK;

    if-ne v0, v1, :cond_0

    .line 2599568
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setVisibility(I)V

    .line 2599569
    :goto_0
    return-void

    .line 2599570
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->g:Lcom/facebook/resources/ui/FbImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2599563
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->j:LX/IfK;

    sget-object v1, LX/IfK;->CONTACT_ADDED:LX/IfK;

    if-ne v0, v1, :cond_0

    .line 2599564
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->i:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->e()V

    .line 2599565
    :goto_0
    return-void

    .line 2599566
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->i:LX/4ob;

    invoke-virtual {v0}, LX/4ob;->d()V

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2599559
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->j:LX/IfK;

    sget-object v1, LX/IfK;->ADDING_CONTACT:LX/IfK;

    if-ne v0, v1, :cond_0

    .line 2599560
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2599561
    :goto_0
    return-void

    .line 2599562
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->h:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;ZZ)V
    .locals 1

    .prologue
    .line 2599554
    if-eqz p2, :cond_0

    sget-object v0, LX/IfK;->CONTACT_ADDED:LX/IfK;

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->j:LX/IfK;

    .line 2599555
    invoke-direct {p0, p1}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599556
    invoke-direct {p0}, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->c()V

    .line 2599557
    return-void

    .line 2599558
    :cond_0
    if-eqz p3, :cond_1

    sget-object v0, LX/IfK;->ADDING_CONTACT:LX/IfK;

    goto :goto_0

    :cond_1
    sget-object v0, LX/IfK;->NEW_CONTACT:LX/IfK;

    goto :goto_0
.end method

.method public setListener(LX/Ieq;)V
    .locals 0

    .prologue
    .line 2599552
    iput-object p1, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowItemView;->b:LX/Ieq;

    .line 2599553
    return-void
.end method
