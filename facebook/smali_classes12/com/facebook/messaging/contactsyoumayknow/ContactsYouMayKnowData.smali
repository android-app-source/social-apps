.class public Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field public final b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2599156
    new-instance v0, LX/Iev;

    invoke-direct {v0}, LX/Iev;-><init>()V

    sput-object v0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 2599157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2599158
    iput-object p1, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->a:LX/0Px;

    .line 2599159
    iput-wide p2, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->b:J

    .line 2599160
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2599161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2599162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2599163
    const-class v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 2599164
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->a:LX/0Px;

    .line 2599165
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->b:J

    .line 2599166
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2599167
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2599168
    iget-object v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2599169
    iget-wide v0, p0, Lcom/facebook/messaging/contactsyoumayknow/ContactsYouMayKnowData;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2599170
    return-void
.end method
