.class public final Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2603987
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2603988
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2603985
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2603986
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 2603936
    if-nez p1, :cond_0

    .line 2603937
    :goto_0
    return v0

    .line 2603938
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2603939
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2603940
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2603941
    const v2, -0x489bc5fb

    const/4 v4, 0x0

    .line 2603942
    if-nez v1, :cond_1

    move v3, v4

    .line 2603943
    :goto_1
    move v1, v3

    .line 2603944
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2603945
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2603946
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2603947
    :sswitch_1
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2603948
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2603949
    const-class v0, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel$EdgesModel$MatchedMessagesModel$MessageSenderModel;

    invoke-virtual {p0, p1, v7, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel$EdgesModel$MatchedMessagesModel$MessageSenderModel;

    .line 2603950
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2603951
    invoke-virtual {p0, p1, v8}, LX/15i;->p(II)I

    move-result v2

    .line 2603952
    const v3, -0x17c98593

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2603953
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2603954
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2603955
    const/4 v4, 0x5

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2603956
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2603957
    const/4 v5, 0x6

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2603958
    invoke-virtual {p3, v6, v1}, LX/186;->b(II)V

    .line 2603959
    invoke-virtual {p3, v7, v0}, LX/186;->b(II)V

    .line 2603960
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 2603961
    invoke-virtual {p3, v9, v3}, LX/186;->b(II)V

    .line 2603962
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 2603963
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2603964
    :sswitch_2
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2603965
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2603966
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2603967
    invoke-virtual {p3, v6, v0}, LX/186;->b(II)V

    .line 2603968
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2603969
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2603970
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2603971
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2603972
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2603973
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2603974
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2603975
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2603976
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2603977
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v5

    .line 2603978
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 2603979
    :goto_2
    if-ge v4, v5, :cond_3

    .line 2603980
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result v7

    .line 2603981
    invoke-static {p0, v7, v2, p3}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v7

    aput v7, v3, v4

    .line 2603982
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2603983
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 2603984
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x489bc5fb -> :sswitch_1
        -0x1ca7b44f -> :sswitch_2
        -0x17c98593 -> :sswitch_3
        0x7ae5309e -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2603927
    if-nez p0, :cond_0

    move v0, v1

    .line 2603928
    :goto_0
    return v0

    .line 2603929
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2603930
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2603931
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2603932
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2603933
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2603934
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2603935
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2603920
    const/4 v7, 0x0

    .line 2603921
    const/4 v1, 0x0

    .line 2603922
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2603923
    invoke-static {v2, v3, v0}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2603924
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2603925
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2603926
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2603919
    new-instance v0, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2603914
    if-eqz p0, :cond_0

    .line 2603915
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 2603916
    if-eq v0, p0, :cond_0

    .line 2603917
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2603918
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2603899
    sparse-switch p2, :sswitch_data_0

    .line 2603900
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2603901
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2603902
    const v1, -0x489bc5fb

    .line 2603903
    if-eqz v0, :cond_0

    .line 2603904
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2603905
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2603906
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2603907
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2603908
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2603909
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 2603910
    :sswitch_2
    const/4 v0, 0x2

    const-class v1, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel$EdgesModel$MatchedMessagesModel$MessageSenderModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel$EdgesModel$MatchedMessagesModel$MessageSenderModel;

    .line 2603911
    invoke-static {v0, p3}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 2603912
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2603913
    const v1, -0x17c98593

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x489bc5fb -> :sswitch_2
        -0x1ca7b44f -> :sswitch_1
        -0x17c98593 -> :sswitch_1
        0x7ae5309e -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2603893
    if-eqz p1, :cond_0

    .line 2603894
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2603895
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;

    .line 2603896
    if-eq v0, v1, :cond_0

    .line 2603897
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2603898
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2603892
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2603989
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2603990
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2603887
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2603888
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2603889
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2603890
    iput p2, p0, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->b:I

    .line 2603891
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2603886
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2603885
    new-instance v0, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2603882
    iget v0, p0, LX/1vt;->c:I

    .line 2603883
    move v0, v0

    .line 2603884
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2603879
    iget v0, p0, LX/1vt;->c:I

    .line 2603880
    move v0, v0

    .line 2603881
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2603876
    iget v0, p0, LX/1vt;->b:I

    .line 2603877
    move v0, v0

    .line 2603878
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2603873
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2603874
    move-object v0, v0

    .line 2603875
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2603864
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2603865
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2603866
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2603867
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2603868
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2603869
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2603870
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2603871
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2603872
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2603861
    iget v0, p0, LX/1vt;->c:I

    .line 2603862
    move v0, v0

    .line 2603863
    return v0
.end method
