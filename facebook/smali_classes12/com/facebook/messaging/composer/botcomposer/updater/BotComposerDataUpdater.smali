.class public final Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/2Oi;

.field public final c:LX/3MV;

.field private final d:LX/3MW;

.field private final e:LX/0tX;

.field public final f:LX/3N0;

.field private final g:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation
.end field

.field public final h:LX/03V;

.field private final i:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Xl;

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2598291
    const-class v0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/2Oi;LX/3MV;LX/3MW;LX/0tX;LX/3N0;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0Xl;)V
    .locals 1
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p8    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2598292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2598293
    iput-object p1, p0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->b:LX/2Oi;

    .line 2598294
    iput-object p2, p0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->c:LX/3MV;

    .line 2598295
    iput-object p3, p0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->d:LX/3MW;

    .line 2598296
    iput-object p4, p0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->e:LX/0tX;

    .line 2598297
    iput-object p5, p0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->f:LX/3N0;

    .line 2598298
    iput-object p6, p0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->g:Ljava/util/concurrent/ExecutorService;

    .line 2598299
    iput-object p7, p0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->h:LX/03V;

    .line 2598300
    iput-object p8, p0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->j:LX/0Xl;

    .line 2598301
    new-instance v0, LX/Ie3;

    invoke-direct {v0, p0}, LX/Ie3;-><init>(Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;)V

    move-object v0, v0

    .line 2598302
    iput-object v0, p0, Lcom/facebook/messaging/composer/botcomposer/updater/BotComposerDataUpdater;->i:LX/0TF;

    .line 2598303
    return-void
.end method
