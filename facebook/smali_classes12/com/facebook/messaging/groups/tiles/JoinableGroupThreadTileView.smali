.class public Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# instance fields
.field private c:LX/8ui;

.field private d:Lcom/facebook/messaging/model/threads/ThreadSummary;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2600055
    invoke-direct {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2600056
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->a(Landroid/util/AttributeSet;II)V

    .line 2600057
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1af;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2600078
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;LX/1af;)V

    .line 2600079
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->a(Landroid/util/AttributeSet;II)V

    .line 2600080
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2600075
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2600076
    invoke-direct {p0, p2, v0, v0}, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->a(Landroid/util/AttributeSet;II)V

    .line 2600077
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2600081
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2600082
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->a(Landroid/util/AttributeSet;II)V

    .line 2600083
    return-void
.end method

.method private a(Landroid/util/AttributeSet;II)V
    .locals 5

    .prologue
    .line 2600064
    invoke-virtual {p0}, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->JoinableGroupsThreadTileView:[I

    invoke-virtual {v0, p1, v1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2600065
    const/16 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b005b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    .line 2600066
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2600067
    new-instance v0, LX/8ui;

    invoke-direct {v0}, LX/8ui;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->c:LX/8ui;

    .line 2600068
    iget-object v0, p0, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->c:LX/8ui;

    invoke-virtual {v0, v1}, LX/8ui;->a(F)V

    .line 2600069
    iget-object v0, p0, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->c:LX/8ui;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, LX/8ui;->b(I)V

    .line 2600070
    iget-object v0, p0, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->c:LX/8ui;

    invoke-virtual {p0}, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v3, LX/0xr;->REGULAR:LX/0xr;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8ui;->a(Landroid/graphics/Typeface;)V

    .line 2600071
    iget-object v0, p0, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->c:LX/8ui;

    const/4 v1, 0x1

    .line 2600072
    iput-boolean v1, v0, LX/8ui;->j:Z

    .line 2600073
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p0, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->c:LX/8ui;

    invoke-virtual {v0, v1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2600074
    return-void
.end method


# virtual methods
.method public setGroupName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2600061
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2600062
    iget-object v0, p0, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->c:LX/8ui;

    invoke-virtual {v0, p1}, LX/8ui;->a(Ljava/lang/String;)Z

    .line 2600063
    return-void
.end method

.method public setPlaceholderColor(I)V
    .locals 1

    .prologue
    .line 2600058
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2600059
    iget-object v0, p0, Lcom/facebook/messaging/groups/tiles/JoinableGroupThreadTileView;->c:LX/8ui;

    invoke-virtual {v0, p1}, LX/8ui;->c(I)V

    .line 2600060
    return-void
.end method
