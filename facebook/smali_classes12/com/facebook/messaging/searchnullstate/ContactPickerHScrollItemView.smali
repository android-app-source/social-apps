.class public Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/3GL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/FJv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2Of;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/widget/tiles/ThreadTileView;

.field private e:Landroid/widget/TextView;

.field public f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2621244
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2621245
    invoke-direct {p0}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->a()V

    .line 2621246
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2621241
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2621242
    invoke-direct {p0}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->a()V

    .line 2621243
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2621238
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2621239
    invoke-direct {p0}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->a()V

    .line 2621240
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2621230
    const-class v0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2621231
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->setOrientation(I)V

    .line 2621232
    const v0, 0x7f030cd9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2621233
    invoke-virtual {p0}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 2621234
    invoke-virtual {p0, v0, v2, v0, v2}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->setPadding(IIII)V

    .line 2621235
    const v0, 0x7f0d1ffc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->d:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2621236
    const v0, 0x7f0d1ffd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->e:Landroid/widget/TextView;

    .line 2621237
    return-void
.end method

.method private static a(Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;LX/3GL;LX/FJv;LX/2Of;)V
    .locals 0

    .prologue
    .line 2621229
    iput-object p1, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->a:LX/3GL;

    iput-object p2, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->b:LX/FJv;

    iput-object p3, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->c:LX/2Of;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;

    invoke-static {v2}, LX/3GL;->b(LX/0QB;)LX/3GL;

    move-result-object v0

    check-cast v0, LX/3GL;

    invoke-static {v2}, LX/FJv;->a(LX/0QB;)LX/FJv;

    move-result-object v1

    check-cast v1, LX/FJv;

    invoke-static {v2}, LX/2Of;->a(LX/0QB;)LX/2Of;

    move-result-object v2

    check-cast v2, LX/2Of;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->a(Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;LX/3GL;LX/FJv;LX/2Of;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/User;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2621225
    iget-object v0, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->b:LX/FJv;

    invoke-virtual {v0, p1}, LX/FJv;->a(Lcom/facebook/user/model/User;)LX/8Vc;

    move-result-object v0

    .line 2621226
    iget-object v1, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->d:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2621227
    iget-object v0, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2621228
    return-void
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 2621224
    iget v0, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->f:I

    return v0
.end method

.method public setPosition(I)V
    .locals 0

    .prologue
    .line 2621222
    iput p1, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->f:I

    .line 2621223
    return-void
.end method

.method public setSingleLine(Z)V
    .locals 2

    .prologue
    .line 2621218
    iget-object v0, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2621219
    iget-object v1, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->e:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLines(I)V

    .line 2621220
    return-void

    .line 2621221
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public setThreadSummary(Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 3

    .prologue
    .line 2621205
    iget-object v0, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->b:LX/FJv;

    invoke-virtual {v0, p1}, LX/FJv;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/8Vc;

    move-result-object v0

    .line 2621206
    iget-object v1, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->d:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2621207
    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2621208
    iget-object v0, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->e:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2621209
    :goto_0
    return-void

    .line 2621210
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->c:LX/2Of;

    invoke-virtual {v0, p1}, LX/2Of;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)Ljava/util/List;

    move-result-object v0

    .line 2621211
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2621212
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080246

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2621213
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->a:LX/3GL;

    invoke-virtual {v2, v0}, LX/3GL;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setUser(Lcom/facebook/user/model/User;)V
    .locals 1

    .prologue
    .line 2621214
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    .line 2621215
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/facebook/messaging/searchnullstate/ContactPickerHScrollItemView;->a(Lcom/facebook/user/model/User;Ljava/lang/String;)V

    .line 2621216
    return-void

    .line 2621217
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
