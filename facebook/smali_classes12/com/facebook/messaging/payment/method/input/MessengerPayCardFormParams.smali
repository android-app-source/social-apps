.class public Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Z

.field public final f:Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2605632
    new-instance v0, LX/IjI;

    invoke-direct {v0}, LX/IjI;-><init>()V

    sput-object v0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/IjJ;)V
    .locals 1

    .prologue
    .line 2605603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605604
    iget-object v0, p1, LX/IjJ;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-object v0, v0

    .line 2605605
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2605606
    iget-boolean v0, p1, LX/IjJ;->g:Z

    move v0, v0

    .line 2605607
    if-eqz v0, :cond_0

    .line 2605608
    iget-object v0, p1, LX/IjJ;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-object v0, v0

    .line 2605609
    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2605610
    iget-object v0, p1, LX/IjJ;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2605611
    iput-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->a:Ljava/lang/String;

    .line 2605612
    iget-object v0, p1, LX/IjJ;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2605613
    iput-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->b:Ljava/lang/String;

    .line 2605614
    iget-object v0, p1, LX/IjJ;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2605615
    iput-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->c:Ljava/lang/String;

    .line 2605616
    iget-object v0, p1, LX/IjJ;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2605617
    iput-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->d:Ljava/lang/String;

    .line 2605618
    iget-boolean v0, p1, LX/IjJ;->e:Z

    move v0, v0

    .line 2605619
    iput-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->e:Z

    .line 2605620
    iget-object v0, p1, LX/IjJ;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-object v0, v0

    .line 2605621
    iput-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    .line 2605622
    iget-boolean v0, p1, LX/IjJ;->g:Z

    move v0, v0

    .line 2605623
    iput-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->g:Z

    .line 2605624
    iget-boolean v0, p1, LX/IjJ;->h:Z

    move v0, v0

    .line 2605625
    iput-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->h:Z

    .line 2605626
    iget-boolean v0, p1, LX/IjJ;->i:Z

    move v0, v0

    .line 2605627
    iput-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->i:Z

    .line 2605628
    iget-boolean v0, p1, LX/IjJ;->j:Z

    move v0, v0

    .line 2605629
    iput-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->j:Z

    .line 2605630
    return-void

    .line 2605631
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2605591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605592
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->a:Ljava/lang/String;

    .line 2605593
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->b:Ljava/lang/String;

    .line 2605594
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->c:Ljava/lang/String;

    .line 2605595
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->d:Ljava/lang/String;

    .line 2605596
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->e:Z

    .line 2605597
    const-class v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    iput-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    .line 2605598
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->g:Z

    .line 2605599
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->h:Z

    .line 2605600
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->i:Z

    .line 2605601
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->j:Z

    .line 2605602
    return-void
.end method

.method public static newBuilder()LX/IjJ;
    .locals 1

    .prologue
    .line 2605590
    new-instance v0, LX/IjJ;

    invoke-direct {v0}, LX/IjJ;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;
    .locals 1

    .prologue
    .line 2605577
    iget-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2605589
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2605578
    iget-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2605579
    iget-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2605580
    iget-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2605581
    iget-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2605582
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2605583
    iget-object v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->f:Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2605584
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2605585
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->h:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2605586
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->i:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2605587
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/method/input/MessengerPayCardFormParams;->j:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2605588
    return-void
.end method
