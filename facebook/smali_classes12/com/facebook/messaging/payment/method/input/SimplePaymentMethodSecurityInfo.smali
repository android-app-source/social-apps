.class public Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;
.super LX/6E7;
.source ""


# instance fields
.field public a:LX/73s;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2605883
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 2605884
    invoke-direct {p0}, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;->a()V

    .line 2605885
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605877
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2605878
    invoke-direct {p0}, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;->a()V

    .line 2605879
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605880
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2605881
    invoke-direct {p0}, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;->a()V

    .line 2605882
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 2605872
    const-class v0, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2605873
    const v0, 0x7f03132f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2605874
    const v0, 0x7f0d2210

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/resources/ui/FbTextView;

    .line 2605875
    iget-object v0, p0, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;->a:LX/73s;

    const v1, 0x7f082c1d

    const-string v2, "[[learn_more_link]]"

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;->getContext()Landroid/content/Context;

    move-result-object v3

    const v5, 0x7f082c1e

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "https://m.facebook.com/help/messenger-app/728431013914433"

    invoke-virtual/range {v0 .. v5}, LX/73s;->a(ILjava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2605876
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;

    invoke-static {v0}, LX/73s;->b(LX/0QB;)LX/73s;

    move-result-object v0

    check-cast v0, LX/73s;

    iput-object v0, p0, Lcom/facebook/messaging/payment/method/input/SimplePaymentMethodSecurityInfo;->a:LX/73s;

    return-void
.end method
