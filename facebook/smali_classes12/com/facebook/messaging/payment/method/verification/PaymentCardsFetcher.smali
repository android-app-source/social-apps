.class public Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field public b:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605969
    iput-object p1, p0, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->a:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2605970
    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2606006
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2606007
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2606008
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/PaymentCard;->v()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2606009
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2606010
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2606011
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;
    .locals 4

    .prologue
    .line 2605995
    const-class v1, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    monitor-enter v1

    .line 2605996
    :try_start_0
    sget-object v0, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2605997
    sput-object v2, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2605998
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2605999
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2606000
    new-instance p0, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    invoke-static {v0}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-direct {p0, v3}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    .line 2606001
    move-object v0, p0

    .line 2606002
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2606003
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2606004
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2606005
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2605989
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2605990
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2605991
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/PaymentCard;->m()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2605992
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2605993
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2605994
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2605983
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2605984
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2605985
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/PaymentCard;->n()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2605986
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2605987
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2605988
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2605971
    iget-object v3, p0, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v3}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2605972
    :goto_0
    new-instance v0, LX/Ijg;

    invoke-direct {v0, p0}, LX/Ijg;-><init>(Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;)V

    .line 2605973
    iget-object v1, p0, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2605974
    :cond_0
    iget-object v3, p0, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->a:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2605975
    iget-object v4, v3, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v4}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2605976
    iget-object v4, v3, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2605977
    :goto_1
    move-object v3, v4

    .line 2605978
    iput-object v3, p0, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2605979
    :cond_1
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2605980
    iget-object v4, v3, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a:LX/0aG;

    const-string v5, "fetch_payment_cards"

    sget-object v7, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, 0x19d9e314

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    .line 2605981
    new-instance v5, LX/J02;

    invoke-direct {v5, v3}, LX/J02;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v6

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2605982
    iget-object v4, v3, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1
.end method
