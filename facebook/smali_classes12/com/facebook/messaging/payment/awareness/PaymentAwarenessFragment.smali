.class public Lcom/facebook/messaging/payment/awareness/PaymentAwarenessFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Iig;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Inj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2605127
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2605128
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605129
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2605130
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessFragment;

    new-instance p1, LX/Iig;

    const/16 v3, 0x2856

    invoke-static {v2, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    const/16 v3, 0x2857

    invoke-static {v2, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {v2}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {p1, v0, v1, v3}, LX/Iig;-><init>(LX/0Ot;LX/0Ot;LX/0Uh;)V

    move-object v2, p1

    check-cast v2, LX/Iig;

    iput-object v2, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessFragment;->a:LX/Iig;

    .line 2605131
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3b5b637a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2605132
    const v1, 0x7f03014a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x651c820e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605133
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2605134
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2605135
    const-string v1, "payment_awareness_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Iiv;

    .line 2605136
    iget-object v1, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessFragment;->a:LX/Iig;

    invoke-virtual {v1, v0}, LX/Iig;->a(LX/Iiv;)LX/Iik;

    move-result-object v1

    .line 2605137
    new-instance v0, LX/Iit;

    invoke-direct {v0, p0}, LX/Iit;-><init>(Lcom/facebook/messaging/payment/awareness/PaymentAwarenessFragment;)V

    invoke-interface {v1, v0}, LX/Iik;->setListener(LX/Iit;)V

    .line 2605138
    const v0, 0x7f0d0553

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2605139
    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2605140
    return-void
.end method
