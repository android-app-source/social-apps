.class public Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2605345
    new-instance v0, LX/Ij5;

    invoke-direct {v0}, LX/Ij5;-><init>()V

    sput-object v0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Ij6;)V
    .locals 1

    .prologue
    .line 2605288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605289
    iget-object v0, p1, LX/Ij6;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->a:Ljava/lang/String;

    .line 2605290
    iget-object v0, p1, LX/Ij6;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->b:Ljava/lang/String;

    .line 2605291
    iget-object v0, p1, LX/Ij6;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->c:Ljava/lang/String;

    .line 2605292
    iget-object v0, p1, LX/Ij6;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->d:Ljava/lang/String;

    .line 2605293
    iget v0, p1, LX/Ij6;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->e:I

    .line 2605294
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2605295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605296
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 2605297
    iput-object v1, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->a:Ljava/lang/String;

    .line 2605298
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 2605299
    iput-object v1, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->b:Ljava/lang/String;

    .line 2605300
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    .line 2605301
    iput-object v1, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->c:Ljava/lang/String;

    .line 2605302
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    .line 2605303
    iput-object v1, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->d:Ljava/lang/String;

    .line 2605304
    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->e:I

    .line 2605305
    return-void

    .line 2605306
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->a:Ljava/lang/String;

    goto :goto_0

    .line 2605307
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->b:Ljava/lang/String;

    goto :goto_1

    .line 2605308
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->c:Ljava/lang/String;

    goto :goto_2

    .line 2605309
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->d:Ljava/lang/String;

    goto :goto_3
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2605310
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2605311
    if-ne p0, p1, :cond_1

    .line 2605312
    :cond_0
    :goto_0
    return v0

    .line 2605313
    :cond_1
    instance-of v2, p1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;

    if-nez v2, :cond_2

    move v0, v1

    .line 2605314
    goto :goto_0

    .line 2605315
    :cond_2
    check-cast p1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;

    .line 2605316
    iget-object v2, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2605317
    goto :goto_0

    .line 2605318
    :cond_3
    iget-object v2, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2605319
    goto :goto_0

    .line 2605320
    :cond_4
    iget-object v2, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->c:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 2605321
    goto :goto_0

    .line 2605322
    :cond_5
    iget-object v2, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2605323
    goto :goto_0

    .line 2605324
    :cond_6
    iget v2, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->e:I

    iget v3, p1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2605325
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2605326
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2605327
    iget-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2605328
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2605329
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2605330
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2605331
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2605332
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2605333
    :goto_2
    iget-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 2605334
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2605335
    :goto_3
    iget v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2605336
    return-void

    .line 2605337
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2605338
    iget-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 2605339
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2605340
    iget-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 2605341
    :cond_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2605342
    iget-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 2605343
    :cond_3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2605344
    iget-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessViewV2Params;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3
.end method
