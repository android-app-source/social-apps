.class public Lcom/facebook/messaging/payment/awareness/PaymentAwarenessLearnMoreFooterView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2605156
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2605157
    invoke-direct {p0}, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessLearnMoreFooterView;->a()V

    .line 2605158
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605153
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2605154
    invoke-direct {p0}, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessLearnMoreFooterView;->a()V

    .line 2605155
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605150
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2605151
    invoke-direct {p0}, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessLearnMoreFooterView;->a()V

    .line 2605152
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2605145
    const-class v0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessLearnMoreFooterView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessLearnMoreFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2605146
    const v0, 0x7f030ef9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2605147
    const v0, 0x7f0d247b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2605148
    new-instance v1, LX/Iiu;

    invoke-direct {v1, p0}, LX/Iiu;-><init>(Lcom/facebook/messaging/payment/awareness/PaymentAwarenessLearnMoreFooterView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2605149
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessLearnMoreFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessLearnMoreFooterView;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessLearnMoreFooterView;->a:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method
