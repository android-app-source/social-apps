.class public Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Landroid/widget/ImageView;

.field private f:LX/InO;

.field public g:LX/InM;

.field private h:I

.field private i:I

.field private j:LX/0Tn;

.field private k:LX/0Tn;

.field private l:F

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2610576
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0e0310

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {p0, v0, v3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610577
    iput v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->h:I

    .line 2610578
    iput v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->i:I

    .line 2610579
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->l:F

    .line 2610580
    invoke-direct {p0, p1, v3}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610581
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2610570
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0e0310

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610571
    iput v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->h:I

    .line 2610572
    iput v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->i:I

    .line 2610573
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->l:F

    .line 2610574
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610575
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2610564
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0e0310

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {p0, v0, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610565
    iput v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->h:I

    .line 2610566
    iput v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->i:I

    .line 2610567
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->l:F

    .line 2610568
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610569
    return-void
.end method

.method private a(LX/InO;)V
    .locals 1

    .prologue
    .line 2610557
    iput-object p1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->f:LX/InO;

    .line 2610558
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->f:LX/InO;

    iget-object v0, v0, LX/InO;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->f:LX/InO;

    iget-object v0, v0, LX/InO;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2610559
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->setVisibility(I)V

    .line 2610560
    :goto_0
    return-void

    .line 2610561
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->h()V

    .line 2610562
    invoke-direct {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->i()V

    .line 2610563
    invoke-direct {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->j()V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2610532
    const-class v0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2610533
    const v0, 0x7f03079b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2610534
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0800

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2610535
    const v0, 0x7f0d144f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2610536
    const v0, 0x7f0d071f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2610537
    const v0, 0x7f0d1455

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2610538
    const v0, 0x7f0d1453

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->e:Landroid/widget/ImageView;

    .line 2610539
    sget-object v0, LX/03r;->MessengerPayNuxBannerView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2610540
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v2, 0x0

    invoke-static {v0, v1, v2}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v2

    .line 2610541
    const/16 v0, 0x2

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 2610542
    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2610543
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0x1

    invoke-static {v3, v1, v4}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v3

    .line 2610544
    new-instance v4, LX/InP;

    invoke-direct {v4}, LX/InP;-><init>()V

    move-object v4, v4

    .line 2610545
    iput-object v2, v4, LX/InP;->b:Ljava/lang/String;

    .line 2610546
    move-object v2, v4

    .line 2610547
    iput-object v3, v2, LX/InP;->c:Ljava/lang/String;

    .line 2610548
    move-object v2, v2

    .line 2610549
    iput-object v0, v2, LX/InP;->a:Landroid/graphics/drawable/Drawable;

    .line 2610550
    move-object v0, v2

    .line 2610551
    new-instance v2, LX/InO;

    invoke-direct {v2, v0}, LX/InO;-><init>(LX/InP;)V

    move-object v0, v2

    .line 2610552
    invoke-direct {p0, v0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a(LX/InO;)V

    .line 2610553
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2610554
    invoke-virtual {p0, v5}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->setWillNotDraw(Z)V

    .line 2610555
    return-void

    .line 2610556
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;F)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2610516
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 2610517
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0800

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2610518
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2610519
    iget v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->l:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    iget v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->l:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    .line 2610520
    :cond_0
    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->l:F

    .line 2610521
    :cond_1
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->l:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    float-to-int v3, p2

    sub-int/2addr v2, v3

    invoke-direct {v1, v2, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 2610522
    new-instance v2, Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->l:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    float-to-int v4, p2

    add-int/2addr v3, v4

    invoke-direct {v2, v3, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 2610523
    new-instance v3, Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->l:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    float-to-int v5, p2

    neg-int v5, v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    .line 2610524
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 2610525
    sget-object v5, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v4, v5}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 2610526
    iget v5, v2, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v4, v5, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2610527
    iget v2, v3, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2610528
    iget v2, v1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {v4, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 2610529
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 2610530
    invoke-virtual {p1, v4, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2610531
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 2610515
    invoke-direct {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2610511
    iget-object v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->k:LX/0Tn;

    if-nez v1, :cond_1

    .line 2610512
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->k:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    iget v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->i:I

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2610513
    iget-object v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->j:LX/0Tn;

    if-nez v1, :cond_1

    .line 2610514
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->j:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v1

    iget v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->h:I

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 2610582
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->setVisibility(I)V

    .line 2610583
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 2610486
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->setVisibility(I)V

    .line 2610487
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2610481
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->f:LX/InO;

    iget-object v0, v0, LX/InO;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 2610482
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2610483
    :goto_0
    return-void

    .line 2610484
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->f:LX/InO;

    iget-object v1, v1, LX/InO;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2610485
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2610476
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->f:LX/InO;

    iget-object v0, v0, LX/InO;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2610477
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2610478
    :goto_0
    return-void

    .line 2610479
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->f:LX/InO;

    iget-object v1, v1, LX/InO;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610480
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->c:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 2610471
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->f:LX/InO;

    iget-object v0, v0, LX/InO;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2610472
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->d:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2610473
    :goto_0
    return-void

    .line 2610474
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->f:LX/InO;

    iget-object v1, v1, LX/InO;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610475
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->d:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2610463
    invoke-direct {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2610464
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->k:LX/0Tn;

    if-eqz v0, :cond_0

    .line 2610465
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->k:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->k:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2610466
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->g()V

    .line 2610467
    :cond_1
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 2610488
    iput p1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->h:I

    .line 2610489
    sget-object v0, LX/Ik4;->a:LX/0Tn;

    invoke-virtual {v0, p2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->j:LX/0Tn;

    .line 2610490
    return-void
.end method

.method public final a(ZF)V
    .locals 0

    .prologue
    .line 2610491
    iput-boolean p1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->m:Z

    .line 2610492
    iput p2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->l:F

    .line 2610493
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->invalidate()V

    .line 2610494
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2610495
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->j:LX/0Tn;

    if-eqz v0, :cond_0

    .line 2610496
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->j:LX/0Tn;

    iget-object v2, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v3, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->j:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2610497
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->f()V

    .line 2610498
    return-void
.end method

.method public final b(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 2610499
    iput p1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->i:I

    .line 2610500
    sget-object v0, LX/Ik4;->a:LX/0Tn;

    invoke-virtual {v0, p2}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->k:LX/0Tn;

    .line 2610501
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2610502
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->m:Z

    if-nez v0, :cond_0

    .line 2610503
    :goto_0
    return-void

    .line 2610504
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1eb6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 2610505
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2610506
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 2610507
    const/4 v2, 0x0

    float-to-int v3, v0

    neg-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->inset(II)V

    .line 2610508
    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 2610509
    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a(Landroid/graphics/Canvas;F)V

    .line 2610510
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public setListener(LX/InM;)V
    .locals 2

    .prologue
    .line 2610468
    iput-object p1, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->g:LX/InM;

    .line 2610469
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->e:Landroid/widget/ImageView;

    new-instance v1, LX/InL;

    invoke-direct {v1, p0}, LX/InL;-><init>(Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610470
    return-void
.end method
