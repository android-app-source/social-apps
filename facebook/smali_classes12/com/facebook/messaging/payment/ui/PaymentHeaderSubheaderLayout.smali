.class public Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;
.super LX/6E7;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/text/BetterTextView;

.field private final b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2610602
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610603
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610600
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610601
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610594
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610595
    const-class v0, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2610596
    const v0, 0x7f030efe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2610597
    const v0, 0x7f0d04f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2610598
    const v0, 0x7f0d2491

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2610599
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    return-void
.end method


# virtual methods
.method public setHeader(I)V
    .locals 1

    .prologue
    .line 2610604
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2610605
    return-void
.end method

.method public setHeader(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2610592
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610593
    return-void
.end method

.method public setHeaderVisibility(I)V
    .locals 1

    .prologue
    .line 2610590
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2610591
    return-void
.end method

.method public setSubheader(I)V
    .locals 1

    .prologue
    .line 2610588
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2610589
    return-void
.end method

.method public setSubheader(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2610586
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610587
    return-void
.end method

.method public setSubheaderVisibility(I)V
    .locals 1

    .prologue
    .line 2610584
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2610585
    return-void
.end method
