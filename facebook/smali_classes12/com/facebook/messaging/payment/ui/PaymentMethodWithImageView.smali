.class public Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2610627
    const-class v0, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2610624
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2610625
    invoke-direct {p0}, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->a()V

    .line 2610626
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610621
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610622
    invoke-direct {p0}, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->a()V

    .line 2610623
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610618
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610619
    invoke-direct {p0}, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->a()V

    .line 2610620
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2610612
    const v0, 0x7f030f01

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2610613
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->setOrientation(I)V

    .line 2610614
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->setGravity(I)V

    .line 2610615
    const v0, 0x7f0d2496

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2610616
    const v0, 0x7f0d0340

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2610617
    return-void
.end method


# virtual methods
.method public setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2610610
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2610611
    return-void
.end method

.method public setImageUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2610608
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2610609
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2610606
    iget-object v0, p0, Lcom/facebook/messaging/payment/ui/PaymentMethodWithImageView;->b:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610607
    return-void
.end method
