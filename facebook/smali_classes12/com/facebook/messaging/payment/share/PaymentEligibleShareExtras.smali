.class public Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/facebook/payments/p2p/model/Amount;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final g:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2608754
    new-instance v0, LX/Im2;

    invoke-direct {v0}, LX/Im2;-><init>()V

    sput-object v0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2608755
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2608756
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->a:I

    .line 2608757
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->b:Ljava/lang/String;

    .line 2608758
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->c:Ljava/lang/String;

    .line 2608759
    const-class v0, Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/Amount;

    iput-object v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->d:Lcom/facebook/payments/p2p/model/Amount;

    .line 2608760
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->e:Ljava/lang/String;

    .line 2608761
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->f:I

    .line 2608762
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->g:J

    .line 2608763
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2608764
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2608765
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "maxRecipients"

    iget v2, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->a:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "shareCamption"

    iget-object v2, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "qpEntryPoint"

    iget-object v2, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "amount"

    iget-object v2, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->d:Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "campaignName"

    iget-object v2, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "addCardFlowType"

    iget v2, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->f:I

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;I)LX/237;

    move-result-object v0

    const-string v1, "incentivesTransferId"

    iget-wide v2, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->g:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2608766
    iget v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2608767
    iget-object v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2608768
    iget-object v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2608769
    iget-object v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->d:Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2608770
    iget-object v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2608771
    iget v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2608772
    iget-wide v0, p0, Lcom/facebook/messaging/payment/share/PaymentEligibleShareExtras;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2608773
    return-void
.end method
