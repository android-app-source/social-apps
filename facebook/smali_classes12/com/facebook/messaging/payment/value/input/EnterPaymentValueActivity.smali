.class public Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/67U;
.implements LX/0f2;


# static fields
.field public static final p:Lcom/facebook/payments/currency/CurrencyAmount;


# instance fields
.field public A:Landroid/widget/ProgressBar;

.field public B:Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;

.field private C:Landroid/widget/LinearLayout;

.field private D:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public E:Lcom/facebook/messaging/payment/value/input/ui/OrionMessengerPayViewPager;

.field private F:LX/63L;

.field private G:Z

.field public H:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
            ">;"
        }
    .end annotation
.end field

.field public I:Z

.field public J:LX/5g0;

.field public final K:LX/Ino;

.field private final L:LX/Ini;

.field private final M:LX/Inj;

.field public q:LX/67X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/73q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Or;
    .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pPaymentsRequestEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/Iii;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2611081
    new-instance v0, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v1, "USD"

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    sput-object v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->p:Lcom/facebook/payments/currency/CurrencyAmount;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2611076
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2611077
    new-instance v0, LX/Ino;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/Ino;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->K:LX/Ino;

    .line 2611078
    new-instance v0, LX/Ini;

    invoke-direct {v0, p0}, LX/Ini;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->L:LX/Ini;

    .line 2611079
    new-instance v0, LX/Inj;

    invoke-direct {v0, p0}, LX/Inj;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->M:LX/Inj;

    .line 2611080
    return-void
.end method

.method private A()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2611072
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->C:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2611073
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->D:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 2611074
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->E:Lcom/facebook/messaging/payment/value/input/ui/OrionMessengerPayViewPager;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/payment/value/input/ui/OrionMessengerPayViewPager;->setVisibility(I)V

    .line 2611075
    return-void
.end method

.method public static C(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2611068
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->C:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2611069
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->D:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 2611070
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->E:Lcom/facebook/messaging/payment/value/input/ui/OrionMessengerPayViewPager;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/payment/value/input/ui/OrionMessengerPayViewPager;->setVisibility(I)V

    .line 2611071
    return-void
.end method

.method public static a(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;LX/Iiv;)Z
    .locals 5

    .prologue
    .line 2611062
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    sget-object v1, LX/5g0;->SEND:LX/5g0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    sget-object v1, LX/5g0;->THREAD_DETAILS_SEND_FLOW:LX/5g0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    sget-object v1, LX/5g0;->TRIGGER_SEND_FLOW:LX/5g0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    sget-object v1, LX/5g0;->META_RANGE_SEND_FLOW:LX/5g0;

    if-ne v0, v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2611063
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->x:LX/Iii;

    const/4 v1, 0x0

    .line 2611064
    invoke-static {v0, p1}, LX/Iii;->c(LX/Iii;LX/Iiv;)LX/Iio;

    move-result-object v2

    .line 2611065
    iget-object v3, v0, LX/Iii;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, LX/Iio;->a()LX/0Tn;

    move-result-object v4

    invoke-interface {v3, v4, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v3

    .line 2611066
    invoke-interface {v2}, LX/Iio;->c()I

    move-result v4

    if-ge v3, v4, :cond_1

    invoke-interface {v2}, LX/Iio;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    move v0, v1

    .line 2611067
    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->I:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 3

    .prologue
    .line 2611058
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2611059
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0d86

    const-string v2, "enter_payment_value_fragment"

    invoke-virtual {v0, v1, p1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2611060
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->A()V

    .line 2611061
    return-void
.end method

.method public static b(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;LX/Iiv;)V
    .locals 4

    .prologue
    .line 2611043
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->A()V

    .line 2611044
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 2611045
    const-string v1, "payment_awareness_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2611046
    :goto_0
    return-void

    .line 2611047
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2611048
    const-string v2, "payment_awareness_mode"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2611049
    new-instance v2, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessFragment;-><init>()V

    .line 2611050
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2611051
    move-object v1, v2

    .line 2611052
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d0d86

    const-string v3, "payment_awareness_fragment"

    invoke-virtual {v0, v2, v1, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2611053
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->x:LX/Iii;

    .line 2611054
    invoke-static {v0, p1}, LX/Iii;->c(LX/Iii;LX/Iiv;)LX/Iio;

    move-result-object v1

    .line 2611055
    iget-object v2, v0, LX/Iii;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, LX/Iio;->a()LX/0Tn;

    move-result-object v3

    const/4 p0, 0x0

    invoke-interface {v2, v3, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    .line 2611056
    iget-object v3, v0, LX/Iii;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    invoke-interface {v1}, LX/Iio;->a()LX/0Tn;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v3, v1, v2}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2611057
    goto :goto_0
.end method

.method public static n(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)Z
    .locals 2

    .prologue
    .line 2610872
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->p(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    move-result-object v1

    .line 2610873
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2610874
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2610875
    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)Z
    .locals 2

    .prologue
    .line 2611039
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->p(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    move-result-object v1

    .line 2611040
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2611041
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2611042
    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->j()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2611037
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "orion_messenger_pay_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;

    .line 2611038
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    goto :goto_0
.end method

.method public static r(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V
    .locals 5

    .prologue
    .line 2610997
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 2610998
    const-string v1, "enter_payment_value_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2610999
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->A()V

    .line 2611000
    :goto_0
    return-void

    .line 2611001
    :cond_0
    sget-object v0, LX/Inn;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    invoke-virtual {v1}, LX/5g0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2611002
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported paymentFlowType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2611003
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "orion_messenger_pay_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;

    .line 2611004
    new-instance v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;-><init>()V

    .line 2611005
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2611006
    const-string v3, "payment_flow_type"

    sget-object v4, LX/5g0;->GROUP_COMMERCE_SEND:LX/5g0;

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2611007
    const-string v3, "orion_messenger_pay_params"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2611008
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2611009
    move-object v0, v1

    .line 2611010
    invoke-static {p0, v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->a$redex0(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    goto :goto_0

    .line 2611011
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->H:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2611012
    :goto_1
    goto :goto_0

    .line 2611013
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->y:LX/0Uh;

    const/16 v1, 0x22b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2611014
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "orion_messenger_pay_params"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;

    invoke-static {v1, v0}, LX/Io5;->a(LX/5g0;Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;)Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    move-result-object v0

    .line 2611015
    invoke-static {p0, v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->a$redex0(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611016
    :goto_2
    goto/16 :goto_0

    .line 2611017
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->y:LX/0Uh;

    const/16 v1, 0x22b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2611018
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "orion_messenger_pay_params"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;

    invoke-static {v1, v0}, LX/Io5;->a(LX/5g0;Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;)Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    move-result-object v0

    .line 2611019
    invoke-static {p0, v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->a$redex0(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611020
    :goto_3
    goto/16 :goto_0

    .line 2611021
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->s:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "request_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2611022
    new-instance v2, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;

    invoke-direct {v2, v1}, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;-><init>(Ljava/lang/String;)V

    .line 2611023
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2611024
    sget-object v4, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2611025
    const-string v2, "fetch_payment_request"

    invoke-static {v0, v3, v2}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2611026
    new-instance v3, LX/J03;

    invoke-direct {v3, v0}, LX/J03;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2611027
    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->H:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2611028
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->A:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_4

    .line 2611029
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->A:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2611030
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->H:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Inm;

    invoke-direct {v1, p0}, LX/Inm;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->u:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_1

    .line 2611031
    :cond_5
    sget-object v0, LX/Inp;->TAB_ORION_REQUEST:LX/Inp;

    invoke-virtual {v0}, LX/Inp;->ordinal()I

    move-result v0

    .line 2611032
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->E:Lcom/facebook/messaging/payment/value/input/ui/OrionMessengerPayViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2611033
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->C(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V

    goto/16 :goto_2

    .line 2611034
    :cond_6
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->y:LX/0Uh;

    const/16 v1, 0x55d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2611035
    :goto_4
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->C(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V

    goto :goto_3

    .line 2611036
    :cond_7
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->B:Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;

    invoke-virtual {v0}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a()V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private u()Z
    .locals 4

    .prologue
    .line 2610977
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "enter_payment_value_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2610978
    if-eqz v0, :cond_1

    .line 2610979
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->A:LX/IoU;

    instance-of v1, v1, LX/Ip7;

    if-eqz v1, :cond_3

    .line 2610980
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->A:LX/IoU;

    check-cast v1, LX/Ip7;

    .line 2610981
    iget-object v2, v1, LX/Ip7;->o:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    .line 2610982
    iget-object v3, v2, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->f:LX/Iph;

    move-object v2, v3

    .line 2610983
    sget-object v3, LX/Iph;->BUTTON_LEFT_SELECTED:LX/Iph;

    if-eq v2, v3, :cond_0

    iget-object v2, v1, LX/Ip7;->o:Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    .line 2610984
    iget-object v3, v2, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->f:LX/Iph;

    move-object v2, v3

    .line 2610985
    sget-object v3, LX/Iph;->BUTTON_RIGHT_SELECTED:LX/Iph;

    if-ne v2, v3, :cond_4

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2610986
    if-eqz v2, :cond_3

    .line 2610987
    invoke-virtual {v1}, LX/Ip7;->b()V

    .line 2610988
    const/4 v1, 0x0

    .line 2610989
    :goto_1
    move v0, v1

    .line 2610990
    if-eqz v0, :cond_2

    .line 2610991
    :cond_1
    const/4 v0, 0x1

    .line 2610992
    :goto_2
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 2610993
    :cond_3
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->o:LX/Ine;

    const-string v2, "p2p_send_money_cancelled"

    iget-object v3, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2610994
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p0, p0

    .line 2610995
    invoke-virtual {v1, v2, v3, p0}, LX/Ine;->a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;Landroid/os/Bundle;)V

    .line 2610996
    const/4 v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static v(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V
    .locals 4

    .prologue
    .line 2610970
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->G:Z

    if-nez v0, :cond_0

    .line 2610971
    :goto_0
    return-void

    .line 2610972
    :cond_0
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 2610973
    const v1, 0x7f0d0d86

    .line 2610974
    new-instance v2, Lcom/facebook/payments/connectivity/PaymentNoInternetFragment;

    invoke-direct {v2}, Lcom/facebook/payments/connectivity/PaymentNoInternetFragment;-><init>()V

    move-object v2, v2

    .line 2610975
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v3

    invoke-virtual {v3}, LX/0hH;->b()I

    .line 2610976
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->A()V

    goto :goto_0
.end method

.method private w()V
    .locals 4

    .prologue
    .line 2610954
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->q:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v1

    .line 2610955
    const/16 v0, 0x12

    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/3u1;->a(II)V

    .line 2610956
    const v0, 0x7f030b04

    invoke-virtual {v1, v0}, LX/3u1;->a(I)V

    .line 2610957
    invoke-virtual {v1}, LX/3u1;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;

    .line 2610958
    new-instance v2, LX/63L;

    invoke-direct {v2, p0, v1}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    iput-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->F:LX/63L;

    .line 2610959
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "orion_messenger_pay_params"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;

    .line 2610960
    sget-object v2, LX/Inn;->a:[I

    iget-object v3, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    invoke-virtual {v3}, LX/5g0;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2610961
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported paymentFlowType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2610962
    :pswitch_0
    const v2, 0x7f082d1e

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->setTitle(I)V

    .line 2610963
    iget-object v1, v1, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->setUserName(Ljava/lang/String;)V

    .line 2610964
    :cond_0
    :goto_0
    return-void

    .line 2610965
    :pswitch_1
    const v1, 0x7f082d1f

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->setTitle(I)V

    goto :goto_0

    .line 2610966
    :pswitch_2
    const v1, 0x7f082c81

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->setTitle(I)V

    goto :goto_0

    .line 2610967
    :pswitch_3
    const v2, 0x7f082c81

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->setTitle(I)V

    .line 2610968
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->v:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2610969
    iget-object v1, v1, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->setUserName(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public static z(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V
    .locals 2

    .prologue
    .line 2610923
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->A:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 2610924
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->A:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2610925
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2610876
    const-string v0, "payment_tray_popup"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 2610877
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v1, p0

    check-cast v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    invoke-static {p1}, LX/67X;->b(LX/0QB;)LX/67X;

    move-result-object v2

    check-cast v2, LX/67X;

    invoke-static {p1}, LX/73q;->b(LX/0QB;)LX/73q;

    move-result-object v3

    check-cast v3, LX/73q;

    invoke-static {p1}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    const/16 v7, 0x1546

    invoke-static {p1, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v8

    check-cast v8, LX/0Zb;

    invoke-static {p1}, LX/Iii;->b(LX/0QB;)LX/Iii;

    move-result-object v9

    check-cast v9, LX/Iii;

    invoke-static {p1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    const/16 v0, 0x12cc

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object v2, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->q:LX/67X;

    iput-object v3, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->r:LX/73q;

    iput-object v4, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->s:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    iput-object v5, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->t:LX/03V;

    iput-object v6, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->u:Ljava/util/concurrent/Executor;

    iput-object v7, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->v:LX/0Or;

    iput-object v8, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->w:LX/0Zb;

    iput-object v9, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->x:LX/Iii;

    iput-object v10, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->y:LX/0Uh;

    iput-object p1, v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->z:LX/0Or;

    .line 2610878
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->q:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2610879
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2610880
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 2610881
    instance-of v0, p1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    if-eqz v0, :cond_1

    .line 2610882
    check-cast p1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->L:LX/Ini;

    .line 2610883
    iput-object v0, p1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->F:LX/Ini;

    .line 2610884
    :cond_0
    :goto_0
    return-void

    .line 2610885
    :cond_1
    instance-of v0, p1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessFragment;

    if-eqz v0, :cond_0

    .line 2610886
    check-cast p1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessFragment;

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->M:LX/Inj;

    .line 2610887
    iput-object v0, p1, Lcom/facebook/messaging/payment/awareness/PaymentAwarenessFragment;->b:LX/Inj;

    .line 2610888
    goto :goto_0
.end method

.method public final b()LX/3u1;
    .locals 1

    .prologue
    .line 2610889
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->q:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2610890
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2610891
    const v0, 0x7f03048a

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->setContentView(I)V

    .line 2610892
    const v0, 0x7f0e0a9f

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->setTheme(I)V

    .line 2610893
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "payment_flow_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5g0;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    .line 2610894
    const v0, 0x7f0d0d88

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->A:Landroid/widget/ProgressBar;

    .line 2610895
    const v0, 0x7f0d0d87

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->B:Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;

    .line 2610896
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->B:Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;

    const-string v1, "request_nux_banner_dismiss_count"

    invoke-virtual {v0, v3, v1}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a(ILjava/lang/String;)V

    .line 2610897
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->B:Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;

    const/4 v1, 0x2

    const-string v2, "request_nux_banner_impression_count"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->b(ILjava/lang/String;)V

    .line 2610898
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->B:Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-virtual {v0, v3, v1}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->a(ZF)V

    .line 2610899
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->B:Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;

    new-instance v1, LX/Ink;

    invoke-direct {v1, p0}, LX/Ink;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/ui/MessengerPayNuxBannerView;->setListener(LX/InM;)V

    .line 2610900
    const v0, 0x7f0d0d86

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->C:Landroid/widget/LinearLayout;

    .line 2610901
    const v0, 0x7f0d0d84

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->D:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2610902
    const v0, 0x7f0d0d85

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/ui/OrionMessengerPayViewPager;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->E:Lcom/facebook/messaging/payment/value/input/ui/OrionMessengerPayViewPager;

    .line 2610903
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->E:Lcom/facebook/messaging/payment/value/input/ui/OrionMessengerPayViewPager;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->K:LX/Ino;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2610904
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->D:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->E:Lcom/facebook/messaging/payment/value/input/ui/OrionMessengerPayViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2610905
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->D:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v1, LX/Inl;

    invoke-direct {v1, p0}, LX/Inl;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V

    .line 2610906
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2610907
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->w()V

    .line 2610908
    if-eqz p1, :cond_0

    .line 2610909
    const-string v0, "is_awareness_screen_next_clicked"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->I:Z

    .line 2610910
    :cond_0
    const/4 v0, 0x1

    .line 2610911
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->n(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/Iiv;->ORION_C2C_THREAD_BUYER_SEND:LX/Iiv;

    invoke-static {p0, v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->a(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;LX/Iiv;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2610912
    sget-object v1, LX/Iiv;->ORION_C2C_THREAD_BUYER_SEND:LX/Iiv;

    invoke-static {p0, v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->b(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;LX/Iiv;)V

    .line 2610913
    :goto_0
    move v0, v0

    .line 2610914
    if-eqz v0, :cond_1

    .line 2610915
    :goto_1
    return-void

    .line 2610916
    :cond_1
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->r(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)V

    goto :goto_1

    .line 2610917
    :cond_2
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->o(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, LX/Iiv;->ORION_C2C_THREAD_SELLER_SEND:LX/Iiv;

    invoke-static {p0, v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->a(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;LX/Iiv;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2610918
    sget-object v1, LX/Iiv;->ORION_C2C_THREAD_SELLER_SEND:LX/Iiv;

    invoke-static {p0, v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->b(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;LX/Iiv;)V

    goto :goto_0

    .line 2610919
    :cond_3
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->p(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    move-result-object v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2610920
    if-nez v1, :cond_4

    sget-object v1, LX/Iiv;->ORION_SEND:LX/Iiv;

    invoke-static {p0, v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->a(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;LX/Iiv;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2610921
    sget-object v1, LX/Iiv;->ORION_SEND:LX/Iiv;

    invoke-static {p0, v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->b(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;LX/Iiv;)V

    goto :goto_0

    .line 2610922
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2610926
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2610927
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2610928
    :cond_0
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 2610929
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 2610930
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 2610931
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->getMenuInflater()Landroid/view/MenuInflater;

    .line 2610932
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->F:LX/63L;

    invoke-virtual {v1, p1}, LX/63L;->a(Landroid/view/Menu;)V

    .line 2610933
    return v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x39c488e2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2610934
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2610935
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->H:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 2610936
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->H:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2610937
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->H:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2610938
    :cond_0
    const/16 v1, 0x23

    const v2, -0x4ff487df

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2610939
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 2610940
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->u()Z

    .line 2610941
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->r:LX/73q;

    invoke-virtual {v0, p0}, LX/73q;->a(Landroid/app/Activity;)V

    .line 2610942
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->finish()V

    .line 2610943
    const/4 v0, 0x1

    .line 2610944
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x1044d91

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2610945
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->G:Z

    .line 2610946
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2610947
    const/16 v1, 0x23

    const v2, -0x5be96af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPostResume()V
    .locals 1

    .prologue
    .line 2610948
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPostResume()V

    .line 2610949
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->G:Z

    .line 2610950
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2610951
    const-string v0, "is_awareness_screen_next_clicked"

    iget-boolean v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->I:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2610952
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2610953
    return-void
.end method
