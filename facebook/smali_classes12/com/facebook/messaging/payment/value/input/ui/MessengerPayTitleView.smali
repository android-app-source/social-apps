.class public Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2613247
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2613248
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->a()V

    .line 2613249
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2613250
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2613251
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->a()V

    .line 2613252
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2613244
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2613245
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->a()V

    .line 2613246
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2613239
    const-class v0, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2613240
    const v0, 0x7f030b03

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2613241
    const v0, 0x7f0d1bd3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->a:Landroid/widget/TextView;

    .line 2613242
    const v0, 0x7f0d1bd4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->b:Landroid/widget/TextView;

    .line 2613243
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    return-void
.end method


# virtual methods
.method public setTitle(I)V
    .locals 1

    .prologue
    .line 2613237
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2613238
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2613235
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2613236
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2613230
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2613231
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2613232
    :goto_0
    return-void

    .line 2613233
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->b:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2613234
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ui/MessengerPayTitleView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
