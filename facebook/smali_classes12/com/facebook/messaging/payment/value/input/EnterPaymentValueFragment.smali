.class public Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final t:Lcom/facebook/payments/currency/CurrencyAmount;

.field private static final u:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public A:LX/IoU;

.field public B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

.field public C:LX/5g0;

.field public D:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public E:LX/0Yb;

.field public F:LX/Ini;

.field public G:LX/6nr;

.field public final H:LX/6wv;

.field public final I:LX/6wv;

.field public final J:LX/6wv;

.field public final K:LX/Inz;

.field private final L:LX/6nc;

.field public a:LX/5fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/73q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pPaymentsSyncProtocolEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Xj;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ijy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/2MA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Duk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/InW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/Inh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/InZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/Ine;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/Inc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/IoW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/IpF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/6ns;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final v:Ljava/lang/String;

.field public final w:Ljava/lang/String;

.field public final x:Ljava/lang/String;

.field public final y:Ljava/lang/String;

.field private z:Landroid/view/MenuItem;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2611407
    new-instance v0, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v1, "USD"

    new-instance v2, Ljava/math/BigDecimal;

    const-string v3, "100"

    invoke-direct {v2, v3}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    sput-object v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->t:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2611408
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->u:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2611253
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2611254
    const-string v0, "send_confirm_dialog"

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->v:Ljava/lang/String;

    .line 2611255
    const-string v0, "decline_request_dialog"

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->w:Ljava/lang/String;

    .line 2611256
    const-string v0, "ineligible_recipient_dialog"

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->x:Ljava/lang/String;

    .line 2611257
    const-string v0, "select_card_dialog"

    invoke-static {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->y:Ljava/lang/String;

    .line 2611258
    new-instance v0, LX/Inx;

    invoke-direct {v0, p0}, LX/Inx;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->H:LX/6wv;

    .line 2611259
    new-instance v0, LX/Iny;

    invoke-direct {v0, p0}, LX/Iny;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->I:LX/6wv;

    .line 2611260
    new-instance v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment$3;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment$3;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->J:LX/6wv;

    .line 2611261
    new-instance v0, LX/Inz;

    invoke-direct {v0, p0}, LX/Inz;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->K:LX/Inz;

    .line 2611262
    new-instance v0, LX/Io0;

    invoke-direct {v0, p0}, LX/Io0;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->L:LX/6nc;

    return-void
.end method

.method public static C(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 5

    .prologue
    .line 2611409
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611410
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->e:LX/0Px;

    move-object v0, v1

    .line 2611411
    const/4 v1, 0x0

    move v1, v1

    .line 2611412
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2611413
    const-string v3, "payment_cards"

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2611414
    const-string v3, "credit_card_enabled"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2611415
    new-instance v3, Lcom/facebook/messaging/payment/value/input/PaymentSelectCardDialogFragment;

    invoke-direct {v3}, Lcom/facebook/messaging/payment/value/input/PaymentSelectCardDialogFragment;-><init>()V

    .line 2611416
    invoke-virtual {v3, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2611417
    move-object v0, v3

    .line 2611418
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->K:LX/Inz;

    .line 2611419
    iput-object v1, v0, Lcom/facebook/messaging/payment/value/input/PaymentSelectCardDialogFragment;->n:LX/Inz;

    .line 2611420
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2611421
    return-void
.end method

.method public static F(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 2

    .prologue
    .line 2611422
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->A:LX/IoU;

    invoke-interface {v0}, LX/IoU;->b()V

    .line 2611423
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b(Z)V

    .line 2611424
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    sget-object v1, LX/IoT;->PREPARE_PAYMENT:LX/IoT;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(LX/IoT;)V

    .line 2611425
    return-void
.end method

.method public static G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 3

    .prologue
    .line 2611426
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611427
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    move-object v0, v1

    .line 2611428
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->C:LX/5g0;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, v1, v2}, LX/IoT;->next(LX/5g0;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)LX/IoT;

    move-result-object v0

    .line 2611429
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(LX/IoT;)V

    .line 2611430
    return-void
.end method

.method private static H(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 3

    .prologue
    .line 2611377
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611378
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    move-object v0, v1

    .line 2611379
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->C:LX/5g0;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, v1, v2}, LX/IoT;->next(LX/5g0;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)LX/IoT;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->C:LX/5g0;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, v1, v2}, LX/IoT;->next(LX/5g0;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)LX/IoT;

    move-result-object v0

    .line 2611380
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(LX/IoT;)V

    .line 2611381
    return-void
.end method

.method private static a(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;LX/5fv;LX/03V;LX/73q;LX/0Or;LX/0Xj;LX/Ijy;Lcom/facebook/content/SecureContextHelper;LX/2MA;LX/Duk;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Ljava/util/concurrent/Executor;LX/InW;LX/Inh;LX/InZ;LX/Ine;LX/Inc;LX/IoW;LX/IpF;LX/6ns;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;",
            "LX/5fv;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/73q;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Xj;",
            "LX/Ijy;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/2MA;",
            "LX/Duk;",
            "Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;",
            "Ljava/util/concurrent/Executor;",
            "LX/InW;",
            "LX/Inh;",
            "LX/InZ;",
            "LX/Ine;",
            "LX/Inc;",
            "LX/IoW;",
            "LX/IpF;",
            "LX/6ns;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2611431
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->a:LX/5fv;

    iput-object p2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->b:LX/03V;

    iput-object p3, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->c:LX/73q;

    iput-object p4, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->d:LX/0Or;

    iput-object p5, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->e:LX/0Xj;

    iput-object p6, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->f:LX/Ijy;

    iput-object p7, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->g:Lcom/facebook/content/SecureContextHelper;

    iput-object p8, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->h:LX/2MA;

    iput-object p9, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->i:LX/Duk;

    iput-object p10, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->j:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    iput-object p11, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->k:Ljava/util/concurrent/Executor;

    iput-object p12, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->l:LX/InW;

    iput-object p13, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->m:LX/Inh;

    iput-object p14, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->n:LX/InZ;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->o:LX/Ine;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->p:LX/Inc;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->q:LX/IoW;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->r:LX/IpF;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->s:LX/6ns;

    return-void
.end method

.method private a(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V
    .locals 2

    .prologue
    .line 2611432
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611433
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    new-instance v1, LX/Io4;

    invoke-direct {v1, p0}, LX/Io4;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611434
    iput-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a:LX/Io4;

    .line 2611435
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 22

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v20

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-static/range {v20 .. v20}, LX/5fv;->a(LX/0QB;)LX/5fv;

    move-result-object v2

    check-cast v2, LX/5fv;

    invoke-static/range {v20 .. v20}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static/range {v20 .. v20}, LX/73q;->a(LX/0QB;)LX/73q;

    move-result-object v4

    check-cast v4, LX/73q;

    const/16 v5, 0x1547

    move-object/from16 v0, v20

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static/range {v20 .. v20}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v6

    check-cast v6, LX/0Xj;

    invoke-static/range {v20 .. v20}, LX/Ijy;->a(LX/0QB;)LX/Ijy;

    move-result-object v7

    check-cast v7, LX/Ijy;

    invoke-static/range {v20 .. v20}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v20 .. v20}, LX/2M4;->a(LX/0QB;)LX/2MA;

    move-result-object v9

    check-cast v9, LX/2MA;

    invoke-static/range {v20 .. v20}, LX/Duk;->a(LX/0QB;)LX/Duk;

    move-result-object v10

    check-cast v10, LX/Duk;

    invoke-static/range {v20 .. v20}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    move-result-object v11

    check-cast v11, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-static/range {v20 .. v20}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/Executor;

    invoke-static/range {v20 .. v20}, LX/InW;->a(LX/0QB;)LX/InW;

    move-result-object v13

    check-cast v13, LX/InW;

    invoke-static/range {v20 .. v20}, LX/Inh;->a(LX/0QB;)LX/Inh;

    move-result-object v14

    check-cast v14, LX/Inh;

    invoke-static/range {v20 .. v20}, LX/InZ;->a(LX/0QB;)LX/InZ;

    move-result-object v15

    check-cast v15, LX/InZ;

    invoke-static/range {v20 .. v20}, LX/Ine;->a(LX/0QB;)LX/Ine;

    move-result-object v16

    check-cast v16, LX/Ine;

    invoke-static/range {v20 .. v20}, LX/Inc;->a(LX/0QB;)LX/Inc;

    move-result-object v17

    check-cast v17, LX/Inc;

    invoke-static/range {v20 .. v20}, LX/IoW;->a(LX/0QB;)LX/IoW;

    move-result-object v18

    check-cast v18, LX/IoW;

    invoke-static/range {v20 .. v20}, LX/IpF;->a(LX/0QB;)LX/IpF;

    move-result-object v19

    check-cast v19, LX/IpF;

    const-class v21, LX/6ns;

    invoke-interface/range {v20 .. v21}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/6ns;

    invoke-static/range {v1 .. v20}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->a(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;LX/5fv;LX/03V;LX/73q;LX/0Or;LX/0Xj;LX/Ijy;Lcom/facebook/content/SecureContextHelper;LX/2MA;LX/Duk;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Ljava/util/concurrent/Executor;LX/InW;LX/Inh;LX/InZ;LX/Ine;LX/Inc;LX/IoW;LX/IpF;LX/6ns;)V

    return-void
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2611436
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->u:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2611437
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2611438
    const-string v1, "payment_request"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2611439
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->r:LX/IpF;

    invoke-virtual {v1, p1, v0}, LX/IpF;->a(Ljava/lang/String;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    .line 2611440
    return-void
.end method

.method public static m$redex0(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 3

    .prologue
    .line 2611441
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->A:LX/IoU;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->C:LX/5g0;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-static {v1, v2}, LX/IoW;->a(LX/5g0;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)LX/Ios;

    move-result-object v1

    invoke-interface {v0, v1}, LX/IoU;->setMessengerPayViewParams(LX/Ios;)V

    .line 2611442
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->A:LX/IoU;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->z:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, LX/IoU;->a(Landroid/view/MenuItem;)V

    .line 2611443
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->n()V

    .line 2611444
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    .line 2611445
    if-nez v0, :cond_1

    .line 2611446
    :cond_0
    :goto_0
    return-void

    .line 2611447
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/PaymentSelectCardDialogFragment;

    .line 2611448
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2611449
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2611450
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->C(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    goto :goto_0
.end method

.method private n()V
    .locals 5

    .prologue
    .line 2611451
    sget-object v0, LX/Inw;->b:[I

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611452
    iget-object v2, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    move-object v1, v2

    .line 2611453
    invoke-virtual {v1}, LX/IoT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2611454
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid state found + "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611455
    iget-object p0, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    move-object v2, p0

    .line 2611456
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2611457
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611458
    iget-boolean v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->c:Z

    move v0, v1

    .line 2611459
    if-eqz v0, :cond_0

    .line 2611460
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611461
    :cond_0
    :goto_0
    :pswitch_1
    return-void

    .line 2611462
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611463
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q:LX/03R;

    move-object v0, v1

    .line 2611464
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2611465
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611466
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q:LX/03R;

    move-object v0, v1

    .line 2611467
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2611468
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->H(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    goto :goto_0

    .line 2611469
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f082cd9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->l:LX/InW;

    .line 2611470
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2611471
    iget-object v3, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v1, v2, v3}, LX/InW;->a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080016

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08002c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v0

    .line 2611472
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->I:LX/6wv;

    .line 2611473
    iput-object v1, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2611474
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2611475
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611476
    goto :goto_0

    .line 2611477
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611478
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    move-object v0, v1

    .line 2611479
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611480
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-object v0, v1

    .line 2611481
    if-nez v0, :cond_3

    .line 2611482
    :cond_2
    :goto_1
    goto :goto_0

    .line 2611483
    :pswitch_4
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->t()V

    goto :goto_0

    .line 2611484
    :pswitch_5
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611485
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G:LX/6nr;

    invoke-virtual {v0}, LX/6nr;->a()V

    .line 2611486
    goto :goto_0

    .line 2611487
    :pswitch_6
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611488
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->m:LX/Inh;

    .line 2611489
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2611490
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, v1, v2}, LX/Inh;->a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2611491
    goto/16 :goto_0

    .line 2611492
    :cond_3
    new-instance v1, LX/Inr;

    invoke-direct {v1, p0}, LX/Inr;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611493
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611494
    invoke-static {}, LX/Ik2;->newBuilder()LX/Ik0;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611495
    iget-object v3, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    move-object v0, v3

    .line 2611496
    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2611497
    iput-object v0, v2, LX/Ik0;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2611498
    move-object v0, v2

    .line 2611499
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611500
    iget-object v3, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->e:LX/0Px;

    move-object v2, v3

    .line 2611501
    iput-object v2, v0, LX/Ik0;->b:LX/0Px;

    .line 2611502
    move-object v0, v0

    .line 2611503
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->C:LX/5g0;

    .line 2611504
    iput-object v2, v0, LX/Ik0;->e:LX/5g0;

    .line 2611505
    move-object v0, v0

    .line 2611506
    const/4 v2, 0x0

    move v2, v2

    .line 2611507
    iput-boolean v2, v0, LX/Ik0;->k:Z

    .line 2611508
    move-object v0, v0

    .line 2611509
    iput-object p0, v0, LX/Ik0;->c:Landroid/support/v4/app/Fragment;

    .line 2611510
    move-object v0, v0

    .line 2611511
    const/4 v2, 0x1

    .line 2611512
    iput-boolean v2, v0, LX/Ik0;->d:Z

    .line 2611513
    move-object v0, v0

    .line 2611514
    invoke-virtual {v0}, LX/Ik0;->a()LX/Ik2;

    move-result-object v0

    .line 2611515
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->f:LX/Ijy;

    invoke-virtual {v2, v0, v1}, LX/Ijy;->a(LX/Ik2;LX/Ijx;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_1
    .end packed-switch
.end method

.method private t()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2611382
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->a:LX/5fv;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611383
    iget-object v2, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v1, v2

    .line 2611384
    iget-object v2, v1, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2611385
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611386
    iget-object v3, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v2, v3

    .line 2611387
    iget-object v3, v2, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2611388
    invoke-virtual {v0, v1, v2}, LX/5fv;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2611389
    sget-object v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->t:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, v0}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Lcom/facebook/payments/currency/CurrencyAmount;)I

    move-result v0

    if-gez v0, :cond_0

    .line 2611390
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->H(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611391
    :goto_0
    return-void

    .line 2611392
    :catch_0
    move-exception v0

    .line 2611393
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2611394
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611395
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v0, v2

    .line 2611396
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611397
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v0, v2

    .line 2611398
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v0

    .line 2611399
    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082ca2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082ca3

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->a:LX/5fv;

    sget-object v7, LX/5fu;->NO_EMPTY_DECIMALS:LX/5fu;

    invoke-virtual {v6, v1, v7}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v8

    const/4 v1, 0x1

    aput-object v0, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f082ca4

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v2, v0, v1, v3, v8}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v0

    .line 2611400
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->H:LX/6wv;

    .line 2611401
    iput-object v1, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2611402
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2611403
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    goto :goto_0

    .line 2611404
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611405
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v0, v2

    .line 2611406
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static x(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 2

    .prologue
    .line 2611374
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->c:LX/73q;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/73q;->a(Landroid/app/Activity;)V

    .line 2611375
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2611376
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2611364
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2611365
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2611366
    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 2611367
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2611368
    const-string v1, "payment_flow_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5g0;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->C:LX/5g0;

    .line 2611369
    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2611370
    new-instance v0, LX/Ins;

    invoke-direct {v0, p0}, LX/Ins;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611371
    new-instance v1, LX/Int;

    invoke-direct {v1, p0}, LX/Int;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611372
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->e:LX/0Xj;

    invoke-virtual {v2}, LX/0Xk;->a()LX/0YX;

    move-result-object v2

    const-string p1, "com.facebook.messaging.payment.ACTION_PAYMENT_CARD_UPDATED"

    invoke-interface {v2, p1, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string p1, "com.facebook.payments.auth.ACTION_PIN_UPDATED"

    invoke-interface {v2, p1, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    const-string v2, "com.facebook.orca.CONNECTIVITY_CHANGED"

    invoke-interface {v0, v2, v1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->E:LX/0Yb;

    .line 2611373
    return-void
.end method

.method public final c()Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;
    .locals 1

    .prologue
    .line 2611361
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611362
    iget-object p0, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v0, p0

    .line 2611363
    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2611358
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611359
    iget-object p0, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->s:Ljava/lang/String;

    move-object v0, p0

    .line 2611360
    return-object v0
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 2611351
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2611352
    :goto_0
    return-void

    .line 2611353
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->A:LX/IoU;

    invoke-interface {v0}, LX/IoU;->getImmediateFocusView()Landroid/view/View;

    move-result-object v0

    .line 2611354
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 2611355
    if-nez v0, :cond_1

    .line 2611356
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_0

    .line 2611357
    :cond_1
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->c:LX/73q;

    invoke-virtual {v2, v1, v0}, LX/73q;->a(Landroid/app/Activity;Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2611335
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2611336
    sparse-switch p1, :sswitch_data_0

    .line 2611337
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->A:LX/IoU;

    invoke-interface {v0}, LX/IoU;->a()V

    .line 2611338
    return-void

    .line 2611339
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->f:LX/Ijy;

    invoke-virtual {v0, p1, p2, p3}, LX/Ijy;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 2611340
    :sswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->f:LX/Ijy;

    invoke-virtual {v0, p1, p2, p3}, LX/Ijy;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 2611341
    :sswitch_2
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 2611342
    const-string v0, "selected_payment_method"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 2611343
    if-eqz v0, :cond_0

    .line 2611344
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 2611345
    iget-object p1, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->g:LX/0am;

    if-eqz p1, :cond_3

    .line 2611346
    iget-object p1, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->g:LX/0am;

    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result p1

    if-nez p1, :cond_2

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result p1

    if-nez p1, :cond_2

    .line 2611347
    :cond_1
    :goto_1
    goto :goto_0

    .line 2611348
    :cond_2
    iget-object p1, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->g:LX/0am;

    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->g:LX/0am;

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 2611349
    :cond_3
    iput-object v0, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->g:LX/0am;

    .line 2611350
    invoke-static {v1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_2
        0x3e8 -> :sswitch_0
        0x3e9 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 2611331
    const v0, 0x7f110026

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2611332
    const v0, 0x7f0d324a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->z:Landroid/view/MenuItem;

    .line 2611333
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->A:LX/IoU;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->z:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, LX/IoU;->a(Landroid/view/MenuItem;)V

    .line 2611334
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x746a8b49

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2611329
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->q:LX/IoW;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->C:LX/5g0;

    invoke-virtual {v0, v2}, LX/IoW;->a(LX/5g0;)LX/IoU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->A:LX/IoU;

    .line 2611330
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->A:LX/IoU;

    check-cast v0, Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x6a05a4de

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7e62efad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2611324
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2611325
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->m:LX/Inh;

    invoke-virtual {v1}, LX/Inh;->a()V

    .line 2611326
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->p:LX/Inc;

    invoke-virtual {v1}, LX/Inc;->a()V

    .line 2611327
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->E:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2611328
    const/16 v1, 0x2b

    const v2, -0x2881a5e5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2611320
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0d324a

    if-ne v0, v1, :cond_0

    .line 2611321
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b(Z)V

    .line 2611322
    const/4 v0, 0x1

    .line 2611323
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x38528e3c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2611313
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2611314
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->E:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 2611315
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611316
    iget-object v2, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    move-object v1, v2

    .line 2611317
    sget-object v2, LX/IoT;->PROCESSING_CHECK_AUTHENTICATION:LX/IoT;

    if-eq v1, v2, :cond_0

    .line 2611318
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->F(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611319
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x2f8d8c29

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2611310
    const-string v0, "messenger_pay_data"

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2611311
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2611312
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2611263
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2611264
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->A:LX/IoU;

    new-instance v1, LX/Io1;

    invoke-direct {v1, p0}, LX/Io1;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    invoke-interface {v0, v1}, LX/IoU;->setListener(LX/Io1;)V

    .line 2611265
    if-eqz p2, :cond_4

    .line 2611266
    const-string v0, "messenger_pay_data"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->a(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2611267
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->m$redex0(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611268
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->m:LX/Inh;

    new-instance v1, LX/Io2;

    invoke-direct {v1, p0}, LX/Io2;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    invoke-virtual {v0, v1}, LX/Inh;->a(LX/Io2;)V

    .line 2611269
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->p:LX/Inc;

    new-instance v1, LX/Io3;

    invoke-direct {v1, p0}, LX/Io3;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    invoke-virtual {v0, v1}, LX/Inc;->a(LX/Io3;)V

    .line 2611270
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->p:LX/Inc;

    .line 2611271
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2611272
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, v1, v2}, LX/Inc;->a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2611273
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->k()V

    .line 2611274
    invoke-static {}, LX/6nh;->newBuilder()LX/6ng;

    move-result-object v0

    .line 2611275
    iput-object p0, v0, LX/6ng;->a:Lcom/facebook/base/fragment/FbFragment;

    .line 2611276
    move-object v0, v0

    .line 2611277
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->L:LX/6nc;

    .line 2611278
    iput-object v1, v0, LX/6ng;->b:LX/6nc;

    .line 2611279
    move-object v0, v0

    .line 2611280
    const-string v1, "fingerprint_dialog_%s"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->C:LX/5g0;

    invoke-virtual {v2}, LX/5g0;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2611281
    iput-object v1, v0, LX/6ng;->c:Ljava/lang/String;

    .line 2611282
    move-object v0, v0

    .line 2611283
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2611284
    iget-object v2, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    move-object v1, v2

    .line 2611285
    iput-object v1, v0, LX/6ng;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2611286
    move-object v0, v0

    .line 2611287
    invoke-virtual {v0}, LX/6ng;->a()LX/6nh;

    move-result-object v0

    .line 2611288
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->s:LX/6ns;

    invoke-virtual {v1, v0}, LX/6ns;->a(LX/6nh;)LX/6nr;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->G:LX/6nr;

    .line 2611289
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 2611290
    if-eqz v0, :cond_0

    .line 2611291
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->H:LX/6wv;

    .line 2611292
    iput-object v1, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2611293
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 2611294
    if-eqz v0, :cond_1

    .line 2611295
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->J:LX/6wv;

    .line 2611296
    iput-object v1, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2611297
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 2611298
    if-eqz v0, :cond_2

    .line 2611299
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->I:LX/6wv;

    .line 2611300
    iput-object v1, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2611301
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/PaymentSelectCardDialogFragment;

    .line 2611302
    if-eqz v0, :cond_3

    .line 2611303
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->K:LX/Inz;

    .line 2611304
    iput-object v1, v0, Lcom/facebook/messaging/payment/value/input/PaymentSelectCardDialogFragment;->n:LX/Inz;

    .line 2611305
    :cond_3
    return-void

    .line 2611306
    :cond_4
    new-instance v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-direct {v0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->a(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2611307
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->n:LX/InZ;

    .line 2611308
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2611309
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, v1, v2}, LX/InZ;->a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto/16 :goto_0
.end method
