.class public Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/IoU;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/J1j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IoY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Lcom/facebook/common/callercontext/CallerContext;

.field public e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field public f:Landroid/widget/ProgressBar;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Lcom/facebook/resources/ui/FbButton;

.field public i:Landroid/widget/LinearLayout;

.field public j:Landroid/widget/LinearLayout;

.field public k:Lcom/facebook/resources/ui/FbTextView;

.field public l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Lcom/facebook/messaging/payment/value/input/MemoInputView;

.field public n:Landroid/support/v4/view/ViewPager;

.field public o:LX/Io1;

.field public p:LX/J1i;

.field public q:LX/Ios;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2612519
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2612520
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Landroid/content/Context;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2612521
    const-class v0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2612522
    const v0, 0x7f030de5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2612523
    const v0, 0x7f0d220d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->n:Landroid/support/v4/view/ViewPager;

    .line 2612524
    const v0, 0x7f0d2202

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2612525
    const v0, 0x7f0d2204

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->f:Landroid/widget/ProgressBar;

    .line 2612526
    const v0, 0x7f0d2209

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2612527
    const v0, 0x7f0d220a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->h:Lcom/facebook/resources/ui/FbButton;

    .line 2612528
    const v0, 0x7f0d2208

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->i:Landroid/widget/LinearLayout;

    .line 2612529
    const v0, 0x7f0d2205

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->j:Landroid/widget/LinearLayout;

    .line 2612530
    const v0, 0x7f0d2206

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2612531
    const v0, 0x7f0d2207

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2612532
    const v0, 0x7f0d220b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/MemoInputView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->m:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    .line 2612533
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->b()V

    .line 2612534
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setLongClickable(Z)V

    .line 2612535
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->h:Lcom/facebook/resources/ui/FbButton;

    new-instance p1, LX/Ip8;

    invoke-direct {p1, p0}, LX/Ip8;-><init>(Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;)V

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2612536
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->l:Lcom/facebook/resources/ui/FbTextView;

    new-instance p1, LX/Ip9;

    invoke-direct {p1, p0}, LX/Ip9;-><init>(Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;)V

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2612537
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const-class v2, LX/J1j;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/J1j;

    invoke-static {p0}, LX/IoY;->b(LX/0QB;)LX/IoY;

    move-result-object p0

    check-cast p0, LX/IoY;

    iput-object v1, p1, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->a:Landroid/content/res/Resources;

    iput-object v2, p1, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->b:LX/J1j;

    iput-object p0, p1, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->c:LX/IoY;

    return-void
.end method

.method public static setPaymentCardInfoVisibility(Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;I)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 2612538
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2612539
    if-nez p1, :cond_0

    .line 2612540
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612541
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612542
    :goto_0
    return-void

    .line 2612543
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612544
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612545
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->k:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f082c85

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2612546
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->k:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f020e62

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 2612547
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2612548
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->p:LX/J1i;

    invoke-virtual {v0}, LX/J1i;->a()V

    .line 2612549
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)V
    .locals 6
    .param p1    # Landroid/view/MenuItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2612550
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->c:LX/IoY;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v2, v1, LX/Ios;->b:LX/0am;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v3, v1, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v4, v1, LX/Ios;->a:LX/IoT;

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/IoY;->a(Landroid/view/MenuItem;LX/0am;Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;LX/IoT;Z)V

    .line 2612551
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2612552
    return-void
.end method

.method public getImmediateFocusView()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2612553
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    return-object v0
.end method

.method public setListener(LX/Io1;)V
    .locals 2

    .prologue
    .line 2612554
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->o:LX/Io1;

    .line 2612555
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->m:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    new-instance v1, LX/IpA;

    invoke-direct {v1, p0}, LX/IpA;-><init>(Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MemoInputView;->setListener(LX/IoG;)V

    .line 2612556
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->n:Landroid/support/v4/view/ViewPager;

    new-instance v1, LX/IpB;

    invoke-direct {v1, p0}, LX/IpB;-><init>(Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2612557
    return-void
.end method

.method public setMessengerPayViewParams(LX/Ios;)V
    .locals 4

    .prologue
    .line 2612558
    check-cast p1, LX/Ios;

    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    .line 2612559
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->p:LX/J1i;

    if-eqz v0, :cond_0

    .line 2612560
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->p:LX/J1i;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v1, v1, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2612561
    iget-object v2, v1, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2612562
    invoke-virtual {v0, v1}, LX/J1i;->a(Ljava/lang/String;)V

    .line 2612563
    sget-object v0, LX/IpD;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    .line 2612564
    iget-object v2, v1, LX/Ios;->a:LX/IoT;

    move-object v1, v2

    .line 2612565
    invoke-virtual {v1}, LX/IoT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2612566
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->p:LX/J1i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/J1i;->a(Z)V

    .line 2612567
    :goto_1
    const/4 v3, 0x0

    const/16 v1, 0x8

    .line 2612568
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    if-nez v0, :cond_1

    .line 2612569
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612570
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612571
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2612572
    :goto_2
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->m:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v1, v1, LX/Ios;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MemoInputView;->setMemoText(Ljava/lang/String;)V

    .line 2612573
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v0, v0, LX/Ios;->h:Ljava/util/List;

    if-nez v0, :cond_3

    .line 2612574
    :goto_3
    return-void

    .line 2612575
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->b:LX/J1j;

    new-instance v1, LX/IpC;

    invoke-direct {v1, p0}, LX/IpC;-><init>(Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;)V

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v3, v3, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2612576
    iget-object p1, v3, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->b:Ljava/lang/String;

    move-object v3, p1

    .line 2612577
    invoke-virtual {v0, v1, v2, v3}, LX/J1j;->a(LX/Ioo;ZLjava/lang/String;)LX/J1i;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->p:LX/J1i;

    .line 2612578
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->p:LX/J1i;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->e:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0, v1}, LX/J1i;->a(Lcom/facebook/payments/p2p/ui/DollarIconEditText;)V

    goto :goto_0

    .line 2612579
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->p:LX/J1i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/J1i;->a(Z)V

    goto :goto_1

    .line 2612580
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2612581
    invoke-static {p0, v1}, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->setPaymentCardInfoVisibility(Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;I)V

    goto :goto_2

    .line 2612582
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->g:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/p2p/model/PaymentCard;->c(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2612583
    invoke-static {p0, v3}, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->setPaymentCardInfoVisibility(Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;I)V

    goto :goto_2

    .line 2612584
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-nez v0, :cond_4

    .line 2612585
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->n:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v2, v2, LX/Ios;->h:Ljava/util/List;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;-><init>(Ljava/util/List;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    goto :goto_3

    .line 2612586
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->n:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v1, v1, LX/Ios;->h:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;->q:LX/Ios;

    iget-object v2, v2, LX/Ios;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    invoke-static {v1, v2}, LX/Ipi;->a(Ljava/util/List;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
