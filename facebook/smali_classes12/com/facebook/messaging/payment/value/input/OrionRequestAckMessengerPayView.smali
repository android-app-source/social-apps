.class public Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/IoU;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1zC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/J1k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final f:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field public final g:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field public final h:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field public final i:Lcom/facebook/user/tiles/UserTileView;

.field public final j:Landroid/widget/ProgressBar;

.field public final k:Lcom/facebook/resources/ui/FbTextView;

.field public final l:Lcom/facebook/resources/ui/FbButton;

.field public final m:Landroid/widget/LinearLayout;

.field public n:Lcom/facebook/widget/text/BetterButton;

.field public o:Lcom/facebook/resources/ui/FbTextView;

.field public p:LX/1af;

.field public q:LX/Io1;

.field public r:LX/Ios;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2612819
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2612820
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2612746
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2612747
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2612800
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2612801
    const-class v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2612802
    const v0, 0x7f030de9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2612803
    const v0, 0x7f0d2212

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2612804
    const v0, 0x7f0d2213

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->f:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2612805
    const v0, 0x7f0d2201

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->g:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2612806
    const v0, 0x7f0d1b7f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->h:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2612807
    const v0, 0x7f0d21ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->i:Lcom/facebook/user/tiles/UserTileView;

    .line 2612808
    const v0, 0x7f0d2204

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->j:Landroid/widget/ProgressBar;

    .line 2612809
    const v0, 0x7f0d2209

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2612810
    const v0, 0x7f0d220a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->l:Lcom/facebook/resources/ui/FbButton;

    .line 2612811
    const v0, 0x7f0d2208

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->m:Landroid/widget/LinearLayout;

    .line 2612812
    const v0, 0x7f0d2215

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->n:Lcom/facebook/widget/text/BetterButton;

    .line 2612813
    const v0, 0x7f0d2216

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2612814
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->d:LX/0ad;

    sget-short v1, LX/Iie;->e:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2612815
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->o:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2612816
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->f:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->b()V

    .line 2612817
    return-void

    .line 2612818
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v2

    check-cast v2, LX/1zC;

    invoke-static {p0}, LX/J1k;->b(LX/0QB;)LX/J1k;

    move-result-object v3

    check-cast v3, LX/J1k;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v1, p1, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->a:Landroid/content/res/Resources;

    iput-object v2, p1, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->b:LX/1zC;

    iput-object v3, p1, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->c:LX/J1k;

    iput-object p0, p1, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->d:LX/0ad;

    return-void
.end method

.method public static f(Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;)V
    .locals 3

    .prologue
    .line 2612793
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    if-eqz v0, :cond_0

    .line 2612794
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->f:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v1, v1, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2612795
    iget-object v2, v1, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2612796
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v2, v2, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2612797
    iget-object p0, v2, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v2, p0

    .line 2612798
    invoke-virtual {v0, v1, v2}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612799
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2612821
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)V
    .locals 4
    .param p1    # Landroid/view/MenuItem;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2612782
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    if-nez v0, :cond_0

    .line 2612783
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->n:Lcom/facebook/widget/text/BetterButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2612784
    :goto_0
    return-void

    .line 2612785
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->n:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2612786
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->n:Lcom/facebook/widget/text/BetterButton;

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f082d22

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterButton;->setText(I)V

    .line 2612787
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->c:LX/J1k;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v2, v2, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    invoke-virtual {v0, v2}, LX/J1k;->a(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)Z

    move-result v0

    .line 2612788
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->n:Lcom/facebook/widget/text/BetterButton;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    .line 2612789
    iget-object v3, v0, LX/Ios;->a:LX/IoT;

    move-object v0, v3

    .line 2612790
    sget-object v3, LX/IoT;->PREPARE_PAYMENT:LX/IoT;

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterButton;->setEnabled(Z)V

    goto :goto_0

    .line 2612791
    :cond_1
    const v0, 0x7f082c7f

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2612792
    goto :goto_2
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2612781
    return-void
.end method

.method public getImmediateFocusView()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2612780
    const/4 v0, 0x0

    return-object v0
.end method

.method public setListener(LX/Io1;)V
    .locals 1

    .prologue
    .line 2612775
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->q:LX/Io1;

    .line 2612776
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->n:Lcom/facebook/widget/text/BetterButton;

    new-instance p1, LX/IpI;

    invoke-direct {p1, p0}, LX/IpI;-><init>(Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2612777
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->l:Lcom/facebook/resources/ui/FbButton;

    new-instance p1, LX/IpJ;

    invoke-direct {p1, p0}, LX/IpJ;-><init>(Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;)V

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2612778
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->o:Lcom/facebook/resources/ui/FbTextView;

    new-instance p1, LX/IpK;

    invoke-direct {p1, p0}, LX/IpK;-><init>(Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;)V

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2612779
    return-void
.end method

.method public setMessengerPayViewParams(LX/Ios;)V
    .locals 3

    .prologue
    .line 2612748
    check-cast p1, LX/Ios;

    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    .line 2612749
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->d:Lcom/facebook/user/model/UserKey;

    if-eqz v0, :cond_0

    .line 2612750
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->i:Lcom/facebook/user/tiles/UserTileView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v1, v1, LX/Ios;->d:Lcom/facebook/user/model/UserKey;

    sget-object v2, LX/8ue;->NONE:LX/8ue;

    invoke-static {v1, v2}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2612751
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->g:Lcom/facebook/payments/ui/FloatingLabelTextView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->a:Landroid/content/res/Resources;

    const v2, 0x7f082d20

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2612752
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->c:Lcom/facebook/user/model/Name;

    if-eqz v0, :cond_1

    .line 2612753
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->g:Lcom/facebook/payments/ui/FloatingLabelTextView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v1, v1, LX/Ios;->c:Lcom/facebook/user/model/Name;

    invoke-virtual {v1}, Lcom/facebook/user/model/Name;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2612754
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2612755
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->h:Lcom/facebook/payments/ui/FloatingLabelTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setVisibility(I)V

    .line 2612756
    :goto_0
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->f(Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;)V

    .line 2612757
    const/4 p1, 0x0

    const/16 v1, 0x8

    .line 2612758
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    if-nez v0, :cond_3

    .line 2612759
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2612760
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->j:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2612761
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    if-nez v0, :cond_5

    .line 2612762
    :goto_2
    return-void

    .line 2612763
    :cond_2
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v2, v2, LX/Ios;->g:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2612764
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->b:LX/1zC;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->h:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/FloatingLabelTextView;->getTextSize()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v0, v2}, LX/1zC;->a(Landroid/text/Editable;I)Z

    .line 2612765
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->h:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2612766
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->h:Lcom/facebook/payments/ui/FloatingLabelTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setVisibility(I)V

    goto :goto_0

    .line 2612767
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->j:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2612768
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2612769
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 2612770
    :cond_4
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->k:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v0, v0, LX/Ios;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v2}, Lcom/facebook/payments/p2p/model/PaymentCard;->c(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2612771
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 2612772
    :cond_5
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    new-instance v1, LX/Ipj;

    invoke-direct {v1}, LX/Ipj;-><init>()V

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->p:LX/1af;

    .line 2612773
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->p:LX/1af;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2612774
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->r:LX/Ios;

    iget-object v1, v1, LX/Ios;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    invoke-static {v1}, LX/Ipi;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Landroid/content/Context;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_2
.end method
