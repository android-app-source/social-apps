.class public Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;
.super LX/0gG;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$Theme;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/1af;


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$Theme;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2613210
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2613211
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->b:Ljava/util/List;

    .line 2613212
    iput-object p2, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->a:Landroid/content/Context;

    .line 2613213
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2613214
    new-instance v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2613215
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    if-lez p2, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le p2, v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2613216
    :goto_0
    return-object v0

    .line 2613217
    :cond_1
    new-instance v0, LX/1Uo;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    new-instance v2, LX/Ipj;

    invoke-direct {v2}, LX/Ipj;-><init>()V

    invoke-virtual {v0, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->c:LX/1af;

    .line 2613218
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->c:LX/1af;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2613219
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->b:Ljava/util/List;

    add-int/lit8 v2, p2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    invoke-static {v0}, LX/Ipi;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;)Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Landroid/content/Context;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2613220
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->a:Landroid/content/Context;

    const v3, 0x106000d

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v0, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2613221
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2613222
    check-cast p1, Landroid/support/v4/view/ViewPager;

    invoke-virtual {p1, v1}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;)V

    move-object v0, v1

    .line 2613223
    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2613224
    check-cast p1, Landroid/support/v4/view/ViewPager;

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 2613225
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2613226
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2613227
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2613228
    const/4 v0, 0x1

    .line 2613229
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/ThemePagerAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
