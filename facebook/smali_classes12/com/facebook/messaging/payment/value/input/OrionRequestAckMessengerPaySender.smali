.class public Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ing;


# instance fields
.field private final a:LX/5fv;

.field private final b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field private final c:LX/3Ed;

.field private final d:Ljava/util/concurrent/Executor;

.field public final e:LX/03V;

.field public final f:Landroid/content/Context;

.field public final g:LX/IpF;

.field public final h:LX/InS;

.field private i:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

.field public k:LX/Io2;


# direct methods
.method public constructor <init>(LX/5fv;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;LX/3Ed;LX/03V;Ljava/util/concurrent/Executor;Landroid/content/Context;LX/IpF;LX/InS;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2612713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2612714
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->a:LX/5fv;

    .line 2612715
    iput-object p2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2612716
    iput-object p3, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->c:LX/3Ed;

    .line 2612717
    iput-object p4, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->e:LX/03V;

    .line 2612718
    iput-object p5, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->d:Ljava/util/concurrent/Executor;

    .line 2612719
    iput-object p6, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->f:Landroid/content/Context;

    .line 2612720
    iput-object p7, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->g:LX/IpF;

    .line 2612721
    iput-object p8, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->h:LX/InS;

    .line 2612722
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2612709
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2612710
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2612711
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612712
    :cond_0
    return-void
.end method

.method public final a(LX/Io2;)V
    .locals 0

    .prologue
    .line 2612652
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->k:LX/Io2;

    .line 2612653
    return-void
.end method

.method public final a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V
    .locals 13

    .prologue
    .line 2612654
    iput-object p2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->j:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612655
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2612656
    :goto_0
    return-void

    .line 2612657
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->j:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612658
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v0, v1

    .line 2612659
    iget-object v1, v0, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2612660
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->j:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612661
    iget-object v2, v1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v1, v2

    .line 2612662
    iget-object v2, v1, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2612663
    :try_start_0
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->a:LX/5fv;

    invoke-virtual {v2, v1, v0}, LX/5fv;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2612664
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->j:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612665
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    move-object v0, v2

    .line 2612666
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2612667
    iget-wide v7, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v2, v7

    .line 2612668
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2612669
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->j:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612670
    iget-object v3, v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v0, v3

    .line 2612671
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    .line 2612672
    const-string v0, "payment_request"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2612673
    new-instance v4, LX/IpG;

    invoke-direct {v4, p0, v0}, LX/IpG;-><init>(Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    .line 2612674
    invoke-static {}, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->newBuilder()LX/J1W;

    move-result-object v5

    .line 2612675
    iput-object v1, v5, LX/J1W;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2612676
    move-object v1, v5

    .line 2612677
    iput-object v2, v1, LX/J1W;->b:Ljava/lang/String;

    .line 2612678
    move-object v1, v1

    .line 2612679
    iput-object v3, v1, LX/J1W;->c:Ljava/lang/String;

    .line 2612680
    move-object v1, v1

    .line 2612681
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->j:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612682
    iget-object v3, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->u:Ljava/lang/String;

    move-object v2, v3

    .line 2612683
    iput-object v2, v1, LX/J1W;->e:Ljava/lang/String;

    .line 2612684
    move-object v1, v1

    .line 2612685
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->j:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612686
    iget-object v3, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->v:Ljava/lang/String;

    move-object v2, v3

    .line 2612687
    iput-object v2, v1, LX/J1W;->f:Ljava/lang/String;

    .line 2612688
    move-object v1, v1

    .line 2612689
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 2612690
    iput-object v2, v1, LX/J1W;->m:Ljava/lang/String;

    .line 2612691
    move-object v1, v1

    .line 2612692
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->c:LX/3Ed;

    invoke-virtual {v2}, LX/3Ed;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2612693
    iput-object v2, v1, LX/J1W;->h:Ljava/lang/String;

    .line 2612694
    move-object v1, v1

    .line 2612695
    invoke-virtual {v1}, LX/J1W;->o()Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;

    move-result-object v1

    .line 2612696
    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    iget-object v3, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->f:Landroid/content/Context;

    iget-object v5, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->f:Landroid/content/Context;

    const v6, 0x7f082d23

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2612697
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 2612698
    sget-object v7, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->a:Ljava/lang/String;

    invoke-virtual {v9, v7, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2612699
    iget-object v7, v2, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a:LX/0aG;

    const-string v8, "mc_place_order"

    sget-object v10, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v11

    const v12, 0x73fbf9ff

    invoke-static/range {v7 .. v12}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v7

    .line 2612700
    if-eqz v5, :cond_1

    .line 2612701
    new-instance v8, LX/4At;

    invoke-direct {v8, v3, v5}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v7, v8}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    .line 2612702
    :cond_1
    invoke-interface {v7}, LX/1MF;->start()LX/1ML;

    move-result-object v7

    .line 2612703
    new-instance v8, LX/J00;

    invoke-direct {v8, v2}, LX/J00;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    iget-object v9, v2, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->d:Ljava/util/concurrent/Executor;

    invoke-static {v7, v8, v9}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v1, v7

    .line 2612704
    iput-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2612705
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->d:Ljava/util/concurrent/Executor;

    invoke-static {v1, v4, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2612706
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPaySender;->g:LX/IpF;

    const-string v2, "p2p_confirm_send"

    invoke-virtual {v1, v2, v0}, LX/IpF;->a(Ljava/lang/String;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    goto/16 :goto_0

    .line 2612707
    :catch_0
    move-exception v0

    .line 2612708
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
