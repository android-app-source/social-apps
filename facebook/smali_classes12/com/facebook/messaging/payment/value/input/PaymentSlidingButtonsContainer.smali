.class public Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/J1k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

.field private c:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

.field private d:Landroid/widget/LinearLayout;

.field public e:LX/Ip5;

.field public f:LX/Iph;

.field private g:LX/Ipg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2613185
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2613186
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b()V

    .line 2613187
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2613182
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2613183
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b()V

    .line 2613184
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2613179
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2613180
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b()V

    .line 2613181
    return-void
.end method

.method private a(LX/Iph;)V
    .locals 2

    .prologue
    .line 2613169
    sget-object v0, LX/Ipf;->b:[I

    invoke-virtual {p1}, LX/Iph;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2613170
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid state."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2613171
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->d()V

    .line 2613172
    :goto_0
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->f:LX/Iph;

    .line 2613173
    return-void

    .line 2613174
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->e()V

    goto :goto_0

    .line 2613175
    :pswitch_2
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->g()V

    goto :goto_0

    .line 2613176
    :pswitch_3
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->h()V

    goto :goto_0

    .line 2613177
    :pswitch_4
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->i()V

    goto :goto_0

    .line 2613178
    :pswitch_5
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->j()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static a(Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V
    .locals 4

    .prologue
    .line 2613164
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2613165
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 2613166
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/animation/Animator;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 2613167
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 2613168
    return-void
.end method

.method private static a(Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;)V
    .locals 2

    .prologue
    .line 2613160
    sget-object v0, LX/IoL;->SELECTED:LX/IoL;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a(LX/IoL;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2613161
    sget-object v1, LX/IoL;->HIDDEN:LX/IoL;

    invoke-virtual {p1, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a(LX/IoL;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 2613162
    invoke-static {v0, v1}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->a(Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V

    .line 2613163
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    invoke-static {v0}, LX/J1k;->b(LX/0QB;)LX/J1k;

    move-result-object v0

    check-cast v0, LX/J1k;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->a:LX/J1k;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2613148
    const-class v0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2613149
    const v0, 0x7f030de2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2613150
    const v0, 0x7f0d1d28

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->d:Landroid/widget/LinearLayout;

    .line 2613151
    const v0, 0x7f0d21fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    .line 2613152
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    sget-object v1, LX/IoK;->REQUEST:LX/IoK;

    .line 2613153
    iput-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->d:LX/IoK;

    .line 2613154
    const v0, 0x7f0d21fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->c:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    .line 2613155
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->c:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    sget-object v1, LX/IoK;->PAY:LX/IoK;

    .line 2613156
    iput-object v1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->d:LX/IoK;

    .line 2613157
    sget-object v0, LX/Iph;->DISABLED:LX/Iph;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->setState(LX/Iph;)V

    .line 2613158
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->c()V

    .line 2613159
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2613145
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    new-instance v1, LX/Ipb;

    invoke-direct {v1, p0}, LX/Ipb;-><init>(Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2613146
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->c:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    new-instance v1, LX/Ipc;

    invoke-direct {v1, p0}, LX/Ipc;-><init>(Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2613147
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2613143
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x19

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/Ipd;

    invoke-direct {v1, p0}, LX/Ipd;-><init>(Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2613144
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 2613188
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->f()V

    .line 2613189
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->setVisibility(I)V

    .line 2613190
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/Ipe;

    invoke-direct {v1, p0}, LX/Ipe;-><init>(Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2613191
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 2613100
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    sget-object v1, LX/IoL;->NORMAL:LX/IoL;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a(LX/IoL;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2613101
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->c:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    sget-object v2, LX/IoL;->NORMAL:LX/IoL;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a(LX/IoL;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 2613102
    invoke-static {v0, v1}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->a(Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V

    .line 2613103
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->d:Landroid/widget/LinearLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 2613104
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2613105
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->c:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    invoke-static {v0, v1}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->a(Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;)V

    .line 2613106
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->d:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 2613107
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2613108
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->c:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    invoke-static {v0, v1}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->a(Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;)V

    .line 2613109
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->d:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 2613110
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2613111
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->e:LX/Ip5;

    if-eqz v0, :cond_0

    .line 2613112
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    sget-object v1, LX/IoL;->CONFIRMED:LX/IoL;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a(LX/IoL;)Landroid/animation/ObjectAnimator;

    .line 2613113
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->e:LX/Ip5;

    invoke-interface {v0}, LX/Ip5;->a()V

    .line 2613114
    :cond_0
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 2613115
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->e:LX/Ip5;

    if-eqz v0, :cond_0

    .line 2613116
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->c:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    sget-object v1, LX/IoL;->CONFIRMED:LX/IoL;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a(LX/IoL;)Landroid/animation/ObjectAnimator;

    .line 2613117
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->e:LX/Ip5;

    invoke-interface {v0}, LX/Ip5;->b()V

    .line 2613118
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2613119
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->f:LX/Iph;

    sget-object v1, LX/Iph;->DISABLED:LX/Iph;

    if-eq v0, v1, :cond_0

    .line 2613120
    sget-object v0, LX/Iph;->NORMAL:LX/Iph;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->setState(LX/Iph;)V

    .line 2613121
    :cond_0
    return-void
.end method

.method public getState()LX/Iph;
    .locals 1

    .prologue
    .line 2613122
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->f:LX/Iph;

    return-object v0
.end method

.method public setButtonMode(LX/Ipg;)V
    .locals 2

    .prologue
    .line 2613123
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->g:LX/Ipg;

    .line 2613124
    sget-object v0, LX/Ipf;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->g:LX/Ipg;

    invoke-virtual {v1}, LX/Ipg;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2613125
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid mode of SlideInSlideOutContainer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2613126
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->c:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    sget-object v1, LX/IoL;->DISABLED:LX/IoL;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a(LX/IoL;)Landroid/animation/ObjectAnimator;

    .line 2613127
    :goto_0
    :pswitch_1
    return-void

    .line 2613128
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    sget-object v1, LX/IoL;->DISABLED:LX/IoL;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a(LX/IoL;)Landroid/animation/ObjectAnimator;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setListener(LX/Ip5;)V
    .locals 0

    .prologue
    .line 2613129
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->e:LX/Ip5;

    .line 2613130
    return-void
.end method

.method public setPaymentValue(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)V
    .locals 1

    .prologue
    .line 2613131
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->b:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    .line 2613132
    iput-object p1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->e:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2613133
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->c:Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;

    .line 2613134
    iput-object p1, v0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->e:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2613135
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->a:LX/J1k;

    invoke-virtual {v0, p1}, LX/J1k;->a(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)Z

    move-result v0

    .line 2613136
    if-eqz v0, :cond_0

    .line 2613137
    sget-object v0, LX/Iph;->NORMAL:LX/Iph;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->setState(LX/Iph;)V

    .line 2613138
    :goto_0
    return-void

    .line 2613139
    :cond_0
    sget-object v0, LX/Iph;->DISABLED:LX/Iph;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->setState(LX/Iph;)V

    goto :goto_0
.end method

.method public setState(LX/Iph;)V
    .locals 1

    .prologue
    .line 2613140
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->f:LX/Iph;

    if-eq v0, p1, :cond_0

    .line 2613141
    invoke-direct {p0, p1}, Lcom/facebook/messaging/payment/value/input/PaymentSlidingButtonsContainer;->a(LX/Iph;)V

    .line 2613142
    :cond_0
    return-void
.end method
