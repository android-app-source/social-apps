.class public Lcom/facebook/messaging/payment/value/input/PaymentSelectCardDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/Inz;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2613053
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2613054
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    .line 2613055
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2613056
    const-string v1, "payment_cards"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2613057
    const-string v2, "credit_card_enabled"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2613058
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2613059
    if-eqz v0, :cond_0

    .line 2613060
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v1}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->c(LX/0Px;)LX/0Px;

    move-result-object v5

    invoke-static {v4, v5}, LX/Gza;->a(Landroid/content/Context;LX/0Px;)LX/0Px;

    move-result-object v4

    const v5, 0x7f082c84

    invoke-virtual {p0, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f082c88

    invoke-virtual {p0, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    new-instance v8, LX/IpZ;

    invoke-direct {v8, p0, v1}, LX/IpZ;-><init>(Lcom/facebook/messaging/payment/value/input/PaymentSelectCardDialogFragment;LX/0Px;)V

    invoke-static/range {v3 .. v8}, LX/Gza;->a(Landroid/content/Context;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/GzZ;)LX/2EJ;

    move-result-object v3

    .line 2613061
    :goto_0
    move-object v0, v3

    .line 2613062
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v1}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->d(LX/0Px;)LX/0Px;

    move-result-object v5

    invoke-static {v4, v5}, LX/Gza;->a(Landroid/content/Context;LX/0Px;)LX/0Px;

    move-result-object v4

    const v5, 0x7f082c83

    invoke-virtual {p0, v5}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f082c97

    invoke-virtual {p0, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f082c87

    invoke-virtual {p0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, LX/Ipa;

    invoke-direct {v8, p0, v1}, LX/Ipa;-><init>(Lcom/facebook/messaging/payment/value/input/PaymentSelectCardDialogFragment;LX/0Px;)V

    invoke-static/range {v3 .. v8}, LX/Gza;->a(Landroid/content/Context;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/GzZ;)LX/2EJ;

    move-result-object v3

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5ee9bded

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2613063
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2613064
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/payment/value/input/PaymentSelectCardDialogFragment;

    invoke-static {v1}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->a(LX/0QB;)Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    iput-object v1, p0, Lcom/facebook/messaging/payment/value/input/PaymentSelectCardDialogFragment;->m:Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    .line 2613065
    const/16 v1, 0x2b

    const v2, -0x76c8f0e2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
