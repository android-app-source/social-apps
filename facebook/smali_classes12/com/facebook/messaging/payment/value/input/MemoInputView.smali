.class public Lcom/facebook/messaging/payment/value/input/MemoInputView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/J1m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/Vibrator;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/J1c;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:Lcom/facebook/widget/text/BetterEditTextView;

.field public e:LX/IoG;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2611629
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/payment/value/input/MemoInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2611630
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2611627
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/payment/value/input/MemoInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2611628
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2611622
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2611623
    const-class v0, Lcom/facebook/messaging/payment/value/input/MemoInputView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/value/input/MemoInputView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2611624
    const v0, 0x7f030ac4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2611625
    const v0, 0x7f0d1b7f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->d:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2611626
    return-void
.end method

.method private static a(Lcom/facebook/messaging/payment/value/input/MemoInputView;LX/J1m;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/payment/value/input/MemoInputView;",
            "LX/J1m;",
            "LX/0Ot",
            "<",
            "Landroid/os/Vibrator;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/J1c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2611621
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->a:LX/J1m;

    iput-object p2, p0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->c:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/value/input/MemoInputView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/payment/value/input/MemoInputView;

    invoke-static {v1}, LX/J1m;->a(LX/0QB;)LX/J1m;

    move-result-object v0

    check-cast v0, LX/J1m;

    const/16 v2, 0x34

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2d73

    invoke-static {v1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/messaging/payment/value/input/MemoInputView;->a(Lcom/facebook/messaging/payment/value/input/MemoInputView;LX/J1m;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public setListener(LX/IoG;)V
    .locals 2

    .prologue
    .line 2611613
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->e:LX/IoG;

    .line 2611614
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->a:LX/J1m;

    new-instance v1, LX/IoF;

    invoke-direct {v1, p0}, LX/IoF;-><init>(Lcom/facebook/messaging/payment/value/input/MemoInputView;)V

    .line 2611615
    iput-object v1, v0, LX/J1m;->b:LX/IoE;

    .line 2611616
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->d:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->a:LX/J1m;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2611617
    return-void
.end method

.method public setMemoText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2611618
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->d:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2611619
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->d:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2611620
    :cond_0
    return-void
.end method
