.class public final Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wv;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V
    .locals 0

    .prologue
    .line 2611171
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment$3;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    .line 2611157
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment$3;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2611158
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->D:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v1}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2611159
    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->D:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2611160
    :cond_0
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2611161
    const-string v2, "payment_request"

    invoke-static {v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2611162
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->j:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082d25

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2611163
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2611164
    sget-object v6, Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;->a:Ljava/lang/String;

    new-instance v7, Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;

    invoke-direct {v7, v1}, Lcom/facebook/payments/p2p/service/model/request/DeclinePaymentRequestParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2611165
    iget-object v6, v2, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a:LX/0aG;

    const-string v7, "decline_payment_request"

    sget-object v9, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v10

    const v11, -0x7a56c1c

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    new-instance v7, LX/4At;

    invoke-direct {v7, v3, v4}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v6, v7}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    move-result-object v6

    move-object v1, v6

    .line 2611166
    iput-object v1, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->D:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2611167
    new-instance v1, LX/Inu;

    invoke-direct {v1, v0}, LX/Inu;-><init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611168
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->D:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v3, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->k:Ljava/util/concurrent/Executor;

    invoke-static {v2, v1, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2611169
    const-string v1, "p2p_confirm_decline"

    invoke-static {v0, v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->e(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;Ljava/lang/String;)V

    .line 2611170
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2611172
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2611156
    return-void
.end method
