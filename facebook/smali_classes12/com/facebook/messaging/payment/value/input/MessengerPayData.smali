.class public Lcom/facebook/messaging/payment/value/input/MessengerPayData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/payment/value/input/MessengerPayData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/Io4;

.field public b:LX/IoT;

.field public c:Z

.field public d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/PaymentMethod;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation
.end field

.field private k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/shipping/model/MailingAddress;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$Theme;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

.field public o:Lcom/facebook/user/model/Name;

.field public p:Lcom/facebook/user/model/UserKey;

.field public q:LX/03R;

.field public r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

.field public s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

.field private x:Lcom/facebook/common/locale/Country;

.field private y:Ljava/lang/String;

.field public z:LX/IoB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2611702
    new-instance v0, LX/IoH;

    invoke-direct {v0}, LX/IoH;-><init>()V

    sput-object v0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2611747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2611748
    sget-object v0, LX/IoT;->PREPARE_PAYMENT:LX/IoT;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    .line 2611749
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q:LX/03R;

    .line 2611750
    new-instance v0, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2611751
    sget-object v0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;->a:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    move-object v0, v0

    .line 2611752
    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->w:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    .line 2611753
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2611714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2611715
    const-class v0, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/Name;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    .line 2611716
    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    .line 2611717
    const-class v0, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2611718
    const-class v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2611719
    const-class v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Class;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->g:LX/0am;

    .line 2611720
    const-class v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Class;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    .line 2611721
    const-class v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2611722
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->e:LX/0Px;

    .line 2611723
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/03R;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q:LX/03R;

    .line 2611724
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->c:Z

    .line 2611725
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->s:Ljava/lang/String;

    .line 2611726
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->t:Ljava/lang/String;

    .line 2611727
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/IoT;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    .line 2611728
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->u:Ljava/lang/String;

    .line 2611729
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->v:Ljava/lang/String;

    .line 2611730
    const-class v0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->w:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    .line 2611731
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->m:Ljava/util/List;

    .line 2611732
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    .line 2611733
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->h:Ljava/util/List;

    .line 2611734
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    .line 2611735
    if-eqz v0, :cond_1

    .line 2611736
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 2611737
    :goto_1
    move-object v0, v0

    .line 2611738
    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->i:LX/0am;

    .line 2611739
    const-class v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-static {p1, v0}, LX/46R;->c(Landroid/os/Parcel;Ljava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->j:LX/0Px;

    .line 2611740
    const-class v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Class;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->k:LX/0am;

    .line 2611741
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    .line 2611742
    const-class v0, Lcom/facebook/common/locale/Country;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/locale/Country;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->x:Lcom/facebook/common/locale/Country;

    .line 2611743
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->y:Ljava/lang/String;

    .line 2611744
    const-class v0, LX/IoB;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IoB;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->z:LX/IoB;

    .line 2611745
    return-void

    .line 2611746
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V
    .locals 1

    .prologue
    .line 2611710
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a:LX/Io4;

    if-eqz v0, :cond_0

    .line 2611711
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a:LX/Io4;

    .line 2611712
    iget-object p0, v0, LX/Io4;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->m$redex0(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;)V

    .line 2611713
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/IoB;)V
    .locals 0

    .prologue
    .line 2611707
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->z:LX/IoB;

    .line 2611708
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2611709
    return-void
.end method

.method public final a(LX/IoT;)V
    .locals 1

    .prologue
    .line 2611703
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    invoke-virtual {v0, p1}, LX/IoT;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2611704
    :goto_0
    return-void

    .line 2611705
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    .line 2611706
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;)V
    .locals 1

    .prologue
    .line 2611634
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    if-ne v0, p1, :cond_0

    .line 2611635
    :goto_0
    return-void

    .line 2611636
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    .line 2611637
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)V
    .locals 1

    .prologue
    .line 2611698
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2611699
    :goto_0
    return-void

    .line 2611700
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2611701
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/user/model/Name;)V
    .locals 1

    .prologue
    .line 2611694
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    invoke-virtual {v0, p1}, Lcom/facebook/user/model/Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2611695
    :goto_0
    return-void

    .line 2611696
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    .line 2611697
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/user/model/UserKey;)V
    .locals 1

    .prologue
    .line 2611754
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0, p1}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2611755
    :goto_0
    return-void

    .line 2611756
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    .line 2611757
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2611690
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->s:Ljava/lang/String;

    invoke-static {p1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2611691
    :goto_0
    return-void

    .line 2611692
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->s:Ljava/lang/String;

    .line 2611693
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2611686
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q:LX/03R;

    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    if-ne v0, p1, :cond_0

    .line 2611687
    :goto_0
    return-void

    .line 2611688
    :cond_0
    invoke-static {p1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q:LX/03R;

    .line 2611689
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method

.method public final b(LX/0am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2611680
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    if-eqz v0, :cond_2

    .line 2611681
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2611682
    :cond_0
    :goto_0
    return-void

    .line 2611683
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/p2p/model/PaymentCard;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2611684
    :cond_2
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    .line 2611685
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method

.method public final b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$Theme;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2611676
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2611677
    :goto_0
    return-void

    .line 2611678
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->m:Ljava/util/List;

    .line 2611679
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2611672
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->c:Z

    if-ne v0, p1, :cond_0

    .line 2611673
    :goto_0
    return-void

    .line 2611674
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->c:Z

    .line 2611675
    invoke-static {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q(Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2611671
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2611670
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mRecipientName"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mRecipientUserKey"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mPaymentValue"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mPaymentPin"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "selectedPaymentMethod"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->g:LX/0am;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mSelectedPaymentCard"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mPaymentCards"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->e:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mIsRecipientEligible"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q:LX/03R;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mHasSenderInitiatedPay"

    iget-boolean v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->c:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "mMessengerPayState"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mSenderPin"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mNuxFollowUpAction"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->w:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mThemeList"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->m:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mSelectedTheme"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mShippingOptions"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->h:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mSelectedShippingOption"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->i:LX/0am;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mMailingAddresses"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->j:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mSelectedMailingAddress"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->k:LX/0am;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mPaymentPlatformItem"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mCountry"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->x:Lcom/facebook/common/locale/Country;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mSellerNotes"

    iget-object v2, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2611638
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2611639
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2611640
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2611641
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->d:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2611642
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->g:LX/0am;

    invoke-static {p1, v0, p2}, LX/46R;->a(Landroid/os/Parcel;LX/0am;I)V

    .line 2611643
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    invoke-static {p1, v0, p2}, LX/46R;->a(Landroid/os/Parcel;LX/0am;I)V

    .line 2611644
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2611645
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q:LX/03R;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2611646
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2611647
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2611648
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2611649
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2611650
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2611651
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2611652
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->w:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2611653
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->m:Ljava/util/List;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2611654
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2611655
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->h:Ljava/util/List;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2611656
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->i:LX/0am;

    .line 2611657
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 2611658
    :goto_0
    invoke-static {p1, v1}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2611659
    if-eqz v1, :cond_0

    .line 2611660
    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v1}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2611661
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->j:LX/0Px;

    .line 2611662
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2611663
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->k:LX/0am;

    invoke-static {p1, v0, p2}, LX/46R;->a(Landroid/os/Parcel;LX/0am;I)V

    .line 2611664
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->l:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2611665
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->x:Lcom/facebook/common/locale/Country;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2611666
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2611667
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->z:LX/IoB;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2611668
    return-void

    .line 2611669
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
