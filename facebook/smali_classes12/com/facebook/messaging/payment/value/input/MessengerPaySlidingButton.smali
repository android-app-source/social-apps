.class public Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/ProgressBar;

.field private c:LX/IoL;

.field public d:LX/IoK;

.field public e:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2611860
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2611861
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a()V

    .line 2611862
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2611821
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2611822
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a()V

    .line 2611823
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2611857
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2611858
    invoke-direct {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a()V

    .line 2611859
    return-void
.end method

.method private a(F)Landroid/animation/ObjectAnimator;
    .locals 3

    .prologue
    .line 2611853
    new-instance v0, LX/IoR;

    invoke-direct {v0}, LX/IoR;-><init>()V

    .line 2611854
    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2611855
    new-instance v1, LX/IoI;

    invoke-direct {v1, p0}, LX/IoI;-><init>(Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2611856
    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2611848
    const v0, 0x7f030b02

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2611849
    const v0, 0x7f0d1bd2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a:Landroid/widget/TextView;

    .line 2611850
    const v0, 0x7f0d1bd1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->b:Landroid/widget/ProgressBar;

    .line 2611851
    sget-object v0, LX/IoL;->NORMAL:LX/IoL;

    iput-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->c:LX/IoL;

    .line 2611852
    return-void
.end method

.method private static a(LX/IoL;LX/IoL;)Z
    .locals 2

    .prologue
    .line 2611845
    invoke-virtual {p0}, LX/IoL;->ordinal()I

    move-result v0

    invoke-virtual {p1}, LX/IoL;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2611846
    const/4 v0, 0x1

    .line 2611847
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/IoL;)Landroid/animation/ObjectAnimator;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/16 v0, 0x8

    .line 2611833
    sget-object v1, LX/IoL;->DISABLED:LX/IoL;

    if-ne p1, v1, :cond_0

    .line 2611834
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->setVisibility(I)V

    .line 2611835
    const/4 v0, 0x0

    .line 2611836
    :goto_0
    return-object v0

    .line 2611837
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->c:LX/IoL;

    invoke-static {v1, p1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a(LX/IoL;LX/IoL;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2611838
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->d:LX/IoK;

    iget-object v4, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->e:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2611839
    iget-object v5, v4, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2611840
    invoke-virtual {p1, v2, v3, v4}, LX/IoL;->getButtonText(Landroid/content/res/Resources;LX/IoK;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2611841
    :cond_1
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p1, LX/IoL;->mButtonColorResId:I

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2611842
    iget v1, p1, LX/IoL;->mAlpha:F

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->setAlpha(F)V

    .line 2611843
    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->b:Landroid/widget/ProgressBar;

    iget-boolean v2, p1, LX/IoL;->mShowProgressBar:Z

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    :cond_2
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2611844
    iget v0, p1, LX/IoL;->mLayoutWeight:F

    invoke-direct {p0, v0}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a(F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;)V
    .locals 5

    .prologue
    .line 2611829
    iget-object v0, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->c:LX/IoL;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->d:LX/IoK;

    iget-object v4, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->e:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2611830
    iget-object p0, v4, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v4, p0

    .line 2611831
    invoke-virtual {v1, v2, v3, v4}, LX/IoL;->getButtonText(Landroid/content/res/Resources;LX/IoK;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2611832
    return-void
.end method


# virtual methods
.method public final a(LX/IoL;)Landroid/animation/ObjectAnimator;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2611826
    invoke-direct {p0, p1}, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->b(LX/IoL;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2611827
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->c:LX/IoL;

    .line 2611828
    return-object v0
.end method

.method public setFlowType(LX/IoK;)V
    .locals 0

    .prologue
    .line 2611824
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->d:LX/IoK;

    .line 2611825
    return-void
.end method

.method public setPaymentValue(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)V
    .locals 0

    .prologue
    .line 2611819
    iput-object p1, p0, Lcom/facebook/messaging/payment/value/input/MessengerPaySlidingButton;->e:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2611820
    return-void
.end method
