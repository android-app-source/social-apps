.class public final Lcom/facebook/messaging/payment/sync/connection/PaymentsSyncConnectionHandler$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7G5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7G5",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Im8;


# direct methods
.method public constructor <init>(LX/Im8;)V
    .locals 0

    .prologue
    .line 2608855
    iput-object p1, p0, Lcom/facebook/messaging/payment/sync/connection/PaymentsSyncConnectionHandler$2;->a:LX/Im8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/7Gc;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/7Gc",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2608856
    const-wide/16 v0, -0x1

    .line 2608857
    :try_start_0
    iget-object v3, p0, Lcom/facebook/messaging/payment/sync/connection/PaymentsSyncConnectionHandler$2;->a:LX/Im8;

    iget-object v3, v3, LX/Im8;->d:LX/ImB;

    const/4 v10, 0x0

    .line 2608858
    invoke-static {v3}, LX/ImB;->c(LX/ImB;)Ljava/lang/String;

    move-result-object v5

    .line 2608859
    iget-object v4, v3, LX/ImB;->p:LX/18V;

    invoke-virtual {v4}, LX/18V;->a()LX/2VK;

    move-result-object v6

    .line 2608860
    iget-object v4, v3, LX/ImB;->h:LX/6qC;

    invoke-static {v4, v10}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    const-string v7, "fetchPaymentPin"

    .line 2608861
    iput-object v7, v4, LX/2Vk;->c:Ljava/lang/String;

    .line 2608862
    move-object v4, v4

    .line 2608863
    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    invoke-interface {v6, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 2608864
    iget-object v4, v3, LX/ImB;->k:LX/J0e;

    new-instance v7, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;

    sget-object v8, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->a:LX/DtK;

    const/16 v9, 0x32

    invoke-direct {v7, v8, v9}, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;-><init>(LX/DtK;I)V

    invoke-static {v4, v7}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    const-string v7, "fetchTransactionListMethod"

    .line 2608865
    iput-object v7, v4, LX/2Vk;->c:Ljava/lang/String;

    .line 2608866
    move-object v4, v4

    .line 2608867
    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    invoke-interface {v6, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 2608868
    iget-object v4, v3, LX/ImB;->n:LX/J0b;

    invoke-static {v4, v10}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    const-string v7, "fetchPaymentPlatformContextsMethod"

    .line 2608869
    iput-object v7, v4, LX/2Vk;->c:Ljava/lang/String;

    .line 2608870
    move-object v4, v4

    .line 2608871
    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    invoke-interface {v6, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 2608872
    iget-object v4, v3, LX/ImB;->l:LX/J0T;

    new-instance v7, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;

    sget-object v8, LX/J1G;->INCOMING:LX/J1G;

    invoke-direct {v7, v8}, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;-><init>(LX/J1G;)V

    invoke-static {v4, v7}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v4

    const-string v7, "fetchPaymentRequestsMethod"

    .line 2608873
    iput-object v7, v4, LX/2Vk;->c:Ljava/lang/String;

    .line 2608874
    move-object v4, v4

    .line 2608875
    invoke-virtual {v4}, LX/2Vk;->a()LX/2Vj;

    move-result-object v4

    invoke-interface {v6, v4}, LX/2VK;->a(LX/2Vj;)V

    .line 2608876
    const-string v4, "fetchAllforSync"

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    invoke-interface {v6, v4, v7}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2608877
    const-string v4, "fetchPaymentPin"

    invoke-interface {v6, v4}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2608878
    iget-object v7, v3, LX/ImB;->a:LX/6og;

    invoke-virtual {v7, v4}, LX/6og;->a(Lcom/facebook/payments/auth/pin/model/PaymentPin;)V

    .line 2608879
    const-string v4, "fetchTransactionListMethod"

    invoke-interface {v6, v4}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;

    .line 2608880
    iget-object v7, v4, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;->a:LX/0Px;

    move-object v9, v7

    .line 2608881
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v7, 0x0

    move v8, v7

    :goto_0
    if-ge v8, v10, :cond_0

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2608882
    iget-object p0, v3, LX/ImB;->e:LX/Izl;

    invoke-virtual {p0, v7}, LX/Izl;->b(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2608883
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_0

    .line 2608884
    :cond_0
    const-string v4, "fetchPaymentPlatformContextsMethod"

    invoke-interface {v6, v4}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 2608885
    iget-object v7, v3, LX/ImB;->g:LX/IzG;

    invoke-virtual {v7, v4}, LX/IzG;->a(Ljava/util/ArrayList;)V

    .line 2608886
    iget-object v7, v3, LX/ImB;->o:LX/J0B;

    invoke-virtual {v7}, LX/J0B;->f()V

    .line 2608887
    const-string v4, "fetchPaymentRequestsMethod"

    invoke-interface {v6, v4}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;

    .line 2608888
    iget-object v6, v3, LX/ImB;->f:LX/Izk;

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/Izk;->a(LX/0Px;)V

    .line 2608889
    iget-object v6, v3, LX/ImB;->c:LX/2JS;

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2JS;->a(LX/0Px;)V

    .line 2608890
    invoke-virtual {v3}, LX/ImB;->b()V

    .line 2608891
    move-object v3, v5

    .line 2608892
    const-wide/16 v6, -0x1

    .line 2608893
    if-nez v3, :cond_2

    .line 2608894
    :goto_1
    move-wide v4, v6

    .line 2608895
    move-wide v0, v4
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    .line 2608896
    move-wide v2, v0

    .line 2608897
    :goto_2
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_1

    .line 2608898
    new-instance v0, LX/7Gc;

    const/4 v1, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/7Gc;-><init>(ZLjava/lang/Object;)V

    .line 2608899
    :goto_3
    return-object v0

    :catch_0
    move-wide v2, v0

    goto :goto_2

    :cond_1
    new-instance v0, LX/7Gc;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/7Gc;-><init>(ZLjava/lang/Object;)V

    goto :goto_3

    .line 2608900
    :cond_2
    :try_start_1
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch LX/2Oo; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v6

    goto :goto_1

    :catch_1
    goto :goto_1
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 2608901
    iget-object v0, p0, Lcom/facebook/messaging/payment/sync/connection/PaymentsSyncConnectionHandler$2;->a:LX/Im8;

    iget-object v0, v0, LX/Im8;->h:LX/03V;

    const-string v1, "payment_sync_initial_fetch_error"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to do initial payment fetch, backing off by "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms. viewerContextUserId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/payment/sync/connection/PaymentsSyncConnectionHandler$2;->a:LX/Im8;

    iget-object v3, v3, LX/Im8;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2608902
    return-void
.end method
