.class public Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609172
    new-instance v0, LX/ImE;

    invoke-direct {v0}, LX/ImE;-><init>()V

    sput-object v0, Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2609176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2609177
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2609175
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2609174
    const-class v0, Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;

    invoke-static {v0}, LX/0kk;->toStringHelper(Ljava/lang/Class;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 2609173
    return-void
.end method
