.class public Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/73q;

.field public b:LX/6yo;

.field private c:Lcom/facebook/resources/ui/FbEditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2608490
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;

    invoke-static {p0}, LX/73q;->b(LX/0QB;)LX/73q;

    move-result-object v1

    check-cast v1, LX/73q;

    invoke-static {p0}, LX/6yo;->a(LX/0QB;)LX/6yo;

    move-result-object p0

    check-cast p0, LX/6yo;

    iput-object v1, p1, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;->a:LX/73q;

    iput-object p0, p1, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;->b:LX/6yo;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2608486
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2608487
    const-class v0, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2608488
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2608489
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 2608491
    const v0, 0x7f11002e

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2608492
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2608493
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x287b22f0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2608485
    const v1, 0x7f03122b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x4e4a6ff1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 2608476
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0d3258

    if-ne v0, v1, :cond_0

    .line 2608477
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2608478
    check-cast v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    .line 2608479
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/6yU;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2608480
    invoke-static {}, Lcom/facebook/payments/p2p/model/verification/UserInput;->newBuilder()LX/Duc;

    move-result-object v2

    .line 2608481
    iput-object v1, v2, LX/Duc;->c:Ljava/lang/String;

    .line 2608482
    move-object v1, v2

    .line 2608483
    invoke-virtual {v1}, LX/Duc;->h()Lcom/facebook/payments/p2p/model/verification/UserInput;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->a(Lcom/facebook/payments/p2p/model/verification/UserInput;Ljava/lang/String;)V

    .line 2608484
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2608453
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2608454
    const-string v1, "screen_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608455
    const v1, 0x7f0d2a8c

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

    .line 2608456
    const v2, 0x7f082cbf

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setHeader(I)V

    .line 2608457
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f082cc0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->f()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setSubheader(Ljava/lang/CharSequence;)V

    .line 2608458
    const v1, 0x7f0d2a8f

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2608459
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x2

    invoke-static {v3}, LX/6yU;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {v3}, LX/6yU;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2608460
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2608461
    const v1, 0x7f0d2a8e

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbEditText;

    iput-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    .line 2608462
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;->b:LX/6yo;

    const/16 v2, 0x20

    .line 2608463
    iput-char v2, v1, LX/6yo;->a:C

    .line 2608464
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;->b:LX/6yo;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2608465
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 2608466
    if-eqz v1, :cond_0

    .line 2608467
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;->a:LX/73q;

    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2, v1, v3}, LX/73q;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 2608468
    :cond_0
    const v1, 0x7f0d2a8d

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2608469
    const v2, 0x7f0d0b0e

    invoke-static {p1, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 2608470
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2608471
    const v0, 0x7f021413

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2608472
    invoke-virtual {v2, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2608473
    :goto_0
    return-void

    .line 2608474
    :cond_1
    const v0, 0x7f021411

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2608475
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method
