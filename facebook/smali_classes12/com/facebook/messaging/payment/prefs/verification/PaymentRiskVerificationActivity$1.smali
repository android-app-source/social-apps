.class public final Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wv;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;)V
    .locals 0

    .prologue
    .line 2608226
    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity$1;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2608234
    return-void
.end method

.method public final b()V
    .locals 8

    .prologue
    .line 2608228
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity$1;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;

    iget-object v0, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->q:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity$1;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;

    iget-object v1, v1, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->u:Ljava/lang/String;

    .line 2608229
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2608230
    sget-object v2, Lcom/facebook/payments/p2p/service/model/transactions/CancelPaymentTransactionParams;->a:Ljava/lang/String;

    new-instance v3, Lcom/facebook/payments/p2p/service/model/transactions/CancelPaymentTransactionParams;

    invoke-direct {v3, v1}, Lcom/facebook/payments/p2p/service/model/transactions/CancelPaymentTransactionParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2608231
    iget-object v2, v0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a:LX/0aG;

    const-string v3, "cancel_payment_transaction"

    sget-object v5, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    const v7, 0x2a44b3d

    invoke-static/range {v2 .. v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1MF;->setFireAndForget(Z)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    .line 2608232
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity$1;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;

    invoke-virtual {v0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->finish()V

    .line 2608233
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2608227
    return-void
.end method
