.class public Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:LX/6wv;

.field public final d:LX/6wv;

.field public final e:LX/6wv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2608516
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2608517
    new-instance v0, LX/Ilt;

    invoke-direct {v0, p0}, LX/Ilt;-><init>(Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->c:LX/6wv;

    .line 2608518
    new-instance v0, LX/Ilu;

    invoke-direct {v0, p0}, LX/Ilu;-><init>(Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->d:LX/6wv;

    .line 2608519
    new-instance v0, LX/Ilv;

    invoke-direct {v0, p0}, LX/Ilv;-><init>(Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->e:LX/6wv;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2608520
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2608521
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->a:LX/03V;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2608522
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x5ddacdf2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2608523
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2608524
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2608525
    const-string v2, "screen_data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608526
    if-nez p1, :cond_0

    .line 2608527
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->m()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->l()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2608528
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    const-string v3, "unexpected_exception"

    invoke-virtual {v2, v3}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 2608529
    if-eqz v2, :cond_7

    .line 2608530
    :cond_0
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "unexpected_exception"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 2608531
    if-eqz v0, :cond_1

    .line 2608532
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->c:LX/6wv;

    .line 2608533
    iput-object v2, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2608534
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "verification_failure"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 2608535
    if-eqz v0, :cond_2

    .line 2608536
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->d:LX/6wv;

    .line 2608537
    iput-object v2, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2608538
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "payment_error"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 2608539
    if-eqz v0, :cond_3

    .line 2608540
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->e:LX/6wv;

    .line 2608541
    iput-object v2, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2608542
    :cond_3
    const v0, 0x65734aa4

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2608543
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->m()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->l()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2608544
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v2, "verification_failure"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 2608545
    if-eqz v0, :cond_8

    .line 2608546
    :goto_1
    goto :goto_0

    .line 2608547
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->m()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->l()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2608548
    const/4 p1, 0x1

    .line 2608549
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    const-string v3, "payment_error"

    invoke-virtual {v2, v3}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 2608550
    if-eqz v2, :cond_9

    .line 2608551
    :goto_2
    goto :goto_0

    .line 2608552
    :cond_6
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->a:LX/03V;

    const-string v3, "RiskFailureFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid ScreenData received for FAILURE screen: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2608553
    :cond_7
    const v2, 0x7f082cce

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->g()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f080016

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 p1, 0x1

    invoke-static {v2, v3, v4, v5, p1}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v2

    .line 2608554
    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->c:LX/6wv;

    .line 2608555
    iput-object v3, v2, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2608556
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    const-string v4, "unexpected_exception"

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2608557
    :cond_8
    const v0, 0x7f082cce

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f082ccf

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f080016

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f082cd0

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v0, v2, v3, v4, v5}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v0

    .line 2608558
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->d:LX/6wv;

    .line 2608559
    iput-object v2, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2608560
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    const-string v3, "verification_failure"

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2608561
    :cond_9
    const v2, 0x7f082cd1

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f082cd2

    new-array v4, p1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->g()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f080016

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5, p1}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v2

    .line 2608562
    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;->e:LX/6wv;

    .line 2608563
    iput-object v3, v2, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2608564
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    const-string v4, "payment_error"

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
