.class public Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Lcom/facebook/widget/text/BetterButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2608589
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2608590
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2608591
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2608592
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x577c1037

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2608588
    const v1, 0x7f03122c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4112f3a2

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2608575
    const v0, 0x7f0d2a90

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2608576
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2608577
    const-string v1, "screen_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608578
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2608579
    :goto_1
    const v0, 0x7f0d2a91

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2608580
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;->c:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/Ilw;

    invoke-direct {v1, p0}, LX/Ilw;-><init>(Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2608581
    const v0, 0x7f0d2a93

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;->d:Lcom/facebook/widget/text/BetterButton;

    .line 2608582
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;->d:Lcom/facebook/widget/text/BetterButton;

    new-instance v1, LX/Ilx;

    invoke-direct {v1, p0}, LX/Ilx;-><init>(Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2608583
    return-void

    .line 2608584
    :sswitch_0
    const-string v2, "VERIFICATION_REASON_RISK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "VERIFICATION_REASON_COMPLIANCE_WEEKLY_300"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "VERIFICATION_REASON_COMPLIANCE_LIFETIME_2000"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 2608585
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;->b:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f082cb5

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2608586
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;->b:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f082cb6

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2608587
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;->b:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f082cb7

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3f342639 -> :sswitch_2
        -0x28d4543a -> :sswitch_0
        -0x229330be -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
