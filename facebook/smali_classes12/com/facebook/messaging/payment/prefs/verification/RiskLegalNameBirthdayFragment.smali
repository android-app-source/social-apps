.class public Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/73q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

.field public c:Landroid/widget/LinearLayout;

.field private d:Lcom/facebook/resources/ui/FbEditText;

.field private e:Lcom/facebook/resources/ui/FbEditText;

.field public f:Landroid/widget/DatePicker;

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2608624
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;

    invoke-static {p0}, LX/73q;->b(LX/0QB;)LX/73q;

    move-result-object p0

    check-cast p0, LX/73q;

    iput-object p0, p1, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->a:LX/73q;

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2608679
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2608680
    const-string v1, "screen_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608681
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->b:Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

    const v2, 0x7f082cc2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setHeader(I)V

    .line 2608682
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->b:Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

    const v2, 0x7f082cc3

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setSubheader(I)V

    .line 2608683
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2608684
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2608685
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2608686
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2608687
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->f:Landroid/widget/DatePicker;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/DatePicker;->setVisibility(I)V

    .line 2608688
    iput-boolean v3, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->g:Z

    .line 2608689
    return-void

    .line 2608690
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->a:LX/73q;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1, v2}, LX/73q;->a(Landroid/app/Activity;Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2608675
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2608676
    const-class v0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2608677
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2608678
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 2608672
    const v0, 0x7f11002e

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2608673
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2608674
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5fa7e2f0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2608671
    const v1, 0x7f03122e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0xbb830f7

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 2608632
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x7f0d3258

    if-ne v0, v2, :cond_1

    .line 2608633
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->g:Z

    if-nez v0, :cond_0

    .line 2608634
    const/4 p1, 0x1

    .line 2608635
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2608636
    const-string v2, "screen_data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608637
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->b:Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

    const v3, 0x7f082cc4

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setHeader(I)V

    .line 2608638
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->b:Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

    const v3, 0x7f082cc5

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setSubheader(I)V

    .line 2608639
    if-eqz v0, :cond_2

    .line 2608640
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->f:Landroid/widget/DatePicker;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->h()I

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->i()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->j()I

    move-result v0

    new-instance v5, LX/Ily;

    invoke-direct {v5, p0}, LX/Ily;-><init>(Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;)V

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    .line 2608641
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->c:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2608642
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->f:Landroid/widget/DatePicker;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/DatePicker;->setVisibility(I)V

    .line 2608643
    iput-boolean p1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->g:Z

    .line 2608644
    move v0, v1

    .line 2608645
    :goto_1
    return v0

    .line 2608646
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2608647
    check-cast v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    .line 2608648
    invoke-static {}, Lcom/facebook/payments/p2p/model/verification/UserInput;->newBuilder()LX/Duc;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2608649
    iput-object v3, v2, LX/Duc;->a:Ljava/lang/String;

    .line 2608650
    move-object v2, v2

    .line 2608651
    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2608652
    iput-object v3, v2, LX/Duc;->b:Ljava/lang/String;

    .line 2608653
    move-object v2, v2

    .line 2608654
    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->f:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getYear()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 2608655
    iput-object v3, v2, LX/Duc;->d:Ljava/lang/String;

    .line 2608656
    move-object v2, v2

    .line 2608657
    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->f:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getMonth()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 2608658
    iput-object v3, v2, LX/Duc;->e:Ljava/lang/String;

    .line 2608659
    move-object v2, v2

    .line 2608660
    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->f:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 2608661
    iput-object v3, v2, LX/Duc;->f:Ljava/lang/String;

    .line 2608662
    move-object v2, v2

    .line 2608663
    invoke-virtual {v2}, LX/Duc;->h()Lcom/facebook/payments/p2p/model/verification/UserInput;

    move-result-object v2

    .line 2608664
    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->a(Lcom/facebook/payments/p2p/model/verification/UserInput;Ljava/lang/String;)V

    .line 2608665
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->b()V

    move v0, v1

    .line 2608666
    goto :goto_1

    .line 2608667
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_1

    .line 2608668
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2608669
    const/16 v2, -0x12

    invoke-virtual {v0, p1, v2}, Ljava/util/Calendar;->add(II)V

    .line 2608670
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->f:Landroid/widget/DatePicker;

    invoke-virtual {v0, p1}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v0

    new-instance v5, LX/Ilz;

    invoke-direct {v5, p0}, LX/Ilz;-><init>(Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;)V

    invoke-virtual {v2, v3, v4, v0, v5}, Landroid/widget/DatePicker;->init(IIILandroid/widget/DatePicker$OnDateChangedListener;)V

    goto/16 :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2608625
    const v0, 0x7f0d2a8c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->b:Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

    .line 2608626
    const v0, 0x7f0d2a96

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->c:Landroid/widget/LinearLayout;

    .line 2608627
    const v0, 0x7f0d2a97

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->d:Lcom/facebook/resources/ui/FbEditText;

    .line 2608628
    const v0, 0x7f0d2a98

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->e:Lcom/facebook/resources/ui/FbEditText;

    .line 2608629
    const v0, 0x7f0d2a99

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DatePicker;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->f:Landroid/widget/DatePicker;

    .line 2608630
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;->b()V

    .line 2608631
    return-void
.end method
