.class public Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/67U;


# instance fields
.field public p:LX/67X;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/63L;

.field public t:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field private final w:LX/6wv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2608235
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2608236
    new-instance v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity$1;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity$1;-><init>(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->w:LX/6wv;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;

    invoke-static {v2}, LX/67X;->b(LX/0QB;)LX/67X;

    move-result-object v0

    check-cast v0, LX/67X;

    invoke-static {v2}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    const/16 v3, 0x12cc

    invoke-static {v2, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->p:LX/67X;

    iput-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->q:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    iput-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->r:LX/0Or;

    return-void
.end method

.method private l()V
    .locals 5

    .prologue
    .line 2608275
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->t:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->t:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    .line 2608276
    iget-object v1, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->n:LX/DuZ;

    move-object v0, v1

    .line 2608277
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->t:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    .line 2608278
    iget-object v1, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->n:LX/DuZ;

    move-object v0, v1

    .line 2608279
    iget-boolean v0, v0, LX/DuZ;->isTerminal:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2608280
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2608281
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2608282
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->finish()V

    .line 2608283
    :goto_0
    return-void

    .line 2608284
    :cond_1
    const v0, 0x7f082cd3

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f082cd4

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f082cd5

    invoke-virtual {p0, v2}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f082cd6

    invoke-virtual {p0, v3}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v0

    .line 2608285
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "risk_flow_exit_confirm_dialog_fragment_tag"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2608272
    invoke-static {p0, p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2608273
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->p:LX/67X;

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(LX/0T2;)V

    .line 2608274
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2608267
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 2608268
    instance-of v0, p1, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    if-eqz v0, :cond_0

    .line 2608269
    check-cast p1, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->w:LX/6wv;

    .line 2608270
    iput-object v0, p1, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2608271
    :cond_0
    return-void
.end method

.method public final b()LX/3u1;
    .locals 1

    .prologue
    .line 2608266
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->p:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2608247
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2608248
    const v0, 0x7f03068b

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->setContentView(I)V

    .line 2608249
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->p:LX/67X;

    invoke-virtual {v0}, LX/67X;->g()LX/3u1;

    move-result-object v0

    .line 2608250
    new-instance v1, LX/63L;

    invoke-direct {v1, p0, v0}, LX/63L;-><init>(LX/0ew;LX/3u1;)V

    iput-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->s:LX/63L;

    .line 2608251
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "transaction_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->u:Ljava/lang/String;

    .line 2608252
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "recipient_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->v:Ljava/lang/String;

    .line 2608253
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "payment_risk_verification_controller_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->t:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    .line 2608254
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->t:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    if-nez v0, :cond_0

    .line 2608255
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->u:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->v:Ljava/lang/String;

    .line 2608256
    new-instance v2, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;-><init>()V

    .line 2608257
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2608258
    const-string p1, "transaction_id"

    invoke-virtual {v3, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2608259
    const-string p1, "recipient_id"

    invoke-virtual {v3, p1, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2608260
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2608261
    move-object v0, v2

    .line 2608262
    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->t:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    .line 2608263
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d03c5

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->t:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    const-string v3, "payment_risk_verification_controller_fragment_tag"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2608264
    :cond_0
    const v0, 0x7f082cb3

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->setTitle(I)V

    .line 2608265
    return-void
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 2608245
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->l()V

    .line 2608246
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 2608241
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 2608242
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 2608243
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->s:LX/63L;

    invoke-virtual {v1, p1}, LX/63L;->a(Landroid/view/Menu;)V

    .line 2608244
    return v0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 2608237
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 2608238
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationActivity;->l()V

    .line 2608239
    const/4 v0, 0x1

    .line 2608240
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
