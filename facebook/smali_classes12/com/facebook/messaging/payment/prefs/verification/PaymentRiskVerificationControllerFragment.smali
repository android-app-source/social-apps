.class public Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Duh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Dui;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Or;
    .annotation runtime Lcom/facebook/payments/p2p/config/AreP2pPaymentsRiskNativeEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

.field private m:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/DuZ;

.field public o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

.field public p:Ljava/lang/String;

.field private final q:LX/6wv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2608340
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2608341
    new-instance v0, LX/Ilo;

    invoke-direct {v0, p0}, LX/Ilo;-><init>(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->q:LX/6wv;

    return-void
.end method

.method public static c(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V
    .locals 6

    .prologue
    .line 2608397
    sget-object v0, LX/Ils;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->n:LX/DuZ;

    invoke-virtual {v1}, LX/DuZ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2608398
    invoke-static {p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->k(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V

    .line 2608399
    :cond_0
    :goto_0
    return-void

    .line 2608400
    :pswitch_0
    const-string v1, "risk_introduction_fragment_tag"

    .line 2608401
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608402
    new-instance v2, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/payment/prefs/verification/RiskIntroductionFragment;-><init>()V

    .line 2608403
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2608404
    const-string v4, "screen_data"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2608405
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2608406
    move-object v0, v2

    .line 2608407
    :goto_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    .line 2608408
    invoke-virtual {v2, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->k()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2608409
    :cond_1
    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const v3, 0x7f0d24a7

    invoke-virtual {v2, v3, v0, v1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0

    .line 2608410
    :pswitch_1
    const-string v1, "risk_security_code_fragment_tag"

    .line 2608411
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608412
    new-instance v2, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;-><init>()V

    .line 2608413
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2608414
    const-string v4, "screen_data"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2608415
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2608416
    move-object v0, v2

    .line 2608417
    goto :goto_1

    .line 2608418
    :pswitch_2
    const-string v1, "risk_card_first_six_fragment_tag"

    .line 2608419
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608420
    new-instance v2, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/payment/prefs/verification/RiskCardFirstSixFragment;-><init>()V

    .line 2608421
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2608422
    const-string v4, "screen_data"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2608423
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2608424
    move-object v0, v2

    .line 2608425
    goto :goto_1

    .line 2608426
    :pswitch_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    .line 2608427
    const-string v1, "risk_failure_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2608428
    :goto_2
    goto :goto_0

    .line 2608429
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2608430
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2608431
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2608432
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v0, 0x7f082ccb

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->d:LX/Duh;

    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/Duh;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f082ccc

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    invoke-virtual {v5}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    invoke-virtual {v5}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    invoke-virtual {v5}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->f()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v0, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    const v3, 0x7f080016

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/Ilr;

    invoke-direct {v4, p0}, LX/Ilr;-><init>(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V

    invoke-static {v1, v2, v0, v3, v4}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2608433
    goto/16 :goto_0

    .line 2608434
    :pswitch_5
    const-string v1, "risk_legal_name_birthday_fragment_tag"

    .line 2608435
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608436
    new-instance v2, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/payment/prefs/verification/RiskLegalNameBirthdayFragment;-><init>()V

    .line 2608437
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2608438
    const-string v4, "screen_data"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2608439
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2608440
    move-object v0, v2

    .line 2608441
    goto/16 :goto_1

    .line 2608442
    :pswitch_6
    const-string v1, "risk_last_4_ssn_fragment_tag"

    .line 2608443
    new-instance v0, Lcom/facebook/messaging/payment/prefs/verification/RiskLast4SSNFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/payment/prefs/verification/RiskLast4SSNFragment;-><init>()V

    move-object v0, v0

    .line 2608444
    goto/16 :goto_1

    .line 2608445
    :cond_2
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608446
    new-instance v2, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;

    invoke-direct {v2}, Lcom/facebook/messaging/payment/prefs/verification/RiskFailureFragment;-><init>()V

    .line 2608447
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2608448
    const-string p0, "screen_data"

    invoke-virtual {v3, p0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2608449
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2608450
    move-object v1, v2

    .line 2608451
    const-string v2, "risk_failure_fragment_tag"

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto/16 :goto_2

    .line 2608452
    :cond_3
    const v0, 0x7f082ccd

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static k(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V
    .locals 5

    .prologue
    .line 2608388
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v1, "msite_dialog_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    .line 2608389
    if-nez v0, :cond_0

    .line 2608390
    const v0, 0x7f082cb1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->d:LX/Duh;

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/Duh;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f082cb0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f08001a

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f080017

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v1, v0, v2, v3, v4}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v0

    .line 2608391
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const-string v2, "msite_dialog_fragment_tag"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2608392
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->e:LX/0Zb;

    invoke-static {p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->m(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "p2p_mobile_browser_risk_confirm"

    invoke-static {v2, v3}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2608393
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->q:LX/6wv;

    .line 2608394
    iput-object v1, v0, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2608395
    return-void

    .line 2608396
    :cond_1
    const v0, 0x7f082caf

    goto :goto_0
.end method

.method public static m(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2608387
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->d:LX/Duh;

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Duh;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "p2p_receive"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "p2p_send"

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/p2p/model/verification/UserInput;Ljava/lang/String;)V
    .locals 12
    .param p1    # Lcom/facebook/payments/p2p/model/verification/UserInput;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2608371
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/1v3;->d(Ljava/util/concurrent/Future;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2608372
    :goto_0
    return-void

    .line 2608373
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->p:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2608374
    invoke-static {p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->k(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V

    goto :goto_0

    .line 2608375
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->l:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const-string v2, "show_risk_controller_fragment_tag"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2608376
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->a:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->n:LX/DuZ;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 2608377
    iget-object v4, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v4

    .line 2608378
    move-object v3, p1

    move-object v4, p2

    .line 2608379
    new-instance v6, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;

    move-object v7, v1

    move-object v8, v2

    move-object v9, v3

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/payments/p2p/model/verification/UserInput;Ljava/lang/String;Ljava/lang/String;)V

    .line 2608380
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2608381
    sget-object v8, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentParams;->a:Ljava/lang/String;

    invoke-virtual {v7, v8, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2608382
    const-string v6, "verify_payment"

    invoke-static {v0, v7, v6}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2608383
    new-instance v7, LX/J01;

    invoke-direct {v7, v0}, LX/J01;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    iget-object v8, v0, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->d:Ljava/util/concurrent/Executor;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 2608384
    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2608385
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Ilq;

    invoke-direct {v1, p0}, LX/Ilq;-><init>(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 2608386
    :cond_2
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->n:LX/DuZ;

    invoke-virtual {v2}, LX/DuZ;->name()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4c4dd3ee    # 5.3956536E7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2608348
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2608349
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-static {v0}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    move-result-object v6

    check-cast v6, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    const/16 v7, 0x12cc

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/Duh;->a(LX/0QB;)LX/Duh;

    move-result-object v9

    check-cast v9, LX/Duh;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    new-instance v12, LX/Dui;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v11

    check-cast v11, LX/0SG;

    invoke-direct {v12, v11}, LX/Dui;-><init>(LX/0SG;)V

    move-object v11, v12

    check-cast v11, LX/Dui;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v12

    check-cast v12, Lcom/facebook/content/SecureContextHelper;

    const/16 v13, 0x153f

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v6, v3, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->a:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    iput-object v7, v3, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->b:LX/0Or;

    iput-object v8, v3, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->c:Ljava/util/concurrent/Executor;

    iput-object v9, v3, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->d:LX/Duh;

    iput-object v10, v3, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->e:LX/0Zb;

    iput-object v11, v3, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->f:LX/Dui;

    iput-object v12, v3, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->g:Lcom/facebook/content/SecureContextHelper;

    iput-object v13, v3, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->h:LX/0Or;

    iput-object v0, v3, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->i:LX/0ad;

    .line 2608350
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->e:LX/0Zb;

    invoke-static {p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->m(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "p2p_initiate_risk"

    invoke-static {v2, v3}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2608351
    const v0, 0x7f082cc9

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->l:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    .line 2608352
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2608353
    const-string v2, "transaction_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->j:Ljava/lang/String;

    .line 2608354
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2608355
    const-string v2, "recipient_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->k:Ljava/lang/String;

    .line 2608356
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2608357
    const-string v0, "msite"

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->i:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-char v6, LX/Iie;->c:C

    const-string v7, "msite"

    invoke-interface {v2, v3, v6, v7}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    move v0, v0

    .line 2608358
    if-eqz v0, :cond_1

    .line 2608359
    :cond_0
    invoke-static {p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->k(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V

    .line 2608360
    const/16 v0, 0x2b

    const v2, 0x312eca89

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2608361
    :goto_0
    return-void

    .line 2608362
    :cond_1
    if-eqz p1, :cond_2

    .line 2608363
    const-string v0, "risk_screen"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/DuZ;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->n:LX/DuZ;

    .line 2608364
    const-string v0, "screen_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/verification/ScreenData;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608365
    const-string v0, "fallback_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->p:Ljava/lang/String;

    .line 2608366
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->n:LX/DuZ;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    if-eqz v0, :cond_2

    .line 2608367
    invoke-static {p0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->c(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V

    .line 2608368
    const v0, -0x2f4c4ce7

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_0

    .line 2608369
    :cond_2
    invoke-virtual {p0, v5, v5}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->a(Lcom/facebook/payments/p2p/model/verification/UserInput;Ljava/lang/String;)V

    .line 2608370
    const v0, -0x5f8d5bc7

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5f8ac616    # 1.9999408E19f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2608347
    const v1, 0x7f030f11

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x6dbeb84e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2608342
    const-string v0, "risk_screen"

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->n:LX/DuZ;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2608343
    const-string v0, "screen_data"

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2608344
    const-string v0, "fallback_uri"

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2608345
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2608346
    return-void
.end method
