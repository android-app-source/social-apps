.class public Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/73q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/6zC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6yq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

.field public e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

.field public f:Landroid/view/MenuItem;

.field public g:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2608703
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;

    invoke-static {p0}, LX/73q;->b(LX/0QB;)LX/73q;

    move-result-object v1

    check-cast v1, LX/73q;

    invoke-static {p0}, LX/6zC;->b(LX/0QB;)LX/6zC;

    move-result-object v2

    check-cast v2, LX/6zC;

    invoke-static {p0}, LX/6yq;->a(LX/0QB;)LX/6yq;

    move-result-object p0

    check-cast p0, LX/6yq;

    iput-object v1, p1, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->a:LX/73q;

    iput-object v2, p1, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->b:LX/6zC;

    iput-object p0, p1, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->c:LX/6yq;

    return-void
.end method

.method public static c(Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;)Z
    .locals 2

    .prologue
    .line 2608746
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->b:LX/6zC;

    invoke-static {p0}, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d(Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;)LX/6zD;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6zC;->a(LX/6z8;)Z

    move-result v0

    return v0
.end method

.method public static d(Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;)LX/6zD;
    .locals 3

    .prologue
    .line 2608745
    new-instance v0, LX/6zD;

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v1}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->g:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    invoke-direct {v0, v1, v2}, LX/6zD;-><init>(Ljava/lang/String;Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2608741
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2608742
    const-class v0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2608743
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2608744
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 2608747
    const v0, 0x7f11002e

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2608748
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2608749
    const v0, 0x7f0d3258

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->f:Landroid/view/MenuItem;

    .line 2608750
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x37caf2a4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2608740
    const v1, 0x7f03122f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x6990fc5a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 2608735
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0d3258

    if-ne v0, v1, :cond_0

    .line 2608736
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2608737
    check-cast v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    .line 2608738
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->a(Lcom/facebook/payments/p2p/model/verification/UserInput;Ljava/lang/String;)V

    .line 2608739
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 2608732
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 2608733
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->f:Landroid/view/MenuItem;

    invoke-static {p0}, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->c(Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;)Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2608734
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 2608704
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2608705
    const-string v1, "screen_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608706
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->forValue(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->g:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    .line 2608707
    const v1, 0x7f0d2a8c

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

    .line 2608708
    const v2, 0x7f082cbb

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setHeader(I)V

    .line 2608709
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->g:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    sget-object v4, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-ne v2, v4, :cond_1

    const v2, 0x7f082cbd

    :goto_0
    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->f()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setSubheader(Ljava/lang/CharSequence;)V

    .line 2608710
    const v1, 0x7f0d2a9a

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 2608711
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/verification/ScreenData;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2608712
    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 2608713
    const v0, 0x7f0d2a9b

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/PaymentFormEditTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    .line 2608714
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v8}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->setInputType(I)V

    .line 2608715
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->a:LX/73q;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0, v1, v2}, LX/73q;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 2608716
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v1, "security_code_input_controller_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 2608717
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    if-nez v0, :cond_0

    .line 2608718
    new-instance v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    invoke-direct {v0}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 2608719
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    const-string v2, "security_code_input_controller_fragment_tag"

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2608720
    :cond_0
    new-instance v0, LX/Im0;

    invoke-direct {v0, p0}, LX/Im0;-><init>(Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;)V

    .line 2608721
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->e:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    const v3, 0x7f0d0190

    invoke-virtual {v1, v2, v3}, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a(Lcom/facebook/payments/ui/PaymentFormEditTextView;I)V

    .line 2608722
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->c:LX/6yq;

    .line 2608723
    iput-object v2, v1, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->b:Landroid/text/TextWatcher;

    .line 2608724
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->b:LX/6zC;

    .line 2608725
    iput-object v2, v1, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->c:LX/6wl;

    .line 2608726
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    .line 2608727
    iput-object v0, v1, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->d:Landroid/text/TextWatcher;

    .line 2608728
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;->d:Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;

    new-instance v1, LX/Im1;

    invoke-direct {v1, p0}, LX/Im1;-><init>(Lcom/facebook/messaging/payment/prefs/verification/RiskSecurityCodeFragment;)V

    .line 2608729
    iput-object v1, v0, Lcom/facebook/payments/paymentmethods/cardform/controller/PaymentInputControllerFragment;->a:LX/6vE;

    .line 2608730
    return-void

    .line 2608731
    :cond_1
    const v2, 0x7f082cbc

    goto/16 :goto_0
.end method
