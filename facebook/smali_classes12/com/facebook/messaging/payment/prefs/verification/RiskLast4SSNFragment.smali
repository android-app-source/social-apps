.class public Lcom/facebook/messaging/payment/prefs/verification/RiskLast4SSNFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/73q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/resources/ui/FbEditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2608619
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/payment/prefs/verification/RiskLast4SSNFragment;

    invoke-static {p0}, LX/73q;->b(LX/0QB;)LX/73q;

    move-result-object p0

    check-cast p0, LX/73q;

    iput-object p0, p1, Lcom/facebook/messaging/payment/prefs/verification/RiskLast4SSNFragment;->a:LX/73q;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2608615
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2608616
    const-class v0, Lcom/facebook/messaging/payment/prefs/verification/RiskLast4SSNFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/prefs/verification/RiskLast4SSNFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2608617
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2608618
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 2608612
    const v0, 0x7f11002e

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2608613
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2608614
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x7d7e1317

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2608611
    const v1, 0x7f03122d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x5179e0d6

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 2608602
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0d3258

    if-ne v0, v1, :cond_0

    .line 2608603
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2608604
    check-cast v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    .line 2608605
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLast4SSNFragment;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/6yU;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2608606
    invoke-static {}, Lcom/facebook/payments/p2p/model/verification/UserInput;->newBuilder()LX/Duc;

    move-result-object v2

    .line 2608607
    iput-object v1, v2, LX/Duc;->g:Ljava/lang/String;

    .line 2608608
    move-object v1, v2

    .line 2608609
    invoke-virtual {v1}, LX/Duc;->h()Lcom/facebook/payments/p2p/model/verification/UserInput;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->a(Lcom/facebook/payments/p2p/model/verification/UserInput;Ljava/lang/String;)V

    .line 2608610
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2608593
    const v0, 0x7f0d2a8c

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;

    .line 2608594
    const v1, 0x7f082cc6

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setHeader(I)V

    .line 2608595
    const v1, 0x7f082cc7

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/ui/PaymentHeaderSubheaderLayout;->setSubheader(I)V

    .line 2608596
    const v0, 0x7f0d2a94

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2608597
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x3

    invoke-static {v2}, LX/6yU;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, LX/6yU;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2608598
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2608599
    const v0, 0x7f0d2a95

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLast4SSNFragment;->b:Lcom/facebook/resources/ui/FbEditText;

    .line 2608600
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLast4SSNFragment;->a:LX/73q;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/verification/RiskLast4SSNFragment;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1, v2}, LX/73q;->a(Landroid/app/Activity;Landroid/view/View;)V

    .line 2608601
    return-void
.end method
