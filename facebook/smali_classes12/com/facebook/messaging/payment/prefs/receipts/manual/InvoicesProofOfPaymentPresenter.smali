.class public Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/5fv;

.field public final c:LX/03V;

.field public final d:LX/IkW;

.field public final e:LX/1Ck;

.field public final f:LX/Ikl;

.field public final g:LX/IkZ;

.field public h:LX/IZ9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/ui/media/attachments/MediaResource;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/payments/invoice/protocol/graphql/InvoiceMutationsModels$PaymentInvoiceMutationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/4BY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/lang/String;

.field public final m:Lcom/facebook/payments/currency/CurrencyAmount;

.field public final n:Ljava/lang/String;

.field public final o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2606985
    const-class v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/5fv;LX/03V;LX/IkW;LX/1Ck;LX/Ikl;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;LX/IkZ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p5    # LX/Ikl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/payments/currency/CurrencyAmount;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/IkZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606987
    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->b:LX/5fv;

    .line 2606988
    iput-object p2, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->c:LX/03V;

    .line 2606989
    iput-object p3, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->d:LX/IkW;

    .line 2606990
    iput-object p4, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->e:LX/1Ck;

    .line 2606991
    iput-object p5, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    .line 2606992
    iput-object p6, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->l:Ljava/lang/String;

    .line 2606993
    iput-object p7, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->m:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2606994
    iput-object p8, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->g:LX/IkZ;

    .line 2606995
    iput-object p9, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->n:Ljava/lang/String;

    .line 2606996
    iput-object p10, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->o:Ljava/lang/String;

    .line 2606997
    iput-object p11, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->p:Ljava/lang/String;

    .line 2606998
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->g()V

    .line 2606999
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->h()V

    .line 2607000
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    .line 2607001
    iget-object p1, v0, LX/Ikl;->a:Landroid/widget/ViewSwitcher;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/ViewSwitcher;->setVisibility(I)V

    .line 2607002
    iget-object p1, v0, LX/Ikl;->a:Landroid/widget/ViewSwitcher;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/widget/ViewSwitcher;->setDisplayedChild(I)V

    .line 2607003
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    iget-object p1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->m:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2607004
    iget-object p2, p1, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object p1, p2

    .line 2607005
    iget-object p2, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->b:LX/5fv;

    iget-object p3, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->m:Lcom/facebook/payments/currency/CurrencyAmount;

    sget-object p4, LX/5fu;->NO_CURRENCY_SYMBOL:LX/5fu;

    invoke-virtual {p2, p3, p4}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;LX/5fu;)Ljava/lang/String;

    move-result-object p2

    .line 2607006
    iget-object p3, v0, LX/Ikl;->b:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {p3, p1, p2}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2607007
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    iget-object p1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->o:Ljava/lang/String;

    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    const/16 p4, 0x8

    const/4 p3, 0x0

    .line 2607008
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2607009
    iget-object p2, v0, LX/Ikl;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2607010
    iget-object p2, v0, LX/Ikl;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2607011
    iget-object p3, v0, LX/Ikl;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p3, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2607012
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    iget-object p1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->p:Ljava/lang/String;

    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    const/16 p4, 0x8

    const/4 p3, 0x0

    .line 2607013
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 2607014
    iget-object p2, v0, LX/Ikl;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2607015
    iget-object p2, v0, LX/Ikl;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2607016
    iget-object p3, v0, LX/Ikl;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p3, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2607017
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    iget-object p1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->l:Ljava/lang/String;

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    const/4 p0, 0x0

    .line 2607018
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 2607019
    iget-object p2, v0, LX/Ikl;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2607020
    iget-object p2, v0, LX/Ikl;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2}, Lcom/facebook/widget/text/BetterTextView;->getContext()Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f082d43

    const/4 p4, 0x1

    new-array p4, p4, [Ljava/lang/Object;

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p5

    aput-object p5, p4, p0

    invoke-virtual {p2, p3, p4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 2607021
    iget-object p3, v0, LX/Ikl;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p3, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2607022
    :goto_2
    return-void

    .line 2607023
    :cond_0
    iget-object p2, v0, LX/Ikl;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2607024
    iget-object p2, v0, LX/Ikl;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 2607025
    :cond_1
    iget-object p2, v0, LX/Ikl;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2607026
    iget-object p2, v0, LX/Ikl;->f:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2, p4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_1

    .line 2607027
    :cond_2
    iget-object p2, v0, LX/Ikl;->h:Lcom/facebook/widget/text/BetterTextView;

    const/16 p3, 0x8

    invoke-virtual {p2, p3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_2
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2607028
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    new-instance v1, LX/Ikg;

    invoke-direct {v1, p0}, LX/Ikg;-><init>(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;)V

    .line 2607029
    iget-object p0, v0, LX/Ikl;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2607030
    iget-object p0, v0, LX/Ikl;->j:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2607031
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2607032
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    new-instance v1, LX/Ikh;

    invoke-direct {v1, p0}, LX/Ikh;-><init>(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;)V

    .line 2607033
    iget-object p0, v0, LX/Ikl;->l:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2607034
    return-void
.end method

.method public static k(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;)V
    .locals 1

    .prologue
    .line 2607035
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->k:LX/4BY;

    if-eqz v0, :cond_0

    .line 2607036
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->k:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2607037
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->k:LX/4BY;

    .line 2607038
    :cond_0
    return-void
.end method
