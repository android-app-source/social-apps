.class public Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;
.super Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;
.source ""


# static fields
.field public static final q:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public m:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/IjX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2607703
    const-class v0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;

    sput-object v0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->q:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2607668
    invoke-direct {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;-><init>()V

    .line 2607669
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2607680
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2607681
    const-string v1, "sender_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2607682
    new-instance v1, LX/6dy;

    const v2, 0x7f082c6d

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f082c6f

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v2, 0x7f082c6e

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2607683
    iput-object v0, v1, LX/6dy;->d:Ljava/lang/String;

    .line 2607684
    move-object v0, v1

    .line 2607685
    iput-boolean v4, v0, LX/6dy;->f:Z

    .line 2607686
    move-object v0, v0

    .line 2607687
    invoke-virtual {v0}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v0

    .line 2607688
    iput-object v0, p0, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->m:Lcom/facebook/messaging/dialog/ConfirmActionParams;

    .line 2607689
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 11

    .prologue
    .line 2607690
    invoke-super {p0}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->a()V

    .line 2607691
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->p:LX/0Zb;

    const-string v1, "p2p_decline_payment_confirm"

    const-string v2, "p2p_receive"

    invoke-static {v1, v2}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v1

    .line 2607692
    iget-object v2, v1, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v1, v2

    .line 2607693
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2607694
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->n:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2607695
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2607696
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2607697
    const-string v4, "transaction_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f082ca6

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2607698
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2607699
    sget-object v5, Lcom/facebook/payments/p2p/service/model/transactions/DeclinePaymentParams;->a:Ljava/lang/String;

    new-instance v6, Lcom/facebook/payments/p2p/service/model/transactions/DeclinePaymentParams;

    invoke-direct {v6, v0, v3}, Lcom/facebook/payments/p2p/service/model/transactions/DeclinePaymentParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2607700
    iget-object v5, v1, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a:LX/0aG;

    const-string v6, "decline_payment"

    sget-object v8, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v9

    const v10, -0x6aed97db

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    new-instance v6, LX/4At;

    invoke-direct {v6, v2, v4}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v5, v6}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    move-result-object v5

    move-object v0, v5

    .line 2607701
    new-instance v1, LX/IlM;

    invoke-direct {v1, p0}, LX/IlM;-><init>(Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;)V

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->o:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2607702
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x440d72df

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2607677
    invoke-super {p0, p1}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2607678
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;

    const/16 v4, 0x12cc

    invoke-static {p1, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p1}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0QB;)Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-static {p1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p1

    check-cast p1, LX/0Zb;

    iput-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->m:LX/0Or;

    iput-object v4, p0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->n:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    iput-object v5, p0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->o:Ljava/util/concurrent/Executor;

    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->p:LX/0Zb;

    .line 2607679
    const/16 v1, 0x2b

    const v2, -0x2dcfb935

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2607670
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/dialog/ConfirmActionDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2607671
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->p:LX/0Zb;

    const-string v1, "p2p_decline_payment_initiate"

    const-string v2, "p2p_receive"

    invoke-static {v1, v2}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v2

    .line 2607672
    iget-object p0, v1, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string p1, "parent_activity_name"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2607673
    move-object v1, v1

    .line 2607674
    iget-object v2, v1, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v1, v2

    .line 2607675
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2607676
    return-void
.end method
