.class public Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Ikp;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Landroid/view/ViewGroup;

.field private final c:Landroid/view/ViewGroup;

.field private final d:Lcom/facebook/widget/text/BetterTextView;

.field private final e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final g:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2607578
    const-class v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p2    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607603
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607604
    const v0, 0x7f0311a1

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->b:Landroid/view/ViewGroup;

    .line 2607605
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->b:Landroid/view/ViewGroup;

    const v1, 0x7f0d2960

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->c:Landroid/view/ViewGroup;

    .line 2607606
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->b:Landroid/view/ViewGroup;

    const v1, 0x7f0d2961

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2607607
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->b:Landroid/view/ViewGroup;

    const v1, 0x7f0d2962

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2607608
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->b:Landroid/view/ViewGroup;

    const v1, 0x7f0d2963

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2607609
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->g:Landroid/content/res/Resources;

    .line 2607610
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 2607611
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->b:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public final a(LX/Il0;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2607579
    iget-object v0, p1, LX/Il0;->q:LX/0am;

    move-object v0, v0

    .line 2607580
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2607581
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2607582
    iget-object v0, p1, LX/Il0;->q:LX/0am;

    move-object v0, v0

    .line 2607583
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2607584
    iget-boolean v0, p1, LX/Il0;->r:Z

    move v0, v0

    .line 2607585
    if-nez v0, :cond_0

    .line 2607586
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->d:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->g:Landroid/content/res/Resources;

    const v2, 0x7f0a0809

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2607587
    :goto_0
    iget-object v0, p1, LX/Il0;->j:LX/0am;

    move-object v0, v0

    .line 2607588
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2607589
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2607590
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2607591
    iget-object v0, p1, LX/Il0;->j:LX/0am;

    move-object v0, v0

    .line 2607592
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    sget-object v2, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2607593
    :goto_1
    iget-object v0, p1, LX/Il0;->i:LX/0am;

    move-object v0, v0

    .line 2607594
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2607595
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2607596
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2607597
    iget-object v0, p1, LX/Il0;->i:LX/0am;

    move-object v0, v0

    .line 2607598
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    sget-object v2, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2607599
    :goto_2
    return-void

    .line 2607600
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->d:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->g:Landroid/content/res/Resources;

    const v2, 0x7f0a0038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0

    .line 2607601
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_1

    .line 2607602
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/ui/PaymentStatusWithAttachmentBindable;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_2
.end method
