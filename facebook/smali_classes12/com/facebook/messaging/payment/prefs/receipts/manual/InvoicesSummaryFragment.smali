.class public Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""


# instance fields
.field public a:LX/Ikw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/IZ9;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private c:LX/Ikv;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2607132
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2607133
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2607178
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->c:LX/Ikv;

    if-eqz v0, :cond_0

    .line 2607179
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->c:LX/Ikv;

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->b:LX/IZ9;

    .line 2607180
    iput-object v1, v0, LX/Ikv;->g:LX/IZ9;

    .line 2607181
    :cond_0
    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2607173
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2607174
    const-string v0, "InvoicesSummaryFragment_KEY_TRANSACTION_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->d:Ljava/lang/String;

    .line 2607175
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2607176
    :cond_0
    return-void

    .line 2607177
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2607172
    const v0, 0x7f082d47

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/3u1;)V
    .locals 1

    .prologue
    .line 2607170
    const v0, 0x7f020015

    invoke-virtual {p1, v0}, LX/3u1;->d(I)V

    .line 2607171
    return-void
.end method

.method public final a(LX/IZ9;)V
    .locals 0

    .prologue
    .line 2607167
    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->b:LX/IZ9;

    .line 2607168
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->b()V

    .line 2607169
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2607164
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2607165
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;

    const-class v0, LX/Ikw;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/Ikw;

    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->a:LX/Ikw;

    .line 2607166
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 2607162
    check-cast p1, Landroid/os/Bundle;

    invoke-direct {p0, p1}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->b(Landroid/os/Bundle;)V

    .line 2607163
    return-void
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 2607160
    const v0, 0x7f0e0586

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTheme(I)V

    .line 2607161
    return-void
.end method

.method public final c(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 2607158
    const v0, 0x7f040034

    const v1, 0x7f0400c6

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 2607159
    return-void
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 2607156
    const v0, 0x7f0400c5

    const v1, 0x7f040036

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 2607157
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x15160dd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2607155
    const v1, 0x7f0302ec

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x6afe4888

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7deda15

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2607149
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onDestroy()V

    .line 2607150
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->c:LX/Ikv;

    if-eqz v1, :cond_0

    .line 2607151
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->c:LX/Ikv;

    .line 2607152
    iget-object v2, v1, LX/Ikv;->a:LX/IkY;

    .line 2607153
    iget-object v1, v2, LX/IkY;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2607154
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x1e4767b9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2607134
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2607135
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2607136
    invoke-direct {p0, v0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->b(Landroid/os/Bundle;)V

    .line 2607137
    const v0, 0x7f0d0a19

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 2607138
    const v1, 0x7f0d0a0d

    invoke-static {p1, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ViewSwitcher;

    .line 2607139
    new-instance v2, LX/Ikx;

    new-instance v3, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p2

    invoke-direct {v3, p2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-direct {v2, v0, v1, v3}, LX/Ikx;-><init>(Landroid/support/v7/widget/RecyclerView;Landroid/widget/ViewSwitcher;LX/1OR;)V

    move-object v0, v2

    .line 2607140
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->a:LX/Ikw;

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->d:Ljava/lang/String;

    .line 2607141
    new-instance v3, LX/Ikv;

    .line 2607142
    new-instance v7, LX/IkY;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v1}, LX/Il6;->b(LX/0QB;)LX/Il6;

    move-result-object v6

    check-cast v6, LX/Il6;

    invoke-direct {v7, v4, v5, v6}, LX/IkY;-><init>(LX/0tX;LX/1Ck;LX/Il6;)V

    .line 2607143
    move-object v4, v7

    .line 2607144
    check-cast v4, LX/IkY;

    const-class v5, LX/Iko;

    invoke-interface {v1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Iko;

    invoke-static {v1}, LX/IkW;->b(LX/0QB;)LX/IkW;

    move-result-object v8

    check-cast v8, LX/IkW;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    move-object v6, v0

    move-object v7, v2

    invoke-direct/range {v3 .. v9}, LX/Ikv;-><init>(LX/IkY;LX/Iko;LX/Ikx;Ljava/lang/String;LX/IkW;Ljava/util/concurrent/ExecutorService;)V

    .line 2607145
    move-object v0, v3

    .line 2607146
    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->c:LX/Ikv;

    .line 2607147
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesSummaryFragment;->b()V

    .line 2607148
    return-void
.end method
