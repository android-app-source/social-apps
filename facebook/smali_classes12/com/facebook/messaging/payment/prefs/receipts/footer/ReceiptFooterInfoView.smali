.class public Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Lcom/facebook/widget/text/BetterTextView;

.field private final e:Lcom/facebook/widget/text/BetterTextView;

.field private f:LX/IkJ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2606566
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2606567
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606568
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2606569
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606570
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2606571
    const-class v0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2606572
    const v0, 0x7f031198

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2606573
    const v0, 0x7f0d2950

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2606574
    const v0, 0x7f0d2952

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2606575
    const v0, 0x7f0d2953

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2606576
    const v0, 0x7f0d2954

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2606577
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 2606578
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082c50

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->f:LX/IkJ;

    iget-object v5, v5, LX/IkJ;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2606579
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->b()V

    .line 2606580
    return-void
.end method

.method private a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2606581
    new-instance v0, LX/IkH;

    invoke-direct {v0, p0, p2}, LX/IkH;-><init>(Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2606582
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->a:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2606583
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->f:LX/IkJ;

    iget-object v0, v0, LX/IkJ;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2606584
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid number of links provides "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->f:LX/IkJ;

    iget-object v2, v2, LX/IkJ;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2606585
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2606586
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2606587
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2606588
    :goto_0
    return-void

    .line 2606589
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->f:LX/IkJ;

    iget-object v0, v0, LX/IkJ;->b:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 2606590
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2606591
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v0, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 2606592
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2606593
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2606594
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 2606595
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->f:LX/IkJ;

    iget-object v0, v0, LX/IkJ;->b:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 2606596
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2606597
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v0, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 2606598
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    const-string v1, "\u00b7"

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2606599
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->f:LX/IkJ;

    iget-object v0, v0, LX/IkJ;->b:LX/0Px;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 2606600
    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, v0, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2606601
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v0, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V

    .line 2606602
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2606603
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2606604
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public setViewParams(LX/IkJ;)V
    .locals 0

    .prologue
    .line 2606605
    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->f:LX/IkJ;

    .line 2606606
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/footer/ReceiptFooterInfoView;->a()V

    .line 2606607
    return-void
.end method
