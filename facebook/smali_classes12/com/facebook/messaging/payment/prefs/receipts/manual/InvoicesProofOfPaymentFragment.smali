.class public Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;
.super Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/Ikk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ikf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/6xb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

.field public f:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

.field public g:LX/Ike;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;

.field private m:Lcom/facebook/payments/currency/CurrencyAmount;

.field public n:Z

.field private o:LX/IZ9;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final p:LX/IkZ;

.field private final q:LX/Ika;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2606889
    invoke-direct {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;-><init>()V

    .line 2606890
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->n:Z

    .line 2606891
    new-instance v0, LX/IkZ;

    invoke-direct {v0, p0}, LX/IkZ;-><init>(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->p:LX/IkZ;

    .line 2606892
    new-instance v0, LX/Ika;

    invoke-direct {v0, p0}, LX/Ika;-><init>(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->q:LX/Ika;

    .line 2606893
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    const-class v1, LX/Ikk;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Ikk;

    const-class v2, LX/Ikf;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Ikf;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object p0

    check-cast p0, LX/6xb;

    iput-object v1, p1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->a:LX/Ikk;

    iput-object v2, p1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->b:LX/Ikf;

    iput-object v3, p1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->d:LX/6xb;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2606894
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->d:LX/6xb;

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    sget-object v2, LX/6xZ;->PROOF_OF_PAYMENT:LX/6xZ;

    invoke-virtual {v0, v1, v2, p1}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 2606895
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2606896
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->f:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    if-eqz v0, :cond_0

    .line 2606897
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->f:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->o:LX/IZ9;

    .line 2606898
    iput-object v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->h:LX/IZ9;

    .line 2606899
    :cond_0
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 2606909
    const-string v0, "payflows_back_click"

    invoke-static {p0, v0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->a$redex0(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;Ljava/lang/String;)V

    .line 2606910
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2606900
    const v0, 0x7f082d40

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/3u1;)V
    .locals 1

    .prologue
    .line 2606901
    const v0, 0x7f020015

    invoke-virtual {p1, v0}, LX/3u1;->d(I)V

    .line 2606902
    return-void
.end method

.method public final a(LX/IZ9;)V
    .locals 1

    .prologue
    .line 2606903
    new-instance v0, LX/Ikc;

    invoke-direct {v0, p0, p1}, LX/Ikc;-><init>(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;LX/IZ9;)V

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->o:LX/IZ9;

    .line 2606904
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->b()V

    .line 2606905
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 2606906
    const-string v0, "payflows_cancel"

    invoke-static {p0, v0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->a$redex0(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;Ljava/lang/String;)V

    .line 2606907
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/app/Activity;)V

    .line 2606908
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2606868
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->a(Landroid/os/Bundle;)V

    .line 2606869
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2606870
    const-class v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2606871
    new-instance v0, LX/8FZ;

    invoke-direct {v0}, LX/8FZ;-><init>()V

    .line 2606872
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2606873
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 2606874
    check-cast p1, Landroid/os/Bundle;

    .line 2606875
    const-string v0, "InvoicesProofOfPaymentFragment_KEY_TRANSACTION_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->h:Ljava/lang/String;

    .line 2606876
    const-string v0, "InvoicesProofOfPaymentFragment_KEY_TOTAL_AMOUNT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->m:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2606877
    const-string v0, "InvoicesProofOfPaymentFragment_KEY_MANUAL_TRANSFER_METHOD"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->l:Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;

    .line 2606878
    const-string v0, "payments_logging_session_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    .line 2606879
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->l:Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->i:Ljava/lang/String;

    .line 2606880
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->l:Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;

    .line 2606881
    iget-object p1, v0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->d:Ljava/lang/String;

    move-object v0, p1

    .line 2606882
    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->j:Ljava/lang/String;

    .line 2606883
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->l:Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;

    .line 2606884
    iget-object p1, v0, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->e:Ljava/lang/String;

    move-object v0, p1

    .line 2606885
    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->k:Ljava/lang/String;

    .line 2606886
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2606887
    return-void

    .line 2606888
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 2606817
    const v0, 0x7f0e0586

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTheme(I)V

    .line 2606818
    return-void
.end method

.method public final c(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 2606819
    const v0, 0x7f040034

    const v1, 0x7f0400c6

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 2606820
    return-void
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 2606821
    const v0, 0x7f0400c5

    const v1, 0x7f040036

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 2606822
    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2606823
    instance-of v0, p1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    if-eqz v0, :cond_0

    .line 2606824
    check-cast p1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->q:LX/Ika;

    .line 2606825
    iput-object v0, p1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->G:LX/Ika;

    .line 2606826
    :cond_0
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 2606827
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2606828
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->n:Z

    if-eqz v0, :cond_0

    .line 2606829
    const v0, 0x7f11002a

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2606830
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x552618f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2606831
    const v1, 0x7f0302eb

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x5c5e662f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x63b74373

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2606832
    invoke-super {p0}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onDestroy()V

    .line 2606833
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->f:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    .line 2606834
    iget-object v2, v1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->e:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2606835
    const/4 v2, 0x0

    iput-object v2, v1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2606836
    const/16 v1, 0x2b

    const v2, 0x424d0173

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 2606837
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0d3252

    if-ne v0, v1, :cond_0

    .line 2606838
    const-string v0, "payflows_done_click"

    invoke-static {p0, v0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->a$redex0(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;Ljava/lang/String;)V

    .line 2606839
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->f:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    .line 2606840
    iget-object v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    const-string v2, "Cannot perform media upload with null MediaResource"

    invoke-static {v1, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2606841
    iget-object v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->k:LX/4BY;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->k:LX/4BY;

    invoke-virtual {v1}, LX/4BY;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2606842
    :goto_0
    iget-object v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->d:LX/IkW;

    iget-object v2, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v3, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->n:Ljava/lang/String;

    iget-object v4, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->l:Ljava/lang/String;

    .line 2606843
    iget-object p0, v1, LX/IkW;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {p0, v2}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    .line 2606844
    new-instance p1, LX/IkU;

    invoke-direct {p1, v1, v4, v3}, LX/IkU;-><init>(LX/IkW;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, p1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    move-object v1, p0

    .line 2606845
    iput-object v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2606846
    iget-object v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->e:LX/1Ck;

    sget-object v2, LX/Ikj;->UPLOAD_RECEIPT:LX/Ikj;

    iget-object v3, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->j:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v4, LX/Iki;

    invoke-direct {v4, v0}, LX/Iki;-><init>(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2606847
    const/4 v0, 0x1

    .line 2606848
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 2606849
    :cond_1
    new-instance v1, LX/4BY;

    iget-object v2, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    invoke-virtual {v2}, LX/Ikl;->a()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/4BY;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->k:LX/4BY;

    .line 2606850
    iget-object v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->k:LX/4BY;

    iget-object v2, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->f:LX/Ikl;

    invoke-virtual {v2}, LX/Ikl;->a()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f082d4e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2606851
    iget-object v1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->k:LX/4BY;

    invoke-virtual {v1}, LX/4BY;->show()V

    goto :goto_0
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 2606852
    invoke-super {p0, p1}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 2606853
    const v0, 0x7f0d3252

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 2606854
    if-eqz v0, :cond_0

    .line 2606855
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->f:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    .line 2606856
    iget-object p0, v1, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v1, p0

    .line 2606857
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2606858
    :cond_0
    return-void

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606859
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/business/common/activity/BusinessActivityFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2606860
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->d:LX/6xb;

    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->e:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    sget-object v2, LX/6xg;->NMOR_PAGES_COMMERCE:LX/6xg;

    sget-object v3, LX/6xZ;->PROOF_OF_PAYMENT:LX/6xZ;

    invoke-virtual {v0, v1, v2, v3, p2}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xg;LX/6xZ;Landroid/os/Bundle;)V

    .line 2606861
    new-instance v1, LX/Ikl;

    check-cast p1, Landroid/view/ViewGroup;

    invoke-direct {v1, p1}, LX/Ikl;-><init>(Landroid/view/ViewGroup;)V

    .line 2606862
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->a:LX/Ikk;

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->m:Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v4, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->p:LX/IkZ;

    iget-object v5, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->i:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->j:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->k:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, LX/Ikk;->a(LX/Ikl;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;LX/IkZ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->f:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    .line 2606863
    new-instance v0, LX/Ike;

    invoke-direct {v0, p0}, LX/Ike;-><init>(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;)V

    .line 2606864
    move-object v0, v0

    .line 2606865
    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->g:LX/Ike;

    .line 2606866
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->b()V

    .line 2606867
    return-void
.end method
