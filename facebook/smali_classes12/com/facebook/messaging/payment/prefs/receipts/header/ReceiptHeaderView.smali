.class public Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsPartialAccount;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/IkS;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2606718
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2606719
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606739
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2606740
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606744
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2606745
    const-class v0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2606746
    const v0, 0x7f03119a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2606747
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2606741
    const v0, 0x7f0d2957

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2606742
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->b:LX/IkS;

    iget-object v1, v1, LX/IkS;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2606743
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;

    const/16 v1, 0x1453

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->a:LX/0Or;

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2606736
    const v0, 0x7f0d2958

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2606737
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->b:LX/IkS;

    iget-object v1, v1, LX/IkS;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2606738
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 2606731
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2606732
    const v0, 0x7f0d2956

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    .line 2606733
    new-instance v2, Lcom/facebook/user/model/UserKey;

    sget-object v3, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v4, p0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->b:LX/IkS;

    iget-object v4, v4, LX/IkS;->a:LX/DtN;

    invoke-interface {v4}, LX/DtN;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->b:LX/IkS;

    iget-object v3, v3, LX/IkS;->a:LX/DtN;

    invoke-interface {v3}, LX/DtN;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v1, LX/8ue;->MESSENGER:LX/8ue;

    :goto_0
    invoke-static {v2, v1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2606734
    return-void

    .line 2606735
    :cond_0
    if-eqz v1, :cond_1

    sget-object v1, LX/8ue;->NONE:LX/8ue;

    goto :goto_0

    :cond_1
    sget-object v1, LX/8ue;->FACEBOOK:LX/8ue;

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2606726
    const v0, 0x7f0d2959

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2606727
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->b:LX/IkS;

    iget-boolean v1, v1, LX/IkS;->d:Z

    if-eqz v1, :cond_0

    .line 2606728
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2606729
    :goto_0
    return-void

    .line 2606730
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public setReceiptHeaderViewParams(LX/IkS;)V
    .locals 0

    .prologue
    .line 2606720
    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->b:LX/IkS;

    .line 2606721
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->a()V

    .line 2606722
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->b()V

    .line 2606723
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->c()V

    .line 2606724
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/receipts/header/ReceiptHeaderView;->d()V

    .line 2606725
    return-void
.end method
