.class public Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryLoaderResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryLoaderResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentTransaction;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2607853
    new-instance v0, LX/IlY;

    invoke-direct {v0}, LX/IlY;-><init>()V

    sput-object v0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryLoaderResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2607854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607855
    const-class v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryLoaderResult;->a:LX/0Px;

    .line 2607856
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryLoaderResult;->b:LX/0Px;

    .line 2607857
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryLoaderResult;->c:Z

    .line 2607858
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2607859
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2607860
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryLoaderResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2607861
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryLoaderResult;->b:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2607862
    iget-boolean v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryLoaderResult;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2607863
    return-void
.end method
