.class public Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/5fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/IlU;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2608188
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2608189
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2608159
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2608160
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2608184
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2608185
    const-class v0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2608186
    const v0, 0x7f03132c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2608187
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2608181
    const v0, 0x7f0d037b

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    .line 2608182
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->b:LX/IlU;

    iget-object v1, v1, LX/IlU;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setText(Ljava/lang/CharSequence;)V

    .line 2608183
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;

    invoke-static {v0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v0

    check-cast v0, LX/5fv;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->a:LX/5fv;

    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    .line 2608175
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->a:LX/5fv;

    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->b:LX/IlU;

    iget-object v2, v2, LX/IlU;->c:Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/Amount;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->b:LX/IlU;

    iget-object v3, v3, LX/IlU;->c:Lcom/facebook/payments/p2p/model/Amount;

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/Amount;->d()I

    move-result v3

    int-to-long v4, v3

    invoke-direct {v1, v2, v4, v5}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v0

    .line 2608176
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->b:LX/IlU;

    iget-boolean v1, v1, LX/IlU;->e:Z

    if-eqz v1, :cond_0

    const-string v1, "- %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2608177
    :goto_0
    const v0, 0x7f0d2494

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2608178
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2608179
    return-void

    :cond_0
    move-object v1, v0

    .line 2608180
    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2608166
    const v0, 0x7f0d2125

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;

    .line 2608167
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->b:LX/IlU;

    iget-object v1, v1, LX/IlU;->d:LX/Ila;

    .line 2608168
    iget-object v2, v1, LX/Ila;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2608169
    iget-object v2, v1, LX/Ila;->b:LX/Ilc;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->setMessengerPayHistoryStatusState(LX/Ilc;)V

    .line 2608170
    iget-object v2, v1, LX/Ila;->c:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2608171
    iget-object v1, v1, LX/Ila;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2608172
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->setVisibility(I)V

    .line 2608173
    :goto_0
    return-void

    .line 2608174
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public setMessengerPayHistoryItemViewCommonParams(LX/IlU;)V
    .locals 0

    .prologue
    .line 2608161
    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->b:LX/IlU;

    .line 2608162
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->a()V

    .line 2608163
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->b()V

    .line 2608164
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->c()V

    .line 2608165
    return-void
.end method
