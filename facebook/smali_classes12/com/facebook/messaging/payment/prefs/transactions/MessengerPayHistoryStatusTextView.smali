.class public Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[I

.field private static final c:[I

.field private static final d:[I


# instance fields
.field private e:LX/Ilc;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2607865
    const-class v0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;

    sput-object v0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->a:Ljava/lang/Class;

    .line 2607866
    new-array v0, v3, [I

    const v1, 0x7f0107aa

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->b:[I

    .line 2607867
    new-array v0, v3, [I

    const v1, 0x7f0107ab

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->c:[I

    .line 2607868
    new-array v0, v3, [I

    const v1, 0x7f0107ac

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->d:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2607886
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2607887
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2607884
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2607885
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2607882
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2607883
    return-void
.end method


# virtual methods
.method public final onCreateDrawableState(I)[I
    .locals 6

    .prologue
    .line 2607873
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->e:LX/Ilc;

    if-nez v0, :cond_0

    .line 2607874
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 2607875
    :goto_0
    return-object v0

    .line 2607876
    :cond_0
    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 2607877
    sget-object v1, LX/IlZ;->a:[I

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->e:LX/Ilc;

    invoke-virtual {v2}, LX/Ilc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2607878
    sget-object v1, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->a:Ljava/lang/Class;

    const-string v2, "Unknown MessengerPayHistoryStatusViewState %s found"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->e:LX/Ilc;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2607879
    :pswitch_0
    sget-object v1, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->b:[I

    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 2607880
    :pswitch_1
    sget-object v1, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->c:[I

    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 2607881
    :pswitch_2
    sget-object v1, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->d:[I

    invoke-static {v0, v1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setMessengerPayHistoryStatusState(LX/Ilc;)V
    .locals 1

    .prologue
    .line 2607869
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->e:LX/Ilc;

    if-eq v0, p1, :cond_0

    .line 2607870
    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->e:LX/Ilc;

    .line 2607871
    invoke-virtual {p0}, Lcom/facebook/messaging/payment/prefs/transactions/MessengerPayHistoryStatusTextView;->refreshDrawableState()V

    .line 2607872
    :cond_0
    return-void
.end method
