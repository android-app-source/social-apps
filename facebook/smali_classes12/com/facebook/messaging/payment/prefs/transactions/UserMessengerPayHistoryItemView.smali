.class public Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/IlT;


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsPartialAccount;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/Ilm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2608206
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2608207
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2608204
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2608205
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2608200
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2608201
    const-class v0, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2608202
    const v0, 0x7f03155c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2608203
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2608208
    const v0, 0x7f0d2707

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;

    .line 2608209
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->b:LX/Ilm;

    .line 2608210
    iget-object p0, v1, LX/Ilm;->b:LX/IlU;

    move-object v1, p0

    .line 2608211
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->setMessengerPayHistoryItemViewCommonParams(LX/IlU;)V

    .line 2608212
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;

    const/16 v1, 0x1453

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->a:LX/0Or;

    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    .line 2608190
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2608191
    const-string v0, "499725321"

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->b:LX/Ilm;

    iget-object v2, v2, LX/Ilm;->a:LX/DtN;

    invoke-interface {v2}, LX/DtN;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 2608192
    const v0, 0x7f0d300f

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    .line 2608193
    new-instance v3, Lcom/facebook/user/model/UserKey;

    sget-object v4, LX/0XG;->FACEBOOK:LX/0XG;

    iget-object v5, p0, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->b:LX/Ilm;

    iget-object v5, v5, LX/Ilm;->a:LX/DtN;

    invoke-interface {v5}, LX/DtN;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->b:LX/Ilm;

    iget-object v4, v4, LX/Ilm;->a:LX/DtN;

    invoke-interface {v4}, LX/DtN;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v1, LX/8ue;->MESSENGER:LX/8ue;

    :goto_0
    invoke-static {v3, v1}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;LX/8ue;)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2608194
    return-void

    .line 2608195
    :cond_0
    if-nez v1, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    sget-object v1, LX/8ue;->NONE:LX/8ue;

    goto :goto_0

    :cond_2
    sget-object v1, LX/8ue;->FACEBOOK:LX/8ue;

    goto :goto_0
.end method


# virtual methods
.method public setMessengerPayHistoryItemViewParams(LX/IlW;)V
    .locals 0

    .prologue
    .line 2608196
    check-cast p1, LX/Ilm;

    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->b:LX/Ilm;

    .line 2608197
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->b()V

    .line 2608198
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;->a()V

    .line 2608199
    return-void
.end method
