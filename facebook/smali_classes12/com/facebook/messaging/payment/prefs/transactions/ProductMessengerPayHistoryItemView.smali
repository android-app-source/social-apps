.class public Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/IlT;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/Ilk;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2608140
    const-class v0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2608138
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2608139
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2608136
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2608137
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2608111
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2608112
    const-class v0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2608113
    const v0, 0x7f031040

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2608114
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2608131
    const v0, 0x7f0d2707

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;

    .line 2608132
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->c:LX/Ilk;

    .line 2608133
    iget-object p0, v1, LX/Ilk;->c:LX/IlU;

    move-object v1, p0

    .line 2608134
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/prefs/transactions/SimpleMessengerPayHistoryItemView;->setMessengerPayHistoryItemViewCommonParams(LX/IlU;)V

    .line 2608135
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->a:Landroid/content/res/Resources;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2608119
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->a:Landroid/content/res/Resources;

    invoke-static {v0}, LX/1Uo;->a(Landroid/content/res/Resources;)LX/1Uo;

    move-result-object v0

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v1

    .line 2608120
    iput-object v1, v0, LX/1Uo;->u:LX/4Ab;

    .line 2608121
    move-object v1, v0

    .line 2608122
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->c:LX/Ilk;

    iget v0, v0, LX/Ilk;->b:I

    if-eqz v0, :cond_0

    .line 2608123
    iget-object v0, p0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->a:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->c:LX/Ilk;

    iget v2, v2, LX/Ilk;->b:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2608124
    iput-object v0, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2608125
    :cond_0
    const v0, 0x7f0d2700

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2608126
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2608127
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->c:LX/Ilk;

    iget-object v1, v1, LX/Ilk;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2608128
    iget-object v1, p0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->c:LX/Ilk;

    iget-object v1, v1, LX/Ilk;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2608129
    sget-object v2, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2608130
    :cond_1
    return-void
.end method


# virtual methods
.method public setMessengerPayHistoryItemViewParams(LX/IlW;)V
    .locals 0

    .prologue
    .line 2608115
    check-cast p1, LX/Ilk;

    iput-object p1, p0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->c:LX/Ilk;

    .line 2608116
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->a()V

    .line 2608117
    invoke-direct {p0}, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;->b()V

    .line 2608118
    return-void
.end method
