.class public Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2610103
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2610104
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->a()V

    .line 2610105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610100
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610101
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->a()V

    .line 2610102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610097
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610098
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->a()V

    .line 2610099
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2610080
    const-class v0, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2610081
    const v0, 0x7f030d74

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2610082
    const v0, 0x7f0d1648

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2610083
    const v0, 0x7f0d212a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2610084
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    return-void
.end method


# virtual methods
.method public setDrawableText(I)V
    .locals 1

    .prologue
    .line 2610095
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2610096
    return-void
.end method

.method public setDrawableText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2610093
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610094
    return-void
.end method

.method public setDrawableTextVisibility(I)V
    .locals 1

    .prologue
    .line 2610091
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2610092
    return-void
.end method

.method public setMessageText(I)V
    .locals 1

    .prologue
    .line 2610089
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2610090
    return-void
.end method

.method public setMessageText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2610087
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610088
    return-void
.end method

.method public setMessageTextVisibility(I)V
    .locals 1

    .prologue
    .line 2610085
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleSupplementaryView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2610086
    return-void
.end method
