.class public Lcom/facebook/messaging/payment/thread/PaymentBubbleThemeView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2610116
    const-class v0, Lcom/facebook/messaging/payment/thread/PaymentBubbleThemeView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/payment/thread/PaymentBubbleThemeView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2610113
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2610114
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleThemeView;->a()V

    .line 2610115
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2610110
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610111
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleThemeView;->a()V

    .line 2610112
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2610117
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610118
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleThemeView;->a()V

    .line 2610119
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2610106
    const-class v0, Lcom/facebook/messaging/payment/thread/PaymentBubbleThemeView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleThemeView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2610107
    const v0, 0x7f030d76

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2610108
    const v0, 0x7f0d212c

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleThemeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleThemeView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2610109
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    return-void
.end method
