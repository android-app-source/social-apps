.class public Lcom/facebook/messaging/payment/thread/PaymentBubbleRecipientNameView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2610065
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2610066
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleRecipientNameView;->a()V

    .line 2610067
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610068
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610069
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleRecipientNameView;->a()V

    .line 2610070
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610071
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610072
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleRecipientNameView;->a()V

    .line 2610073
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2610074
    const-class v0, Lcom/facebook/messaging/payment/thread/PaymentBubbleRecipientNameView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleRecipientNameView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2610075
    const v0, 0x7f030d72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2610076
    const v0, 0x7f0d2129

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleRecipientNameView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2610077
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    return-void
.end method


# virtual methods
.method public setRecipientNameText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2610078
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleRecipientNameView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610079
    return-void
.end method
