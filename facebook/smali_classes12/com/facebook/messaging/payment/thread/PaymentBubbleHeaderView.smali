.class public Lcom/facebook/messaging/payment/thread/PaymentBubbleHeaderView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2610050
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2610051
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleHeaderView;->a()V

    .line 2610052
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2610062
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610063
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleHeaderView;->a()V

    .line 2610064
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2610055
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610056
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleHeaderView;->a()V

    .line 2610057
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2610058
    const-class v0, Lcom/facebook/messaging/payment/thread/PaymentBubbleHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2610059
    const v0, 0x7f030d6f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2610060
    const v0, 0x7f0d2128

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleHeaderView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2610061
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    return-void
.end method


# virtual methods
.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2610053
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleHeaderView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610054
    return-void
.end method
