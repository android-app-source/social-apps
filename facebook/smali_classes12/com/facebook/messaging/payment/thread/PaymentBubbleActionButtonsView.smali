.class public Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2610025
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2610026
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->a()V

    .line 2610027
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610022
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610023
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->a()V

    .line 2610024
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610003
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610004
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->a()V

    .line 2610005
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2610017
    const-class v0, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2610018
    const v0, 0x7f030d6a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2610019
    const v0, 0x7f0d2122

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2610020
    const v0, 0x7f0d2121

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2610021
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    return-void
.end method


# virtual methods
.method public setListener(LX/Img;)V
    .locals 2

    .prologue
    .line 2610014
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->a:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/Ime;

    invoke-direct {v1, p0, p1}, LX/Ime;-><init>(Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;LX/Img;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610015
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->b:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/Imf;

    invoke-direct {v1, p0, p1}, LX/Imf;-><init>(Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;LX/Img;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610016
    return-void
.end method

.method public setPrimaryActionText(I)V
    .locals 1

    .prologue
    .line 2610012
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2610013
    return-void
.end method

.method public setPrimaryActionVisibility(I)V
    .locals 1

    .prologue
    .line 2610010
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2610011
    return-void
.end method

.method public setSecondaryActionText(I)V
    .locals 1

    .prologue
    .line 2610008
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2610009
    return-void
.end method

.method public setSecondaryActionVisibility(I)V
    .locals 1

    .prologue
    .line 2610006
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/PaymentBubbleActionButtonsView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2610007
    return-void
.end method
