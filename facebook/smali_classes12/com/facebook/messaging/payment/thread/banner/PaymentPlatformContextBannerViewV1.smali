.class public Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/InB;


# instance fields
.field public a:LX/Duf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/ImageButton;

.field private f:Landroid/view/View;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field public i:LX/In6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2610312
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2610313
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->a()V

    .line 2610314
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610346
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610347
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->a()V

    .line 2610348
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610343
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610344
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->a()V

    .line 2610345
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2610333
    const-class v0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2610334
    const v0, 0x7f03079b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2610335
    const v0, 0x7f0d144d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->b:Landroid/view/View;

    .line 2610336
    const v0, 0x7f0d071f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->c:Landroid/widget/TextView;

    .line 2610337
    const v0, 0x7f0d1455

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->d:Landroid/widget/TextView;

    .line 2610338
    const v0, 0x7f0d1453

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->e:Landroid/widget/ImageButton;

    .line 2610339
    const v0, 0x7f0d1454

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->f:Landroid/view/View;

    .line 2610340
    const v0, 0x7f0d056c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2610341
    const v0, 0x7f0d1457

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2610342
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;

    invoke-static {v0}, LX/Duf;->a(LX/0QB;)LX/Duf;

    move-result-object v0

    check-cast v0, LX/Duf;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->a:LX/Duf;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;Z)V
    .locals 2

    .prologue
    .line 2610349
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->kf_()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    move-result-object v0

    .line 2610350
    if-eqz p2, :cond_2

    .line 2610351
    iget-object v1, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->a:LX/Duf;

    invoke-virtual {v1, v0}, LX/Duf;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->setTitle(Ljava/lang/String;)V

    .line 2610352
    iget-object v1, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->a:LX/Duf;

    invoke-virtual {v1, v0}, LX/Duf;->b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->setDescription(Ljava/lang/String;)V

    .line 2610353
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;->b()Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProductAvailability;->OUT_OF_STOCK:Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    if-ne v0, v1, :cond_1

    .line 2610354
    const v0, 0x7f082d0a

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->setPrimaryAction(I)V

    .line 2610355
    :cond_0
    :goto_0
    return-void

    .line 2610356
    :cond_1
    const v0, 0x7f082d0c

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->setPrimaryAction(I)V

    .line 2610357
    const v0, 0x7f082d0d

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->setSecondaryAction(I)V

    goto :goto_0

    .line 2610358
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->a:LX/Duf;

    invoke-virtual {v1, v0}, LX/Duf;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->setTitle(Ljava/lang/String;)V

    .line 2610359
    iget-object v1, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->a:LX/Duf;

    invoke-virtual {v1, v0}, LX/Duf;->b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->setDescription(Ljava/lang/String;)V

    .line 2610360
    const v0, 0x7f082d0a

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->setSecondaryAction(I)V

    .line 2610361
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2610362
    const v0, 0x7f082d0b

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->setPrimaryAction(I)V

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2610330
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610331
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2610332
    return-void
.end method

.method public setListener(LX/In6;)V
    .locals 2

    .prologue
    .line 2610324
    iput-object p1, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->i:LX/In6;

    .line 2610325
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->b:Landroid/view/View;

    new-instance v1, LX/InC;

    invoke-direct {v1, p0}, LX/InC;-><init>(Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610326
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->e:Landroid/widget/ImageButton;

    new-instance v1, LX/InD;

    invoke-direct {v1, p0}, LX/InD;-><init>(Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610327
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->h:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/InE;

    invoke-direct {v1, p0}, LX/InE;-><init>(Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610328
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->g:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/InF;

    invoke-direct {v1, p0}, LX/InF;-><init>(Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610329
    return-void
.end method

.method public setPrimaryAction(I)V
    .locals 2

    .prologue
    .line 2610321
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2610322
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->h:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2610323
    return-void
.end method

.method public setSecondaryAction(I)V
    .locals 2

    .prologue
    .line 2610318
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2610319
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->g:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2610320
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2610315
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610316
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV1;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2610317
    return-void
.end method
