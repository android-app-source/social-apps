.class public Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/InB;


# instance fields
.field public a:LX/Duf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Lcom/facebook/fbui/glyph/GlyphView;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Lcom/facebook/fbui/glyph/GlyphView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/ImageButton;

.field private l:Landroid/view/ViewGroup;

.field private m:Lcom/facebook/resources/ui/FbTextView;

.field private n:Lcom/facebook/resources/ui/FbTextView;

.field public o:LX/In6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2610453
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2610454
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->a()V

    .line 2610455
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610450
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610451
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->a()V

    .line 2610452
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2610447
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610448
    invoke-direct {p0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->a()V

    .line 2610449
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2610432
    const-class v0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;

    invoke-static {v0, p0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2610433
    const v0, 0x7f030f0a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2610434
    const v0, 0x7f0d144f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2610435
    const v0, 0x7f0d24a0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2610436
    const v0, 0x7f0d144d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->e:Landroid/view/View;

    .line 2610437
    const v0, 0x7f0d071f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->f:Landroid/widget/TextView;

    .line 2610438
    const v0, 0x7f0d24a1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->g:Landroid/widget/TextView;

    .line 2610439
    const v0, 0x7f0d24a2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->h:Landroid/widget/TextView;

    .line 2610440
    const v0, 0x7f0d24a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->i:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2610441
    const v0, 0x7f0d1455

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->j:Landroid/widget/TextView;

    .line 2610442
    const v0, 0x7f0d1453

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->k:Landroid/widget/ImageButton;

    .line 2610443
    const v0, 0x7f0d1456

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->l:Landroid/view/ViewGroup;

    .line 2610444
    const v0, 0x7f0d056c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2610445
    const v0, 0x7f0d1457

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 2610446
    return-void
.end method

.method private static a(Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;LX/Duf;LX/0Uh;)V
    .locals 0

    .prologue
    .line 2610431
    iput-object p1, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->a:LX/Duf;

    iput-object p2, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->b:LX/0Uh;

    return-void
.end method

.method private a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2610421
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->kf_()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    move-result-object v0

    .line 2610422
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;->d()LX/0Px;

    move-result-object v0

    .line 2610423
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2610424
    iget-object v1, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-class v2, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2610425
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2610426
    :goto_0
    return-void

    .line 2610427
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2610428
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->d:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020989

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2610429
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0

    .line 2610430
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->d:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020921

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;

    invoke-static {v1}, LX/Duf;->a(LX/0QB;)LX/Duf;

    move-result-object v0

    check-cast v0, LX/Duf;

    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->a(Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;LX/Duf;LX/0Uh;)V

    return-void
.end method

.method private b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2610373
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->b:LX/0Uh;

    const/16 v1, 0x21

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2610374
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->l:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2610375
    :goto_0
    return-void

    .line 2610376
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->l:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2610377
    if-eqz p2, :cond_2

    .line 2610378
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->kf_()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;->b()Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLProductAvailability;->OUT_OF_STOCK:Lcom/facebook/graphql/enums/GraphQLProductAvailability;

    if-ne v0, v1, :cond_1

    .line 2610379
    const v0, 0x7f082d0a

    invoke-direct {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->setPrimaryActionText(I)V

    goto :goto_0

    .line 2610380
    :cond_1
    const v0, 0x7f082d0c

    invoke-direct {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->setPrimaryActionText(I)V

    .line 2610381
    const v0, 0x7f082d0d

    invoke-direct {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->setSecondaryActionText(I)V

    goto :goto_0

    .line 2610382
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2610383
    const v0, 0x7f082d0b

    invoke-direct {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->setPrimaryActionText(I)V

    .line 2610384
    :cond_3
    const v0, 0x7f082d0a

    invoke-direct {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->setSecondaryActionText(I)V

    goto :goto_0
.end method

.method private setMetaData(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2610410
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2610411
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->j:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610412
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2610413
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f02073d

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2610414
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2610415
    :cond_0
    :goto_0
    return-void

    .line 2610416
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2610417
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->j:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610418
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2610419
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->i:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020966

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2610420
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setPrice(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2610404
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->a:LX/Duf;

    invoke-virtual {v0, p1}, LX/Duf;->c(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)Ljava/lang/String;

    move-result-object v0

    .line 2610405
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2610406
    iget-object v1, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610407
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2610408
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2610409
    :cond_0
    return-void
.end method

.method private setPrimaryActionText(I)V
    .locals 2

    .prologue
    .line 2610401
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2610402
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->m:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2610403
    return-void
.end method

.method private setSecondaryActionText(I)V
    .locals 2

    .prologue
    .line 2610398
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2610399
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->n:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2610400
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;Z)V
    .locals 3

    .prologue
    .line 2610391
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->kf_()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;

    move-result-object v0

    .line 2610392
    invoke-direct {p0, p1}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;)V

    .line 2610393
    iget-object v1, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->f:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->a:LX/Duf;

    invoke-virtual {v2, v0}, LX/Duf;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610394
    invoke-direct {p0, v0}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->setPrice(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformItemModel;)V

    .line 2610395
    invoke-direct {p0, p1}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->setMetaData(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;)V

    .line 2610396
    invoke-direct {p0, p1, p2}, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;Z)V

    .line 2610397
    return-void
.end method

.method public setListener(LX/In6;)V
    .locals 2

    .prologue
    .line 2610385
    iput-object p1, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->o:LX/In6;

    .line 2610386
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->e:Landroid/view/View;

    new-instance v1, LX/InG;

    invoke-direct {v1, p0}, LX/InG;-><init>(Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610387
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->k:Landroid/widget/ImageButton;

    new-instance v1, LX/InH;

    invoke-direct {v1, p0}, LX/InH;-><init>(Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610388
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->m:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/InI;

    invoke-direct {v1, p0}, LX/InI;-><init>(Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610389
    iget-object v0, p0, Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;->n:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/InJ;

    invoke-direct {v1, p0}, LX/InJ;-><init>(Lcom/facebook/messaging/payment/thread/banner/PaymentPlatformContextBannerViewV2;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2610390
    return-void
.end method
