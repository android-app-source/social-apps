.class public Lcom/facebook/messaging/analytics/navigation/NavigationLogs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/analytics/navigation/NavigationLogs;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2588304
    new-instance v0, LX/IYu;

    invoke-direct {v0}, LX/IYu;-><init>()V

    sput-object v0, Lcom/facebook/messaging/analytics/navigation/NavigationLogs;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2588305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2588306
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0P1;

    iput-object v0, p0, Lcom/facebook/messaging/analytics/navigation/NavigationLogs;->a:LX/0P1;

    .line 2588307
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2588308
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2588309
    iget-object v0, p0, Lcom/facebook/messaging/analytics/navigation/NavigationLogs;->a:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2588310
    return-void
.end method
