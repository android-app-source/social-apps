.class public final Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0lF;

.field public final synthetic b:LX/296;


# direct methods
.method public constructor <init>(LX/296;LX/0lF;)V
    .locals 0

    .prologue
    .line 2627320
    iput-object p1, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;->b:LX/296;

    iput-object p2, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;->a:LX/0lF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2627321
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 2627322
    :try_start_0
    const-string v1, "p_id"

    iget-object v2, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;->a:LX/0lF;

    const-string v3, "PushNotifID"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2627323
    const-string v1, "mid"

    iget-object v2, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;->a:LX/0lF;

    const-string v3, "n"

    invoke-virtual {v2, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2627324
    iget-object v1, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;->b:LX/296;

    iget-boolean v1, v1, LX/296;->g:Z

    if-eqz v1, :cond_1

    .line 2627325
    iget-object v1, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;->b:LX/296;

    iget-object v1, v1, LX/296;->d:LX/290;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "0"

    invoke-virtual {v1, v0, v2}, LX/290;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2627326
    :cond_0
    :goto_0
    return-void

    .line 2627327
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;->b:LX/296;

    iget-object v1, v1, LX/296;->e:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 2627328
    const-string v1, "re_time"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 2627329
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2627330
    iget-object v1, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;->b:LX/296;

    iget-object v1, v1, LX/296;->d:LX/290;

    const-string v2, "-1"

    invoke-virtual {v1, v0, v2}, LX/290;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2627331
    iget-object v1, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;->b:LX/296;

    iget-object v1, v1, LX/296;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2gS;->c:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2627332
    iget-object v1, p0, Lcom/facebook/messaging/zeropayloadrule/ZeroPayloadWakeUpLogger$2;->b:LX/296;

    iget-object v1, v1, LX/296;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/2gS;->c:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2627333
    :catch_0
    goto :goto_0
.end method
