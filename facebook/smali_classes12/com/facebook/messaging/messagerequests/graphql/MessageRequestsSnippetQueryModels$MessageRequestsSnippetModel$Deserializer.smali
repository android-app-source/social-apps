.class public final Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2603621
    const-class v0, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    new-instance v1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2603622
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2603623
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2603624
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2603625
    const/4 v2, 0x0

    .line 2603626
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 2603627
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2603628
    :goto_0
    move v1, v2

    .line 2603629
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2603630
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2603631
    new-instance v1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    invoke-direct {v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;-><init>()V

    .line 2603632
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2603633
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2603634
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2603635
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2603636
    :cond_0
    return-object v1

    .line 2603637
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2603638
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2603639
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2603640
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2603641
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 2603642
    const-string v5, "message_threads"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2603643
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2603644
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_e

    .line 2603645
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2603646
    :goto_2
    move v3, v4

    .line 2603647
    goto :goto_1

    .line 2603648
    :cond_3
    const-string v5, "other_threads_count"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2603649
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2603650
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_13

    .line 2603651
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2603652
    :goto_3
    move v1, v4

    .line 2603653
    goto :goto_1

    .line 2603654
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2603655
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2603656
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2603657
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 2603658
    :cond_6
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_b

    .line 2603659
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2603660
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2603661
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v11, :cond_6

    .line 2603662
    const-string p0, "count"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2603663
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v6

    move v10, v6

    move v6, v5

    goto :goto_4

    .line 2603664
    :cond_7
    const-string p0, "mailbox_banner_snippet"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 2603665
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 2603666
    :cond_8
    const-string p0, "nodes"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2603667
    invoke-static {p1, v0}, LX/IiG;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_4

    .line 2603668
    :cond_9
    const-string p0, "unread_count"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 2603669
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v5

    goto :goto_4

    .line 2603670
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2603671
    :cond_b
    const/4 v11, 0x4

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 2603672
    if-eqz v6, :cond_c

    .line 2603673
    invoke-virtual {v0, v4, v10, v4}, LX/186;->a(III)V

    .line 2603674
    :cond_c
    invoke-virtual {v0, v5, v9}, LX/186;->b(II)V

    .line 2603675
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v8}, LX/186;->b(II)V

    .line 2603676
    if-eqz v3, :cond_d

    .line 2603677
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v7, v4}, LX/186;->a(III)V

    .line 2603678
    :cond_d
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_e
    move v3, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    move v10, v4

    goto :goto_4

    .line 2603679
    :cond_f
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_11

    .line 2603680
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2603681
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2603682
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_f

    if-eqz v7, :cond_f

    .line 2603683
    const-string v8, "count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 2603684
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v5

    goto :goto_5

    .line 2603685
    :cond_10
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 2603686
    :cond_11
    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2603687
    if-eqz v1, :cond_12

    .line 2603688
    invoke-virtual {v0, v4, v6, v4}, LX/186;->a(III)V

    .line 2603689
    :cond_12
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto/16 :goto_3

    :cond_13
    move v1, v4

    move v6, v4

    goto :goto_5
.end method
