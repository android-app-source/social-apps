.class public final Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2603690
    const-class v0, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    new-instance v1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2603691
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2603692
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 2603693
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2603694
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2603695
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2603696
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2603697
    if-eqz v2, :cond_8

    .line 2603698
    const-string v3, "message_threads"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2603699
    const/4 v5, 0x0

    .line 2603700
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2603701
    invoke-virtual {v1, v2, v5, v5}, LX/15i;->a(III)I

    move-result v3

    .line 2603702
    if-eqz v3, :cond_0

    .line 2603703
    const-string v4, "count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2603704
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 2603705
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2603706
    if-eqz v3, :cond_1

    .line 2603707
    const-string v4, "mailbox_banner_snippet"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2603708
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2603709
    :cond_1
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2603710
    if-eqz v3, :cond_6

    .line 2603711
    const-string v4, "nodes"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2603712
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2603713
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v6

    if-ge v4, v6, :cond_5

    .line 2603714
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v6

    .line 2603715
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2603716
    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, LX/15i;->g(II)I

    move-result v7

    .line 2603717
    if-eqz v7, :cond_4

    .line 2603718
    const-string p0, "thread_key"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2603719
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2603720
    const/4 p0, 0x0

    invoke-virtual {v1, v7, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2603721
    if-eqz p0, :cond_2

    .line 2603722
    const-string v6, "other_user_id"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2603723
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2603724
    :cond_2
    const/4 p0, 0x1

    invoke-virtual {v1, v7, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2603725
    if-eqz p0, :cond_3

    .line 2603726
    const-string v6, "thread_fbid"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2603727
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2603728
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2603729
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2603730
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2603731
    :cond_5
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2603732
    :cond_6
    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 2603733
    if-eqz v3, :cond_7

    .line 2603734
    const-string v4, "unread_count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2603735
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 2603736
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2603737
    :cond_8
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2603738
    if-eqz v2, :cond_a

    .line 2603739
    const-string v3, "other_threads_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2603740
    const/4 v3, 0x0

    .line 2603741
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2603742
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 2603743
    if-eqz v3, :cond_9

    .line 2603744
    const-string v4, "count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2603745
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 2603746
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2603747
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2603748
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2603749
    check-cast p1, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel$Serializer;->a(Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$MessageRequestsSnippetModel;LX/0nX;LX/0my;)V

    return-void
.end method
