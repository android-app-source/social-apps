.class public final Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2603559
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2603560
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2603561
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2603562
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 11

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2603563
    if-nez p1, :cond_0

    .line 2603564
    :goto_0
    return v0

    .line 2603565
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2603566
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2603567
    :sswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2603568
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2603569
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2603570
    invoke-virtual {p0, p1, v7}, LX/15i;->p(II)I

    move-result v3

    .line 2603571
    const v4, -0x2959451

    const/4 v9, 0x0

    .line 2603572
    if-nez v3, :cond_1

    move v5, v9

    .line 2603573
    :goto_1
    move v3, v5

    .line 2603574
    invoke-virtual {p0, p1, v8, v0}, LX/15i;->a(III)I

    move-result v4

    .line 2603575
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2603576
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2603577
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2603578
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 2603579
    invoke-virtual {p3, v8, v4, v0}, LX/186;->a(III)V

    .line 2603580
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2603581
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2603582
    const v2, 0x182b6e0

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2603583
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2603584
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2603585
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2603586
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2603587
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2603588
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2603589
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2603590
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2603591
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2603592
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2603593
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2603594
    :sswitch_3
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2603595
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2603596
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2603597
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2603598
    :cond_1
    invoke-virtual {p0, v3}, LX/15i;->c(I)I

    move-result v10

    .line 2603599
    if-nez v10, :cond_2

    const/4 v5, 0x0

    .line 2603600
    :goto_2
    if-ge v9, v10, :cond_3

    .line 2603601
    invoke-virtual {p0, v3, v9}, LX/15i;->q(II)I

    move-result p2

    .line 2603602
    invoke-static {p0, p2, v4, p3}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v5, v9

    .line 2603603
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 2603604
    :cond_2
    new-array v5, v10, [I

    goto :goto_2

    .line 2603605
    :cond_3
    const/4 v9, 0x1

    invoke-virtual {p3, v5, v9}, LX/186;->a([IZ)I

    move-result v5

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2959451 -> :sswitch_1
        0x182b6e0 -> :sswitch_2
        0x1458aca4 -> :sswitch_0
        0x408a8551 -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2603620
    new-instance v0, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2603606
    sparse-switch p2, :sswitch_data_0

    .line 2603607
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2603608
    :sswitch_0
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2603609
    const v1, -0x2959451

    .line 2603610
    if-eqz v0, :cond_0

    .line 2603611
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2603612
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2603613
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2603614
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2603615
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2603616
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 2603617
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2603618
    const v1, 0x182b6e0

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2959451 -> :sswitch_2
        0x182b6e0 -> :sswitch_1
        0x1458aca4 -> :sswitch_0
        0x408a8551 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2603541
    if-eqz p1, :cond_0

    .line 2603542
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2603543
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;

    .line 2603544
    if-eq v0, v1, :cond_0

    .line 2603545
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2603546
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2603619
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2603552
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2603553
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2603554
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2603555
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2603556
    :cond_0
    iput-object p1, p0, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2603557
    iput p2, p0, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;->b:I

    .line 2603558
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2603551
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2603550
    new-instance v0, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2603547
    iget v0, p0, LX/1vt;->c:I

    .line 2603548
    move v0, v0

    .line 2603549
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2603538
    iget v0, p0, LX/1vt;->c:I

    .line 2603539
    move v0, v0

    .line 2603540
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2603535
    iget v0, p0, LX/1vt;->b:I

    .line 2603536
    move v0, v0

    .line 2603537
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2603532
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2603533
    move-object v0, v0

    .line 2603534
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2603523
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2603524
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2603525
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2603526
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2603527
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2603528
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2603529
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2603530
    invoke-static {v3, v9, v2}, Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/messaging/messagerequests/graphql/MessageRequestsSnippetQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2603531
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2603520
    iget v0, p0, LX/1vt;->c:I

    .line 2603521
    move v0, v0

    .line 2603522
    return v0
.end method
