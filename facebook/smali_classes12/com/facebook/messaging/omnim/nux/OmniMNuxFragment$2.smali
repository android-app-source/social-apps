.class public final Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public final synthetic b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public final synthetic c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public final synthetic d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final synthetic e:Landroid/view/LayoutInflater;

.field public final synthetic f:Landroid/widget/LinearLayout;

.field public final synthetic g:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/view/LayoutInflater;Landroid/widget/LinearLayout;)V
    .locals 0

    .prologue
    .line 2604459
    iput-object p1, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->g:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;

    iput-object p2, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object p3, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object p4, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object p5, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p6, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->e:Landroid/view/LayoutInflater;

    iput-object p7, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->f:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2604425
    const-string v0, "OmniMNuxFragment"

    const-string v1, "Failed to fetch OmniM Nux: "

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2604426
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2604427
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/16 v11, 0x8

    const/4 v4, 0x0

    .line 2604428
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2604429
    check-cast v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    move-result-object v1

    .line 2604430
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;->m()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setLinkableTextWithEntities(LX/3Ab;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 2604431
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setLinkableTextWithEntities(LX/3Ab;)V
    :try_end_1
    .catch LX/47A; {:try_start_1 .. :try_end_1} :catch_1

    .line 2604432
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2604433
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    new-instance v3, LX/IiV;

    invoke-direct {v3, p0}, LX/IiV;-><init>(Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;)V

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/3Ab;LX/7wC;)V

    .line 2604434
    :goto_2
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-class v3, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2604435
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->k()I

    move-result v3

    invoke-virtual {v1}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->a()I

    move-result v5

    invoke-direct {v2, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2604436
    invoke-virtual {v1}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;->l()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v4

    :goto_3
    if-ge v3, v6, :cond_3

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;

    .line 2604437
    iget-object v1, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->e:Landroid/view/LayoutInflater;

    const v2, 0x7f030a61

    iget-object v7, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2, v7, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2604438
    const v2, 0x7f0d1a68

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2604439
    invoke-virtual {v0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    move-result-object v7

    .line 2604440
    if-eqz v7, :cond_2

    .line 2604441
    invoke-virtual {v7}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    const-class v9, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;

    invoke-static {v9}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2604442
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v7}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;->l()I

    move-result v9

    invoke-virtual {v7}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;->a()I

    move-result v10

    invoke-direct {v8, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2604443
    invoke-virtual {v7}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;->j()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 2604444
    invoke-virtual {v0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2604445
    :cond_0
    :goto_4
    const v2, 0x7f0d1a69

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2604446
    :try_start_2
    invoke-virtual {v0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setLinkableTextWithEntities(LX/3Ab;)V
    :try_end_2
    .catch LX/47A; {:try_start_2 .. :try_end_2} :catch_2

    .line 2604447
    :goto_5
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2604448
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 2604449
    :catch_0
    move-exception v0

    .line 2604450
    const-string v2, "OmniMNuxFragment"

    const-string v3, "Unable to set nux title."

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 2604451
    :catch_1
    move-exception v0

    .line 2604452
    const-string v2, "OmniMNuxFragment"

    const-string v3, "Unable to set nux description."

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 2604453
    :cond_1
    const-string v0, "OmniMNuxFragment"

    const-string v2, "Nux footer is null."

    invoke-static {v0, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2604454
    :cond_2
    invoke-virtual {v2, v11}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_4

    .line 2604455
    :catch_2
    move-exception v0

    .line 2604456
    const-string v7, "OmniMNuxFragment"

    const-string v8, "Unable to set nux step title."

    invoke-static {v7, v8, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2604457
    invoke-virtual {v2, v11}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    goto :goto_5

    .line 2604458
    :cond_3
    return-void
.end method
