.class public Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dde;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/IiW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2604460
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2604461
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2604462
    iput-object v0, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->m:LX/0Ot;

    .line 2604463
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2604464
    iput-object v0, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->o:LX/0Ot;

    .line 2604465
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2604466
    iput-object v0, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->p:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;

    const/16 v1, 0x26ff

    invoke-static {v2, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    new-instance v0, LX/IiW;

    invoke-static {v2}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v2}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    invoke-direct {v0, v1, v4, v5, p0}, LX/IiW;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0W3;)V

    move-object v1, v0

    check-cast v1, LX/IiW;

    const/16 v4, 0xb19

    invoke-static {v2, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x455

    invoke-static {v2, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v3, p1, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->m:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->n:LX/IiW;

    iput-object v4, p1, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->o:LX/0Ot;

    iput-object v2, p1, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->p:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2b6c6a65

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2604467
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2604468
    const-class v1, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;

    invoke-static {v1, p0}, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2604469
    const v1, 0x1030010

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2604470
    const/16 v1, 0x2b

    const v2, 0x72b36b02

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/16 v0, 0x2a

    const v1, 0x2c4d2e0c

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2604471
    const v0, 0x7f030a60

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 2604472
    const v0, 0x7f0d1a62

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2604473
    new-instance v1, LX/IiU;

    invoke-direct {v1, p0}, LX/IiU;-><init>(Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2604474
    const v0, 0x7f0d1a63

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2604475
    const v0, 0x7f0d1a64

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2604476
    const v0, 0x7f0d1a65

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2604477
    const v0, 0x7f0d1a67

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2604478
    const v0, 0x7f0d1a66

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 2604479
    new-instance v0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;-><init>(Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/view/LayoutInflater;Landroid/widget/LinearLayout;)V

    .line 2604480
    iget-object v1, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->n:LX/IiW;

    .line 2604481
    new-instance v2, LX/IiX;

    invoke-direct {v2}, LX/IiX;-><init>()V

    move-object v2, v2

    .line 2604482
    const-string v3, "type"

    const-string v4, "OMNI_M_PROACTIVE"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2604483
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2604484
    iget-object v3, v1, LX/IiW;->b:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2604485
    iget-object v3, v1, LX/IiW;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v0, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2604486
    const/16 v0, 0x2b

    const v1, 0x63ef2470

    invoke-static {v10, v0, v1, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v9
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x78ac6311    # -1.5919994E-34f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2604487
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 2604488
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2604489
    if-eqz v0, :cond_1

    .line 2604490
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2604491
    const-string v1, "ACTION_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2604492
    :goto_0
    if-eqz v1, :cond_0

    .line 2604493
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dde;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, LX/Dde;->a(Ljava/lang/String;Z)V

    .line 2604494
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dde;

    invoke-virtual {v0, v1}, LX/Dde;->a(Ljava/lang/String;)V

    .line 2604495
    :cond_0
    const v0, -0x74846634

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-void

    .line 2604496
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method
