.class public final Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x10fc807f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2604828
    const-class v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2604831
    const-class v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2604829
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2604830
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2604814
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2604815
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2604816
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2604817
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2604818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2604819
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2604820
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2604821
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2604822
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    .line 2604823
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2604824
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;

    .line 2604825
    iput-object v0, v1, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;->e:Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    .line 2604826
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2604827
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2604812
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;->e:Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    iput-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;->e:Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    .line 2604813
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;->e:Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2604809
    new-instance v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;

    invoke-direct {v0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;-><init>()V

    .line 2604810
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2604811
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2604808
    const v0, 0x1b84ed59

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2604807
    const v0, -0x6747e1ce

    return v0
.end method
