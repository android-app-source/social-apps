.class public final Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6fcae577
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:D

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2604578
    const-class v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2604577
    const-class v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2604575
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2604576
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2604573
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->e:Ljava/lang/String;

    .line 2604574
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2604571
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->h:Ljava/lang/String;

    .line 2604572
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2604533
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->i:Ljava/lang/String;

    .line 2604534
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2604569
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2604570
    iget v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->f:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2604553
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2604554
    invoke-direct {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2604555
    invoke-direct {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2604556
    invoke-direct {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2604557
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2604558
    const/16 v3, 0x8

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2604559
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 2604560
    const/4 v0, 0x1

    iget v3, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->f:I

    invoke-virtual {p1, v0, v3, v7}, LX/186;->a(III)V

    .line 2604561
    const/4 v0, 0x2

    iget-boolean v3, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->g:Z

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 2604562
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2604563
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2604564
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->j:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2604565
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2604566
    const/4 v0, 0x7

    iget v1, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->l:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 2604567
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2604568
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2604550
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2604551
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2604552
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2604544
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2604545
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->f:I

    .line 2604546
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->g:Z

    .line 2604547
    const/4 v0, 0x5

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->j:D

    .line 2604548
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->l:I

    .line 2604549
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2604541
    new-instance v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;

    invoke-direct {v0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;-><init>()V

    .line 2604542
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2604543
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2604540
    const v0, -0x6e5267b7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2604539
    const v0, 0x437b93b

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2604537
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->k:Ljava/lang/String;

    .line 2604538
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 2604535
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2604536
    iget v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$CoverPhotoModel;->l:I

    return v0
.end method
