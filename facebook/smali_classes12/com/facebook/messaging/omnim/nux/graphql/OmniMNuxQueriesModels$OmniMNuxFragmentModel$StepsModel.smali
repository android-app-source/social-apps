.class public final Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6bdba7aa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2604706
    const-class v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2604705
    const-class v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2604703
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2604704
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2604695
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2604696
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2604697
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2604698
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2604699
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2604700
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2604701
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2604702
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2604682
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2604683
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2604684
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    .line 2604685
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2604686
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;

    .line 2604687
    iput-object v0, v1, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->e:Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    .line 2604688
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2604689
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2604690
    invoke-virtual {p0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2604691
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;

    .line 2604692
    iput-object v0, v1, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2604693
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2604694
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2604673
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->e:Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->e:Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    .line 2604674
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->e:Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel$ImageModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2604679
    new-instance v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;-><init>()V

    .line 2604680
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2604681
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2604678
    const v0, 0x4b17d640    # 9950784.0f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2604677
    const v0, 0x1efe55a1

    return v0
.end method

.method public final j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2604675
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2604676
    iget-object v0, p0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxFragmentModel$StepsModel;->f:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    return-object v0
.end method
