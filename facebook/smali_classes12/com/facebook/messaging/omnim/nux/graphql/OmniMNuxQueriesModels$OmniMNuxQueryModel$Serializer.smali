.class public final Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2604794
    const-class v0, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;

    new-instance v1, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2604795
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2604796
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2604797
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2604798
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2604799
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2604800
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2604801
    if-eqz v2, :cond_0

    .line 2604802
    const-string p0, "new_user_experience"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604803
    invoke-static {v1, v2, p1, p2}, LX/Iib;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2604804
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2604805
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2604806
    check-cast p1, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel$Serializer;->a(Lcom/facebook/messaging/omnim/nux/graphql/OmniMNuxQueriesModels$OmniMNuxQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
