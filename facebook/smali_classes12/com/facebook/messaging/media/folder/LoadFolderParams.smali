.class public Lcom/facebook/messaging/media/folder/LoadFolderParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/folder/LoadFolderParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Z

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2600993
    const-string v0, "load_folder_params_key"

    sput-object v0, Lcom/facebook/messaging/media/folder/LoadFolderParams;->a:Ljava/lang/String;

    .line 2600994
    new-instance v0, LX/IgM;

    invoke-direct {v0}, LX/IgM;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/folder/LoadFolderParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/IgN;)V
    .locals 1

    .prologue
    .line 2600989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600990
    iget-boolean v0, p1, LX/IgN;->a:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/media/folder/LoadFolderParams;->b:Z

    .line 2600991
    iget-boolean v0, p1, LX/IgN;->b:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/media/folder/LoadFolderParams;->c:Z

    .line 2600992
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2600981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600982
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/media/folder/LoadFolderParams;->b:Z

    .line 2600983
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/media/folder/LoadFolderParams;->c:Z

    .line 2600984
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2600988
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2600985
    iget-boolean v0, p0, Lcom/facebook/messaging/media/folder/LoadFolderParams;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2600986
    iget-boolean v0, p0, Lcom/facebook/messaging/media/folder/LoadFolderParams;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2600987
    return-void
.end method
