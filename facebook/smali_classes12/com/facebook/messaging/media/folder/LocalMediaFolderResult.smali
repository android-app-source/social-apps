.class public Lcom/facebook/messaging/media/folder/LocalMediaFolderResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/folder/LocalMediaFolderResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/media/folder/Folder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2601066
    new-instance v0, LX/IgP;

    invoke-direct {v0}, LX/IgP;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/folder/LocalMediaFolderResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2601067
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2601068
    sget-object v0, Lcom/facebook/messaging/media/folder/Folder;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/folder/LocalMediaFolderResult;->a:LX/0Px;

    .line 2601069
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/media/folder/Folder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2601070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2601071
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/folder/LocalMediaFolderResult;->a:LX/0Px;

    .line 2601072
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2601073
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2601074
    iget-object v0, p0, Lcom/facebook/messaging/media/folder/LocalMediaFolderResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2601075
    return-void
.end method
