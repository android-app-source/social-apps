.class public Lcom/facebook/messaging/media/folder/Folder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/folder/Folder;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Landroid/net/Uri;

.field public d:I

.field public e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2600931
    new-instance v0, LX/IgH;

    invoke-direct {v0}, LX/IgH;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/folder/Folder;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/IgI;)V
    .locals 2

    .prologue
    .line 2600932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600933
    iget-object v0, p1, LX/IgI;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/media/folder/Folder;->a:Ljava/lang/String;

    .line 2600934
    iget-object v0, p1, LX/IgI;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/media/folder/Folder;->b:Ljava/lang/String;

    .line 2600935
    iget-object v0, p1, LX/IgI;->c:Landroid/net/Uri;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/media/folder/Folder;->c:Landroid/net/Uri;

    .line 2600936
    iget v0, p1, LX/IgI;->d:I

    iput v0, p0, Lcom/facebook/messaging/media/folder/Folder;->d:I

    .line 2600937
    iget-wide v0, p1, LX/IgI;->e:J

    iput-wide v0, p0, Lcom/facebook/messaging/media/folder/Folder;->e:J

    .line 2600938
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2600939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600940
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/folder/Folder;->b:Ljava/lang/String;

    .line 2600941
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/folder/Folder;->a:Ljava/lang/String;

    .line 2600942
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/media/folder/Folder;->c:Landroid/net/Uri;

    .line 2600943
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/media/folder/Folder;->d:I

    .line 2600944
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/media/folder/Folder;->e:J

    .line 2600945
    return-void
.end method

.method public static newBuilder()LX/IgI;
    .locals 1

    .prologue
    .line 2600946
    new-instance v0, LX/IgI;

    invoke-direct {v0}, LX/IgI;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2600947
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2600948
    iget-object v0, p0, Lcom/facebook/messaging/media/folder/Folder;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2600949
    iget-object v0, p0, Lcom/facebook/messaging/media/folder/Folder;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2600950
    iget-object v0, p0, Lcom/facebook/messaging/media/folder/Folder;->c:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2600951
    iget v0, p0, Lcom/facebook/messaging/media/folder/Folder;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2600952
    iget-wide v0, p0, Lcom/facebook/messaging/media/folder/Folder;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2600953
    return-void
.end method
