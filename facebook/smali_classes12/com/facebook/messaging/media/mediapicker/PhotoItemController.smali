.class public Lcom/facebook/messaging/media/mediapicker/PhotoItemController;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final m:LX/1Ad;

.field public final n:Landroid/content/res/Resources;

.field public final o:LX/11S;

.field public final p:LX/Igp;

.field private final q:LX/Igq;

.field private final r:LX/Igr;

.field public final s:LX/1o9;

.field public final t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public u:Lcom/facebook/widget/PhotoToggleButton;

.field public v:Landroid/view/View;

.field public w:LX/Igl;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/ui/media/attachments/MediaResource;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2601782
    const-class v0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Landroid/content/res/Resources;LX/11S;Landroid/view/View;)V
    .locals 2
    .param p4    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2601783
    invoke-direct {p0, p4}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2601784
    new-instance v0, LX/Igp;

    invoke-direct {v0, p0}, LX/Igp;-><init>(Lcom/facebook/messaging/media/mediapicker/PhotoItemController;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->p:LX/Igp;

    .line 2601785
    new-instance v0, LX/Igq;

    invoke-direct {v0, p0}, LX/Igq;-><init>(Lcom/facebook/messaging/media/mediapicker/PhotoItemController;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->q:LX/Igq;

    .line 2601786
    new-instance v0, LX/Igr;

    invoke-direct {v0, p0}, LX/Igr;-><init>(Lcom/facebook/messaging/media/mediapicker/PhotoItemController;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->r:LX/Igr;

    .line 2601787
    iput-object p1, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->m:LX/1Ad;

    .line 2601788
    iput-object p2, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->n:Landroid/content/res/Resources;

    .line 2601789
    iput-object p3, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->o:LX/11S;

    .line 2601790
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->n:Landroid/content/res/Resources;

    const v1, 0x7f0b1f00

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2601791
    new-instance v1, LX/1o9;

    invoke-direct {v1, v0, v0}, LX/1o9;-><init>(II)V

    iput-object v1, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->s:LX/1o9;

    .line 2601792
    const v0, 0x7f0d1063

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2601793
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->q:LX/Igq;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2601794
    const v0, 0x7f0d1adc

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->v:Landroid/view/View;

    .line 2601795
    const v0, 0x7f0d24f7

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/PhotoToggleButton;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->u:Lcom/facebook/widget/PhotoToggleButton;

    .line 2601796
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->u:Lcom/facebook/widget/PhotoToggleButton;

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->r:LX/Igr;

    .line 2601797
    iput-object v1, v0, Lcom/facebook/widget/PhotoToggleButton;->e:LX/4oG;

    .line 2601798
    return-void
.end method
