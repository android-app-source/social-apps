.class public final Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;


# instance fields
.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 2601259
    new-instance v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    const/4 v1, 0x0

    move v3, v2

    move v4, v2

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;-><init>(ZZZZLcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601260
    new-instance v0, LX/IgY;

    invoke-direct {v0}, LX/IgY;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/IgZ;)V
    .locals 1

    .prologue
    .line 2601261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2601262
    iget-boolean v0, p1, LX/IgZ;->a:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    .line 2601263
    iget-boolean v0, p1, LX/IgZ;->b:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->c:Z

    .line 2601264
    iget-boolean v0, p1, LX/IgZ;->c:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->d:Z

    .line 2601265
    iget-boolean v0, p1, LX/IgZ;->d:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->e:Z

    .line 2601266
    iget-object v0, p1, LX/IgZ;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2601267
    iget-object v0, p1, LX/IgZ;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->g:Ljava/lang/String;

    .line 2601268
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2601269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2601270
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    .line 2601271
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->c:Z

    .line 2601272
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->d:Z

    .line 2601273
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->e:Z

    .line 2601274
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2601275
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->g:Ljava/lang/String;

    .line 2601276
    return-void
.end method

.method private constructor <init>(ZZZZLcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V
    .locals 0
    .param p5    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2601277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2601278
    iput-boolean p1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    .line 2601279
    iput-boolean p2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->c:Z

    .line 2601280
    iput-boolean p3, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->d:Z

    .line 2601281
    iput-boolean p4, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->e:Z

    .line 2601282
    iput-object p5, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2601283
    iput-object p6, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->g:Ljava/lang/String;

    .line 2601284
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2601285
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2601286
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "singleMediaItemOnly"

    iget-boolean v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "supportGifs"

    iget-boolean v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->c:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "supportVideo"

    iget-boolean v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->d:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "sendInstantly"

    iget-boolean v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->e:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "threadKey"

    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "pendingOfflineThreadingId"

    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2601287
    iget-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2601288
    iget-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2601289
    iget-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2601290
    iget-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2601291
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2601292
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2601293
    return-void
.end method
