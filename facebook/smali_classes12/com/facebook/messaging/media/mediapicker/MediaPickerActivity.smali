.class public Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private final p:LX/IgV;

.field private final q:LX/IgX;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2601216
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2601217
    new-instance v0, LX/IgV;

    invoke-direct {v0, p0}, LX/IgV;-><init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->p:LX/IgV;

    .line 2601218
    new-instance v0, LX/IgX;

    invoke-direct {v0, p0}, LX/IgX;-><init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->q:LX/IgX;

    .line 2601219
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;)Landroid/content/Intent;
    .locals 2
    .param p2    # Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<+",
            "Landroid/os/Parcelable;",
            ">;",
            "Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 2601220
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2601221
    const-string v1, "extra_selection"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2601222
    if-eqz p2, :cond_0

    .line 2601223
    const-string v1, "extra_environment"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2601224
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2601225
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 2601226
    instance-of v0, p1, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    if-eqz v0, :cond_1

    .line 2601227
    check-cast p1, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->p:LX/IgV;

    .line 2601228
    iput-object v0, p1, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->I:LX/IgV;

    .line 2601229
    :cond_0
    :goto_0
    return-void

    .line 2601230
    :cond_1
    instance-of v0, p1, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    if-eqz v0, :cond_0

    .line 2601231
    check-cast p1, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->q:LX/IgX;

    .line 2601232
    iput-object v0, p1, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->v:LX/IgW;

    .line 2601233
    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2601234
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2601235
    const v0, 0x7f030a96

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->setContentView(I)V

    .line 2601236
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "media-picker-fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2601237
    new-instance v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;-><init>()V

    .line 2601238
    invoke-virtual {p0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2601239
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    const-string v3, "media-picker-fragment"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2601240
    :cond_0
    return-void
.end method
