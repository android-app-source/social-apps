.class public Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "WorldWriteableFiles"
    }
.end annotation


# static fields
.field public static final m:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private B:LX/1Ml;

.field public C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

.field public D:LX/0Uh;

.field public E:Landroid/net/Uri;

.field public F:Landroid/net/Uri;

.field public G:LX/Ika;

.field public H:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private I:Landroid/os/Bundle;

.field public n:LX/6eV;

.field public o:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

.field private p:LX/5zt;

.field public q:LX/2Ib;

.field public r:LX/1Er;

.field public s:Lcom/facebook/content/SecureContextHelper;

.field public t:LX/0kL;

.field private u:LX/0TD;

.field private v:Ljava/util/concurrent/Executor;

.field public w:LX/0Sh;

.field public x:Landroid/content/ContentResolver;

.field public y:LX/03V;

.field private z:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2602130
    const-class v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    sput-object v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->m:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2602131
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2602132
    return-void
.end method

.method private static a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Landroid/content/ContentResolver;LX/03V;LX/6eV;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/5zt;LX/2Ib;LX/1Er;Lcom/facebook/content/SecureContextHelper;LX/0kL;LX/0TD;Ljava/util/concurrent/Executor;LX/0Sh;LX/0Or;LX/0Or;LX/0Uh;LX/1Ml;)V
    .locals 1
    .param p9    # LX/0kL;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p10    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p12    # LX/0Sh;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMultipickerInMessageComposerEnabled;
        .end annotation
    .end param
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsVideoSendingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/6eV;",
            "Lcom/facebook/ui/media/attachments/MediaResourceHelper;",
            "LX/5zt;",
            "LX/2Ib;",
            "LX/1Er;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0kL;",
            "LX/0TD;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/1Ml;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2602133
    iput-object p1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->x:Landroid/content/ContentResolver;

    .line 2602134
    iput-object p2, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->y:LX/03V;

    .line 2602135
    iput-object p3, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->n:LX/6eV;

    .line 2602136
    iput-object p4, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->o:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    .line 2602137
    iput-object p5, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->p:LX/5zt;

    .line 2602138
    iput-object p6, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->q:LX/2Ib;

    .line 2602139
    iput-object p7, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->r:LX/1Er;

    .line 2602140
    iput-object p8, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s:Lcom/facebook/content/SecureContextHelper;

    .line 2602141
    iput-object p9, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->t:LX/0kL;

    .line 2602142
    iput-object p10, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->u:LX/0TD;

    .line 2602143
    iput-object p11, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->v:Ljava/util/concurrent/Executor;

    .line 2602144
    iput-object p12, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->w:LX/0Sh;

    .line 2602145
    iput-object p13, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->z:LX/0Or;

    .line 2602146
    iput-object p14, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->A:LX/0Or;

    .line 2602147
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->B:LX/1Ml;

    .line 2602148
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->D:LX/0Uh;

    .line 2602149
    return-void
.end method

.method public static a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 2602150
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->b:Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    .line 2602151
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/messaging/camerautil/CropImage;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2602152
    const-string v2, "image/*"

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 2602153
    const-string v2, "outputX"

    iget v3, v0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->a:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2602154
    const-string v2, "outputY"

    iget v3, v0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->b:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2602155
    const-string v2, "aspectX"

    iget v3, v0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->c:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2602156
    const-string v2, "aspectY"

    iget v0, v0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->d:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2602157
    const-string v0, "scale"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2602158
    const-string v0, "output"

    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->F:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2602159
    const-string v0, "outputFormat"

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2}, Landroid/graphics/Bitmap$CompressFormat;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2602160
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2602161
    return-void
.end method

.method public static a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2602162
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->u:LX/0TD;

    new-instance v1, LX/Ih1;

    invoke-direct {v1, p0, p1}, LX/Ih1;-><init>(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Ljava/util/List;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2602163
    new-instance v1, LX/Ih2;

    invoke-direct {v1, p0}, LX/Ih2;-><init>(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->v:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2602164
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 18

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    invoke-static/range {v17 .. v17}, LX/0cd;->a(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v2

    check-cast v2, Landroid/content/ContentResolver;

    invoke-static/range {v17 .. v17}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static/range {v17 .. v17}, LX/6eV;->a(LX/0QB;)LX/6eV;

    move-result-object v4

    check-cast v4, LX/6eV;

    invoke-static/range {v17 .. v17}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/0QB;)Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    move-result-object v5

    check-cast v5, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-static/range {v17 .. v17}, LX/5zt;->a(LX/0QB;)LX/5zt;

    move-result-object v6

    check-cast v6, LX/5zt;

    invoke-static/range {v17 .. v17}, LX/2Ib;->a(LX/0QB;)LX/2Ib;

    move-result-object v7

    check-cast v7, LX/2Ib;

    invoke-static/range {v17 .. v17}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v8

    check-cast v8, LX/1Er;

    invoke-static/range {v17 .. v17}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v17 .. v17}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-static/range {v17 .. v17}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v11

    check-cast v11, LX/0TD;

    invoke-static/range {v17 .. v17}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/Executor;

    invoke-static/range {v17 .. v17}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v13

    check-cast v13, LX/0Sh;

    const/16 v14, 0x14eb

    move-object/from16 v0, v17

    invoke-static {v0, v14}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const/16 v15, 0x14f6

    move-object/from16 v0, v17

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    invoke-static/range {v17 .. v17}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v16

    check-cast v16, LX/0Uh;

    invoke-static/range {v17 .. v17}, LX/1Ml;->a(LX/0QB;)LX/1Ml;

    move-result-object v17

    check-cast v17, LX/1Ml;

    invoke-static/range {v1 .. v17}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Landroid/content/ContentResolver;LX/03V;LX/6eV;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/5zt;LX/2Ib;LX/1Er;Lcom/facebook/content/SecureContextHelper;LX/0kL;LX/0TD;Ljava/util/concurrent/Executor;LX/0Sh;LX/0Or;LX/0Or;LX/0Uh;LX/1Ml;)V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)Z
    .locals 4

    .prologue
    .line 2602165
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->B:LX/1Ml;

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    .line 2602166
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->B:LX/1Ml;

    const-string v2, "android.permission.CAMERA"

    invoke-virtual {v1, v2}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v1

    .line 2602167
    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v2, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->a:LX/Ih7;

    sget-object v3, LX/Ih7;->CAMERA:LX/Ih7;

    if-ne v2, v3, :cond_0

    .line 2602168
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 2602169
    :cond_0
    :goto_0
    return v0

    .line 2602170
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V
    .locals 3

    .prologue
    .line 2601937
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->u:LX/0TD;

    new-instance v1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment$1;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment$1;-><init>(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->H:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2601938
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->I:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2601939
    :goto_0
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->H:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/Ih0;

    invoke-direct {v2, p0, v0}, LX/Ih0;-><init>(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Z)V

    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->v:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2601940
    return-void

    .line 2601941
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V
    .locals 7

    .prologue
    .line 2602081
    sget-object v0, LX/Ih3;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->a:LX/Ih7;

    invoke-virtual {v1}, LX/Ih7;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2602082
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2602083
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->d:LX/0Rf;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2602084
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2602085
    const-string v1, "output"

    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->E:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2602086
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x2

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2602087
    :goto_0
    return-void

    .line 2602088
    :cond_0
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    goto :goto_0

    .line 2602089
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->z:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2602090
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2602091
    new-instance v3, LX/IgZ;

    invoke-direct {v3}, LX/IgZ;-><init>()V

    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->d:LX/0Rf;

    sget-object v4, LX/2MK;->VIDEO:LX/2MK;

    invoke-virtual {v0, v4}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->A:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 2602092
    :goto_1
    iput-boolean v0, v3, LX/IgZ;->c:Z

    .line 2602093
    move-object v0, v3

    .line 2602094
    iget-object v3, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-boolean v3, v3, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->f:Z

    .line 2602095
    iput-boolean v3, v0, LX/IgZ;->d:Z

    .line 2602096
    move-object v0, v0

    .line 2602097
    iget-object v3, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-boolean v3, v3, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->c:Z

    if-nez v3, :cond_4

    .line 2602098
    :goto_2
    iput-boolean v1, v0, LX/IgZ;->a:Z

    .line 2602099
    move-object v0, v0

    .line 2602100
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2602101
    iput-object v1, v0, LX/IgZ;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2602102
    move-object v0, v0

    .line 2602103
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->i:Ljava/lang/String;

    .line 2602104
    iput-object v1, v0, LX/IgZ;->f:Ljava/lang/String;

    .line 2602105
    move-object v0, v0

    .line 2602106
    new-instance v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;-><init>(LX/IgZ;)V

    move-object v0, v1

    .line 2602107
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->n:LX/6eV;

    .line 2602108
    iget-object v2, v1, LX/6eV;->a:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget-short v5, LX/6eU;->a:S

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v2

    move v1, v2

    .line 2602109
    if-eqz v1, :cond_5

    .line 2602110
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2602111
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2602112
    const-string v3, "extra_environment"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2602113
    move-object v0, v2

    .line 2602114
    :goto_3
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x5

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2602115
    goto/16 :goto_0

    .line 2602116
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2602117
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2602118
    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v2, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->d:LX/0Rf;

    sget-object v3, LX/2MK;->PHOTO:LX/2MK;

    invoke-virtual {v2, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2602119
    const-string v2, "image/*"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2602120
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2602121
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    .line 2602122
    :goto_4
    goto/16 :goto_0

    .line 2602123
    :catch_0
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->t(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    .line 2602124
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->t:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08119f

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 2602125
    goto/16 :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    .line 2602126
    :cond_5
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v2, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->g:Ljava/util/ArrayList;

    invoke-static {v1, v2, v0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerActivity;->a(Landroid/content/Context;Ljava/util/ArrayList;Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_3

    .line 2602127
    :cond_6
    const/16 v2, 0x2c

    invoke-static {v2}, LX/0PO;->on(C)LX/0PO;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 2602128
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2602129
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static s(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V
    .locals 1

    .prologue
    .line 2602078
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2602079
    :goto_0
    return-void

    .line 2602080
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0
.end method

.method public static t(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V
    .locals 1

    .prologue
    .line 2602075
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2602076
    :goto_0
    return-void

    .line 2602077
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 2602072
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 2602073
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 2602074
    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x55aa087b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2602052
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2602053
    iput-object p1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->I:Landroid/os/Bundle;

    .line 2602054
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2602055
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->l(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    .line 2602056
    :goto_0
    const v1, 0x7c69a087

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2602057
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2602058
    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2602059
    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v2, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->a:LX/Ih7;

    sget-object v3, LX/Ih7;->CAMERA:LX/Ih7;

    if-ne v2, v3, :cond_1

    .line 2602060
    const-string v2, "android.permission.CAMERA"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2602061
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    move-object v1, v1

    .line 2602062
    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v2, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->a:LX/Ih7;

    .line 2602063
    sget-object v3, LX/Ih7;->CAMERA:LX/Ih7;

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0802b2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    move-object v2, v3

    .line 2602064
    iget-object v3, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v3, v3, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->a:LX/Ih7;

    .line 2602065
    sget-object v4, LX/Ih7;->CAMERA:LX/Ih7;

    if-ne v3, v4, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0802b4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_2
    move-object v3, v4

    .line 2602066
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const-class p1, Lcom/facebook/runtimepermissions/RequestPermissionsActivity;

    invoke-direct {v4, v5, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2602067
    const-string v5, "extra_permissions"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2602068
    const-string v1, "extra_custom_title"

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2602069
    const-string v1, "extra_custom_subtitle"

    invoke-virtual {v4, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2602070
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x6

    invoke-interface {v1, v4, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2602071
    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0802b3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0802b5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_2
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2601960
    packed-switch p1, :pswitch_data_0

    .line 2601961
    :goto_0
    :pswitch_0
    return-void

    .line 2601962
    :pswitch_1
    const/4 p1, 0x0

    .line 2601963
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2601964
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    .line 2601965
    :goto_1
    goto :goto_0

    .line 2601966
    :pswitch_2
    const/4 v0, -0x1

    if-eq p2, v0, :cond_a

    .line 2601967
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    .line 2601968
    :goto_2
    goto :goto_0

    .line 2601969
    :pswitch_3
    const/4 v0, -0x1

    if-ne p2, v0, :cond_d

    .line 2601970
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->b:Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    if-eqz v0, :cond_c

    .line 2601971
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->E:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Landroid/net/Uri;)V

    .line 2601972
    :goto_3
    goto :goto_0

    .line 2601973
    :pswitch_4
    sget-object v0, LX/Ih3;->a:[I

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->a:LX/Ih7;

    invoke-virtual {v1}, LX/Ih7;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2601974
    sget-object v0, LX/5zj;->UNSPECIFIED:LX/5zj;

    .line 2601975
    :goto_4
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->a:LX/Ih7;

    sget-object v2, LX/Ih7;->GALLERY:LX/Ih7;

    if-ne v1, v2, :cond_0

    .line 2601976
    sget-object v0, LX/5zj;->GALLERY:LX/5zj;

    .line 2601977
    :cond_0
    const/4 v1, -0x1

    if-ne p2, v1, :cond_e

    .line 2601978
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->F:Landroid/net/Uri;

    .line 2601979
    iput-object v2, v1, LX/5zn;->b:Landroid/net/Uri;

    .line 2601980
    move-object v1, v1

    .line 2601981
    sget-object v2, LX/2MK;->PHOTO:LX/2MK;

    .line 2601982
    iput-object v2, v1, LX/5zn;->c:LX/2MK;

    .line 2601983
    move-object v1, v1

    .line 2601984
    iput-object v0, v1, LX/5zn;->d:LX/5zj;

    .line 2601985
    move-object v0, v1

    .line 2601986
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2601987
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Ljava/util/List;)V

    .line 2601988
    :goto_5
    goto :goto_0

    .line 2601989
    :pswitch_5
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2601990
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->l(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    .line 2601991
    :goto_6
    goto :goto_0

    .line 2601992
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_9

    invoke-virtual {p3}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 2601993
    invoke-virtual {p3}, Landroid/content/Intent;->getClipData()Landroid/content/ClipData;

    move-result-object v1

    .line 2601994
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2601995
    const/4 v0, 0x0

    :goto_7
    invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 2601996
    invoke-virtual {v1, v0}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2601997
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 2601998
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2601999
    :goto_8
    move-object v0, v0

    .line 2602000
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2602001
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2602002
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->x:Landroid/content/ContentResolver;

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 2602003
    if-nez v1, :cond_3

    .line 2602004
    invoke-virtual {p3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    .line 2602005
    :cond_3
    if-nez v1, :cond_4

    .line 2602006
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2602007
    :cond_4
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v3

    .line 2602008
    iput-object v0, v3, LX/5zn;->b:Landroid/net/Uri;

    .line 2602009
    move-object v0, v3

    .line 2602010
    sget-object v3, LX/5zj;->GALLERY:LX/5zj;

    .line 2602011
    iput-object v3, v0, LX/5zn;->d:LX/5zj;

    .line 2602012
    move-object v0, v0

    .line 2602013
    if-eqz v1, :cond_6

    const-string v3, "image"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2602014
    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    .line 2602015
    iput-object v1, v0, LX/5zn;->c:LX/2MK;

    .line 2602016
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2602017
    const/4 v1, 0x0

    .line 2602018
    if-eqz v0, :cond_5

    iget-object v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    if-eqz v3, :cond_5

    .line 2602019
    :try_start_0
    invoke-static {v0}, LX/5zs;->from(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zs;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2602020
    const/4 v1, 0x1

    .line 2602021
    :cond_5
    :goto_9
    move v1, v1

    .line 2602022
    if-nez v1, :cond_7

    .line 2602023
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->t(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    goto/16 :goto_1

    .line 2602024
    :cond_6
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->y:LX/03V;

    sget-object v1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->m:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "unsupported/unknown media type returned from gallery"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2602025
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->t(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    goto/16 :goto_1

    .line 2602026
    :cond_7
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2602027
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2602028
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->b:Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    if-eqz v1, :cond_8

    .line 2602029
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 2602030
    :cond_8
    invoke-static {p0, v0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Ljava/util/List;)V

    goto/16 :goto_1

    .line 2602031
    :cond_9
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto/16 :goto_8

    :catch_0
    goto :goto_9

    .line 2602032
    :cond_a
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2602033
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->b:Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    if-eqz v1, :cond_b

    .line 2602034
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {p0, v0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Landroid/net/Uri;)V

    goto/16 :goto_2

    .line 2602035
    :cond_b
    invoke-static {p0, v0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Ljava/util/List;)V

    goto/16 :goto_2

    .line 2602036
    :cond_c
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->E:Landroid/net/Uri;

    .line 2602037
    iput-object v1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2602038
    move-object v0, v0

    .line 2602039
    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    .line 2602040
    iput-object v1, v0, LX/5zn;->c:LX/2MK;

    .line 2602041
    move-object v0, v0

    .line 2602042
    sget-object v1, LX/5zj;->CAMERA:LX/5zj;

    .line 2602043
    iput-object v1, v0, LX/5zn;->d:LX/5zj;

    .line 2602044
    move-object v0, v0

    .line 2602045
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2602046
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->a(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Ljava/util/List;)V

    goto/16 :goto_3

    .line 2602047
    :cond_d
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    goto/16 :goto_3

    .line 2602048
    :pswitch_6
    sget-object v0, LX/5zj;->GALLERY:LX/5zj;

    goto/16 :goto_4

    .line 2602049
    :pswitch_7
    sget-object v0, LX/5zj;->CAMERA:LX/5zj;

    goto/16 :goto_4

    .line 2602050
    :cond_e
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    goto/16 :goto_5

    .line 2602051
    :cond_f
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->s(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x268f85d6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2601950
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2601951
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2601952
    const v0, 0x1030010

    invoke-virtual {p0, v3, v0}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2601953
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2601954
    if-eqz v0, :cond_0

    .line 2601955
    const-string v2, "p"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    .line 2601956
    :cond_0
    if-eqz p1, :cond_1

    .line 2601957
    const-string v0, "tmp_camera_file"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->E:Landroid/net/Uri;

    .line 2601958
    const-string v0, "tmp_crop_file"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->F:Landroid/net/Uri;

    .line 2601959
    :cond_1
    const/16 v0, 0x2b

    const v2, -0xddc4537    # -3.2429994E30f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x18244c85

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2601946
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 2601947
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->H:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    .line 2601948
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->H:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2601949
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x1f274ae1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2601942
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2601943
    const-string v0, "tmp_camera_file"

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->E:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2601944
    const-string v0, "tmp_crop_file"

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->F:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2601945
    return-void
.end method
