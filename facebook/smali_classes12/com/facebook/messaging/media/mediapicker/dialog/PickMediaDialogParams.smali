.class public Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/Ih7;

.field public final b:Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

.field public final c:Z

.field public final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2MK;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final f:Z

.field public final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2602175
    new-instance v0, LX/Ih4;

    invoke-direct {v0}, LX/Ih4;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Ih6;)V
    .locals 1

    .prologue
    .line 2602176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2602177
    iget-object v0, p1, LX/Ih6;->a:LX/Ih7;

    move-object v0, v0

    .line 2602178
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2602179
    iget-object v0, p1, LX/Ih6;->d:Ljava/util/Set;

    move-object v0, v0

    .line 2602180
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2602181
    iget-object v0, p1, LX/Ih6;->a:LX/Ih7;

    move-object v0, v0

    .line 2602182
    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->a:LX/Ih7;

    .line 2602183
    iget-object v0, p1, LX/Ih6;->b:Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    move-object v0, v0

    .line 2602184
    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->b:Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    .line 2602185
    iget-boolean v0, p1, LX/Ih6;->c:Z

    move v0, v0

    .line 2602186
    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->c:Z

    .line 2602187
    iget-object v0, p1, LX/Ih6;->d:Ljava/util/Set;

    move-object v0, v0

    .line 2602188
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->d:LX/0Rf;

    .line 2602189
    iget-object v0, p1, LX/Ih6;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v0

    .line 2602190
    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2602191
    iget-boolean v0, p1, LX/Ih6;->f:Z

    move v0, v0

    .line 2602192
    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->f:Z

    .line 2602193
    iget-object v0, p1, LX/Ih6;->g:Ljava/util/ArrayList;

    move-object v0, v0

    .line 2602194
    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->g:Ljava/util/ArrayList;

    .line 2602195
    iget-object v0, p1, LX/Ih6;->h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 2602196
    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2602197
    iget-object v0, p1, LX/Ih6;->i:Ljava/lang/String;

    move-object v0, v0

    .line 2602198
    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->i:Ljava/lang/String;

    .line 2602199
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 2602200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2602201
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Ih7;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->a:LX/Ih7;

    .line 2602202
    const-class v0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->b:Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    .line 2602203
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->c:Z

    .line 2602204
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Rf;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->d:LX/0Rf;

    .line 2602205
    const-class v0, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2602206
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->f:Z

    .line 2602207
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2602208
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->i:Ljava/lang/String;

    .line 2602209
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 2602210
    if-lez v2, :cond_0

    .line 2602211
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->g:Ljava/util/ArrayList;

    .line 2602212
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2602213
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/4gF;

    .line 2602214
    sget-object v3, LX/Ih5;->a:[I

    invoke-virtual {v0}, LX/4gF;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    .line 2602215
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2602216
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->g:Ljava/util/ArrayList;

    sget-object v3, Lcom/facebook/photos/base/media/PhotoItem;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2602217
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->g:Ljava/util/ArrayList;

    sget-object v3, Lcom/facebook/photos/base/media/VideoItem;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2602218
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->g:Ljava/util/ArrayList;

    .line 2602219
    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2602220
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2602221
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->a:LX/Ih7;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2602222
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->b:Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2602223
    iget-boolean v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->c:Z

    invoke-static {p1, v1}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2602224
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->d:LX/0Rf;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2602225
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2602226
    iget-boolean v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->f:Z

    invoke-static {p1, v1}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2602227
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->h:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2602228
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2602229
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->g:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 2602230
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2602231
    :cond_0
    return-void

    .line 2602232
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2602233
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2602234
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2602235
    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/media/MediaItem;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2602236
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
