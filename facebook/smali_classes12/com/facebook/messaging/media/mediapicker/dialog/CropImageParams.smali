.class public Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2601853
    new-instance v0, LX/Igx;

    invoke-direct {v0}, LX/Igx;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2601860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2601861
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->a:I

    .line 2601862
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->b:I

    .line 2601863
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->c:I

    .line 2601864
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->d:I

    .line 2601865
    return-void
.end method

.method public static newBuilder()LX/Igy;
    .locals 1

    .prologue
    .line 2601866
    new-instance v0, LX/Igy;

    invoke-direct {v0}, LX/Igy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2601859
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2601854
    iget v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2601855
    iget v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2601856
    iget v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2601857
    iget v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2601858
    return-void
.end method
