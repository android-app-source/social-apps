.class public final Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;)V
    .locals 0

    .prologue
    .line 2601873
    iput-object p1, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment$1;->a:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2601874
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment$1;->a:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    .line 2601875
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->w:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->b()V

    .line 2601876
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->E:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 2601877
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->q:LX/2Ib;

    invoke-virtual {v1}, LX/2Ib;->c()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->E:Landroid/net/Uri;

    .line 2601878
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->C:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->b:Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    if-eqz v1, :cond_1

    .line 2601879
    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->F:Landroid/net/Uri;

    if-nez v1, :cond_1

    .line 2601880
    iget-object v2, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->r:LX/1Er;

    const-string v3, "crop"

    const-string v4, ".jpg"

    iget-object v1, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->D:LX/0Uh;

    const/16 v5, 0x268

    const/4 p0, 0x0

    invoke-virtual {v1, v5, p0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    :goto_0
    invoke-virtual {v2, v3, v4, v1}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v1

    .line 2601881
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->F:Landroid/net/Uri;

    .line 2601882
    :cond_1
    return-void

    .line 2601883
    :cond_2
    sget-object v1, LX/46h;->PREFER_SDCARD:LX/46h;

    goto :goto_0
.end method
