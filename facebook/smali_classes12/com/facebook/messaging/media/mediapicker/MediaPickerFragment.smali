.class public Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public final D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final E:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field public F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

.field public G:LX/Igi;

.field public H:LX/Igt;

.field public I:LX/IgV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:LX/3Rl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/photos/annotation/MaxNumberPhotosPerUpload;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/FGd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Igo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/messaging/media/loader/LocalMediaLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/6eS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Iht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/Ihz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Ii1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final m:LX/Igd;

.field private final n:LX/Igh;

.field public o:Landroid/widget/RadioButton;

.field public p:Landroid/widget/LinearLayout;

.field public q:Landroid/view/ViewGroup;

.field public r:Lcom/facebook/widget/CountBadge;

.field private s:Landroid/widget/ImageView;

.field public t:Landroid/support/v7/widget/RecyclerView;

.field public u:Landroid/widget/RadioButton;

.field public v:LX/Ign;

.field private w:LX/3wu;

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2601584
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2601585
    new-instance v0, LX/Igd;

    invoke-direct {v0, p0}, LX/Igd;-><init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->m:LX/Igd;

    .line 2601586
    new-instance v0, LX/Igh;

    invoke-direct {v0, p0}, LX/Igh;-><init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->n:LX/Igh;

    .line 2601587
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->D:Ljava/util/List;

    .line 2601588
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    .line 2601589
    sget-object v0, LX/Igi;->ALL:LX/Igi;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->G:LX/Igi;

    .line 2601590
    return-void
.end method

.method public static a(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;II)V
    .locals 1

    .prologue
    .line 2601591
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->w:LX/3wu;

    invoke-virtual {v0, p1, p2}, LX/1P1;->d(II)V

    .line 2601592
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2601593
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->a:LX/3Rl;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Click on "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/6FV;->MEDIA_PICKER:LX/6FV;

    invoke-virtual {v0, v1, v2}, LX/3Rl;->a(Ljava/lang/String;LX/6FV;)V

    .line 2601594
    return-void
.end method

.method public static b$redex0(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2601621
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->I:LX/IgV;

    if-nez v0, :cond_0

    .line 2601622
    :goto_0
    return-void

    .line 2601623
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2601624
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601625
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iget-object v4, v4, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->g:Ljava/lang/String;

    .line 2601626
    iput-object v4, v0, LX/5zn;->o:Ljava/lang/String;

    .line 2601627
    move-object v0, v0

    .line 2601628
    sget-object v4, LX/5zj;->MEDIA_PICKER_GALLERY:LX/5zj;

    .line 2601629
    iput-object v4, v0, LX/5zn;->d:LX/5zj;

    .line 2601630
    move-object v0, v0

    .line 2601631
    iget-object v4, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iget-object v4, v4, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2601632
    iput-object v4, v0, LX/5zn;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2601633
    move-object v0, v0

    .line 2601634
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2601635
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2601636
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->I:LX/IgV;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/IgV;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public static e$redex0(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V
    .locals 7

    .prologue
    .line 2601595
    const-string v0, "post_photo_button"

    invoke-static {p0, v0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->a$redex0(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;Ljava/lang/String;)V

    .line 2601596
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    invoke-virtual {v0}, LX/Igt;->b()I

    move-result v0

    if-nez v0, :cond_1

    .line 2601597
    :cond_0
    :goto_0
    return-void

    .line 2601598
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 2601599
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v2

    .line 2601600
    :goto_1
    move-object v0, v1

    .line 2601601
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->b$redex0(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;LX/0Px;)V

    goto :goto_0

    .line 2601602
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    invoke-virtual {v1}, LX/Igt;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 2601603
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->E:Ljava/util/Map;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601604
    if-eqz v1, :cond_3

    .line 2601605
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    move-object v1, v2

    .line 2601606
    goto :goto_1
.end method

.method public static n(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2601607
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->t:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2601608
    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->w:LX/3wu;

    invoke-virtual {v2}, LX/1P1;->l()I

    move-result v2

    .line 2601609
    if-nez v1, :cond_1

    .line 2601610
    :goto_0
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->G:LX/Igi;

    sget-object v3, LX/Igi;->SELECTED:LX/Igi;

    if-ne v1, v3, :cond_2

    .line 2601611
    iput v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->z:I

    .line 2601612
    iput v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->B:I

    .line 2601613
    :cond_0
    :goto_1
    return-void

    .line 2601614
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0

    .line 2601615
    :cond_2
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->G:LX/Igi;

    sget-object v3, LX/Igi;->ALL:LX/Igi;

    if-ne v1, v3, :cond_0

    .line 2601616
    iput v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->A:I

    .line 2601617
    iput v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->C:I

    goto :goto_1
.end method

.method public static o(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V
    .locals 2

    .prologue
    .line 2601618
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->r:Lcom/facebook/widget/CountBadge;

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    invoke-virtual {v1}, LX/Igt;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CountBadge;->setCount(I)V

    .line 2601619
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->q(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    .line 2601620
    return-void
.end method

.method public static q(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2601549
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    invoke-virtual {v0}, LX/Igt;->b()I

    move-result v4

    .line 2601550
    if-lez v4, :cond_0

    move v0, v1

    .line 2601551
    :goto_0
    iget-object v3, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 2601552
    iget-object v5, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->s:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    const/high16 v3, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 2601553
    iget-object v3, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->s:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f013f

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v2

    invoke-virtual {v5, v6, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2601554
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->r:Lcom/facebook/widget/CountBadge;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CountBadge;->setEnabled(Z)V

    .line 2601555
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->r:Lcom/facebook/widget/CountBadge;

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CountBadge;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2601556
    return-void

    :cond_0
    move v0, v2

    .line 2601557
    goto :goto_0

    .line 2601558
    :cond_1
    const/high16 v3, 0x3f000000    # 0.5f

    goto :goto_1

    .line 2601559
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082e04

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2601560
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2601561
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-static {v0}, LX/3Rl;->a(LX/0QB;)LX/3Rl;

    move-result-object v3

    check-cast v3, LX/3Rl;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const/16 v6, 0x15d6

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/FGd;->b(LX/0QB;)LX/FGd;

    move-result-object v7

    check-cast v7, LX/FGd;

    const-class v8, LX/Igo;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Igo;

    invoke-static {v0}, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->b(LX/0QB;)Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    move-result-object v9

    check-cast v9, Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    invoke-static {v0}, LX/6eS;->b(LX/0QB;)LX/6eS;

    move-result-object v10

    check-cast v10, LX/6eS;

    invoke-static {v0}, LX/Iht;->b(LX/0QB;)LX/Iht;

    move-result-object v11

    check-cast v11, LX/Iht;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    invoke-static {v0}, LX/Ihz;->b(LX/0QB;)LX/Ihz;

    move-result-object v13

    check-cast v13, LX/Ihz;

    invoke-static {v0}, LX/Ii1;->b(LX/0QB;)LX/Ii1;

    move-result-object v0

    check-cast v0, LX/Ii1;

    iput-object v3, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->a:LX/3Rl;

    iput-object v4, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->b:Ljava/util/concurrent/Executor;

    iput-object v5, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->c:LX/03V;

    iput-object v6, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->d:LX/0Or;

    iput-object v7, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->e:LX/FGd;

    iput-object v8, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->f:LX/Igo;

    iput-object v9, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->g:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    iput-object v10, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->h:LX/6eS;

    iput-object v11, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->i:LX/Iht;

    iput-object v12, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->j:LX/0kL;

    iput-object v13, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->k:LX/Ihz;

    iput-object v0, v2, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->l:LX/Ii1;

    .line 2601562
    if-nez p1, :cond_2

    .line 2601563
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2601564
    const-string v2, "extra_environment"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iput-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601565
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    if-nez v1, :cond_0

    .line 2601566
    sget-object v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iput-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601567
    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2601568
    const-string v2, "extra_selection"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2601569
    if-eqz v1, :cond_1

    .line 2601570
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/media/MediaItem;

    .line 2601571
    iget-object v3, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->D:Ljava/util/List;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    .line 2601572
    iget-wide v7, v1, Lcom/facebook/ipc/media/MediaIdKey;->b:J

    move-wide v5, v7

    .line 2601573
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2601574
    :cond_1
    :goto_1
    return-void

    .line 2601575
    :cond_2
    const/4 v2, 0x0

    .line 2601576
    const-string v1, "environment"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iput-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601577
    const-string v1, "grid_view_scroll_position"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->x:I

    .line 2601578
    const-string v1, "grid_view_scroll_offset"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->y:I

    .line 2601579
    const-string v1, "selected_mode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LX/Igi;

    iput-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->G:LX/Igi;

    .line 2601580
    const-string v1, "selection"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v3

    array-length v4, v3

    move v1, v2

    :goto_2
    if-ge v1, v4, :cond_3

    aget-wide v5, v3, v1

    .line 2601581
    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->D:Ljava/util/List;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2601582
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2601583
    :cond_3
    goto :goto_1
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 2601472
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 2601473
    instance-of v0, p1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    if-eqz v0, :cond_0

    .line 2601474
    check-cast p1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->n:LX/Igh;

    .line 2601475
    iput-object v0, p1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->w:LX/Igg;

    .line 2601476
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x3fe3fe19

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2601437
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 2601438
    const v2, 0x7f0d1b1a

    if-ne v1, v2, :cond_1

    .line 2601439
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->e$redex0(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    .line 2601440
    :cond_0
    :goto_0
    const v1, -0x362260bf

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2601441
    :cond_1
    const v2, 0x7f0d1b18

    if-ne v1, v2, :cond_2

    .line 2601442
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->n(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    .line 2601443
    const-string v1, "all_tab"

    invoke-static {p0, v1}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->a$redex0(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;Ljava/lang/String;)V

    .line 2601444
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->o:Landroid/widget/RadioButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 2601445
    sget-object v1, LX/Igi;->ALL:LX/Igi;

    iput-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->G:LX/Igi;

    .line 2601446
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->t:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2601447
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->q:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2601448
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->v:LX/Ign;

    sget-object v2, LX/Igi;->ALL:LX/Igi;

    invoke-virtual {v1, v2}, LX/Ign;->a(LX/Igi;)V

    .line 2601449
    iget v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->A:I

    iget v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->C:I

    invoke-static {p0, v1, v2}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->a(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;II)V

    .line 2601450
    goto :goto_0

    .line 2601451
    :cond_2
    const v2, 0x7f0d1b19

    if-ne v1, v2, :cond_0

    .line 2601452
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->n(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    .line 2601453
    const-string v1, "selected_tab"

    invoke-static {p0, v1}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->a$redex0(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;Ljava/lang/String;)V

    .line 2601454
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->u:Landroid/widget/RadioButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 2601455
    sget-object v1, LX/Igi;->SELECTED:LX/Igi;

    iput-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->G:LX/Igi;

    .line 2601456
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    invoke-virtual {v1}, LX/Igt;->b()I

    move-result v1

    if-lez v1, :cond_3

    .line 2601457
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->v:LX/Ign;

    sget-object v2, LX/Igi;->SELECTED:LX/Igi;

    invoke-virtual {v1, v2}, LX/Ign;->a(LX/Igi;)V

    .line 2601458
    iget v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->z:I

    iget v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->B:I

    invoke-static {p0, v1, v2}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->a(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;II)V

    .line 2601459
    :goto_1
    goto :goto_0

    .line 2601460
    :cond_3
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->q:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2601461
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->t:Landroid/support/v7/widget/RecyclerView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x458ce1c4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2601462
    const v1, 0x7f030a95

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1f3f28ab

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x2

    const/16 v2, 0x2a

    const v3, 0x2f507a08

    invoke-static {v1, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2601463
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2601464
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2601465
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v3, v3

    .line 2601466
    invoke-static {v2, v3}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 2601467
    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->w:LX/3wu;

    invoke-virtual {v2}, LX/1P1;->l()I

    move-result v2

    iput v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->x:I

    .line 2601468
    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->t:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2601469
    if-nez v2, :cond_0

    :goto_0
    iput v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->y:I

    .line 2601470
    const v0, 0x618ab24b

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2601471
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5d3fd72d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2601477
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2601478
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->r:Lcom/facebook/widget/CountBadge;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CountBadge;->setVisibility(I)V

    .line 2601479
    const/16 v1, 0x2b

    const v2, -0x16f6d524

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2601534
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2601535
    const-string v0, "environment"

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2601536
    const-string v0, "grid_view_scroll_position"

    iget v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->x:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2601537
    const-string v0, "grid_view_scroll_offset"

    iget v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->y:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2601538
    const-string v0, "selected_mode"

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->G:LX/Igi;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2601539
    const-string v0, "selection"

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    .line 2601540
    iget-object v2, v1, LX/Igt;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v4, v2, [J

    .line 2601541
    const/4 v2, 0x0

    .line 2601542
    iget-object v3, v1, LX/Igt;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2601543
    aput-wide v6, v4, v3

    .line 2601544
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    .line 2601545
    goto :goto_0

    .line 2601546
    :cond_0
    move-object v1, v4

    .line 2601547
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 2601548
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x67b2edf7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2601480
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2601481
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v2

    .line 2601482
    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601483
    iget-boolean v3, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->d:Z

    move v0, v3

    .line 2601484
    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 2601485
    :goto_0
    iget-object v3, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->g:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    iget-object v4, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->m:LX/Igd;

    invoke-virtual {v3, v4}, LX/6Lb;->a(LX/3Mb;)V

    .line 2601486
    iget-object v3, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->g:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    invoke-static {}, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->a()LX/IgU;

    move-result-object v4

    .line 2601487
    iput-boolean v0, v4, LX/IgU;->a:Z

    .line 2601488
    move-object v0, v4

    .line 2601489
    iput-boolean v2, v0, LX/IgU;->c:Z

    .line 2601490
    move-object v0, v0

    .line 2601491
    invoke-virtual {v0}, LX/IgU;->e()Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/6Lb;->a(Ljava/lang/Object;)V

    .line 2601492
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601493
    iget-boolean v2, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->e:Z

    move v0, v2

    .line 2601494
    if-eqz v0, :cond_1

    .line 2601495
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->s:Landroid/widget/ImageView;

    const v2, 0x7f020ec0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2601496
    :cond_1
    const v0, 0x15a02009

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2601497
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7d693467

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2601498
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2601499
    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->g:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    invoke-virtual {v1}, LX/6Lb;->a()V

    .line 2601500
    const/16 v1, 0x2b

    const v2, -0x2b09890d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 10
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2601501
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2601502
    const v0, 0x7f0d1b18

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->o:Landroid/widget/RadioButton;

    .line 2601503
    const v0, 0x7f0d1b15

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->p:Landroid/widget/LinearLayout;

    .line 2601504
    const v0, 0x7f0d1b12

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->q:Landroid/view/ViewGroup;

    .line 2601505
    const v0, 0x7f0d1b1b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CountBadge;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->r:Lcom/facebook/widget/CountBadge;

    .line 2601506
    const v0, 0x7f0d1b1a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->s:Landroid/widget/ImageView;

    .line 2601507
    const v0, 0x7f0d1b11

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->t:Landroid/support/v7/widget/RecyclerView;

    .line 2601508
    const v0, 0x7f0d1b19

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->u:Landroid/widget/RadioButton;

    .line 2601509
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->o:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2601510
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2601511
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->u:Landroid/widget/RadioButton;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2601512
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->t:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/3wu;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->w:LX/3wu;

    .line 2601513
    new-instance v1, LX/Igt;

    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v1, v0}, LX/Igt;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    .line 2601514
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    new-instance v1, LX/Igf;

    invoke-direct {v1, p0}, LX/Igf;-><init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    invoke-virtual {v0, v1}, LX/Igt;->a(LX/Ige;)V

    .line 2601515
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->f:LX/Igo;

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601516
    new-instance v4, LX/Ign;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    const-class v7, LX/Igs;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Igs;

    const-class v8, LX/Igw;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Igw;

    move-object v9, v1

    invoke-direct/range {v4 .. v9}, LX/Ign;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;LX/Igs;LX/Igw;Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;)V

    .line 2601517
    move-object v0, v4

    .line 2601518
    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->v:LX/Ign;

    .line 2601519
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->t:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->v:LX/Ign;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2601520
    iget v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->x:I

    iget v1, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->y:I

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->a(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;II)V

    .line 2601521
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601522
    iget-boolean v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    move v0, v1

    .line 2601523
    if-eqz v0, :cond_1

    .line 2601524
    const v0, 0x7f0d1b16

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2601525
    const v0, 0x7f0d1b17

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2601526
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->t:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 2601527
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->r:Lcom/facebook/widget/CountBadge;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CountBadge;->setVisibility(I)V

    .line 2601528
    sget-object v0, LX/Igi;->ALL:LX/Igi;

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->G:LX/Igi;

    .line 2601529
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2601530
    :cond_0
    :goto_0
    return-void

    .line 2601531
    :cond_1
    invoke-static {p0}, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->q(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;)V

    .line 2601532
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->G:LX/Igi;

    sget-object v1, LX/Igi;->SELECTED:LX/Igi;

    if-ne v0, v1, :cond_0

    .line 2601533
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->u:Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0
.end method
