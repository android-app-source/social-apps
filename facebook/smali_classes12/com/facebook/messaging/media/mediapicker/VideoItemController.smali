.class public Lcom/facebook/messaging/media/mediapicker/VideoItemController;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final m:LX/Igu;

.field public final n:LX/Igv;

.field public final o:LX/1Ad;

.field public final p:Landroid/content/res/Resources;

.field public final q:LX/11S;

.field public r:LX/Igm;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/ui/media/attachments/MediaResource;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public u:LX/1o9;

.field public v:Landroid/view/View;

.field public w:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2601839
    const-class v0, Lcom/facebook/messaging/media/mediapicker/VideoItemController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Landroid/content/res/Resources;LX/11S;Landroid/view/View;)V
    .locals 2
    .param p4    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2601840
    invoke-direct {p0, p4}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2601841
    new-instance v0, LX/Igu;

    invoke-direct {v0, p0}, LX/Igu;-><init>(Lcom/facebook/messaging/media/mediapicker/VideoItemController;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->m:LX/Igu;

    .line 2601842
    new-instance v0, LX/Igv;

    invoke-direct {v0, p0}, LX/Igv;-><init>(Lcom/facebook/messaging/media/mediapicker/VideoItemController;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->n:LX/Igv;

    .line 2601843
    iput-object p4, p0, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->w:Landroid/view/View;

    .line 2601844
    iput-object p1, p0, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->o:LX/1Ad;

    .line 2601845
    iput-object p2, p0, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->p:Landroid/content/res/Resources;

    .line 2601846
    iput-object p3, p0, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->q:LX/11S;

    .line 2601847
    return-void
.end method
