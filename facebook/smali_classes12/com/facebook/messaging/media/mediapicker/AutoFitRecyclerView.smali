.class public Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source ""


# instance fields
.field private h:I

.field private i:LX/3wu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2601172
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 2601173
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->h:I

    .line 2601174
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2601175
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2601176
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2601177
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->h:I

    .line 2601178
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2601179
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2601180
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2601181
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->h:I

    .line 2601182
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2601183
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2601184
    if-eqz p2, :cond_0

    .line 2601185
    new-array v0, v3, [I

    const v1, 0x1010117

    aput v1, v0, v2

    .line 2601186
    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2601187
    const/4 v1, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->h:I

    .line 2601188
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2601189
    :cond_0
    new-instance v0, LX/3wu;

    invoke-virtual {p0}, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v3}, LX/3wu;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->i:LX/3wu;

    .line 2601190
    iget-object v0, p0, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->i:LX/3wu;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2601191
    return-void
.end method


# virtual methods
.method public final onSizeChanged(IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x29144763

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2601192
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView;->onSizeChanged(IIII)V

    .line 2601193
    iget v1, p0, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->h:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2601194
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->h:I

    div-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2601195
    iget-object v2, p0, Lcom/facebook/messaging/media/mediapicker/AutoFitRecyclerView;->i:LX/3wu;

    invoke-virtual {v2, v1}, LX/3wu;->a(I)V

    .line 2601196
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x1e617f8f

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
