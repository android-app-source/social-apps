.class public Lcom/facebook/messaging/media/ui/MediaFabView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Landroid/animation/ValueAnimator;

.field public b:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private c:I

.field public d:Lcom/facebook/uicontrib/fab/FabView;

.field private e:Landroid/widget/TextView;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2603444
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2603445
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->a:Landroid/animation/ValueAnimator;

    .line 2603446
    return-void

    .line 2603447
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 2603440
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2603441
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->a:Landroid/animation/ValueAnimator;

    .line 2603442
    return-void

    .line 2603443
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2603420
    const-class v0, Lcom/facebook/messaging/media/ui/MediaFabView;

    invoke-static {v0, p0}, Lcom/facebook/messaging/media/ui/MediaFabView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2603421
    const v0, 0x7f030aa7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2603422
    const v0, 0x7f0d0f65

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    iput-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->d:Lcom/facebook/uicontrib/fab/FabView;

    .line 2603423
    const v0, 0x7f0d1b3c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->e:Landroid/widget/TextView;

    .line 2603424
    iget-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->d:Lcom/facebook/uicontrib/fab/FabView;

    new-instance v1, LX/Ii8;

    invoke-direct {v1, p0}, LX/Ii8;-><init>(Lcom/facebook/messaging/media/ui/MediaFabView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2603425
    iget-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->b:LX/0wM;

    const v1, 0x7f020eb8

    invoke-virtual {p0}, Lcom/facebook/messaging/media/ui/MediaFabView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x106000b

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v1, v2, v5}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/RotateDrawable;

    .line 2603426
    iget-object v1, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->b:LX/0wM;

    const v2, 0x7f020eb7

    invoke-virtual {p0}, Lcom/facebook/messaging/media/ui/MediaFabView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a019a

    invoke-static {v3, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v1, v2, v3, v5}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/RotateDrawable;

    .line 2603427
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    aput-object v1, v2, v5

    aput-object v0, v2, v6

    .line 2603428
    iget-object v3, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->d:Lcom/facebook/uicontrib/fab/FabView;

    new-instance v4, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v4, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v3, v4}, Lcom/facebook/uicontrib/fab/FabView;->setGlyphDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2603429
    iget-object v2, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->a:Landroid/animation/ValueAnimator;

    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2603430
    iget-object v2, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->a:Landroid/animation/ValueAnimator;

    new-instance v3, LX/Ii9;

    invoke-direct {v3, p0, v0, v1}, LX/Ii9;-><init>(Lcom/facebook/messaging/media/ui/MediaFabView;Landroid/graphics/drawable/RotateDrawable;Landroid/graphics/drawable/RotateDrawable;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2603431
    iget v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->f:I

    if-nez v0, :cond_1

    .line 2603432
    iget-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->a:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 2603433
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2603434
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/media/ui/MediaFabView;->a(Ljava/util/Set;)V

    .line 2603435
    :cond_0
    :goto_0
    return-void

    .line 2603436
    :cond_1
    iget v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->f:I

    if-ne v0, v6, :cond_0

    .line 2603437
    iget-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 2603438
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2603439
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/media/ui/MediaFabView;->a(Ljava/util/Set;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/media/ui/MediaFabView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/media/ui/MediaFabView;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->b:LX/0wM;

    return-void
.end method

.method private b(Ljava/util/Set;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 2603386
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2603387
    invoke-virtual {p0}, Lcom/facebook/messaging/media/ui/MediaFabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080691

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2603388
    :goto_0
    return-object v0

    .line 2603389
    :cond_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    move v3, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2603390
    iget-object v5, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v6, LX/2MK;->PHOTO:LX/2MK;

    if-ne v5, v6, :cond_1

    .line 2603391
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2603392
    :cond_1
    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v5, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v5, :cond_7

    .line 2603393
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 2603394
    goto :goto_1

    .line 2603395
    :cond_2
    if-nez v1, :cond_3

    .line 2603396
    invoke-virtual {p0}, Lcom/facebook/messaging/media/ui/MediaFabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f002e

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2603397
    :cond_3
    if-nez v3, :cond_4

    .line 2603398
    invoke-virtual {p0}, Lcom/facebook/messaging/media/ui/MediaFabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f002f

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2603399
    :cond_4
    if-le v1, v7, :cond_5

    .line 2603400
    invoke-virtual {p0}, Lcom/facebook/messaging/media/ui/MediaFabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0f0030

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v7

    invoke-virtual {v0, v4, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2603401
    :cond_5
    if-le v3, v7, :cond_6

    .line 2603402
    invoke-virtual {p0}, Lcom/facebook/messaging/media/ui/MediaFabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0f0031

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v7

    invoke-virtual {v0, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2603403
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/messaging/media/ui/MediaFabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080698

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto/16 :goto_2
.end method


# virtual methods
.method public final a(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2603407
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v1

    .line 2603408
    iget v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->c:I

    if-nez v0, :cond_2

    if-lez v1, :cond_2

    .line 2603409
    iget v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->f:I

    if-nez v0, :cond_1

    .line 2603410
    iget-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2603411
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->e:Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2603412
    iget-object v2, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->e:Landroid/widget/TextView;

    if-lez v1, :cond_3

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2603413
    iget-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->d:Lcom/facebook/uicontrib/fab/FabView;

    invoke-direct {p0, p1}, Lcom/facebook/messaging/media/ui/MediaFabView;->b(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/uicontrib/fab/FabView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2603414
    iput v1, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->c:I

    .line 2603415
    return-void

    .line 2603416
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    goto :goto_0

    .line 2603417
    :cond_2
    iget v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->c:I

    if-lez v0, :cond_0

    if-nez v1, :cond_0

    .line 2603418
    iget-object v0, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    goto :goto_0

    .line 2603419
    :cond_3
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public setDisplayMode(I)V
    .locals 0

    .prologue
    .line 2603404
    iput p1, p0, Lcom/facebook/messaging/media/ui/MediaFabView;->f:I

    .line 2603405
    invoke-direct {p0}, Lcom/facebook/messaging/media/ui/MediaFabView;->a()V

    .line 2603406
    return-void
.end method
