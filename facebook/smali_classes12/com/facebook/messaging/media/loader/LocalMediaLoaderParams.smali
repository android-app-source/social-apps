.class public final Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:I

.field public d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2601167
    new-instance v0, LX/IgT;

    invoke-direct {v0}, LX/IgT;-><init>()V

    sput-object v0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/IgU;)V
    .locals 1

    .prologue
    .line 2601157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2601158
    iget-boolean v0, p1, LX/IgU;->a:Z

    move v0, v0

    .line 2601159
    iput-boolean v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->a:Z

    .line 2601160
    iget v0, p1, LX/IgU;->b:I

    move v0, v0

    .line 2601161
    iput v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->c:I

    .line 2601162
    iget-boolean v0, p1, LX/IgU;->c:Z

    move v0, v0

    .line 2601163
    iput-boolean v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->b:Z

    .line 2601164
    iget-object v0, p1, LX/IgU;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2601165
    iput-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->d:Ljava/lang/String;

    .line 2601166
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2601151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2601152
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->a:Z

    .line 2601153
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->b:Z

    .line 2601154
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->c:I

    .line 2601155
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->d:Ljava/lang/String;

    .line 2601156
    return-void
.end method

.method public static a()LX/IgU;
    .locals 1

    .prologue
    .line 2601168
    new-instance v0, LX/IgU;

    invoke-direct {v0}, LX/IgU;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2601150
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2601145
    iget-boolean v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2601146
    iget-boolean v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2601147
    iget v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2601148
    iget-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2601149
    return-void
.end method
