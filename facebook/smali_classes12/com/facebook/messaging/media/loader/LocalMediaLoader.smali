.class public Lcom/facebook/messaging/media/loader/LocalMediaLoader;
.super LX/6Lb;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6Lb",
        "<",
        "Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;",
        "LX/0Px",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaResource;",
        ">;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:LX/0aG;

.field private final d:Ljava/util/concurrent/Executor;

.field public final e:LX/03V;

.field private final f:LX/IgS;

.field public g:Landroid/os/Bundle;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2601140
    const-class v0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    sput-object v0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->a:Ljava/lang/Class;

    .line 2601141
    const-class v0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/03V;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2601122
    invoke-direct {p0, p4}, LX/6Lb;-><init>(Ljava/util/concurrent/Executor;)V

    .line 2601123
    new-instance v0, LX/IgS;

    invoke-direct {v0, p0}, LX/IgS;-><init>(Lcom/facebook/messaging/media/loader/LocalMediaLoader;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->f:LX/IgS;

    .line 2601124
    iput-object p1, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->c:LX/0aG;

    .line 2601125
    iput-object p2, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->e:LX/03V;

    .line 2601126
    iput-object p3, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->d:Ljava/util/concurrent/Executor;

    .line 2601127
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/media/loader/LocalMediaLoader;
    .locals 1

    .prologue
    .line 2601139
    invoke-static {p0}, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->b(LX/0QB;)Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/messaging/media/loader/LocalMediaLoader;
    .locals 5

    .prologue
    .line 2601137
    new-instance v4, Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/facebook/messaging/media/loader/LocalMediaLoader;-><init>(LX/0aG;LX/03V;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    .line 2601138
    return-object v4
.end method

.method public static b(Lcom/facebook/messaging/media/loader/LocalMediaLoader;Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2601130
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->g:Landroid/os/Bundle;

    .line 2601131
    iget-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->g:Landroid/os/Bundle;

    const-string v1, "hideVideos"

    iget-boolean v2, p1, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->a:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2601132
    iget-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->g:Landroid/os/Bundle;

    const-string v1, "hideGifs"

    iget-boolean v2, p1, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->b:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2601133
    iget-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->g:Landroid/os/Bundle;

    const-string v1, "mediaLimit"

    iget v2, p1, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2601134
    iget-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->g:Landroid/os/Bundle;

    const-string v1, "folderId"

    iget-object v2, p1, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2601135
    iget-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->c:LX/0aG;

    const-string v1, "load_local_media"

    iget-object v2, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->g:Landroid/os/Bundle;

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x36406112

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2601136
    iget-object v1, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->f:LX/IgS;

    iget-object v2, p0, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 2601129
    check-cast p1, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;

    invoke-static {p0, p1}, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->b(Lcom/facebook/messaging/media/loader/LocalMediaLoader;Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)LX/6LZ;
    .locals 1

    .prologue
    .line 2601128
    sget-object v0, LX/6Lb;->a:LX/6LZ;

    return-object v0
.end method
