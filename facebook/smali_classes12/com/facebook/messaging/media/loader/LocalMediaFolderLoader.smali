.class public Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;
.super LX/6Lb;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6Lb",
        "<",
        "Lcom/facebook/messaging/media/folder/LoadFolderParams;",
        "LX/0Px",
        "<",
        "Lcom/facebook/messaging/media/folder/Folder;",
        ">;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0aG;

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:LX/IgR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2601098
    const-class v0, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0aG;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 2
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2601099
    invoke-direct {p0, p3}, LX/6Lb;-><init>(Ljava/util/concurrent/Executor;)V

    .line 2601100
    new-instance v0, LX/IgR;

    invoke-direct {v0}, LX/IgR;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;->d:LX/IgR;

    .line 2601101
    iput-object p1, p0, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;->b:LX/0aG;

    .line 2601102
    iput-object p2, p0, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;->c:Ljava/util/concurrent/Executor;

    .line 2601103
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 2601104
    check-cast p1, Lcom/facebook/messaging/media/folder/LoadFolderParams;

    .line 2601105
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2601106
    sget-object v0, Lcom/facebook/messaging/media/folder/LoadFolderParams;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2601107
    iget-object v0, p0, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;->b:LX/0aG;

    const-string v1, "load_local_folders"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    const v5, 0x451343a0

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2601108
    iget-object v1, p0, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;->d:LX/IgR;

    iget-object v2, p0, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)LX/6LZ;
    .locals 1

    .prologue
    .line 2601109
    sget-object v0, LX/6Lb;->a:LX/6LZ;

    return-object v0
.end method
