.class public Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/IhJ;

.field public b:LX/IhL;

.field public c:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;

.field public d:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

.field public e:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public f:Landroid/support/v4/view/ViewPager;

.field public final g:LX/IhR;

.field public final h:LX/IhS;

.field public i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2602691
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2602692
    new-instance v0, LX/IhR;

    invoke-direct {v0, p0}, LX/IhR;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->g:LX/IhR;

    .line 2602693
    new-instance v0, LX/IhS;

    invoke-direct {v0, p0}, LX/IhS;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->h:LX/IhS;

    .line 2602694
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2602687
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->d:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    if-eqz v0, :cond_0

    .line 2602688
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->d:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->a(Ljava/util/ArrayList;)V

    .line 2602689
    :goto_0
    return-void

    .line 2602690
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->i:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x30a23689

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2602686
    const v1, 0x7f030a9b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x69072ed9

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2602678
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2602679
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2602680
    const-string v1, "extra_environment"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->j:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602681
    const v0, 0x7f0d1b25

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->f:Landroid/support/v4/view/ViewPager;

    .line 2602682
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->f:Landroid/support/v4/view/ViewPager;

    new-instance v1, LX/IhT;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object p1

    invoke-direct {v1, p0, p1}, LX/IhT;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;LX/0gc;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2602683
    const v0, 0x7f0d1b24

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->e:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2602684
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->e:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2602685
    return-void
.end method
