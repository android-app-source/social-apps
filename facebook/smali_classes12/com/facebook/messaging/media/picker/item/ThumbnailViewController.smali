.class public Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final a:LX/4ob;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/animation/ValueAnimator;

.field public final d:LX/Iho;

.field public e:LX/1Ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:Landroid/content/res/Resources;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/11S;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public i:Z

.field public j:Z

.field public k:LX/IhC;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/ui/media/attachments/MediaResource;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/1o9;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2603026
    const-class v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2603027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2603028
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->c:Landroid/animation/ValueAnimator;

    .line 2603029
    new-instance v0, LX/Iho;

    invoke-direct {v0, p0}, LX/Iho;-><init>(Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->d:LX/Iho;

    .line 2603030
    const v0, 0x7f0d1b29

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2603031
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/Ihm;

    invoke-direct {v1, p0}, LX/Ihm;-><init>(Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2603032
    const v0, 0x7f0d0ab2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    invoke-static {v0}, LX/4ob;->a(Landroid/support/v7/internal/widget/ViewStubCompat;)LX/4ob;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->a:LX/4ob;

    .line 2603033
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->c:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2603034
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;->c:Landroid/animation/ValueAnimator;

    new-instance v1, LX/Ihn;

    invoke-direct {v1, p0}, LX/Ihn;-><init>(Lcom/facebook/messaging/media/picker/item/ThumbnailViewController;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2603035
    return-void

    .line 2603036
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f6b851f    # 0.92f
    .end array-data
.end method
