.class public Lcom/facebook/messaging/media/picker/FolderItemViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final l:Lcom/facebook/common/callercontext/CallerContext;

.field public final m:LX/1Ad;

.field public final n:Landroid/content/res/Resources;

.field public final o:LX/1o9;

.field public final p:Landroid/view/View;

.field public final q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final r:Landroid/widget/TextView;

.field public final s:Landroid/widget/TextView;

.field public t:LX/IhY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Lcom/facebook/messaging/media/folder/Folder;


# direct methods
.method public constructor <init>(LX/1Ad;Landroid/content/res/Resources;Landroid/view/View;LX/1o9;)V
    .locals 2
    .param p3    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1o9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2602270
    invoke-direct {p0, p3}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2602271
    const-class v0, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    .line 2602272
    iput-object p3, p0, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->p:Landroid/view/View;

    .line 2602273
    iput-object p1, p0, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->m:LX/1Ad;

    .line 2602274
    iput-object p2, p0, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->n:Landroid/content/res/Resources;

    .line 2602275
    iput-object p4, p0, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->o:LX/1o9;

    .line 2602276
    const v0, 0x7f0d1b1c

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2602277
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2602278
    const v0, 0x7f0d1b1d

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->r:Landroid/widget/TextView;

    .line 2602279
    const v0, 0x7f0d1b1e

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->s:Landroid/widget/TextView;

    .line 2602280
    return-void
.end method
