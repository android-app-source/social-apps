.class public Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IhZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

.field public d:LX/IhR;

.field public e:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2602621
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2602622
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2602618
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2602619
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;

    new-instance v1, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;

    invoke-static {v3}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v2

    check-cast v2, LX/0aG;

    invoke-static {v3}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {v3}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/Executor;

    invoke-direct {v1, v2, v4, p1}, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;-><init>(LX/0aG;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    move-object v2, v1

    check-cast v2, Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;

    new-instance v1, LX/IhZ;

    invoke-direct {v1}, LX/IhZ;-><init>()V

    invoke-static {v3}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    const-class p1, LX/Ih9;

    invoke-interface {v3, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/Ih9;

    invoke-static {v3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v4, v1, LX/IhZ;->a:Landroid/view/LayoutInflater;

    iput-object p1, v1, LX/IhZ;->b:LX/Ih9;

    iput-object v0, v1, LX/IhZ;->c:Landroid/content/res/Resources;

    move-object v3, v1

    check-cast v3, LX/IhZ;

    iput-object v2, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;

    iput-object v3, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->b:LX/IhZ;

    .line 2602620
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x775fe6ee

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2602617
    const v1, 0x7f030a9a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1c2ba64

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x65c53aff

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2602614
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2602615
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->e:Landroid/support/v7/widget/RecyclerView;

    .line 2602616
    const/16 v1, 0x2b

    const v2, 0x1326587d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3ae39c59

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2602596
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2602597
    const/4 v1, 0x0

    .line 2602598
    iget-object v2, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;

    new-instance v4, LX/IhP;

    invoke-direct {v4, p0}, LX/IhP;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;)V

    invoke-virtual {v2, v4}, LX/6Lb;->a(LX/3Mb;)V

    .line 2602599
    iget-object v2, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;

    .line 2602600
    new-instance v4, LX/IgN;

    invoke-direct {v4}, LX/IgN;-><init>()V

    move-object v4, v4

    .line 2602601
    iget-object v5, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->c:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602602
    iget-object v6, v5, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v5, v6

    .line 2602603
    invoke-static {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v5

    .line 2602604
    iput-boolean v5, v4, LX/IgN;->a:Z

    .line 2602605
    move-object v4, v4

    .line 2602606
    iget-object v5, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->c:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602607
    iget-boolean v6, v5, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->d:Z

    move v5, v6

    .line 2602608
    if-nez v5, :cond_0

    const/4 v1, 0x1

    .line 2602609
    :cond_0
    iput-boolean v1, v4, LX/IgN;->b:Z

    .line 2602610
    move-object v1, v4

    .line 2602611
    new-instance v4, Lcom/facebook/messaging/media/folder/LoadFolderParams;

    invoke-direct {v4, v1}, Lcom/facebook/messaging/media/folder/LoadFolderParams;-><init>(LX/IgN;)V

    move-object v1, v4

    .line 2602612
    invoke-virtual {v2, v1}, LX/6Lb;->a(Ljava/lang/Object;)V

    .line 2602613
    const/16 v1, 0x2b

    const v2, -0x12859620

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1c8aeda0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2602582
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2602583
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;

    invoke-virtual {v1}, LX/6Lb;->a()V

    .line 2602584
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaFolderLoader;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/6Lb;->a(LX/3Mb;)V

    .line 2602585
    const/16 v1, 0x2b

    const v2, -0x4cec0951

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2602586
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2602587
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2602588
    const-string v1, "extra_environment"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->c:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602589
    const v0, 0x7f0d1b23

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->e:Landroid/support/v7/widget/RecyclerView;

    .line 2602590
    const v0, 0x7f0d1b23

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->e:Landroid/support/v7/widget/RecyclerView;

    .line 2602591
    new-instance v0, LX/3wu;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 p1, 0x2

    invoke-direct {v0, v1, p1}, LX/3wu;-><init>(Landroid/content/Context;I)V

    .line 2602592
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 2602593
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2602594
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->b:LX/IhZ;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2602595
    return-void
.end method
