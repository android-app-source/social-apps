.class public Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/messaging/media/loader/LocalMediaLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IhX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Lcom/facebook/messaging/media/folder/Folder;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/IhK;

.field public f:LX/IhW;

.field private g:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

.field public h:LX/IhI;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2602805
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2602806
    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->c:Ljava/util/ArrayList;

    .line 2602807
    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->d:Lcom/facebook/messaging/media/folder/Folder;

    .line 2602808
    return-void
.end method

.method public static c(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;)LX/IgU;
    .locals 3

    .prologue
    .line 2602883
    invoke-static {}, Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;->a()LX/IgU;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->g:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602884
    iget-object v2, v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v1, v2

    .line 2602885
    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    .line 2602886
    iput-boolean v1, v0, LX/IgU;->c:Z

    .line 2602887
    move-object v1, v0

    .line 2602888
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->g:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602889
    iget-boolean v2, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->d:Z

    move v0, v2

    .line 2602890
    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2602891
    :goto_0
    iput-boolean v0, v1, LX/IgU;->a:Z

    .line 2602892
    move-object v0, v1

    .line 2602893
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2602880
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2602881
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    invoke-static {p1}, Lcom/facebook/messaging/media/loader/LocalMediaLoader;->b(LX/0QB;)Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    const-class v0, LX/IhX;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/IhX;

    iput-object v2, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    iput-object p1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->b:LX/IhX;

    .line 2602882
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2602876
    iput-object p1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->c:Ljava/util/ArrayList;

    .line 2602877
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->f:LX/IhW;

    if-eqz v0, :cond_0

    .line 2602878
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->f:LX/IhW;

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/IhW;->a(Ljava/util/List;)V

    .line 2602879
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3b67c432

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2602875
    const v1, 0x7f030a9e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x2f0f0b5d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0xb57fc17

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2602860
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2602861
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->d:Lcom/facebook/messaging/media/folder/Folder;

    if-eqz v1, :cond_0

    .line 2602862
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->d:Lcom/facebook/messaging/media/folder/Folder;

    .line 2602863
    invoke-static {p0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->c(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;)LX/IgU;

    move-result-object v2

    .line 2602864
    iget-object v3, v1, Lcom/facebook/messaging/media/folder/Folder;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2602865
    iput-object v3, v2, LX/IgU;->d:Ljava/lang/String;

    .line 2602866
    move-object v2, v2

    .line 2602867
    invoke-virtual {v2}, LX/IgU;->e()Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;

    move-result-object v2

    .line 2602868
    iget-object v3, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    new-instance v4, LX/Ihb;

    invoke-direct {v4, p0}, LX/Ihb;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;)V

    invoke-virtual {v3, v4}, LX/6Lb;->a(LX/3Mb;)V

    .line 2602869
    iget-object v3, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    invoke-virtual {v3, v2}, LX/6Lb;->a(Ljava/lang/Object;)V

    .line 2602870
    :goto_0
    const v1, 0x37f8f6f

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2602871
    :cond_0
    invoke-static {p0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->c(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;)LX/IgU;

    move-result-object v1

    invoke-virtual {v1}, LX/IgU;->e()Lcom/facebook/messaging/media/loader/LocalMediaLoaderParams;

    move-result-object v1

    .line 2602872
    iget-object v2, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    new-instance v3, LX/Ihb;

    invoke-direct {v3, p0}, LX/Ihb;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;)V

    invoke-virtual {v2, v3}, LX/6Lb;->a(LX/3Mb;)V

    .line 2602873
    iget-object v2, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    invoke-virtual {v2, v1}, LX/6Lb;->a(Ljava/lang/Object;)V

    .line 2602874
    goto :goto_0
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3a35e9a0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2602856
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2602857
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    invoke-virtual {v1}, LX/6Lb;->a()V

    .line 2602858
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->a:Lcom/facebook/messaging/media/loader/LocalMediaLoader;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/6Lb;->a(LX/3Mb;)V

    .line 2602859
    const/16 v1, 0x2b

    const v2, -0x54477d0c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 2602809
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2602810
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2602811
    const-string v3, "environment"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->g:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602812
    new-instance v3, LX/Ihi;

    invoke-direct {v3}, LX/Ihi;-><init>()V

    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->g:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602813
    iget-boolean v4, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    move v0, v4

    .line 2602814
    if-nez v0, :cond_1

    move v0, v1

    .line 2602815
    :goto_0
    iput-boolean v0, v3, LX/Ihi;->d:Z

    .line 2602816
    move-object v0, v3

    .line 2602817
    iget-object v3, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->g:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602818
    iget-boolean v4, v3, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    move v3, v4

    .line 2602819
    iput-boolean v3, v0, LX/Ihi;->b:Z

    .line 2602820
    move-object v0, v0

    .line 2602821
    iput v1, v0, LX/Ihi;->e:I

    .line 2602822
    move-object v0, v0

    .line 2602823
    const/4 v1, 0x4

    .line 2602824
    iput v1, v0, LX/Ihi;->f:I

    .line 2602825
    move-object v0, v0

    .line 2602826
    new-instance v1, LX/Ihj;

    invoke-direct {v1, v0}, LX/Ihj;-><init>(LX/Ihi;)V

    move-object v0, v1

    .line 2602827
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->b:LX/IhX;

    .line 2602828
    new-instance p2, LX/IhW;

    invoke-direct {p2, v0}, LX/IhW;-><init>(LX/Ihj;)V

    .line 2602829
    const-class v3, Landroid/content/Context;

    invoke-interface {v1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v1}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    const-class p1, LX/IhE;

    invoke-interface {v1, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/IhE;

    .line 2602830
    iput-object v3, p2, LX/IhW;->a:Landroid/content/Context;

    iput-object v4, p2, LX/IhW;->b:Landroid/view/LayoutInflater;

    iput-object p1, p2, LX/IhW;->c:LX/IhE;

    .line 2602831
    move-object v0, p2

    .line 2602832
    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->f:LX/IhW;

    .line 2602833
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->f:LX/IhW;

    new-instance v1, LX/Ihc;

    invoke-direct {v1, p0}, LX/Ihc;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;)V

    .line 2602834
    iput-object v1, v0, LX/IhW;->d:LX/Ihc;

    .line 2602835
    const v0, 0x7f0d1b2e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 2602836
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 2602837
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->f:LX/IhW;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2602838
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2602839
    const-string v1, "selected"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->c:Ljava/util/ArrayList;

    .line 2602840
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2602841
    const-string v1, "folder"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/folder/Folder;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->d:Lcom/facebook/messaging/media/folder/Folder;

    .line 2602842
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->d:Lcom/facebook/messaging/media/folder/Folder;

    if-eqz v0, :cond_0

    .line 2602843
    const v0, 0x7f0d1b2d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 2602844
    invoke-virtual {v0, v2}, Landroid/support/v7/widget/Toolbar;->setVisibility(I)V

    .line 2602845
    invoke-virtual {v0, v5}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 2602846
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f020f67

    invoke-static {v1, v2}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2602847
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    new-instance v3, Landroid/graphics/PorterDuffColorFilter;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v5, v4}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2602848
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 2602849
    const v1, 0x7f080850

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(I)V

    .line 2602850
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->d:Lcom/facebook/messaging/media/folder/Folder;

    .line 2602851
    iget-object v2, v1, Lcom/facebook/messaging/media/folder/Folder;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2602852
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 2602853
    new-instance v1, LX/Iha;

    invoke-direct {v1, p0}, LX/Iha;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2602854
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 2602855
    goto/16 :goto_0
.end method
