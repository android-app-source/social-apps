.class public Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private A:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

.field private B:Landroid/support/v7/widget/RecyclerView;

.field public C:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field public D:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

.field public p:LX/FGd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Iht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Ihd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/Ihz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/Ii1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final u:LX/IhL;

.field private final v:LX/IhM;

.field private final w:LX/IhN;

.field private x:Lcom/facebook/messaging/media/ui/MediaFabView;

.field public y:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

.field public z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2602551
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2602552
    new-instance v0, LX/IhL;

    invoke-direct {v0, p0}, LX/IhL;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->u:LX/IhL;

    .line 2602553
    new-instance v0, LX/IhM;

    invoke-direct {v0, p0}, LX/IhM;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->v:LX/IhM;

    .line 2602554
    new-instance v0, LX/IhN;

    invoke-direct {v0, p0}, LX/IhN;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->w:LX/IhN;

    .line 2602555
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    .line 2602556
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    invoke-static {v5}, LX/FGd;->b(LX/0QB;)LX/FGd;

    move-result-object v1

    check-cast v1, LX/FGd;

    invoke-static {v5}, LX/Iht;->b(LX/0QB;)LX/Iht;

    move-result-object v2

    check-cast v2, LX/Iht;

    new-instance p1, LX/Ihd;

    invoke-direct {p1}, LX/Ihd;-><init>()V

    invoke-static {v5}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    const-class v4, LX/Ihe;

    invoke-interface {v5, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Ihe;

    invoke-static {v5}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    iput-object v3, p1, LX/Ihd;->a:Landroid/view/LayoutInflater;

    iput-object v4, p1, LX/Ihd;->b:LX/Ihe;

    iput-object p0, p1, LX/Ihd;->c:Landroid/content/res/Resources;

    move-object v3, p1

    check-cast v3, LX/Ihd;

    invoke-static {v5}, LX/Ihz;->b(LX/0QB;)LX/Ihz;

    move-result-object v4

    check-cast v4, LX/Ihz;

    invoke-static {v5}, LX/Ii1;->b(LX/0QB;)LX/Ii1;

    move-result-object v5

    check-cast v5, LX/Ii1;

    iput-object v1, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->p:LX/FGd;

    iput-object v2, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->q:LX/Iht;

    iput-object v3, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->r:LX/Ihd;

    iput-object v4, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->s:LX/Ihz;

    iput-object v5, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->t:LX/Ii1;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 2

    .prologue
    .line 2602567
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->p:LX/FGd;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v0, v1}, LX/FGd;->a(LX/2MK;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2602568
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->p:LX/FGd;

    invoke-virtual {v0, p1}, LX/FGd;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2602569
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2602557
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2602558
    iget-object v3, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v4, LX/2MK;->VIDEO:LX/2MK;

    if-ne v3, v4, :cond_1

    .line 2602559
    iget-object v3, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->t:LX/Ii1;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    const-string v4, "messenger_video_send"

    invoke-virtual {v3, v0, v4}, LX/Ii1;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2602560
    :cond_0
    :goto_1
    return-void

    .line 2602561
    :cond_1
    iget-object v3, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->s:LX/Ihz;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v3, v0}, LX/Ihz;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2602562
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2602563
    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2602564
    const-string v1, "extra_media_items"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2602565
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->setResult(ILandroid/content/Intent;)V

    .line 2602566
    invoke-virtual {p0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->finish()V

    goto :goto_1
.end method

.method public static b$redex0(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 2

    .prologue
    .line 2602570
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->p:LX/FGd;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v0, v1}, LX/FGd;->a(LX/2MK;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2602571
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->p:LX/FGd;

    invoke-virtual {v0, p1}, LX/FGd;->b(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2602572
    :cond_0
    return-void
.end method

.method public static e$redex0(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 2

    .prologue
    .line 2602538
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->i:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2602539
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2602540
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2602541
    :goto_0
    invoke-static {p0, p1}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->a$redex0(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2602542
    return-void

    .line 2602543
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static l(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)Z
    .locals 3

    .prologue
    .line 2602544
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2602545
    :goto_0
    if-eqz v0, :cond_0

    .line 2602546
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    invoke-virtual {v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2602547
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->y:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v2, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->a(Ljava/util/ArrayList;)V

    .line 2602548
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    .line 2602549
    :cond_0
    return v0

    .line 2602550
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 2602528
    iget-object v3, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->B:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2602529
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->x:Lcom/facebook/messaging/media/ui/MediaFabView;

    iget-object v3, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/media/ui/MediaFabView;->setVisibility(I)V

    .line 2602530
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->r:LX/Ihd;

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/Ihd;->a(Ljava/util/List;)V

    .line 2602531
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2602532
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->B:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 2602533
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 2602534
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->x:Lcom/facebook/messaging/media/ui/MediaFabView;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/media/ui/MediaFabView;->a(Ljava/util/Set;)V

    .line 2602535
    return-void

    :cond_1
    move v0, v2

    .line 2602536
    goto :goto_0

    :cond_2
    move v1, v2

    .line 2602537
    goto :goto_1
.end method

.method private p()V
    .locals 3

    .prologue
    .line 2602520
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2602521
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2602522
    new-instance v2, Ljava/io/File;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2602523
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2602524
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2602525
    :cond_1
    invoke-static {p0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->n(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)V

    .line 2602526
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->y:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->a(Ljava/util/ArrayList;)V

    .line 2602527
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 2602507
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 2602508
    instance-of v0, p1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    if-eqz v0, :cond_1

    .line 2602509
    check-cast p1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iput-object p1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->y:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    .line 2602510
    :cond_0
    :goto_0
    return-void

    .line 2602511
    :cond_1
    instance-of v0, p1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    if-eqz v0, :cond_2

    .line 2602512
    check-cast p1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iput-object p1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->A:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    .line 2602513
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->A:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->v:LX/IhM;

    .line 2602514
    iput-object v1, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->w:LX/Igg;

    .line 2602515
    goto :goto_0

    .line 2602516
    :cond_2
    instance-of v0, p1, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    if-eqz v0, :cond_0

    .line 2602517
    check-cast p1, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->w:LX/IhN;

    .line 2602518
    iput-object v0, p1, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->v:LX/IgW;

    .line 2602519
    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2602473
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2602474
    invoke-static {p0, p0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2602475
    const v0, 0x7f030a99

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->setContentView(I)V

    .line 2602476
    invoke-virtual {p0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_environment"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->D:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602477
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "container_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->y:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    .line 2602478
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->y:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    if-nez v0, :cond_0

    .line 2602479
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->D:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602480
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2602481
    const-string p1, "extra_environment"

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v2, p1, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2602482
    new-instance v1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;-><init>()V

    .line 2602483
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2602484
    move-object v0, v1

    .line 2602485
    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->y:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    .line 2602486
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1b1f

    iget-object v2, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->y:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    const-string p1, "container_fragment_tag"

    invoke-virtual {v0, v1, v2, p1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2602487
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->y:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    new-instance v1, LX/IhJ;

    invoke-direct {v1, p0}, LX/IhJ;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)V

    .line 2602488
    iput-object v1, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->a:LX/IhJ;

    .line 2602489
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->y:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->u:LX/IhL;

    .line 2602490
    iput-object v1, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->b:LX/IhL;

    .line 2602491
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "album_contents_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    .line 2602492
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    if-eqz v0, :cond_1

    .line 2602493
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    new-instance v1, LX/IhI;

    invoke-direct {v1, p0}, LX/IhI;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)V

    .line 2602494
    iput-object v1, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->h:LX/IhI;

    .line 2602495
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->u:LX/IhL;

    .line 2602496
    iput-object v1, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->e:LX/IhK;

    .line 2602497
    :cond_1
    const v0, 0x7f0d1b20

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->B:Landroid/support/v7/widget/RecyclerView;

    .line 2602498
    new-instance v0, LX/1P1;

    invoke-direct {v0, p0}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 2602499
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 2602500
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->B:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->r:LX/Ihd;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2602501
    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->B:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2602502
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->r:LX/Ihd;

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, LX/Ihd;->a(Ljava/util/List;)V

    .line 2602503
    const v0, 0x7f0d1b21

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/ui/MediaFabView;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->x:Lcom/facebook/messaging/media/ui/MediaFabView;

    .line 2602504
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->x:Lcom/facebook/messaging/media/ui/MediaFabView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/media/ui/MediaFabView;->setDisplayMode(I)V

    .line 2602505
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->x:Lcom/facebook/messaging/media/ui/MediaFabView;

    new-instance v1, LX/IhH;

    invoke-direct {v1, p0}, LX/IhH;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/media/ui/MediaFabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2602506
    return-void
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2602464
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->A:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->A:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 2602465
    :goto_0
    invoke-static {p0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->l(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)Z

    move-result v1

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    .line 2602466
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2602467
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2602468
    invoke-static {p0, v0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->b$redex0(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2602469
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2602470
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->finish()V

    .line 2602471
    :cond_1
    return-void

    .line 2602472
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onRestart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x2e1f6ee7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2602461
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onRestart()V

    .line 2602462
    invoke-direct {p0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->p()V

    .line 2602463
    const/16 v1, 0x23

    const v2, 0x6d3aa42b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2602456
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 2602457
    const-string v0, "selected_media"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2602458
    const-string v0, "selected_media"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    .line 2602459
    invoke-direct {p0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->p()V

    .line 2602460
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2602452
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2602453
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2602454
    const-string v0, "selected_media"

    iget-object v1, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2602455
    :cond_0
    return-void
.end method
