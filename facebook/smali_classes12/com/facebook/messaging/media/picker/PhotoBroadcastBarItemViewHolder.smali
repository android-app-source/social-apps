.class public Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final m:LX/1Ad;

.field public final n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final o:LX/1o9;

.field private final p:Landroid/content/res/Resources;

.field public q:Lcom/facebook/ui/media/attachments/MediaResource;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2602924
    const-class v0, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Landroid/content/res/Resources;Landroid/view/View;)V
    .locals 2
    .param p3    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2602925
    invoke-direct {p0, p3}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2602926
    iput-object p1, p0, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->m:LX/1Ad;

    .line 2602927
    iput-object p2, p0, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->p:Landroid/content/res/Resources;

    .line 2602928
    const v0, 0x7f0d1b29

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2602929
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2602930
    iget-object v0, p0, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->p:Landroid/content/res/Resources;

    const v1, 0x7f0b18c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2602931
    new-instance v1, LX/1o9;

    invoke-direct {v1, v0, v0}, LX/1o9;-><init>(II)V

    iput-object v1, p0, Lcom/facebook/messaging/media/picker/PhotoBroadcastBarItemViewHolder;->o:LX/1o9;

    .line 2602932
    return-void
.end method
