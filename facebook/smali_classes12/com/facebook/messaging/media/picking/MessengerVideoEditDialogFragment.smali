.class public Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;
.super Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;
.source ""

# interfaces
.implements LX/8tT;


# instance fields
.field public m:Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

.field private n:Lcom/facebook/fbui/glyph/GlyphView;

.field private o:Lcom/facebook/fbui/glyph/GlyphView;

.field public p:LX/2Mi;

.field public q:LX/G7C;

.field public r:LX/Ie2;

.field public s:LX/FGW;

.field public t:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public u:Lcom/facebook/ui/media/attachments/MediaResource;

.field public v:LX/IgW;

.field public w:LX/39A;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2603145
    invoke-direct {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;-><init>()V

    .line 2603146
    return-void
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;LX/6eE;)Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;
    .locals 3

    .prologue
    .line 2603147
    new-instance v0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;-><init>()V

    .line 2603148
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2603149
    const-string v2, "m"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2603150
    invoke-virtual {p1, v1}, LX/6eE;->a(Landroid/os/Bundle;)V

    .line 2603151
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2603152
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    invoke-static {v3}, LX/2Mi;->a(LX/0QB;)LX/2Mi;

    move-result-object v0

    check-cast v0, LX/2Mi;

    invoke-static {v3}, LX/G7C;->a(LX/0QB;)LX/G7C;

    move-result-object v1

    check-cast v1, LX/G7C;

    new-instance v4, LX/Ie2;

    const-class v5, Landroid/content/Context;

    invoke-interface {v3, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v3}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v6

    check-cast v6, LX/0gh;

    invoke-static {v3}, LX/2MU;->b(LX/0QB;)LX/2MU;

    move-result-object v7

    check-cast v7, LX/2MV;

    invoke-static {v3}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-static {v3}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, LX/0TD;

    invoke-static {v3}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-direct/range {v4 .. v10}, LX/Ie2;-><init>(Landroid/content/Context;LX/0gh;LX/2MV;Ljava/util/concurrent/ExecutorService;LX/0TD;LX/03V;)V

    move-object v2, v4

    check-cast v2, LX/Ie2;

    invoke-static {v3}, LX/FGW;->b(LX/0QB;)LX/FGW;

    move-result-object v3

    check-cast v3, LX/FGW;

    iput-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->p:LX/2Mi;

    iput-object v1, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->q:LX/G7C;

    iput-object v2, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->r:LX/Ie2;

    iput-object v3, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->s:LX/FGW;

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 2603142
    if-eqz p1, :cond_0

    .line 2603143
    iget-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->n:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 2603144
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x47af5a1e

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2603153
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2603154
    const v0, 0x7f0d118a

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->n:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2603155
    const v0, 0x7f0d09a9

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->o:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2603156
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2603157
    if-eqz v0, :cond_0

    .line 2603158
    const-string v2, "m"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->u:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2603159
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->u:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2603160
    iget-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->m:Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    iget-object v2, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->u:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(Landroid/net/Uri;)V

    .line 2603161
    iget-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->m:Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    iget-object v2, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->p:LX/2Mi;

    invoke-virtual {v0, v2}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(LX/2Mj;)V

    .line 2603162
    iget-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->m:Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    iget-object v2, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->s:LX/FGW;

    invoke-virtual {v2}, LX/FGW;->a()I

    move-result v2

    .line 2603163
    iput v2, v0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->q:I

    .line 2603164
    new-instance v0, LX/Ihu;

    invoke-direct {v0, p0}, LX/Ihu;-><init>(Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;)V

    iput-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->w:LX/39A;

    .line 2603165
    iget-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->n:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/Ihv;

    invoke-direct {v2, p0}, LX/Ihv;-><init>(Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2603166
    iget-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->n:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 2603167
    iget-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->o:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/Ihw;

    invoke-direct {v2, p0}, LX/Ihw;-><init>(Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2603168
    iget-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->r:LX/Ie2;

    iget-object v2, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->u:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    const-string v3, "messenger_video_edit"

    new-instance v4, LX/Ihx;

    invoke-direct {v4, p0}, LX/Ihx;-><init>(Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;)V

    .line 2603169
    iget-object v6, v0, LX/Ie2;->e:LX/0TD;

    new-instance p0, LX/Ie0;

    invoke-direct {p0, v0, v2}, LX/Ie0;-><init>(LX/Ie2;Landroid/net/Uri;)V

    invoke-interface {v6, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2603170
    new-instance p0, LX/Ie1;

    invoke-direct {p0, v0, v3, v4}, LX/Ie1;-><init>(LX/Ie2;Ljava/lang/String;LX/0QK;)V

    iget-object p1, v0, LX/Ie2;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2603171
    const/16 v0, 0x2b

    const v2, -0x472a00de

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 2603135
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 2603136
    instance-of v0, p1, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    if-eqz v0, :cond_0

    .line 2603137
    check-cast p1, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    iput-object p1, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->m:Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    .line 2603138
    iget-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->m:Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->a(Z)V

    .line 2603139
    iget-object v0, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->m:Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    .line 2603140
    iput-object p0, v0, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->x:LX/8tT;

    .line 2603141
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3a9e00e1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2603132
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2603133
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2603134
    const/16 v1, 0x2b

    const v2, -0x3aae4d95

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x31471d51

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2603110
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2603111
    const v2, 0x7f030dd6

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2603112
    invoke-virtual {v2, v1, v1, v1, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 2603113
    const/16 v1, 0x2710

    invoke-virtual {v2, v1}, Landroid/view/View;->setMinimumWidth(I)V

    .line 2603114
    const/16 v1, 0x2b

    const v3, -0x7ed5f16c

    invoke-static {v4, v1, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x32659803

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2603115
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onDestroy()V

    .line 2603116
    iget-object v1, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->v:LX/IgW;

    if-eqz v1, :cond_0

    .line 2603117
    iget-object v1, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->v:LX/IgW;

    invoke-interface {v1}, LX/IgW;->a()V

    .line 2603118
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->q:LX/G7C;

    if-eqz v1, :cond_2

    .line 2603119
    iget-object v1, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->q:LX/G7C;

    sget-object v2, LX/0yY;->UNKNOWN:LX/0yY;

    iget-object v3, p0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->w:LX/39A;

    .line 2603120
    sget-object v5, LX/0yY;->UNKNOWN:LX/0yY;

    if-ne v2, v5, :cond_3

    .line 2603121
    iget-object v5, v1, LX/G7C;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/121;

    .line 2603122
    if-eqz v3, :cond_2

    .line 2603123
    iget-object p0, v5, LX/121;->a:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2603124
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 2603125
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Map$Entry;

    .line 2603126
    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/3J6;

    iget-object p0, p0, LX/3J6;->d:LX/39A;

    if-ne p0, v3, :cond_1

    .line 2603127
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2603128
    :cond_2
    :goto_1
    const/16 v1, 0x2b

    const v2, 0x7e14dcee

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2603129
    :cond_3
    invoke-static {v2}, LX/G7C;->b(LX/0yY;)LX/0yY;

    move-result-object p0

    .line 2603130
    iget-object v5, v1, LX/G7C;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/121;

    invoke-virtual {v5, p0, v3}, LX/121;->a(LX/0yY;LX/39A;)V

    .line 2603131
    iget-object v5, v1, LX/G7C;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/121;

    sget-object p0, LX/0yY;->SMS_THREAD_INTERSTITIAL:LX/0yY;

    invoke-virtual {v5, p0, v3}, LX/121;->a(LX/0yY;LX/39A;)V

    goto :goto_1
.end method
