.class public Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;
.super LX/620;
.source ""


# instance fields
.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2598158
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2598159
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 2598160
    invoke-direct {p0, p1, p2, p3}, LX/620;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2598161
    iput v2, p0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->d:I

    .line 2598162
    iput v2, p0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->e:I

    .line 2598163
    if-eqz p2, :cond_0

    .line 2598164
    invoke-virtual {p0}, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FavoritesDragSortListView:[I

    invoke-virtual {v0, p2, v1, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2598165
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->d:I

    .line 2598166
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->e:I

    .line 2598167
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2598168
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2598149
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2598150
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2598151
    iget v1, p0, LX/620;->a:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2598152
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2598153
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2598154
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2598155
    iget v0, p0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->d:I

    const/4 v1, 0x4

    invoke-static {p1, v0, v1}, LX/620;->a(Landroid/view/View;II)V

    .line 2598156
    iget v0, p0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->e:I

    invoke-static {p1, v0, v2}, LX/620;->a(Landroid/view/View;II)V

    .line 2598157
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;III)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 2598131
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 2598132
    check-cast v0, LX/Idv;

    .line 2598133
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2598134
    iput p2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2598135
    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2598136
    invoke-virtual {p1, p4}, Landroid/view/View;->setVisibility(I)V

    .line 2598137
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, LX/620;->a:I

    invoke-direct {v1, v2, v3, p3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 2598138
    invoke-virtual {v0}, LX/Idv;->getInnerRow()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2598139
    iget v1, p0, LX/620;->b:I

    if-ne p2, v1, :cond_3

    .line 2598140
    const/16 v1, 0x30

    if-ne p3, v1, :cond_0

    .line 2598141
    iget v1, p0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->e:I

    invoke-static {p1, v1, v4}, LX/620;->a(Landroid/view/View;II)V

    .line 2598142
    iget v1, p0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->d:I

    invoke-static {p1, v1, v5}, LX/620;->a(Landroid/view/View;II)V

    .line 2598143
    iget v1, p0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->e:I

    invoke-static {p1, v1, v4}, LX/620;->a(Landroid/view/View;II)V

    .line 2598144
    iget v1, p0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->d:I

    invoke-static {p1, v1, v4}, LX/620;->a(Landroid/view/View;II)V

    .line 2598145
    :cond_0
    :goto_0
    iget v1, p0, LX/620;->a:I

    if-ne p2, v1, :cond_1

    if-eqz p4, :cond_2

    .line 2598146
    :cond_1
    iput-boolean v4, v0, LX/Idv;->a:Z

    .line 2598147
    :cond_2
    return-void

    .line 2598148
    :cond_3
    iget v1, p0, Lcom/facebook/messaging/common/ui/widgets/FavoritesDragSortListView;->d:I

    invoke-static {p1, v1, v5}, LX/620;->a(Landroid/view/View;II)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 2598130
    iget v0, p0, LX/620;->c:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
