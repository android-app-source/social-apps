.class public Lcom/facebook/messaging/camerautil/CropImage;
.super Lcom/facebook/messaging/camerautil/MonitoredActivity;
.source ""


# static fields
.field private static final t:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:Z

.field public final B:Landroid/os/Handler;

.field private C:I

.field private D:I

.field private E:Z

.field private F:Z

.field public G:Lcom/facebook/messaging/camerautil/CropImageView;

.field private H:Landroid/content/ContentResolver;

.field public I:Landroid/graphics/Bitmap;

.field private J:LX/Idf;

.field public K:LX/Idk;

.field public p:Z

.field public q:Z

.field public r:LX/Idd;

.field public s:Ljava/lang/Runnable;

.field private u:Landroid/graphics/Bitmap$CompressFormat;

.field private v:Landroid/net/Uri;

.field private w:Z

.field public x:I

.field public y:I

.field public z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2597380
    const-class v0, Lcom/facebook/messaging/camerautil/CropImage;

    sput-object v0, Lcom/facebook/messaging/camerautil/CropImage;->t:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2597204
    invoke-direct {p0}, Lcom/facebook/messaging/camerautil/MonitoredActivity;-><init>()V

    .line 2597205
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->u:Landroid/graphics/Bitmap$CompressFormat;

    .line 2597206
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->v:Landroid/net/Uri;

    .line 2597207
    iput-boolean v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->w:Z

    .line 2597208
    iput-boolean v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->z:Z

    .line 2597209
    iput-boolean v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->A:Z

    .line 2597210
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->B:Landroid/os/Handler;

    .line 2597211
    iput-boolean v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->F:Z

    .line 2597212
    new-instance v0, Lcom/facebook/messaging/camerautil/CropImage$6;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/camerautil/CropImage$6;-><init>(Lcom/facebook/messaging/camerautil/CropImage;)V

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->s:Ljava/lang/Runnable;

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2597376
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImage;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2597377
    :goto_0
    return-void

    .line 2597378
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(Landroid/graphics/Bitmap;Z)V

    .line 2597379
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080415

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/facebook/messaging/camerautil/CropImage$3;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/camerautil/CropImage$3;-><init>(Lcom/facebook/messaging/camerautil/CropImage;)V

    iget-object v3, p0, Lcom/facebook/messaging/camerautil/CropImage;->B:Landroid/os/Handler;

    invoke-static {p0, v0, v1, v2, v3}, LX/IdZ;->a(Lcom/facebook/messaging/camerautil/MonitoredActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/messaging/camerautil/CropImage;Landroid/graphics/Bitmap;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v7, -0x1

    const/4 v0, 0x0

    .line 2597341
    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->v:Landroid/net/Uri;

    if-eqz v2, :cond_1

    .line 2597342
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->H:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->v:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v1

    .line 2597343
    if-eqz v1, :cond_0

    .line 2597344
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->u:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x4b

    invoke-virtual {p1, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2597345
    :cond_0
    invoke-static {v1}, LX/IdZ;->a(Ljava/io/Closeable;)V

    .line 2597346
    :goto_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2597347
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->v:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v7, v0}, Lcom/facebook/messaging/camerautil/CropImage;->setResult(ILandroid/content/Intent;)V

    .line 2597348
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->B:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/messaging/camerautil/CropImage$5;

    invoke-direct {v1, p0, p1}, Lcom/facebook/messaging/camerautil/CropImage$5;-><init>(Lcom/facebook/messaging/camerautil/CropImage;Landroid/graphics/Bitmap;)V

    const v2, -0x224bc7d5

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2597349
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImage;->finish()V

    .line 2597350
    return-void

    .line 2597351
    :catch_0
    move-exception v0

    .line 2597352
    :try_start_1
    sget-object v2, Lcom/facebook/messaging/camerautil/CropImage;->t:Ljava/lang/Class;

    const-string v3, "Cannot open file: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/facebook/messaging/camerautil/CropImage;->v:Landroid/net/Uri;

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2597353
    invoke-static {v1}, LX/IdZ;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/IdZ;->a(Ljava/io/Closeable;)V

    throw v0

    .line 2597354
    :cond_1
    iget-boolean v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->w:Z

    if-eqz v1, :cond_2

    .line 2597355
    :try_start_2
    invoke-static {p0}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/app/WallpaperManager;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 2597356
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/facebook/messaging/camerautil/CropImage;->setResult(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 2597357
    :catch_1
    move-exception v1

    .line 2597358
    sget-object v2, Lcom/facebook/messaging/camerautil/CropImage;->t:Ljava/lang/Class;

    const-string v3, "Failed to set wallpaper."

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2597359
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/camerautil/CropImage;->setResult(I)V

    goto :goto_1

    .line 2597360
    :cond_2
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 2597361
    const-string v1, "rect"

    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->r:LX/Idd;

    invoke-virtual {v2}, LX/Idd;->b()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2597362
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->K:LX/Idk;

    invoke-interface {v2}, LX/Idk;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2597363
    new-instance v5, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2597364
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 2597365
    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 2597366
    :goto_2
    add-int/lit8 v6, v0, 0x1

    .line 2597367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2597368
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    .line 2597369
    if-eqz v0, :cond_3

    move v0, v6

    .line 2597370
    goto :goto_2

    .line 2597371
    :cond_3
    const/4 v0, 0x1

    :try_start_3
    new-array v9, v0, [I

    .line 2597372
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->H:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->K:LX/Idk;

    invoke-interface {v1}, LX/Idk;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->K:LX/Idk;

    invoke-interface {v2}, LX/Idk;->b()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".jpg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    move-object v7, p1

    invoke-static/range {v0 .. v9}, LX/Idi;->a(Landroid/content/ContentResolver;Ljava/lang/String;JLandroid/location/Location;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;[B[I)Landroid/net/Uri;

    move-result-object v0

    .line 2597373
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/messaging/camerautil/CropImage;->setResult(ILandroid/content/Intent;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    .line 2597374
    :catch_2
    move-exception v0

    .line 2597375
    sget-object v1, Lcom/facebook/messaging/camerautil/CropImage;->t:Ljava/lang/Class;

    const-string v2, "store image fail, continue anyway"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method

.method public static b(Lcom/facebook/messaging/camerautil/CropImage;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v5, 0x1

    const/4 v9, 0x0

    .line 2597295
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->r:LX/Idd;

    if-nez v0, :cond_1

    .line 2597296
    :cond_0
    :goto_0
    return-void

    .line 2597297
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->q:Z

    if-nez v0, :cond_0

    .line 2597298
    iput-boolean v5, p0, Lcom/facebook/messaging/camerautil/CropImage;->q:Z

    .line 2597299
    iget v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->C:I

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->D:I

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->E:Z

    if-nez v0, :cond_4

    .line 2597300
    iget v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->C:I

    iget v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->D:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2597301
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2597302
    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->r:LX/Idd;

    invoke-virtual {v2}, LX/Idd;->b()Landroid/graphics/Rect;

    move-result-object v2

    .line 2597303
    new-instance v3, Landroid/graphics/Rect;

    iget v4, p0, Lcom/facebook/messaging/camerautil/CropImage;->C:I

    iget v6, p0, Lcom/facebook/messaging/camerautil/CropImage;->D:I

    invoke-direct {v3, v9, v9, v4, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2597304
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v6

    sub-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x2

    .line 2597305
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v7

    sub-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    .line 2597306
    invoke-static {v9, v4}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-static {v9, v6}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-virtual {v2, v7, v8}, Landroid/graphics/Rect;->inset(II)V

    .line 2597307
    neg-int v4, v4

    invoke-static {v9, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    neg-int v6, v6

    invoke-static {v9, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-virtual {v3, v4, v6}, Landroid/graphics/Rect;->inset(II)V

    .line 2597308
    iget-object v4, p0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v4, v2, v3, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2597309
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->a()V

    .line 2597310
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2597311
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    invoke-virtual {v0, v1, v5}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(Landroid/graphics/Bitmap;Z)V

    .line 2597312
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    invoke-virtual {v0, v5, v5}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(ZZ)V

    .line 2597313
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2597314
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2597315
    if-eqz v0, :cond_7

    const-string v2, "data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v2, "return-data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2597316
    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2597317
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2597318
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "inline-data"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/messaging/camerautil/CropImage;->setResult(ILandroid/content/Intent;)V

    .line 2597319
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImage;->finish()V

    goto/16 :goto_0

    .line 2597320
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->r:LX/Idd;

    invoke-virtual {v0}, LX/Idd;->b()Landroid/graphics/Rect;

    move-result-object v2

    .line 2597321
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    .line 2597322
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v4

    .line 2597323
    iget-boolean v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->A:Z

    if-eqz v0, :cond_6

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :goto_2
    invoke-static {v3, v4, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2597324
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2597325
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v9, v9, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2597326
    iget-object v7, p0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v7, v2, v6, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2597327
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->a()V

    .line 2597328
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2597329
    iget-boolean v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->A:Z

    if-eqz v0, :cond_5

    .line 2597330
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2597331
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    .line 2597332
    int-to-float v6, v3

    div-float/2addr v6, v8

    int-to-float v4, v4

    div-float/2addr v4, v8

    int-to-float v3, v3

    div-float/2addr v3, v8

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v6, v4, v3, v7}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 2597333
    sget-object v3, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 2597334
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v9, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2597335
    :cond_5
    iget v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->C:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->D:I

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->E:Z

    if-eqz v0, :cond_2

    .line 2597336
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iget v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->C:I

    iget v3, p0, Lcom/facebook/messaging/camerautil/CropImage;->D:I

    iget-boolean v4, p0, Lcom/facebook/messaging/camerautil/CropImage;->F:Z

    invoke-static/range {v0 .. v5}, LX/IdZ;->a(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZZ)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_1

    .line 2597337
    :cond_6
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    goto :goto_2

    .line 2597338
    :cond_7
    iget-boolean v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->w:Z

    if-eqz v0, :cond_8

    const v0, 0x7f080418

    .line 2597339
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/facebook/messaging/camerautil/CropImage$4;

    invoke-direct {v2, p0, v1}, Lcom/facebook/messaging/camerautil/CropImage$4;-><init>(Lcom/facebook/messaging/camerautil/CropImage;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->B:Landroid/os/Handler;

    invoke-static {p0, v10, v0, v2, v1}, LX/IdZ;->a(Lcom/facebook/messaging/camerautil/MonitoredActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Landroid/os/Handler;)V

    goto/16 :goto_0

    .line 2597340
    :cond_8
    const v0, 0x7f080419

    goto :goto_3
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 2597219
    invoke-super {p0, p1}, Lcom/facebook/messaging/camerautil/MonitoredActivity;->b(Landroid/os/Bundle;)V

    .line 2597220
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImage;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->H:Landroid/content/ContentResolver;

    .line 2597221
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/camerautil/CropImage;->requestWindowFeature(I)Z

    .line 2597222
    const v0, 0x7f030cfc

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/camerautil/CropImage;->setContentView(I)V

    .line 2597223
    const v0, 0x7f0d0340

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/camerautil/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/camerautil/CropImageView;

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    .line 2597224
    :try_start_0
    const/4 v0, 0x1

    invoke-static {v0}, LX/Idi;->a(Z)Z

    move-result v0

    move v0, v0

    .line 2597225
    if-nez v0, :cond_9

    .line 2597226
    const/4 v0, -0x1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2597227
    :goto_0
    move v0, v0

    .line 2597228
    const/4 v2, 0x0

    .line 2597229
    const/4 v3, -0x1

    if-ne v0, v3, :cond_b

    .line 2597230
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v2

    .line 2597231
    const-string v3, "checking"

    if-ne v2, v3, :cond_a

    .line 2597232
    const v2, 0x7f080412

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2597233
    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    .line 2597234
    const/16 v3, 0x1388

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2597235
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImage;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 2597236
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 2597237
    if-eqz v3, :cond_4

    .line 2597238
    const-string v0, "circleCrop"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2597239
    iput-boolean v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->A:Z

    .line 2597240
    iput v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->x:I

    .line 2597241
    iput v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->y:I

    .line 2597242
    :cond_2
    const-string v0, "output"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->v:Landroid/net/Uri;

    .line 2597243
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->v:Landroid/net/Uri;

    if-eqz v0, :cond_6

    .line 2597244
    const-string v0, "outputFormat"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2597245
    if-eqz v0, :cond_3

    .line 2597246
    invoke-static {v0}, Landroid/graphics/Bitmap$CompressFormat;->valueOf(Ljava/lang/String;)Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->u:Landroid/graphics/Bitmap$CompressFormat;

    .line 2597247
    :cond_3
    :goto_2
    const-string v0, "data"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    .line 2597248
    const-string v0, "aspectX"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->x:I

    .line 2597249
    const-string v0, "aspectY"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->y:I

    .line 2597250
    const-string v0, "outputX"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->C:I

    .line 2597251
    const-string v0, "outputY"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->D:I

    .line 2597252
    const-string v0, "scale"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->E:Z

    .line 2597253
    const-string v0, "scaleUpIfNeeded"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->F:Z

    .line 2597254
    const-string v0, "noFaceDetection"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "noFaceDetection"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->z:Z

    .line 2597255
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    if-nez v0, :cond_5

    .line 2597256
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 2597257
    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->H:Landroid/content/ContentResolver;

    const/4 p1, 0x0

    .line 2597258
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2597259
    :goto_4
    const-string v4, "content://drm"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2597260
    sget-object v3, LX/Ide;->ALL:LX/Ide;

    const/4 v4, 0x2

    invoke-static {v2, v3, v4, v1, p1}, LX/Idi;->a(Landroid/content/ContentResolver;LX/Ide;IILjava/lang/String;)LX/Idf;

    move-result-object v3

    .line 2597261
    :goto_5
    move-object v2, v3

    .line 2597262
    iput-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->J:LX/Idf;

    .line 2597263
    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage;->J:LX/Idf;

    invoke-interface {v2, v0}, LX/Idf;->a(Landroid/net/Uri;)LX/Idk;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->K:LX/Idk;

    .line 2597264
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->K:LX/Idk;

    if-eqz v0, :cond_5

    .line 2597265
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->K:LX/Idk;

    invoke-interface {v0, v1}, LX/Idk;->a(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    .line 2597266
    :cond_5
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    if-nez v0, :cond_8

    .line 2597267
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImage;->finish()V

    .line 2597268
    :goto_6
    return-void

    .line 2597269
    :cond_6
    const-string v0, "setWallpaper"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/camerautil/CropImage;->w:Z

    goto/16 :goto_2

    :cond_7
    move v0, v1

    .line 2597270
    goto :goto_3

    .line 2597271
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImage;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 2597272
    const v0, 0x7f0d17eb

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/camerautil/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/Ida;

    invoke-direct {v1, p0}, LX/Ida;-><init>(Lcom/facebook/messaging/camerautil/CropImage;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2597273
    const v0, 0x7f0d206e

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/camerautil/CropImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/Idb;

    invoke-direct {v1, p0}, LX/Idb;-><init>(Lcom/facebook/messaging/camerautil/CropImage;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2597274
    invoke-direct {p0}, Lcom/facebook/messaging/camerautil/CropImage;->a()V

    goto :goto_6

    .line 2597275
    :cond_9
    :try_start_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2597276
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 2597277
    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    const v2, 0x49b71b00    # 1500000.0f

    div-float/2addr v0, v2

    .line 2597278
    float-to-int v0, v0

    goto/16 :goto_0

    .line 2597279
    :catch_0
    const/4 v0, -0x2

    goto/16 :goto_0

    .line 2597280
    :cond_a
    const v2, 0x7f080413

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 2597281
    :cond_b
    if-gtz v0, :cond_0

    .line 2597282
    const v2, 0x7f080414

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 2597283
    :cond_c
    const-string v3, ""

    goto/16 :goto_4

    .line 2597284
    :cond_d
    const-string v4, "content://media/external/video"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 2597285
    sget-object v3, LX/Ide;->EXTERNAL:LX/Ide;

    const/4 v4, 0x4

    invoke-static {v2, v3, v4, v1, p1}, LX/Idi;->a(Landroid/content/ContentResolver;LX/Ide;IILjava/lang/String;)LX/Idf;

    move-result-object v3

    goto/16 :goto_5

    .line 2597286
    :cond_e
    sget-object v4, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_10

    sget-object v4, Landroid/provider/MediaStore$Images$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_10

    const/4 v4, 0x1

    :goto_7
    move v3, v4

    .line 2597287
    if-eqz v3, :cond_f

    .line 2597288
    new-instance v3, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;

    invoke-direct {v3}, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;-><init>()V

    .line 2597289
    iput-object v0, v3, Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;->e:Landroid/net/Uri;

    .line 2597290
    move-object v3, v3

    .line 2597291
    invoke-static {v2, v3}, LX/Idi;->a(Landroid/content/ContentResolver;Lcom/facebook/messaging/camerautil/ImageManager$ImageListParam;)LX/Idf;

    move-result-object v3

    move-object v3, v3

    .line 2597292
    goto/16 :goto_5

    .line 2597293
    :cond_f
    const-string v3, "bucketId"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2597294
    sget-object v4, LX/Ide;->ALL:LX/Ide;

    const/4 p1, 0x1

    invoke-static {v2, v4, p1, v1, v3}, LX/Idi;->a(Landroid/content/ContentResolver;LX/Ide;IILjava/lang/String;)LX/Idf;

    move-result-object v3

    goto/16 :goto_5

    :cond_10
    const/4 v4, 0x0

    goto :goto_7
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0xbcc355e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2597215
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->J:LX/Idf;

    if-eqz v1, :cond_0

    .line 2597216
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage;->J:LX/Idf;

    invoke-interface {v1}, LX/Idf;->a()V

    .line 2597217
    :cond_0
    invoke-super {p0}, Lcom/facebook/messaging/camerautil/MonitoredActivity;->onDestroy()V

    .line 2597218
    const/16 v1, 0x23

    const v2, 0x189c640d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x420679

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2597213
    invoke-super {p0}, Lcom/facebook/messaging/camerautil/MonitoredActivity;->onPause()V

    .line 2597214
    const/16 v1, 0x23

    const v2, -0x3809c6d1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
