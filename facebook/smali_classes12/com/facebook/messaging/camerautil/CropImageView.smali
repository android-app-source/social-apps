.class public Lcom/facebook/messaging/camerautil/CropImageView;
.super Lcom/facebook/widget/images/ImageViewTouchBase;
.source ""


# instance fields
.field public i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/Idd;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/Idd;

.field public k:F

.field public l:F

.field public m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2597528
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/images/ImageViewTouchBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2597529
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    .line 2597530
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->j:LX/Idd;

    .line 2597531
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2597544
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2597545
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idd;

    .line 2597546
    iput-boolean v2, v0, LX/Idd;->b:Z

    .line 2597547
    invoke-virtual {v0}, LX/Idd;->c()V

    .line 2597548
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2597549
    :cond_0
    add-int/lit8 v2, v2, 0x1

    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2597550
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idd;

    .line 2597551
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v0, v1, v3}, LX/Idd;->a(FF)I

    move-result v1

    .line 2597552
    if-eq v1, v4, :cond_0

    .line 2597553
    iget-boolean v1, v0, LX/Idd;->b:Z

    move v1, v1

    .line 2597554
    if-nez v1, :cond_2

    .line 2597555
    iput-boolean v4, v0, LX/Idd;->b:Z

    .line 2597556
    invoke-virtual {v0}, LX/Idd;->c()V

    .line 2597557
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->invalidate()V

    .line 2597558
    return-void
.end method

.method private b(LX/Idd;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2597532
    iget-object v1, p1, LX/Idd;->d:Landroid/graphics/Rect;

    .line 2597533
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->getLeft()I

    move-result v0

    iget v2, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v2

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2597534
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->getRight()I

    move-result v0

    iget v3, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v3

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 2597535
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->getTop()I

    move-result v0

    iget v4, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v4

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2597536
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->getBottom()I

    move-result v4

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v1, v4, v1

    invoke-static {v5, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2597537
    if-eqz v2, :cond_2

    .line 2597538
    :goto_0
    if-eqz v0, :cond_3

    .line 2597539
    :goto_1
    if-nez v2, :cond_0

    if-eqz v0, :cond_1

    .line 2597540
    :cond_0
    int-to-float v1, v2

    int-to-float v0, v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->b(FF)V

    .line 2597541
    :cond_1
    return-void

    :cond_2
    move v2, v3

    .line 2597542
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2597543
    goto :goto_1
.end method

.method private c(LX/Idd;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const v4, 0x3f19999a    # 0.6f

    .line 2597505
    iget-object v0, p1, LX/Idd;->d:Landroid/graphics/Rect;

    .line 2597506
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    .line 2597507
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    .line 2597508
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    .line 2597509
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    .line 2597510
    div-float v1, v2, v1

    mul-float/2addr v1, v4

    .line 2597511
    div-float v0, v3, v0

    mul-float/2addr v0, v4

    .line 2597512
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2597513
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getScale()F

    move-result v1

    mul-float/2addr v0, v1

    .line 2597514
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2597515
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getScale()F

    move-result v1

    sub-float v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v2, v1

    const-wide v4, 0x3fb999999999999aL    # 0.1

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    .line 2597516
    const/4 v1, 0x2

    new-array v1, v1, [F

    iget-object v2, p1, LX/Idd;->e:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    aput v2, v1, v6

    iget-object v2, p1, LX/Idd;->e:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    aput v2, v1, v7

    .line 2597517
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 2597518
    aget v2, v1, v6

    aget v1, v1, v7

    const/high16 v3, 0x43960000    # 300.0f

    invoke-virtual {p0, v0, v2, v1, v3}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(FFFF)V

    .line 2597519
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/messaging/camerautil/CropImageView;->b(LX/Idd;)V

    .line 2597520
    return-void
.end method


# virtual methods
.method public final a(FF)V
    .locals 3

    .prologue
    .line 2597521
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(FF)V

    .line 2597522
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2597523
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idd;

    .line 2597524
    iget-object v2, v0, LX/Idd;->f:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 2597525
    invoke-virtual {v0}, LX/Idd;->c()V

    .line 2597526
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2597527
    :cond_0
    return-void
.end method

.method public final a(FFF)V
    .locals 5

    .prologue
    .line 2597381
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(FFF)V

    .line 2597382
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idd;

    .line 2597383
    iget-object v3, v0, LX/Idd;->f:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 2597384
    invoke-virtual {v0}, LX/Idd;->c()V

    .line 2597385
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2597386
    :cond_0
    return-void
.end method

.method public final a(LX/Idd;)V
    .locals 1

    .prologue
    .line 2597387
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2597388
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->invalidate()V

    .line 2597389
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2597390
    invoke-super {p0, p1}, Lcom/facebook/widget/images/ImageViewTouchBase;->onDraw(Landroid/graphics/Canvas;)V

    .line 2597391
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2597392
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idd;

    invoke-virtual {v0, p1}, LX/Idd;->a(Landroid/graphics/Canvas;)V

    .line 2597393
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2597394
    :cond_0
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 2597395
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/images/ImageViewTouchBase;->onLayout(ZIIII)V

    .line 2597396
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 2597397
    iget-object v1, v0, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 2597398
    if-eqz v0, :cond_1

    .line 2597399
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idd;

    .line 2597400
    iget-object v3, v0, LX/Idd;->f:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 2597401
    invoke-virtual {v0}, LX/Idd;->c()V

    .line 2597402
    iget-boolean v3, v0, LX/Idd;->b:Z

    if-eqz v3, :cond_0

    .line 2597403
    invoke-direct {p0, v0}, Lcom/facebook/messaging/camerautil/CropImageView;->c(LX/Idd;)V

    .line 2597404
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2597405
    :cond_1
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x1

    const v0, -0x6f727ae2

    invoke-static {v3, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2597406
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/camerautil/CropImage;

    .line 2597407
    iget-boolean v1, v0, Lcom/facebook/messaging/camerautil/CropImage;->q:Z

    if-eqz v1, :cond_0

    .line 2597408
    const v0, 0x219fda78

    invoke-static {v3, v3, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2597409
    :goto_0
    return v2

    .line 2597410
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2597411
    :cond_1
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 2597412
    :cond_2
    :goto_2
    const v0, -0x31c51c5e    # -7.8387008E8f

    invoke-static {v0, v6}, LX/02F;->a(II)V

    move v2, v5

    goto :goto_0

    .line 2597413
    :pswitch_0
    iget-boolean v0, v0, Lcom/facebook/messaging/camerautil/CropImage;->p:Z

    if-eqz v0, :cond_3

    .line 2597414
    invoke-direct {p0, p1}, Lcom/facebook/messaging/camerautil/CropImageView;->a(Landroid/view/MotionEvent;)V

    goto :goto_1

    :cond_3
    move v1, v2

    .line 2597415
    :goto_3
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2597416
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idd;

    .line 2597417
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/Idd;->a(FF)I

    move-result v2

    .line 2597418
    if-eq v2, v5, :cond_5

    .line 2597419
    iput v2, p0, Lcom/facebook/messaging/camerautil/CropImageView;->m:I

    .line 2597420
    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->j:LX/Idd;

    .line 2597421
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->k:F

    .line 2597422
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->l:F

    .line 2597423
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImageView;->j:LX/Idd;

    const/16 v0, 0x20

    if-ne v2, v0, :cond_4

    sget-object v0, LX/Idc;->Move:LX/Idc;

    :goto_4
    invoke-virtual {v1, v0}, LX/Idd;->a(LX/Idc;)V

    goto :goto_1

    :cond_4
    sget-object v0, LX/Idc;->Grow:LX/Idc;

    goto :goto_4

    .line 2597424
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2597425
    :pswitch_1
    iget-boolean v1, v0, Lcom/facebook/messaging/camerautil/CropImage;->p:Z

    if-eqz v1, :cond_9

    move v3, v2

    .line 2597426
    :goto_5
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v3, v1, :cond_a

    .line 2597427
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Idd;

    .line 2597428
    iget-boolean v4, v1, LX/Idd;->b:Z

    move v4, v4

    .line 2597429
    if-eqz v4, :cond_8

    .line 2597430
    iput-object v1, v0, Lcom/facebook/messaging/camerautil/CropImage;->r:LX/Idd;

    move v4, v2

    .line 2597431
    :goto_6
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_7

    .line 2597432
    if-eq v4, v3, :cond_6

    .line 2597433
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Idd;

    .line 2597434
    iput-boolean v5, v0, LX/Idd;->c:Z

    .line 2597435
    :cond_6
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_6

    .line 2597436
    :cond_7
    invoke-direct {p0, v1}, Lcom/facebook/messaging/camerautil/CropImageView;->c(LX/Idd;)V

    .line 2597437
    invoke-virtual {p0}, Lcom/facebook/messaging/camerautil/CropImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/camerautil/CropImage;

    iput-boolean v2, v0, Lcom/facebook/messaging/camerautil/CropImage;->p:Z

    .line 2597438
    const v0, -0x2d995528

    invoke-static {v0, v6}, LX/02F;->a(II)V

    move v2, v5

    goto/16 :goto_0

    .line 2597439
    :cond_8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_5

    .line 2597440
    :cond_9
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->j:LX/Idd;

    if-eqz v0, :cond_a

    .line 2597441
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->j:LX/Idd;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/camerautil/CropImageView;->c(LX/Idd;)V

    .line 2597442
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->j:LX/Idd;

    sget-object v1, LX/Idc;->None:LX/Idc;

    invoke-virtual {v0, v1}, LX/Idd;->a(LX/Idc;)V

    .line 2597443
    :cond_a
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->j:LX/Idd;

    goto/16 :goto_1

    .line 2597444
    :pswitch_2
    iget-boolean v0, v0, Lcom/facebook/messaging/camerautil/CropImage;->p:Z

    if-eqz v0, :cond_b

    .line 2597445
    invoke-direct {p0, p1}, Lcom/facebook/messaging/camerautil/CropImageView;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 2597446
    :cond_b
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->j:LX/Idd;

    if-eqz v0, :cond_1

    .line 2597447
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->j:LX/Idd;

    iget v1, p0, Lcom/facebook/messaging/camerautil/CropImageView;->m:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/facebook/messaging/camerautil/CropImageView;->k:F

    sub-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget v4, p0, Lcom/facebook/messaging/camerautil/CropImageView;->l:F

    sub-float/2addr v3, v4

    const/4 v7, -0x1

    const/4 v4, 0x0

    const/4 v8, 0x1

    .line 2597448
    invoke-static {v0}, LX/Idd;->e(LX/Idd;)Landroid/graphics/Rect;

    move-result-object v9

    .line 2597449
    if-ne v1, v8, :cond_c

    .line 2597450
    :goto_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->k:F

    .line 2597451
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->l:F

    .line 2597452
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImageView;->j:LX/Idd;

    invoke-direct {p0, v0}, Lcom/facebook/messaging/camerautil/CropImageView;->b(LX/Idd;)V

    goto/16 :goto_1

    .line 2597453
    :pswitch_3
    invoke-virtual {p0, v5, v5}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(ZZ)V

    goto/16 :goto_2

    .line 2597454
    :pswitch_4
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getScale()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 2597455
    invoke-virtual {p0, v5, v5}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(ZZ)V

    goto/16 :goto_2

    .line 2597456
    :cond_c
    const/16 v10, 0x20

    if-ne v1, v10, :cond_d

    .line 2597457
    iget-object v4, v0, LX/Idd;->e:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v4, v7

    mul-float/2addr v4, v2

    iget-object v7, v0, LX/Idd;->e:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v7, v8

    mul-float/2addr v7, v3

    const/16 v3, -0xa

    const/4 v2, 0x0

    .line 2597458
    new-instance v8, Landroid/graphics/Rect;

    iget-object v9, v0, LX/Idd;->d:Landroid/graphics/Rect;

    invoke-direct {v8, v9}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 2597459
    iget-object v9, v0, LX/Idd;->e:Landroid/graphics/RectF;

    invoke-virtual {v9, v4, v7}, Landroid/graphics/RectF;->offset(FF)V

    .line 2597460
    iget-object v9, v0, LX/Idd;->e:Landroid/graphics/RectF;

    iget-object v10, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    iget-object v11, v0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    invoke-static {v2, v10}, Ljava/lang/Math;->max(FF)F

    move-result v10

    iget-object v11, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    iget-object v1, v0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v11, v1

    invoke-static {v2, v11}, Ljava/lang/Math;->max(FF)F

    move-result v11

    invoke-virtual {v9, v10, v11}, Landroid/graphics/RectF;->offset(FF)V

    .line 2597461
    iget-object v9, v0, LX/Idd;->e:Landroid/graphics/RectF;

    iget-object v10, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    iget-object v11, v0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->right:F

    sub-float/2addr v10, v11

    invoke-static {v2, v10}, Ljava/lang/Math;->min(FF)F

    move-result v10

    iget-object v11, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    iget-object v1, v0, LX/Idd;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v11, v1

    invoke-static {v2, v11}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-virtual {v9, v10, v11}, Landroid/graphics/RectF;->offset(FF)V

    .line 2597462
    invoke-static {v0}, LX/Idd;->e(LX/Idd;)Landroid/graphics/Rect;

    move-result-object v9

    iput-object v9, v0, LX/Idd;->d:Landroid/graphics/Rect;

    .line 2597463
    iget-object v9, v0, LX/Idd;->d:Landroid/graphics/Rect;

    invoke-virtual {v8, v9}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 2597464
    invoke-virtual {v8, v3, v3}, Landroid/graphics/Rect;->inset(II)V

    .line 2597465
    iget-object v9, v0, LX/Idd;->a:Landroid/view/View;

    invoke-virtual {v9, v8}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 2597466
    goto/16 :goto_7

    .line 2597467
    :cond_d
    and-int/lit8 v10, v1, 0x6

    if-nez v10, :cond_e

    move v2, v4

    .line 2597468
    :cond_e
    and-int/lit8 v10, v1, 0x18

    if-nez v10, :cond_f

    move v3, v4

    .line 2597469
    :cond_f
    iget-object v4, v0, LX/Idd;->e:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v4, v10

    mul-float v10, v2, v4

    .line 2597470
    iget-object v4, v0, LX/Idd;->e:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v4, v9

    mul-float v9, v3, v4

    .line 2597471
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_16

    move v4, v7

    :goto_8
    int-to-float v4, v4

    mul-float/2addr v10, v4

    and-int/lit8 v4, v1, 0x8

    if-eqz v4, :cond_17

    move v4, v7

    :goto_9
    int-to-float v4, v4

    mul-float/2addr v4, v9

    const/high16 v9, 0x41c80000    # 25.0f

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 2597472
    iget-boolean v7, v0, LX/Idd;->j:Z

    if-eqz v7, :cond_10

    .line 2597473
    cmpl-float v7, v10, v2

    if-eqz v7, :cond_18

    .line 2597474
    iget v7, v0, LX/Idd;->k:F

    div-float v4, v10, v7

    .line 2597475
    :cond_10
    :goto_a
    new-instance v11, Landroid/graphics/RectF;

    iget-object v7, v0, LX/Idd;->e:Landroid/graphics/RectF;

    invoke-direct {v11, v7}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    .line 2597476
    cmpl-float v7, v10, v2

    if-lez v7, :cond_1c

    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v7

    mul-float v8, v3, v10

    add-float/2addr v7, v8

    iget-object v8, v0, LX/Idd;->i:Landroid/graphics/RectF;

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_1c

    .line 2597477
    iget-object v7, v0, LX/Idd;->i:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v8

    sub-float/2addr v7, v8

    div-float v10, v7, v3

    .line 2597478
    iget-boolean v7, v0, LX/Idd;->j:Z

    if-eqz v7, :cond_1c

    .line 2597479
    iget v7, v0, LX/Idd;->k:F

    div-float v4, v10, v7

    move v7, v4

    move v8, v10

    .line 2597480
    :goto_b
    cmpl-float v12, v7, v2

    if-lez v12, :cond_11

    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v12

    mul-float v1, v3, v7

    add-float/2addr v12, v1

    iget-object v1, v0, LX/Idd;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v12, v12, v1

    if-lez v12, :cond_11

    .line 2597481
    iget-object v7, v0, LX/Idd;->i:Landroid/graphics/RectF;

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v12

    sub-float/2addr v7, v12

    div-float/2addr v7, v3

    .line 2597482
    iget-boolean v12, v0, LX/Idd;->j:Z

    if-eqz v12, :cond_11

    .line 2597483
    iget v8, v0, LX/Idd;->k:F

    mul-float/2addr v8, v7

    .line 2597484
    :cond_11
    neg-float v8, v8

    neg-float v7, v7

    invoke-virtual {v11, v8, v7}, Landroid/graphics/RectF;->inset(FF)V

    .line 2597485
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v7

    cmpg-float v7, v7, v9

    if-gez v7, :cond_12

    .line 2597486
    invoke-virtual {v11}, Landroid/graphics/RectF;->width()F

    move-result v7

    sub-float v7, v9, v7

    neg-float v7, v7

    div-float/2addr v7, v3

    invoke-virtual {v11, v7, v2}, Landroid/graphics/RectF;->inset(FF)V

    .line 2597487
    :cond_12
    iget-boolean v7, v0, LX/Idd;->j:Z

    if-eqz v7, :cond_19

    iget v7, v0, LX/Idd;->k:F

    div-float v7, v9, v7

    .line 2597488
    :goto_c
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpg-float v8, v8, v7

    if-gez v8, :cond_13

    .line 2597489
    invoke-virtual {v11}, Landroid/graphics/RectF;->height()F

    move-result v8

    sub-float/2addr v7, v8

    neg-float v7, v7

    div-float/2addr v7, v3

    invoke-virtual {v11, v2, v7}, Landroid/graphics/RectF;->inset(FF)V

    .line 2597490
    :cond_13
    iget v7, v11, Landroid/graphics/RectF;->left:F

    iget-object v8, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    cmpg-float v7, v7, v8

    if-gez v7, :cond_1a

    .line 2597491
    iget-object v7, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    iget v8, v11, Landroid/graphics/RectF;->left:F

    sub-float/2addr v7, v8

    invoke-virtual {v11, v7, v2}, Landroid/graphics/RectF;->offset(FF)V

    .line 2597492
    :cond_14
    :goto_d
    iget v7, v11, Landroid/graphics/RectF;->top:F

    iget-object v8, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    cmpg-float v7, v7, v8

    if-gez v7, :cond_1b

    .line 2597493
    iget-object v7, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget v8, v11, Landroid/graphics/RectF;->top:F

    sub-float/2addr v7, v8

    invoke-virtual {v11, v2, v7}, Landroid/graphics/RectF;->offset(FF)V

    .line 2597494
    :cond_15
    :goto_e
    iget-object v7, v0, LX/Idd;->e:Landroid/graphics/RectF;

    invoke-virtual {v7, v11}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 2597495
    invoke-static {v0}, LX/Idd;->e(LX/Idd;)Landroid/graphics/Rect;

    move-result-object v7

    iput-object v7, v0, LX/Idd;->d:Landroid/graphics/Rect;

    .line 2597496
    iget-object v7, v0, LX/Idd;->a:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->invalidate()V

    .line 2597497
    goto/16 :goto_7

    :cond_16
    move v4, v8

    goto/16 :goto_8

    :cond_17
    move v4, v8

    goto/16 :goto_9

    .line 2597498
    :cond_18
    cmpl-float v7, v4, v2

    if-eqz v7, :cond_10

    .line 2597499
    iget v7, v0, LX/Idd;->k:F

    mul-float v10, v4, v7

    goto/16 :goto_a

    :cond_19
    move v7, v9

    .line 2597500
    goto :goto_c

    .line 2597501
    :cond_1a
    iget v7, v11, Landroid/graphics/RectF;->right:F

    iget-object v8, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    cmpl-float v7, v7, v8

    if-lez v7, :cond_14

    .line 2597502
    iget v7, v11, Landroid/graphics/RectF;->right:F

    iget-object v8, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v8

    neg-float v7, v7

    invoke-virtual {v11, v7, v2}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_d

    .line 2597503
    :cond_1b
    iget v7, v11, Landroid/graphics/RectF;->bottom:F

    iget-object v8, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    cmpl-float v7, v7, v8

    if-lez v7, :cond_15

    .line 2597504
    iget v7, v11, Landroid/graphics/RectF;->bottom:F

    iget-object v8, v0, LX/Idd;->i:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v7, v8

    neg-float v7, v7

    invoke-virtual {v11, v2, v7}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_e

    :cond_1c
    move v7, v4

    move v8, v10

    goto/16 :goto_b

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
