.class public final Lcom/facebook/messaging/camerautil/CropImage$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:F

.field public b:Landroid/graphics/Matrix;

.field public c:[Landroid/media/FaceDetector$Face;

.field public d:I

.field public final synthetic e:Lcom/facebook/messaging/camerautil/CropImage;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/camerautil/CropImage;)V
    .locals 1

    .prologue
    .line 2597180
    iput-object p1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2597181
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->a:F

    .line 2597182
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/media/FaceDetector$Face;

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->c:[Landroid/media/FaceDetector$Face;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/camerautil/CropImage$6;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 2597165
    new-instance v0, LX/Idd;

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    invoke-direct {v0, v1}, LX/Idd;-><init>(Landroid/view/View;)V

    .line 2597166
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 2597167
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 2597168
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v5, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2597169
    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    div-int/lit8 v3, v1, 0x5

    .line 2597170
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->x:I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->y:I

    if-eqz v1, :cond_2

    .line 2597171
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->x:I

    iget-object v4, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v4, v4, Lcom/facebook/messaging/camerautil/CropImage;->y:I

    if-le v1, v4, :cond_1

    .line 2597172
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->y:I

    mul-int/2addr v1, v3

    iget-object v4, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v4, v4, Lcom/facebook/messaging/camerautil/CropImage;->x:I

    div-int/2addr v1, v4

    move v4, v3

    .line 2597173
    :goto_0
    sub-int v3, v6, v4

    div-int/lit8 v6, v3, 0x2

    .line 2597174
    sub-int v3, v7, v1

    div-int/lit8 v7, v3, 0x2

    .line 2597175
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v8, v6

    int-to-float v9, v7

    add-int/2addr v4, v6

    int-to-float v4, v4

    add-int/2addr v1, v7

    int-to-float v1, v1

    invoke-direct {v3, v8, v9, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2597176
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->b:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-boolean v4, v4, Lcom/facebook/messaging/camerautil/CropImage;->A:Z

    iget-object v6, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v6, v6, Lcom/facebook/messaging/camerautil/CropImage;->x:I

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v6, v6, Lcom/facebook/messaging/camerautil/CropImage;->y:I

    if-eqz v6, :cond_0

    const/4 v5, 0x1

    :cond_0
    invoke-virtual/range {v0 .. v5}, LX/Idd;->a(Landroid/graphics/Matrix;Landroid/graphics/Rect;Landroid/graphics/RectF;ZZ)V

    .line 2597177
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/camerautil/CropImageView;->a(LX/Idd;)V

    .line 2597178
    return-void

    .line 2597179
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->x:I

    mul-int/2addr v1, v3

    iget-object v4, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v4, v4, Lcom/facebook/messaging/camerautil/CropImage;->y:I

    div-int/2addr v1, v4

    move v4, v1

    move v1, v3

    goto :goto_0

    :cond_2
    move v1, v3

    move v4, v3

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/messaging/camerautil/CropImage$6;Landroid/media/FaceDetector$Face;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 2597141
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 2597142
    invoke-virtual {p1}, Landroid/media/FaceDetector$Face;->eyesDistance()F

    move-result v1

    iget v2, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->a:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    mul-int/lit8 v1, v1, 0x2

    .line 2597143
    invoke-virtual {p1, v0}, Landroid/media/FaceDetector$Face;->getMidPoint(Landroid/graphics/PointF;)V

    .line 2597144
    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v3, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->a:F

    mul-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 2597145
    iget v2, v0, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->a:F

    mul-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/PointF;->y:F

    .line 2597146
    iget v2, v0, Landroid/graphics/PointF;->x:F

    float-to-int v4, v2

    .line 2597147
    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-int v6, v0

    .line 2597148
    new-instance v0, LX/Idd;

    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v2, v2, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    invoke-direct {v0, v2}, LX/Idd;-><init>(Landroid/view/View;)V

    .line 2597149
    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v2, v2, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 2597150
    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v2, v2, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 2597151
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v5, v5, v3, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2597152
    new-instance v3, Landroid/graphics/RectF;

    int-to-float v7, v4

    int-to-float v8, v6

    int-to-float v4, v4

    int-to-float v6, v6

    invoke-direct {v3, v7, v8, v4, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2597153
    neg-int v4, v1

    int-to-float v4, v4

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v3, v4, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 2597154
    iget v1, v3, Landroid/graphics/RectF;->left:F

    cmpg-float v1, v1, v9

    if-gez v1, :cond_0

    .line 2597155
    iget v1, v3, Landroid/graphics/RectF;->left:F

    neg-float v1, v1

    iget v4, v3, Landroid/graphics/RectF;->left:F

    neg-float v4, v4

    invoke-virtual {v3, v1, v4}, Landroid/graphics/RectF;->inset(FF)V

    .line 2597156
    :cond_0
    iget v1, v3, Landroid/graphics/RectF;->top:F

    cmpg-float v1, v1, v9

    if-gez v1, :cond_1

    .line 2597157
    iget v1, v3, Landroid/graphics/RectF;->top:F

    neg-float v1, v1

    iget v4, v3, Landroid/graphics/RectF;->top:F

    neg-float v4, v4

    invoke-virtual {v3, v1, v4}, Landroid/graphics/RectF;->inset(FF)V

    .line 2597158
    :cond_1
    iget v1, v3, Landroid/graphics/RectF;->right:F

    iget v4, v2, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    cmpl-float v1, v1, v4

    if-lez v1, :cond_2

    .line 2597159
    iget v1, v3, Landroid/graphics/RectF;->right:F

    iget v4, v2, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    sub-float/2addr v1, v4

    iget v4, v3, Landroid/graphics/RectF;->right:F

    iget v6, v2, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    sub-float/2addr v4, v6

    invoke-virtual {v3, v1, v4}, Landroid/graphics/RectF;->inset(FF)V

    .line 2597160
    :cond_2
    iget v1, v3, Landroid/graphics/RectF;->bottom:F

    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    cmpl-float v1, v1, v4

    if-lez v1, :cond_3

    .line 2597161
    iget v1, v3, Landroid/graphics/RectF;->bottom:F

    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    sub-float/2addr v1, v4

    iget v4, v3, Landroid/graphics/RectF;->bottom:F

    iget v6, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    sub-float/2addr v4, v6

    invoke-virtual {v3, v1, v4}, Landroid/graphics/RectF;->inset(FF)V

    .line 2597162
    :cond_3
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->b:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-boolean v4, v4, Lcom/facebook/messaging/camerautil/CropImage;->A:Z

    iget-object v6, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v6, v6, Lcom/facebook/messaging/camerautil/CropImage;->x:I

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget v6, v6, Lcom/facebook/messaging/camerautil/CropImage;->y:I

    if-eqz v6, :cond_4

    const/4 v5, 0x1

    :cond_4
    invoke-virtual/range {v0 .. v5}, LX/Idd;->a(Landroid/graphics/Matrix;Landroid/graphics/Rect;Landroid/graphics/RectF;ZZ)V

    .line 2597163
    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/camerautil/CropImageView;->a(LX/Idd;)V

    .line 2597164
    return-void
.end method

.method private b()Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2597123
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 2597124
    const/4 v0, 0x0

    .line 2597125
    :goto_0
    return-object v0

    .line 2597126
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    const/16 v2, 0x100

    if-le v0, v2, :cond_1

    .line 2597127
    const/high16 v0, 0x43800000    # 256.0f

    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v2, v2, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    iput v0, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->a:F

    .line 2597128
    :cond_1
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 2597129
    iget v0, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->a:F

    iget v2, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->a:F

    invoke-virtual {v5, v0, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 2597130
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v2, v2, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v2, v2, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2597131
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    invoke-virtual {v0}, Lcom/facebook/messaging/camerautil/CropImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->b:Landroid/graphics/Matrix;

    .line 2597132
    invoke-direct {p0}, Lcom/facebook/messaging/camerautil/CropImage$6;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2597133
    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->a:F

    div-float/2addr v1, v2

    iput v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->a:F

    .line 2597134
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-boolean v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->z:Z

    if-eqz v1, :cond_0

    .line 2597135
    new-instance v1, Landroid/media/FaceDetector;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->c:[Landroid/media/FaceDetector$Face;

    array-length v4, v4

    invoke-direct {v1, v2, v3, v4}, Landroid/media/FaceDetector;-><init>(III)V

    .line 2597136
    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->c:[Landroid/media/FaceDetector$Face;

    invoke-virtual {v1, v0, v2}, Landroid/media/FaceDetector;->findFaces(Landroid/graphics/Bitmap;[Landroid/media/FaceDetector$Face;)I

    move-result v1

    iput v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->d:I

    .line 2597137
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    if-eq v0, v1, :cond_1

    .line 2597138
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2597139
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$6;->e:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage;->B:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/messaging/camerautil/CropImage$6$1;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/camerautil/CropImage$6$1;-><init>(Lcom/facebook/messaging/camerautil/CropImage$6;)V

    const v2, 0x3fa50a30

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2597140
    return-void
.end method
