.class public final Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;
.super LX/IdY;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Lcom/facebook/messaging/camerautil/MonitoredActivity;

.field public final b:LX/4BY;

.field private final c:Ljava/lang/Runnable;

.field private final d:Landroid/os/Handler;

.field private final e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/camerautil/MonitoredActivity;Ljava/lang/Runnable;LX/4BY;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 2596944
    invoke-direct {p0}, LX/IdY;-><init>()V

    .line 2596945
    new-instance v0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob$1;

    invoke-direct {v0, p0}, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob$1;-><init>(Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;)V

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->e:Ljava/lang/Runnable;

    .line 2596946
    iput-object p1, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->a:Lcom/facebook/messaging/camerautil/MonitoredActivity;

    .line 2596947
    iput-object p3, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->b:LX/4BY;

    .line 2596948
    iput-object p2, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->c:Ljava/lang/Runnable;

    .line 2596949
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->a:Lcom/facebook/messaging/camerautil/MonitoredActivity;

    .line 2596950
    iget-object p1, v0, Lcom/facebook/messaging/camerautil/MonitoredActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2596951
    :goto_0
    iput-object p4, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->d:Landroid/os/Handler;

    .line 2596952
    return-void

    .line 2596953
    :cond_0
    iget-object p1, v0, Lcom/facebook/messaging/camerautil/MonitoredActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2596954
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2596955
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->e:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2596956
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2596957
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->b:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->hide()V

    .line 2596958
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2596959
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->b:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->show()V

    .line 2596960
    return-void
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 2596961
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2596962
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->e:Ljava/lang/Runnable;

    const v2, -0x1e9ade3f

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2596963
    return-void

    .line 2596964
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->d:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/messaging/camerautil/CameraUtil$BackgroundJob;->e:Ljava/lang/Runnable;

    const v3, 0x544fbd5f    # 3.5689414E12f

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    throw v0
.end method
