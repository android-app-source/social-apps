.class public final Lcom/facebook/messaging/camerautil/CropImage$3$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/graphics/Bitmap;

.field public final synthetic b:Ljava/util/concurrent/CountDownLatch;

.field public final synthetic c:Lcom/facebook/messaging/camerautil/CropImage$3;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/camerautil/CropImage$3;Landroid/graphics/Bitmap;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    .prologue
    .line 2597077
    iput-object p1, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->c:Lcom/facebook/messaging/camerautil/CropImage$3;

    iput-object p2, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->a:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2597078
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->a:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->c:Lcom/facebook/messaging/camerautil/CropImage$3;

    iget-object v1, v1, Lcom/facebook/messaging/camerautil/CropImage$3;->a:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v1, v1, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 2597079
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->c:Lcom/facebook/messaging/camerautil/CropImage$3;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage$3;->a:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(Landroid/graphics/Bitmap;Z)V

    .line 2597080
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->c:Lcom/facebook/messaging/camerautil/CropImage$3;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage$3;->a:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 2597081
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->c:Lcom/facebook/messaging/camerautil/CropImage$3;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage$3;->a:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v1, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->a:Landroid/graphics/Bitmap;

    .line 2597082
    iput-object v1, v0, Lcom/facebook/messaging/camerautil/CropImage;->I:Landroid/graphics/Bitmap;

    .line 2597083
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->c:Lcom/facebook/messaging/camerautil/CropImage$3;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage$3;->a:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getScale()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 2597084
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->c:Lcom/facebook/messaging/camerautil/CropImage$3;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage$3;->a:Lcom/facebook/messaging/camerautil/CropImage;

    iget-object v0, v0, Lcom/facebook/messaging/camerautil/CropImage;->G:Lcom/facebook/messaging/camerautil/CropImageView;

    invoke-virtual {v0, v2, v2}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(ZZ)V

    .line 2597085
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/CropImage$3$1;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 2597086
    return-void
.end method
