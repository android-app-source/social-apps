.class public Lcom/facebook/messaging/camerautil/MonitoredActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public final p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/messaging/camerautil/MonitoredActivity$LifeCycleListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2597183
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2597184
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/camerautil/MonitoredActivity;->p:Ljava/util/ArrayList;

    .line 2597185
    return-void
.end method


# virtual methods
.method public b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2597186
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2597187
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/MonitoredActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/facebook/messaging/camerautil/MonitoredActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2597188
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0xadb72f7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2597189
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2597190
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/MonitoredActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/camerautil/MonitoredActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IdY;

    .line 2597191
    invoke-virtual {v0}, LX/IdY;->a()V

    .line 2597192
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2597193
    :cond_0
    const v0, -0x117adb23

    invoke-static {v0, v2}, LX/02F;->c(II)V

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x5761610a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2597194
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2597195
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/MonitoredActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/camerautil/MonitoredActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IdY;

    .line 2597196
    invoke-virtual {v0}, LX/IdY;->c()V

    .line 2597197
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2597198
    :cond_0
    const v0, 0x4f39b38f

    invoke-static {v0, v2}, LX/02F;->c(II)V

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, 0x71413609

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2597199
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2597200
    iget-object v0, p0, Lcom/facebook/messaging/camerautil/MonitoredActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/camerautil/MonitoredActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IdY;

    .line 2597201
    invoke-virtual {v0}, LX/IdY;->b()V

    .line 2597202
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2597203
    :cond_0
    const v0, -0x6a212889    # -9.000503E-26f

    invoke-static {v0, v2}, LX/02F;->c(II)V

    return-void
.end method
