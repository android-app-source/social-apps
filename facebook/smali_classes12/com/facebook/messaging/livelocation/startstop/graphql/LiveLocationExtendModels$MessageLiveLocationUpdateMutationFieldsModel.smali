.class public final Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1d200f1b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2600839
    const-class v0, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2600840
    const-class v0, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2600841
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2600842
    return-void
.end method

.method private a()Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2600843
    iget-object v0, p0, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;->e:Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;

    iput-object v0, p0, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;->e:Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;

    .line 2600844
    iget-object v0, p0, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;->e:Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2600845
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2600846
    invoke-direct {p0}, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;->a()Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2600847
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2600848
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2600849
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2600850
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2600851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2600852
    invoke-direct {p0}, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;->a()Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2600853
    invoke-direct {p0}, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;->a()Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;

    .line 2600854
    invoke-direct {p0}, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;->a()Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2600855
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;

    .line 2600856
    iput-object v0, v1, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;->e:Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel$ViewerModel;

    .line 2600857
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2600858
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2600859
    new-instance v0, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/messaging/livelocation/startstop/graphql/LiveLocationExtendModels$MessageLiveLocationUpdateMutationFieldsModel;-><init>()V

    .line 2600860
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2600861
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2600862
    const v0, -0x228475cd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2600863
    const v0, 0x1b50b89c

    return v0
.end method
