.class public Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/6ZZ;


# static fields
.field private static final p:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;


# instance fields
.field private q:LX/6Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2600647
    new-instance v0, LX/2rN;

    invoke-direct {v0}, LX/2rN;-><init>()V

    sget-object v1, LX/0jt;->NEVER_SHOW:LX/0jt;

    .line 2600648
    iput-object v1, v0, LX/2rN;->c:LX/0jt;

    .line 2600649
    move-object v0, v0

    .line 2600650
    const/4 v1, 0x1

    .line 2600651
    iput-boolean v1, v0, LX/2rN;->d:Z

    .line 2600652
    move-object v0, v0

    .line 2600653
    invoke-virtual {v0}, LX/2rN;->e()Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;->p:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2600646
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;LX/6Zb;LX/0i4;)V
    .locals 0

    .prologue
    .line 2600634
    iput-object p1, p0, Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;->q:LX/6Zb;

    iput-object p2, p0, Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;->r:LX/0i4;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;

    invoke-static {v1}, LX/6Zb;->b(LX/0QB;)LX/6Zb;

    move-result-object v0

    check-cast v0, LX/6Zb;

    const-class v2, LX/0i4;

    invoke-interface {v1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/0i4;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;->a(Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;LX/6Zb;LX/0i4;)V

    return-void
.end method


# virtual methods
.method public final a(LX/6ZY;)V
    .locals 4

    .prologue
    .line 2600643
    iget-object v0, p0, Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;->r:LX/0i4;

    invoke-virtual {v0, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    .line 2600644
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    sget-object v2, Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;->p:Lcom/facebook/runtimepermissions/RequestPermissionsConfig;

    new-instance v3, LX/Ig8;

    invoke-direct {v3, p0}, LX/Ig8;-><init>(Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;)V

    invoke-interface {v0, v1, v2, v3}, LX/0i6;->a(Ljava/lang/String;Lcom/facebook/runtimepermissions/RequestPermissionsConfig;LX/6Zx;)V

    .line 2600645
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2600638
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2600639
    invoke-static {p0, p0}, Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2600640
    iget-object v0, p0, Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;->q:LX/6Zb;

    invoke-virtual {v0, p0, p0}, LX/6Zb;->a(Lcom/facebook/base/activity/FbFragmentActivity;LX/6ZZ;)V

    .line 2600641
    iget-object v0, p0, Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;->q:LX/6Zb;

    new-instance v1, LX/2si;

    invoke-direct {v1}, LX/2si;-><init>()V

    const-string v2, "surface_messenger_live_location"

    const-string v3, "mechanism_messenger_live_location_sharing_start"

    invoke-virtual {v0, v1, v2, v3}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    .line 2600642
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x7fdb9c67

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2600635
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2600636
    iget-object v1, p0, Lcom/facebook/messaging/livelocation/startstop/LiveLocationSettingsResolutionActivity;->q:LX/6Zb;

    invoke-virtual {v1}, LX/6Zb;->a()V

    .line 2600637
    const/16 v1, 0x23

    const v2, 0xc1e4dd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
