.class public Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;
.super Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/user/model/UserKey;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/Ie7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2598367
    invoke-direct {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    .line 2598368
    iget-object v0, p0, Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;->p:Lcom/google/common/util/concurrent/SettableFuture;

    sget-object v1, LX/Ie7;->NOTICE_DECLINED:LX/Ie7;

    const v2, 0x17dbb85d

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2598369
    invoke-super {p0}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->S_()Z

    move-result v0

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4368c79b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2598370
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2598371
    if-eqz p1, :cond_0

    .line 2598372
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2598373
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x325d7fd0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x673e154d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2598374
    const v1, 0x7f030b3c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x83ea61d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2598375
    iget-object v0, p0, Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;->p:Lcom/google/common/util/concurrent/SettableFuture;

    sget-object v1, LX/Ie7;->NOTICE_DECLINED:LX/Ie7;

    const v2, -0x3187e431

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2598376
    invoke-super {p0, p1}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2598377
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2598378
    invoke-super {p0, p1, p2}, Lcom/facebook/messaging/fullscreendialog/FullScreenDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2598379
    const p1, 0x7f0d1c4f

    invoke-virtual {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/user/tiles/UserTileView;

    .line 2598380
    iget-object p2, p0, Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;->m:Lcom/facebook/user/model/UserKey;

    invoke-static {p2}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2598381
    const v0, 0x7f0d1c50

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2598382
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0839ff

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget-object p2, p0, Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;->n:Ljava/lang/String;

    aput-object p2, v3, p1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2598383
    const v0, 0x7f0d1c51

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2598384
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083a00

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget-object p2, p0, Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;->o:Ljava/lang/String;

    aput-object p2, v3, p1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2598385
    const v0, 0x7f0d1c52

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/IeA;

    invoke-direct {v1, p0}, LX/IeA;-><init>(Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2598386
    const v0, 0x7f0d1c4e

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2598387
    new-instance v1, LX/IeB;

    invoke-direct {v1, p0}, LX/IeB;-><init>(Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2598388
    return-void
.end method
