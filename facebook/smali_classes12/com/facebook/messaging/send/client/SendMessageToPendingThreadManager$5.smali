.class public final Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;)V
    .locals 0

    .prologue
    .line 2623203
    iput-object p1, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager$5;->a:Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2623204
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager$5;->a:Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2623205
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager$5;->a:Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;

    .line 2623206
    iget-object v1, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->d:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 2623207
    iget-object v1, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledFuture;

    .line 2623208
    const/4 p0, 0x1

    invoke-interface {v1, p0}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    goto :goto_0

    .line 2623209
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2623210
    return-void
.end method
