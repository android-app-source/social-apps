.class public Lcom/facebook/messaging/send/client/SendMessageManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final ad:Ljava/lang/Object;


# instance fields
.field private A:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/media/upload/MediaUploadManager;",
            ">;"
        }
    .end annotation
.end field

.field public B:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3QL;",
            ">;"
        }
    .end annotation
.end field

.field private C:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;",
            ">;"
        }
    .end annotation
.end field

.field public D:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;",
            ">;"
        }
    .end annotation
.end field

.field private E:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMw;",
            ">;"
        }
    .end annotation
.end field

.field private F:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Lw;",
            ">;"
        }
    .end annotation
.end field

.field private G:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FGm;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/2Mv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public I:LX/2My;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final J:Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;

.field private final K:LX/4z1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4z1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadQueuingLock"
    .end annotation
.end field

.field private final L:LX/4z1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4z1",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadQueuingLock"
    .end annotation
.end field

.field public final M:LX/It2;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadQueuingLock"
    .end annotation
.end field

.field private final N:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final O:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field private final P:LX/0Yb;

.field private final Q:LX/0Yb;

.field private final R:LX/0Yb;

.field public final S:LX/Isw;

.field public T:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadQueuingLock"
    .end annotation
.end field

.field private U:Ljava/lang/String;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadQueuingLock"
    .end annotation
.end field

.field private final V:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/send/PendingSendQueueKey;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadQueuingLock"
    .end annotation
.end field

.field private final W:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadQueuingLock"
    .end annotation
.end field

.field public final X:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadQueuingLock"
    .end annotation
.end field

.field public final Y:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadQueuingLock"
    .end annotation
.end field

.field private final Z:I

.field public final a:[Ljava/lang/Object;

.field private aa:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mThreadQueuingLock"
    .end annotation
.end field

.field public final ab:LX/03V;

.field private ac:Ljava/lang/Runnable;

.field private final b:LX/2Ow;

.field public final c:LX/0aG;

.field private final d:LX/0Sh;

.field public final e:Ljava/util/concurrent/ScheduledExecutorService;

.field public final f:Ljava/util/concurrent/Executor;

.field private final g:Ljava/util/concurrent/ExecutorService;

.field private final h:LX/0So;

.field public final i:LX/1sj;

.field private final j:LX/FKF;

.field private final k:LX/2Mk;

.field private final l:LX/2Ly;

.field private final m:LX/0Xl;

.field private final n:LX/IYt;

.field public final o:LX/0Zb;

.field private final p:LX/0SG;

.field public final q:LX/2MA;

.field private final r:LX/ItV;

.field private final s:LX/2Mq;

.field private final t:LX/7V0;

.field private final u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/ItB;",
            ">;"
        }
    .end annotation
.end field

.field public final v:LX/0So;

.field public final w:LX/6f6;

.field private final x:LX/0Uh;

.field public final y:LX/0TD;

.field public final z:LX/Itk;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2622728
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/send/client/SendMessageManager;->ad:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(LX/2Ow;LX/0aG;LX/0Sh;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ExecutorService;LX/0So;LX/1sj;LX/FKF;LX/2Mk;LX/2Ly;LX/0Xl;LX/0Zb;LX/0SG;LX/IYt;LX/2MA;LX/03V;LX/Isw;LX/ItV;LX/2Mq;LX/7V0;LX/0Or;LX/0So;LX/6f6;LX/0Uh;Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;LX/0TD;LX/Itk;)V
    .locals 6
    .param p4    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .param p12    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .param p14    # LX/0SG;
        .annotation runtime Lcom/facebook/messaging/database/threads/NeedsDbClock;
        .end annotation
    .end param
    .param p23    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .param p27    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/SendMessageAsyncExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Ow;",
            "LX/0aG;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0So;",
            "LX/1sj;",
            "LX/FKF;",
            "LX/2Mk;",
            "LX/2Ly;",
            "LX/0Xl;",
            "LX/0Zb;",
            "LX/0SG;",
            "LX/IYt;",
            "LX/2MA;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/Isw;",
            "LX/ItV;",
            "LX/2Mq;",
            "LX/7V0;",
            "LX/0Or",
            "<",
            "LX/ItB;",
            ">;",
            "LX/0So;",
            "LX/6f6;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;",
            "LX/0TD;",
            "LX/Itk;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2622729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2622730
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    .line 2622731
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->A:LX/0Ot;

    .line 2622732
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->B:LX/0Ot;

    .line 2622733
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->C:LX/0Ot;

    .line 2622734
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->D:LX/0Ot;

    .line 2622735
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->E:LX/0Ot;

    .line 2622736
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->F:LX/0Ot;

    .line 2622737
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->G:LX/0Ot;

    .line 2622738
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->V:Ljava/util/Set;

    .line 2622739
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->W:Ljava/util/Set;

    .line 2622740
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->X:Ljava/util/List;

    .line 2622741
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Y:Ljava/util/Set;

    .line 2622742
    const/16 v2, 0xa

    iput v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Z:I

    .line 2622743
    iput-object p1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->b:LX/2Ow;

    .line 2622744
    iput-object p2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->c:LX/0aG;

    .line 2622745
    iput-object p3, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->d:LX/0Sh;

    .line 2622746
    iput-object p4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->e:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2622747
    iput-object p5, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->f:Ljava/util/concurrent/Executor;

    .line 2622748
    iput-object p6, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->g:Ljava/util/concurrent/ExecutorService;

    .line 2622749
    iput-object p7, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->h:LX/0So;

    .line 2622750
    iput-object p8, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->i:LX/1sj;

    .line 2622751
    iput-object p9, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    .line 2622752
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->k:LX/2Mk;

    .line 2622753
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->l:LX/2Ly;

    .line 2622754
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->m:LX/0Xl;

    .line 2622755
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->o:LX/0Zb;

    .line 2622756
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->p:LX/0SG;

    .line 2622757
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->n:LX/IYt;

    .line 2622758
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->t:LX/7V0;

    .line 2622759
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->x:LX/0Uh;

    .line 2622760
    invoke-static {}, LX/4z1;->u()LX/4z1;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->K:LX/4z1;

    .line 2622761
    invoke-static {}, LX/4z1;->u()LX/4z1;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->L:LX/4z1;

    .line 2622762
    new-instance v2, LX/It2;

    invoke-direct {v2, p7}, LX/It2;-><init>(LX/0So;)V

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    .line 2622763
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v2

    const-wide/16 v4, 0x12c

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v2

    invoke-virtual {v2}, LX/0QN;->q()LX/0QI;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->N:LX/0QI;

    .line 2622764
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v2

    const-wide/16 v4, 0x258

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v2

    invoke-virtual {v2}, LX/0QN;->q()LX/0QI;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->O:LX/0QI;

    .line 2622765
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->m:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.push.mqtt.ACTION_CHANNEL_STATE_CHANGED"

    new-instance v4, LX/ItH;

    invoke-direct {v4, p0}, LX/ItH;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    invoke-interface {v2}, LX/0YX;->a()LX/0Yb;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->P:LX/0Yb;

    .line 2622766
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->m:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.orca.media.upload.MEDIA_UPLOAD_STATUS_CHANGED"

    new-instance v4, LX/ItI;

    invoke-direct {v4, p0}, LX/ItI;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    invoke-interface {v2}, LX/0YX;->a()LX/0Yb;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Q:LX/0Yb;

    .line 2622767
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Q:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->b()V

    .line 2622768
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->m:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    sget-object v3, LX/0aY;->k:Ljava/lang/String;

    new-instance v4, LX/ItJ;

    invoke-direct {v4, p0}, LX/ItJ;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    invoke-interface {v2}, LX/0YX;->a()LX/0Yb;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->R:LX/0Yb;

    .line 2622769
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->R:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->b()V

    .line 2622770
    new-instance v2, Lcom/facebook/messaging/send/client/SendMessageManager$4;

    invoke-direct {v2, p0}, Lcom/facebook/messaging/send/client/SendMessageManager$4;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->ac:Ljava/lang/Runnable;

    .line 2622771
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->q:LX/2MA;

    .line 2622772
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->ab:LX/03V;

    .line 2622773
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->S:LX/Isw;

    .line 2622774
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->r:LX/ItV;

    .line 2622775
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    .line 2622776
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->u:LX/0Or;

    .line 2622777
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->v:LX/0So;

    .line 2622778
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->w:LX/6f6;

    .line 2622779
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->J:Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;

    .line 2622780
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->J:Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;

    invoke-virtual {v2}, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->a()V

    .line 2622781
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->y:LX/0TD;

    .line 2622782
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->z:LX/Itk;

    .line 2622783
    return-void
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/send/trigger/NavigationTrigger;)Lcom/facebook/messaging/model/messages/Message;
    .locals 4
    .param p2    # Lcom/facebook/messaging/send/trigger/NavigationTrigger;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x24

    .line 2622784
    const-string v0, "SendMessageManager.prepareMessageForSend"

    const v1, -0x3c025cd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2622785
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2}, LX/2Mq;->a(Ljava/lang/String;S)V

    .line 2622786
    :try_start_0
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    .line 2622787
    if-eqz p2, :cond_0

    .line 2622788
    invoke-virtual {p2}, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b()Ljava/lang/String;

    move-result-object v1

    .line 2622789
    if-eqz v1, :cond_0

    .line 2622790
    const-string v2, "trigger"

    invoke-virtual {v0, v2, v1}, LX/6f7;->a(Ljava/lang/String;Ljava/lang/String;)LX/6f7;

    .line 2622791
    iget-object v1, p2, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2622792
    if-eqz v1, :cond_0

    .line 2622793
    const-string v2, "ld"

    invoke-virtual {v0, v2, v1}, LX/6f7;->a(Ljava/lang/String;Ljava/lang/String;)LX/6f7;

    .line 2622794
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v1

    .line 2622795
    iput-object v1, v0, LX/6f7;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    .line 2622796
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2622797
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/2Mq;->a(Ljava/lang/String;S)V

    .line 2622798
    const v1, 0x436d45d1

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 2622799
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/2Mq;->a(Ljava/lang/String;S)V

    .line 2622800
    const v1, -0x3ad36fce

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private static a(Lcom/facebook/messaging/send/client/SendMessageManager;LX/It3;)Lcom/facebook/messaging/model/messages/Message;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2622801
    invoke-virtual {p1}, LX/It3;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v2

    .line 2622802
    :goto_0
    return-object v0

    .line 2622803
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->h:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 2622804
    iget-wide v8, p1, LX/It3;->i:J

    move-wide v4, v8

    .line 2622805
    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    .line 2622806
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->q:LX/2MA;

    sget-object v1, LX/8Bk;->MQTT:LX/8Bk;

    invoke-interface {v0, v1}, LX/2MA;->b(LX/8Bk;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2622807
    const-wide/16 v0, 0x0

    .line 2622808
    iput-wide v0, p1, LX/It3;->i:J

    .line 2622809
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->ab:LX/03V;

    const-string v1, "SendMessageManager"

    const-string v3, "Message got queued for reconnection retry even when mqtt is connected."

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v0, v2

    .line 2622810
    goto :goto_0

    .line 2622811
    :cond_2
    const-wide/16 v6, 0x3e8

    add-long/2addr v0, v6

    cmp-long v0, v0, v4

    if-gez v0, :cond_3

    move-object v0, v2

    .line 2622812
    goto :goto_0

    .line 2622813
    :cond_3
    iget-object v3, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v3

    .line 2622814
    :try_start_0
    invoke-virtual {p1}, LX/It3;->n()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2622815
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->A:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/messaging/model/messages/Message;)LX/FHZ;

    move-result-object v1

    iget-object v1, v1, LX/FHZ;->b:LX/FHY;

    .line 2622816
    sget-object v4, LX/ItG;->b:[I

    invoke-virtual {v1}, LX/FHY;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2622817
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Unknown state: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2622818
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2622819
    :pswitch_0
    :try_start_1
    monitor-exit v3

    goto :goto_0

    .line 2622820
    :pswitch_1
    monitor-exit v3

    move-object v0, v2

    goto :goto_0

    .line 2622821
    :pswitch_2
    monitor-exit v3

    goto :goto_0

    .line 2622822
    :cond_4
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    .line 2622823
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Ljava/lang/Throwable;)Lcom/facebook/messaging/model/messages/Message;
    .locals 1

    .prologue
    .line 2622824
    instance-of v0, p0, LX/FKG;

    if-eqz v0, :cond_0

    .line 2622825
    check-cast p0, LX/FKG;

    iget-object v0, p0, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    .line 2622826
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/send/client/SendMessageManager;
    .locals 7

    .prologue
    .line 2622827
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2622828
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2622829
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2622830
    if-nez v1, :cond_0

    .line 2622831
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2622832
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2622833
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2622834
    sget-object v1, Lcom/facebook/messaging/send/client/SendMessageManager;->ad:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2622835
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2622836
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2622837
    :cond_1
    if-nez v1, :cond_4

    .line 2622838
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2622839
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2622840
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->b(LX/0QB;)Lcom/facebook/messaging/send/client/SendMessageManager;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2622841
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2622842
    if-nez v1, :cond_2

    .line 2622843
    sget-object v0, Lcom/facebook/messaging/send/client/SendMessageManager;->ad:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/client/SendMessageManager;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2622844
    :goto_1
    if-eqz v0, :cond_3

    .line 2622845
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2622846
    :goto_3
    check-cast v0, Lcom/facebook/messaging/send/client/SendMessageManager;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2622847
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2622848
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2622849
    :catchall_1
    move-exception v0

    .line 2622850
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2622851
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2622852
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2622853
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/send/client/SendMessageManager;->ad:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/client/SendMessageManager;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2622854
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->O:LX/0QI;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2622855
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2622856
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "send_failure"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2622857
    invoke-static {v0, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2622858
    if-eqz p2, :cond_0

    .line 2622859
    const-string v2, "error_message"

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2622860
    :cond_0
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->o:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2622861
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622862
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->S:LX/Isw;

    invoke-virtual {v0}, LX/Isw;->a()V

    .line 2622863
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;Ljava/lang/Throwable;)V

    .line 2622864
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622865
    invoke-static {p2}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Ljava/lang/Throwable;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2622866
    if-eqz v0, :cond_1

    .line 2622867
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->b:LX/2Ow;

    .line 2622868
    new-instance v2, Landroid/content/Intent;

    sget-object v3, LX/0aY;->P:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2622869
    const-string v3, "outgoing_message"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2622870
    invoke-static {v1, v2}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 2622871
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->b:LX/2Ow;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2622872
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v2, 0xcc

    invoke-virtual {v0, v1, v2}, LX/FKF;->b(Ljava/lang/String;I)V

    .line 2622873
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ItB;

    invoke-virtual {v0, p1, p2}, LX/ItB;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/Throwable;)V

    .line 2622874
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622875
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 2622876
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2622877
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2622878
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622879
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v0, p1}, LX/It2;->b(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v0

    .line 2622880
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622881
    if-eqz v0, :cond_0

    .line 2622882
    invoke-virtual {v0}, LX/It3;->h()V

    .line 2622883
    invoke-virtual {v0}, LX/It3;->n()Ljava/util/List;

    move-result-object v0

    .line 2622884
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2622885
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->O:LX/0QI;

    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v2, v3, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2622886
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v3, 0xcc

    invoke-virtual {v2, v0, v3}, LX/FKF;->b(Ljava/lang/String;I)V

    goto :goto_0

    .line 2622887
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2622888
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 4

    .prologue
    .line 2622889
    iget-object v0, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->GROUP:LX/5e9;

    if-ne v0, v1, :cond_0

    .line 2622890
    const-string v0, "thread_fbid"

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2622891
    :goto_0
    return-void

    .line 2622892
    :cond_0
    const-string v0, "other_user_id"

    iget-wide v2, p0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/messaging/send/client/SendMessageManager;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2Mv;LX/2My;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/send/client/SendMessageManager;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/media/upload/MediaUploadManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3QL;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FMw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Lw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FGm;",
            ">;",
            "LX/2Mv;",
            "LX/2My;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2622893
    iput-object p1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->A:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->B:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->C:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->D:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->E:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->F:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->G:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->H:LX/2Mv;

    iput-object p9, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->I:LX/2My;

    return-void
.end method

.method private static a(Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2622894
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622895
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->X:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2622896
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2622897
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2622898
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2622899
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2622900
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private static a(Ljava/util/Set;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2622901
    const-string v0, "SendMessageManager.removeEquivalentMessage"

    const v1, 0x15221ea8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2622902
    :try_start_0
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2622903
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2622904
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2622905
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2622906
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2622907
    :catchall_0
    move-exception v0

    const v1, -0x49a7d9

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    const v0, -0x588d4345

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2622908
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 2622930
    const-string v0, "event"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, LX/2EU;->fromValue(I)LX/2EU;

    move-result-object v0

    .line 2622931
    sget-object v1, LX/ItG;->c:[I

    invoke-virtual {v0}, LX/2EU;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2622932
    :goto_0
    return-void

    .line 2622933
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->S:LX/Isw;

    invoke-virtual {v0}, LX/Isw;->b()V

    .line 2622934
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622935
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v0}, LX/It2;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It3;

    .line 2622936
    const-wide/16 v4, 0x0

    .line 2622937
    iput-wide v4, v0, LX/It3;->i:J

    .line 2622938
    goto :goto_1

    .line 2622939
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2622940
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/send/PendingSendQueueKey;Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/send/PendingSendQueueKey;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2623106
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2623107
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->X:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 2623108
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v0, p1}, LX/It2;->c(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v2

    .line 2623109
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->X:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->X:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v0, v3}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v3

    .line 2623110
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2623111
    invoke-interface {v3}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2623112
    iget-object v4, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {p2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2623113
    invoke-virtual {v2, v0}, LX/It3;->b(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_0

    .line 2623114
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2623115
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->X:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2623116
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2623088
    invoke-static {p0, p2}, Lcom/facebook/messaging/send/client/SendMessageManager;->n(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2623089
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    .line 2623090
    :goto_0
    return-void

    .line 2623091
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2623092
    :try_start_0
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->V:Ljava/util/Set;

    invoke-virtual {p0, p2}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2623093
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->T:Z

    .line 2623094
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->W:Ljava/util/Set;

    invoke-interface {v2, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2623095
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->U:Ljava/lang/String;

    .line 2623096
    iget-boolean v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->aa:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Y:Ljava/util/Set;

    iget-object v3, p2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 2623097
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->aa:Z

    .line 2623098
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Y:Ljava/util/Set;

    iget-object v3, p2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2623099
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2623100
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2623101
    if-eqz v0, :cond_3

    .line 2623102
    invoke-static {p0, p2}, Lcom/facebook/messaging/send/client/SendMessageManager;->t(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2623103
    :goto_1
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    goto :goto_0

    .line 2623104
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2623105
    :cond_3
    invoke-static {p0, p1, p2}, Lcom/facebook/messaging/send/client/SendMessageManager;->b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/NewMessageResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2623046
    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v4

    .line 2623047
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->T:Z

    move v3, v2

    .line 2623048
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 2623049
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2623050
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2623051
    iget-object v5, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 2623052
    iget-object v6, v1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v6, v6

    .line 2623053
    iget-object v6, v6, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 2623054
    iget-object v5, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 2623055
    iget-boolean v6, v1, Lcom/facebook/messaging/service/model/NewMessageResult;->e:Z

    move v6, v6

    .line 2623056
    if-eqz v6, :cond_0

    .line 2623057
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    const/16 v6, 0xcb

    invoke-virtual {v1, v5, v6}, LX/FKF;->b(Ljava/lang/String;I)V

    .line 2623058
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->i:LX/1sj;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    sget-object v6, LX/2gR;->RESPONSE_SEND:LX/2gR;

    const/4 v7, 0x0

    invoke-virtual {v1, v0, v6, v7}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2623059
    invoke-static {p0, v5}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/lang/String;)V

    .line 2623060
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2623061
    :cond_0
    iget-object v6, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->i:LX/1sj;

    iget-object v7, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    sget-object v8, LX/2gR;->RESPONSE_SEND:LX/2gR;

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2623062
    invoke-static {p0, v5}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/lang/String;)V

    .line 2623063
    iget-object v6, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Y:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v5

    .line 2623064
    if-eqz v5, :cond_1

    .line 2623065
    iget-object v0, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->t(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_1

    .line 2623066
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2623067
    :cond_1
    :try_start_1
    new-instance v5, LX/FKG;

    .line 2623068
    iget-object v6, v1, Lcom/facebook/messaging/service/model/NewMessageResult;->f:Ljava/lang/String;

    move-object v6, v6

    .line 2623069
    iget-object v7, v1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v7, v7

    .line 2623070
    invoke-direct {v5, v6, v7}, LX/FKG;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2623071
    iget-object v6, v1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v6

    .line 2623072
    invoke-direct {p0, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->o(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2623073
    iget-object v0, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-direct {p0, v0, v5}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_2
    move v1, v2

    .line 2623074
    goto :goto_2

    .line 2623075
    :cond_3
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v0

    .line 2623076
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Y:Ljava/util/Set;

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->a$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/send/PendingSendQueueKey;Ljava/util/Set;)V

    .line 2623077
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v1, v0}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v1

    .line 2623078
    if-eqz v1, :cond_4

    .line 2623079
    invoke-static {v1}, LX/It3;->o(LX/It3;)V

    .line 2623080
    invoke-virtual {v1}, LX/It3;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2623081
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v1, v0}, LX/It2;->b(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    .line 2623082
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Y:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2623083
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2623084
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->S:LX/Isw;

    invoke-virtual {v0}, LX/Isw;->b()V

    .line 2623085
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->h(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    .line 2623086
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    .line 2623087
    return-void
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/send/client/SendMessageManager;
    .locals 31

    .prologue
    .line 2623043
    new-instance v2, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static/range {p0 .. p0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v3

    check-cast v3, LX/2Ow;

    invoke-static/range {p0 .. p0}, LX/0aF;->getInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static/range {p0 .. p0}, LX/0T9;->a(LX/0QB;)Ljava/util/concurrent/Executor;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/0UA;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v9

    check-cast v9, LX/0So;

    invoke-static/range {p0 .. p0}, LX/1sj;->a(LX/0QB;)LX/1sj;

    move-result-object v10

    check-cast v10, LX/1sj;

    invoke-static/range {p0 .. p0}, LX/FKF;->a(LX/0QB;)LX/FKF;

    move-result-object v11

    check-cast v11, LX/FKF;

    invoke-static/range {p0 .. p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v12

    check-cast v12, LX/2Mk;

    invoke-static/range {p0 .. p0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v13

    check-cast v13, LX/2Ly;

    invoke-static/range {p0 .. p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v14

    check-cast v14, LX/0Xl;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v15

    check-cast v15, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/2N6;->a(LX/0QB;)LX/2N6;

    move-result-object v16

    check-cast v16, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/IYt;->a(LX/0QB;)LX/IYt;

    move-result-object v17

    check-cast v17, LX/IYt;

    invoke-static/range {p0 .. p0}, LX/2M4;->a(LX/0QB;)LX/2MA;

    move-result-object v18

    check-cast v18, LX/2MA;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v19

    check-cast v19, LX/03V;

    invoke-static/range {p0 .. p0}, LX/Isw;->a(LX/0QB;)LX/Isw;

    move-result-object v20

    check-cast v20, LX/Isw;

    invoke-static/range {p0 .. p0}, LX/ItV;->a(LX/0QB;)LX/ItV;

    move-result-object v21

    check-cast v21, LX/ItV;

    invoke-static/range {p0 .. p0}, LX/2Mq;->a(LX/0QB;)LX/2Mq;

    move-result-object v22

    check-cast v22, LX/2Mq;

    invoke-static/range {p0 .. p0}, LX/7V1;->a(LX/0QB;)LX/7V1;

    move-result-object v23

    check-cast v23, LX/7V0;

    const/16 v24, 0x2922

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v24

    invoke-static/range {p0 .. p0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v25

    check-cast v25, LX/0So;

    invoke-static/range {p0 .. p0}, LX/6f6;->a(LX/0QB;)LX/6f6;

    move-result-object v26

    check-cast v26, LX/6f6;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v27

    check-cast v27, LX/0Uh;

    invoke-static/range {p0 .. p0}, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;->a(LX/0QB;)Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;

    move-result-object v28

    check-cast v28, Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;

    invoke-static/range {p0 .. p0}, LX/44h;->a(LX/0QB;)LX/0TD;

    move-result-object v29

    check-cast v29, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/Itk;->a(LX/0QB;)LX/Itk;

    move-result-object v30

    check-cast v30, LX/Itk;

    invoke-direct/range {v2 .. v30}, Lcom/facebook/messaging/send/client/SendMessageManager;-><init>(LX/2Ow;LX/0aG;LX/0Sh;Ljava/util/concurrent/ScheduledExecutorService;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ExecutorService;LX/0So;LX/1sj;LX/FKF;LX/2Mk;LX/2Ly;LX/0Xl;LX/0Zb;LX/0SG;LX/IYt;LX/2MA;LX/03V;LX/Isw;LX/ItV;LX/2Mq;LX/7V0;LX/0Or;LX/0So;LX/6f6;LX/0Uh;Lcom/facebook/messaging/tincan/messenger/AttachmentUploadCompleteListener;LX/0TD;LX/Itk;)V

    .line 2623044
    const/16 v3, 0xd4e

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xcd1

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2a31

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2924

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x299b

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xcce

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x27f8

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/2Mv;->a(LX/0QB;)LX/2Mv;

    move-result-object v10

    check-cast v10, LX/2Mv;

    invoke-static/range {p0 .. p0}, LX/2My;->a(LX/0QB;)LX/2My;

    move-result-object v11

    check-cast v11, LX/2My;

    invoke-static/range {v2 .. v11}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/send/client/SendMessageManager;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2Mv;LX/2My;)V

    .line 2623045
    return-object v2
.end method

.method public static b(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2623028
    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2623029
    :goto_0
    return-void

    .line 2623030
    :cond_0
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v2

    .line 2623031
    const/4 v1, 0x0

    .line 2623032
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->L:LX/4z1;

    invoke-virtual {v0, p1}, LX/0vW;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 2623033
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 2623034
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2623035
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2623036
    invoke-static {v0}, LX/2Mk;->r(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {p2, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2623037
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 2623038
    const/4 v0, 0x1

    :goto_2
    move v1, v0

    .line 2623039
    goto :goto_1

    .line 2623040
    :cond_1
    if-eqz v1, :cond_2

    .line 2623041
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->b:LX/2Ow;

    invoke-virtual {v0, p1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2623042
    :cond_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private b(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2623018
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v2

    .line 2623019
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->X:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2623020
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2623021
    monitor-exit v2

    move v0, v1

    .line 2623022
    :goto_0
    return v0

    .line 2623023
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->W:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2623024
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2623025
    monitor-exit v2

    move v0, v1

    goto :goto_0

    .line 2623026
    :cond_3
    const/4 v0, 0x0

    monitor-exit v2

    goto :goto_0

    .line 2623027
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;)V
    .locals 3

    .prologue
    .line 2623014
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->x:LX/0Uh;

    const/16 v1, 0x1c1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2623015
    invoke-direct {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->c()V

    .line 2623016
    :goto_0
    return-void

    .line 2623017
    :cond_0
    invoke-direct {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->d()V

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 2

    .prologue
    .line 2623003
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->q:LX/2MA;

    sget-object v1, LX/8Bk;->MQTT:LX/8Bk;

    invoke-interface {v0, v1}, LX/2MA;->b(LX/8Bk;)Z

    move-result v0

    .line 2623004
    invoke-static {p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Ljava/lang/Throwable;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2623005
    invoke-direct {p0, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->o(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2623006
    if-eqz v0, :cond_1

    .line 2623007
    invoke-direct {p0, p2}, Lcom/facebook/messaging/send/client/SendMessageManager;->r(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2623008
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->P:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2623009
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->P:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2623010
    :cond_0
    :goto_1
    return-void

    .line 2623011
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ItB;

    invoke-virtual {v0, p2}, LX/ItB;->b(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2623012
    invoke-direct {p0, p2}, Lcom/facebook/messaging/send/client/SendMessageManager;->s(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_0

    .line 2623013
    :cond_2
    invoke-direct {p0, p2, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2622982
    const-string v0, "SendMessageManager.maybeSendAnotherMessageSync"

    const v1, 0x3c3e46cf

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2622983
    :try_start_0
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2622984
    :try_start_1
    iget-boolean v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->T:Z

    if-eqz v0, :cond_0

    .line 2622985
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2622986
    const v0, 0xbdd482d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2622987
    :goto_0
    return-void

    .line 2622988
    :cond_0
    :try_start_2
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->f(Lcom/facebook/messaging/send/client/SendMessageManager;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2622989
    if-nez v0, :cond_1

    .line 2622990
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2622991
    const v0, 0x5a7406

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 2622992
    :cond_1
    const/4 v2, 0x1

    :try_start_3
    iput-boolean v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->T:Z

    .line 2622993
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2622994
    :try_start_4
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->H:LX/2Mv;

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v2}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v0}, LX/2Mk;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2622995
    :cond_2
    invoke-static {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->i(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2622996
    const v0, -0x4157f3ec

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 2622997
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2622998
    :catchall_1
    move-exception v0

    const v1, -0xfb05c13

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2622999
    :cond_3
    :try_start_7
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->x:LX/0Uh;

    const/16 v2, 0x1c2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2623000
    invoke-direct {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->j(Lcom/facebook/messaging/model/messages/Message;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2623001
    :goto_1
    const v0, 0x1835478

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 2623002
    :cond_4
    :try_start_8
    invoke-static {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->i(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1
.end method

.method private d()V
    .locals 11

    .prologue
    .line 2622909
    const-string v0, "SendMessageManager.maybeSendAnotherMessageAsync"

    const v1, -0x6f3bf9f6

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2622910
    :try_start_0
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2622911
    :try_start_1
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->e(Lcom/facebook/messaging/send/client/SendMessageManager;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2622912
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2622913
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2622914
    const v0, 0x61cd64b6

    invoke-static {v0}, LX/02m;->a(I)V

    :goto_0
    return-void

    .line 2622915
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->W:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2622916
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2622917
    :try_start_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2622918
    invoke-static {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->k(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/service/model/SendMessageParams;

    move-result-object v10

    .line 2622919
    iget-object v4, v10, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2622920
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2622921
    const-string v4, "sendMessageParams"

    invoke-virtual {v6, v4, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2622922
    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->c:LX/0aG;

    const-string v5, "handle_media_db"

    sget-object v7, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v8, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, 0x7d455b62

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    .line 2622923
    :cond_1
    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->I:LX/2My;

    invoke-virtual {v4}, LX/2My;->c()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->H:LX/2Mv;

    iget-object v5, v10, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4, v5}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2622924
    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->y:LX/0TD;

    new-instance v5, LX/ItC;

    invoke-direct {v5, p0, v10}, LX/ItC;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/service/model/SendMessageParams;)V

    invoke-interface {v4, v5}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2622925
    :goto_2
    new-instance v5, Lcom/facebook/messaging/send/client/SendMessageManager$12;

    invoke-direct {v5, p0, v10, v0}, Lcom/facebook/messaging/send/client/SendMessageManager$12;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/messaging/model/messages/Message;)V

    iget-object v6, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->y:LX/0TD;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2622926
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2622927
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2622928
    :catchall_1
    move-exception v0

    const v1, -0x786cd2e6

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_2
    const v0, -0x1b955be5

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    .line 2622929
    :cond_3
    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->y:LX/0TD;

    new-instance v5, LX/ItD;

    invoke-direct {v5, p0, v10}, LX/ItD;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/service/model/SendMessageParams;)V

    invoke-interface {v4, v5}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    goto :goto_2
.end method

.method public static d$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 8

    .prologue
    const/16 v7, 0x26

    .line 2622941
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2622942
    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v6

    .line 2622943
    const-string v2, "SendMessageManager.startAsyncSend%s"

    if-eqz v6, :cond_4

    const-string v0, "Sms"

    :goto_0
    const v3, 0x7fde1822

    invoke-static {v2, v0, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 2622944
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v3, 0x25

    invoke-virtual {v0, v2, v3}, LX/2Mq;->a(Ljava/lang/String;S)V

    .line 2622945
    :try_start_0
    if-nez v6, :cond_1

    .line 2622946
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->r:LX/ItV;

    .line 2622947
    iget-object v2, v0, LX/ItV;->i:LX/01T;

    sget-object v3, LX/01T;->PAA:LX/01T;

    if-ne v2, v3, :cond_5

    .line 2622948
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ItB;

    invoke-virtual {v0, p1}, LX/ItB;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622949
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2622950
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->L:LX/4z1;

    invoke-virtual {v0, v1}, LX/0vW;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 2622951
    iget-object v3, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->K:LX/4z1;

    invoke-virtual {v3, v1, p1}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2622952
    invoke-static {v0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Ljava/util/Set;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622953
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2622954
    :cond_1
    :try_start_2
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2622955
    const-string v0, "outgoingMessage"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2622956
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->c:LX/0aG;

    const-string v1, "insert_pending_sent_message"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x590abb47

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2622957
    new-instance v1, LX/ItK;

    invoke-direct {v1, p0, p1}, LX/ItK;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2622958
    if-nez v6, :cond_2

    .line 2622959
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->N:LX/0QI;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->h:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2622960
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2622961
    :try_start_3
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/It2;->c(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v0

    .line 2622962
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2622963
    :try_start_4
    invoke-virtual {v0, p1}, LX/It3;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622964
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v2, 0x29

    invoke-virtual {v0, v1, v2}, LX/2Mq;->a(Ljava/lang/String;S)V

    .line 2622965
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v2, 0xc9

    invoke-virtual {v0, v1, v2}, LX/FKF;->b(Ljava/lang/String;I)V

    .line 2622966
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    .line 2622967
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    sget-object v1, LX/6fR;->PAYMENT:LX/6fR;

    if-ne v0, v1, :cond_3

    .line 2622968
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const-string v2, "payment_transfer"

    invoke-virtual {v0, v1, v2}, LX/2Mq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2622969
    :cond_3
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v7}, LX/2Mq;->a(Ljava/lang/String;S)V

    .line 2622970
    const v0, 0x2de35f42

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2622971
    return-void

    .line 2622972
    :cond_4
    const-string v0, ""

    goto/16 :goto_0

    .line 2622973
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2622974
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v7}, LX/2Mq;->a(Ljava/lang/String;S)V

    .line 2622975
    const v1, 0x6fb48f07

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2622976
    throw v0

    .line 2622977
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2622978
    :cond_5
    iget-object v2, v0, LX/ItV;->k:LX/ItU;

    invoke-static {v2}, LX/ItU;->isCompleted(LX/ItU;)Z

    move-result v2

    move v2, v2

    .line 2622979
    if-nez v2, :cond_0

    .line 2622980
    sget-object v2, LX/ItU;->ABORTED:LX/ItU;

    iput-object v2, v0, LX/ItV;->k:LX/ItU;

    .line 2622981
    iget-object v2, v0, LX/ItV;->d:LX/0TD;

    new-instance v3, Lcom/facebook/messaging/send/client/StartupRetryManager$3;

    invoke-direct {v3, v0}, Lcom/facebook/messaging/send/client/StartupRetryManager$3;-><init>(LX/ItV;)V

    const v4, 0x3565b857

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto/16 :goto_1
.end method

.method private static e(Lcom/facebook/messaging/send/client/SendMessageManager;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2622709
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2622710
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v2

    .line 2622711
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v0}, LX/It2;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It3;

    .line 2622712
    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->V:Ljava/util/Set;

    iget-object v5, v0, LX/It3;->a:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2622713
    invoke-static {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/send/client/SendMessageManager;LX/It3;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2622714
    if-eqz v0, :cond_0

    invoke-static {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->l(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2622715
    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->V:Ljava/util/Set;

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2622716
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2622717
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2622718
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method private static f(Lcom/facebook/messaging/send/client/SendMessageManager;)Lcom/facebook/messaging/model/messages/Message;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2622719
    move-object v0, v1

    .line 2622720
    :goto_0
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->i(Lcom/facebook/messaging/send/client/SendMessageManager;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 2622721
    if-nez v2, :cond_0

    .line 2622722
    :goto_1
    return-object v1

    .line 2622723
    :cond_0
    if-ne v2, v0, :cond_1

    .line 2622724
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Message repeated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2622725
    :cond_1
    invoke-static {p0, v2}, Lcom/facebook/messaging/send/client/SendMessageManager;->l(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v1, v2

    .line 2622726
    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 2622727
    goto :goto_0
.end method

.method public static f(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 2622329
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622330
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->K:LX/4z1;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2, p1}, LX/0Xt;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 2622331
    if-eqz v0, :cond_0

    .line 2622332
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->L:LX/4z1;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2, p1}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2622333
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622334
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v2, 0x65

    invoke-virtual {v0, v1, v2}, LX/FKF;->a(Ljava/lang/String;I)V

    .line 2622335
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    .line 2622336
    sget-object v0, LX/DdH;->MESSAGE_QUEUED:LX/DdH;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2Ow;->a(LX/DdH;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2622337
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->b:LX/2Ow;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v2, v0}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Landroid/os/Bundle;)V

    .line 2622338
    return-void

    .line 2622339
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static g(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 2622340
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2622341
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622342
    :try_start_0
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->K:LX/4z1;

    invoke-virtual {v2, v0, p1}, LX/0Xt;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2622343
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622344
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->b:LX/2Ow;

    invoke-virtual {v1, v0}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2622345
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "queue_failure"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2622346
    invoke-static {v0, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2622347
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->o:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2622348
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v2, 0x66

    invoke-virtual {v0, v1, v2}, LX/FKF;->a(Ljava/lang/String;I)V

    .line 2622349
    return-void

    .line 2622350
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static h(Lcom/facebook/messaging/send/client/SendMessageManager;)V
    .locals 10

    .prologue
    .line 2622351
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622352
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v0}, LX/It2;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It3;

    .line 2622353
    iget-wide v8, v0, LX/It3;->i:J

    move-wide v4, v8

    .line 2622354
    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 2622355
    const-wide/16 v4, 0x0

    .line 2622356
    iput-wide v4, v0, LX/It3;->i:J

    .line 2622357
    goto :goto_0

    .line 2622358
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private static i(Lcom/facebook/messaging/send/client/SendMessageManager;)Lcom/facebook/messaging/model/messages/Message;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2622359
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v2

    .line 2622360
    const/4 v0, 0x0

    .line 2622361
    :try_start_0
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->V:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2622362
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v1}, LX/It2;->a()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It3;

    .line 2622363
    invoke-static {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/send/client/SendMessageManager;LX/It3;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2622364
    if-nez v1, :cond_0

    move-object v1, v0

    .line 2622365
    goto :goto_0

    .line 2622366
    :cond_0
    if-eqz v0, :cond_2

    iget-wide v4, v0, Lcom/facebook/messaging/model/messages/Message;->d:J

    iget-wide v6, v1, Lcom/facebook/messaging/model/messages/Message;->d:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    :goto_1
    move-object v1, v0

    .line 2622367
    goto :goto_0

    .line 2622368
    :cond_1
    monitor-exit v2

    return-object v1

    .line 2622369
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private static i(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 7

    .prologue
    .line 2622370
    invoke-static {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->k(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/service/model/SendMessageParams;

    move-result-object v0

    .line 2622371
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622372
    :try_start_0
    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    iput-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->U:Ljava/lang/String;

    .line 2622373
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622374
    new-instance v6, LX/ItE;

    invoke-direct {v6, p0, v0, p1}, LX/ItE;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622375
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2622376
    const-string v1, "sendMessageParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2622377
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->I:LX/2My;

    invoke-virtual {v0}, LX/2My;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->H:LX/2Mv;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2622378
    const-string v1, "send_to_montage"

    .line 2622379
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->c:LX/0aG;

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x662862fe

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2622380
    invoke-static {v0, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2622381
    return-void

    .line 2622382
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2622383
    :cond_0
    const-string v1, "send"

    goto :goto_0
.end method

.method private j(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 8

    .prologue
    .line 2622384
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2622385
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v2

    .line 2622386
    invoke-static {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->p(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    .line 2622387
    if-nez v0, :cond_0

    .line 2622388
    invoke-static {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->q(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622389
    :cond_0
    iget-object v3, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v3

    .line 2622390
    :try_start_0
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2622391
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->X:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2622392
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v4

    .line 2622393
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2622394
    invoke-virtual {v4}, LX/It3;->n()Ljava/util/List;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2622395
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2622396
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    const/16 v7, 0xa

    if-ge v6, v7, :cond_1

    .line 2622397
    invoke-static {v0}, LX/2Mk;->c(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2622398
    iget-object v6, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v4, v6}, LX/It3;->a(Ljava/lang/String;)Z

    .line 2622399
    iget-object v6, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->X:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2622400
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2622401
    goto :goto_0

    .line 2622402
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622403
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 2622404
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2622405
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2622406
    invoke-static {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->k(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/service/model/SendMessageParams;

    move-result-object v0

    .line 2622407
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2622408
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2622409
    :cond_2
    new-instance v6, LX/ItF;

    invoke-direct {v6, p0, p1, v3, v2}, LX/ItF;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;Ljava/util/ArrayList;Lcom/facebook/messaging/model/send/PendingSendQueueKey;)V

    .line 2622410
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2622411
    const-string v0, "sendMessageParams"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2622412
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->c:LX/0aG;

    const-string v1, "send_batch"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x29b7b878

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2622413
    invoke-static {v0, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2622414
    return-void
.end method

.method public static k(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/service/model/SendMessageParams;
    .locals 12

    .prologue
    .line 2622415
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v2, 0xca

    invoke-virtual {v0, v1, v2}, LX/FKF;->b(Ljava/lang/String;I)V

    .line 2622416
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->i:LX/1sj;

    invoke-virtual {v0}, LX/1sj;->a()Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v1

    .line 2622417
    invoke-static {v1}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v0

    .line 2622418
    const-string v8, "op"

    const-string v9, "message_send"

    invoke-interface {v0, v8, v9}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2622419
    const-string v8, "offline_threading_id"

    iget-object v9, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v0, v8, v9}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2622420
    sget-object v8, LX/ItG;->a:[I

    iget-object v9, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v9, v9, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    invoke-virtual {v9}, LX/5e9;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 2622421
    :goto_0
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->i:LX/1sj;

    sget-object v3, LX/2gR;->REQUEST_RECEIVE:LX/2gR;

    invoke-virtual {v2, v1, v3, v0}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2622422
    invoke-static {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->p(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    .line 2622423
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2622424
    iget-object v3, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v3

    .line 2622425
    if-nez v2, :cond_0

    .line 2622426
    :try_start_0
    invoke-static {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->q(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622427
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2622428
    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v4

    .line 2622429
    invoke-static {}, Lcom/facebook/messaging/service/model/SendMessageParams;->a()LX/6iz;

    move-result-object v5

    .line 2622430
    iput-object v0, v5, LX/6iz;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2622431
    move-object v0, v5

    .line 2622432
    invoke-virtual {v0, v2}, LX/6iz;->a(Z)LX/6iz;

    move-result-object v0

    .line 2622433
    iput-object v1, v0, LX/6iz;->c:Lcom/facebook/fbtrace/FbTraceNode;

    .line 2622434
    move-object v0, v0

    .line 2622435
    iget v1, v4, LX/It3;->g:I

    move v1, v1

    .line 2622436
    iput v1, v0, LX/6iz;->d:I

    .line 2622437
    move-object v0, v0

    .line 2622438
    iget-wide v8, v4, LX/It3;->h:J

    move-wide v6, v8

    .line 2622439
    iput-wide v6, v0, LX/6iz;->e:J

    .line 2622440
    move-object v0, v0

    .line 2622441
    iget-wide v8, v4, LX/It3;->d:J

    move-wide v4, v8

    .line 2622442
    iput-wide v4, v0, LX/6iz;->f:J

    .line 2622443
    move-object v0, v0

    .line 2622444
    invoke-virtual {v0}, LX/6iz;->a()Lcom/facebook/messaging/service/model/SendMessageParams;

    move-result-object v0

    .line 2622445
    monitor-exit v3

    .line 2622446
    return-object v0

    .line 2622447
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2622448
    :pswitch_0
    const-string v8, "recipient_id"

    iget-object v9, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v10, v9, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v0, v8, v9}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2622449
    :pswitch_1
    const-string v8, "thread_fbid"

    iget-object v9, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v10, v9, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v0, v8, v9}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static l(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2622450
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/messaging/model/messages/Message;)LX/FHZ;

    move-result-object v0

    iget-object v0, v0, LX/FHZ;->b:LX/FHY;

    .line 2622451
    sget-object v2, LX/ItG;->b:[I

    invoke-virtual {v0}, LX/FHY;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2622452
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2622453
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622454
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 2622455
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->u(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622456
    invoke-static {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->p(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->q:LX/2MA;

    invoke-interface {v0}, LX/2MA;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2622457
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622458
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lw;

    invoke-virtual {v0, p1}, LX/2Lw;->b(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622459
    invoke-direct {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->r(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_0

    :cond_0
    move v0, v1

    .line 2622460
    goto :goto_1

    .line 2622461
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->u(Lcom/facebook/messaging/model/messages/Message;)V

    move v0, v1

    .line 2622462
    goto :goto_1

    .line 2622463
    :pswitch_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static m(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 2622464
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622465
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->V:Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2622466
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->T:Z

    .line 2622467
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->W:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2622468
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->U:Ljava/lang/String;

    .line 2622469
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Y:Ljava/util/Set;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2622470
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->aa:Z

    .line 2622471
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622472
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v2, 0xcb

    invoke-virtual {v0, v1, v2}, LX/FKF;->b(Ljava/lang/String;I)V

    .line 2622473
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->S:LX/Isw;

    invoke-virtual {v0}, LX/Isw;->b()V

    .line 2622474
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->h(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    .line 2622475
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v0

    .line 2622476
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622477
    :try_start_1
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v2, v0}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v2

    .line 2622478
    if-eqz v2, :cond_0

    .line 2622479
    invoke-static {v2}, LX/It3;->o(LX/It3;)V

    .line 2622480
    invoke-virtual {v2}, LX/It3;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2622481
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v2, v0}, LX/It2;->b(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    .line 2622482
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2622483
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageManager;->b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    .line 2622484
    return-void

    .line 2622485
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2622486
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static n(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 3

    .prologue
    .line 2622487
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622488
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v0

    .line 2622489
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622490
    if-eqz v0, :cond_0

    .line 2622491
    iget-object v1, v0, LX/It3;->f:Ljava/lang/String;

    move-object v0, v1

    .line 2622492
    if-eqz v0, :cond_0

    .line 2622493
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2622494
    :goto_0
    return v0

    .line 2622495
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2622496
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 4
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2622497
    if-nez p1, :cond_1

    .line 2622498
    :cond_0
    :goto_0
    return v0

    .line 2622499
    :cond_1
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v1, v1, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-boolean v1, v1, LX/6fP;->shouldNotBeRetried:Z

    if-nez v1, :cond_0

    .line 2622500
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622501
    :try_start_0
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v2

    .line 2622502
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622503
    if-eqz v2, :cond_0

    .line 2622504
    iget-boolean v1, v2, LX/It3;->j:Z

    move v1, v1

    .line 2622505
    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 2622506
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static p(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2622507
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->l:LX/2Ly;

    invoke-virtual {v0, p1}, LX/2Ly;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v2

    .line 2622508
    iget-object v3, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v3

    .line 2622509
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/messaging/model/messages/Message;)LX/FHZ;

    move-result-object v4

    .line 2622510
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622511
    sget-object v0, LX/6eh;->VIDEO_CLIP:LX/6eh;

    if-ne v2, v0, :cond_0

    iget-object v0, v4, LX/FHZ;->b:LX/FHY;

    sget-object v3, LX/FHY;->FAILED:LX/FHY;

    if-ne v0, v3, :cond_0

    .line 2622512
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGm;

    invoke-static {v4}, LX/FGm;->a(LX/FHZ;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2622513
    :goto_0
    return v0

    .line 2622514
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2622515
    :cond_0
    sget-object v0, LX/6eh;->PHOTOS:LX/6eh;

    if-eq v2, v0, :cond_1

    sget-object v0, LX/6eh;->AUDIO_CLIP:LX/6eh;

    if-ne v2, v0, :cond_2

    :cond_1
    iget-object v0, v4, LX/FHZ;->b:LX/FHY;

    sget-object v3, LX/FHY;->FAILED:LX/FHY;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGm;

    invoke-static {v4}, LX/FGm;->a(LX/FHZ;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2622516
    goto :goto_0

    .line 2622517
    :cond_2
    sget-object v0, LX/6eh;->PAYMENT:LX/6eh;

    if-ne v2, v0, :cond_3

    move v0, v1

    .line 2622518
    goto :goto_0

    .line 2622519
    :cond_3
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v2

    .line 2622520
    :try_start_2
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v0

    .line 2622521
    monitor-exit v2

    .line 2622522
    if-nez v0, :cond_4

    move v0, v1

    .line 2622523
    goto :goto_0

    .line 2622524
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 2622525
    :cond_4
    iget-wide v6, v0, LX/It3;->h:J

    move-wide v2, v6

    .line 2622526
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->h:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    sub-long v2, v4, v2

    const-wide/32 v4, 0x927c0

    cmp-long v0, v2, v4

    if-lez v0, :cond_5

    move v0, v1

    .line 2622527
    goto :goto_0

    .line 2622528
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static q(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 2622529
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622530
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v0

    .line 2622531
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622532
    const/4 v1, 0x1

    .line 2622533
    iput-boolean v1, v0, LX/It3;->j:Z

    .line 2622534
    return-void

    .line 2622535
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private r(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x7530

    .line 2622536
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const-string v2, "retry_after_failure"

    invoke-virtual {v0, v1, v2}, LX/2Mq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2622537
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v2, 0x34

    invoke-virtual {v0, v1, v2}, LX/2Mq;->a(Ljava/lang/String;S)V

    .line 2622538
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622539
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/It2;->c(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v0

    .line 2622540
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622541
    invoke-virtual {v0, p1}, LX/It3;->b(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622542
    invoke-virtual {v0}, LX/It3;->f()V

    .line 2622543
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->h:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    add-long/2addr v2, v4

    .line 2622544
    iput-wide v2, v0, LX/It3;->i:J

    .line 2622545
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/messaging/send/client/SendMessageManager$15;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/send/client/SendMessageManager$15;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v4, v5, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2622546
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->b:LX/2Ow;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2622547
    return-void

    .line 2622548
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private s(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 5

    .prologue
    .line 2622549
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const-string v2, "retry_after_reconnect"

    invoke-virtual {v0, v1, v2}, LX/2Mq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2622550
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, LX/2Mq;->a(Ljava/lang/String;S)V

    .line 2622551
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622552
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/It2;->c(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v0

    .line 2622553
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622554
    invoke-virtual {v0, p1}, LX/It3;->b(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622555
    invoke-virtual {v0}, LX/It3;->i()V

    .line 2622556
    const-wide/16 v2, -0x1

    .line 2622557
    iput-wide v2, v0, LX/It3;->i:J

    .line 2622558
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->b:LX/2Ow;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v1}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2622559
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->S:LX/Isw;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->ac:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, LX/Isw;->a(Ljava/lang/Runnable;)V

    .line 2622560
    return-void

    .line 2622561
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static t(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 2622562
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v0

    .line 2622563
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622564
    :try_start_0
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v2, v0}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v2

    .line 2622565
    if-eqz v2, :cond_0

    .line 2622566
    invoke-static {v2}, LX/It3;->o(LX/It3;)V

    .line 2622567
    invoke-virtual {v2}, LX/It3;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2622568
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v2, v0}, LX/It2;->b(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    .line 2622569
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622570
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const-string v2, "canceled"

    invoke-virtual {v0, v1, v2}, LX/2Mq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2622571
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v2, 0xcc

    invoke-virtual {v0, v1, v2}, LX/FKF;->b(Ljava/lang/String;I)V

    .line 2622572
    return-void

    .line 2622573
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private u(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 3

    .prologue
    .line 2622574
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v0

    .line 2622575
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622576
    :try_start_0
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v2, v0}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v0

    .line 2622577
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622578
    if-eqz v0, :cond_0

    .line 2622579
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/It3;->a(Ljava/lang/String;)Z

    .line 2622580
    invoke-virtual {v0, p1}, LX/It3;->c(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622581
    :cond_0
    return-void

    .line 2622582
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FKH;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2622583
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2622584
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v3, LX/2uW;->PENDING_SEND:LX/2uW;

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2622585
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2622586
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2622587
    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2622588
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2622589
    new-instance v1, LX/ItL;

    invoke-direct {v1, p0}, LX/ItL;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    .line 2622590
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2622591
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 2622592
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2622593
    goto :goto_1

    .line 2622594
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    invoke-virtual {v0, p1}, LX/FKF;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2622595
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v4, 0x64

    invoke-virtual {v1, v3, v4}, LX/FKF;->a(Ljava/lang/String;I)V

    .line 2622596
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->x:LX/0Uh;

    const/16 v3, 0x1c3

    invoke-virtual {v1, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2622597
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/messaging/send/client/SendMessageManager$7;

    invoke-direct {v2, p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager$7;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    const v3, 0x634dfbfb

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_2

    .line 2622598
    :cond_3
    invoke-static {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->d$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;Lcom/facebook/messaging/send/trigger/NavigationTrigger;LX/9VZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/send/trigger/NavigationTrigger;",
            "LX/9VZ;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FKH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2622599
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2622600
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2622601
    :goto_0
    return-object v0

    .line 2622602
    :cond_0
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    if-nez p3, :cond_7

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v2, v0}, LX/2Mq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2622603
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->s:LX/2Mq;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, LX/2Mq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2622604
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->n:LX/IYt;

    .line 2622605
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v8, "send_message"

    invoke-direct {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2622606
    iput-object p2, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2622607
    move-object v7, v7

    .line 2622608
    if-eqz p3, :cond_1

    .line 2622609
    const-string v8, "navigation_trigger"

    invoke-virtual {p3}, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2622610
    :cond_1
    if-eqz p4, :cond_2

    .line 2622611
    const-string v8, "message_send_trigger"

    invoke-virtual {p4}, LX/9VZ;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2622612
    :cond_2
    iget-object v8, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-eqz v8, :cond_3

    .line 2622613
    const-string v8, "platform_app_id"

    iget-object v9, p1, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    iget-object v9, v9, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2622614
    :cond_3
    invoke-static {p1}, LX/2Mk;->n(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2622615
    const-string v8, "has_text"

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2622616
    :cond_4
    iget-object v8, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v8}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2622617
    const-string v8, "sms_tid"

    iget-object v9, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v9, v9, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2622618
    :cond_5
    invoke-static {p1}, LX/6f6;->c(Lcom/facebook/messaging/model/messages/Message;)LX/6f5;

    move-result-object v8

    invoke-virtual {v8}, LX/6f5;->s()LX/6f4;

    move-result-object v8

    move-object v8, v8

    .line 2622619
    const-string v9, "has_like"

    iget v10, v8, LX/6f4;->f:I

    invoke-static {v7, v9, v10}, LX/IYt;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;I)V

    .line 2622620
    const-string v9, "has_sticker"

    iget v10, v8, LX/6f4;->e:I

    invoke-static {v7, v9, v10}, LX/IYt;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;I)V

    .line 2622621
    const-string v9, "num_photos"

    iget v10, v8, LX/6f4;->b:I

    invoke-static {v7, v9, v10}, LX/IYt;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;I)V

    .line 2622622
    const-string v9, "num_videos"

    iget v10, v8, LX/6f4;->c:I

    invoke-static {v7, v9, v10}, LX/IYt;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;I)V

    .line 2622623
    const-string v9, "num_audio_clips"

    iget v10, v8, LX/6f4;->d:I

    invoke-static {v7, v9, v10}, LX/IYt;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;I)V

    .line 2622624
    const-string v9, "num_share_attachments"

    iget v10, v8, LX/6f4;->g:I

    invoke-static {v7, v9, v10}, LX/IYt;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;I)V

    .line 2622625
    const-string v9, "has_payment"

    iget v10, v8, LX/6f4;->h:I

    invoke-static {v7, v9, v10}, LX/IYt;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;I)V

    .line 2622626
    const-string v9, "has_xma"

    iget v8, v8, LX/6f4;->i:I

    invoke-static {v7, v9, v8}, LX/IYt;->b(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;I)V

    .line 2622627
    iget-object v8, v0, LX/IYt;->a:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2622628
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622629
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {p0, p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v0

    .line 2622630
    if-nez v0, :cond_8

    const/4 v6, 0x0

    .line 2622631
    :goto_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622632
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3QL;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->q:LX/2MA;

    sget-object v2, LX/8Bk;->MQTT:LX/8Bk;

    invoke-interface {v1, v2}, LX/2MA;->a(LX/8Bk;)LX/2MB;

    move-result-object v1

    invoke-virtual {v1}, LX/2MB;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->q:LX/2MA;

    invoke-interface {v1}, LX/2MA;->b()Z

    move-result v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v6}, LX/3QL;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;Lcom/facebook/messaging/send/trigger/NavigationTrigger;Ljava/lang/String;ZI)V

    .line 2622633
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lw;

    invoke-virtual {v0, p1}, LX/2Lw;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622634
    invoke-direct {p0, p1, p3}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/send/trigger/NavigationTrigger;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2622635
    const/4 v1, 0x2

    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2622636
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sending message"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2622637
    const-string v2, " thread:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2622638
    const-string v2, " navigationTrigger:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2622639
    if-eqz p3, :cond_6

    .line 2622640
    invoke-virtual {p3}, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2622641
    :cond_6
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto/16 :goto_0

    .line 2622642
    :cond_7
    invoke-virtual {p3}, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2622643
    :cond_8
    :try_start_1
    iget-object v2, v0, LX/It3;->c:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    move v6, v2

    .line 2622644
    goto :goto_2

    .line 2622645
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 2622646
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622647
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v0}, LX/It2;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It3;

    .line 2622648
    const/4 v3, 0x1

    .line 2622649
    iput-boolean v3, v0, LX/It3;->j:Z

    .line 2622650
    goto :goto_0

    .line 2622651
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2622652
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v2

    .line 2622653
    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2622654
    invoke-virtual {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v0

    .line 2622655
    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v4, v0}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v0

    .line 2622656
    if-eqz v0, :cond_0

    .line 2622657
    const/4 v4, 0x1

    .line 2622658
    iput-boolean v4, v0, LX/It3;->j:Z

    .line 2622659
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2622660
    :cond_1
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 2

    .prologue
    .line 2622661
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v1

    .line 2622662
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->L:LX/4z1;

    invoke-virtual {v0, p1}, LX/0Xs;->f(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2622663
    monitor-exit v1

    .line 2622664
    :goto_0
    return-void

    .line 2622665
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->L:LX/4z1;

    invoke-virtual {v0, p1}, LX/0vW;->b(Ljava/lang/Object;)Ljava/util/Set;

    .line 2622666
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Collection;)V
    .locals 11
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2622667
    iget-object v3, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v3

    .line 2622668
    :try_start_0
    invoke-static {}, LX/6fM;->values()[LX/6fM;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_5

    aget-object v0, v4, v2

    .line 2622669
    new-instance v6, Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    invoke-direct {v6, p1, v0}, Lcom/facebook/messaging/model/send/PendingSendQueueKey;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/6fM;)V

    .line 2622670
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v0, v6}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v7

    .line 2622671
    if-eqz v7, :cond_4

    .line 2622672
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2622673
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->U:Ljava/lang/String;

    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2622674
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->aa:Z

    .line 2622675
    :cond_0
    :goto_2
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->A:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    .line 2622676
    iget-object v9, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->j:LX/2MT;

    .line 2622677
    invoke-static {v9, v0}, LX/2MT;->b(LX/2MT;Ljava/lang/String;)LX/0Rf;

    move-result-object v10

    .line 2622678
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/6ed;

    .line 2622679
    invoke-static {v9, v10, v0}, LX/2MT;->a(LX/2MT;LX/6ed;Ljava/lang/String;)V

    goto :goto_3

    .line 2622680
    :cond_1
    goto :goto_1

    .line 2622681
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2622682
    :cond_2
    :try_start_1
    invoke-direct {p0, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2622683
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Y:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2622684
    :cond_3
    invoke-virtual {v7, v0}, LX/It3;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2622685
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->j:LX/FKF;

    const/16 v9, 0xcc

    invoke-virtual {v1, v0, v9}, LX/FKF;->b(Ljava/lang/String;I)V

    .line 2622686
    invoke-virtual {v7}, LX/It3;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2622687
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    invoke-virtual {v1, v6}, LX/It2;->b(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    goto :goto_2

    .line 2622688
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2622689
    :cond_5
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2622690
    const-string v0, "SendMessageManager.queueKeyForMessage"

    const v1, -0x74bce748

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2622691
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    if-eqz v0, :cond_0

    .line 2622692
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622693
    const v1, -0x54f4ad4c

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 2622694
    :cond_0
    :try_start_1
    sget-object v0, LX/ItG;->d:[I

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->l:LX/2Ly;

    invoke-virtual {v1, p1}, LX/2Ly;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v1

    invoke-virtual {v1}, LX/6eh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2622695
    sget-object v0, LX/6fM;->NORMAL:LX/6fM;

    move-object v1, v0

    .line 2622696
    :goto_1
    new-instance v0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {v0, v2, v1}, Lcom/facebook/messaging/model/send/PendingSendQueueKey;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/6fM;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2622697
    const v1, 0x64dd6aa2

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2622698
    :pswitch_0
    :try_start_2
    sget-object v0, LX/6fM;->VIDEO:LX/6fM;

    move-object v1, v0

    .line 2622699
    goto :goto_1

    .line 2622700
    :pswitch_1
    sget-object v0, LX/6fM;->LIGHT_MEDIA:LX/6fM;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v0

    .line 2622701
    goto :goto_1

    .line 2622702
    :catchall_0
    move-exception v0

    const v1, -0x6a985398

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final clearUserData()V
    .locals 3

    .prologue
    .line 2622703
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->P:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2622704
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->P:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2622705
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->Q:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2622706
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->R:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2622707
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/messaging/send/client/SendMessageManager$5;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/send/client/SendMessageManager$5;-><init>(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    const v2, 0x4a92eed4    # 4814698.0f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2622708
    return-void
.end method
