.class public final Lcom/facebook/messaging/send/client/SendMessageManager$9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/messages/Message;

.field public final synthetic b:Lcom/facebook/messaging/send/client/SendMessageManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 0

    .prologue
    .line 2622303
    iput-object p1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iput-object p2, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 2622304
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v2, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->a:[Ljava/lang/Object;

    monitor-enter v2

    .line 2622305
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v0, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->n(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2622306
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v0, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->p(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v1

    .line 2622307
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->M:LX/It2;

    iget-object v3, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v3, v4}, Lcom/facebook/messaging/send/client/SendMessageManager;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/It2;->a(Lcom/facebook/messaging/model/send/PendingSendQueueKey;)LX/It3;

    move-result-object v0

    .line 2622308
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2622309
    if-eqz v0, :cond_0

    .line 2622310
    invoke-static {}, Lcom/facebook/messaging/service/model/SendMessageParams;->a()LX/6iz;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2622311
    iput-object v3, v2, LX/6iz;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2622312
    move-object v2, v2

    .line 2622313
    invoke-virtual {v2, v1}, LX/6iz;->a(Z)LX/6iz;

    move-result-object v1

    .line 2622314
    iget v2, v0, LX/It3;->g:I

    move v2, v2

    .line 2622315
    iput v2, v1, LX/6iz;->d:I

    .line 2622316
    move-object v1, v1

    .line 2622317
    iget-wide v13, v0, LX/It3;->h:J

    move-wide v2, v13

    .line 2622318
    iput-wide v2, v1, LX/6iz;->e:J

    .line 2622319
    move-object v1, v1

    .line 2622320
    iget-wide v13, v0, LX/It3;->d:J

    move-wide v2, v13

    .line 2622321
    iput-wide v2, v1, LX/6iz;->f:J

    .line 2622322
    move-object v0, v1

    .line 2622323
    invoke-virtual {v0}, LX/6iz;->a()Lcom/facebook/messaging/service/model/SendMessageParams;

    move-result-object v1

    .line 2622324
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->w:LX/6f6;

    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0, v2}, LX/6f6;->b(Lcom/facebook/messaging/model/messages/Message;)LX/6f4;

    move-result-object v4

    .line 2622325
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3QL;

    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v2, v2, Lcom/facebook/messaging/send/client/SendMessageManager;->v:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iget-object v5, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-wide v10, v5, Lcom/facebook/messaging/model/messages/Message;->d:J

    sub-long/2addr v2, v10

    const-string v5, "via_mqtt"

    iget-object v7, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v7, v7, Lcom/facebook/messaging/send/client/SendMessageManager;->q:LX/2MA;

    invoke-interface {v7}, LX/2MA;->c()Z

    move-result v10

    sget-object v11, LX/FCY;->MQTT:LX/FCY;

    const/4 v12, 0x1

    move v7, v6

    move-object v9, v8

    invoke-virtual/range {v0 .. v12}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;Ljava/lang/String;IZLX/1Mb;LX/1Mb;ZLX/FCY;Z)V

    .line 2622326
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$9;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v0, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->m(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622327
    :cond_0
    return-void

    .line 2622328
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object v0, v8

    move v1, v6

    goto :goto_0
.end method
