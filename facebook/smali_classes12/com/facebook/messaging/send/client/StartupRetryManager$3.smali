.class public final Lcom/facebook/messaging/send/client/StartupRetryManager$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/ItV;


# direct methods
.method public constructor <init>(LX/ItV;)V
    .locals 0

    .prologue
    .line 2623376
    iput-object p1, p0, Lcom/facebook/messaging/send/client/StartupRetryManager$3;->a:LX/ItV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 2623377
    iget-object v0, p0, Lcom/facebook/messaging/send/client/StartupRetryManager$3;->a:LX/ItV;

    iget-object v0, v0, LX/ItV;->g:LX/2N4;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/messaging/send/client/StartupRetryManager$3;->a:LX/ItV;

    iget-wide v2, v2, LX/ItV;->l:J

    const/16 v4, 0x15

    const/4 v5, 0x1

    new-array v5, v5, [LX/2uW;

    const/4 v6, 0x0

    sget-object v7, LX/2uW;->PENDING_SEND:LX/2uW;

    aput-object v7, v5, v6

    invoke-virtual/range {v0 .. v5}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JI[LX/2uW;)Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 2623378
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2623379
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    int-to-long v2, v1

    const-wide/16 v4, 0x14

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 2623380
    iget-object v0, p0, Lcom/facebook/messaging/send/client/StartupRetryManager$3;->a:LX/ItV;

    invoke-static {v0}, LX/ItV;->e(LX/ItV;)V

    .line 2623381
    :cond_0
    :goto_0
    return-void

    .line 2623382
    :cond_1
    iget-object v1, p0, Lcom/facebook/messaging/send/client/StartupRetryManager$3;->a:LX/ItV;

    invoke-static {v1, v0}, LX/ItV;->b(LX/ItV;Ljava/util/LinkedHashMap;)V

    goto :goto_0
.end method
