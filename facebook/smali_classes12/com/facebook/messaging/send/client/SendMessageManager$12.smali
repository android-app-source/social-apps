.class public final Lcom/facebook/messaging/send/client/SendMessageManager$12;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/messaging/service/model/NewMessageResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/service/model/SendMessageParams;

.field public final synthetic b:Lcom/facebook/messaging/model/messages/Message;

.field public final synthetic c:Lcom/facebook/messaging/send/client/SendMessageManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 0

    .prologue
    .line 2622210
    iput-object p1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iput-object p2, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->a:Lcom/facebook/messaging/service/model/SendMessageParams;

    iput-object p3, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2622202
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->i:LX/1sj;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->a:Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v1, v1, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    sget-object v2, LX/2gR;->RESPONSE_SEND:LX/2gR;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2622203
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2622204
    const-string v0, "sendMessageParams"

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->a:Lcom/facebook/messaging/service/model/SendMessageParams;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2622205
    const-string v0, "sendMessageException"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2622206
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->c:LX/0aG;

    const-string v1, "handle_send_result"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x7c93a1f9

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2622207
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 2622208
    invoke-static {v0, p1, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->a$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622209
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2622194
    check-cast p1, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2622195
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->i:LX/1sj;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->a:Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v1, v1, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    sget-object v2, LX/2gR;->RESPONSE_SEND:LX/2gR;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2622196
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2622197
    const-string v0, "sendMessageParams"

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->a:Lcom/facebook/messaging/service/model/SendMessageParams;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2622198
    const-string v0, "newMessageResult"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2622199
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->c:LX/0aG;

    const-string v1, "handle_send_result"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v4, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, 0x1ecece95

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    .line 2622200
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->c:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$12;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v0, v1}, Lcom/facebook/messaging/send/client/SendMessageManager;->m(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2622201
    return-void
.end method
