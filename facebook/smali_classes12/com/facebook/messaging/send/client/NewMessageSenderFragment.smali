.class public Lcom/facebook/messaging/send/client/NewMessageSenderFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field private static final m:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:Z

.field private B:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadResult;",
            ">;"
        }
    .end annotation
.end field

.field private C:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadResult;",
            ">;"
        }
    .end annotation
.end field

.field public D:J

.field public E:J

.field public F:LX/0Yb;

.field public n:LX/Isv;

.field public o:Lcom/facebook/messaging/send/client/SendMessageManager;

.field public p:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

.field public q:LX/It0;

.field public r:LX/3Ec;

.field public s:Ljava/util/concurrent/ExecutorService;

.field public t:LX/0Xl;

.field public u:LX/2Mq;

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2qz;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public x:Lcom/facebook/messaging/model/messages/Message;

.field public y:Ljava/lang/String;

.field public z:Lcom/facebook/messaging/send/trigger/NavigationTrigger;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2621652
    const-class v0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;

    sput-object v0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->m:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2621653
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2621654
    return-void
.end method

.method private a()V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2621655
    iget-boolean v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->A:Z

    if-nez v0, :cond_0

    move v1, v2

    .line 2621656
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->w:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    move v4, v2

    .line 2621657
    :goto_1
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->w:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->m()Lcom/facebook/user/model/UserFbidIdentifier;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2621658
    :goto_2
    if-eqz v1, :cond_3

    if-eqz v4, :cond_3

    if-eqz v2, :cond_3

    .line 2621659
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->w:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2621660
    iget-object v1, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->r:LX/3Ec;

    .line 2621661
    iget-object v2, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v0, v2

    .line 2621662
    invoke-virtual {v1, v0}, LX/3Ec;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2621663
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->x:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v1, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v1

    .line 2621664
    iput-object v0, v1, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2621665
    move-object v1, v1

    .line 2621666
    invoke-virtual {v1}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2621667
    iget-object v2, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->u:LX/2Mq;

    iget-object v3, v1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2Mq;->a(Ljava/lang/String;)V

    .line 2621668
    iget-object v2, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->o:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v3, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->y:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->z:Lcom/facebook/messaging/send/trigger/NavigationTrigger;

    sget-object v5, LX/9VZ;->NEW_MESSAGE:LX/9VZ;

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;Lcom/facebook/messaging/send/trigger/NavigationTrigger;LX/9VZ;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2621669
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2621670
    :goto_3
    return-void

    :cond_0
    move v1, v3

    .line 2621671
    goto :goto_0

    :cond_1
    move v4, v3

    .line 2621672
    goto :goto_1

    :cond_2
    move v2, v3

    .line 2621673
    goto :goto_2

    .line 2621674
    :cond_3
    const/4 v6, 0x0

    .line 2621675
    iget-object v5, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->x:Lcom/facebook/messaging/model/messages/Message;

    iget-object v8, v5, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v7, v6

    :goto_4
    if-ge v7, v9, :cond_7

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2621676
    iget-object v10, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->p:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v10, v5}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FGc;

    move-result-object v5

    .line 2621677
    iget-object v5, v5, LX/FGc;->b:LX/FGb;

    sget-object v10, LX/FGb;->IN_PROGRESS:LX/FGb;

    if-ne v5, v10, :cond_6

    .line 2621678
    const/4 v5, 0x1

    .line 2621679
    :goto_5
    move v5, v5

    .line 2621680
    if-eqz v5, :cond_5

    .line 2621681
    iget-wide v5, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->E:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-gtz v5, :cond_4

    .line 2621682
    iget-object v5, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->q:LX/It0;

    invoke-virtual {v5}, LX/It0;->a()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->E:J

    .line 2621683
    :cond_4
    iget-object v5, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->x:Lcom/facebook/messaging/model/messages/Message;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v7, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->E:J

    const/4 v6, 0x1

    invoke-static {p0, v5, v7, v8, v6}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->a(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;Lcom/facebook/messaging/model/threadkey/ThreadKey;JZ)V

    .line 2621684
    iget-object v5, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->F:LX/0Yb;

    invoke-virtual {v5}, LX/0Yb;->b()V

    .line 2621685
    iget-object v5, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v5, v5

    .line 2621686
    check-cast v5, LX/4BY;

    .line 2621687
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/4BY;->a(Z)V

    .line 2621688
    invoke-static {p0}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->n(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;)V

    .line 2621689
    :goto_6
    goto :goto_3

    .line 2621690
    :cond_5
    invoke-static {p0}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->o(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;)V

    goto :goto_6

    .line 2621691
    :cond_6
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_4

    :cond_7
    move v5, v6

    .line 2621692
    goto :goto_5
.end method

.method public static a(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V
    .locals 12

    .prologue
    .line 2621693
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2qz;

    sget-object v1, LX/2r0;->MULTIPICKER:LX/2r0;

    const/4 v9, 0x0

    .line 2621694
    sget-object v4, LX/6be;->SUCCEEDED:LX/6be;

    const/4 v10, -0x1

    move-object v3, v0

    move-object v5, p1

    move-wide v6, p2

    move-object v8, v1

    move v11, v9

    invoke-static/range {v3 .. v11}, LX/2qz;->a(LX/2qz;LX/6be;Lcom/facebook/messaging/model/threadkey/ThreadKey;JLX/2r0;ZII)V

    .line 2621695
    return-void
.end method

.method public static a(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;Lcom/facebook/messaging/model/threadkey/ThreadKey;JZ)V
    .locals 8

    .prologue
    .line 2621703
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2qz;

    sget-object v4, LX/2r0;->MULTIPICKER:LX/2r0;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->w:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v6, v1, 0x1

    move-object v1, p1

    move-wide v2, p2

    move v5, p4

    invoke-virtual/range {v0 .. v6}, LX/2qz;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JLX/2r0;ZI)V

    .line 2621704
    return-void
.end method

.method public static a$redex0(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;ZLjava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2621696
    if-eqz p1, :cond_0

    .line 2621697
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->x:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->E:J

    const/4 v1, 0x1

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;JZ)V

    .line 2621698
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2621699
    return-void

    .line 2621700
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->x:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->D:J

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;JZ)V

    goto :goto_0
.end method

.method private b(Lcom/facebook/messaging/model/threadkey/ThreadKey;JZ)V
    .locals 6
    .param p1    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2621701
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2qz;

    sget-object v4, LX/2r0;->MULTIPICKER:LX/2r0;

    move-object v1, p1

    move-wide v2, p2

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/2qz;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JLX/2r0;Z)V

    .line 2621702
    return-void
.end method

.method public static n(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 2621524
    const-wide/16 v2, 0x0

    .line 2621525
    iget-object v1, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->x:Lcom/facebook/messaging/model/messages/Message;

    iget-object v7, v1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move-wide v4, v2

    move v1, v0

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v8, :cond_0

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2621526
    add-int/lit8 v6, v1, 0x1

    .line 2621527
    iget-object v1, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->p:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FGc;

    move-result-object v1

    .line 2621528
    sget-object v9, LX/Isz;->a:[I

    iget-object v1, v1, LX/FGc;->b:LX/FGb;

    invoke-virtual {v1}, LX/FGb;->ordinal()I

    move-result v1

    aget v1, v9, v1

    packed-switch v1, :pswitch_data_0

    .line 2621529
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Media upload failed"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->a$redex0(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;ZLjava/lang/Throwable;)V

    .line 2621530
    :goto_1
    return-void

    .line 2621531
    :pswitch_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v4

    .line 2621532
    add-int/lit8 v2, v2, 0x1

    .line 2621533
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move-wide v4, v0

    move v1, v6

    goto :goto_0

    .line 2621534
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->p:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->e(Lcom/facebook/ui/media/attachments/MediaResource;)D

    move-result-wide v0

    add-double/2addr v0, v4

    .line 2621535
    goto :goto_2

    .line 2621536
    :cond_0
    if-ne v2, v1, :cond_1

    .line 2621537
    invoke-static {p0}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->o(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;)V

    goto :goto_1

    .line 2621538
    :cond_1
    int-to-double v0, v1

    div-double v2, v4, v0

    .line 2621539
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 2621540
    check-cast v0, LX/4BY;

    .line 2621541
    const-wide v4, 0x4058c00000000000L    # 99.0

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-virtual {v0, v1}, LX/4BY;->b(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static o(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2621608
    const/4 v3, 0x0

    .line 2621609
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->x:Lcom/facebook/messaging/model/messages/Message;

    iget-object v5, v0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_6

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2621610
    iget-object v8, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->p:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v8, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->d(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FGc;

    move-result-object v0

    .line 2621611
    iget-object v0, v0, LX/FGc;->b:LX/FGb;

    sget-object v8, LX/FGb;->SUCCEEDED:LX/FGb;

    if-eq v0, v8, :cond_5

    move v0, v3

    .line 2621612
    :goto_1
    move v0, v0

    .line 2621613
    if-nez v0, :cond_0

    .line 2621614
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Media upload failed"

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v1, v0}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->a$redex0(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;ZLjava/lang/Throwable;)V

    .line 2621615
    :goto_2
    return-void

    .line 2621616
    :cond_0
    invoke-virtual {p0, v2}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 2621617
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->F:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2621618
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 2621619
    check-cast v0, LX/4BY;

    .line 2621620
    const/16 v3, 0x63

    invoke-virtual {v0, v3}, LX/4BY;->b(I)V

    .line 2621621
    iput-object v7, v0, LX/4BY;->h:Ljava/text/NumberFormat;

    .line 2621622
    invoke-static {v0}, LX/4BY;->c(LX/4BY;)V

    .line 2621623
    invoke-virtual {v0, v1}, LX/4BY;->a(Z)V

    .line 2621624
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->p:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v3, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->x:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0, v3}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    .line 2621625
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2621626
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->w:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_3
    if-ge v3, v6, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->w:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2621627
    iget-object v8, v0, Lcom/facebook/user/model/User;->ai:Lcom/facebook/user/model/UserIdentifier;

    move-object v0, v8

    .line 2621628
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2621629
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 2621630
    :cond_1
    new-instance v3, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;

    invoke-direct {v3, v7, v4, v5}, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;Ljava/util/List;)V

    .line 2621631
    iget-wide v4, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->E:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    move v0, v1

    .line 2621632
    :goto_4
    if-eqz v0, :cond_4

    .line 2621633
    iget-wide v4, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->E:J

    iput-wide v4, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->D:J

    .line 2621634
    :goto_5
    if-nez v0, :cond_2

    .line 2621635
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->x:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->D:J

    invoke-static {p0, v0, v4, v5, v2}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->a(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;Lcom/facebook/messaging/model/threadkey/ThreadKey;JZ)V

    .line 2621636
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->n:LX/Isv;

    iget-wide v4, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->D:J

    .line 2621637
    iget-object v1, v0, LX/Isv;->b:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 2621638
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2621639
    const-string v2, "createThreadParams"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2621640
    iget-object v2, v0, LX/Isv;->c:LX/0aG;

    const-string v6, "create_thread"

    sget-object v7, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v8, -0x531b275a

    invoke-static {v2, v6, v1, v7, v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 2621641
    new-instance v2, LX/Ist;

    invoke-direct {v2, v0, v4, v5}, LX/Ist;-><init>(LX/Isv;J)V

    .line 2621642
    iget-object v6, v0, LX/Isv;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2621643
    iget-object v6, v0, LX/Isv;->e:LX/0QI;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-static {v1, v2}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v2

    invoke-interface {v6, v7, v2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2621644
    invoke-static {v0, v1}, LX/Isv;->a(LX/Isv;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2621645
    iput-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->B:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2621646
    invoke-direct {p0}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->q()LX/0Ve;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->C:LX/0Ve;

    .line 2621647
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->B:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->C:LX/0Ve;

    iget-object v2, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->s:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 2621648
    goto :goto_4

    .line 2621649
    :cond_4
    iget-object v1, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->q:LX/It0;

    invoke-virtual {v1}, LX/It0;->a()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->D:J

    goto :goto_5

    .line 2621650
    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 2621651
    :cond_6
    const/4 v0, 0x1

    goto/16 :goto_1
.end method

.method private q()LX/0Ve;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Ve",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2621607
    new-instance v0, LX/Isy;

    invoke-direct {v0, p0}, LX/Isy;-><init>(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2621599
    new-instance v0, LX/4BY;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 2621600
    iput v2, v0, LX/4BY;->d:I

    .line 2621601
    invoke-virtual {v0, v2}, LX/4BY;->a(Z)V

    .line 2621602
    invoke-virtual {v0, v2}, LX/4BY;->setCancelable(Z)V

    .line 2621603
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, LX/4BY;->c(I)V

    .line 2621604
    const v1, 0x7f0802ff

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2621605
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4BY;->a(Ljava/lang/String;)V

    .line 2621606
    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x2

    const/16 v3, 0x2a

    const v4, 0x57deb326

    invoke-static {v2, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2621573
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2621574
    if-eqz p1, :cond_0

    const-string v2, "waitingToCreateThreadId"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2621575
    const-string v2, "waitingToCreateThreadId"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->E:J

    .line 2621576
    :cond_0
    if-eqz p1, :cond_2

    const-string v2, "createThreadId"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->w:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-le v2, v0, :cond_2

    move v2, v0

    .line 2621577
    :goto_0
    if-eqz p1, :cond_3

    const-string v4, "createThreadId"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2621578
    :goto_1
    if-eqz p1, :cond_1

    if-eqz v2, :cond_4

    .line 2621579
    :cond_1
    invoke-direct {p0}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->a()V

    .line 2621580
    :goto_2
    const v0, 0x3da6bf4f

    invoke-static {v0, v3}, LX/02F;->f(II)V

    return-void

    :cond_2
    move v2, v1

    .line 2621581
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2621582
    goto :goto_1

    .line 2621583
    :cond_4
    if-eqz v0, :cond_6

    .line 2621584
    const-string v0, "createThreadId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->D:J

    .line 2621585
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->n:LX/Isv;

    iget-wide v4, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->D:J

    .line 2621586
    iget-object v1, v0, LX/Isv;->b:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 2621587
    iget-object v1, v0, LX/Isv;->e:LX/0QI;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Mv;

    .line 2621588
    if-nez v1, :cond_7

    .line 2621589
    const/4 v1, 0x0

    .line 2621590
    :goto_3
    move-object v0, v1

    .line 2621591
    iput-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->B:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2621592
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->B:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_5

    .line 2621593
    invoke-direct {p0}, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->q()LX/0Ve;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->C:LX/0Ve;

    .line 2621594
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->B:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->C:LX/0Ve;

    iget-object v2, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->s:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_2

    .line 2621595
    :cond_5
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_2

    .line 2621596
    :cond_6
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_2

    .line 2621597
    :cond_7
    iget-object v2, v1, LX/1Mv;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v1, v2

    .line 2621598
    invoke-static {v0, v1}, LX/Isv;->a(LX/Isv;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_3
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 2621568
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->B:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2621569
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->B:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2621570
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->B:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2621571
    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->F:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2621572
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x521546be

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2621553
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2621554
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 2621555
    const-string v0, "r"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2621556
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2621557
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->w:LX/0Px;

    .line 2621558
    const-string v0, "m"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    iput-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->x:Lcom/facebook/messaging/model/messages/Message;

    .line 2621559
    const-string v0, "om"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->y:Ljava/lang/String;

    .line 2621560
    const-string v0, "t"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "t"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/trigger/NavigationTrigger;

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->z:Lcom/facebook/messaging/send/trigger/NavigationTrigger;

    .line 2621561
    const-string v0, "rtv"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->A:Z

    .line 2621562
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v3, p0

    check-cast v3, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;

    invoke-static {p1}, LX/Isv;->a(LX/0QB;)LX/Isv;

    move-result-object v4

    check-cast v4, LX/Isv;

    invoke-static {p1}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(LX/0QB;)Lcom/facebook/messaging/send/client/SendMessageManager;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {p1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(LX/0QB;)Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-static {p1}, LX/It0;->a(LX/0QB;)LX/It0;

    move-result-object v7

    check-cast v7, LX/It0;

    invoke-static {p1}, LX/3Ec;->b(LX/0QB;)LX/3Ec;

    move-result-object v8

    check-cast v8, LX/3Ec;

    invoke-static {p1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/ExecutorService;

    invoke-static {p1}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-static {p1}, LX/2Mq;->a(LX/0QB;)LX/2Mq;

    move-result-object v11

    check-cast v11, LX/2Mq;

    const/16 v0, 0xcd4

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v4, v3, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->n:LX/Isv;

    iput-object v5, v3, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->o:Lcom/facebook/messaging/send/client/SendMessageManager;

    iput-object v6, v3, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->p:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object v7, v3, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->q:LX/It0;

    iput-object v8, v3, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->r:LX/3Ec;

    iput-object v9, v3, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->s:Ljava/util/concurrent/ExecutorService;

    iput-object v10, v3, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->t:LX/0Xl;

    iput-object v11, v3, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->u:LX/2Mq;

    iput-object p1, v3, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->v:LX/0Ot;

    .line 2621563
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 2621564
    new-instance v0, LX/Isx;

    invoke-direct {v0, p0}, LX/Isx;-><init>(Lcom/facebook/messaging/send/client/NewMessageSenderFragment;)V

    .line 2621565
    iget-object v2, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->t:LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.orca.media.upload.PROCESS_MEDIA_TOTAL_PROGRESS"

    invoke-interface {v2, v3, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.orca.media.upload.PROCESS_MEDIA_COMPLETE"

    invoke-interface {v2, v3, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.orca.media.upload.MEDIA_UPLOAD_STATUS_CHANGED"

    invoke-interface {v2, v3, v0}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->F:LX/0Yb;

    .line 2621566
    const v0, 0x70976b61

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2621567
    :cond_0
    const-string v0, "new_message_sender_unknown"

    invoke-static {v0}, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->a(Ljava/lang/String;)Lcom/facebook/messaging/send/trigger/NavigationTrigger;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xbeaacb2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2621548
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 2621549
    iget-object v1, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->F:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2621550
    iget-object v1, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->C:LX/0Ve;

    if-eqz v1, :cond_0

    .line 2621551
    iget-object v1, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->C:LX/0Ve;

    invoke-interface {v1}, LX/0Ve;->dispose()V

    .line 2621552
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x2ba1aa35

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2621542
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2621543
    iget-wide v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->E:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 2621544
    const-string v0, "waitingToCreateThreadId"

    iget-wide v2, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->E:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2621545
    :cond_0
    iget-wide v0, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->D:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 2621546
    const-string v0, "createThreadId"

    iget-wide v2, p0, Lcom/facebook/messaging/send/client/NewMessageSenderFragment;->D:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2621547
    :cond_1
    return-void
.end method
