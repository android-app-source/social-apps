.class public final Lcom/facebook/messaging/send/client/SendMessageManager$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/send/client/SendMessageManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/send/client/SendMessageManager;)V
    .locals 0

    .prologue
    .line 2622271
    iput-object p1, p0, Lcom/facebook/messaging/send/client/SendMessageManager$4;->a:Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2622272
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageManager$4;->a:Lcom/facebook/messaging/send/client/SendMessageManager;

    .line 2622273
    iget-object v1, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->q:LX/2MA;

    sget-object v2, LX/8Bk;->MQTT:LX/8Bk;

    invoke-interface {v1, v2}, LX/2MA;->b(LX/8Bk;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2622274
    iget-object v1, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->ab:LX/03V;

    const-string v2, "SendMessageManager"

    const-string p0, "Backoff timer triggered retry but mqtt is connected."

    invoke-virtual {v1, v2, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2622275
    :cond_0
    invoke-static {v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->h(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    .line 2622276
    invoke-static {v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->b$redex0(Lcom/facebook/messaging/send/client/SendMessageManager;)V

    .line 2622277
    return-void
.end method
