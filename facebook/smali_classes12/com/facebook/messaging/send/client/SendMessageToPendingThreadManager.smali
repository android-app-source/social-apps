.class public Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final r:Ljava/lang/Object;


# instance fields
.field public final a:LX/0aG;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Sh;

.field public final e:LX/It6;

.field public final f:LX/2MA;

.field public final g:LX/0Zb;

.field public final h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

.field private final i:LX/2Ly;

.field private final j:LX/0Xl;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ItB;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Yb;

.field public final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/ItQ;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui_thread"
    .end annotation
.end field

.field public final o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/ItQ;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ui_thread"
    .end annotation
.end field

.field public final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/ItR;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2623352
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->r:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0Ot;LX/0Ot;LX/0Sh;LX/It6;LX/2MA;LX/0Zb;Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;LX/2Ly;LX/0Xl;LX/0Ot;LX/0Ot;)V
    .locals 3
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p10    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0aG;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/It6;",
            "LX/2MA;",
            "LX/0Zb;",
            "Lcom/facebook/messaging/media/upload/MediaUploadManager;",
            "LX/2Ly;",
            "LX/0Xl;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ItB;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2623331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2623332
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->n:Ljava/util/Map;

    .line 2623333
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->o:Ljava/util/Map;

    .line 2623334
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->p:Ljava/util/List;

    .line 2623335
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->q:Ljava/util/Map;

    .line 2623336
    iput-object p1, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->a:LX/0aG;

    .line 2623337
    iput-object p2, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->b:LX/0Ot;

    .line 2623338
    iput-object p3, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->c:LX/0Ot;

    .line 2623339
    iput-object p4, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->d:LX/0Sh;

    .line 2623340
    iput-object p5, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->e:LX/It6;

    .line 2623341
    iput-object p6, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->f:LX/2MA;

    .line 2623342
    iput-object p7, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->g:LX/0Zb;

    .line 2623343
    iput-object p8, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    .line 2623344
    iput-object p9, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->i:LX/2Ly;

    .line 2623345
    iput-object p10, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->j:LX/0Xl;

    .line 2623346
    iput-object p11, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->k:LX/0Ot;

    .line 2623347
    iput-object p12, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->l:LX/0Ot;

    .line 2623348
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->j:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.CONNECTIVITY_CHANGED"

    new-instance v2, LX/ItM;

    invoke-direct {v2, p0}, LX/ItM;-><init>(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    .line 2623349
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->j:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.orca.media.upload.MEDIA_UPLOAD_STATUS_CHANGED"

    new-instance v2, LX/ItN;

    invoke-direct {v2, p0}, LX/ItN;-><init>(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->m:LX/0Yb;

    .line 2623350
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->m:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2623351
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;
    .locals 7

    .prologue
    .line 2623304
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2623305
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2623306
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2623307
    if-nez v1, :cond_0

    .line 2623308
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2623309
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2623310
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2623311
    sget-object v1, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->r:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2623312
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2623313
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2623314
    :cond_1
    if-nez v1, :cond_4

    .line 2623315
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2623316
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2623317
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->b(LX/0QB;)Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2623318
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2623319
    if-nez v1, :cond_2

    .line 2623320
    sget-object v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->r:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2623321
    :goto_1
    if-eqz v0, :cond_3

    .line 2623322
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2623323
    :goto_3
    check-cast v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2623324
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2623325
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2623326
    :catchall_1
    move-exception v0

    .line 2623327
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2623328
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2623329
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2623330
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->r:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/UserIdentifier;
    .locals 3

    .prologue
    .line 2623223
    invoke-virtual {p0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v0

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    if-ne v0, v1, :cond_0

    .line 2623224
    new-instance v0, Lcom/facebook/user/model/UserFbidIdentifier;

    invoke-virtual {p0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/user/model/UserFbidIdentifier;-><init>(Ljava/lang/String;)V

    .line 2623225
    :goto_0
    return-object v0

    .line 2623226
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v0

    sget-object v1, LX/0XG;->PHONE_NUMBER:LX/0XG;

    if-ne v0, v1, :cond_1

    .line 2623227
    new-instance v0, Lcom/facebook/user/model/UserSmsIdentifier;

    invoke-virtual {p0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/user/model/UserKey;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/user/model/UserSmsIdentifier;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2623228
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v0

    sget-object v1, LX/0XG;->EMAIL:LX/0XG;

    if-ne v0, v1, :cond_2

    .line 2623229
    new-instance v0, Lcom/facebook/user/model/UserSmsIdentifier;

    invoke-virtual {p0}, Lcom/facebook/user/model/UserKey;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/user/model/UserSmsIdentifier;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 2623230
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported UserKey type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;)V
    .locals 14

    .prologue
    .line 2623258
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2623259
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->f:LX/2MA;

    sget-object v1, LX/8Bk;->MQTT:LX/8Bk;

    invoke-interface {v0, v1}, LX/2MA;->b(LX/8Bk;)Z

    move-result v2

    .line 2623260
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2623261
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->n:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ItQ;

    .line 2623262
    if-nez v2, :cond_1

    .line 2623263
    iget v4, v1, LX/ItQ;->c:I

    if-lez v4, :cond_4

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 2623264
    if-eqz v4, :cond_1

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpl-double v4, v4, v6

    if-gtz v4, :cond_0

    .line 2623265
    :cond_1
    const/4 v4, 0x0

    .line 2623266
    invoke-virtual {v1}, LX/ItQ;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v5

    .line 2623267
    if-nez v5, :cond_5

    .line 2623268
    :goto_2
    move v4, v4

    .line 2623269
    if-eqz v4, :cond_0

    .line 2623270
    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->n:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2623271
    iget-object v4, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->o:Ljava/util/Map;

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2623272
    invoke-virtual {v1}, LX/ItQ;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2623273
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2623274
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->c(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2623275
    :try_start_0
    iget-object v8, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->e:LX/It6;

    iget-object v9, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2623276
    invoke-static {v8, v9}, LX/It6;->c(LX/It6;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/It5;

    move-result-object v10

    .line 2623277
    iget-object v10, v10, LX/It5;->b:LX/0Px;

    move-object v10, v10

    .line 2623278
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 2623279
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v12

    const/4 v8, 0x0

    move v9, v8

    :goto_3
    if-ge v9, v12, :cond_2

    invoke-virtual {v10, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2623280
    iget-object v8, v8, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-static {v8}, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/UserIdentifier;

    move-result-object v8

    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2623281
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_3

    .line 2623282
    :cond_2
    new-instance v8, LX/6j1;

    invoke-direct {v8}, LX/6j1;-><init>()V

    move-object v8, v8

    .line 2623283
    iput-object v1, v8, LX/6j1;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2623284
    move-object v8, v8

    .line 2623285
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 2623286
    invoke-static {v9}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v10

    iput-object v10, v8, LX/6j1;->b:LX/0Px;

    .line 2623287
    move-object v8, v8

    .line 2623288
    iget-object v9, v8, LX/6j1;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2623289
    iget-object v9, v8, LX/6j1;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v9, v9, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v9

    invoke-static {v9}, LX/0PB;->checkArgument(Z)V

    .line 2623290
    iget-object v9, v8, LX/6j1;->b:LX/0Px;

    invoke-static {v9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2623291
    new-instance v9, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;

    iget-object v10, v8, LX/6j1;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v11, v8, LX/6j1;->b:LX/0Px;

    iget-object v12, v8, LX/6j1;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-direct {v9, v10, v11, v12}, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;-><init>(Lcom/facebook/messaging/model/messages/Message;LX/0Px;Lcom/facebook/fbtrace/FbTraceNode;)V

    move-object v8, v9

    .line 2623292
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 2623293
    const-string v9, "sendMessageToPendingThreadParams"

    invoke-virtual {v10, v9, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2623294
    iget-object v8, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->a:LX/0aG;

    const-string v9, "send_to_pending_thread"

    sget-object v11, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const-class v12, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;

    invoke-static {v12}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v12

    const v13, -0x6a175261

    invoke-static/range {v8 .. v13}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v8

    invoke-interface {v8}, LX/1MF;->start()LX/1ML;

    move-result-object v9

    .line 2623295
    new-instance v10, LX/ItO;

    invoke-direct {v10, p0, v1}, LX/ItO;-><init>(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;Lcom/facebook/messaging/model/messages/Message;)V

    iget-object v8, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->c:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    invoke-static {v9, v10, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch LX/It4; {:try_start_0 .. :try_end_0} :catch_0

    .line 2623296
    goto/16 :goto_0

    .line 2623297
    :catch_0
    move-exception v1

    .line 2623298
    invoke-static {p0, v0, v1}, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->a(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;Lcom/facebook/messaging/model/messages/Message;LX/It4;)V

    goto/16 :goto_0

    .line 2623299
    :cond_3
    return-void

    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 2623300
    :cond_5
    iget-object v6, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v6, v5}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/messaging/model/messages/Message;)LX/FHZ;

    move-result-object v6

    iget-object v6, v6, LX/FHZ;->b:LX/FHY;

    .line 2623301
    sget-object v7, LX/ItP;->a:[I

    invoke-virtual {v6}, LX/FHY;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_0

    goto/16 :goto_2

    .line 2623302
    :pswitch_0
    iget-object v6, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v6, v5}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(Lcom/facebook/messaging/model/messages/Message;)V

    goto/16 :goto_2

    .line 2623303
    :pswitch_1
    const/4 v4, 0x1

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;Lcom/facebook/messaging/model/messages/Message;LX/It4;)V
    .locals 4

    .prologue
    .line 2623256
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "SendMessageToPendingThreadManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PendingThreadsManager doesn\'t have pending thread key: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2623257
    return-void
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;
    .locals 13

    .prologue
    .line 2623254
    new-instance v0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    const/16 v2, 0x1430

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x1622

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {p0}, LX/It6;->a(LX/0QB;)LX/It6;

    move-result-object v5

    check-cast v5, LX/It6;

    invoke-static {p0}, LX/2M4;->a(LX/0QB;)LX/2MA;

    move-result-object v6

    check-cast v6, LX/2MA;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {p0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(LX/0QB;)Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-static {p0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v9

    check-cast v9, LX/2Ly;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v10

    check-cast v10, LX/0Xl;

    const/16 v11, 0x259

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2922

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v0 .. v12}, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;-><init>(LX/0aG;LX/0Ot;LX/0Ot;LX/0Sh;LX/It6;LX/2MA;LX/0Zb;Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;LX/2Ly;LX/0Xl;LX/0Ot;LX/0Ot;)V

    .line 2623255
    return-object v0
.end method

.method public static b(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;LX/ItQ;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2623246
    iget v1, p1, LX/ItQ;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, LX/ItQ;->c:I

    move v1, v1

    .line 2623247
    int-to-long v2, v1

    const-wide/16 v4, 0x3

    cmp-long v1, v2, v4

    if-ltz v1, :cond_1

    .line 2623248
    :cond_0
    :goto_0
    return v0

    .line 2623249
    :cond_1
    invoke-virtual {p1}, LX/ItQ;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2623250
    if-eqz v1, :cond_2

    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v2, v2, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-boolean v2, v2, LX/6fP;->shouldNotBeRetried:Z

    if-nez v2, :cond_0

    .line 2623251
    :cond_2
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->i:LX/2Ly;

    invoke-virtual {v2, v1}, LX/2Ly;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v2

    sget-object v3, LX/6eh;->VIDEO_CLIP:LX/6eh;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->h:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v2, v1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/messaging/model/messages/Message;)LX/FHZ;

    move-result-object v2

    iget-object v2, v2, LX/FHZ;->b:LX/FHY;

    sget-object v3, LX/FHY;->FAILED:LX/FHY;

    if-eq v2, v3, :cond_0

    .line 2623252
    :cond_3
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->i:LX/2Ly;

    invoke-virtual {v2, v1}, LX/2Ly;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6eh;

    move-result-object v1

    sget-object v2, LX/6eh;->PAYMENT:LX/6eh;

    if-eq v1, v2, :cond_0

    .line 2623253
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/model/messages/Message;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/messages/Message;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/ItR;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2623234
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2623235
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2623236
    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2623237
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->n:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ItQ;

    .line 2623238
    if-nez v0, :cond_0

    .line 2623239
    new-instance v0, LX/ItQ;

    invoke-direct {v0, v1}, LX/ItQ;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2623240
    iget-object v2, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->n:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2623241
    :cond_0
    iget-object v1, v0, LX/ItQ;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2623242
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 2623243
    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->q:Ljava/util/Map;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2623244
    invoke-static {p0}, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->a(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;)V

    .line 2623245
    return-object v0
.end method

.method public final clearUserData()V
    .locals 3

    .prologue
    .line 2623231
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->m:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->c()V

    .line 2623232
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager$5;

    invoke-direct {v1, p0}, Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager$5;-><init>(Lcom/facebook/messaging/send/client/SendMessageToPendingThreadManager;)V

    const v2, -0x5c87408c

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2623233
    return-void
.end method
