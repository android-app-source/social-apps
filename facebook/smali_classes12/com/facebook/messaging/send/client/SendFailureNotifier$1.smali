.class public final Lcom/facebook/messaging/send/client/SendFailureNotifier$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/messages/Message;

.field public final synthetic b:LX/ItB;


# direct methods
.method public constructor <init>(LX/ItB;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 0

    .prologue
    .line 2622006
    iput-object p1, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->b:LX/ItB;

    iput-object p2, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2622007
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->b:LX/ItB;

    .line 2622008
    iget-object v1, v0, LX/ItB;->f:LX/2EL;

    .line 2622009
    iget-object v2, v1, LX/2EL;->k:LX/2EQ;

    move-object v1, v2

    .line 2622010
    sget-object v2, LX/2EQ;->CAPTIVE_PORTAL:LX/2EQ;

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2622011
    if-eqz v0, :cond_1

    .line 2622012
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->b:LX/ItB;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2622013
    sget-object v2, LX/0db;->aL:LX/0Tn;

    invoke-static {v0, v2}, LX/ItB;->b(LX/ItB;LX/0Tn;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2622014
    :cond_0
    :goto_1
    return-void

    .line 2622015
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->b:LX/ItB;

    invoke-static {v0}, LX/ItB;->b$redex0(LX/ItB;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2622016
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->b:LX/ItB;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2622017
    sget-object v2, LX/0db;->aN:LX/0Tn;

    invoke-static {v0, v2}, LX/ItB;->b(LX/ItB;LX/0Tn;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2622018
    :goto_2
    goto :goto_1

    .line 2622019
    :cond_2
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->b:LX/ItB;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2622020
    iget-object v2, v0, LX/ItB;->h:LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    iget-wide v4, v1, Lcom/facebook/messaging/model/messages/Message;->c:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xa4cb80

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    const/4 v2, 0x1

    :goto_3
    move v0, v2

    .line 2622021
    if-eqz v0, :cond_0

    .line 2622022
    iget-object v0, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->b:LX/ItB;

    iget-object v1, p0, Lcom/facebook/messaging/send/client/SendFailureNotifier$1;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2622023
    sget-object v2, LX/0db;->aM:LX/0Tn;

    invoke-static {v0, v2}, LX/ItB;->b(LX/ItB;LX/0Tn;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2622024
    :goto_4
    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 2622025
    :cond_4
    sget-object v2, LX/ItA;->CAPTIVE_PORTAL:LX/ItA;

    sget-object p0, LX/0db;->aL:LX/0Tn;

    invoke-static {v0, v1, v2, p0}, LX/ItB;->a(LX/ItB;Lcom/facebook/messaging/model/messages/Message;LX/ItA;LX/0Tn;)V

    goto :goto_1

    .line 2622026
    :cond_5
    sget-object v2, LX/ItA;->BACKGROUND_DATA_RESTRICTION:LX/ItA;

    sget-object p0, LX/0db;->aN:LX/0Tn;

    invoke-static {v0, v1, v2, p0}, LX/ItB;->a(LX/ItB;Lcom/facebook/messaging/model/messages/Message;LX/ItA;LX/0Tn;)V

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    goto :goto_3

    .line 2622027
    :cond_7
    sget-object v2, LX/ItA;->LONG_QUEUE_TIME:LX/ItA;

    sget-object v3, LX/0db;->aM:LX/0Tn;

    invoke-static {v0, v1, v2, v3}, LX/ItB;->a(LX/ItB;Lcom/facebook/messaging/model/messages/Message;LX/ItA;LX/0Tn;)V

    goto :goto_4
.end method
