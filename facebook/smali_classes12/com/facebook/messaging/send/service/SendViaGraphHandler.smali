.class public Lcom/facebook/messaging/send/service/SendViaGraphHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final r:Ljava/lang/Object;


# instance fields
.field public final a:LX/18V;

.field private final b:LX/FKx;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FL0;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/J0j;

.field public final e:LX/FKY;

.field private final f:LX/FKd;

.field private final g:LX/FKe;

.field private final h:LX/Itm;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0SG;

.field private final k:LX/1sj;

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IzJ;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/1tL;

.field private final n:LX/2Ly;

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKg;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/2Mv;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FKz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2624909
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->r:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/18V;LX/FKx;LX/0Ot;LX/J0j;LX/FKY;LX/FKd;LX/FKe;LX/Itm;LX/0Ot;LX/0SG;LX/1sj;LX/0Ot;LX/1tL;LX/2Ly;LX/0Ot;LX/2Mv;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "LX/FKx;",
            "LX/0Ot",
            "<",
            "LX/FL0;",
            ">;",
            "LX/J0j;",
            "LX/FKY;",
            "LX/FKd;",
            "LX/FKe;",
            "LX/Itm;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0SG;",
            "LX/1sj;",
            "LX/0Ot",
            "<",
            "LX/IzJ;",
            ">;",
            "LX/1tL;",
            "LX/2Ly;",
            "LX/0Ot",
            "<",
            "LX/FKg;",
            ">;",
            "LX/2Mv;",
            "LX/0Ot",
            "<",
            "LX/FKz;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2624911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2624912
    iput-object p1, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a:LX/18V;

    .line 2624913
    iput-object p2, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->b:LX/FKx;

    .line 2624914
    iput-object p3, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->c:LX/0Ot;

    .line 2624915
    iput-object p4, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->d:LX/J0j;

    .line 2624916
    iput-object p5, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->e:LX/FKY;

    .line 2624917
    iput-object p6, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->f:LX/FKd;

    .line 2624918
    iput-object p7, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->g:LX/FKe;

    .line 2624919
    iput-object p8, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->h:LX/Itm;

    .line 2624920
    iput-object p9, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->i:LX/0Ot;

    .line 2624921
    iput-object p10, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->j:LX/0SG;

    .line 2624922
    iput-object p11, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->k:LX/1sj;

    .line 2624923
    iput-object p12, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->l:LX/0Ot;

    .line 2624924
    iput-object p13, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->m:LX/1tL;

    .line 2624925
    iput-object p14, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->n:LX/2Ly;

    .line 2624926
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->o:LX/0Ot;

    .line 2624927
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->p:LX/2Mv;

    .line 2624928
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->q:LX/0Ot;

    .line 2624929
    return-void
.end method

.method private static a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;
    .locals 2

    .prologue
    .line 2624910
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    sget-object v1, LX/6f3;->GRAPH:LX/6f3;

    invoke-virtual {v0, v1}, LX/6f7;->a(LX/6f3;)LX/6f7;

    move-result-object v0

    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/send/service/SendViaGraphHandler;
    .locals 7

    .prologue
    .line 2624874
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2624875
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2624876
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2624877
    if-nez v1, :cond_0

    .line 2624878
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2624879
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2624880
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2624881
    sget-object v1, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->r:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2624882
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2624883
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2624884
    :cond_1
    if-nez v1, :cond_4

    .line 2624885
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2624886
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2624887
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->b(LX/0QB;)Lcom/facebook/messaging/send/service/SendViaGraphHandler;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2624888
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2624889
    if-nez v1, :cond_2

    .line 2624890
    sget-object v0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->r:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2624891
    :goto_1
    if-eqz v0, :cond_3

    .line 2624892
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2624893
    :goto_3
    check-cast v0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2624894
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2624895
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2624896
    :catchall_1
    move-exception v0

    .line 2624897
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2624898
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2624899
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2624900
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->r:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(Lcom/facebook/messaging/send/service/SendViaGraphHandler;Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 4

    .prologue
    .line 2624901
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "missing_sent_msg"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2624902
    const-string v0, "server_received_msg_id"

    invoke-virtual {v1, v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2624903
    const-string v0, "offline_threading_id"

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2624904
    const-string v0, "thread_type"

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    iget-object v2, v2, LX/5e9;->dbValue:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2624905
    const-string v0, "thread_fbid"

    iget-object v2, p2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2624906
    const-string v0, "msg_type"

    iget-object v2, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->n:LX/2Ly;

    invoke-virtual {v2, p2}, LX/2Ly;->b(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2624907
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2624908
    return-void
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/send/service/SendViaGraphHandler;
    .locals 20

    .prologue
    .line 2624872
    new-instance v2, Lcom/facebook/messaging/send/service/SendViaGraphHandler;

    invoke-static/range {p0 .. p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    invoke-static/range {p0 .. p0}, LX/FKx;->a(LX/0QB;)LX/FKx;

    move-result-object v4

    check-cast v4, LX/FKx;

    const/16 v5, 0x2959

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/J0j;->a(LX/0QB;)LX/J0j;

    move-result-object v6

    check-cast v6, LX/J0j;

    invoke-static/range {p0 .. p0}, LX/FKY;->a(LX/0QB;)LX/FKY;

    move-result-object v7

    check-cast v7, LX/FKY;

    invoke-static/range {p0 .. p0}, LX/FKd;->a(LX/0QB;)LX/FKd;

    move-result-object v8

    check-cast v8, LX/FKd;

    invoke-static/range {p0 .. p0}, LX/FKe;->a(LX/0QB;)LX/FKe;

    move-result-object v9

    check-cast v9, LX/FKe;

    invoke-static/range {p0 .. p0}, LX/Itm;->a(LX/0QB;)LX/Itm;

    move-result-object v10

    check-cast v10, LX/Itm;

    const/16 v11, 0xbc

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/1sj;->a(LX/0QB;)LX/1sj;

    move-result-object v13

    check-cast v13, LX/1sj;

    const/16 v14, 0x2d30

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/1tL;->a(LX/0QB;)LX/1tL;

    move-result-object v15

    check-cast v15, LX/1tL;

    invoke-static/range {p0 .. p0}, LX/2Ly;->a(LX/0QB;)LX/2Ly;

    move-result-object v16

    check-cast v16, LX/2Ly;

    const/16 v17, 0x2946

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/2Mv;->a(LX/0QB;)LX/2Mv;

    move-result-object v18

    check-cast v18, LX/2Mv;

    const/16 v19, 0x2958

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-direct/range {v2 .. v19}, Lcom/facebook/messaging/send/service/SendViaGraphHandler;-><init>(LX/18V;LX/FKx;LX/0Ot;LX/J0j;LX/FKY;LX/FKd;LX/FKe;LX/Itm;LX/0Ot;LX/0SG;LX/1sj;LX/0Ot;LX/1tL;LX/2Ly;LX/0Ot;LX/2Mv;LX/0Ot;)V

    .line 2624873
    return-object v2
.end method

.method private static c(Lcom/facebook/messaging/send/service/SendViaGraphHandler;Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 13

    .prologue
    .line 2624769
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2624770
    iget-object v9, v0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v10, v9, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    .line 2624771
    iget-object v9, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v9, v9, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v11, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-eq v9, v11, :cond_0

    iget-object v9, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v9, v9, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v11, LX/5e9;->GROUP:LX/5e9;

    if-ne v9, v11, :cond_2

    iget-object v9, v10, Lcom/facebook/messaging/model/payment/SentPayment;->j:Ljava/lang/String;

    if-eqz v9, :cond_2

    :cond_0
    const/4 v9, 0x1

    :goto_0
    invoke-static {v9}, LX/0PB;->checkArgument(Z)V

    .line 2624772
    invoke-static {}, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->newBuilder()LX/J1W;

    move-result-object v9

    iget-object v11, v10, Lcom/facebook/messaging/model/payment/SentPayment;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2624773
    iput-object v11, v9, LX/J1W;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2624774
    move-object v9, v9

    .line 2624775
    iget-wide v11, v10, Lcom/facebook/messaging/model/payment/SentPayment;->b:J

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    .line 2624776
    iput-object v11, v9, LX/J1W;->b:Ljava/lang/String;

    .line 2624777
    move-object v9, v9

    .line 2624778
    iget-object v11, v10, Lcom/facebook/messaging/model/payment/SentPayment;->c:Ljava/lang/String;

    .line 2624779
    iput-object v11, v9, LX/J1W;->c:Ljava/lang/String;

    .line 2624780
    move-object v9, v9

    .line 2624781
    iget-object v11, v10, Lcom/facebook/messaging/model/payment/SentPayment;->d:Ljava/lang/String;

    .line 2624782
    iput-object v11, v9, LX/J1W;->d:Ljava/lang/String;

    .line 2624783
    move-object v9, v9

    .line 2624784
    iget-object v11, v10, Lcom/facebook/messaging/model/payment/SentPayment;->e:Ljava/lang/String;

    .line 2624785
    iput-object v11, v9, LX/J1W;->e:Ljava/lang/String;

    .line 2624786
    move-object v9, v9

    .line 2624787
    iget-object v11, v10, Lcom/facebook/messaging/model/payment/SentPayment;->f:Ljava/lang/String;

    .line 2624788
    iput-object v11, v9, LX/J1W;->f:Ljava/lang/String;

    .line 2624789
    move-object v9, v9

    .line 2624790
    iget-object v11, v10, Lcom/facebook/messaging/model/payment/SentPayment;->j:Ljava/lang/String;

    .line 2624791
    iput-object v11, v9, LX/J1W;->g:Ljava/lang/String;

    .line 2624792
    move-object v9, v9

    .line 2624793
    iget-object v11, v0, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 2624794
    iput-object v11, v9, LX/J1W;->h:Ljava/lang/String;

    .line 2624795
    move-object v9, v9

    .line 2624796
    iget-object v11, v10, Lcom/facebook/messaging/model/payment/SentPayment;->h:Ljava/lang/String;

    .line 2624797
    iput-object v11, v9, LX/J1W;->i:Ljava/lang/String;

    .line 2624798
    move-object v9, v9

    .line 2624799
    iget-object v11, v10, Lcom/facebook/messaging/model/payment/SentPayment;->i:LX/5g0;

    .line 2624800
    iput-object v11, v9, LX/J1W;->j:LX/5g0;

    .line 2624801
    move-object v9, v9

    .line 2624802
    iget-object v10, v10, Lcom/facebook/messaging/model/payment/SentPayment;->k:Ljava/lang/String;

    .line 2624803
    iput-object v10, v9, LX/J1W;->n:Ljava/lang/String;

    .line 2624804
    move-object v9, v9

    .line 2624805
    invoke-virtual {v9}, LX/J1W;->o()Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;

    move-result-object v9

    move-object v8, v9

    .line 2624806
    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-static {v0}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v2

    .line 2624807
    invoke-static {v2}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v0

    .line 2624808
    const-string v1, "op"

    const-string v3, "send_payment_message_via_graph"

    invoke-interface {v0, v1, v3}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2624809
    iget-object v1, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->k:LX/1sj;

    sget-object v3, LX/2gR;->REQUEST_SEND:LX/2gR;

    invoke-virtual {v1, v2, v3, v0}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2624810
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    .line 2624811
    iput-object v2, v0, LX/14U;->g:Lcom/facebook/fbtrace/FbTraceNode;

    .line 2624812
    iget-object v1, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a:LX/18V;

    iget-object v3, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->d:LX/J0j;

    invoke-virtual {v1, v3, v8, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;

    .line 2624813
    iget-object v1, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    const-string v3, "p2p_send_success"

    iget-object v4, v8, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->k:LX/5g0;

    iget-object v4, v4, LX/5g0;->analyticsModule:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v3

    iget-object v4, v8, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v3, v4}, LX/5fz;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/5fz;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v4, v4, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    iget-object v4, v4, Lcom/facebook/messaging/model/payment/SentPayment;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/5fz;->i(Ljava/lang/String;)LX/5fz;

    move-result-object v3

    iget-object v4, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/5fz;->a(Ljava/lang/String;)LX/5fz;

    move-result-object v3

    .line 2624814
    iget-object v4, v3, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v3, v4

    .line 2624815
    invoke-interface {v1, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2624816
    iget-object v1, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->k:LX/1sj;

    sget-object v3, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2624817
    invoke-static {v0}, LX/InS;->a(Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2624818
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->d()LX/0am;

    move-result-object v1

    .line 2624819
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageResult;->b()Ljava/lang/String;

    move-result-object v0

    .line 2624820
    iget-object v2, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->h:LX/Itm;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    sget-object v4, LX/6f3;->GRAPH:LX/6f3;

    .line 2624821
    invoke-virtual {v2}, LX/Itm;->a()LX/Itl;

    move-result-object v7

    .line 2624822
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2624823
    iget-object v5, v2, LX/Itm;->e:Landroid/content/res/Resources;

    const v6, 0x7f082caa

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2624824
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2624825
    iput-object v5, v7, LX/Itl;->f:Ljava/lang/String;

    .line 2624826
    move-object v5, v6

    .line 2624827
    :goto_1
    invoke-virtual {v7, v3}, LX/Itl;->a(Lcom/facebook/messaging/model/messages/Message;)LX/Itl;

    move-result-object v6

    .line 2624828
    iput-object v5, v6, LX/Itl;->e:Ljava/lang/String;

    .line 2624829
    move-object v6, v6

    .line 2624830
    sget-object v9, LX/6fP;->P2P_PAYMENT_RISK_FAILURE:LX/6fP;

    invoke-virtual {v6, v9}, LX/Itl;->a(LX/6fP;)LX/Itl;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/Itl;->a(LX/6f3;)LX/Itl;

    move-result-object v6

    .line 2624831
    iput-object v0, v6, LX/Itl;->g:Ljava/lang/String;

    .line 2624832
    new-instance v6, LX/FKG;

    invoke-virtual {v7}, LX/Itl;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v7

    invoke-direct {v6, v5, v7}, LX/FKG;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;)V

    move-object v0, v6

    .line 2624833
    throw v0
    :try_end_0
    .catch LX/FKG; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 2624834
    :catch_0
    move-exception v0

    .line 2624835
    throw v0

    .line 2624836
    :cond_1
    :try_start_1
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v0}, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V
    :try_end_1
    .catch LX/FKG; {:try_start_1 .. :try_end_1} :catch_0
    .catch LX/2Oo; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 2624837
    return-object v1

    .line 2624838
    :catch_1
    move-exception v0

    .line 2624839
    iget-object v1, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->h:LX/Itm;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 2624840
    iget-object v9, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->l:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/IzJ;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-virtual {v9, v11, v12}, LX/IzJ;->a(J)LX/0am;

    move-result-object v9

    .line 2624841
    invoke-virtual {v9}, LX/0am;->isPresent()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2624842
    iget-object v10, v9, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    move-object v9, v10

    .line 2624843
    :goto_2
    move-object v3, v9

    .line 2624844
    sget-object v4, LX/6f3;->GRAPH:LX/6f3;

    .line 2624845
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v5

    .line 2624846
    if-nez v5, :cond_5

    .line 2624847
    invoke-virtual {v1, v0, v2, v4}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v5

    .line 2624848
    :goto_3
    move-object v1, v5

    .line 2624849
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v2, "p2p_send_fail"

    iget-object v3, v8, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->k:LX/5g0;

    iget-object v3, v3, LX/5g0;->analyticsModule:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5fz;->i(Ljava/lang/String;)LX/5fz;

    move-result-object v2

    iget-object v3, v8, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v2, v3}, LX/5fz;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/5fz;

    move-result-object v2

    .line 2624850
    iget-object v3, v1, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    move-object v3, v3

    .line 2624851
    iget-object v3, v3, Lcom/facebook/messaging/model/send/SendError;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/5fz;->f(Ljava/lang/String;)LX/5fz;

    move-result-object v2

    const-class v3, LX/2Oo;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5fz;->g(Ljava/lang/String;)LX/5fz;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/5fz;->a(Ljava/lang/String;)LX/5fz;

    move-result-object v2

    .line 2624852
    iget-object v3, v2, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v2, v3

    .line 2624853
    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2624854
    throw v1

    .line 2624855
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 2624856
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v2, "p2p_send_fail"

    iget-object v3, v8, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->k:LX/5g0;

    iget-object v3, v3, LX/5g0;->analyticsModule:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5fz;->i(Ljava/lang/String;)LX/5fz;

    move-result-object v2

    iget-object v3, v8, Lcom/facebook/payments/p2p/service/model/transactions/SendPaymentMessageParams;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v2, v3}, LX/5fz;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/5fz;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5fz;->f(Ljava/lang/String;)LX/5fz;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5fz;->g(Ljava/lang/String;)LX/5fz;

    move-result-object v2

    iget-object v3, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/5fz;->a(Ljava/lang/String;)LX/5fz;

    move-result-object v2

    .line 2624857
    iget-object v3, v2, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v2, v3

    .line 2624858
    invoke-interface {v0, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2624859
    throw v1

    .line 2624860
    :cond_2
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 2624861
    :cond_3
    :try_start_2
    iget-object v5, v2, LX/Itm;->e:Landroid/content/res/Resources;

    const v6, 0x7f082cab

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1
    :try_end_2
    .catch LX/FKG; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/2Oo; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_4
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 2624862
    :cond_5
    invoke-virtual {v5}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2624863
    invoke-virtual {v5}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v5

    invoke-static {v1, v6, v5}, LX/Itm;->a(LX/Itm;Ljava/lang/String;I)V

    .line 2624864
    invoke-virtual {v1}, LX/Itm;->a()LX/Itl;

    move-result-object v7

    .line 2624865
    invoke-virtual {v7, v2}, LX/Itl;->a(Lcom/facebook/messaging/model/messages/Message;)LX/Itl;

    move-result-object v5

    sget-object v9, LX/6fP;->P2P_PAYMENT_FAILURE:LX/6fP;

    invoke-virtual {v5, v9}, LX/Itl;->a(LX/6fP;)LX/Itl;

    move-result-object v5

    .line 2624866
    iput-object v6, v5, LX/Itl;->e:Ljava/lang/String;

    .line 2624867
    move-object v5, v5

    .line 2624868
    iput-object v3, v5, LX/Itl;->g:Ljava/lang/String;

    .line 2624869
    move-object v5, v5

    .line 2624870
    invoke-virtual {v5, v4}, LX/Itl;->a(LX/6f3;)LX/Itl;

    .line 2624871
    new-instance v5, LX/FKG;

    invoke-virtual {v7}, LX/Itl;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v6

    invoke-direct {v5, v0, v6}, LX/FKG;-><init>(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V

    goto/16 :goto_3
.end method

.method private static d(Lcom/facebook/messaging/send/service/SendViaGraphHandler;Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2624711
    iget-object v9, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2624712
    if-eqz v9, :cond_1

    move v0, v7

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2624713
    iget-object v0, v9, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v7

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2624714
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a:LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v10

    .line 2624715
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->b:LX/FKx;

    invoke-static {v0, v9}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "send"

    .line 2624716
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2624717
    move-object v0, v0

    .line 2624718
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v10, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2624719
    new-instance v0, Lcom/facebook/messaging/service/model/FetchMessageParams;

    const-string v1, "{result=send:$.uuid}"

    iget-object v2, v9, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/service/model/FetchMessageParams;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2624720
    iget-object v1, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->f:LX/FKd;

    invoke-static {v1, v0}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetch_sent"

    .line 2624721
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2624722
    move-object v0, v0

    .line 2624723
    const-string v1, "send"

    .line 2624724
    iput-object v1, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2624725
    move-object v0, v0

    .line 2624726
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v10, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2624727
    new-instance v1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;

    iget-object v2, v9, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const-wide/16 v4, 0x0

    const/4 v6, 0x2

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JI)V

    .line 2624728
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->g:LX/FKe;

    invoke-static {v0, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v0

    const-string v1, "fetch"

    .line 2624729
    iput-object v1, v0, LX/2Vk;->c:Ljava/lang/String;

    .line 2624730
    move-object v0, v0

    .line 2624731
    const-string v1, "send"

    .line 2624732
    iput-object v1, v0, LX/2Vk;->d:Ljava/lang/String;

    .line 2624733
    move-object v0, v0

    .line 2624734
    invoke-virtual {v0}, LX/2Vk;->a()LX/2Vj;

    move-result-object v0

    invoke-interface {v10, v0}, LX/2VK;->a(LX/2Vj;)V

    .line 2624735
    iget-object v0, v9, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    .line 2624736
    if-eqz v0, :cond_3

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v7, :cond_3

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->AUDIO:LX/2MK;

    invoke-virtual {v0, v1}, LX/2MK;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2624737
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "audio_upload"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 2624738
    :goto_2
    iget-object v1, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-static {v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v4

    .line 2624739
    invoke-static {v4}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v1

    .line 2624740
    const-string v2, "op"

    const-string v5, "send_message_via_graph"

    invoke-interface {v1, v2, v5}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2624741
    iget-object v2, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->k:LX/1sj;

    sget-object v5, LX/2gR;->REQUEST_SEND:LX/2gR;

    invoke-virtual {v2, v4, v5, v1}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2624742
    new-instance v1, LX/14U;

    invoke-direct {v1}, LX/14U;-><init>()V

    .line 2624743
    iput-object v4, v1, LX/14U;->g:Lcom/facebook/fbtrace/FbTraceNode;

    .line 2624744
    iget-object v2, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->m:LX/1tL;

    invoke-virtual {v2}, LX/1tL;->a()Ljava/lang/String;

    move-result-object v2

    .line 2624745
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2624746
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "X-MSGR-Region"

    invoke-direct {v5, v6, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2624747
    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/14U;->a(LX/0Px;)V

    .line 2624748
    :cond_0
    const-string v2, "sendMessage"

    invoke-interface {v10, v2, v0, v1}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/14U;)V

    .line 2624749
    const-string v0, "send"

    invoke-interface {v10, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2624750
    const-string v1, "fetch_sent"

    invoke-interface {v10, v1}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/FetchMessageResult;

    .line 2624751
    const-string v2, "fetch"

    invoke-interface {v10, v2}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    .line 2624752
    if-nez v1, :cond_4

    .line 2624753
    invoke-static {p0, v0, v9}, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a(Lcom/facebook/messaging/send/service/SendViaGraphHandler;Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624754
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v0, v8

    .line 2624755
    goto/16 :goto_0

    :cond_2
    move v0, v8

    .line 2624756
    goto/16 :goto_1

    .line 2624757
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    goto :goto_2

    .line 2624758
    :cond_4
    invoke-static {v4}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v5

    .line 2624759
    const-string v6, "message_id"

    invoke-interface {v5, v6, v0}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2624760
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->k:LX/1sj;

    sget-object v5, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    invoke-virtual {v0, v4, v5, v3}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2624761
    iget-object v0, v1, Lcom/facebook/messaging/service/model/FetchMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v4, v0

    .line 2624762
    iget-object v1, v2, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->c:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2624763
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v5, v0

    .line 2624764
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v8

    :goto_3
    if-ge v2, v6, :cond_7

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2624765
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-object v9, v4, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-static {v0, v9}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2624766
    :goto_4
    if-nez v7, :cond_6

    move-object v8, v3

    .line 2624767
    :goto_5
    new-instance v5, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v6, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {v4}, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v7

    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v10

    move-object v9, v3

    invoke-direct/range {v5 .. v11}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    return-object v5

    .line 2624768
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_6
    move-object v8, v1

    goto :goto_5

    :cond_7
    move v7, v8

    goto :goto_4
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 11

    .prologue
    .line 2624691
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2624692
    iget-object v1, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->p:LX/2Mv;

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v2}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2624693
    const/4 v7, 0x0

    .line 2624694
    iget-object v4, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2624695
    new-instance v5, LX/14U;

    invoke-direct {v5}, LX/14U;-><init>()V

    .line 2624696
    iget-object v3, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    .line 2624697
    iput-object v3, v5, LX/14U;->g:Lcom/facebook/fbtrace/FbTraceNode;

    .line 2624698
    iget-object v6, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a:LX/18V;

    iget-object v3, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->q:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0e6;

    invoke-virtual {v6, v3, v4, v5}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2624699
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v5

    .line 2624700
    iput-object v3, v5, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2624701
    move-object v5, v5

    .line 2624702
    sget-object v6, LX/6f3;->GRAPH:LX/6f3;

    invoke-virtual {v5, v6}, LX/6f7;->a(LX/6f3;)LX/6f7;

    move-result-object v5

    new-instance v6, Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v4, v4, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    invoke-direct {v6, v3, v4}, Lcom/facebook/messaging/model/send/PendingSendQueueKey;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/6fM;)V

    .line 2624703
    iput-object v6, v5, LX/6f7;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    .line 2624704
    move-object v3, v5

    .line 2624705
    invoke-virtual {v3}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v6

    .line 2624706
    new-instance v4, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v5, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    iget-object v3, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->j:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v9

    move-object v8, v7

    invoke-direct/range {v4 .. v10}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    move-object v0, v4

    .line 2624707
    :goto_0
    return-object v0

    .line 2624708
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    sget-object v1, LX/6fR;->PAYMENT:LX/6fR;

    if-ne v0, v1, :cond_1

    .line 2624709
    invoke-static {p0, p1}, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->c(Lcom/facebook/messaging/send/service/SendViaGraphHandler;Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    goto :goto_0

    .line 2624710
    :cond_1
    invoke-static {p0, p1}, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->d(Lcom/facebook/messaging/send/service/SendViaGraphHandler;Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2624670
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v0

    .line 2624671
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2624672
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->f(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2624673
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-static {v0}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v2

    .line 2624674
    invoke-static {v2}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v0

    .line 2624675
    const-string v3, "op"

    const-string v5, "send_to_pending_thread"

    invoke-interface {v0, v3, v5}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2624676
    iget-object v3, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->k:LX/1sj;

    sget-object v5, LX/2gR;->REQUEST_SEND:LX/2gR;

    invoke-virtual {v3, v2, v5, v0}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2624677
    new-instance v3, LX/14U;

    invoke-direct {v3}, LX/14U;-><init>()V

    .line 2624678
    iput-object v2, v3, LX/14U;->g:Lcom/facebook/fbtrace/FbTraceNode;

    .line 2624679
    iget-object v5, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a:LX/18V;

    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    invoke-virtual {v5, v0, p1, v3}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadResult;

    .line 2624680
    iget-object v3, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->k:LX/1sj;

    sget-object v5, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    invoke-virtual {v3, v2, v5, v4}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V

    .line 2624681
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadResult;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2624682
    iput-object v3, v2, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2624683
    move-object v2, v2

    .line 2624684
    new-instance v3, Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadResult;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v1, v1, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    invoke-direct {v3, v0, v1}, Lcom/facebook/messaging/model/send/PendingSendQueueKey;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/6fM;)V

    .line 2624685
    iput-object v3, v2, LX/6f7;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    .line 2624686
    move-object v0, v2

    .line 2624687
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2624688
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    invoke-static {v0}, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2624689
    return-object v1

    .line 2624690
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
