.class public Lcom/facebook/messaging/send/service/SendApiHandler;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final v:Ljava/lang/Object;


# instance fields
.field private final b:LX/Itt;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/service/SendViaGraphHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/3QL;

.field private final e:LX/FCR;

.field private final f:LX/2Lw;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/media/upload/MediaUploadManager;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/13O;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Itm;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/2Hu;

.field public final k:LX/6f6;

.field private final l:LX/0kb;

.field private final m:LX/0So;

.field private final n:LX/3QM;

.field private final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/FLz;

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Itp;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/2Mv;

.field private final t:LX/0Uh;

.field private final u:LX/2My;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2624194
    const-class v0, Lcom/facebook/messaging/send/service/SendApiHandler;

    sput-object v0, Lcom/facebook/messaging/send/service/SendApiHandler;->a:Ljava/lang/Class;

    .line 2624195
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/facebook/messaging/send/service/SendApiHandler;->v:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Itt;LX/0Ot;LX/3QL;LX/FCR;LX/2Lw;LX/0Ot;LX/13O;LX/0Ot;LX/2Hu;LX/6f6;LX/0kb;LX/0So;LX/3QM;LX/0Or;LX/0Or;LX/FLz;LX/0Ot;LX/2Mv;LX/0Uh;LX/2My;)V
    .locals 1
    .param p12    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .param p14    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/zero/IsMessageCapEnabled;
        .end annotation
    .end param
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/zero/IsMqttDynamicPricingEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Itt;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/service/SendViaGraphHandler;",
            ">;",
            "LX/3QL;",
            "LX/FCR;",
            "LX/2Lw;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/media/upload/MediaUploadManager;",
            ">;",
            "LX/13O;",
            "LX/0Ot",
            "<",
            "LX/Itm;",
            ">;",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClientManager;",
            "LX/6f6;",
            "LX/0kb;",
            "LX/0So;",
            "LX/3QM;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/FLz;",
            "LX/0Ot",
            "<",
            "LX/Itp;",
            ">;",
            "LX/2Mv;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/2My;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2624172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2624173
    iput-object p1, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->b:LX/Itt;

    .line 2624174
    iput-object p2, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->c:LX/0Ot;

    .line 2624175
    iput-object p3, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    .line 2624176
    iput-object p4, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->e:LX/FCR;

    .line 2624177
    iput-object p5, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->f:LX/2Lw;

    .line 2624178
    iput-object p6, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->g:LX/0Ot;

    .line 2624179
    iput-object p7, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->h:LX/13O;

    .line 2624180
    iput-object p8, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->i:LX/0Ot;

    .line 2624181
    iput-object p9, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->j:LX/2Hu;

    .line 2624182
    iput-object p10, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->k:LX/6f6;

    .line 2624183
    iput-object p11, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->l:LX/0kb;

    .line 2624184
    iput-object p12, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    .line 2624185
    iput-object p13, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->n:LX/3QM;

    .line 2624186
    iput-object p14, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->o:LX/0Or;

    .line 2624187
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->p:LX/0Or;

    .line 2624188
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->q:LX/FLz;

    .line 2624189
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->r:LX/0Ot;

    .line 2624190
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->s:LX/2Mv;

    .line 2624191
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->t:LX/0Uh;

    .line 2624192
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->u:LX/2My;

    .line 2624193
    return-void
.end method

.method private a(Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;)LX/Itx;
    .locals 32

    .prologue
    .line 2624112
    invoke-static/range {p0 .. p1}, Lcom/facebook/messaging/send/service/SendApiHandler;->b(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/service/model/SendMessageParams;)LX/Itv;

    move-result-object v2

    .line 2624113
    sget-object v3, LX/Itv;->NONE:LX/Itv;

    if-eq v2, v3, :cond_1

    .line 2624114
    invoke-static {v2}, LX/Itx;->a(LX/Itv;)LX/Itx;

    move-result-object v27

    .line 2624115
    :cond_0
    :goto_0
    return-object v27

    .line 2624116
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->l:LX/0kb;

    invoke-virtual {v3}, LX/0kb;->d()Z

    move-result v3

    sget-object v4, LX/FCY;->MQTT:LX/FCY;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1, v3, v4}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;ZLX/FCY;)V

    .line 2624117
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->e:LX/FCR;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/FCR;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    .line 2624118
    new-instance v29, LX/FCM;

    invoke-direct/range {v29 .. v29}, LX/FCM;-><init>()V

    .line 2624119
    const/4 v2, 0x0

    .line 2624120
    const/4 v8, 0x1

    move-object v9, v2

    :goto_1
    move-object/from16 v0, v29

    iget v2, v0, LX/FCM;->c:I

    if-gt v8, v2, :cond_8

    .line 2624121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->f:LX/2Lw;

    sget-object v3, LX/FCY;->MQTT:LX/FCY;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v2, v3, v4}, LX/2Lw;->a(LX/FCY;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624122
    const/4 v3, 0x0

    .line 2624123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v30

    .line 2624124
    const/4 v10, 0x0

    .line 2624125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->l:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v12

    .line 2624126
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->j:LX/2Hu;

    invoke-virtual {v2}, LX/2Hu;->a()LX/2gV;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v28

    .line 2624127
    :try_start_1
    invoke-virtual/range {v28 .. v28}, LX/2gV;->c()LX/1Mb;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v10

    .line 2624128
    :try_start_2
    move-object/from16 v0, v29

    iget v2, v0, LX/FCM;->e:I

    if-gt v8, v2, :cond_2

    .line 2624129
    move-object/from16 v0, v29

    iget-wide v2, v0, LX/FCM;->d:J

    move-object/from16 v0, v28

    invoke-virtual {v0, v2, v3}, LX/2gV;->a(J)Z

    .line 2624130
    :cond_2
    const-string v2, "SendViaMqttHandler.attemptSend"

    const v3, 0x8ca6891

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2624131
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->b:LX/Itt;

    move-object/from16 v3, p1

    move-object/from16 v4, v29

    move v5, v8

    move-wide/from16 v6, v30

    invoke-virtual/range {v2 .. v7}, LX/Itt;->a(Lcom/facebook/messaging/service/model/SendMessageParams;LX/FCM;IJ)LX/Itx;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v27

    .line 2624132
    :try_start_4
    sget-object v2, LX/Itj;->a:[I

    invoke-virtual/range {v27 .. v27}, LX/Itx;->a()LX/Itw;

    move-result-object v3

    invoke-virtual {v3}, LX/Itw;->ordinal()I

    move-result v3

    aget v2, v2, v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    packed-switch v2, :pswitch_data_0

    .line 2624133
    :cond_3
    const v2, -0x2dd9d5b6

    :try_start_5
    invoke-static {v2}, LX/02m;->a(I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2624134
    if-eqz v28, :cond_9

    .line 2624135
    invoke-virtual/range {v28 .. v28}, LX/2gV;->f()V

    move-object/from16 v2, v27

    .line 2624136
    :cond_4
    :goto_2
    add-int/lit8 v8, v8, 0x1

    move-object v9, v2

    goto :goto_1

    .line 2624137
    :pswitch_0
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    sub-long v4, v4, v30

    const-string v7, "via_mqtt"

    invoke-virtual/range {v27 .. v27}, LX/Itx;->g()Z

    move-result v9

    invoke-virtual/range {v28 .. v28}, LX/2gV;->c()LX/1Mb;

    move-result-object v11

    sget-object v13, LX/FCY;->MQTT:LX/FCY;

    const/4 v14, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v2 .. v14}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;Ljava/lang/String;IZLX/1Mb;LX/1Mb;ZLX/FCY;Z)V

    .line 2624138
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->f:LX/2Lw;

    sget-object v3, LX/FCY;->MQTT:LX/FCY;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/2Lw;->a(LX/FCY;Ljava/lang/String;Z)V

    .line 2624139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->h:LX/13O;

    sget-object v3, LX/77X;->MESSAGE_SENT:LX/77X;

    invoke-virtual {v3}, LX/77X;->toEventName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/13O;->b(Ljava/lang/String;)V

    .line 2624140
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    if-eqz v2, :cond_5

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v2, :cond_5

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v3, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v2, v3, :cond_5

    .line 2624141
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    .line 2624142
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->n:LX/3QM;

    invoke-virtual {v4, v2, v3}, LX/3QM;->a(J)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2624143
    :cond_5
    const v2, -0x117ea0a4

    :try_start_7
    invoke-static {v2}, LX/02m;->a(I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2624144
    if-eqz v28, :cond_0

    .line 2624145
    invoke-virtual/range {v28 .. v28}, LX/2gV;->f()V

    goto/16 :goto_0

    .line 2624146
    :pswitch_1
    :try_start_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    invoke-virtual/range {v27 .. v27}, LX/Itx;->c()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v14, p1

    move-object/from16 v15, p2

    move/from16 v17, v8

    move-object/from16 v18, v10

    move/from16 v19, v12

    invoke-virtual/range {v13 .. v19}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;Ljava/lang/String;ILX/1Mb;Z)V

    .line 2624147
    sget-object v2, Lcom/facebook/messaging/send/service/SendApiHandler;->a:Ljava/lang/Class;

    const-string v3, "Attempted to send an ineligible message over MQTT. message id: %s, reason: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v6, v6, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual/range {v27 .. v27}, LX/Itx;->c()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 2624148
    const v2, 0x6beb1022

    :try_start_9
    invoke-static {v2}, LX/02m;->a(I)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 2624149
    if-eqz v28, :cond_0

    .line 2624150
    invoke-virtual/range {v28 .. v28}, LX/2gV;->f()V

    goto/16 :goto_0

    .line 2624151
    :pswitch_2
    :try_start_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    sub-long v16, v2, v30

    invoke-virtual/range {v27 .. v27}, LX/Itx;->d()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v27 .. v27}, LX/Itx;->e()I

    move-result v20

    invoke-virtual/range {v27 .. v27}, LX/Itx;->f()Z

    move-result v21

    invoke-virtual/range {v28 .. v28}, LX/2gV;->c()LX/1Mb;

    move-result-object v23

    invoke-virtual/range {v27 .. v27}, LX/Itx;->g()Z

    move-result v26

    move-object/from16 v15, p1

    move-object/from16 v18, p2

    move-object/from16 v22, v10

    move/from16 v24, v12

    move/from16 v25, v8

    invoke-virtual/range {v14 .. v26}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;Ljava/lang/String;IZLX/1Mb;LX/1Mb;ZIZ)V

    .line 2624152
    invoke-virtual/range {v27 .. v27}, LX/Itx;->f()Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result v2

    if-eqz v2, :cond_3

    .line 2624153
    const v2, 0x58baad1f

    :try_start_b
    invoke-static {v2}, LX/02m;->a(I)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 2624154
    if-eqz v28, :cond_0

    .line 2624155
    invoke-virtual/range {v28 .. v28}, LX/2gV;->f()V

    goto/16 :goto_0

    .line 2624156
    :catchall_0
    move-exception v2

    :goto_3
    const v3, -0x53ad94cc

    :try_start_c
    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 2624157
    :catch_0
    move-exception v2

    move-object/from16 v22, v10

    move-object/from16 v3, v28

    move-object/from16 v9, v27

    .line 2624158
    :goto_4
    :try_start_d
    sget-object v4, Lcom/facebook/messaging/send/service/SendApiHandler;->a:Ljava/lang/Class;

    const-string v5, "Calling mqtt service failed"

    invoke-static {v4, v5, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2624159
    sget-object v4, LX/Itv;->MQTT_EXCEPTION:LX/Itv;

    iget v4, v4, LX/Itv;->errorCode:I

    invoke-static {v2, v4}, LX/Itx;->a(Ljava/lang/Exception;I)LX/Itx;

    move-result-object v9

    .line 2624160
    const/16 v23, 0x0

    .line 2624161
    if-eqz v3, :cond_6

    .line 2624162
    invoke-virtual {v3}, LX/2gV;->c()LX/1Mb;

    move-result-object v23

    .line 2624163
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    sub-long v16, v4, v30

    invoke-virtual {v9}, LX/Itx;->d()Ljava/lang/String;

    move-result-object v19

    invoke-virtual {v9}, LX/Itx;->e()I

    move-result v20

    invoke-virtual {v9}, LX/Itx;->f()Z

    move-result v21

    invoke-virtual {v9}, LX/Itx;->g()Z

    move-result v26

    move-object/from16 v15, p1

    move-object/from16 v18, p2

    move/from16 v24, v12

    move/from16 v25, v8

    invoke-virtual/range {v14 .. v26}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;Ljava/lang/String;IZLX/1Mb;LX/1Mb;ZIZ)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    move-object v2, v9

    .line 2624164
    :goto_5
    if-eqz v3, :cond_4

    .line 2624165
    invoke-virtual {v3}, LX/2gV;->f()V

    goto/16 :goto_2

    :catch_1
    move-object v2, v9

    goto :goto_5

    .line 2624166
    :catchall_1
    move-exception v2

    move-object/from16 v3, v28

    :goto_6
    if-eqz v3, :cond_7

    .line 2624167
    invoke-virtual {v3}, LX/2gV;->f()V

    :cond_7
    throw v2

    :cond_8
    move-object/from16 v27, v9

    .line 2624168
    goto/16 :goto_0

    .line 2624169
    :catchall_2
    move-exception v2

    goto :goto_6

    .line 2624170
    :catch_2
    move-exception v2

    move-object/from16 v22, v10

    goto :goto_4

    :catch_3
    move-exception v2

    move-object/from16 v22, v10

    move-object/from16 v3, v28

    goto :goto_4

    :catch_4
    move-exception v2

    move-object/from16 v22, v10

    move-object/from16 v3, v28

    goto :goto_4

    .line 2624171
    :catchall_3
    move-exception v2

    move-object/from16 v27, v9

    goto :goto_3

    :cond_9
    move-object/from16 v2, v27

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)Lcom/facebook/messaging/send/service/SendApiHandler;
    .locals 7

    .prologue
    .line 2624085
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2624086
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2624087
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2624088
    if-nez v1, :cond_0

    .line 2624089
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2624090
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2624091
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2624092
    sget-object v1, Lcom/facebook/messaging/send/service/SendApiHandler;->v:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2624093
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2624094
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2624095
    :cond_1
    if-nez v1, :cond_4

    .line 2624096
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2624097
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2624098
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/messaging/send/service/SendApiHandler;->b(LX/0QB;)Lcom/facebook/messaging/send/service/SendApiHandler;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2624099
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2624100
    if-nez v1, :cond_2

    .line 2624101
    sget-object v0, Lcom/facebook/messaging/send/service/SendApiHandler;->v:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/service/SendApiHandler;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2624102
    :goto_1
    if-eqz v0, :cond_3

    .line 2624103
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2624104
    :goto_3
    check-cast v0, Lcom/facebook/messaging/send/service/SendApiHandler;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2624105
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2624106
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2624107
    :catchall_1
    move-exception v0

    .line 2624108
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2624109
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2624110
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2624111
    :cond_2
    :try_start_8
    sget-object v0, Lcom/facebook/messaging/send/service/SendApiHandler;->v:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/service/SendApiHandler;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;Ljava/lang/String;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 19

    .prologue
    .line 2624066
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->l:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v12

    .line 2624067
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    sget-object v3, LX/FCY;->GRAPH:LX/FCY;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1, v12, v3}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;ZLX/FCY;)V

    .line 2624068
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2624069
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->e:LX/FCR;

    iget-object v3, v15, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v4, v15, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/FCR;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    .line 2624070
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->f:LX/2Lw;

    sget-object v3, LX/FCY;->GRAPH:LX/FCY;

    invoke-virtual {v2, v3, v15}, LX/2Lw;->a(LX/FCY;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624071
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v16

    .line 2624072
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v2, v15}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/messaging/model/messages/Message;)LX/FHZ;

    move-result-object v3

    .line 2624073
    iget-object v2, v3, LX/FHZ;->b:LX/FHY;

    sget-object v4, LX/FHY;->IN_PROGRESS:LX/FHY;

    if-eq v2, v4, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v4, "Attempted to send message with in progress media items"

    invoke-static {v2, v4}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2624074
    :try_start_0
    move-object/from16 v0, p0

    invoke-static {v0, v15, v3}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/model/messages/Message;LX/FHZ;)V

    .line 2624075
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/send/service/SendViaGraphHandler;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v18

    .line 2624076
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    sub-long v4, v4, v16

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    sget-object v13, LX/FCY;->GRAPH:LX/FCY;

    const/4 v14, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v2 .. v14}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;Ljava/lang/String;IZLX/1Mb;LX/1Mb;ZLX/FCY;Z)V

    .line 2624077
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->f:LX/2Lw;

    sget-object v3, LX/FCY;->GRAPH:LX/FCY;

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/messaging/service/model/NewMessageResult;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/2Lw;->a(LX/FCY;Ljava/lang/String;Z)V

    .line 2624078
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->h:LX/13O;

    sget-object v3, LX/77X;->MESSAGE_SENT:LX/77X;

    invoke-virtual {v3}, LX/77X;->toEventName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/13O;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2624079
    return-object v18

    .line 2624080
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 2624081
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 2624082
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Itm;

    sget-object v4, LX/6f3;->GRAPH:LX/6f3;

    invoke-virtual {v2, v3, v15, v4}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v11

    .line 2624083
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    sub-long v8, v2, v16

    move-object/from16 v7, p1

    move-object/from16 v10, p2

    move-object/from16 v13, p3

    invoke-virtual/range {v6 .. v13}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;LX/FKG;ZLjava/lang/String;)V

    .line 2624084
    throw v11
.end method

.method public static a(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;LX/6f4;Ljava/lang/String;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2624044
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v10, v0

    .line 2624045
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->l:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v7

    .line 2624046
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    sget-object v2, LX/FCY;->GRAPH:LX/FCY;

    invoke-virtual {v0, p1, p2, v7, v2}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;LX/6f4;ZLX/FCY;)V

    .line 2624047
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->f:LX/2Lw;

    sget-object v2, LX/FCY;->GRAPH:LX/FCY;

    invoke-virtual {v0, v2, v10}, LX/2Lw;->a(LX/FCY;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624048
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 2624049
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-virtual {v0, v10}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/messaging/model/messages/Message;)LX/FHZ;

    move-result-object v4

    .line 2624050
    iget-object v0, v4, LX/FHZ;->b:LX/FHY;

    sget-object v5, LX/FHY;->IN_PROGRESS:LX/FHY;

    if-eq v0, v5, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Attempted to send message with in progress media items"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2624051
    :try_start_0
    invoke-static {p0, v10, v4}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/model/messages/Message;LX/FHZ;)V

    .line 2624052
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2624053
    iget-object v1, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v4, LX/5e9;->PENDING_MY_MONTAGE:LX/5e9;

    if-ne v1, v4, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2624054
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Itp;

    invoke-virtual {v0, p1}, LX/Itp;->a(Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    move-object v9, v0

    .line 2624055
    :goto_2
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    iget-object v1, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    sub-long v2, v4, v2

    const/4 v6, 0x0

    sget-object v8, LX/FCY;->GRAPH:LX/FCY;

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v8}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;JLX/6f4;Ljava/lang/String;IZLX/FCY;)V

    .line 2624056
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->f:LX/2Lw;

    sget-object v1, LX/FCY;->GRAPH:LX/FCY;

    .line 2624057
    iget-object v2, v9, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v2, v2

    .line 2624058
    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/2Lw;->a(LX/FCY;Ljava/lang/String;Z)V

    .line 2624059
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->h:LX/13O;

    sget-object v1, LX/77X;->MESSAGE_SENT:LX/77X;

    invoke-virtual {v1}, LX/77X;->toEventName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/13O;->b(Ljava/lang/String;)V

    .line 2624060
    return-object v9

    :cond_0
    move v0, v1

    .line 2624061
    goto :goto_0

    .line 2624062
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a(Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v9, v0

    goto :goto_2

    .line 2624063
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2624064
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Itm;

    sget-object v2, LX/6f3;->GRAPH:LX/6f3;

    invoke-virtual {v0, v1, v10, v2}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v0

    .line 2624065
    throw v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/model/messages/Message;LX/FHZ;)V
    .locals 4

    .prologue
    .line 2624040
    iget-object v0, p2, LX/FHZ;->b:LX/FHY;

    sget-object v1, LX/FHY;->FAILED:LX/FHY;

    if-ne v0, v1, :cond_0

    .line 2624041
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Itm;

    invoke-virtual {v0}, LX/Itm;->a()LX/Itl;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/Itl;->a(Lcom/facebook/messaging/model/messages/Message;)LX/Itl;

    move-result-object v0

    sget-object v1, LX/6fP;->MEDIA_UPLOAD_FAILED:LX/6fP;

    invoke-virtual {v0, v1}, LX/Itl;->a(LX/6fP;)LX/Itl;

    move-result-object v0

    sget-object v1, LX/6f3;->GRAPH:LX/6f3;

    invoke-virtual {v0, v1}, LX/Itl;->a(LX/6f3;)LX/Itl;

    move-result-object v0

    invoke-virtual {v0}, LX/Itl;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2624042
    new-instance v1, LX/FKG;

    const-string v2, "Media upload failed"

    iget-object v3, p2, LX/FHZ;->a:LX/FGc;

    iget-object v3, v3, LX/FGc;->f:Ljava/lang/Throwable;

    invoke-direct {v1, v2, v3, v0}, LX/FKG;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V

    throw v1

    .line 2624043
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/messaging/send/service/SendApiHandler;Ljava/util/List;Ljava/util/List;ILX/1Mb;LX/1Mb;ZJ)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;",
            "Ljava/util/List",
            "<",
            "LX/Itx;",
            ">;I",
            "LX/1Mb;",
            "LX/1Mb;",
            "ZJ)V"
        }
    .end annotation

    .prologue
    .line 2624196
    const/4 v2, 0x0

    move v15, v2

    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v15, v2, :cond_1

    .line 2624197
    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, LX/Itx;

    .line 2624198
    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2624199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->k:LX/6f6;

    iget-object v5, v3, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v2, v5}, LX/6f6;->b(Lcom/facebook/messaging/model/messages/Message;)LX/6f4;

    move-result-object v6

    .line 2624200
    sget-object v2, LX/Itj;->a:[I

    invoke-virtual {v4}, LX/Itx;->a()LX/Itw;

    move-result-object v5

    invoke-virtual {v5}, LX/Itw;->ordinal()I

    move-result v5

    aget v2, v2, v5

    packed-switch v2, :pswitch_data_0

    .line 2624201
    :cond_0
    :goto_1
    :pswitch_0
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto :goto_0

    .line 2624202
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    const-string v7, "via_mqtt"

    invoke-virtual {v4}, LX/Itx;->g()Z

    move-result v9

    sget-object v13, LX/FCY;->MQTT:LX/FCY;

    const/4 v14, 0x0

    move-wide/from16 v4, p7

    move/from16 v8, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move/from16 v12, p6

    invoke-virtual/range {v2 .. v14}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;Ljava/lang/String;IZLX/1Mb;LX/1Mb;ZLX/FCY;Z)V

    .line 2624203
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->f:LX/2Lw;

    sget-object v4, LX/FCY;->MQTT:LX/FCY;

    iget-object v5, v3, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, LX/2Lw;->a(LX/FCY;Ljava/lang/String;Z)V

    .line 2624204
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->h:LX/13O;

    sget-object v4, LX/77X;->MESSAGE_SENT:LX/77X;

    invoke-virtual {v4}, LX/77X;->toEventName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/13O;->b(Ljava/lang/String;)V

    .line 2624205
    iget-object v2, v3, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    if-eqz v2, :cond_0

    iget-object v2, v3, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v2, :cond_0

    iget-object v2, v3, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v4, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v2, v4, :cond_0

    .line 2624206
    iget-object v2, v3, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    .line 2624207
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->n:LX/3QM;

    invoke-virtual {v4, v2, v3}, LX/3QM;->a(J)V

    goto :goto_1

    .line 2624208
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    invoke-virtual {v4}, LX/Itx;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, LX/Itx;->e()I

    move-result v8

    invoke-virtual {v4}, LX/Itx;->f()Z

    move-result v9

    invoke-virtual {v4}, LX/Itx;->g()Z

    move-result v14

    move-wide/from16 v4, p7

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move/from16 v12, p6

    move/from16 v13, p3

    invoke-virtual/range {v2 .. v14}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;Ljava/lang/String;IZLX/1Mb;LX/1Mb;ZIZ)V

    goto :goto_1

    .line 2624209
    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static b(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/service/model/SendMessageParams;)LX/Itv;
    .locals 4

    .prologue
    .line 2624005
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2624006
    sget-object v0, LX/Itv;->SEND_SKIPPED_DYNAMIC_PRICING_ENABLED:LX/Itv;

    .line 2624007
    :goto_0
    return-object v0

    .line 2624008
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->z:LX/0P1;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->z:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2624009
    sget-object v0, LX/Itv;->SEND_SKIPPED_BROADCAST:LX/Itv;

    goto :goto_0

    .line 2624010
    :cond_1
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->b:Lcom/facebook/messaging/model/share/Share;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->b:Lcom/facebook/messaging/model/share/Share;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/Share;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2624011
    sget-object v0, LX/Itv;->SEND_SKIPPED_FB_SHARE:LX/Itv;

    goto :goto_0

    .line 2624012
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->F:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    if-eqz v0, :cond_4

    .line 2624013
    :cond_3
    sget-object v0, LX/Itv;->SEND_SKIPPED_HAS_APP_ATTRIBUTION:LX/Itv;

    goto :goto_0

    .line 2624014
    :cond_4
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->q:LX/FLz;

    .line 2624015
    iget-object v1, v0, LX/FLz;->a:LX/0ad;

    sget-short v2, LX/FM2;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 2624016
    if-eqz v0, :cond_5

    .line 2624017
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2624018
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 2624019
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_5

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    if-nez v0, :cond_5

    .line 2624020
    invoke-static {}, Landroid/text/Spannable$Factory;->getInstance()Landroid/text/Spannable$Factory;

    move-result-object v0

    .line 2624021
    invoke-virtual {v0, v1}, Landroid/text/Spannable$Factory;->newSpannable(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    .line 2624022
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/7Gw;->a(Landroid/text/Spannable;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2624023
    sget-object v0, LX/Itv;->SEND_SKIPPED_XMA_MESSAGE:LX/Itv;

    goto :goto_0

    .line 2624024
    :cond_5
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v0, :cond_6

    .line 2624025
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->a:LX/6fR;

    sget-object v1, LX/6fR;->PAYMENT:LX/6fR;

    if-ne v0, v1, :cond_6

    .line 2624026
    sget-object v0, LX/Itv;->SEND_SKIPPED_PAYMENT_MESSAGE:LX/Itv;

    goto/16 :goto_0

    .line 2624027
    :cond_6
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v0}, LX/FEC;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2624028
    sget-object v0, LX/Itv;->SEND_SKIPPED_HAS_EVENT_MESSAGE:LX/Itv;

    goto/16 :goto_0

    .line 2624029
    :cond_7
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v0}, LX/FFt;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2624030
    sget-object v0, LX/Itv;->SEND_SKIPPED_LIVE_LOCATION_MESSAGE:LX/Itv;

    goto/16 :goto_0

    .line 2624031
    :cond_8
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-boolean v0, v0, Lcom/facebook/messaging/model/messages/Message;->O:Z

    if-eqz v0, :cond_9

    .line 2624032
    sget-object v0, LX/Itv;->SEND_SKIPPED_HAS_FLOWER_BORDER:LX/Itv;

    goto/16 :goto_0

    .line 2624033
    :cond_9
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 2624034
    sget-object v0, LX/Itv;->SEND_SKIPPED_HAS_MONTAGE_REPLY:LX/Itv;

    goto/16 :goto_0

    .line 2624035
    :cond_a
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {v0}, LX/FFj;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2624036
    sget-object v0, LX/Itv;->SEND_SKIPPED_LIGHTWEIGHT_ACTION_MESSAGE:LX/Itv;

    goto/16 :goto_0

    .line 2624037
    :cond_b
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    if-eqz v0, :cond_c

    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->t:LX/0Uh;

    const/16 v1, 0x283

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_c

    .line 2624038
    sget-object v0, LX/Itv;->SEND_SKIPPED_HAS_PLATFORM_METADATA:LX/Itv;

    goto/16 :goto_0

    .line 2624039
    :cond_c
    sget-object v0, LX/Itv;->NONE:LX/Itv;

    goto/16 :goto_0
.end method

.method private static b(Lcom/facebook/messaging/send/service/SendApiHandler;Ljava/util/List;)LX/Ity;
    .locals 29
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;)",
            "LX/Ity;"
        }
    .end annotation

    .prologue
    .line 2623955
    new-instance v2, LX/Ity;

    invoke-direct {v2}, LX/Ity;-><init>()V

    .line 2623956
    invoke-static/range {p0 .. p1}, Lcom/facebook/messaging/send/service/SendApiHandler;->c(Lcom/facebook/messaging/send/service/SendApiHandler;Ljava/util/List;)V

    .line 2623957
    new-instance v4, LX/FCM;

    invoke-direct {v4}, LX/FCM;-><init>()V

    .line 2623958
    const/4 v5, 0x1

    move-object v8, v2

    :goto_0
    iget v2, v4, LX/FCM;->c:I

    if-gt v5, v2, :cond_7

    .line 2623959
    const/4 v3, 0x0

    .line 2623960
    const/4 v13, 0x0

    .line 2623961
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->l:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v15

    .line 2623962
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v6

    .line 2623963
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->j:LX/2Hu;

    invoke-virtual {v2}, LX/2Hu;->a()LX/2gV;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v18

    .line 2623964
    :try_start_1
    invoke-virtual/range {v18 .. v18}, LX/2gV;->c()LX/1Mb;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v13

    .line 2623965
    :try_start_2
    iget v2, v4, LX/FCM;->e:I

    if-gt v5, v2, :cond_0

    .line 2623966
    iget-wide v2, v4, LX/FCM;->d:J

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v3}, LX/2gV;->a(J)Z

    .line 2623967
    :cond_0
    const-string v2, "SendViaMqttHandler.attemptSend"

    const v3, 0x2e349882

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2623968
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->b:LX/Itt;

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, LX/Itt;->a(Ljava/util/List;LX/FCM;IJ)LX/Ity;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    .line 2623969
    :try_start_4
    invoke-virtual {v3}, LX/Ity;->a()Ljava/util/List;

    move-result-object v11

    .line 2623970
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2623971
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Itx;

    .line 2623972
    invoke-virtual {v2}, LX/Itx;->a()LX/Itw;

    move-result-object v8

    sget-object v9, LX/Itw;->SUCCEEDED:LX/Itw;

    if-ne v8, v9, :cond_2

    .line 2623973
    invoke-virtual/range {v18 .. v18}, LX/2gV;->c()LX/1Mb;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v8

    sub-long v16, v8, v6

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move v12, v5

    invoke-static/range {v9 .. v17}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Lcom/facebook/messaging/send/service/SendApiHandler;Ljava/util/List;Ljava/util/List;ILX/1Mb;LX/1Mb;ZJ)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 2623974
    const v2, -0x430acb27

    :try_start_5
    invoke-static {v2}, LX/02m;->a(I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2623975
    if-eqz v18, :cond_1

    .line 2623976
    invoke-virtual/range {v18 .. v18}, LX/2gV;->f()V

    .line 2623977
    :cond_1
    :goto_1
    return-object v3

    .line 2623978
    :cond_2
    :try_start_6
    invoke-virtual {v2}, LX/Itx;->a()LX/Itw;

    move-result-object v8

    sget-object v9, LX/Itw;->FAILED:LX/Itw;

    if-ne v8, v9, :cond_3

    .line 2623979
    invoke-virtual {v2}, LX/Itx;->f()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2623980
    invoke-virtual/range {v18 .. v18}, LX/2gV;->c()LX/1Mb;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v8

    sub-long v16, v8, v6

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move v12, v5

    invoke-static/range {v9 .. v17}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Lcom/facebook/messaging/send/service/SendApiHandler;Ljava/util/List;Ljava/util/List;ILX/1Mb;LX/1Mb;ZJ)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 2623981
    const v2, -0x28267035

    :try_start_7
    invoke-static {v2}, LX/02m;->a(I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2623982
    if-eqz v18, :cond_1

    .line 2623983
    invoke-virtual/range {v18 .. v18}, LX/2gV;->f()V

    goto :goto_1

    .line 2623984
    :cond_3
    const v2, -0x15a43a41

    :try_start_8
    invoke-static {v2}, LX/02m;->a(I)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 2623985
    if-eqz v18, :cond_8

    .line 2623986
    invoke-virtual/range {v18 .. v18}, LX/2gV;->f()V

    move-object v2, v3

    .line 2623987
    :cond_4
    :goto_2
    add-int/lit8 v5, v5, 0x1

    move-object v8, v2

    goto/16 :goto_0

    .line 2623988
    :catchall_0
    move-exception v2

    move-object v3, v8

    :goto_3
    const v8, -0x4545e2d4

    :try_start_9
    invoke-static {v8}, LX/02m;->a(I)V

    throw v2
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 2623989
    :catch_0
    move-exception v2

    move-object/from16 v24, v13

    move-object v8, v3

    move-object/from16 v3, v18

    .line 2623990
    :goto_4
    :try_start_a
    sget-object v9, Lcom/facebook/messaging/send/service/SendApiHandler;->a:Ljava/lang/Class;

    const-string v10, "Calling mqtt service failed"

    invoke-static {v9, v10, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2623991
    sget-object v9, LX/Itv;->MQTT_EXCEPTION:LX/Itv;

    iget v9, v9, LX/Itv;->errorCode:I

    invoke-static {v2, v9}, LX/Itx;->a(Ljava/lang/Exception;I)LX/Itx;

    move-result-object v9

    .line 2623992
    new-instance v2, LX/Ity;

    invoke-direct {v2}, LX/Ity;-><init>()V

    invoke-virtual {v2, v9}, LX/Ity;->a(LX/Itx;)LX/Ity;

    move-result-object v8

    .line 2623993
    const/16 v25, 0x0

    .line 2623994
    if-eqz v3, :cond_5

    .line 2623995
    invoke-virtual {v3}, LX/2gV;->c()LX/1Mb;

    move-result-object v25

    .line 2623996
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    move-object/from16 v16, v0

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/facebook/messaging/service/model/SendMessageParams;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v10

    sub-long v18, v10, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->k:LX/6f6;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v2, v2, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v6, v2}, LX/6f6;->b(Lcom/facebook/messaging/model/messages/Message;)LX/6f4;

    move-result-object v20

    invoke-virtual {v9}, LX/Itx;->d()Ljava/lang/String;

    move-result-object v21

    invoke-virtual {v9}, LX/Itx;->e()I

    move-result v22

    invoke-virtual {v9}, LX/Itx;->f()Z

    move-result v23

    invoke-virtual {v9}, LX/Itx;->g()Z

    move-result v28

    move/from16 v26, v15

    move/from16 v27, v5

    invoke-virtual/range {v16 .. v28}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;JLX/6f4;Ljava/lang/String;IZLX/1Mb;LX/1Mb;ZIZ)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-object v2, v8

    .line 2623997
    :goto_5
    if-eqz v3, :cond_4

    .line 2623998
    invoke-virtual {v3}, LX/2gV;->f()V

    goto :goto_2

    :catch_1
    move-object v2, v8

    goto :goto_5

    .line 2623999
    :catchall_1
    move-exception v2

    :goto_6
    if-eqz v3, :cond_6

    .line 2624000
    invoke-virtual {v3}, LX/2gV;->f()V

    :cond_6
    throw v2

    :cond_7
    move-object v3, v8

    .line 2624001
    goto/16 :goto_1

    .line 2624002
    :catchall_2
    move-exception v2

    move-object/from16 v3, v18

    goto :goto_6

    .line 2624003
    :catch_2
    move-exception v2

    move-object/from16 v24, v13

    goto :goto_4

    :catch_3
    move-exception v2

    move-object/from16 v24, v13

    move-object/from16 v3, v18

    goto/16 :goto_4

    :catch_4
    move-exception v2

    move-object/from16 v24, v13

    move-object/from16 v3, v18

    goto/16 :goto_4

    :catch_5
    move-exception v2

    move-object/from16 v24, v13

    move-object v8, v3

    move-object/from16 v3, v18

    goto/16 :goto_4

    .line 2624004
    :catchall_3
    move-exception v2

    goto/16 :goto_3

    :cond_8
    move-object v2, v3

    goto/16 :goto_2
.end method

.method private static b(LX/0QB;)Lcom/facebook/messaging/send/service/SendApiHandler;
    .locals 23

    .prologue
    .line 2623953
    new-instance v2, Lcom/facebook/messaging/send/service/SendApiHandler;

    invoke-static/range {p0 .. p0}, LX/Itt;->a(LX/0QB;)LX/Itt;

    move-result-object v3

    check-cast v3, LX/Itt;

    const/16 v4, 0x2930

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/3QL;->a(LX/0QB;)LX/3QL;

    move-result-object v5

    check-cast v5, LX/3QL;

    invoke-static/range {p0 .. p0}, LX/FCR;->a(LX/0QB;)LX/FCR;

    move-result-object v6

    check-cast v6, LX/FCR;

    invoke-static/range {p0 .. p0}, LX/2Lw;->a(LX/0QB;)LX/2Lw;

    move-result-object v7

    check-cast v7, LX/2Lw;

    const/16 v8, 0xd4e

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/13O;->a(LX/0QB;)LX/13O;

    move-result-object v9

    check-cast v9, LX/13O;

    const/16 v10, 0x292d

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v11

    check-cast v11, LX/2Hu;

    invoke-static/range {p0 .. p0}, LX/6f6;->a(LX/0QB;)LX/6f6;

    move-result-object v12

    check-cast v12, LX/6f6;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v13

    check-cast v13, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v14

    check-cast v14, LX/0So;

    invoke-static/range {p0 .. p0}, LX/3QM;->a(LX/0QB;)LX/3QM;

    move-result-object v15

    check-cast v15, LX/3QM;

    const/16 v16, 0x1536

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const/16 v17, 0x1537

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {p0 .. p0}, LX/FLz;->a(LX/0QB;)LX/FLz;

    move-result-object v18

    check-cast v18, LX/FLz;

    const/16 v19, 0x292f

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {p0 .. p0}, LX/2Mv;->a(LX/0QB;)LX/2Mv;

    move-result-object v20

    check-cast v20, LX/2Mv;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v21

    check-cast v21, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/2My;->a(LX/0QB;)LX/2My;

    move-result-object v22

    check-cast v22, LX/2My;

    invoke-direct/range {v2 .. v22}, Lcom/facebook/messaging/send/service/SendApiHandler;-><init>(LX/Itt;LX/0Ot;LX/3QL;LX/FCR;LX/2Lw;LX/0Ot;LX/13O;LX/0Ot;LX/2Hu;LX/6f6;LX/0kb;LX/0So;LX/3QM;LX/0Or;LX/0Or;LX/FLz;LX/0Ot;LX/2Mv;LX/0Uh;LX/2My;)V

    .line 2623954
    return-object v2
.end method

.method public static b(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 10

    .prologue
    .line 2623916
    iget-object v0, p1, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->b:Lcom/facebook/messaging/model/messages/Message;

    move-object v3, v0

    .line 2623917
    const-wide v4, 0x7fffffffffffffffL

    .line 2623918
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->k:LX/6f6;

    invoke-virtual {v0, v3}, LX/6f6;->b(Lcom/facebook/messaging/model/messages/Message;)LX/6f4;

    move-result-object v6

    .line 2623919
    :try_start_0
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    sget-object v1, LX/FCY;->GRAPH:LX/FCY;

    invoke-virtual {v0, v3, v6, v1}, LX/3QL;->a(Lcom/facebook/messaging/model/messages/Message;LX/6f4;LX/FCY;)V

    .line 2623920
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .line 2623921
    :try_start_1
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;

    .line 2623922
    iget-object v1, v0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->a:LX/18V;

    invoke-virtual {v1}, LX/18V;->a()LX/2VK;

    move-result-object v2

    .line 2623923
    iget-object v1, v0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->e:LX/FKY;

    invoke-static {v1, p1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    const-string v4, "create-thread"

    .line 2623924
    iput-object v4, v1, LX/2Vk;->c:Ljava/lang/String;

    .line 2623925
    move-object v1, v1

    .line 2623926
    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v2, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 2623927
    new-instance v1, LX/6iM;

    invoke-direct {v1}, LX/6iM;-><init>()V

    sget-object v4, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 2623928
    iput-object v4, v1, LX/6iM;->b:LX/0rS;

    .line 2623929
    move-object v1, v1

    .line 2623930
    const/4 v4, 0x3

    .line 2623931
    iput v4, v1, LX/6iM;->g:I

    .line 2623932
    move-object v4, v1

    .line 2623933
    const-string v1, "{result=create-thread:$.thread_fbid}"

    invoke-static {v1}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-result-object v1

    .line 2623934
    iput-object v1, v4, LX/6iM;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    .line 2623935
    iget-object v1, v0, Lcom/facebook/messaging/send/service/SendViaGraphHandler;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0e6;

    invoke-virtual {v4}, LX/6iM;->j()Lcom/facebook/messaging/service/model/FetchThreadParams;

    move-result-object v4

    invoke-static {v1, v4}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    .line 2623936
    const-string v4, "fetch-thread"

    .line 2623937
    iput-object v4, v1, LX/2Vk;->c:Ljava/lang/String;

    .line 2623938
    move-object v1, v1

    .line 2623939
    const-string v4, "create-thread"

    .line 2623940
    iput-object v4, v1, LX/2Vk;->d:Ljava/lang/String;

    .line 2623941
    move-object v1, v1

    .line 2623942
    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v2, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 2623943
    const-string v1, "createThread"

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    invoke-interface {v2, v1, v4}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2623944
    const-string v1, "fetch-thread"

    invoke-interface {v2, v1}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2623945
    move-object v0, v1

    .line 2623946
    iget-object v1, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v4, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    sub-long/2addr v4, v8

    sget-object v7, LX/FCY;->GRAPH:LX/FCY;

    invoke-virtual/range {v1 .. v7}, LX/3QL;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/Message;JLX/6f4;LX/FCY;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 2623947
    return-object v0

    .line 2623948
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2623949
    :goto_0
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Itm;

    sget-object v2, LX/6f3;->UNKNOWN:LX/6f3;

    invoke-virtual {v0, v1, v3, v2}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v7

    .line 2623950
    iget-object v2, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    sub-long v4, v0, v4

    sget-object v8, LX/FCY;->GRAPH:LX/FCY;

    invoke-virtual/range {v2 .. v8}, LX/3QL;->a(Lcom/facebook/messaging/model/messages/Message;JLX/6f4;LX/FKG;LX/FCY;)V

    .line 2623951
    throw v7

    .line 2623952
    :catch_1
    move-exception v0

    move-object v1, v0

    move-wide v4, v8

    goto :goto_0
.end method

.method private static c(Lcom/facebook/messaging/send/service/SendApiHandler;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2623910
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2623911
    iget-object v2, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->k:LX/6f6;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v2, v3}, LX/6f6;->b(Lcom/facebook/messaging/model/messages/Message;)LX/6f4;

    move-result-object v2

    .line 2623912
    iget-object v3, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    iget-object v4, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->l:LX/0kb;

    invoke-virtual {v4}, LX/0kb;->d()Z

    move-result v4

    sget-object v5, LX/FCY;->MQTT:LX/FCY;

    invoke-virtual {v3, v0, v2, v4, v5}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;ZLX/FCY;)V

    .line 2623913
    iget-object v2, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->e:LX/FCR;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v4, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/FCR;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    .line 2623914
    iget-object v2, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->f:LX/2Lw;

    sget-object v3, LX/FCY;->MQTT:LX/FCY;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v2, v3, v0}, LX/2Lw;->a(LX/FCY;Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_0

    .line 2623915
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 5

    .prologue
    .line 2623886
    iget-object v1, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2623887
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->k:LX/6f6;

    iget-object v2, p1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0, v2}, LX/6f6;->b(Lcom/facebook/messaging/model/messages/Message;)LX/6f4;

    move-result-object v2

    .line 2623888
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->u:LX/2My;

    invoke-virtual {v0}, LX/2My;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->s:LX/2Mv;

    iget-object v3, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v3}, LX/2Mv;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2623889
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Itp;

    invoke-virtual {v0, p1}, LX/Itp;->a(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    .line 2623890
    :goto_0
    return-object v0

    .line 2623891
    :cond_0
    invoke-direct {p0, p1, v2}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;)LX/Itx;

    move-result-object v3

    .line 2623892
    if-eqz v3, :cond_2

    .line 2623893
    iget-object v0, v3, LX/Itx;->a:LX/Itw;

    move-object v0, v0

    .line 2623894
    sget-object v4, LX/Itw;->SUCCEEDED:LX/Itw;

    if-ne v0, v4, :cond_1

    .line 2623895
    invoke-virtual {v3}, LX/Itx;->b()Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    goto :goto_0

    .line 2623896
    :cond_1
    sget-object v4, LX/Itw;->FAILED:LX/Itw;

    if-ne v0, v4, :cond_2

    invoke-virtual {v3}, LX/Itx;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2623897
    iget-object v0, p0, Lcom/facebook/messaging/send/service/SendApiHandler;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Itm;

    invoke-virtual {v0}, LX/Itm;->a()LX/Itl;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/Itl;->a(Lcom/facebook/messaging/model/messages/Message;)LX/Itl;

    move-result-object v0

    invoke-virtual {v3}, LX/Itx;->h()Ljava/lang/String;

    move-result-object v1

    .line 2623898
    iput-object v1, v0, LX/Itl;->e:Ljava/lang/String;

    .line 2623899
    move-object v0, v0

    .line 2623900
    invoke-virtual {v3}, LX/Itx;->e()I

    move-result v1

    .line 2623901
    iput v1, v0, LX/Itl;->h:I

    .line 2623902
    move-object v0, v0

    .line 2623903
    sget-object v1, LX/6fP;->PERMANENT_FAILURE:LX/6fP;

    invoke-virtual {v0, v1}, LX/Itl;->a(LX/6fP;)LX/Itl;

    move-result-object v0

    sget-object v1, LX/6f3;->MQTT:LX/6f3;

    invoke-virtual {v0, v1}, LX/Itl;->a(LX/6f3;)LX/Itl;

    move-result-object v0

    invoke-virtual {v0}, LX/Itl;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2623904
    new-instance v1, LX/FKG;

    invoke-virtual {v3}, LX/Itx;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/FKG;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;)V

    throw v1

    .line 2623905
    :cond_2
    if-eqz v3, :cond_3

    .line 2623906
    iget-object v0, v3, LX/Itx;->a:LX/Itw;

    move-object v0, v0

    .line 2623907
    invoke-virtual {v0}, LX/Itw;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "via_graph_after_mqtt_failure"

    .line 2623908
    :goto_1
    invoke-static {p0, p1, v2, v0}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;Ljava/lang/String;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    goto :goto_0

    .line 2623909
    :cond_3
    const-string v0, "via_graph"

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/NewMessageResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2623841
    new-instance v15, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v15, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2623842
    new-instance v16, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2623843
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2623844
    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/facebook/messaging/send/service/SendApiHandler;->b(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/service/model/SendMessageParams;)LX/Itv;

    move-result-object v4

    .line 2623845
    sget-object v5, LX/Itv;->NONE:LX/Itv;

    if-ne v4, v5, :cond_0

    .line 2623846
    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2623847
    goto :goto_0

    .line 2623848
    :cond_0
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2623849
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2623850
    const-string v3, "via_graph"

    .line 2623851
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->k:LX/6f6;

    iget-object v5, v2, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v4, v5}, LX/6f6;->b(Lcom/facebook/messaging/model/messages/Message;)LX/6f4;

    move-result-object v4

    .line 2623852
    move-object/from16 v0, p0

    invoke-static {v0, v2, v4, v3}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;Ljava/lang/String;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v2

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v15

    .line 2623853
    :goto_1
    return-object v2

    .line 2623854
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, LX/3QL;->a(Ljava/util/List;)V

    .line 2623855
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    .line 2623856
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/facebook/messaging/send/service/SendApiHandler;->b(Lcom/facebook/messaging/send/service/SendApiHandler;Ljava/util/List;)LX/Ity;

    move-result-object v2

    .line 2623857
    invoke-virtual {v2}, LX/Ity;->a()Ljava/util/List;

    move-result-object v17

    .line 2623858
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    .line 2623859
    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Itx;

    .line 2623860
    invoke-virtual {v2}, LX/Itx;->a()LX/Itw;

    move-result-object v3

    sget-object v4, LX/Itw;->FAILED:LX/Itw;

    if-ne v3, v4, :cond_2

    invoke-virtual {v2}, LX/Itx;->f()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2623861
    const-string v4, "via_graph_after_mqtt_failure"

    .line 2623862
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/SendMessageParams;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->k:LX/6f6;

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v3, v3, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v5, v3}, LX/6f6;->b(Lcom/facebook/messaging/model/messages/Message;)LX/6f4;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3, v4}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/service/model/SendMessageParams;LX/6f4;Ljava/lang/String;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v2

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v15

    .line 2623863
    goto :goto_1

    .line 2623864
    :cond_2
    const/4 v4, 0x0

    .line 2623865
    const/4 v3, 0x0

    .line 2623866
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 2623867
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 2623868
    const/4 v2, 0x0

    move v12, v2

    move v13, v3

    move v14, v4

    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    if-ge v12, v2, :cond_5

    .line 2623869
    move-object/from16 v0, v17

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Itx;

    .line 2623870
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2623871
    invoke-virtual {v2}, LX/Itx;->a()LX/Itw;

    move-result-object v4

    sget-object v5, LX/Itw;->SUCCEEDED:LX/Itw;

    if-ne v4, v5, :cond_3

    .line 2623872
    invoke-virtual {v2}, LX/Itx;->b()Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v2

    invoke-interface {v15, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2623873
    add-int/lit8 v4, v14, 0x1

    .line 2623874
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v2, v2, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v3, v13

    .line 2623875
    :goto_3
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    move v13, v3

    move v14, v4

    goto :goto_2

    .line 2623876
    :cond_3
    invoke-virtual {v2}, LX/Itx;->f()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2623877
    invoke-virtual {v2}, LX/Itx;->e()I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_4

    const/4 v4, 0x1

    :goto_4
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2623878
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->i:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Itm;

    invoke-virtual {v4}, LX/Itm;->a()LX/Itl;

    move-result-object v4

    iget-object v3, v3, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v4, v3}, LX/Itl;->a(Lcom/facebook/messaging/model/messages/Message;)LX/Itl;

    move-result-object v3

    invoke-virtual {v2}, LX/Itx;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/Itl;->a(Ljava/lang/String;)LX/Itl;

    move-result-object v3

    invoke-virtual {v2}, LX/Itx;->e()I

    move-result v4

    invoke-virtual {v3, v4}, LX/Itl;->a(I)LX/Itl;

    move-result-object v3

    sget-object v4, LX/6fP;->PERMANENT_FAILURE:LX/6fP;

    invoke-virtual {v3, v4}, LX/Itl;->a(LX/6fP;)LX/Itl;

    move-result-object v3

    sget-object v4, LX/6f3;->MQTT:LX/6f3;

    invoke-virtual {v3, v4}, LX/Itl;->a(LX/6f3;)LX/Itl;

    move-result-object v3

    invoke-virtual {v3}, LX/Itl;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v5

    .line 2623879
    new-instance v3, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v4, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v2}, LX/Itx;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, LX/Itx;->e()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->m:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v10

    invoke-direct/range {v3 .. v11}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;ILcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2623880
    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2623881
    add-int/lit8 v3, v13, 0x1

    .line 2623882
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v2, v2, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v4, v14

    goto/16 :goto_3

    .line 2623883
    :cond_4
    const/4 v4, 0x0

    goto :goto_4

    .line 2623884
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/messaging/send/service/SendApiHandler;->d:LX/3QL;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v3, v16

    move v5, v14

    move v7, v13

    invoke-virtual/range {v2 .. v8}, LX/3QL;->a(Ljava/util/List;IILjava/lang/String;ILjava/lang/String;)V

    move-object v2, v15

    .line 2623885
    goto/16 :goto_1
.end method
