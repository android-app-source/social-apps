.class public Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2650802
    new-instance v0, LX/J7O;

    invoke-direct {v0}, LX/J7O;-><init>()V

    sput-object v0, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2650803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2650804
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->a:Ljava/lang/String;

    .line 2650805
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->b:Ljava/util/List;

    .line 2650806
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->c:Ljava/lang/String;

    .line 2650807
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2650808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2650809
    iput-object p1, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->a:Ljava/lang/String;

    .line 2650810
    iput-object p2, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->b:Ljava/util/List;

    .line 2650811
    iput-object p3, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->c:Ljava/lang/String;

    .line 2650812
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2650813
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2650814
    iget-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2650815
    iget-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2650816
    iget-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2650817
    return-void
.end method
