.class public final Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2650729
    const-class v0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;

    new-instance v1, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2650730
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2650728
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2650702
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2650703
    const/4 v2, 0x0

    .line 2650704
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 2650705
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2650706
    :goto_0
    move v1, v2

    .line 2650707
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2650708
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2650709
    new-instance v1, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;

    invoke-direct {v1}, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;-><init>()V

    .line 2650710
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2650711
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2650712
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2650713
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2650714
    :cond_0
    return-object v1

    .line 2650715
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2650716
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_4

    .line 2650717
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2650718
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2650719
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v4, :cond_2

    .line 2650720
    const-string p0, "cache_id"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2650721
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2650722
    :cond_3
    const-string p0, "status"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2650723
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    goto :goto_1

    .line 2650724
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2650725
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2650726
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2650727
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1
.end method
