.class public Lcom/facebook/profile/inforequest/protocol/DeleteInfoRequestParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/profile/inforequest/protocol/DeleteInfoRequestParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2650649
    new-instance v0, LX/J7I;

    invoke-direct {v0}, LX/J7I;-><init>()V

    sput-object v0, Lcom/facebook/profile/inforequest/protocol/DeleteInfoRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2650650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2650651
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/inforequest/protocol/DeleteInfoRequestParams;->a:Ljava/lang/String;

    .line 2650652
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/inforequest/protocol/DeleteInfoRequestParams;->b:Ljava/util/List;

    .line 2650653
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2650654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2650655
    iput-object p1, p0, Lcom/facebook/profile/inforequest/protocol/DeleteInfoRequestParams;->a:Ljava/lang/String;

    .line 2650656
    iput-object p2, p0, Lcom/facebook/profile/inforequest/protocol/DeleteInfoRequestParams;->b:Ljava/util/List;

    .line 2650657
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2650658
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2650659
    iget-object v0, p0, Lcom/facebook/profile/inforequest/protocol/DeleteInfoRequestParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2650660
    iget-object v0, p0, Lcom/facebook/profile/inforequest/protocol/DeleteInfoRequestParams;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2650661
    return-void
.end method
