.class public final Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x15e578d0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2650785
    const-class v0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2650791
    const-class v0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2650789
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2650790
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2650786
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2650787
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2650788
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;)V
    .locals 4

    .prologue
    .line 2650750
    iput-object p1, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 2650751
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2650752
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2650753
    if-eqz v0, :cond_0

    .line 2650754
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2650755
    :cond_0
    return-void

    .line 2650756
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2650783
    iget-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->e:Ljava/lang/String;

    .line 2650784
    iget-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2650781
    iget-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    iput-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 2650782
    iget-object v0, p0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->f:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2650773
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2650774
    invoke-direct {p0}, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2650775
    invoke-direct {p0}, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2650776
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2650777
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2650778
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2650779
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2650780
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2650792
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2650793
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2650794
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2650772
    new-instance v0, LX/J7L;

    invoke-direct {v0, p1}, LX/J7L;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2650771
    invoke-direct {p0}, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2650765
    const-string v0, "status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2650766
    invoke-direct {p0}, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->k()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2650767
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2650768
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2650769
    :goto_0
    return-void

    .line 2650770
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2650762
    const-string v0, "status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2650763
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    invoke-direct {p0, p2}, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;->a(Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;)V

    .line 2650764
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2650759
    new-instance v0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;-><init>()V

    .line 2650760
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2650761
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2650758
    const v0, -0x5826071e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2650757
    const v0, -0x51dd9867

    return v0
.end method
