.class public final Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2650733
    const-class v0, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;

    new-instance v1, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2650734
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2650735
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2650736
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2650737
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x1

    .line 2650738
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2650739
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2650740
    if-eqz v2, :cond_0

    .line 2650741
    const-string p0, "cache_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2650742
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2650743
    :cond_0
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 2650744
    if-eqz v2, :cond_1

    .line 2650745
    const-string v2, "status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2650746
    invoke-virtual {v1, v0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2650747
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2650748
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2650749
    check-cast p1, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel$Serializer;->a(Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
