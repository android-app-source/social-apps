.class public Lcom/facebook/profile/inforequest/InfoRequestFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/J7A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/J7R;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Landroid/widget/TextView;

.field public g:Landroid/view/View;

.field public h:Landroid/view/View;

.field public i:Landroid/widget/EditText;

.field public j:Landroid/os/ParcelUuid;

.field public k:Landroid/os/Handler;

.field public l:Ljava/lang/Runnable;

.field public m:LX/J73;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2650528
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2650529
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->k:Landroid/os/Handler;

    .line 2650530
    new-instance v0, Lcom/facebook/profile/inforequest/InfoRequestFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/profile/inforequest/InfoRequestFragment$1;-><init>(Lcom/facebook/profile/inforequest/InfoRequestFragment;)V

    iput-object v0, p0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->l:Ljava/lang/Runnable;

    .line 2650531
    new-instance v0, LX/J74;

    invoke-direct {v0, p0}, LX/J74;-><init>(Lcom/facebook/profile/inforequest/InfoRequestFragment;)V

    iput-object v0, p0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->m:LX/J73;

    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x24f5f760

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2650532
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2650533
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v3, p0

    check-cast v3, Lcom/facebook/profile/inforequest/InfoRequestFragment;

    invoke-static {v1}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v4

    check-cast v4, LX/0aG;

    invoke-static {v1}, LX/J7A;->a(LX/0QB;)LX/J7A;

    move-result-object v5

    check-cast v5, LX/J7A;

    invoke-static {v1}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v1}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/Executor;

    invoke-static {v1}, LX/J7R;->b(LX/0QB;)LX/J7R;

    move-result-object v1

    check-cast v1, LX/J7R;

    iput-object v4, v3, Lcom/facebook/profile/inforequest/InfoRequestFragment;->a:LX/0aG;

    iput-object v5, v3, Lcom/facebook/profile/inforequest/InfoRequestFragment;->b:LX/J7A;

    iput-object v6, v3, Lcom/facebook/profile/inforequest/InfoRequestFragment;->c:Landroid/view/inputmethod/InputMethodManager;

    iput-object v7, v3, Lcom/facebook/profile/inforequest/InfoRequestFragment;->d:Ljava/util/concurrent/Executor;

    iput-object v1, v3, Lcom/facebook/profile/inforequest/InfoRequestFragment;->e:LX/J7R;

    .line 2650534
    const v1, 0x7f03090b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 2650535
    const-string v1, "profile_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2650536
    const-string v1, "request_field_cache_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2650537
    const-string v1, "info_request_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    .line 2650538
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2650539
    const-string v1, "arg_parent_fragment_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    iput-object v0, p0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->j:Landroid/os/ParcelUuid;

    .line 2650540
    const v0, 0x7f0d1733

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2650541
    const v1, 0x7f0d1734

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->i:Landroid/widget/EditText;

    .line 2650542
    const v1, 0x7f0d1735

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 2650543
    const v1, 0x7f0d1739

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Landroid/widget/TextView;

    .line 2650544
    const v1, 0x7f0d1738

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->f:Landroid/widget/TextView;

    .line 2650545
    const v1, 0x7f0d1736

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->g:Landroid/view/View;

    .line 2650546
    const v1, 0x7f0d1737

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->h:Landroid/view/View;

    .line 2650547
    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2650548
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f083905

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650549
    :goto_0
    iget-object v0, p0, Lcom/facebook/profile/inforequest/InfoRequestFragment;->i:Landroid/widget/EditText;

    new-instance v1, LX/J75;

    invoke-direct {v1, p0, v5}, LX/J75;-><init>(Lcom/facebook/profile/inforequest/InfoRequestFragment;Landroid/widget/Button;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2650550
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083909

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2650551
    new-instance v0, LX/J76;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, LX/J76;-><init>(Lcom/facebook/profile/inforequest/InfoRequestFragment;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;Ljava/lang/String;Landroid/widget/Button;Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650552
    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2650553
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08390c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650554
    :goto_1
    const v0, -0x2cca6619

    invoke-static {v0, v8}, LX/02F;->f(II)V

    return-object v9

    .line 2650555
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f083906

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v10, v6, v11

    invoke-static {v1, v6}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2650556
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08390d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v10, v1, v2

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
