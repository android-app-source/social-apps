.class public final Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7955a36a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2488084
    const-class v0, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2488085
    const-class v0, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2488086
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2488087
    return-void
.end method

.method private a()Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2488088
    iget-object v0, p0, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;->e:Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    iput-object v0, p0, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;->e:Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    .line 2488089
    iget-object v0, p0, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;->e:Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2488090
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2488091
    invoke-direct {p0}, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;->a()Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2488092
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2488093
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2488094
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2488095
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2488096
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2488097
    invoke-direct {p0}, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;->a()Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2488098
    invoke-direct {p0}, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;->a()Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    .line 2488099
    invoke-direct {p0}, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;->a()Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2488100
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;

    .line 2488101
    iput-object v0, v1, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;->e:Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    .line 2488102
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2488103
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2488104
    new-instance v0, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;

    invoke-direct {v0}, Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationModel;-><init>()V

    .line 2488105
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2488106
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2488107
    const v0, -0x7416c92a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2488108
    const v0, -0x3f22afb3

    return v0
.end method
