.class public Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/Hd7;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Hd7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2487943
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2487944
    iput-object p2, p0, Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;->d:LX/Hd7;

    .line 2487945
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2487918
    iget-object v0, p0, Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;->d:LX/Hd7;

    const/4 v1, 0x0

    .line 2487919
    new-instance v2, LX/Hd6;

    invoke-direct {v2, v0}, LX/Hd6;-><init>(LX/Hd7;)V

    .line 2487920
    sget-object p0, LX/Hd7;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Hd5;

    .line 2487921
    if-nez p0, :cond_0

    .line 2487922
    new-instance p0, LX/Hd5;

    invoke-direct {p0}, LX/Hd5;-><init>()V

    .line 2487923
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/Hd5;->a$redex0(LX/Hd5;LX/1De;IILX/Hd6;)V

    .line 2487924
    move-object v2, p0

    .line 2487925
    move-object v1, v2

    .line 2487926
    move-object v0, v1

    .line 2487927
    iget-object v1, v0, LX/Hd5;->a:LX/Hd6;

    iput-object p2, v1, LX/Hd6;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    .line 2487928
    iget-object v1, v0, LX/Hd5;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2487929
    move-object v0, v0

    .line 2487930
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;
    .locals 5

    .prologue
    .line 2487932
    const-class v1, Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;

    monitor-enter v1

    .line 2487933
    :try_start_0
    sget-object v0, Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2487934
    sput-object v2, Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2487935
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2487936
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2487937
    new-instance p0, Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Hd7;->a(LX/0QB;)LX/Hd7;

    move-result-object v4

    check-cast v4, LX/Hd7;

    invoke-direct {p0, v3, v4}, Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;-><init>(Landroid/content/Context;LX/Hd7;)V

    .line 2487938
    move-object v0, p0

    .line 2487939
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2487940
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2487941
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2487942
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2487946
    check-cast p2, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-direct {p0, p1, p2}, Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;->a(LX/1De;Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2487931
    check-cast p2, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-direct {p0, p1, p2}, Lcom/facebook/topics/sections/follow/TopicFollowButtonPartDefinition;->a(LX/1De;Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2487917
    const/4 v0, 0x1

    return v0
.end method
