.class public Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Ljava/lang/Boolean;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2489433
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2489434
    return-void
.end method

.method private static a(LX/1De;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2489410
    const/4 v0, 0x0

    .line 2489411
    new-instance v1, LX/Hdt;

    invoke-direct {v1}, LX/Hdt;-><init>()V

    .line 2489412
    sget-object v2, LX/Hdu;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Hds;

    .line 2489413
    if-nez v2, :cond_0

    .line 2489414
    new-instance v2, LX/Hds;

    invoke-direct {v2}, LX/Hds;-><init>()V

    .line 2489415
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/Hds;->a$redex0(LX/Hds;LX/1De;IILX/Hdt;)V

    .line 2489416
    move-object v1, v2

    .line 2489417
    move-object v0, v1

    .line 2489418
    move-object v0, v0

    .line 2489419
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;
    .locals 4

    .prologue
    .line 2489421
    const-class v1, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2489422
    :try_start_0
    sget-object v0, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2489423
    sput-object v2, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2489424
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2489425
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2489426
    new-instance p0, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2489427
    move-object v0, p0

    .line 2489428
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2489429
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2489430
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2489431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2489432
    invoke-static {p1}, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2489420
    invoke-static {p1}, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2489409
    const/4 v0, 0x1

    return v0
.end method
