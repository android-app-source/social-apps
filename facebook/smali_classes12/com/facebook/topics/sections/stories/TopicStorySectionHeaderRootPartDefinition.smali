.class public Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2489436
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2489437
    iput-object p1, p0, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderRootPartDefinition;->a:Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;

    .line 2489438
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderRootPartDefinition;
    .locals 4

    .prologue
    .line 2489439
    const-class v1, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderRootPartDefinition;

    monitor-enter v1

    .line 2489440
    :try_start_0
    sget-object v0, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderRootPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2489441
    sput-object v2, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderRootPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2489442
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2489443
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2489444
    new-instance p0, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderRootPartDefinition;-><init>(Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;)V

    .line 2489445
    move-object v0, p0

    .line 2489446
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2489447
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2489448
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2489449
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2489450
    check-cast p2, Ljava/lang/Boolean;

    .line 2489451
    iget-object v0, p0, Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderRootPartDefinition;->a:Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2489452
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2489453
    const/4 v0, 0x1

    return v0
.end method
