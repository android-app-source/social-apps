.class public final Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2489105
    const-class v0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;

    new-instance v1, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2489106
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2489090
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2489092
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2489093
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2489094
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2489095
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2489096
    if-eqz v2, :cond_0

    .line 2489097
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2489098
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2489099
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2489100
    if-eqz v2, :cond_1

    .line 2489101
    const-string p0, "topic_top_stories"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2489102
    invoke-static {v1, v2, p1, p2}, LX/Hdn;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2489103
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2489104
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2489091
    check-cast p1, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$Serializer;->a(Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;LX/0nX;LX/0my;)V

    return-void
.end method
