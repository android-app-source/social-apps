.class public final Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x497184b7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2489247
    const-class v0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2489246
    const-class v0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2489244
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2489245
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2489241
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2489242
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2489243
    :cond_0
    iget-object v0, p0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2489233
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2489234
    invoke-direct {p0}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2489235
    invoke-virtual {p0}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->a()Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2489236
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2489237
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2489238
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2489239
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2489240
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2489225
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2489226
    invoke-virtual {p0}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->a()Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2489227
    invoke-virtual {p0}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->a()Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    .line 2489228
    invoke-virtual {p0}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->a()Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2489229
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;

    .line 2489230
    iput-object v0, v1, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->f:Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    .line 2489231
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2489232
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2489224
    new-instance v0, LX/Hdk;

    invoke-direct {v0, p1}, LX/Hdk;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2489222
    iget-object v0, p0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->f:Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    iput-object v0, p0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->f:Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    .line 2489223
    iget-object v0, p0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;->f:Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel$TopicTopStoriesModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2489214
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2489215
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2489221
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2489218
    new-instance v0, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;

    invoke-direct {v0}, Lcom/facebook/topics/sections/stories/FetchTopicStoriesFeedGraphQLModels$FetchTopicStoriesFeedModel;-><init>()V

    .line 2489219
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2489220
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2489217
    const v0, -0x2485c34f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2489216
    const v0, 0x711ea344

    return v0
.end method
