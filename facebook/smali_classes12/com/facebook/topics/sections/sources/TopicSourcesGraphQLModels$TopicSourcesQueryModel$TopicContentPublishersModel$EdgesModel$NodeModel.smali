.class public final Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x47ec801d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2488510
    const-class v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2488531
    const-class v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2488529
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2488530
    return-void
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2488526
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2488527
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2488528
    :cond_0
    iget-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2488524
    iget-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    .line 2488525
    iget-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2488511
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2488512
    invoke-direct {p0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->m()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2488513
    invoke-direct {p0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2488514
    invoke-virtual {p0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2488515
    invoke-virtual {p0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->l()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2488516
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2488517
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2488518
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2488519
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2488520
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2488521
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2488522
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2488523
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2488502
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2488503
    invoke-virtual {p0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->l()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2488504
    invoke-virtual {p0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->l()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    .line 2488505
    invoke-virtual {p0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->l()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2488506
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;

    .line 2488507
    iput-object v0, v1, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->i:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    .line 2488508
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2488509
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2488501
    new-instance v0, LX/HdS;

    invoke-direct {v0, p1}, LX/HdS;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2488532
    invoke-direct {p0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2488498
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2488499
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->g:Z

    .line 2488500
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2488496
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2488497
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2488495
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2488484
    new-instance v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;-><init>()V

    .line 2488485
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2488486
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2488494
    const v0, -0x268c21cb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2488493
    const v0, 0x50c72189

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2488491
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2488492
    iget-boolean v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->g:Z

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2488489
    iget-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 2488490
    iget-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2488487
    iget-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->i:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->i:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    .line 2488488
    iget-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel;->i:Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel$NodeModel$ProfilePictureModel;

    return-object v0
.end method
