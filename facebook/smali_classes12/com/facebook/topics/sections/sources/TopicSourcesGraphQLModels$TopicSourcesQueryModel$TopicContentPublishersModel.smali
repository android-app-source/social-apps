.class public final Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5986fcbd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2488602
    const-class v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2488601
    const-class v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2488599
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2488600
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2488572
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2488573
    iget v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2488592
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2488593
    invoke-virtual {p0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2488594
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2488595
    iget v1, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 2488596
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2488597
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2488598
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2488584
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2488585
    invoke-virtual {p0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2488586
    invoke-virtual {p0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2488587
    if-eqz v1, :cond_0

    .line 2488588
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;

    .line 2488589
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->f:Ljava/util/List;

    .line 2488590
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2488591
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2488581
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2488582
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->e:I

    .line 2488583
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2488578
    new-instance v0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;

    invoke-direct {v0}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;-><init>()V

    .line 2488579
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2488580
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2488577
    const v0, -0x54ac0d9b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2488576
    const v0, 0x6e2afc6a

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2488574
    iget-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->f:Ljava/util/List;

    .line 2488575
    iget-object v0, p0, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
