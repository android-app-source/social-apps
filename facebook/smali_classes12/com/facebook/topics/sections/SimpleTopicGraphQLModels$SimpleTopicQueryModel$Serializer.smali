.class public final Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2487619
    const-class v0, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    new-instance v1, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2487620
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2487621
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2487622
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2487623
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 2487624
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2487625
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 2487626
    if-eqz p0, :cond_0

    .line 2487627
    const-string p0, "__type__"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2487628
    invoke-static {v1, v0, p2, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2487629
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2487630
    if-eqz p0, :cond_1

    .line 2487631
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2487632
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2487633
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2487634
    if-eqz p0, :cond_2

    .line 2487635
    const-string p2, "name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2487636
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2487637
    :cond_2
    const/4 p0, 0x3

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 2487638
    if-eqz p0, :cond_3

    .line 2487639
    const-string p2, "topic_does_viewer_follow"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2487640
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 2487641
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2487642
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2487643
    check-cast p1, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel$Serializer;->a(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
