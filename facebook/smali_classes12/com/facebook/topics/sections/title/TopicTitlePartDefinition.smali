.class public Lcom/facebook/topics/sections/title/TopicTitlePartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:LX/Hdy;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Hdy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2489532
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2489533
    iput-object p2, p0, Lcom/facebook/topics/sections/title/TopicTitlePartDefinition;->d:LX/Hdy;

    .line 2489534
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2489535
    iget-object v0, p0, Lcom/facebook/topics/sections/title/TopicTitlePartDefinition;->d:LX/Hdy;

    const/4 v1, 0x0

    .line 2489536
    new-instance v2, LX/Hdx;

    invoke-direct {v2, v0}, LX/Hdx;-><init>(LX/Hdy;)V

    .line 2489537
    sget-object p0, LX/Hdy;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Hdw;

    .line 2489538
    if-nez p0, :cond_0

    .line 2489539
    new-instance p0, LX/Hdw;

    invoke-direct {p0}, LX/Hdw;-><init>()V

    .line 2489540
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/Hdw;->a$redex0(LX/Hdw;LX/1De;IILX/Hdx;)V

    .line 2489541
    move-object v2, p0

    .line 2489542
    move-object v1, v2

    .line 2489543
    move-object v0, v1

    .line 2489544
    iget-object v1, v0, LX/Hdw;->a:LX/Hdx;

    iput-object p2, v1, LX/Hdx;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    .line 2489545
    iget-object v1, v0, LX/Hdw;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2489546
    move-object v0, v0

    .line 2489547
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2489531
    check-cast p2, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-direct {p0, p1, p2}, Lcom/facebook/topics/sections/title/TopicTitlePartDefinition;->a(LX/1De;Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2489529
    check-cast p2, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-direct {p0, p1, p2}, Lcom/facebook/topics/sections/title/TopicTitlePartDefinition;->a(LX/1De;Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2489530
    const/4 v0, 0x1

    return v0
.end method
