.class public Lcom/facebook/topics/components/TopicSocialContextComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/GgK;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2487554
    const-class v0, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/GgK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2487555
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2487556
    iput-object p1, p0, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;->b:LX/GgK;

    .line 2487557
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/topics/components/TopicSocialContextComponentSpec;
    .locals 4

    .prologue
    .line 2487558
    const-class v1, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;

    monitor-enter v1

    .line 2487559
    :try_start_0
    sget-object v0, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2487560
    sput-object v2, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2487561
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2487562
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2487563
    new-instance p0, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;

    invoke-static {v0}, LX/GgK;->a(LX/0QB;)LX/GgK;

    move-result-object v3

    check-cast v3, LX/GgK;

    invoke-direct {p0, v3}, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;-><init>(LX/GgK;)V

    .line 2487564
    move-object v0, p0

    .line 2487565
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2487566
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/topics/components/TopicSocialContextComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2487567
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2487568
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
