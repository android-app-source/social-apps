.class public Lcom/facebook/topics/TopicFragment;
.super Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/63S;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->TOPIC_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1rq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Hdr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/NewsFeedRootGroupPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Jm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/sections/TopicSectionsRootPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Hd3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Hd2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/sections/stories/TopicStorySectionHeaderRootPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:LX/2je;

.field public n:LX/63Q;

.field public o:LX/0g8;

.field private p:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/Be9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Be9",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/1Pa;

.field private s:LX/1PY;

.field private t:LX/99d;

.field private u:LX/Hd1;

.field public v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/Hcr;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljava/lang/String;

.field public x:LX/1Cq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cq",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private y:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2487509
    invoke-direct {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;-><init>()V

    .line 2487510
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/topics/TopicFragment;->y:Z

    .line 2487511
    new-instance v0, LX/1Cq;

    invoke-direct {v0}, LX/1Cq;-><init>()V

    iput-object v0, p0, Lcom/facebook/topics/TopicFragment;->x:LX/1Cq;

    .line 2487512
    return-void
.end method

.method private m()Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;
    .locals 11

    .prologue
    .line 2487481
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2487482
    new-instance v1, LX/Hcu;

    invoke-direct {v1}, LX/Hcu;-><init>()V

    const-string v2, "topic_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2487483
    iput-object v2, v1, LX/Hcu;->b:Ljava/lang/String;

    .line 2487484
    move-object v1, v1

    .line 2487485
    const-string v2, "topic_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2487486
    iput-object v0, v1, LX/Hcu;->c:Ljava/lang/String;

    .line 2487487
    move-object v0, v1

    .line 2487488
    const/4 v7, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 2487489
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2487490
    iget-object v4, v0, LX/Hcu;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2487491
    iget-object v6, v0, LX/Hcu;->b:Ljava/lang/String;

    invoke-virtual {v3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2487492
    iget-object v8, v0, LX/Hcu;->c:Ljava/lang/String;

    invoke-virtual {v3, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2487493
    const/4 v9, 0x4

    invoke-virtual {v3, v9}, LX/186;->c(I)V

    .line 2487494
    invoke-virtual {v3, v10, v4}, LX/186;->b(II)V

    .line 2487495
    invoke-virtual {v3, v7, v6}, LX/186;->b(II)V

    .line 2487496
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 2487497
    const/4 v4, 0x3

    iget-boolean v6, v0, LX/Hcu;->d:Z

    invoke-virtual {v3, v4, v6}, LX/186;->a(IZ)V

    .line 2487498
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 2487499
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 2487500
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2487501
    invoke-virtual {v4, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2487502
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2487503
    new-instance v4, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-direct {v4, v3}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;-><init>(LX/15i;)V

    .line 2487504
    move-object v0, v4

    .line 2487505
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2487506
    const-string v0, "topic_ent_page"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2487379
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2487380
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/topics/TopicFragment;

    invoke-static {p1}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v3

    check-cast v3, LX/0zG;

    const-class v4, LX/1rq;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/1rq;

    const-class v5, LX/Hdr;

    invoke-interface {p1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Hdr;

    const/16 v6, 0x6bd

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v7, LX/1Jm;

    invoke-interface {p1, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/1Jm;

    invoke-static {p1}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v8

    check-cast v8, LX/1DS;

    const/16 v9, 0x3712

    invoke-static {p1, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    new-instance v0, LX/Hd3;

    new-instance v10, LX/Hd4;

    const/16 v11, 0x1284

    invoke-static {p1, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct {v10, v11}, LX/Hd4;-><init>(LX/0Ot;)V

    move-object v10, v10

    check-cast v10, LX/Hd4;

    new-instance v11, LX/He0;

    const/16 v12, 0x3723

    invoke-static {p1, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct {v11, v12}, LX/He0;-><init>(LX/0Ot;)V

    move-object v11, v11

    check-cast v11, LX/He0;

    new-instance v12, LX/Hd9;

    const/16 v13, 0x3716

    invoke-static {p1, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct {v12, v13}, LX/Hd9;-><init>(LX/0Ot;)V

    move-object v12, v12

    check-cast v12, LX/Hd9;

    new-instance p0, LX/Hdg;

    const/16 v13, 0x1285

    invoke-static {p1, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const-class v13, Landroid/content/Context;

    invoke-interface {p1, v13}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/Context;

    invoke-direct {p0, v1, v13}, LX/Hdg;-><init>(LX/0Ot;Landroid/content/Context;)V

    move-object v13, p0

    check-cast v13, LX/Hdg;

    invoke-direct {v0, v10, v11, v12, v13}, LX/Hd3;-><init>(LX/Hd4;LX/He0;LX/Hd9;LX/Hdg;)V

    move-object v10, v0

    check-cast v10, LX/Hd3;

    const-class v11, LX/Hd2;

    invoke-interface {p1, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/Hd2;

    invoke-static {p1}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v12

    check-cast v12, LX/1Db;

    const-class v13, LX/193;

    invoke-interface {p1, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/193;

    const/16 v0, 0x3720

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/topics/TopicFragment;->a:LX/0zG;

    iput-object v4, v2, Lcom/facebook/topics/TopicFragment;->b:LX/1rq;

    iput-object v5, v2, Lcom/facebook/topics/TopicFragment;->c:LX/Hdr;

    iput-object v6, v2, Lcom/facebook/topics/TopicFragment;->d:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/topics/TopicFragment;->e:LX/1Jm;

    iput-object v8, v2, Lcom/facebook/topics/TopicFragment;->f:LX/1DS;

    iput-object v9, v2, Lcom/facebook/topics/TopicFragment;->g:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/topics/TopicFragment;->h:LX/Hd3;

    iput-object v11, v2, Lcom/facebook/topics/TopicFragment;->i:LX/Hd2;

    iput-object v12, v2, Lcom/facebook/topics/TopicFragment;->j:LX/1Db;

    iput-object v13, v2, Lcom/facebook/topics/TopicFragment;->k:LX/193;

    iput-object p1, v2, Lcom/facebook/topics/TopicFragment;->l:LX/0Ot;

    .line 2487381
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2487507
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->o:LX/0g8;

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 2487508
    invoke-virtual {p0}, Lcom/facebook/topics/TopicFragment;->mK_()LX/63R;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    return v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2487477
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/topics/TopicFragment;->y:Z

    .line 2487478
    return-void
.end method

.method public final mJ_()V
    .locals 2

    .prologue
    .line 2487479
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->t:LX/99d;

    const v1, -0x7759f772

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2487480
    return-void
.end method

.method public final mK_()LX/63R;
    .locals 2

    .prologue
    .line 2487476
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->o:LX/0g8;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/63R;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x6b3efd2d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2487409
    const v0, 0x7f03151b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 2487410
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    .line 2487411
    const v0, 0x102000a

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2487412
    new-instance v1, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2487413
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2487414
    const-string v2, "topic_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/topics/TopicFragment;->w:Ljava/lang/String;

    .line 2487415
    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->b:LX/1rq;

    const-string v2, "topic_following_stories"

    iget-object v3, p0, Lcom/facebook/topics/TopicFragment;->w:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/topics/TopicFragment;->c:LX/Hdr;

    iget-object v4, p0, Lcom/facebook/topics/TopicFragment;->w:Ljava/lang/String;

    .line 2487416
    new-instance p1, LX/Hdq;

    invoke-static {v3}, LX/0rm;->b(LX/0QB;)LX/0rm;

    move-result-object v5

    check-cast v5, LX/0rm;

    invoke-static {v3}, LX/0w9;->a(LX/0QB;)LX/0w9;

    move-result-object v6

    check-cast v6, LX/0w9;

    invoke-static {v3}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v7

    check-cast v7, LX/0rq;

    invoke-direct {p1, v5, v6, v7, v4}, LX/Hdq;-><init>(LX/0rm;LX/0w9;LX/0rq;Ljava/lang/String;)V

    .line 2487417
    move-object v3, p1

    .line 2487418
    invoke-virtual {v1, v2, v3}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v1

    sget-object v2, LX/2jq;->CHECK_SERVER_FOR_NEWDATA:LX/2jq;

    .line 2487419
    iput-object v2, v1, LX/2jj;->i:LX/2jq;

    .line 2487420
    move-object v1, v1

    .line 2487421
    invoke-virtual {v1}, LX/2jj;->a()LX/2kW;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/topics/TopicFragment;->p:LX/2kW;

    .line 2487422
    new-instance v1, LX/Be9;

    iget-object v2, p0, Lcom/facebook/topics/TopicFragment;->p:LX/2kW;

    invoke-direct {v1, v2}, LX/Be9;-><init>(LX/2kW;)V

    iput-object v1, p0, Lcom/facebook/topics/TopicFragment;->q:LX/Be9;

    .line 2487423
    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->p:LX/2kW;

    iget-object v2, p0, Lcom/facebook/topics/TopicFragment;->q:LX/Be9;

    invoke-virtual {v1, v2}, LX/2kW;->a(LX/1vq;)V

    .line 2487424
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v1, p0, Lcom/facebook/topics/TopicFragment;->o:LX/0g8;

    .line 2487425
    new-instance v0, LX/2je;

    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->k:LX/193;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "topic_scroll_perf"

    invoke-virtual {v1, v2, v3}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2je;-><init>(LX/195;)V

    iput-object v0, p0, Lcom/facebook/topics/TopicFragment;->m:LX/2je;

    .line 2487426
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->o:LX/0g8;

    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->m:LX/2je;

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 2487427
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->o:LX/0g8;

    invoke-static {v0}, LX/1PU;->a(LX/0g8;)LX/1PY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/TopicFragment;->s:LX/1PY;

    .line 2487428
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->e:LX/1Jm;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2487429
    sget-object v3, LX/Hdp;->a:LX/Hdp;

    move-object v3, v3

    .line 2487430
    new-instance v4, Lcom/facebook/topics/TopicFragment$1;

    invoke-direct {v4, p0}, Lcom/facebook/topics/TopicFragment$1;-><init>(Lcom/facebook/topics/TopicFragment;)V

    new-instance v5, LX/3U4;

    invoke-direct {v5}, LX/3U4;-><init>()V

    iget-object v6, p0, Lcom/facebook/topics/TopicFragment;->s:LX/1PY;

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, LX/1Jm;->a(Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1Jg;LX/1PY;Ljava/lang/Runnable;)LX/1Pa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/TopicFragment;->r:LX/1Pa;

    .line 2487431
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->h:LX/Hd3;

    .line 2487432
    iget-object v1, v0, LX/Hd3;->a:LX/Hd4;

    iget-object v2, v0, LX/Hd3;->b:LX/He0;

    iget-object v3, v0, LX/Hd3;->d:LX/Hdg;

    iget-object v4, v0, LX/Hd3;->c:LX/Hd9;

    invoke-static {v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2487433
    iput-object v0, p0, Lcom/facebook/topics/TopicFragment;->v:LX/0Px;

    .line 2487434
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->x:LX/1Cq;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2487435
    iput-object v1, v0, LX/1Cq;->a:Ljava/lang/Object;

    .line 2487436
    new-instance v0, LX/99d;

    const/4 v1, 0x1

    const/4 v2, 0x3

    new-array v2, v2, [LX/1Qq;

    const/4 v3, 0x0

    .line 2487437
    iget-object v4, p0, Lcom/facebook/topics/TopicFragment;->f:LX/1DS;

    iget-object v5, p0, Lcom/facebook/topics/TopicFragment;->g:LX/0Ot;

    new-instance v6, LX/Hcy;

    iget-object v7, p0, Lcom/facebook/topics/TopicFragment;->v:LX/0Px;

    invoke-direct {v6, v7}, LX/Hcy;-><init>(LX/0Px;)V

    invoke-virtual {v4, v5, v6}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v4

    new-instance v5, LX/Hcn;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x0

    new-instance p1, LX/Hcm;

    invoke-direct {p1, p0}, LX/Hcm;-><init>(Lcom/facebook/topics/TopicFragment;)V

    invoke-direct {v5, p0, v6, v7, p1}, LX/Hcn;-><init>(Lcom/facebook/topics/TopicFragment;Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 2487438
    iput-object v5, v4, LX/1Ql;->f:LX/1PW;

    .line 2487439
    move-object v4, v4

    .line 2487440
    invoke-virtual {v4}, LX/1Ql;->e()LX/1Qq;

    move-result-object v4

    move-object v4, v4

    .line 2487441
    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 2487442
    iget-object v4, p0, Lcom/facebook/topics/TopicFragment;->f:LX/1DS;

    iget-object v5, p0, Lcom/facebook/topics/TopicFragment;->l:LX/0Ot;

    iget-object v6, p0, Lcom/facebook/topics/TopicFragment;->x:LX/1Cq;

    invoke-virtual {v4, v5, v6}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/topics/TopicFragment;->r:LX/1Pa;

    .line 2487443
    iput-object v5, v4, LX/1Ql;->f:LX/1PW;

    .line 2487444
    move-object v4, v4

    .line 2487445
    invoke-virtual {v4}, LX/1Ql;->e()LX/1Qq;

    move-result-object v4

    move-object v4, v4

    .line 2487446
    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 2487447
    iget-object v4, p0, Lcom/facebook/topics/TopicFragment;->f:LX/1DS;

    iget-object v5, p0, Lcom/facebook/topics/TopicFragment;->d:LX/0Ot;

    iget-object v6, p0, Lcom/facebook/topics/TopicFragment;->q:LX/Be9;

    invoke-virtual {v4, v5, v6}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/topics/TopicFragment;->r:LX/1Pa;

    .line 2487448
    iput-object v5, v4, LX/1Ql;->f:LX/1PW;

    .line 2487449
    move-object v4, v4

    .line 2487450
    invoke-virtual {v4}, LX/1Ql;->e()LX/1Qq;

    move-result-object v4

    .line 2487451
    move-object v4, v4

    .line 2487452
    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, LX/99d;-><init>(Z[LX/1Qq;)V

    iput-object v0, p0, Lcom/facebook/topics/TopicFragment;->t:LX/99d;

    .line 2487453
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->i:LX/Hd2;

    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->v:LX/0Px;

    invoke-virtual {v0, v1}, LX/Hd2;->a(LX/0Px;)LX/Hd1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/topics/TopicFragment;->u:LX/Hd1;

    .line 2487454
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->u:LX/Hd1;

    invoke-direct {p0}, Lcom/facebook/topics/TopicFragment;->m()Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    move-result-object v1

    .line 2487455
    const-string v2, "TopicFollowingEntPage"

    invoke-static {v2}, LX/2U5;->a(Ljava/lang/String;)LX/0v6;

    move-result-object v4

    .line 2487456
    new-instance v2, LX/Hcs;

    invoke-direct {v2}, LX/Hcs;-><init>()V

    move-object v2, v2

    .line 2487457
    const-string v3, "topic"

    invoke-virtual {v1}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2487458
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    const/4 v3, 0x1

    .line 2487459
    iput-boolean v3, v2, LX/0zO;->p:Z

    .line 2487460
    move-object v2, v2

    .line 2487461
    move-object v2, v2

    .line 2487462
    invoke-virtual {v4, v2}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2487463
    new-instance v3, LX/Hcz;

    invoke-direct {v3, v0}, LX/Hcz;-><init>(LX/Hd1;)V

    move-object v3, v3

    .line 2487464
    iget-object v5, v0, LX/Hd1;->e:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2487465
    iget-object v2, v0, LX/Hd1;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_1

    iget-object v2, v0, LX/Hd1;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Hcr;

    .line 2487466
    invoke-virtual {v2, v1}, LX/Hcr;->a(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)Landroid/util/Pair;

    move-result-object v6

    .line 2487467
    if-eqz v6, :cond_0

    .line 2487468
    iget-object v2, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, LX/0zO;

    invoke-virtual {v4, v2}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 2487469
    iget-object v2, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, LX/0TF;

    iget-object v6, v0, LX/Hd1;->e:Ljava/util/concurrent/Executor;

    invoke-static {v7, v2, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2487470
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2487471
    :cond_1
    iget-object v2, v0, LX/Hd1;->c:LX/0tX;

    invoke-virtual {v2, v4}, LX/0tX;->a(LX/0v6;)V

    .line 2487472
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->o:LX/0g8;

    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->t:LX/99d;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2487473
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->o:LX/0g8;

    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->j:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(LX/1St;)V

    .line 2487474
    iget-object v0, p0, Lcom/facebook/topics/TopicFragment;->p:LX/2kW;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2kW;->b(ILjava/lang/Object;)V

    .line 2487475
    const/4 v0, 0x2

    const/16 v1, 0x2b

    const v2, 0x5e9edab8

    invoke-static {v0, v1, v2, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v9
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x27383c5e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2487402
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroy()V

    .line 2487403
    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->q:LX/Be9;

    invoke-virtual {v1}, LX/Be9;->a()V

    .line 2487404
    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->p:LX/2kW;

    iget-object v2, p0, Lcom/facebook/topics/TopicFragment;->q:LX/Be9;

    invoke-virtual {v1, v2}, LX/2kW;->b(LX/1vq;)V

    .line 2487405
    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->p:LX/2kW;

    invoke-virtual {v1}, LX/2kW;->c()V

    .line 2487406
    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->u:LX/Hd1;

    .line 2487407
    iget-object v2, v1, LX/Hd1;->d:LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 2487408
    const/16 v1, 0x2b

    const v2, -0x46bd649a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x654652c6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2487398
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroyView()V

    .line 2487399
    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->o:LX/0g8;

    iget-object v2, p0, Lcom/facebook/topics/TopicFragment;->m:LX/2je;

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 2487400
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/topics/TopicFragment;->m:LX/2je;

    .line 2487401
    const/16 v1, 0x2b

    const v2, -0x375931b8    # -341618.25f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4b02eca6    # 8580262.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2487394
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onPause()V

    .line 2487395
    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->u:LX/Hd1;

    .line 2487396
    iget-object v2, v1, LX/Hd1;->d:LX/1My;

    invoke-virtual {v2}, LX/1My;->d()V

    .line 2487397
    const/16 v1, 0x2b

    const v2, -0x33725a76    # -7.4263632E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x15ece6fc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2487390
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onResume()V

    .line 2487391
    iget-object v1, p0, Lcom/facebook/topics/TopicFragment;->u:LX/Hd1;

    .line 2487392
    iget-object v2, v1, LX/Hd1;->d:LX/1My;

    invoke-virtual {v2}, LX/1My;->e()V

    .line 2487393
    const/16 v1, 0x2b

    const v2, -0x4a7ed23d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6d582449

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2487382
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onStart()V

    .line 2487383
    iget-boolean v1, p0, Lcom/facebook/topics/TopicFragment;->y:Z

    if-nez v1, :cond_0

    .line 2487384
    const/4 v9, 0x1

    .line 2487385
    const-class v4, LX/63U;

    invoke-virtual {p0, v4}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/63U;

    .line 2487386
    iget-object v4, p0, Lcom/facebook/topics/TopicFragment;->n:LX/63Q;

    if-nez v4, :cond_0

    if-eqz v8, :cond_0

    .line 2487387
    new-instance v4, LX/63Q;

    invoke-direct {v4}, LX/63Q;-><init>()V

    iput-object v4, p0, Lcom/facebook/topics/TopicFragment;->n:LX/63Q;

    .line 2487388
    iget-object v4, p0, Lcom/facebook/topics/TopicFragment;->n:LX/63Q;

    iget-object v5, p0, Lcom/facebook/topics/TopicFragment;->a:LX/0zG;

    invoke-interface {v5}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/63T;

    iget-object v7, p0, Lcom/facebook/topics/TopicFragment;->o:LX/0g8;

    move-object v5, p0

    move v10, v9

    invoke-virtual/range {v4 .. v10}, LX/63Q;->a(LX/63S;LX/63T;LX/0g9;LX/63U;ZZ)V

    .line 2487389
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x3880b04f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
