.class public Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static p:Ljava/lang/String;

.field public static q:Ljava/lang/String;


# instance fields
.field public r:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2564738
    const-string v0, "marketplace"

    sput-object v0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->p:Ljava/lang/String;

    .line 2564739
    const-string v0, "group"

    sput-object v0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->q:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2564775
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/1Kf;LX/0tX;LX/1Ck;LX/0W9;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 2564774
    iput-object p1, p0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->r:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object p2, p0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->s:LX/1Kf;

    iput-object p3, p0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->t:LX/0tX;

    iput-object p4, p0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->u:LX/1Ck;

    iput-object p5, p0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->v:LX/0W9;

    iput-object p6, p0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->w:Landroid/content/res/Resources;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    invoke-static {v6}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v6}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v2

    check-cast v2, LX/1Kf;

    invoke-static {v6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v6}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v6}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    invoke-static {v6}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static/range {v0 .. v6}, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->a(Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/1Kf;LX/0tX;LX/1Ck;LX/0W9;Landroid/content/res/Resources;)V

    return-void
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2564767
    const-string v0, "for_sale_profile_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2564768
    new-instance v1, LX/IJu;

    invoke-direct {v1, p0}, LX/IJu;-><init>(Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;)V

    .line 2564769
    const v2, 0x7f081445

    const/4 v3, 0x0

    invoke-static {v2, v4, v3, v4}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v2

    .line 2564770
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2564771
    new-instance v3, LX/IJv;

    invoke-direct {v3, p0, v0, v2, p0}, LX/IJv;-><init>(Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;Ljava/lang/String;Landroid/support/v4/app/DialogFragment;Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;)V

    .line 2564772
    iget-object v0, p0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->u:LX/1Ck;

    const-string v2, "QUERY_GROUP_INFORMATION_DATA_FOR_SALE_SELL_COMPOSER"

    invoke-virtual {v0, v2, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2564773
    return-void
.end method

.method private e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2564759
    const-string v0, "for_sale_profile_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2564760
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2564761
    new-instance v2, LX/IJw;

    invoke-direct {v2, p0, v0, v1}, LX/IJw;-><init>(Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;J)V

    .line 2564762
    const v0, 0x7f081445

    const/4 v1, 0x0

    invoke-static {v0, v3, v1, v3}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 2564763
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2564764
    new-instance v1, LX/IJx;

    invoke-direct {v1, p0, v0, p0}, LX/IJx;-><init>(Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;Landroid/support/v4/app/DialogFragment;Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;)V

    .line 2564765
    iget-object v0, p0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->u:LX/1Ck;

    const-string v3, "QUERY_GROUP_INFORMATION_DATA_FOR_SALE_SELL_COMPOSER"

    invoke-virtual {v0, v3, v2, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2564766
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2564747
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2564748
    invoke-static {p0, p0}, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2564749
    if-eqz p1, :cond_1

    .line 2564750
    :cond_0
    :goto_0
    return-void

    .line 2564751
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2564752
    if-eqz v0, :cond_0

    .line 2564753
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2564754
    const-string v1, "for_sale_profile_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2564755
    sget-object v2, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2564756
    invoke-direct {p0, v0}, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->d(Landroid/os/Bundle;)V

    goto :goto_0

    .line 2564757
    :cond_2
    sget-object v2, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2564758
    invoke-direct {p0, v0}, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->e(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2564740
    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2564741
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2564742
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->isPhotoContainer:Z

    if-nez v0, :cond_0

    .line 2564743
    iget-object v0, p0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->r:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2564744
    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2564745
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->finish()V

    .line 2564746
    return-void
.end method
