.class public final Lcom/facebook/cookiesync/CookieSyncer$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2JE;


# direct methods
.method public constructor <init>(LX/2JE;)V
    .locals 0

    .prologue
    .line 2521292
    iput-object p1, p0, Lcom/facebook/cookiesync/CookieSyncer$1;->a:LX/2JE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2521293
    sget-object v0, LX/2JE;->a:Ljava/lang/String;

    const-string v1, "Fetch encyrpted cookie sync user data failure."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2521294
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2521295
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2521296
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2521297
    check-cast v0, Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel;

    invoke-virtual {v0}, Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2521298
    const-string v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2521299
    const-string v1, ""

    .line 2521300
    const-string v0, ""

    .line 2521301
    array-length v3, v2

    if-lez v3, :cond_0

    .line 2521302
    const/4 v1, 0x0

    aget-object v1, v2, v1

    .line 2521303
    array-length v3, v2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 2521304
    const/4 v0, 0x1

    aget-object v0, v2, v0

    .line 2521305
    :cond_0
    iget-object v2, p0, Lcom/facebook/cookiesync/CookieSyncer$1;->a:LX/2JE;

    const/4 p1, 0x0

    .line 2521306
    const-string v4, "ATLAS"

    .line 2521307
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/2JE;->b:Ljava/util/Map;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/?a="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2521308
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    iget-object v5, v2, LX/2JE;->k:LX/0Uh;

    const/16 v6, 0x2e7

    invoke-virtual {v5, v6, p1}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2521309
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "&f="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2521310
    :cond_1
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, v3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2521311
    :try_start_0
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v6

    sget-object p0, LX/2JE;->a:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2521312
    iput-object p0, v6, LX/15E;->c:Ljava/lang/String;

    .line 2521313
    move-object v6, v6

    .line 2521314
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p0

    .line 2521315
    iput-object p0, v6, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2521316
    move-object v6, v6

    .line 2521317
    iput-object v5, v6, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2521318
    move-object v5, v6

    .line 2521319
    sget-object v6, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2521320
    iput-object v6, v5, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2521321
    move-object v5, v5

    .line 2521322
    new-instance v6, LX/Hwl;

    invoke-direct {v6, v2, v3}, LX/Hwl;-><init>(LX/2JE;Ljava/lang/String;)V

    .line 2521323
    iput-object v6, v5, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2521324
    move-object v3, v5

    .line 2521325
    invoke-virtual {v3}, LX/15E;->a()LX/15D;

    move-result-object v3

    .line 2521326
    iget-object v5, v2, LX/2JE;->f:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v5, v3}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2521327
    :goto_0
    return-void

    .line 2521328
    :catch_0
    move-exception v3

    .line 2521329
    sget-object v5, LX/2JE;->a:Ljava/lang/String;

    const-string v6, "Unable to send %s cookie sync request."

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    aput-object v4, p0, p1

    invoke-static {v5, v3, v6, p0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
