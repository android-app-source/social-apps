.class public final Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2521161
    const-class v0, Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel;

    new-instance v1, Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2521162
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2521163
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2521164
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2521165
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 2521166
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2521167
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 2521168
    if-eqz p0, :cond_0

    .line 2521169
    const-string p0, "__type__"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2521170
    invoke-static {v1, v0, p2, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2521171
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2521172
    if-eqz p0, :cond_1

    .line 2521173
    const-string p2, "encrypted_cookie_sync_uid_rtbid"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2521174
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2521175
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2521176
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2521177
    check-cast p1, Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel$Serializer;->a(Lcom/facebook/cookiesync/CookieSyncUIDRtbIDEncryptionGraphQLModels$FetchEncryptedCookieSyncUIDRtbIDDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
