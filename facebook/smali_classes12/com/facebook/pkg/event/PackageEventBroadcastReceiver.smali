.class public final Lcom/facebook/pkg/event/PackageEventBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/2QO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2643156
    const-class v0, Lcom/facebook/pkg/event/PackageEventBroadcastReceiver;

    sput-object v0, Lcom/facebook/pkg/event/PackageEventBroadcastReceiver;->c:Ljava/lang/Class;

    .line 2643157
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.instagram"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "org.internet"

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2QP;->a([Ljava/lang/Object;)LX/2QP;

    move-result-object v0

    sput-object v0, Lcom/facebook/pkg/event/PackageEventBroadcastReceiver;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2643125
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pkg/event/PackageEventBroadcastReceiver;

    invoke-static {v0}, LX/2QO;->b(LX/0QB;)LX/2QO;

    move-result-object v0

    check-cast v0, LX/2QO;

    iput-object v0, p0, Lcom/facebook/pkg/event/PackageEventBroadcastReceiver;->b:LX/2QO;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/16 v0, 0x26

    const v4, -0x76542919

    invoke-static {v3, v0, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 2643126
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2643127
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2643128
    iget-object v0, p0, Lcom/facebook/pkg/event/PackageEventBroadcastReceiver;->b:LX/2QO;

    if-nez v0, :cond_0

    .line 2643129
    invoke-static {p0, p1}, Lcom/facebook/pkg/event/PackageEventBroadcastReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2643130
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v5

    .line 2643131
    const-string v0, "com.facebook."

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/facebook/pkg/event/PackageEventBroadcastReceiver;->a:Ljava/util/Set;

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2643132
    const/16 v0, 0x27

    const v1, -0x3c86304a

    invoke-static {p2, v3, v0, v1, v4}, LX/02F;->a(Landroid/content/Intent;IIII)V

    .line 2643133
    :goto_0
    return-void

    .line 2643134
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const/4 v0, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 2643135
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 2643136
    if-eqz v0, :cond_3

    .line 2643137
    sget-object v0, Lcom/facebook/pkg/event/PackageEventBroadcastReceiver;->c:Ljava/lang/Class;

    const-string v3, "Unexpected action \"%s\""

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    invoke-static {v0, v3, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2643138
    :cond_3
    const v0, -0x798e2f5d

    invoke-static {p2, v0, v4}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 2643139
    :sswitch_0
    const-string v3, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_1

    :sswitch_1
    const-string v3, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v2

    goto :goto_1

    :sswitch_2
    const-string v7, "android.intent.action.PACKAGE_FULLY_REMOVED"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v0, v3

    goto :goto_1

    :sswitch_3
    const-string v3, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v0, 0x3

    goto :goto_1

    .line 2643140
    :pswitch_0
    const-string v0, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2643141
    if-eqz v0, :cond_4

    .line 2643142
    const v0, -0x170c7dd5

    invoke-static {p2, v0, v4}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    .line 2643143
    :cond_4
    const-string v0, "app_sibling_installed"

    .line 2643144
    :goto_2
    iget-object v1, p0, Lcom/facebook/pkg/event/PackageEventBroadcastReceiver;->b:LX/2QO;

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 2643145
    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2643146
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v3

    :goto_3
    const-string v7, "eventName must not be empty"

    invoke-static {v2, v7}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 2643147
    invoke-static {v5}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2643148
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    :goto_4
    const-string v2, "packageName must not be empty"

    invoke-static {v3, v2}, LX/03g;->a(ZLjava/lang/Object;)V

    .line 2643149
    iget-object v2, v1, LX/2QO;->i:LX/0TD;

    new-instance v3, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;

    invoke-direct {v3, v1, v0, v5}, Lcom/facebook/pkg/event/PackageEventLogger$BackgroundRunnable;-><init>(LX/2QO;Ljava/lang/String;Ljava/lang/String;)V

    const v6, 0x219450c3

    invoke-static {v2, v3, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2643150
    const v0, 0x6864d8e1

    invoke-static {p2, v0, v4}, LX/02F;->a(Landroid/content/Intent;II)V

    goto/16 :goto_0

    .line 2643151
    :pswitch_1
    const-string v0, "app_sibling_upgraded"

    goto :goto_2

    .line 2643152
    :pswitch_2
    const-string v0, "app_sibling_uninstalled"

    goto :goto_2

    .line 2643153
    :pswitch_3
    const-string v0, "app_sibling_data_cleared"

    goto :goto_2

    :cond_5
    move v2, v6

    .line 2643154
    goto :goto_3

    :cond_6
    move v3, v6

    .line 2643155
    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        -0x304ed112 -> :sswitch_1
        0xff13fb5 -> :sswitch_3
        0x5c1076e2 -> :sswitch_0
        0x5e33a4ad -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
