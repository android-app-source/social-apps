.class public Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field private final c:Landroid/content/res/Resources;

.field public final d:Ljava/lang/String;

.field public final e:LX/1Uj;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Landroid/graphics/drawable/Drawable;

.field public final i:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2629126
    const-class v0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;

    sput-object v0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->a:Ljava/lang/Class;

    .line 2629127
    const-class v0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;

    const-string v1, "lockscreen_notifications"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;LX/1Uj;LX/0Or;LX/0Or;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/android/PackageName;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Ljava/lang/String;",
            "Lcom/facebook/text/CustomFontUtil;",
            "LX/0Or",
            "<",
            "LX/0s9;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2629140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2629141
    iput-object p1, p0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->c:Landroid/content/res/Resources;

    .line 2629142
    iput-object p2, p0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->d:Ljava/lang/String;

    .line 2629143
    iput-object p3, p0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->e:LX/1Uj;

    .line 2629144
    iput-object p4, p0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->f:LX/0Or;

    .line 2629145
    iput-object p5, p0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->g:LX/0Or;

    .line 2629146
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->c:Landroid/content/res/Resources;

    const v1, 0x7f021132

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->h:Landroid/graphics/drawable/Drawable;

    .line 2629147
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->c:Landroid/content/res/Resources;

    const v1, 0x7f021136

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->i:Landroid/graphics/drawable/Drawable;

    .line 2629148
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;
    .locals 9

    .prologue
    .line 2629149
    const-class v1, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;

    monitor-enter v1

    .line 2629150
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2629151
    sput-object v2, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2629152
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2629153
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2629154
    new-instance v3, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0dF;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v0}, LX/1Uj;->a(LX/0QB;)LX/1Uj;

    move-result-object v6

    check-cast v6, LX/1Uj;

    const/16 v7, 0xb5a

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x19e

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;-><init>(Landroid/content/res/Resources;Ljava/lang/String;LX/1Uj;LX/0Or;LX/0Or;)V

    .line 2629155
    move-object v0, v3

    .line 2629156
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2629157
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2629158
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2629159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2629129
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0s9;

    invoke-interface {v0}, LX/0s9;->a()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 2629130
    const-string v0, "method/messaging.getAttachment"

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2629131
    const-string v0, "tid"

    invoke-virtual {v1, v0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2629132
    const-string v0, "hash"

    invoke-virtual {v1, v0, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2629133
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2629134
    if-eqz v0, :cond_0

    .line 2629135
    const-string v2, "access_token"

    .line 2629136
    iget-object p0, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2629137
    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2629138
    :cond_0
    const-string v0, "format"

    const-string v2, "binary"

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2629139
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2629128
    const-string v0, "https://graph.facebook.com/%s/picture?type=large"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
