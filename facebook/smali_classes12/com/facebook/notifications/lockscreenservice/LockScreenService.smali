.class public Lcom/facebook/notifications/lockscreenservice/LockScreenService;
.super LX/0te;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/0wT;

.field private static final x:LX/0wT;


# instance fields
.field public A:Landroid/view/ViewGroup;

.field public B:Landroid/view/View;

.field public C:Landroid/widget/ListView;

.field private D:Landroid/view/ViewGroup;

.field private E:Landroid/widget/TextView;

.field public F:Landroid/view/ViewGroup;

.field public G:Landroid/widget/TextView;

.field public H:Landroid/view/ViewGroup;

.field public I:LX/0wd;

.field public J:LX/0wd;

.field public K:LX/0wd;

.field public L:LX/0wd;

.field public M:F

.field public N:F

.field private O:LX/IvZ;

.field public P:Landroid/view/GestureDetector;

.field public Q:LX/6Lf;

.field private R:LX/Ivg;

.field public S:LX/DqO;

.field public final T:LX/Ivd;

.field public final U:Lcom/facebook/notifications/lockscreenservice/LockScreenService$RunnableProximitySensorTimeout;

.field private V:LX/0Yb;

.field private W:Landroid/content/BroadcastReceiver;

.field private X:LX/Iva;

.field private final Y:LX/Ivc;

.field private final Z:LX/Ive;

.field public aa:LX/1ql;

.field private ab:LX/1ql;

.field private ac:Landroid/os/CountDownTimer;

.field public ad:LX/0hs;

.field public ae:Z

.field private af:I

.field public ag:I

.field public ah:Z

.field public ai:Z

.field public aj:Z

.field private ak:Z

.field public al:Z

.field public c:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0wc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Landroid/app/KeyguardManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Landroid/view/LayoutInflater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/IvG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/3Q7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1r1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/7CY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/DqG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0pu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:Landroid/telephony/TelephonyManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Landroid/view/WindowManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/3ID;

.field public z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    .line 2628819
    const-class v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a:Ljava/lang/String;

    .line 2628820
    const-wide/high16 v0, 0x401c000000000000L    # 7.0

    invoke-static {v2, v3, v0, v1}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b:LX/0wT;

    .line 2628821
    const-wide/high16 v0, 0x4010000000000000L    # 4.0

    invoke-static {v2, v3, v0, v1}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->x:LX/0wT;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2628822
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2628823
    new-instance v0, LX/Ivd;

    invoke-direct {v0, p0}, LX/Ivd;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->T:LX/Ivd;

    .line 2628824
    new-instance v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$RunnableProximitySensorTimeout;

    invoke-direct {v0, p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService$RunnableProximitySensorTimeout;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->U:Lcom/facebook/notifications/lockscreenservice/LockScreenService$RunnableProximitySensorTimeout;

    .line 2628825
    new-instance v0, LX/Iva;

    invoke-direct {v0, p0}, LX/Iva;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->X:LX/Iva;

    .line 2628826
    new-instance v0, LX/Ivc;

    invoke-direct {v0, p0}, LX/Ivc;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Y:LX/Ivc;

    .line 2628827
    new-instance v0, LX/Ive;

    invoke-direct {v0, p0}, LX/Ive;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Z:LX/Ive;

    .line 2628828
    iput-boolean v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ae:Z

    .line 2628829
    iput v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->af:I

    .line 2628830
    iput-boolean v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ah:Z

    .line 2628831
    iput-boolean v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ak:Z

    .line 2628832
    iput-boolean v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->al:Z

    .line 2628833
    return-void
.end method

.method public static a(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/DqP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2628834
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2628835
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->l:LX/3Q7;

    const/4 v2, 0x0

    .line 2628836
    iget-object v3, v1, LX/3Q7;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0hM;->F:LX/0Tn;

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    .line 2628837
    if-nez v3, :cond_0

    if-eqz v2, :cond_0

    .line 2628838
    const-string v4, "user turned off facebook notification setting"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, LX/3Q7;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2628839
    :cond_0
    move v1, v3

    .line 2628840
    if-eqz v1, :cond_1

    .line 2628841
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->o:LX/DqG;

    invoke-virtual {v1}, LX/DqG;->a()Ljava/util/List;

    move-result-object v1

    .line 2628842
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 2628843
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2628844
    :cond_1
    new-instance v1, LX/IvR;

    invoke-direct {v1, p0}, LX/IvR;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2628845
    return-object v0
.end method

.method private a(FZ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2628846
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->q:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    int-to-float v0, v0

    .line 2628847
    if-nez p2, :cond_0

    .line 2628848
    neg-float v0, v0

    .line 2628849
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    float-to-double v2, p1

    invoke-virtual {v1, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v1

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2628850
    invoke-static {p0, v4}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->h(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V

    .line 2628851
    iput-boolean v4, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ak:Z

    .line 2628852
    return-void
.end method

.method private a(LX/DqP;)V
    .locals 7

    .prologue
    const/16 v3, 0x12

    .line 2628853
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->i:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2628854
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x350007

    const-string v2, "NotifLockscreenPermalinkLoadTime"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2628855
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    .line 2628856
    iget-object v1, v0, LX/IvG;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gh;

    const-string v2, "tap_lockscreen_notification"

    invoke-virtual {v1, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2628857
    instance-of v1, p1, LX/DqQ;

    if-eqz v1, :cond_9

    .line 2628858
    iget-object v1, v0, LX/IvG;->e:Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;

    check-cast p1, LX/DqQ;

    .line 2628859
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "lockscreen_notification_click"

    invoke-direct {v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "notifications"

    .line 2628860
    iput-object v4, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2628861
    move-object v2, v2

    .line 2628862
    const-string v4, "notification_type_clicked"

    iget-object v5, p1, LX/DqQ;->f:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2628863
    iget-object v2, p1, LX/DqQ;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_1

    .line 2628864
    const-string v2, "notification_tracking"

    iget-object v4, p1, LX/DqQ;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    invoke-virtual {v5, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2628865
    :cond_1
    iget-object v2, p1, LX/DqQ;->i:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 2628866
    const-string v2, "ndid"

    iget-object v4, p1, LX/DqQ;->i:Ljava/lang/String;

    invoke-virtual {v5, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2628867
    :cond_2
    iget-object v2, p1, LX/DqQ;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_b

    iget-object v2, p1, LX/DqQ;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2628868
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v4

    .line 2628869
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2628870
    :goto_0
    iget-object v4, v1, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->e:LX/3Cm;

    iget-object v6, v1, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->c:Landroid/content/Context;

    invoke-virtual {v4, v6, v2}, LX/3Cm;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v4

    .line 2628871
    if-nez v4, :cond_3

    .line 2628872
    iget-object v4, v1, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->j:LX/3Q0;

    iget-object v6, p1, LX/DqQ;->f:Lcom/facebook/notifications/constants/NotificationType;

    iget-object v0, p1, LX/DqQ;->h:Ljava/lang/String;

    invoke-virtual {v4, v6, v0}, LX/3Q0;->a(Lcom/facebook/notifications/constants/NotificationType;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 2628873
    :cond_3
    if-nez v4, :cond_c

    iget-object v6, p1, LX/DqQ;->e:Ljava/lang/String;

    if-eqz v6, :cond_c

    .line 2628874
    new-instance v4, LX/89k;

    invoke-direct {v4}, LX/89k;-><init>()V

    invoke-virtual {v4, v2}, LX/89k;->e(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/89k;->f(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v2

    iget-object v4, p1, LX/DqQ;->e:Ljava/lang/String;

    .line 2628875
    iput-object v4, v2, LX/89k;->b:Ljava/lang/String;

    .line 2628876
    move-object v2, v2

    .line 2628877
    invoke-virtual {v2}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v2

    .line 2628878
    iget-object v4, v1, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->g:LX/0hy;

    invoke-interface {v4, v2}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v2

    .line 2628879
    :goto_1
    if-nez v2, :cond_4

    .line 2628880
    iget-object v2, v1, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->i:LX/17Y;

    iget-object v4, v1, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->c:Landroid/content/Context;

    sget-object v6, LX/0ax;->dd:Ljava/lang/String;

    invoke-interface {v2, v4, v6}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2628881
    :cond_4
    const-string v4, "target_tab_name"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2628882
    const-string v4, "target_tab_name"

    sget-object v6, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    invoke-virtual {v6}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2628883
    :cond_5
    const-string v4, "notification_launch_source"

    const-string v6, "source_lockscreen"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2628884
    const/4 v4, 0x4

    invoke-static {v4}, LX/01m;->b(I)Z

    move-result v4

    if-nez v4, :cond_d

    .line 2628885
    :goto_2
    const/high16 v4, 0x10000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2628886
    iget-object v4, v1, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->h:Lcom/facebook/content/SecureContextHelper;

    iget-object v6, v1, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->c:Landroid/content/Context;

    invoke-interface {v4, v2, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2628887
    iget-object v2, p1, LX/DqQ;->e:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 2628888
    iget-object v2, v1, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->f:LX/1rn;

    iget-object v4, p1, LX/DqQ;->e:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/1rn;->a(Ljava/lang/String;)V

    .line 2628889
    :cond_6
    move-object v1, v5

    .line 2628890
    :goto_3
    move-object v0, v1

    .line 2628891
    if-eqz v0, :cond_7

    .line 2628892
    const-string v1, "lockscreen_notification_count"

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v2}, LX/IvG;->getCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2628893
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2628894
    :cond_7
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->d$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V

    .line 2628895
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_8

    .line 2628896
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->g()V

    .line 2628897
    :cond_8
    return-void

    .line 2628898
    :cond_9
    instance-of v1, p1, LX/Ivl;

    if-eqz v1, :cond_a

    .line 2628899
    check-cast p1, LX/Ivl;

    .line 2628900
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "lockscreen_notification_click"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "notifications"

    .line 2628901
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2628902
    move-object v1, v1

    .line 2628903
    const-string v2, "notification_type_clicked"

    const-string v4, "message"

    invoke-virtual {v1, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2628904
    :try_start_0
    iget-object v1, p1, LX/Ivl;->c:Lcom/facebook/messages/ipc/FrozenNewMessageNotification;

    .line 2628905
    iget-object v4, v1, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->g:Landroid/app/PendingIntent;

    move-object v1, v4

    .line 2628906
    invoke-virtual {v1}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2628907
    :goto_4
    move-object v1, v2

    .line 2628908
    goto :goto_3

    .line 2628909
    :cond_a
    const/4 v1, 0x0

    goto :goto_3

    .line 2628910
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_c
    move-object v2, v4

    goto/16 :goto_1

    .line 2628911
    :cond_d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2628912
    const-string v6, "Notification intent: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2628913
    invoke-virtual {v2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2628914
    const-string v6, ", type="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2628915
    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2628916
    const-string v6, ", extra="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2628917
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2628918
    const-string v6, ", notification type="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2628919
    iget-object v6, p1, LX/DqQ;->f:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 2628920
    :catch_0
    move-exception v1

    .line 2628921
    sget-object v4, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->a:Ljava/lang/Class;

    const-string v5, "Error launching message notification"

    invoke-static {v4, v5, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4
.end method

.method private a(Landroid/view/View;)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 2628922
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->q:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v1

    .line 2628923
    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    .line 2628924
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v2, v6}, Landroid/view/ViewGroup;->measure(II)V

    .line 2628925
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ag:I

    .line 2628926
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    const v2, 0x7f0d1def

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2628927
    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 2628928
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2628929
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v1, v6}, Landroid/view/ViewGroup;->measure(II)V

    .line 2628930
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ag:I

    .line 2628931
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    const v1, 0x7f0d0721

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/IvS;

    invoke-direct {v1, p0}, LX/IvS;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2628932
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    iget v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ag:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 2628933
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2628934
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2628935
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 2628936
    invoke-virtual {p1}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 2628937
    new-instance v0, LX/IvL;

    invoke-direct {v0, p0}, LX/IvL;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2628938
    return-void
.end method

.method private a(Landroid/view/ViewGroup;LX/DqP;)V
    .locals 0

    .prologue
    .line 2628939
    if-nez p1, :cond_0

    .line 2628940
    :goto_0
    return-void

    .line 2628941
    :cond_0
    invoke-direct {p0, p2}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(LX/DqP;)V

    goto :goto_0
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 6

    .prologue
    .line 2628942
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v0}, LX/IvG;->getCount()I

    move-result v0

    .line 2628943
    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0082

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2628944
    return-void
.end method

.method private a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 5

    .prologue
    .line 2628945
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->o()[Ljava/lang/String;

    move-result-object v1

    .line 2628946
    array-length v2, v1

    .line 2628947
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 2628948
    aget-object v3, v1, v0

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v1, v4

    invoke-virtual {p1, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2628949
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 2628950
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/notifications/lockscreenservice/LockScreenService;LX/0Xl;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;LX/0Zb;LX/0Sh;LX/0wc;Landroid/app/KeyguardManager;Landroid/view/LayoutInflater;LX/IvG;LX/3Q7;LX/1r1;LX/7CY;LX/DqG;LX/0pu;LX/0hB;Lcom/facebook/content/SecureContextHelper;LX/0wW;Landroid/telephony/TelephonyManager;Landroid/view/WindowManager;LX/0Ot;LX/03V;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/lockscreenservice/LockScreenService;",
            "LX/0Xl;",
            "Ljava/util/concurrent/ExecutorService;",
            "Landroid/os/Handler;",
            "LX/0Zb;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0wc;",
            "Landroid/app/KeyguardManager;",
            "Landroid/view/LayoutInflater;",
            "LX/IvG;",
            "LX/3Q7;",
            "LX/1r1;",
            "LX/7CY;",
            "LX/DqG;",
            "LX/0pu;",
            "LX/0hB;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0wW;",
            "Landroid/telephony/TelephonyManager;",
            "Landroid/view/WindowManager;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2628951
    iput-object p1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->c:LX/0Xl;

    iput-object p2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->d:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->e:Landroid/os/Handler;

    iput-object p4, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->f:LX/0Zb;

    iput-object p5, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->g:LX/0Sh;

    iput-object p6, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->h:LX/0wc;

    iput-object p7, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->i:Landroid/app/KeyguardManager;

    iput-object p8, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->j:Landroid/view/LayoutInflater;

    iput-object p9, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    iput-object p10, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->l:LX/3Q7;

    iput-object p11, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->m:LX/1r1;

    iput-object p12, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->n:LX/7CY;

    iput-object p13, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->o:LX/DqG;

    iput-object p14, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->p:LX/0pu;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->q:LX/0hB;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->r:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->s:LX/0wW;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->t:Landroid/telephony/TelephonyManager;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->u:Landroid/view/WindowManager;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->v:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->w:LX/03V;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 24

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v23

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-static/range {v23 .. v23}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static/range {v23 .. v23}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v23 .. v23}, LX/0Ss;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-static/range {v23 .. v23}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static/range {v23 .. v23}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static/range {v23 .. v23}, LX/0wc;->a(LX/0QB;)LX/0wc;

    move-result-object v8

    check-cast v8, LX/0wc;

    invoke-static/range {v23 .. v23}, LX/1sh;->a(LX/0QB;)Landroid/app/KeyguardManager;

    move-result-object v9

    check-cast v9, Landroid/app/KeyguardManager;

    invoke-static/range {v23 .. v23}, LX/1PK;->a(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v10

    check-cast v10, Landroid/view/LayoutInflater;

    invoke-static/range {v23 .. v23}, LX/IvG;->a(LX/0QB;)LX/IvG;

    move-result-object v11

    check-cast v11, LX/IvG;

    invoke-static/range {v23 .. v23}, LX/3Q7;->a(LX/0QB;)LX/3Q7;

    move-result-object v12

    check-cast v12, LX/3Q7;

    invoke-static/range {v23 .. v23}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v13

    check-cast v13, LX/1r1;

    invoke-static/range {v23 .. v23}, LX/7CY;->a(LX/0QB;)LX/7CY;

    move-result-object v14

    check-cast v14, LX/7CY;

    invoke-static/range {v23 .. v23}, LX/DqG;->a(LX/0QB;)LX/DqG;

    move-result-object v15

    check-cast v15, LX/DqG;

    invoke-static/range {v23 .. v23}, LX/0pu;->a(LX/0QB;)LX/0pu;

    move-result-object v16

    check-cast v16, LX/0pu;

    invoke-static/range {v23 .. v23}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v17

    check-cast v17, LX/0hB;

    invoke-static/range {v23 .. v23}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v18

    check-cast v18, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v23 .. v23}, LX/0wW;->a(LX/0QB;)LX/0wW;

    move-result-object v19

    check-cast v19, LX/0wW;

    invoke-static/range {v23 .. v23}, LX/0e7;->a(LX/0QB;)Landroid/telephony/TelephonyManager;

    move-result-object v20

    check-cast v20, Landroid/telephony/TelephonyManager;

    invoke-static/range {v23 .. v23}, LX/0q4;->a(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v21

    check-cast v21, Landroid/view/WindowManager;

    const/16 v22, 0xf12

    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    invoke-static/range {v23 .. v23}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v23

    check-cast v23, LX/03V;

    invoke-static/range {v2 .. v23}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(Lcom/facebook/notifications/lockscreenservice/LockScreenService;LX/0Xl;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;LX/0Zb;LX/0Sh;LX/0wc;Landroid/app/KeyguardManager;Landroid/view/LayoutInflater;LX/IvG;LX/3Q7;LX/1r1;LX/7CY;LX/DqG;LX/0pu;LX/0hB;Lcom/facebook/content/SecureContextHelper;LX/0wW;Landroid/telephony/TelephonyManager;Landroid/view/WindowManager;LX/0Ot;LX/03V;)V

    return-void
.end method

.method private a(ZZ)V
    .locals 4

    .prologue
    .line 2628952
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    if-nez v0, :cond_0

    .line 2628953
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->j:Landroid/view/LayoutInflater;

    const v1, 0x7f030c35

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    .line 2628954
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    new-instance v1, LX/IvT;

    invoke-direct {v1, p0}, LX/IvT;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    .line 2628955
    iput-object v1, v0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->h:LX/DqJ;

    .line 2628956
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    new-instance v1, LX/IvU;

    invoke-direct {v1, p0}, LX/IvU;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    .line 2628957
    iput-object v1, v0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->g:LX/DqI;

    .line 2628958
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    const v1, 0x7f0d1de7

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(Landroid/view/View;)V

    .line 2628959
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    const v1, 0x7f0d1de9

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    .line 2628960
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    const v1, 0x7f0d1dea

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->D:Landroid/view/ViewGroup;

    .line 2628961
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    const v1, 0x7f0d1de8

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->H:Landroid/view/ViewGroup;

    .line 2628962
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    const v1, 0x7f0d1a22

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->E:Landroid/widget/TextView;

    .line 2628963
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->E:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(Landroid/widget/TextView;)V

    .line 2628964
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->D:Landroid/view/ViewGroup;

    new-instance v1, LX/IvV;

    invoke-direct {v1, p0}, LX/IvV;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2628965
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    const v1, 0x7f0d1deb

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->C:Landroid/widget/ListView;

    .line 2628966
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->C:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2628967
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    new-instance v1, LX/IvW;

    invoke-direct {v1, p0}, LX/IvW;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    .line 2628968
    iput-object v1, v0, LX/IvG;->b:LX/IvW;

    .line 2628969
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->h()V

    .line 2628970
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    const v1, 0x7f0d1dec

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->G:Landroid/widget/TextView;

    .line 2628971
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->G:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081168

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2628972
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    const v1, 0x7f0d01f6

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    .line 2628973
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    new-instance v1, LX/IvH;

    invoke-direct {v1, p0}, LX/IvH;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2628974
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->X:LX/Iva;

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2628975
    invoke-direct {p0, p1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->c(Z)V

    .line 2628976
    invoke-direct {p0, p2}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b(Z)V

    .line 2628977
    if-eqz p2, :cond_1

    .line 2628978
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    new-instance v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;

    invoke-direct {v1, p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 2628979
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0a51

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 2628980
    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x41c80000    # 25.0f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 2628981
    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    new-instance v3, LX/IvJ;

    invoke-direct {v3, p0, v0, v1}, LX/IvJ;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2628982
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    new-instance v1, LX/IvK;

    invoke-direct {v1, p0}, LX/IvK;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    .line 2628983
    iput-object v1, v0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->i:LX/DqH;

    .line 2628984
    return-void
.end method

.method private static a(DD)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2628985
    cmpl-double v2, p0, v4

    if-ltz v2, :cond_0

    move v3, v0

    :goto_0
    cmpl-double v2, p2, v4

    if-ltz v2, :cond_1

    move v2, v0

    :goto_1
    if-ne v3, v2, :cond_2

    :goto_2
    return v0

    :cond_0
    move v3, v1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;F)V
    .locals 4

    .prologue
    .line 2628986
    iget-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ak:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ai:Z

    if-eqz v0, :cond_1

    .line 2628987
    :cond_0
    :goto_0
    return-void

    .line 2628988
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ai:Z

    .line 2628989
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->aj:Z

    .line 2628990
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    sub-float/2addr v0, p1

    .line 2628991
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Landroid/view/ViewGroup;I)V
    .locals 1

    .prologue
    .line 2628992
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v0, p2}, LX/IvG;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DqP;

    invoke-direct {p0, p1, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(Landroid/view/ViewGroup;LX/DqP;)V

    .line 2628993
    return-void
.end method

.method public static a$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/DqP;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2628994
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->i:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2628995
    :cond_0
    :goto_0
    return-void

    .line 2628996
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v0}, LX/IvG;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2628997
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v0, p1}, LX/IvG;->a(Ljava/util/List;)V

    .line 2628998
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->l:LX/3Q7;

    invoke-virtual {v0}, LX/3Q7;->d()Z

    move-result v0

    .line 2628999
    invoke-direct {p0, p2, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(ZZ)V

    .line 2629000
    iput v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->af:I

    goto :goto_0

    .line 2629001
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v0, p1}, LX/IvG;->a(Ljava/util/List;)V

    .line 2629002
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    if-nez v0, :cond_3

    .line 2629003
    invoke-direct {p0, p2, v1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(ZZ)V

    goto :goto_0

    .line 2629004
    :cond_3
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->E:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(Landroid/widget/TextView;)V

    .line 2629005
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->h()V

    .line 2629006
    iget-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ae:Z

    if-eqz v0, :cond_5

    .line 2629007
    if-eqz p2, :cond_4

    .line 2629008
    invoke-static {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->e(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    .line 2629009
    :cond_4
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->requestLayout()V

    goto :goto_0

    .line 2629010
    :cond_5
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->C:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 2629011
    invoke-direct {p0, p2}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->c(Z)V

    .line 2629012
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->y:LX/3ID;

    invoke-virtual {v0}, LX/3ID;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2629013
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->y:LX/3ID;

    .line 2629014
    iget-boolean v1, v0, LX/3ID;->e:Z

    move v0, v1

    .line 2629015
    if-eqz v0, :cond_6

    .line 2629016
    invoke-direct {p0, v2}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->f(F)V

    goto :goto_0

    .line 2629017
    :cond_6
    invoke-direct {p0, v2}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->e(F)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V
    .locals 3

    .prologue
    .line 2629018
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2629019
    invoke-static {}, LX/3Q7;->a()Ljava/lang/String;

    .line 2629020
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService$2;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V

    const v2, -0x75360e1a

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2629021
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2629022
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->c:LX/0Xl;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.notifications.lockscreen.ACTION_LOCKSCREEN_NOTIFICATIONS_VIEW_ATTACHED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2629023
    return-void
.end method

.method private b(FZ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2629024
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->q:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    int-to-float v0, v0

    .line 2629025
    if-nez p2, :cond_0

    .line 2629026
    neg-float v0, v0

    .line 2629027
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    float-to-double v2, p1

    invoke-virtual {v1, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v1

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2629028
    invoke-static {p0, v4}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->h(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V

    .line 2629029
    iput-boolean v4, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ak:Z

    .line 2629030
    return-void
.end method

.method private b(Z)V
    .locals 11

    .prologue
    const/high16 v2, 0x40800000    # 4.0f

    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    const-wide v4, 0x3fb999999999999aL    # 0.1

    .line 2629031
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->q:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    int-to-float v0, v0

    .line 2629032
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->q:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    int-to-float v1, v1

    .line 2629033
    div-float/2addr v0, v2

    iput v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->M:F

    .line 2629034
    div-float v0, v1, v2

    iput v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->N:F

    .line 2629035
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/Ivj;

    invoke-direct {v1, p0}, LX/Ivj;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-direct {v0, p0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->P:Landroid/view/GestureDetector;

    .line 2629036
    new-instance v0, LX/3ID;

    invoke-direct {v0}, LX/3ID;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->y:LX/3ID;

    .line 2629037
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->y:LX/3ID;

    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    .line 2629038
    iput v1, v0, LX/3ID;->a:I

    .line 2629039
    new-instance v0, LX/Ivg;

    invoke-direct {v0, p0, p0}, LX/Ivg;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->R:LX/Ivg;

    .line 2629040
    new-instance v0, LX/6Lf;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->R:LX/Ivg;

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->y:LX/3ID;

    invoke-direct {v0, v1, v2}, LX/6Lf;-><init>(LX/Ivg;LX/3ID;)V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Q:LX/6Lf;

    .line 2629041
    new-instance v0, LX/IvZ;

    invoke-direct {v0, p0}, LX/IvZ;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->O:LX/IvZ;

    .line 2629042
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->s:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 2629043
    iput-wide v4, v0, LX/0wd;->l:D

    .line 2629044
    move-object v0, v0

    .line 2629045
    iput-wide v4, v0, LX/0wd;->k:D

    .line 2629046
    move-object v0, v0

    .line 2629047
    iput-boolean v6, v0, LX/0wd;->c:Z

    .line 2629048
    move-object v0, v0

    .line 2629049
    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    .line 2629050
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->O:LX/IvZ;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2629051
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->s:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 2629052
    iput-wide v4, v0, LX/0wd;->l:D

    .line 2629053
    move-object v0, v0

    .line 2629054
    iput-wide v4, v0, LX/0wd;->k:D

    .line 2629055
    move-object v0, v0

    .line 2629056
    iput-boolean v6, v0, LX/0wd;->c:Z

    .line 2629057
    move-object v0, v0

    .line 2629058
    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->J:LX/0wd;

    .line 2629059
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->J:LX/0wd;

    new-instance v1, LX/IvX;

    invoke-direct {v1, p0}, LX/IvX;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2629060
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->s:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 2629061
    iput-wide v4, v0, LX/0wd;->l:D

    .line 2629062
    move-object v0, v0

    .line 2629063
    iput-wide v4, v0, LX/0wd;->k:D

    .line 2629064
    move-object v0, v0

    .line 2629065
    iput-boolean v6, v0, LX/0wd;->c:Z

    .line 2629066
    move-object v0, v0

    .line 2629067
    invoke-virtual {v0, v8, v9}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->K:LX/0wd;

    .line 2629068
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->K:LX/0wd;

    new-instance v1, LX/Ivb;

    invoke-direct {v1, p0}, LX/Ivb;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2629069
    if-eqz p1, :cond_0

    .line 2629070
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->s:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 2629071
    iput-wide v4, v0, LX/0wd;->l:D

    .line 2629072
    move-object v0, v0

    .line 2629073
    iput-wide v4, v0, LX/0wd;->k:D

    .line 2629074
    move-object v0, v0

    .line 2629075
    iput-boolean v6, v0, LX/0wd;->c:Z

    .line 2629076
    move-object v0, v0

    .line 2629077
    invoke-virtual {v0, v8, v9}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->L:LX/0wd;

    .line 2629078
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->L:LX/0wd;

    new-instance v1, LX/Ivi;

    invoke-direct {v1, p0}, LX/Ivi;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2629079
    :cond_0
    return-void
.end method

.method public static b$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;F)V
    .locals 4

    .prologue
    .line 2629080
    iget-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ak:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->aj:Z

    if-eqz v0, :cond_1

    .line 2629081
    :cond_0
    :goto_0
    return-void

    .line 2629082
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ai:Z

    .line 2629083
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->aj:Z

    .line 2629084
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    sub-float/2addr v0, p1

    .line 2629085
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    goto :goto_0
.end method

.method public static c(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 3

    .prologue
    .line 2629086
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    if-eqz v0, :cond_2

    .line 2629087
    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0a68

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2629088
    if-nez v0, :cond_0

    .line 2629089
    const/4 v0, -0x1

    .line 2629090
    :cond_0
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->d()I

    move-result v1

    .line 2629091
    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2629092
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2629093
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    if-eqz v0, :cond_1

    .line 2629094
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    invoke-virtual {v0, v1}, LX/DqO;->b(I)V

    .line 2629095
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->requestLayout()V

    .line 2629096
    :cond_2
    return-void
.end method

.method private c(Z)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2629097
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Y:LX/Ivc;

    invoke-virtual {v0}, LX/Ivc;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2629098
    :goto_0
    return-void

    .line 2629099
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2629100
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->H:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2629101
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->p:LX/0pu;

    invoke-virtual {v0}, LX/0pu;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2629102
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->u:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    invoke-static {v3}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->i(Z)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2629103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ae:Z

    .line 2629104
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    invoke-direct {p0, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(Landroid/view/ViewGroup;)V

    .line 2629105
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b()V

    .line 2629106
    invoke-direct {p0, v3}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->g(Z)V

    goto :goto_0

    .line 2629107
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->n:LX/7CY;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->T:LX/Ivd;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2629108
    iget-object v2, v0, LX/7CY;->e:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2629109
    iget-object v2, v0, LX/7CY;->g:Landroid/hardware/Sensor;

    if-nez v2, :cond_5

    move v3, v4

    .line 2629110
    :cond_3
    :goto_1
    move v0, v3

    .line 2629111
    if-eqz v0, :cond_4

    .line 2629112
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->U:Lcom/facebook/notifications/lockscreenservice/LockScreenService$RunnableProximitySensorTimeout;

    const-wide/16 v2, 0x3e8

    const v4, -0x6c3d9a82

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    .line 2629113
    :cond_4
    invoke-static {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->e(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    goto :goto_0

    .line 2629114
    :cond_5
    iget-object v2, v0, LX/7CY;->h:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2629115
    iget-object v2, v0, LX/7CY;->h:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-le v2, v3, :cond_6

    .line 2629116
    iget-boolean v2, v0, LX/7CY;->k:Z

    if-eqz v2, :cond_3

    .line 2629117
    invoke-static {v0}, LX/7CY;->b(LX/7CY;)Z

    move-result v2

    invoke-virtual {v1, v2}, LX/Ivd;->a(Z)V

    goto :goto_1

    .line 2629118
    :cond_6
    iget-object v2, v0, LX/7CY;->j:LX/7CX;

    if-nez v2, :cond_7

    move v2, v3

    :goto_2
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2629119
    new-instance v2, LX/7CX;

    invoke-direct {v2, v0}, LX/7CX;-><init>(LX/7CY;)V

    iput-object v2, v0, LX/7CY;->j:LX/7CX;

    .line 2629120
    iget-object v2, v0, LX/7CY;->d:Landroid/hardware/SensorManager;

    iget-object v5, v0, LX/7CY;->j:LX/7CX;

    iget-object p1, v0, LX/7CY;->g:Landroid/hardware/Sensor;

    invoke-virtual {v2, v5, p1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2629121
    iget-object v2, v0, LX/7CY;->h:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2629122
    iget-object v2, v0, LX/7CY;->j:LX/7CX;

    invoke-static {v2}, LX/7CX;->a$redex0(LX/7CX;)V

    .line 2629123
    const/4 v2, 0x0

    iput-object v2, v0, LX/7CY;->j:LX/7CX;

    move v3, v4

    .line 2629124
    goto :goto_1

    :cond_7
    move v2, v4

    .line 2629125
    goto :goto_2
.end method

.method public static c$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;F)V
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v8, 0x0

    const-wide/16 v10, 0x0

    .line 2628805
    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    invoke-virtual {v2}, LX/0wd;->d()D

    move-result-wide v2

    .line 2628806
    iget-object v4, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    invoke-virtual {v4}, LX/0wd;->d()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    .line 2628807
    cmpl-float v6, p1, v8

    if-nez v6, :cond_2

    .line 2628808
    iget v6, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->N:F

    float-to-double v6, v6

    cmpl-double v4, v4, v6

    if-lez v4, :cond_1

    .line 2628809
    cmpl-double v2, v2, v10

    if-ltz v2, :cond_0

    :goto_0
    invoke-direct {p0, v8, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(FZ)V

    .line 2628810
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2628811
    goto :goto_0

    .line 2628812
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->e(F)V

    goto :goto_1

    .line 2628813
    :cond_2
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 2628814
    float-to-double v8, p1

    invoke-static {v8, v9, v2, v3}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(DD)Z

    move-result v7

    if-eqz v7, :cond_5

    const v7, 0x455ac000    # 3500.0f

    cmpl-float v6, v6, v7

    if-gtz v6, :cond_3

    iget v6, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->N:F

    float-to-double v6, v6

    cmpl-double v4, v4, v6

    if-lez v4, :cond_5

    .line 2628815
    :cond_3
    cmpl-double v2, v2, v10

    if-ltz v2, :cond_4

    :goto_2
    invoke-direct {p0, p1, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(FZ)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    .line 2628816
    :cond_5
    invoke-direct {p0, p1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->e(F)V

    goto :goto_1
.end method

.method private d()I
    .locals 2

    .prologue
    .line 2628817
    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0a67

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2628818
    if-nez v0, :cond_0

    const/4 v0, -0x1

    :cond_0
    return v0
.end method

.method public static d$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;F)V
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v8, 0x0

    const-wide/16 v10, 0x0

    .line 2628653
    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    invoke-virtual {v2}, LX/0wd;->d()D

    move-result-wide v2

    .line 2628654
    iget-object v4, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    invoke-virtual {v4}, LX/0wd;->d()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    .line 2628655
    cmpl-float v6, p1, v8

    if-nez v6, :cond_2

    .line 2628656
    iget v6, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->M:F

    float-to-double v6, v6

    cmpl-double v4, v4, v6

    if-lez v4, :cond_1

    .line 2628657
    cmpl-double v2, v2, v10

    if-ltz v2, :cond_0

    :goto_0
    invoke-direct {p0, v8, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b(FZ)V

    .line 2628658
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2628659
    goto :goto_0

    .line 2628660
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->f(F)V

    goto :goto_1

    .line 2628661
    :cond_2
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 2628662
    float-to-double v8, p1

    invoke-static {v8, v9, v2, v3}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(DD)Z

    move-result v7

    if-eqz v7, :cond_5

    const v7, 0x455ac000    # 3500.0f

    cmpl-float v6, v6, v7

    if-gtz v6, :cond_3

    iget v6, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->M:F

    float-to-double v6, v6

    cmpl-double v4, v4, v6

    if-lez v4, :cond_5

    .line 2628663
    :cond_3
    cmpl-double v2, v2, v10

    if-ltz v2, :cond_4

    :goto_2
    invoke-direct {p0, p1, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b(FZ)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    .line 2628664
    :cond_5
    invoke-direct {p0, p1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->f(F)V

    goto :goto_1
.end method

.method public static declared-synchronized d$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V
    .locals 4

    .prologue
    .line 2628633
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->al:Z

    .line 2628634
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    if-eqz v0, :cond_0

    .line 2628635
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 2628636
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    .line 2628637
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    .line 2628638
    if-eqz p1, :cond_1

    .line 2628639
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->l()V

    .line 2628640
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ae:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    .line 2628641
    :try_start_1
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->u:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2628642
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    .line 2628643
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ae:Z

    .line 2628644
    :cond_2
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    if-eqz v0, :cond_3

    .line 2628645
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->k()LX/0wd;

    .line 2628646
    :cond_3
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Q:LX/6Lf;

    if-eqz v0, :cond_4

    .line 2628647
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Q:LX/6Lf;

    .line 2628648
    const/4 v1, 0x0

    iput-object v1, v0, LX/6Lf;->a:LX/Ivg;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2628649
    :cond_4
    monitor-exit p0

    return-void

    .line 2628650
    :catch_0
    move-exception v0

    .line 2628651
    :try_start_3
    sget-object v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a:Ljava/lang/String;

    const-string v2, "Notification View already removed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 2628652
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e(F)V
    .locals 4

    .prologue
    .line 2628631
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    float-to-double v2, p1

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2628632
    return-void
.end method

.method public static e(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2628611
    iget-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ae:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->p:LX/0pu;

    invoke-virtual {v0}, LX/0pu;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Y:LX/Ivc;

    invoke-virtual {v0}, LX/Ivc;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2628612
    :cond_1
    :goto_0
    return-void

    .line 2628613
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ae:Z

    if-eqz v0, :cond_3

    .line 2628614
    :try_start_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->u:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2628615
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->u:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    invoke-static {v3}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->i(Z)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2628616
    iput-boolean v3, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ae:Z

    .line 2628617
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    invoke-direct {p0, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(Landroid/view/ViewGroup;)V

    .line 2628618
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    if-eqz v0, :cond_4

    .line 2628619
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 2628620
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 2628621
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2628622
    :cond_4
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b()V

    .line 2628623
    const/4 v0, -0x1

    move v0, v0

    .line 2628624
    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 2628625
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "SAMSUNG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    move v0, v0

    .line 2628626
    if-nez v0, :cond_5

    .line 2628627
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ab:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->c()V

    .line 2628628
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ac:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 2628629
    :goto_2
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->m()V

    goto :goto_0

    .line 2628630
    :cond_5
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ab:LX/1ql;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v2, v3}, LX/1ql;->a(J)V

    goto :goto_2

    :catch_0
    goto :goto_1
.end method

.method public static e$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V
    .locals 1

    .prologue
    .line 2628608
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    if-eqz v0, :cond_0

    .line 2628609
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->a(Z)V

    .line 2628610
    :cond_0
    return-void
.end method

.method private f(F)V
    .locals 4

    .prologue
    .line 2628606
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->I:LX/0wd;

    float-to-double v2, p1

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2628607
    return-void
.end method

.method public static f(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 3

    .prologue
    .line 2628599
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    .line 2628600
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ab:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->d()V

    .line 2628601
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->i(Z)Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 2628602
    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->u:Landroid/view/WindowManager;

    invoke-interface {v2, v0}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 2628603
    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->u:Landroid/view/WindowManager;

    invoke-interface {v2, v0, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2628604
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->b()V

    .line 2628605
    return-void
.end method

.method public static f(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2628587
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    if-nez v0, :cond_0

    .line 2628588
    :goto_0
    return-void

    .line 2628589
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    invoke-virtual {v0, p1}, LX/D8z;->b(Z)LX/D8z;

    .line 2628590
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->l:LX/3Q7;

    .line 2628591
    const/4 v1, 0x0

    invoke-virtual {v0, v6, v1}, LX/3Q7;->a(ZLjava/lang/String;)Z

    move-result v1

    move v0, v1

    .line 2628592
    if-eqz v0, :cond_1

    .line 2628593
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2628594
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2628595
    const/4 v1, 0x3

    new-array v1, v1, [Landroid/animation/Animator;

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    const-string v3, "scaleX"

    new-array v4, v5, [F

    aput v7, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    const-string v3, "scaleY"

    new-array v4, v5, [F

    aput v7, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    const-string v4, "alpha"

    new-array v5, v5, [F

    aput v7, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 2628596
    new-instance v1, LX/IvP;

    invoke-direct {v1, p0}, LX/IvP;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2628597
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 2628598
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->stopSelf()V

    goto :goto_0
.end method

.method private g()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 2628582
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->i:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->i:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2628583
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/notifications/lockscreenservice/LockScreenDismissKeyguardActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2628584
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2628585
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->r:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2628586
    :cond_0
    return-void
.end method

.method private g(Z)V
    .locals 3

    .prologue
    .line 2628575
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "lockscreen_notification_displayed"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "notifications"

    .line 2628576
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2628577
    move-object v0, v0

    .line 2628578
    const-string v1, "lockscreen_notification_count"

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v2}, LX/IvG;->getCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "nux"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2628579
    invoke-direct {p0, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2628580
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2628581
    return-void
.end method

.method private h()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2628566
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->C:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 2628567
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v0}, LX/IvG;->getCount()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 2628568
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->C:Landroid/widget/ListView;

    invoke-virtual {v0, v3, v1, v2}, LX/IvG;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2628569
    if-eqz v0, :cond_0

    .line 2628570
    invoke-virtual {v0, v3, v3}, Landroid/view/View;->measure(II)V

    .line 2628571
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 2628572
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    mul-int/lit8 v3, v0, 0x3

    int-to-float v3, v3

    int-to-float v0, v0

    const/high16 v4, 0x3f400000    # 0.75f

    mul-float/2addr v0, v4

    add-float/2addr v0, v3

    float-to-int v0, v0

    invoke-direct {v1, v2, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2628573
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->C:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2628574
    :cond_0
    return-void
.end method

.method public static h(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V
    .locals 3

    .prologue
    .line 2628562
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "lockscreen_notification_dismiss"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "lockscreen_notification_count"

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v2}, LX/IvG;->getCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "swipe"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2628563
    invoke-direct {p0, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2628564
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2628565
    return-void
.end method

.method private static i(Z)Landroid/view/WindowManager$LayoutParams;
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 2628555
    const v4, 0x1000820

    .line 2628556
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_0

    .line 2628557
    const v4, 0xd000820

    .line 2628558
    :cond_0
    if-eqz p0, :cond_1

    .line 2628559
    const/high16 v0, 0x200000

    or-int/2addr v4, v0

    .line 2628560
    :cond_1
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7da

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 2628561
    return-object v0
.end method

.method public static i$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 4

    .prologue
    .line 2628665
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    if-eqz v0, :cond_0

    .line 2628666
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 2628667
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    .line 2628668
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->L:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2628669
    return-void
.end method

.method public static j(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const v6, 0x3f59999a    # 0.85f

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 2628670
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->p()V

    .line 2628671
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    if-nez v0, :cond_0

    .line 2628672
    new-instance v0, LX/DqO;

    invoke-direct {v0, p0}, LX/DqO;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    .line 2628673
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->H:Landroid/view/ViewGroup;

    const/4 v4, -0x1

    .line 2628674
    iget-object v3, v0, LX/D8z;->q:Landroid/view/ViewGroup;

    if-ne v3, v1, :cond_1

    .line 2628675
    :goto_0
    move-object v0, v0

    .line 2628676
    sget-object v1, LX/D8y;->HOOK_SHOT_BOTTOM:LX/D8y;

    .line 2628677
    iput-object v1, v0, LX/D8z;->p:LX/D8y;

    .line 2628678
    move-object v0, v0

    .line 2628679
    sget-object v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->x:LX/0wT;

    .line 2628680
    iget-object v3, v0, LX/D8z;->d:LX/0wd;

    invoke-virtual {v3, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 2628681
    move-object v0, v0

    .line 2628682
    new-instance v1, LX/IvM;

    invoke-direct {v1, p0}, LX/IvM;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    .line 2628683
    iput-object v1, v0, LX/D8z;->o:LX/IvM;

    .line 2628684
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    invoke-virtual {v0, v2}, LX/DqO;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2628685
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    .line 2628686
    iget-object v1, v0, LX/D8z;->c:Lcom/facebook/widget/CustomFrameLayout;

    invoke-static {v1, v2}, LX/D8z;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2628687
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    new-instance v1, LX/IvN;

    invoke-direct {v1, p0}, LX/IvN;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    .line 2628688
    iput-object v1, v0, LX/DqO;->h:Landroid/view/View$OnClickListener;

    .line 2628689
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->d()I

    move-result v1

    invoke-virtual {v0, v1}, LX/DqO;->b(I)V

    .line 2628690
    :goto_1
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    invoke-virtual {v0, v7}, LX/D8z;->b(Z)LX/D8z;

    .line 2628691
    invoke-static {p0, v5}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->e$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V

    .line 2628692
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2628693
    const/4 v1, 0x3

    new-array v1, v1, [Landroid/animation/Animator;

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    const-string v3, "scaleX"

    new-array v4, v5, [F

    aput v6, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v7

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    const-string v3, "scaleY"

    new-array v4, v5, [F

    aput v6, v4, v7

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    const-string v4, "alpha"

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v6, v5, v7

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 2628694
    new-instance v1, LX/IvO;

    invoke-direct {v1, p0}, LX/IvO;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2628695
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 2628696
    return-void

    .line 2628697
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->S:LX/DqO;

    invoke-virtual {v0}, LX/DqO;->b()V

    goto :goto_1

    .line 2628698
    :cond_1
    iget-object v3, v0, LX/D8z;->q:Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    .line 2628699
    iget-object v3, v0, LX/D8z;->q:Landroid/view/ViewGroup;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2628700
    :cond_2
    iput-object v1, v0, LX/D8z;->q:Landroid/view/ViewGroup;

    .line 2628701
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2628702
    iget-object v4, v0, LX/D8z;->q:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method public static k(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 1

    .prologue
    .line 2628703
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->f(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V

    .line 2628704
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 2628705
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService$19;

    invoke-direct {v1, p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService$19;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    const v2, 0x1097a075

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2628706
    return-void
.end method

.method private m()V
    .locals 3

    .prologue
    .line 2628707
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "lockscreen_notification_screen_on"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "notifications"

    .line 2628708
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2628709
    move-object v0, v0

    .line 2628710
    const-string v1, "lockscreen_notification_count"

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v2}, LX/IvG;->getCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2628711
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2628712
    return-void
.end method

.method public static n(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2628713
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2628714
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v0}, LX/IvG;->getCount()I

    move-result v3

    .line 2628715
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 2628716
    :try_start_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v0, v1}, LX/IvG;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DqP;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2628717
    instance-of v4, v0, LX/DqQ;

    if-eqz v4, :cond_1

    .line 2628718
    check-cast v0, LX/DqQ;

    .line 2628719
    iget-object v4, v0, LX/DqQ;->i:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 2628720
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 2628721
    const/16 v4, 0x2c

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2628722
    :cond_0
    iget-object v0, v0, LX/DqQ;->i:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2628723
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2628724
    :catch_0
    const/4 v0, 0x0

    .line 2628725
    :goto_1
    return-object v0

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private o()[Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x2c

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 2628726
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2628727
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2628728
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v0}, LX/IvG;->getCount()I

    move-result v6

    move v3, v2

    .line 2628729
    :goto_0
    if-ge v3, v6, :cond_4

    .line 2628730
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    invoke-virtual {v0, v3}, LX/IvG;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DqP;

    .line 2628731
    instance-of v7, v0, LX/DqQ;

    if-eqz v7, :cond_3

    .line 2628732
    check-cast v0, LX/DqQ;

    .line 2628733
    iget-object v7, v0, LX/DqQ;->i:Ljava/lang/String;

    if-eqz v7, :cond_1

    .line 2628734
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 2628735
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2628736
    :cond_0
    iget-object v7, v0, LX/DqQ;->i:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2628737
    :cond_1
    iget-object v7, v0, LX/DqQ;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v7, :cond_3

    iget-object v7, v0, LX/DqQ;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v7}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 2628738
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_2

    .line 2628739
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2628740
    :cond_2
    iget-object v0, v0, LX/DqQ;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2628741
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2628742
    :cond_4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_6

    .line 2628743
    new-array v0, v2, [Ljava/lang/String;

    .line 2628744
    :cond_5
    :goto_1
    return-object v0

    .line 2628745
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_7

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_7

    const/4 v0, 0x4

    :goto_2
    new-array v0, v0, [Ljava/lang/String;

    .line 2628746
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_8

    .line 2628747
    const-string v3, "ndid"

    aput-object v3, v0, v2

    .line 2628748
    const/4 v2, 0x1

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    .line 2628749
    :goto_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_5

    .line 2628750
    add-int/lit8 v2, v1, 0x1

    const-string v3, "notification_tracking"

    aput-object v3, v0, v1

    .line 2628751
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    goto :goto_1

    :cond_7
    move v0, v1

    .line 2628752
    goto :goto_2

    :cond_8
    move v1, v2

    goto :goto_3
.end method

.method private p()V
    .locals 2

    .prologue
    .line 2628753
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "lockscreen_notification_setting_click"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "notifications"

    .line 2628754
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2628755
    move-object v0, v0

    .line 2628756
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2628757
    return-void
.end method

.method public static synthetic r(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)I
    .locals 1

    .prologue
    .line 2628758
    iget v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->af:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->af:I

    return v0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2628759
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/16 v0, 0x24

    const v1, 0x7ac180ab

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2628760
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2628761
    invoke-static {p0, p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2628762
    new-instance v7, LX/0Yd;

    const-string v0, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    new-instance v1, LX/IvY;

    invoke-direct {v1, p0}, LX/IvY;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    const-string v2, "android.intent.action.USER_PRESENT"

    new-instance v3, LX/Ivk;

    invoke-direct {v3, p0}, LX/Ivk;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    const-string v4, "android.intent.action.CONFIGURATION_CHANGED"

    new-instance v5, LX/Ivh;

    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {v5, p0, v8}, LX/Ivh;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;I)V

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    invoke-direct {v7, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v7, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->W:Landroid/content/BroadcastReceiver;

    .line 2628763
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2628764
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2628765
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2628766
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2628767
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->W:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2628768
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->c:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.notifications.lockscreen.ACTION_LOCKSCREEN_NOTIFICATIONS_DISMISS"

    new-instance v2, LX/Ivf;

    invoke-direct {v2, p0}, LX/Ivf;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->V:LX/0Yb;

    .line 2628769
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->V:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2628770
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->t:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Y:LX/Ivc;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 2628771
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->p:LX/0pu;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Z:LX/Ive;

    invoke-virtual {v0, v1}, LX/0pu;->a(LX/0q0;)V

    .line 2628772
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->m:LX/1r1;

    const/16 v1, 0xa

    const-string v2, "LockscreenOnTouchWakeLock"

    invoke-virtual {v0, v1, v2}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->aa:LX/1ql;

    .line 2628773
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->m:LX/1r1;

    const/4 v1, 0x1

    const-string v2, "LockscreenScreenOnWakeLock"

    invoke-virtual {v0, v1, v2}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ab:LX/1ql;

    .line 2628774
    new-instance v0, LX/IvQ;

    .line 2628775
    const/4 v1, -0x1

    move v1, v1

    .line 2628776
    int-to-long v2, v1

    .line 2628777
    const/4 v1, -0x1

    move v1, v1

    .line 2628778
    int-to-long v4, v1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/IvQ;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;JJ)V

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ac:Landroid/os/CountDownTimer;

    .line 2628779
    const/16 v0, 0x25

    const v1, -0x9425f8a

    invoke-static {v10, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x24

    const v1, 0x93bd47f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2628780
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 2628781
    invoke-static {p0, v3}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->d$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V

    .line 2628782
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->W:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2628783
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->V:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2628784
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->t:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Y:LX/Ivc;

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 2628785
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->p:LX/0pu;

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Z:LX/Ive;

    invoke-virtual {v1, v2}, LX/0pu;->b(LX/0q0;)V

    .line 2628786
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->aa:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2628787
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->aa:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->d()V

    .line 2628788
    :cond_0
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ab:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2628789
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ab:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->d()V

    .line 2628790
    :cond_1
    const/16 v1, 0x25

    const v2, -0xa6d0aff

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/16 v3, 0x24

    const v4, 0x1ca49469

    invoke-static {v2, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 2628791
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    move v3, v1

    .line 2628792
    :goto_0
    if-eqz v3, :cond_1

    iget-object v5, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->l:LX/3Q7;

    invoke-static {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->n(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, LX/3Q7;->a(ZLjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2628793
    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->stopSelf()V

    .line 2628794
    const v0, -0x387412e6

    invoke-static {v0, v4}, LX/02F;->d(II)V

    move v1, v2

    .line 2628795
    :goto_1
    return v1

    :cond_0
    move v3, v0

    .line 2628796
    goto :goto_0

    .line 2628797
    :cond_1
    iget-object v5, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->Y:LX/Ivc;

    invoke-virtual {v5}, LX/Ivc;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2628798
    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->stopSelf()V

    .line 2628799
    const v0, -0x65e16e80

    invoke-static {v0, v4}, LX/02F;->d(II)V

    move v1, v2

    goto :goto_1

    .line 2628800
    :cond_2
    iget-boolean v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->al:Z

    if-eqz v2, :cond_3

    .line 2628801
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->l:LX/3Q7;

    const-string v2, "dismissed"

    invoke-direct {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->o()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/3Q7;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 2628802
    const v0, -0x6097c063

    invoke-static {v0, v4}, LX/02F;->d(II)V

    goto :goto_1

    .line 2628803
    :cond_3
    if-eqz v3, :cond_4

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->l:LX/3Q7;

    invoke-virtual {v2}, LX/3Q7;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    :cond_4
    invoke-static {p0, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->a$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;Z)V

    .line 2628804
    const v0, -0x7bc3dcfa

    invoke-static {v0, v4}, LX/02F;->d(II)V

    goto :goto_1
.end method
