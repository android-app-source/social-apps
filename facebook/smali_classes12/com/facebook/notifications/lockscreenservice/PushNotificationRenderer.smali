.class public Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static l:LX/0Xm;


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:Landroid/content/res/Resources;

.field public final e:LX/3Cm;

.field public final f:LX/1rn;

.field public final g:LX/0hy;

.field public final h:Lcom/facebook/content/SecureContextHelper;

.field public final i:LX/17Y;

.field public final j:LX/3Q0;

.field public final k:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2629160
    const-class v0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;

    sput-object v0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->a:Ljava/lang/Class;

    .line 2629161
    const-class v0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;

    const-string v1, "lockscreen_notifications"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;LX/3Cm;LX/1rn;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/3Q0;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2629162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2629163
    iput-object p1, p0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->c:Landroid/content/Context;

    .line 2629164
    iput-object p2, p0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->d:Landroid/content/res/Resources;

    .line 2629165
    iput-object p3, p0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->e:LX/3Cm;

    .line 2629166
    iput-object p4, p0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->f:LX/1rn;

    .line 2629167
    iput-object p5, p0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->g:LX/0hy;

    .line 2629168
    iput-object p6, p0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2629169
    iput-object p7, p0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->i:LX/17Y;

    .line 2629170
    iput-object p8, p0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->j:LX/3Q0;

    .line 2629171
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->d:Landroid/content/res/Resources;

    const v1, 0x7f021132

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->k:Landroid/graphics/drawable/Drawable;

    .line 2629172
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;
    .locals 12

    .prologue
    .line 2629173
    const-class v1, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;

    monitor-enter v1

    .line 2629174
    :try_start_0
    sget-object v0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2629175
    sput-object v2, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2629176
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2629177
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2629178
    new-instance v3, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v6

    check-cast v6, LX/3Cm;

    invoke-static {v0}, LX/1rn;->a(LX/0QB;)LX/1rn;

    move-result-object v7

    check-cast v7, LX/1rn;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v8

    check-cast v8, LX/0hy;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v10

    check-cast v10, LX/17Y;

    invoke-static {v0}, LX/3Q0;->b(LX/0QB;)LX/3Q0;

    move-result-object v11

    check-cast v11, LX/3Q0;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;-><init>(Landroid/content/Context;Landroid/content/res/Resources;LX/3Cm;LX/1rn;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/3Q0;)V

    .line 2629179
    move-object v0, v3

    .line 2629180
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2629181
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2629182
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2629183
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
