.class public final Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 0

    .prologue
    .line 2628274
    iput-object p1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 2628275
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-virtual {v1}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0a58

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-virtual {v2}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0a59

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v6, v2, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 2628276
    new-array v0, v7, [I

    .line 2628277
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getLocationOnScreen([I)V

    .line 2628278
    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 2628279
    aget v0, v0, v6

    .line 2628280
    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v2, v2, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->L:LX/0wd;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v2

    invoke-virtual {v2}, LX/0wd;->j()LX/0wd;

    .line 2628281
    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    new-instance v3, LX/0hs;

    iget-object v4, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v4, v4, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v7}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2628282
    iput-object v3, v2, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    .line 2628283
    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v2, v2, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v1, v0

    iget-object v3, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v3, v3, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->F:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {v2, v0, v6, v1, v6}, LX/0ht;->a(IIII)V

    .line 2628284
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    const/16 v1, 0x7da

    .line 2628285
    iput v1, v0, LX/0ht;->s:I

    .line 2628286
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    invoke-virtual {v0, v6}, LX/0ht;->d(Z)V

    .line 2628287
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ht;->b(F)V

    .line 2628288
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    const/4 v1, -0x1

    .line 2628289
    iput v1, v0, LX/0hs;->t:I

    .line 2628290
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    const v1, 0x7f081159

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 2628291
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2628292
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v1, v1, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->B:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->f(Landroid/view/View;)V

    .line 2628293
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    new-instance v1, LX/IvI;

    invoke-direct {v1, p0}, LX/IvI;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;)V

    invoke-virtual {v0, v1}, LX/0hs;->a(LX/5Od;)V

    .line 2628294
    iget-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenService$11;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->l:LX/3Q7;

    .line 2628295
    iget-object v1, v0, LX/3Q7;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->E:LX/0Tn;

    invoke-interface {v1, v2, v6}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2628296
    return-void
.end method
