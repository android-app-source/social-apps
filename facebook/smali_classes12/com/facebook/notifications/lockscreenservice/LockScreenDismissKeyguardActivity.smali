.class public Lcom/facebook/notifications/lockscreenservice/LockScreenDismissKeyguardActivity;
.super Landroid/app/Activity;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field public a:LX/03V;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2628119
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/notifications/lockscreenservice/LockScreenDismissKeyguardActivity;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenDismissKeyguardActivity;->a:LX/03V;

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, 0x49add759

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2628120
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 2628121
    invoke-static {p0, p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenDismissKeyguardActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2628122
    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenDismissKeyguardActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v2, 0x480000

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    .line 2628123
    const v0, 0x1020002

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenDismissKeyguardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, LX/IvF;

    invoke-direct {v2, p0}, LX/IvF;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenDismissKeyguardActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnWindowFocusChangeListener(Landroid/view/ViewTreeObserver$OnWindowFocusChangeListener;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2628124
    :goto_0
    const v0, -0x460c38ae

    invoke-static {v0, v1}, LX/02F;->c(II)V

    return-void

    .line 2628125
    :catch_0
    move-exception v0

    .line 2628126
    iget-object v2, p0, Lcom/facebook/notifications/lockscreenservice/LockScreenDismissKeyguardActivity;->a:LX/03V;

    const-class v3, Lcom/facebook/notifications/lockscreenservice/LockScreenDismissKeyguardActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Error dismissing keyguard"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2628127
    invoke-virtual {p0}, Lcom/facebook/notifications/lockscreenservice/LockScreenDismissKeyguardActivity;->finish()V

    goto :goto_0
.end method
