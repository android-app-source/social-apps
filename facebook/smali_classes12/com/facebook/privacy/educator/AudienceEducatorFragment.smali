.class public final Lcom/facebook/privacy/educator/AudienceEducatorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/2by;

.field public b:Ljava/lang/String;

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/2Oj;

.field public e:Lcom/facebook/content/SecureContextHelper;

.field public f:LX/339;

.field public g:Landroid/view/View;

.field private h:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2650307
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2650308
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    const/16 v1, 0x12cb

    invoke-static {v3, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v3}, LX/2Oj;->a(LX/0QB;)LX/2Oj;

    move-result-object v1

    check-cast v1, LX/2Oj;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v3}, LX/339;->a(LX/0QB;)LX/339;

    move-result-object v3

    check-cast v3, LX/339;

    iput-object p0, p1, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->c:LX/0Or;

    iput-object v1, p1, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->d:LX/2Oj;

    iput-object v2, p1, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object v3, p1, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/privacy/educator/AudienceEducatorFragment;LX/8Pq;)V
    .locals 4

    .prologue
    .line 2650302
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2650303
    const-string v1, "audience_educator_composer_action"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "audience_educator_privacy_type_extra"

    sget-object v3, LX/8Pr;->STICKY:LX/8Pr;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2650304
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2650305
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2650306
    return-void
.end method

.method public static a$redex0(Lcom/facebook/privacy/educator/AudienceEducatorFragment;LX/J6q;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2650262
    sget-object v0, LX/J6p;->b:[I

    invoke-virtual {p1}, LX/J6q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2650263
    :goto_0
    return-void

    .line 2650264
    :pswitch_0
    sget-object v0, LX/J6p;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    invoke-virtual {v1}, LX/2by;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 2650265
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nZ;->EXPOSED:LX/5nZ;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nZ;Ljava/lang/String;)V

    goto :goto_0

    .line 2650266
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nW;->EXPOSED:LX/5nW;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nW;Ljava/lang/String;)V

    .line 2650267
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    const/4 v1, 0x1

    .line 2650268
    iput-boolean v1, v0, LX/339;->h:Z

    .line 2650269
    goto :goto_0

    .line 2650270
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nf;->EXPOSED:LX/5nf;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nf;Ljava/lang/String;)V

    goto :goto_0

    .line 2650271
    :pswitch_4
    sget-object v0, LX/J6p;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    invoke-virtual {v1}, LX/2by;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 2650272
    :pswitch_5
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nZ;->NAVIGATED_BACK:LX/5nZ;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nZ;Ljava/lang/String;)V

    goto :goto_0

    .line 2650273
    :pswitch_6
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nW;->NAVIGATED_BACK:LX/5nW;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nW;Ljava/lang/String;)V

    .line 2650274
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    const/4 v1, 0x0

    .line 2650275
    iput-boolean v1, v0, LX/339;->h:Z

    .line 2650276
    goto :goto_0

    .line 2650277
    :pswitch_7
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nf;->NAVIGATED_BACK:LX/5nf;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nf;Ljava/lang/String;)V

    goto :goto_0

    .line 2650278
    :pswitch_8
    sget-object v0, LX/J6p;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    invoke-virtual {v1}, LX/2by;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 2650279
    :pswitch_9
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nZ;->PUBLIC_PRIVACY:LX/5nZ;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nZ;Ljava/lang/String;)V

    goto :goto_0

    .line 2650280
    :pswitch_a
    const-class v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    const-string v1, "AAA-only-me chose Public"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 2650281
    :pswitch_b
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nf;->WIDEST_PRIVACY:LX/5nf;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nf;Ljava/lang/String;)V

    goto :goto_0

    .line 2650282
    :pswitch_c
    sget-object v0, LX/J6p;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    invoke-virtual {v1}, LX/2by;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_0

    .line 2650283
    :pswitch_d
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nZ;->FRIENDS_PRIVACY:LX/5nZ;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nZ;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650284
    :pswitch_e
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nW;->FRIENDS_PRIVACY:LX/5nW;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nW;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650285
    :pswitch_f
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nf;->FRIENDS_PRIVACY:LX/5nf;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nf;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650286
    :pswitch_10
    sget-object v0, LX/J6p;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    invoke-virtual {v1}, LX/2by;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    goto/16 :goto_0

    .line 2650287
    :pswitch_11
    const-class v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    const-string v1, "AAA (public) chose Only Me"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650288
    :pswitch_12
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nW;->ONLY_ME_PRIVACY:LX/5nW;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nW;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650289
    :pswitch_13
    const-class v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    const-string v1, "NAS chose Only Me"

    invoke-static {v0, v1}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650290
    :pswitch_14
    sget-object v0, LX/J6p;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    invoke-virtual {v1}, LX/2by;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6

    goto/16 :goto_0

    .line 2650291
    :pswitch_15
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nZ;->MORE_OPTIONS:LX/5nZ;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nZ;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650292
    :pswitch_16
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nW;->MORE_OPTIONS:LX/5nW;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nW;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650293
    :pswitch_17
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nf;->MORE_OPTIONS:LX/5nf;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nf;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650294
    :pswitch_18
    sget-object v0, LX/J6p;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    invoke-virtual {v1}, LX/2by;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7

    goto/16 :goto_0

    .line 2650295
    :pswitch_19
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nZ;->DISMISSED:LX/5nZ;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nZ;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650296
    :pswitch_1a
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nW;->DISMISSED:LX/5nW;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nW;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650297
    :pswitch_1b
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nf;->SKIPPED:LX/5nf;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nf;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650298
    :pswitch_1c
    sget-object v0, LX/J6p;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    invoke-virtual {v1}, LX/2by;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_8

    goto/16 :goto_0

    .line 2650299
    :pswitch_1d
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nZ;->LEARN_MORE:LX/5nZ;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nZ;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650300
    :pswitch_1e
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nW;->LEARN_MORE:LX/5nW;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nW;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2650301
    :pswitch_1f
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    sget-object v1, LX/5nf;->LEARN_MORE:LX/5nf;

    invoke-virtual {v0, v1, p2}, LX/339;->a(LX/5nf;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_8
        :pswitch_c
        :pswitch_10
        :pswitch_14
        :pswitch_18
        :pswitch_1c
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x1
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
    .end packed-switch
.end method

.method public static e(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)Z
    .locals 2

    .prologue
    .line 2650261
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    sget-object v1, LX/2by;->NEWCOMER_AUDIENCE_EDUCATOR:LX/2by;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)Z
    .locals 2

    .prologue
    .line 2650260
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    sget-object v1, LX/2by;->AUDIENCE_ALIGNMENT_ONLY_ME_EDUCATOR:LX/2by;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2650251
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2650252
    const-class v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    invoke-static {v0, p0}, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2650253
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2650254
    const-string v1, "extra_audience_educator_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/2by;

    iput-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    .line 2650255
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    sget-object v1, LX/2by;->NONE:LX/2by;

    if-ne v0, v1, :cond_1

    .line 2650256
    :cond_0
    sget-object v0, LX/2by;->AUDIENCE_ALIGNMENT_EDUCATOR:LX/2by;

    iput-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a:LX/2by;

    .line 2650257
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2650258
    const-string v1, "audience_educator_source_extra"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->b:Ljava/lang/String;

    .line 2650259
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, -0x4e358b73

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2650207
    const v0, 0x7f030132

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 2650208
    const v0, 0x7f0d05e5

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->h:Landroid/widget/TextView;

    .line 2650209
    const v0, 0x7f0839cd

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2650210
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2650211
    const-string v1, ""

    .line 2650212
    if-eqz v0, :cond_0

    .line 2650213
    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->d:LX/2Oj;

    invoke-virtual {v1, v0}, LX/2Oj;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v0

    .line 2650214
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v1, v5

    invoke-static {v4, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2650215
    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650216
    const/16 p1, 0x8

    const/4 v5, 0x0

    .line 2650217
    const v0, 0x7f0d05ea

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;

    .line 2650218
    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->f:LX/339;

    .line 2650219
    iget-object v4, v1, LX/339;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v4

    .line 2650220
    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->k(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2650221
    new-instance v4, LX/J6i;

    invoke-direct {v4, p0}, LX/J6i;-><init>(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)V

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650222
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setText(Ljava/lang/CharSequence;)V

    .line 2650223
    invoke-virtual {v0, v5}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setVisibility(I)V

    .line 2650224
    :goto_1
    const v0, 0x7f0d05eb

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2650225
    new-instance v1, LX/J6j;

    invoke-direct {v1, p0}, LX/J6j;-><init>(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650226
    const v0, 0x7f0d05ec

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2650227
    invoke-static {p0}, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->k(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2650228
    new-instance v1, LX/J6k;

    invoke-direct {v1, p0}, LX/J6k;-><init>(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650229
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2650230
    :goto_2
    const v0, 0x7f0d05ed

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2650231
    new-instance v1, LX/J6l;

    invoke-direct {v1, p0}, LX/J6l;-><init>(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650232
    const v0, 0x7f0d05ef

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2650233
    new-instance v1, LX/J6m;

    invoke-direct {v1, p0}, LX/J6m;-><init>(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650234
    const v0, 0x7f0d05ee

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2650235
    new-instance v1, LX/J6n;

    invoke-direct {v1, p0}, LX/J6n;-><init>(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650236
    invoke-static {p0}, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->e(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2650237
    const v0, 0x7f0839bf

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2650238
    :goto_3
    const v0, 0x7f0d05e6

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2650239
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650240
    invoke-static {p0}, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->e(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0839c0

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2650241
    :goto_4
    const v0, 0x7f0d05e8

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2650242
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650243
    const v0, 0x7f0d05e7

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->g:Landroid/view/View;

    .line 2650244
    const/16 v0, 0x2b

    const v1, 0x7241c32f

    invoke-static {v6, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v3

    :cond_0
    move-object v0, v1

    goto/16 :goto_0

    .line 2650245
    :cond_1
    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/SimpleVariableTextLayoutView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2650246
    :cond_2
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 2650247
    :cond_3
    invoke-static {p0}, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->k(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2650248
    const v0, 0x7f0839b6

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_3

    .line 2650249
    :cond_4
    const v0, 0x7f0839ae

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_3

    .line 2650250
    :cond_5
    const v0, 0x7f0839af

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_4
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x19e2ef84

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2650203
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2650204
    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->g:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LX/J6o;

    invoke-direct {v2, p0}, LX/J6o;-><init>(Lcom/facebook/privacy/educator/AudienceEducatorFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2650205
    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->requestFocus()Z

    .line 2650206
    const/16 v1, 0x2b

    const v2, 0x75eb3b44

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
