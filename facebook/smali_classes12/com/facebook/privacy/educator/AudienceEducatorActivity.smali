.class public final Lcom/facebook/privacy/educator/AudienceEducatorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/privacy/educator/AudienceEducatorFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2650151
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2650138
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2650139
    const v0, 0x7f030131

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/educator/AudienceEducatorActivity;->setContentView(I)V

    .line 2650140
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    iput-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorActivity;->p:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    .line 2650141
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorActivity;->p:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    if-nez v0, :cond_0

    .line 2650142
    invoke-virtual {p0}, Lcom/facebook/privacy/educator/AudienceEducatorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2650143
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2650144
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2650145
    new-instance v2, Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    invoke-direct {v2}, Lcom/facebook/privacy/educator/AudienceEducatorFragment;-><init>()V

    .line 2650146
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2650147
    move-object v0, v2

    .line 2650148
    check-cast v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    iput-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorActivity;->p:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    .line 2650149
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/privacy/educator/AudienceEducatorActivity;->p:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2650150
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2650129
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorActivity;->p:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    .line 2650130
    sget-object v1, LX/J6q;->NAVIGATED_BACK:LX/J6q;

    iget-object v2, v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a$redex0(Lcom/facebook/privacy/educator/AudienceEducatorFragment;LX/J6q;Ljava/lang/String;)V

    .line 2650131
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2650132
    return-void
.end method

.method public final onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    .line 2650133
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onWindowFocusChanged(Z)V

    .line 2650134
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorActivity;->p:Lcom/facebook/privacy/educator/AudienceEducatorFragment;

    .line 2650135
    if-eqz p1, :cond_0

    .line 2650136
    sget-object v1, LX/J6q;->EXPOSED:LX/J6q;

    iget-object p0, v0, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->b:Ljava/lang/String;

    invoke-static {v0, v1, p0}, Lcom/facebook/privacy/educator/AudienceEducatorFragment;->a$redex0(Lcom/facebook/privacy/educator/AudienceEducatorFragment;LX/J6q;Ljava/lang/String;)V

    .line 2650137
    :cond_0
    return-void
.end method
