.class public final Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2650309
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2650310
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2650311
    const v0, 0x7f030133

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreActivity;->setContentView(I)V

    .line 2650312
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;

    .line 2650313
    if-nez v0, :cond_0

    .line 2650314
    invoke-virtual {p0}, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2650315
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2650316
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2650317
    new-instance v2, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;

    invoke-direct {v2}, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;-><init>()V

    .line 2650318
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2650319
    move-object v0, v2

    .line 2650320
    check-cast v0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;

    .line 2650321
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2650322
    :cond_0
    return-void
.end method
