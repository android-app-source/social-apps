.class public Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/2Oj;

.field public n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/Hrg;

.field public p:LX/Hrh;

.field public q:Landroid/view/View;

.field public r:Lcom/facebook/resources/ui/FbTextView;

.field public s:Lcom/facebook/resources/ui/EllipsizingTextView;

.field public t:Lcom/facebook/resources/ui/EllipsizingTextView;

.field public u:Lcom/facebook/resources/ui/EllipsizingTextView;

.field public v:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2650441
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2650442
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x17ad74b8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2650449
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2650450
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;

    invoke-static {p1}, LX/2Oj;->a(LX/0QB;)LX/2Oj;

    move-result-object v4

    check-cast v4, LX/2Oj;

    const/16 v1, 0x12cb

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    iput-object v4, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->m:LX/2Oj;

    iput-object p1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->n:LX/0Or;

    .line 2650451
    const/16 v1, 0x2b

    const v2, 0x3af1a2a4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x333a1ff5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2650452
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->o:LX/Hrg;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2650453
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->p:LX/Hrh;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2650454
    const v1, 0x7f030913

    const/4 v2, 0x1

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->q:Landroid/view/View;

    .line 2650455
    iget-object v2, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->m:LX/2Oj;

    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->n:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v2, v1}, LX/2Oj;->a(Lcom/facebook/user/model/User;)Ljava/lang/String;

    move-result-object v1

    .line 2650456
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v1, 0x7f0839ca

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 2650457
    :goto_0
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->q:Landroid/view/View;

    const v3, 0x7f0d1744

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 2650458
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650459
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->q:Landroid/view/View;

    const v2, 0x7f0d1746    # 1.87542E38f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/EllipsizingTextView;

    iput-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->s:Lcom/facebook/resources/ui/EllipsizingTextView;

    .line 2650460
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->s:Lcom/facebook/resources/ui/EllipsizingTextView;

    iget-object v2, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->o:LX/Hrg;

    .line 2650461
    iget-object v3, v2, LX/Hrg;->a:LX/Hrk;

    iget-object v3, v3, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    iget-object v3, v3, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mFirstSurveyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v3

    .line 2650462
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650463
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->s:Lcom/facebook/resources/ui/EllipsizingTextView;

    new-instance v2, LX/J6w;

    invoke-direct {v2, p0}, LX/J6w;-><init>(Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/EllipsizingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650464
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->q:Landroid/view/View;

    const v2, 0x7f0d1747

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/EllipsizingTextView;

    iput-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->t:Lcom/facebook/resources/ui/EllipsizingTextView;

    .line 2650465
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->t:Lcom/facebook/resources/ui/EllipsizingTextView;

    iget-object v2, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->o:LX/Hrg;

    .line 2650466
    iget-object v3, v2, LX/Hrg;->a:LX/Hrk;

    iget-object v3, v3, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    iget-object v3, v3, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mSecondSurveyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v3

    .line 2650467
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/EllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650468
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->t:Lcom/facebook/resources/ui/EllipsizingTextView;

    new-instance v2, LX/J6x;

    invoke-direct {v2, p0}, LX/J6x;-><init>(Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/EllipsizingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650469
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->q:Landroid/view/View;

    const v2, 0x7f0d1748

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/EllipsizingTextView;

    iput-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->u:Lcom/facebook/resources/ui/EllipsizingTextView;

    .line 2650470
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->u:Lcom/facebook/resources/ui/EllipsizingTextView;

    new-instance v2, LX/J6y;

    invoke-direct {v2, p0}, LX/J6y;-><init>(Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/EllipsizingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650471
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->q:Landroid/view/View;

    const v2, 0x7f0d1749

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->v:Lcom/facebook/resources/ui/FbTextView;

    .line 2650472
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->v:Lcom/facebook/resources/ui/FbTextView;

    new-instance v2, LX/J6z;

    invoke-direct {v2, p0}, LX/J6z;-><init>(Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650473
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->q:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x7c2dc2a7

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1

    .line 2650474
    :cond_0
    const v2, 0x7f0839c9

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v1, v3, p1

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x7df67bfa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2650443
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->s:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/EllipsizingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650444
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->t:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/EllipsizingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650445
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->u:Lcom/facebook/resources/ui/EllipsizingTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/EllipsizingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650446
    iget-object v1, p0, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650447
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 2650448
    const/16 v1, 0x2b

    const v2, 0x3edd1c91

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
