.class public final Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;

.field public b:LX/2by;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2650341
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2650342
    return-void
.end method

.method public static a(Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;IIILjava/lang/Object;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/16 v4, 0x21

    .line 2650343
    new-instance v0, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0, v1, v4}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/47x;->a(I)LX/47x;

    move-result-object v0

    invoke-virtual {v0}, LX/47x;->a()LX/47x;

    move-result-object v0

    .line 2650344
    new-instance v1, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v1, p2}, LX/47x;->a(I)LX/47x;

    move-result-object v1

    const-string v2, "%1$s"

    invoke-virtual {p0, p3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p4, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v1

    .line 2650345
    const-string v2, "\n\n"

    invoke-virtual {v0, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->a:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2650346
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2650347
    const-class v0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;

    invoke-static {v0, p0}, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2650348
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2650349
    const-string v1, "extra_audience_educator_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/2by;

    iput-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->b:LX/2by;

    .line 2650350
    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->b:LX/2by;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->b:LX/2by;

    sget-object v1, LX/2by;->NONE:LX/2by;

    if-ne v0, v1, :cond_1

    .line 2650351
    :cond_0
    sget-object v0, LX/2by;->AUDIENCE_ALIGNMENT_EDUCATOR:LX/2by;

    iput-object v0, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->b:LX/2by;

    .line 2650352
    :cond_1
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4ebd4dbe

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2650353
    const v1, 0x7f030134

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->c:Landroid/view/View;

    .line 2650354
    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->c:Landroid/view/View;

    const v2, 0x7f0d05f0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->d:Landroid/view/View;

    .line 2650355
    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->d:Landroid/view/View;

    new-instance v2, LX/J6r;

    invoke-direct {v2, p0}, LX/J6r;-><init>(Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650356
    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->c:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x621b4982    # -6.053979E-21f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7eb3ef92

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2650357
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2650358
    iget-object v1, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->c:Landroid/view/View;

    .line 2650359
    const v2, 0x7f0d05f1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2650360
    sget-object v4, LX/J6t;->a:[I

    iget-object v5, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->b:LX/2by;

    invoke-virtual {v5}, LX/2by;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2650361
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x2b71d8c5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2650362
    :pswitch_0
    const v4, 0x7f0839b0

    const v5, 0x7f0839b1

    .line 2650363
    new-instance v6, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v6, v7}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2650364
    new-instance v7, Landroid/text/style/StyleSpan;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v8, 0x21

    invoke-virtual {v6, v7, v8}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/47x;->a(I)LX/47x;

    move-result-object v6

    invoke-virtual {v6}, LX/47x;->a()LX/47x;

    move-result-object v6

    const-string v7, "\n\n"

    invoke-virtual {v6, v7}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/47x;->a(I)LX/47x;

    move-result-object v6

    invoke-virtual {v6}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v6

    move-object v4, v6

    .line 2650365
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650366
    :goto_1
    sget-object v2, LX/J6t;->a:[I

    iget-object v4, p0, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->b:LX/2by;

    invoke-virtual {v4}, LX/2by;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_1

    .line 2650367
    :goto_2
    goto :goto_0

    .line 2650368
    :pswitch_1
    const/4 v9, 0x1

    .line 2650369
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2650370
    const v5, 0x7f0839c1

    const v6, 0x7f0839c2

    const v7, 0x7f0839c1

    new-instance v8, Landroid/text/style/StyleSpan;

    invoke-direct {v8, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-static {p0, v5, v6, v7, v8}, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->a(Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;IIILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2650371
    const-string v5, "\n\n\n"

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2650372
    const v5, 0x7f0839c3

    const v6, 0x7f0839c4

    const v7, 0x7f0839c3

    new-instance v8, Landroid/text/style/StyleSpan;

    invoke-direct {v8, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-static {p0, v5, v6, v7, v8}, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->a(Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;IIILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2650373
    const-string v5, "\n\n\n"

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2650374
    const v5, 0x7f0839c5

    const v6, 0x7f0839c6

    const v7, 0x7f0839c7

    new-instance v8, Landroid/text/style/StyleSpan;

    invoke-direct {v8, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-static {p0, v5, v6, v7, v8}, Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;->a(Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;IIILjava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2650375
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650376
    goto :goto_1

    .line 2650377
    :pswitch_2
    const v2, 0x7f0839b2

    move v4, v2

    .line 2650378
    :goto_3
    const v2, 0x7f0d05f2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2650379
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2650380
    new-instance v5, LX/J6s;

    invoke-direct {v5, p0}, LX/J6s;-><init>(Lcom/facebook/privacy/educator/AudienceEducatorLearnMoreFragment;)V

    .line 2650381
    new-instance v6, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v6, v7}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v6, v4}, LX/47x;->a(I)LX/47x;

    move-result-object v4

    const-string v6, "%1$s"

    const v7, 0x7f08130a

    invoke-virtual {p0, v7}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x21

    invoke-virtual {v4, v6, v7, v5, v8}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v4

    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v4

    .line 2650382
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2650383
    :pswitch_3
    const v2, 0x7f0839ba

    move v4, v2

    .line 2650384
    goto :goto_3

    .line 2650385
    :pswitch_4
    const v2, 0x7f0839c8

    move v4, v2

    .line 2650386
    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
