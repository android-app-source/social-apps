.class public final Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/J5E;
.implements LX/J5D;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x765154c0
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2646979
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2646978
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2646976
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2646977
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2646958
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2646959
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2646968
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2646969
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2646970
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2646971
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2646972
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2646973
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2646974
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2646975
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2646980
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2646981
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2646982
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2646983
    if-eqz v1, :cond_2

    .line 2646984
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    .line 2646985
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2646986
    :goto_0
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2646987
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2646988
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2646989
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    .line 2646990
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2646991
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2646992
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final synthetic a()LX/0us;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2646967
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2646965
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel$NodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->e:Ljava/util/List;

    .line 2646966
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2646962
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;-><init>()V

    .line 2646963
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2646964
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2646961
    const v0, 0x41bcad7a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2646960
    const v0, 0x1a8a244c

    return v0
.end method
