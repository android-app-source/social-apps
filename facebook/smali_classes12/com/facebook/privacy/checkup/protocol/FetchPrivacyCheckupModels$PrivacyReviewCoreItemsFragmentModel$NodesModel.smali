.class public final Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5f1f052a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2647553
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2647552
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2647550
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2647551
    return-void
.end method

.method private j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSectionData"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2647548
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    .line 2647549
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2647538
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2647539
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2647540
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0xf5a3298

    invoke-static {v2, v1, v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2647541
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->c()Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2647542
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2647543
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2647544
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2647545
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2647546
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2647547
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2647523
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2647524
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2647525
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    .line 2647526
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2647527
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;

    .line 2647528
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    .line 2647529
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2647530
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0xf5a3298

    invoke-static {v2, v0, v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2647531
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2647532
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;

    .line 2647533
    iput v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->f:I

    move-object v1, v0

    .line 2647534
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2647535
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2647536
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2647537
    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSectionData"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2647522
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreSectionDataFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2647519
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2647520
    const/4 v0, 0x1

    const v1, -0xf5a3298

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->f:I

    .line 2647521
    return-void
.end method

.method public final b()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSectionHeader"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2647510
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2647511
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2647516
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;-><init>()V

    .line 2647517
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2647518
    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2647514
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->g:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->g:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    .line 2647515
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyReviewCoreItemsFragmentModel$NodesModel;->g:Lcom/facebook/graphql/enums/GraphQLPrivacyReviewCoreSectionType;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2647513
    const v0, -0x77019cc0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2647512
    const v0, 0x4c185146    # 3.9929112E7f

    return v0
.end method
