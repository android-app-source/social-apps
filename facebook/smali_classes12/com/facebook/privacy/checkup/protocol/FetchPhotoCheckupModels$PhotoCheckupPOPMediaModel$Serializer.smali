.class public final Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2645413
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;

    new-instance v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2645414
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2645415
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2645416
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2645417
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/J54;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2645418
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2645419
    check-cast p1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$Serializer;->a(Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;LX/0nX;LX/0my;)V

    return-void
.end method
