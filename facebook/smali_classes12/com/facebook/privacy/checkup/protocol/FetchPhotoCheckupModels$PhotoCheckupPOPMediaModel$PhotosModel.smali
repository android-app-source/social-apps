.class public final Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4a3f9cb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2645412
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2645411
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2645409
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2645410
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2645401
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2645402
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2645403
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2645404
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2645405
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2645406
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2645407
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2645408
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2645399
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->e:Ljava/util/List;

    .line 2645400
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2645379
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2645380
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2645381
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2645382
    if-eqz v1, :cond_2

    .line 2645383
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    .line 2645384
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2645385
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2645386
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    .line 2645387
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2645388
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    .line 2645389
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->f:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    .line 2645390
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2645391
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2645396
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;-><init>()V

    .line 2645397
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2645398
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2645395
    const v0, 0x76a80be

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2645394
    const v0, -0xbd8e0f7

    return v0
.end method

.method public final j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645392
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->f:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->f:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    .line 2645393
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->f:Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    return-object v0
.end method
