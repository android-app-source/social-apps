.class public final Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2fe49b05
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2646398
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2646397
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2646395
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2646396
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2646389
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2646390
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2646391
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2646392
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2646393
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2646394
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2646381
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2646382
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2646383
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    .line 2646384
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2646385
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    .line 2646386
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    .line 2646387
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2646388
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getReviewSteps"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2646374
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    .line 2646375
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel$ReviewStepsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2646378
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;-><init>()V

    .line 2646379
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2646380
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2646377
    const v0, -0x21eb9146

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2646376
    const v0, 0x5e7a249f

    return v0
.end method
