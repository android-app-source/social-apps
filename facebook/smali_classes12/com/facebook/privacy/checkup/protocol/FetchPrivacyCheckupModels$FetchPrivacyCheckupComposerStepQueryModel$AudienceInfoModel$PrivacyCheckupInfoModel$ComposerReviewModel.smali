.class public final Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x258f3e3f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2646766
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2646769
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2646767
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2646768
    return-void
.end method

.method private j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCheckupItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2646764
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    .line 2646765
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2646755
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2646756
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->c(Ljava/util/List;)I

    move-result v0

    .line 2646757
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2646758
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2646759
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2646760
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2646761
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2646762
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2646763
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2646747
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2646748
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2646749
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    .line 2646750
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2646751
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;

    .line 2646752
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    .line 2646753
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2646754
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()LX/J5E;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCheckupItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2646770
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemsFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2646744
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2646745
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->g:Z

    .line 2646746
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2646742
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->e:Ljava/util/List;

    .line 2646743
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2646739
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;-><init>()V

    .line 2646740
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2646741
    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2646735
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2646736
    iget-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2646738
    const v0, 0x6c8e4fca

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2646737
    const v0, 0x7d141693

    return v0
.end method
