.class public final Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x29dc59a7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2645441
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2645444
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2645442
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2645443
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2645427
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2645428
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2645429
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2645430
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2645431
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2645432
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2645433
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2645434
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2645435
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    .line 2645436
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2645437
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;

    .line 2645438
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    .line 2645439
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2645440
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645425
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    .line 2645426
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2645422
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel;-><init>()V

    .line 2645423
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2645424
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2645420
    const v0, 0xf84d43c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2645421
    const v0, 0x223c5b7f

    return v0
.end method
