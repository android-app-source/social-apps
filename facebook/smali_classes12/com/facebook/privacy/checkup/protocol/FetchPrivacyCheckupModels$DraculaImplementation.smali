.class public final Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2646131
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2646132
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2646129
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2646130
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2646094
    if-nez p1, :cond_0

    .line 2646095
    :goto_0
    return v0

    .line 2646096
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2646097
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2646098
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2646099
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2646100
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2646101
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2646102
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2646103
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2646104
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2646105
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2646106
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2646107
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2646108
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2646109
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2646110
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2646111
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2646112
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2646113
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2646114
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2646115
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2646116
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2646117
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2646118
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2646119
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2646120
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2646121
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2646122
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2646123
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2646124
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2646125
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2646126
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2646127
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 2646128
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4e710561 -> :sswitch_4
        -0xf5a3298 -> :sswitch_3
        0x2c67e2a4 -> :sswitch_0
        0x5a618a60 -> :sswitch_2
        0x729fad3b -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2646093
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2646090
    sparse-switch p0, :sswitch_data_0

    .line 2646091
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2646092
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x4e710561 -> :sswitch_0
        -0xf5a3298 -> :sswitch_0
        0x2c67e2a4 -> :sswitch_0
        0x5a618a60 -> :sswitch_0
        0x729fad3b -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2646089
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2646087
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;->b(I)V

    .line 2646088
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2646133
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2646134
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2646135
    :cond_0
    iput-object p1, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;->a:LX/15i;

    .line 2646136
    iput p2, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;->b:I

    .line 2646137
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2646086
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2646085
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2646082
    iget v0, p0, LX/1vt;->c:I

    .line 2646083
    move v0, v0

    .line 2646084
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2646079
    iget v0, p0, LX/1vt;->c:I

    .line 2646080
    move v0, v0

    .line 2646081
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2646061
    iget v0, p0, LX/1vt;->b:I

    .line 2646062
    move v0, v0

    .line 2646063
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2646076
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2646077
    move-object v0, v0

    .line 2646078
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2646067
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2646068
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2646069
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2646070
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2646071
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2646072
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2646073
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2646074
    invoke-static {v3, v9, v2}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2646075
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2646064
    iget v0, p0, LX/1vt;->c:I

    .line 2646065
    move v0, v0

    .line 2646066
    return v0
.end method
