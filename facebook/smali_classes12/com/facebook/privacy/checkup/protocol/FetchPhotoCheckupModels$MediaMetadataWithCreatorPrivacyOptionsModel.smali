.class public final Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/1U6;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xaa51e43
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel$Serializer;
.end annotation


# instance fields
.field private A:Z

.field private B:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Z

.field private L:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:I

.field private Q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private S:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private T:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private U:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:J

.field private v:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2645289
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2645284
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2645285
    const/16 v0, 0x2b

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2645286
    return-void
.end method

.method private A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645287
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x1e

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645288
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private B()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645290
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->J:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    const/16 v1, 0x1f

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->J:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    .line 2645291
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->J:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    return-object v0
.end method

.method private C()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645292
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->L:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->L:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645293
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->L:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645294
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->M:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->M:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2645295
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->M:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method private E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645296
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->N:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    const/16 v1, 0x23

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->N:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    .line 2645297
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->N:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    return-object v0
.end method

.method private F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645298
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->O:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->O:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    .line 2645299
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->O:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    return-object v0
.end method

.method private G()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645300
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->Q:Ljava/lang/String;

    const/16 v1, 0x26

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->Q:Ljava/lang/String;

    .line 2645301
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->Q:Ljava/lang/String;

    return-object v0
.end method

.method private H()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645302
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->R:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->R:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    .line 2645303
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->R:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    return-object v0
.end method

.method private I()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645304
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->S:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    const/16 v1, 0x28

    const-class v2, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->S:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 2645305
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->S:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    return-object v0
.end method

.method private J()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645306
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->T:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    const/16 v1, 0x29

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->T:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    .line 2645307
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->T:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    return-object v0
.end method

.method private K()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645308
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->U:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->U:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    .line 2645309
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->U:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2645310
    iput-object p1, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->M:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2645311
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2645312
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2645313
    if-eqz v0, :cond_0

    .line 2645314
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x22

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 2645315
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;)V
    .locals 3
    .param p1    # Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2645316
    iput-object p1, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->w:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    .line 2645317
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2645318
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2645319
    if-eqz v0, :cond_0

    .line 2645320
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 2645321
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;)V
    .locals 3
    .param p1    # Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2645322
    iput-object p1, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->O:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    .line 2645323
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2645324
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2645325
    if-eqz v0, :cond_0

    .line 2645326
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x24

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 2645327
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;)V
    .locals 3
    .param p1    # Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2645328
    iput-object p1, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->T:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    .line 2645329
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2645330
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2645331
    if-eqz v0, :cond_0

    .line 2645332
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x29

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 2645333
    :cond_0
    return-void
.end method

.method private m()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645334
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    .line 2645335
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    return-object v0
.end method

.method private n()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645336
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->g:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->g:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    .line 2645337
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->g:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645338
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->h:Ljava/lang/String;

    .line 2645339
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private p()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645340
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->v:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    const/16 v1, 0x11

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->v:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    .line 2645341
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->v:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    return-object v0
.end method

.method private q()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645342
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->w:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->w:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    .line 2645343
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->w:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    return-object v0
.end method

.method private r()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645344
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->x:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->x:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    .line 2645345
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->x:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    return-object v0
.end method

.method private s()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645280
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->y:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->y:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    .line 2645281
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->y:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    return-object v0
.end method

.method private t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645282
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 2645283
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645031
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->B:Ljava/lang/String;

    const/16 v1, 0x17

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->B:Ljava/lang/String;

    .line 2645032
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->B:Ljava/lang/String;

    return-object v0
.end method

.method private v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645033
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->D:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x19

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->D:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645034
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->D:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645035
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x1a

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645036
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645037
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645038
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645039
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645040
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645041
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645042
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 31

    .prologue
    .line 2645043
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2645044
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2645045
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->m()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2645046
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->n()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2645047
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2645048
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->p()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2645049
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->q()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2645050
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->r()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2645051
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->s()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2645052
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2645053
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->u()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 2645054
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->d()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 2645055
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 2645056
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 2645057
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 2645058
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 2645059
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 2645060
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 2645061
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->B()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 2645062
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->C()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v22

    .line 2645063
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v23

    .line 2645064
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 2645065
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v25

    .line 2645066
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->G()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v26

    .line 2645067
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->H()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 2645068
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->I()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 2645069
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->J()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 2645070
    invoke-direct/range {p0 .. p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->K()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 2645071
    const/16 v6, 0x2b

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2645072
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v2}, LX/186;->b(II)V

    .line 2645073
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2645074
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2645075
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2645076
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645077
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645078
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645079
    const/4 v2, 0x7

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->l:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645080
    const/16 v2, 0x8

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->m:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645081
    const/16 v2, 0x9

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->n:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645082
    const/16 v2, 0xa

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->o:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645083
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->p:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645084
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645085
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645086
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->s:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645087
    const/16 v2, 0xf

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->t:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645088
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->u:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2645089
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2645090
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2645091
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2645092
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2645093
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2645094
    const/16 v2, 0x16

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->A:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645095
    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2645096
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2645097
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2645098
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645099
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645100
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645101
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645102
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645103
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645104
    const/16 v2, 0x20

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->K:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2645105
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645106
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645107
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645108
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645109
    const/16 v2, 0x25

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->P:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 2645110
    const/16 v2, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645111
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645112
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645113
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645114
    const/16 v2, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2645115
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2645116
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2645117
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2645118
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->m()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2645119
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->m()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    .line 2645120
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->m()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2645121
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645122
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->f:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$AlbumModel;

    .line 2645123
    :cond_0
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->n()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2645124
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->n()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    .line 2645125
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->n()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2645126
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645127
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->g:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataAttributionAppModel;

    .line 2645128
    :cond_1
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->p()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2645129
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->p()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    .line 2645130
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->p()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2645131
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645132
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->v:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    .line 2645133
    :cond_2
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->q()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2645134
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->q()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    .line 2645135
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->q()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2645136
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645137
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->w:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    .line 2645138
    :cond_3
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->r()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2645139
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->r()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    .line 2645140
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->r()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2645141
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645142
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->x:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    .line 2645143
    :cond_4
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->s()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2645144
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->s()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    .line 2645145
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->s()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2645146
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645147
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->y:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    .line 2645148
    :cond_5
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2645149
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 2645150
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 2645151
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645152
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->z:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 2645153
    :cond_6
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2645154
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645155
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 2645156
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645157
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->D:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645158
    :cond_7
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 2645159
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645160
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 2645161
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645162
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->E:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645163
    :cond_8
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 2645164
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645165
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_9

    .line 2645166
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645167
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->F:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645168
    :cond_9
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 2645169
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645170
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 2645171
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645172
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->G:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645173
    :cond_a
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 2645174
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645175
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_b

    .line 2645176
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645177
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->H:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645178
    :cond_b
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 2645179
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645180
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->A()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 2645181
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645182
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645183
    :cond_c
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->B()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 2645184
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->B()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    .line 2645185
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->B()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_d

    .line 2645186
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645187
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->J:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataInlineActivitiesModel;

    .line 2645188
    :cond_d
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->C()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 2645189
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->C()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645190
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->C()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 2645191
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645192
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->L:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2645193
    :cond_e
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 2645194
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2645195
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->D()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_f

    .line 2645196
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645197
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->M:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2645198
    :cond_f
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 2645199
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    .line 2645200
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_10

    .line 2645201
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645202
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->N:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    .line 2645203
    :cond_10
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 2645204
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    .line 2645205
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 2645206
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645207
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->O:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    .line 2645208
    :cond_11
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->H()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 2645209
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->H()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    .line 2645210
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->H()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 2645211
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645212
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->R:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    .line 2645213
    :cond_12
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->I()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 2645214
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->I()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 2645215
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->I()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 2645216
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645217
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->S:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 2645218
    :cond_13
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->J()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 2645219
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->J()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    .line 2645220
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->J()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v2

    if-eq v2, v0, :cond_14

    .line 2645221
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645222
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->T:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    .line 2645223
    :cond_14
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->K()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 2645224
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->K()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    .line 2645225
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->K()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 2645226
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    .line 2645227
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->U:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$WithTagsModel;

    .line 2645228
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2645229
    if-nez v1, :cond_16

    :goto_0
    return-object p0

    :cond_16
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2645230
    new-instance v0, LX/J4w;

    invoke-direct {v0, p1}, LX/J4w;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645231
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2645232
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2645233
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->i:Z

    .line 2645234
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->j:Z

    .line 2645235
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->k:Z

    .line 2645236
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->l:Z

    .line 2645237
    const/16 v0, 0x8

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->m:Z

    .line 2645238
    const/16 v0, 0x9

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->n:Z

    .line 2645239
    const/16 v0, 0xa

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->o:Z

    .line 2645240
    const/16 v0, 0xb

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->p:Z

    .line 2645241
    const/16 v0, 0xc

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->q:Z

    .line 2645242
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->r:Z

    .line 2645243
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->s:Z

    .line 2645244
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->t:Z

    .line 2645245
    const/16 v0, 0x10

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->u:J

    .line 2645246
    const/16 v0, 0x16

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->A:Z

    .line 2645247
    const/16 v0, 0x20

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->K:Z

    .line 2645248
    const/16 v0, 0x25

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->P:I

    .line 2645249
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2645250
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2645251
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2645252
    const-string v0, "explicit_place"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2645253
    check-cast p2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    invoke-direct {p0, p2}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;)V

    .line 2645254
    :cond_0
    :goto_0
    return-void

    .line 2645255
    :cond_1
    const-string v0, "message"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2645256
    check-cast p2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-direct {p0, p2}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->a(Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;)V

    goto :goto_0

    .line 2645257
    :cond_2
    const-string v0, "pending_place"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2645258
    check-cast p2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;

    invoke-direct {p0, p2}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$PendingPlaceModel;)V

    goto :goto_0

    .line 2645259
    :cond_3
    const-string v0, "tags"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2645260
    check-cast p2, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    invoke-direct {p0, p2}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2645261
    return-void
.end method

.method public final synthetic ai_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645262
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->y()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aj_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645263
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->w()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645264
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2645265
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2645266
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2645267
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;-><init>()V

    .line 2645268
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2645269
    return-object v0
.end method

.method public final synthetic c()LX/1f8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645270
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645271
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->C:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->C:Ljava/lang/String;

    .line 2645272
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2645273
    const v0, 0x28bb9e45

    return v0
.end method

.method public final synthetic e()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645274
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->v()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2645279
    const v0, 0x46c7fc4

    return v0
.end method

.method public final synthetic j()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645275
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->z()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 2645276
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2645277
    iget-wide v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->u:J

    return-wide v0
.end method

.method public final synthetic l()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2645278
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaMetadataWithCreatorPrivacyOptionsModel;->H()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    move-result-object v0

    return-object v0
.end method
