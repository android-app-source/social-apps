.class public final Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2644837
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;

    new-instance v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2644838
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2644839
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2644828
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2644829
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2644830
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2644831
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2644832
    if-eqz v2, :cond_0

    .line 2644833
    const-string p0, "photo_checkup_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2644834
    invoke-static {v1, v2, p1, p2}, LX/J4y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2644835
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2644836
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2644827
    check-cast p1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$Serializer;->a(Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
