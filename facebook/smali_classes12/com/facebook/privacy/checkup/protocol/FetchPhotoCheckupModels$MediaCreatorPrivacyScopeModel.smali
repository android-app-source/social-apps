.class public final Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7a53117
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2645008
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2644998
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2644996
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2644997
    return-void
.end method

.method private j()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2644994
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;

    .line 2644995
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;

    return-object v0
.end method

.method private k()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2644992
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->g:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->g:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    .line 2644993
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->g:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2644999
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2645000
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2645001
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->k()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2645002
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2645003
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 2645004
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2645005
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2645006
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2645007
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2644979
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2644980
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2644981
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;

    .line 2644982
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2644983
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    .line 2644984
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$IconImageModel;

    .line 2644985
    :cond_0
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->k()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2644986
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->k()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    .line 2644987
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->k()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2644988
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    .line 2644989
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->g:Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    .line 2644990
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2644991
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2644978
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->k()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel$PrivacyOptionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2644975
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2644976
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;->e:Z

    .line 2644977
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2644972
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$MediaCreatorPrivacyScopeModel;-><init>()V

    .line 2644973
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2644974
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2644971
    const v0, 0x7513dd5a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2644970
    const v0, -0x1c648c34

    return v0
.end method
