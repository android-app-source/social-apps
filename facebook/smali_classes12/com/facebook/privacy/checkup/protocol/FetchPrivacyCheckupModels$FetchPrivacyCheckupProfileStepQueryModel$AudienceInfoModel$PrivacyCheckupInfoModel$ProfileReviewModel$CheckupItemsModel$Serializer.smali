.class public final Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2646951
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    new-instance v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2646952
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2646953
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2646954
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2646955
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/J5S;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2646956
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2646957
    check-cast p1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel$Serializer;->a(Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;LX/0nX;LX/0my;)V

    return-void
.end method
