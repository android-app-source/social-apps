.class public final Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7d5ab891
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2647283
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2647282
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2647280
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2647281
    return-void
.end method

.method private j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2647278
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->g:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->g:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    .line 2647279
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->g:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2647242
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2647243
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2647244
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->c()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x2c67e2a4

    invoke-static {v2, v1, v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2647245
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2647246
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2647247
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2647248
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2647249
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2647250
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2647251
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2647263
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2647264
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->c()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2647265
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->c()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2c67e2a4

    invoke-static {v2, v0, v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2647266
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->c()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2647267
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;

    .line 2647268
    iput v3, v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->f:I

    move-object v1, v0

    .line 2647269
    :cond_0
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2647270
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    .line 2647271
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2647272
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;

    .line 2647273
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->g:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    .line 2647274
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2647275
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2647276
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2647277
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2647284
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->e:Ljava/lang/String;

    .line 2647285
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2647260
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2647261
    const/4 v0, 0x1

    const v1, 0x2c67e2a4

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->f:I

    .line 2647262
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2647257
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;-><init>()V

    .line 2647258
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2647259
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2647256
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel$PrivacyOptionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getIconImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2647254
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2647255
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$PrivacyCheckupItemPrivacyScopeFragmentModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2647253
    const v0, 0x13726bb7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2647252
    const v0, -0x1c648c34

    return v0
.end method
