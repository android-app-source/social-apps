.class public final Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6a6cb7db
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2647034
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2647035
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2647047
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2647048
    return-void
.end method

.method private j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCheckupItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2647036
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    .line 2647037
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2647038
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2647039
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->c(Ljava/util/List;)I

    move-result v0

    .line 2647040
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2647041
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2647042
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2647043
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2647044
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2647045
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2647046
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2647025
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2647026
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2647027
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    .line 2647028
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2647029
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;

    .line 2647030
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->f:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    .line 2647031
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2647032
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCheckupItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2647033
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->j()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2647022
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2647023
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->g:Z

    .line 2647024
    return-void
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2647020
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->e:Ljava/util/List;

    .line 2647021
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2647017
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;-><init>()V

    .line 2647018
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2647019
    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2647015
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2647016
    iget-boolean v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2647014
    const v0, -0x75392112

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2647013
    const v0, 0x379f5506

    return v0
.end method
