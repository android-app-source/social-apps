.class public final Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x298511d8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2646412
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2646428
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2646426
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2646427
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2646420
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2646421
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2646422
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2646423
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2646424
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2646425
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2646429
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2646430
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2646431
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    .line 2646432
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2646433
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;

    .line 2646434
    iput-object v0, v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    .line 2646435
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2646436
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPrivacyReviewExperiences"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2646418
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    .line 2646419
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;->e:Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel$PrivacyReviewExperiencesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2646415
    new-instance v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;-><init>()V

    .line 2646416
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2646417
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2646414
    const v0, -0x59be0e97

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2646413
    const v0, -0x6747e1ce

    return v0
.end method
