.class public Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2644585
    new-instance v0, LX/J4r;

    invoke-direct {v0}, LX/J4r;-><init>()V

    sput-object v0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2644586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2644587
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->a:I

    .line 2644588
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->b:Ljava/lang/String;

    .line 2644589
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->c:I

    .line 2644590
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 2644591
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->d:Ljava/lang/String;

    .line 2644592
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->e:Ljava/lang/String;

    .line 2644593
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 2644594
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v1, v0

    :cond_0
    iput-object v1, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->f:Ljava/lang/String;

    .line 2644595
    return-void

    :cond_1
    move-object v0, v1

    .line 2644596
    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2644597
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2644598
    iget v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2644599
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2644600
    iget v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2644601
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2644602
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2644603
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2644604
    return-void

    .line 2644605
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->d:Ljava/lang/String;

    goto :goto_0

    .line 2644606
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->f:Ljava/lang/String;

    goto :goto_1
.end method
