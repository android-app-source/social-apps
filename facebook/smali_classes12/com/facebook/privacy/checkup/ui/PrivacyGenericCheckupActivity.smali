.class public Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static B:I


# instance fields
.field public A:LX/2EJ;

.field public C:J

.field public D:I

.field public E:I

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/J3t;

.field private r:Landroid/content/res/Resources;

.field private s:LX/0W3;

.field public t:LX/J5k;

.field public u:LX/0SG;

.field public v:LX/0h5;

.field public w:Lcom/facebook/widget/CustomViewPager;

.field public x:LX/J6h;

.field private y:Ljava/lang/String;

.field public z:Landroid/os/CountDownTimer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2649925
    const/4 v0, 0x0

    sput v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->B:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2649815
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2649816
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->C:J

    .line 2649817
    iput v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->D:I

    .line 2649818
    iput v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->E:I

    .line 2649819
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2649920
    const v0, 0x7f0d26c8

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    .line 2649921
    new-instance v0, LX/J6h;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->r:Landroid/content/res/Resources;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-direct {v0, v1, v2, v3}, LX/J6h;-><init>(Landroid/content/res/Resources;LX/0gc;LX/J3t;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->x:LX/J6h;

    .line 2649922
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->x:LX/J6h;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2649923
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->b()V

    .line 2649924
    return-void
.end method

.method private a(LX/J3t;Landroid/content/res/Resources;LX/0W3;LX/J5k;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2649914
    iput-object p1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649915
    iput-object p2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->r:Landroid/content/res/Resources;

    .line 2649916
    iput-object p3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->s:LX/0W3;

    .line 2649917
    iput-object p4, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->t:LX/J5k;

    .line 2649918
    iput-object p5, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->u:LX/0SG;

    .line 2649919
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-static {v5}, LX/J3t;->a(LX/0QB;)LX/J3t;

    move-result-object v1

    check-cast v1, LX/J3t;

    invoke-static {v5}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {v5}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-static {v5}, LX/J5k;->b(LX/0QB;)LX/J5k;

    move-result-object v4

    check-cast v4, LX/J5k;

    invoke-static {v5}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->a(LX/J3t;Landroid/content/res/Resources;LX/0W3;LX/J5k;LX/0SG;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;LX/0h5;)V
    .locals 3

    .prologue
    .line 2649908
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, LX/J3t;->a(I)LX/J4L;

    move-result-object v1

    iget-object v1, v1, LX/J4L;->l:Ljava/lang/String;

    .line 2649909
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2649910
    move-object v0, v0

    .line 2649911
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2649912
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {p1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2649913
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2649889
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2649890
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    .line 2649891
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083903

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2649892
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2649893
    move-object v0, v0

    .line 2649894
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081304

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2649895
    iput-object v1, v0, LX/108;->j:Ljava/lang/String;

    .line 2649896
    move-object v0, v0

    .line 2649897
    const/4 v1, 0x1

    .line 2649898
    iput-boolean v1, v0, LX/108;->d:Z

    .line 2649899
    move-object v0, v0

    .line 2649900
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2649901
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v1}, LX/J3t;->c()I

    move-result v1

    if-eqz v1, :cond_0

    .line 2649902
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    iget-object v3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v2, v3}, LX/J3t;->a(I)LX/J4L;

    move-result-object v2

    iget-object v2, v2, LX/J4L;->m:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2649903
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    invoke-static {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;LX/0h5;)V

    .line 2649904
    :cond_0
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    new-instance v2, LX/J6U;

    invoke-direct {v2, p0}, LX/J6U;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    invoke-interface {v1, v2}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2649905
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    new-instance v2, LX/J6V;

    invoke-direct {v2, p0}, LX/J6V;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    invoke-interface {v1, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2649906
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2649907
    return-void
.end method

.method public static b(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2649885
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 2649886
    if-eqz v0, :cond_0

    .line 2649887
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2649888
    :cond_0
    return-void
.end method

.method public static synthetic k(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)I
    .locals 2

    .prologue
    .line 2649884
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->E:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->E:I

    return v0
.end method

.method public static l$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2649879
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->u:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->C:J

    .line 2649880
    const v0, 0x7f081304

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "SEND_PROGRESS_DIALOG_TAG"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2649881
    new-instance v0, LX/J6W;

    sget v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->B:I

    int-to-long v2, v1

    sget v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->B:I

    int-to-long v4, v1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/J6W;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;JJ)V

    invoke-virtual {v0}, LX/J6W;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->z:Landroid/os/CountDownTimer;

    .line 2649882
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    const-string v1, "id_backed_privacy_checkup"

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->p:LX/0Px;

    new-instance v3, LX/J6X;

    invoke-direct {v3, p0}, LX/J6X;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/J3t;->a(Ljava/lang/String;LX/0Px;LX/0Vd;)Z

    .line 2649883
    return-void
.end method

.method public static m(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V
    .locals 3

    .prologue
    .line 2649875
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    if-eqz v0, :cond_0

    .line 2649876
    :goto_0
    return-void

    .line 2649877
    :cond_0
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f0838f5

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080043

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/J6a;

    invoke-direct {v2, p0}, LX/J6a;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080018

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/J6Z;

    invoke-direct {v2, p0}, LX/J6Z;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    new-instance v1, LX/J6Y;

    invoke-direct {v1, p0}, LX/J6Y;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    .line 2649878
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public static n(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V
    .locals 3

    .prologue
    .line 2649870
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    if-eqz v0, :cond_0

    .line 2649871
    :goto_0
    return-void

    .line 2649872
    :cond_0
    const-string v0, "FETCH_PROGRESS_DIALOG_TAG"

    invoke-static {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->b(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;Ljava/lang/String;)V

    .line 2649873
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f0838f5

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080018

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/J6Q;

    invoke-direct {v2, p0}, LX/J6Q;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    new-instance v1, LX/J6b;

    invoke-direct {v1, p0}, LX/J6b;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    .line 2649874
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public static o(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V
    .locals 3

    .prologue
    .line 2649866
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    if-eqz v0, :cond_0

    .line 2649867
    :goto_0
    return-void

    .line 2649868
    :cond_0
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f081306

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f081307

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x7f081308

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/J6S;

    invoke-direct {v2, p0}, LX/J6S;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f083904

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    new-instance v1, LX/J6R;

    invoke-direct {v1, p0}, LX/J6R;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    .line 2649869
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public static synthetic q(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)I
    .locals 2

    .prologue
    .line 2649865
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->D:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->D:I

    return v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2649853
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2649854
    invoke-static {p0, p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2649855
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "checkup_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->y:Ljava/lang/String;

    .line 2649856
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "PrivacyGenericCheckupActivity no review_id for query"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2649857
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->y:Ljava/lang/String;

    .line 2649858
    iput-object v1, v0, LX/J3t;->i:Ljava/lang/String;

    .line 2649859
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->s:LX/0W3;

    sget-wide v2, LX/0X5;->gT:J

    const/16 v1, 0x2710

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    sput v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->B:I

    .line 2649860
    const v0, 0x7f031024

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->setContentView(I)V

    .line 2649861
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->a()V

    .line 2649862
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->b()V

    .line 2649863
    return-void

    .line 2649864
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x1c21de4e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2649844
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2649845
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2649846
    const/16 v1, 0x23

    const v2, -0x480be6a4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2649847
    :goto_0
    return-void

    .line 2649848
    :cond_0
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649849
    const/4 v2, 0x0

    sput-boolean v2, LX/J3t;->j:Z

    .line 2649850
    invoke-virtual {v1}, LX/J3t;->a()V

    .line 2649851
    iget-object v2, v1, LX/J3t;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2649852
    const v1, -0x1219d966

    invoke-static {v1, v0}, LX/02F;->c(II)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0xa689bb2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2649831
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2649832
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->z:Landroid/os/CountDownTimer;

    if-eqz v1, :cond_0

    .line 2649833
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->z:Landroid/os/CountDownTimer;

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->cancel()V

    .line 2649834
    :cond_0
    iput-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->z:Landroid/os/CountDownTimer;

    .line 2649835
    const-string v1, "FETCH_PROGRESS_DIALOG_TAG"

    invoke-static {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->b(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;Ljava/lang/String;)V

    .line 2649836
    const-string v1, "SEND_PROGRESS_DIALOG_TAG"

    invoke-static {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->b(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;Ljava/lang/String;)V

    .line 2649837
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    if-eqz v1, :cond_1

    .line 2649838
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    invoke-virtual {v1}, LX/2EJ;->dismiss()V

    .line 2649839
    iput-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->A:LX/2EJ;

    .line 2649840
    :cond_1
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649841
    iget-object v2, v1, LX/J3t;->b:LX/1Ck;

    sget-object v4, LX/J3s;->FETCH_GENERIC_PRIVACY_REVIEW_INFO:LX/J3s;

    invoke-virtual {v2, v4}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2649842
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v1}, LX/J3t;->h()V

    .line 2649843
    const/16 v1, 0x23

    const v2, -0xdda8c77

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/4 v7, 0x0

    const/16 v0, 0x22

    const v1, 0x542bed6b

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2649820
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2649821
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v0}, LX/J3t;->c()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2649822
    const/16 v0, 0x23

    const v1, -0x453e93cc

    invoke-static {v2, v0, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2649823
    :goto_0
    return-void

    .line 2649824
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "FETCH_PROGRESS_DIALOG_TAG"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 2649825
    if-nez v0, :cond_1

    .line 2649826
    const v0, 0x7f080024

    const/4 v1, 0x1

    invoke-static {v0, v1, v7, v7}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 2649827
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "FETCH_PROGRESS_DIALOG_TAG"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2649828
    :cond_1
    new-instance v0, LX/J6T;

    sget v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->B:I

    int-to-long v2, v1

    sget v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->B:I

    int-to-long v4, v1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/J6T;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;JJ)V

    invoke-virtual {v0}, LX/J6T;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->z:Landroid/os/CountDownTimer;

    .line 2649829
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    new-instance v1, LX/J6c;

    invoke-direct {v1, p0}, LX/J6c;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->y:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/J3t;->a(LX/J3r;Ljava/lang/String;Ljava/lang/String;)V

    .line 2649830
    const v0, 0xf3d5f92

    invoke-static {v0, v6}, LX/02F;->c(II)V

    goto :goto_0
.end method
