.class public Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;
.super LX/1Cv;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final a:LX/J64;

.field public final b:LX/J4L;

.field public final d:Landroid/view/LayoutInflater;

.field public final e:Landroid/content/res/Resources;

.field public final f:Landroid/content/Context;

.field public final g:LX/J5k;

.field private final h:Landroid/view/animation/Animation;

.field public i:Landroid/view/View;

.field public j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/8SN;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2648989
    const-class v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/content/res/Resources;Landroid/content/Context;LX/J5k;LX/J64;LX/J4L;)V
    .locals 1
    .param p5    # LX/J64;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/J4L;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2648978
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2648979
    new-instance v0, LX/J6C;

    invoke-direct {v0, p0}, LX/J6C;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->k:LX/8SN;

    .line 2648980
    iput-object p1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->d:Landroid/view/LayoutInflater;

    .line 2648981
    iput-object p2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->e:Landroid/content/res/Resources;

    .line 2648982
    iput-object p3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->f:Landroid/content/Context;

    .line 2648983
    iput-object p4, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->g:LX/J5k;

    .line 2648984
    iput-object p5, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->a:LX/J64;

    .line 2648985
    iput-object p6, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b:LX/J4L;

    .line 2648986
    const v0, 0x7f040029

    invoke-static {p3, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->h:Landroid/view/animation/Animation;

    .line 2648987
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->j:Ljava/util/Set;

    .line 2648988
    return-void
.end method

.method private static a(I)LX/J6K;
    .locals 1

    .prologue
    .line 2648977
    invoke-static {}, LX/J6K;->values()[LX/J6K;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;Ljava/lang/String;ZLandroid/view/View;)V
    .locals 2

    .prologue
    .line 2648970
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->h:Landroid/view/animation/Animation;

    new-instance v1, LX/J6D;

    invoke-direct {v1, p0, p1, p2}, LX/J6D;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2648971
    if-eqz p2, :cond_0

    sget-object v0, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_DELETE_POSTS:LX/J5h;

    .line 2648972
    :goto_0
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->g:LX/J5k;

    invoke-virtual {v1, v0}, LX/J5k;->a(LX/J5h;)V

    .line 2648973
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2648974
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->h:Landroid/view/animation/Animation;

    invoke-virtual {p3, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2648975
    return-void

    .line 2648976
    :cond_0
    sget-object v0, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_DELETE_DIALOG_DELETE_APP_ONLY:LX/J5h;

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2648964
    invoke-static {p1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->a(I)LX/J6K;

    move-result-object v0

    .line 2648965
    sget-object v1, LX/J6F;->a:[I

    invoke-virtual {v0}, LX/J6K;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2648966
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2648967
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f031018

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2648968
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f031014

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2648969
    :pswitch_2
    new-instance v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2648945
    check-cast p2, LX/J4F;

    .line 2648946
    invoke-static {p4}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->a(I)LX/J6K;

    move-result-object v0

    .line 2648947
    sget-object v1, LX/J6F;->a:[I

    invoke-virtual {v0}, LX/J6K;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2648948
    :goto_0
    return-void

    .line 2648949
    :pswitch_0
    const/4 p1, 0x0

    .line 2648950
    check-cast p3, Landroid/view/ViewGroup;

    .line 2648951
    const v0, 0x7f0d04f7

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2648952
    instance-of v1, p2, LX/J4G;

    if-eqz v1, :cond_0

    .line 2648953
    check-cast p2, LX/J4G;

    iget-object v1, p2, LX/J4G;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2648954
    :goto_1
    goto :goto_0

    .line 2648955
    :pswitch_1
    check-cast p2, LX/J4J;

    invoke-virtual {p0, p3, p2}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->a(Landroid/view/View;LX/J4J;)V

    goto :goto_0

    .line 2648956
    :pswitch_2
    check-cast p3, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2648957
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b:LX/J4L;

    iget-boolean v0, v0, LX/J4L;->k:Z

    if-eqz v0, :cond_2

    .line 2648958
    invoke-virtual {p3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2648959
    :goto_2
    goto :goto_0

    .line 2648960
    :cond_0
    instance-of v1, p2, LX/J4H;

    if-eqz v1, :cond_1

    .line 2648961
    check-cast p2, LX/J4H;

    iget-object v1, p2, LX/J4H;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2648962
    :cond_1
    const-string v0, "Section header of type: %s unrecognized!"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, p1

    invoke-static {p1, v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 2648963
    :cond_2
    invoke-virtual {p3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/view/View;LX/J4J;)V
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 2648873
    invoke-virtual {p0, p1, p2}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b(Landroid/view/View;LX/J4J;)V

    .line 2648874
    const v0, 0x7f0d2694

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/spinner/AudienceSpinner;

    .line 2648875
    const v1, 0x7f0d2695

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/ui/PrivacyOptionView;

    .line 2648876
    iget-boolean v2, p2, LX/J4J;->i:Z

    if-eqz v2, :cond_2

    .line 2648877
    invoke-virtual {v1, v4}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setVisibility(I)V

    .line 2648878
    invoke-virtual {v0, v4}, Lcom/facebook/privacy/spinner/AudienceSpinner;->setVisibility(I)V

    .line 2648879
    :goto_0
    const v6, 0x7f0d2697

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 2648880
    iget-boolean v7, p2, LX/J4J;->i:Z

    if-nez v7, :cond_6

    iget-object v7, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b:LX/J4L;

    iget-object v7, v7, LX/J4L;->d:LX/0Px;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->DELETE_APP_ONLY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    invoke-virtual {v7, v8}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b:LX/J4L;

    iget-object v7, v7, LX/J4L;->d:LX/0Px;

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->DELETE_APP_AND_POSTS:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    invoke-virtual {v7, v8}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    :cond_0
    const/4 v7, 0x1

    :goto_1
    move v7, v7

    .line 2648881
    if-nez v7, :cond_4

    .line 2648882
    if-eqz v6, :cond_1

    .line 2648883
    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2648884
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2648885
    :cond_1
    :goto_2
    return-void

    .line 2648886
    :cond_2
    iget-object v2, p2, LX/J4J;->f:LX/8SR;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b:LX/J4L;

    iget-object v2, v2, LX/J4L;->d:LX/0Px;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->CHANGE_PRIVACY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2648887
    iget-object v2, p2, LX/J4J;->f:LX/8SR;

    iget-object v3, p2, LX/J4J;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/privacy/spinner/AudienceSpinner;->a(LX/8SR;Ljava/lang/String;)V

    .line 2648888
    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->k:LX/8SN;

    invoke-virtual {v0, v2}, Lcom/facebook/privacy/spinner/AudienceSpinner;->setPrivacyChangeListener(LX/8SN;)V

    .line 2648889
    invoke-virtual {v0, v5}, Lcom/facebook/privacy/spinner/AudienceSpinner;->setVisibility(I)V

    .line 2648890
    invoke-virtual {v1, v4}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setVisibility(I)V

    goto :goto_0

    .line 2648891
    :cond_3
    iget-object v2, p2, LX/J4J;->g:Ljava/lang/String;

    iget-object v3, p2, LX/J4J;->h:LX/1Fd;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(Ljava/lang/String;LX/1Fd;)V

    .line 2648892
    invoke-virtual {v1, v5}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setVisibility(I)V

    .line 2648893
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2648894
    invoke-virtual {v0, v4}, Lcom/facebook/privacy/spinner/AudienceSpinner;->setVisibility(I)V

    goto :goto_0

    .line 2648895
    :cond_4
    if-nez v6, :cond_5

    .line 2648896
    const v6, 0x7f0d2696

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewStub;

    .line 2648897
    invoke-virtual {v6}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/fbui/glyph/GlyphView;

    move-object v12, v6

    .line 2648898
    :goto_3
    const/4 v6, 0x0

    invoke-virtual {v12, v6}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2648899
    new-instance v6, LX/J6H;

    iget-object v9, p2, LX/J4J;->c:Ljava/lang/String;

    iget-object v10, p2, LX/J4J;->e:Ljava/lang/String;

    iget-object v11, p2, LX/J4J;->a:Ljava/lang/String;

    move-object v7, p0

    move-object v8, p1

    invoke-direct/range {v6 .. v11}, LX/J6H;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v6}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2648900
    :cond_5
    check-cast v6, Lcom/facebook/fbui/glyph/GlyphView;

    move-object v12, v6

    goto :goto_3

    :cond_6
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public final b(Landroid/view/View;LX/J4J;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2648917
    const v0, 0x7f0d0e18

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2648918
    iget-boolean v1, p2, LX/J4J;->i:Z

    if-eqz v1, :cond_1

    .line 2648919
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2648920
    :goto_0
    const v0, 0x7f0d2693

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2648921
    iget-boolean v1, p2, LX/J4J;->i:Z

    if-eqz v1, :cond_2

    .line 2648922
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2648923
    const v1, 0x7f0838ef

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2648924
    :goto_1
    iget-object v0, p2, LX/J4J;->e:Ljava/lang/String;

    iget-object v1, p2, LX/J4J;->c:Ljava/lang/String;

    const/4 p2, 0x0

    .line 2648925
    const v2, 0x7f0d2692

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2648926
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2648927
    if-eqz v2, :cond_0

    .line 2648928
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2648929
    :cond_0
    :goto_2
    return-void

    .line 2648930
    :cond_1
    iget-object v1, p2, LX/J4J;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2648931
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 2648932
    :cond_2
    iget-object v1, p2, LX/J4J;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2648933
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2648934
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2648935
    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2648936
    iget-object v1, p2, LX/J4J;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2648937
    :cond_4
    if-nez v2, :cond_5

    .line 2648938
    new-instance v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2648939
    const v3, 0x7f0d2692

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setId(I)V

    .line 2648940
    check-cast p1, Landroid/view/ViewGroup;

    const v3, 0x7f0d2691

    invoke-static {p1, v3, v2}, LX/478;->a(Landroid/view/ViewGroup;ILandroid/view/View;)V

    .line 2648941
    :goto_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2648942
    iget-object v3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->e:Landroid/content/res/Resources;

    const v4, 0x7f0838e4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, p2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2648943
    invoke-virtual {v2, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_2

    .line 2648944
    :cond_5
    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    goto :goto_3
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2648916
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b:LX/J4L;

    iget-object v0, v0, LX/J4L;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2648913
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b:LX/J4L;

    iget-object v0, v0, LX/J4L;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 2648914
    const/4 v0, 0x0

    .line 2648915
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b:LX/J4L;

    iget-object v0, v0, LX/J4L;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2648912
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2648902
    invoke-virtual {p0, p1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J4F;

    .line 2648903
    if-nez v0, :cond_0

    .line 2648904
    sget-object v0, LX/J6K;->LOADING_INDICATOR:LX/J6K;

    invoke-virtual {v0}, LX/J6K;->ordinal()I

    move-result v0

    .line 2648905
    :goto_0
    return v0

    .line 2648906
    :cond_0
    instance-of v2, v0, LX/J4G;

    if-nez v2, :cond_1

    instance-of v2, v0, LX/J4H;

    if-eqz v2, :cond_2

    .line 2648907
    :cond_1
    sget-object v0, LX/J6K;->CHECKUP_LIST_SECTION_HEADER_TYPE:LX/J6K;

    invoke-virtual {v0}, LX/J6K;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2648908
    :cond_2
    instance-of v2, v0, LX/J4J;

    if-eqz v2, :cond_3

    .line 2648909
    sget-object v0, LX/J6K;->CHECKUP_LIST_SECTION_ITEM_TYPE:LX/J6K;

    invoke-virtual {v0}, LX/J6K;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2648910
    :cond_3
    const-string v2, "Got a PrivacyCheckupRowType that did not map to a view type: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v1, v2, v3}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 2648911
    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2648901
    invoke-static {}, LX/J6K;->values()[LX/J6K;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
