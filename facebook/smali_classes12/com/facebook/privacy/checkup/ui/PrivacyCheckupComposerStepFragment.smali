.class public Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;
.super Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;
.source ""


# instance fields
.field public k:LX/J5t;

.field public l:LX/8RJ;

.field private final m:Landroid/view/View$OnClickListener;

.field public n:Lcom/facebook/privacy/selector/AudienceFragmentDialog;

.field public o:LX/8Q5;

.field public p:LX/8Qm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2649145
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;-><init>()V

    .line 2649146
    new-instance v0, LX/J5u;

    invoke-direct {v0, p0}, LX/J5u;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->m:Landroid/view/View$OnClickListener;

    .line 2649147
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2649158
    invoke-super {p0, p1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->a(Landroid/os/Bundle;)V

    .line 2649159
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;

    const-class p1, LX/J5t;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/J5t;

    invoke-static {v0}, LX/8RJ;->a(LX/0QB;)LX/8RJ;

    move-result-object v0

    check-cast v0, LX/8RJ;

    iput-object p1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->k:LX/J5t;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->l:LX/8RJ;

    .line 2649160
    return-void
.end method

.method public final b()V
    .locals 11

    .prologue
    .line 2649161
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->k:LX/J5t;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->i:LX/J64;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    iget-object v3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {v2, v3}, LX/J45;->b(LX/J4K;)LX/J4L;

    move-result-object v2

    .line 2649162
    new-instance v4, LX/J5s;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/J5k;->b(LX/0QB;)LX/J5k;

    move-result-object v8

    check-cast v8, LX/J5k;

    move-object v9, v1

    move-object v10, v2

    invoke-direct/range {v4 .. v10}, LX/J5s;-><init>(Landroid/view/LayoutInflater;Landroid/content/res/Resources;Landroid/content/Context;LX/J5k;LX/J64;LX/J4L;)V

    .line 2649163
    move-object v0, v4

    .line 2649164
    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->h:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    .line 2649165
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->m:Landroid/view/View$OnClickListener;

    .line 2649166
    iput-object v1, v0, LX/J5s;->c:Landroid/view/View$OnClickListener;

    .line 2649167
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2649168
    return-void
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 2649153
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    new-instance v1, LX/J6P;

    invoke-direct {v1, p0, v3}, LX/J6P;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;Z)V

    .line 2649154
    sget-object v2, LX/J4K;->COMPOSER_STEP:LX/J4K;

    invoke-virtual {v0, v2}, LX/J45;->b(LX/J4K;)LX/J4L;

    move-result-object v2

    iget-boolean v2, v2, LX/J4L;->k:Z

    if-nez v2, :cond_0

    .line 2649155
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    new-instance v1, LX/J6P;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LX/J6P;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;Z)V

    invoke-virtual {v0, v1, v3}, LX/J45;->a(LX/J6P;Z)V

    .line 2649156
    return-void

    .line 2649157
    :cond_0
    iget-object v2, v0, LX/J45;->b:LX/1Ck;

    sget-object v4, LX/J44;->FETCH_COMPOSER_INFO:LX/J44;

    new-instance v5, LX/J3w;

    invoke-direct {v5, v0}, LX/J3w;-><init>(LX/J45;)V

    new-instance v6, LX/J43;

    invoke-direct {v6, v0, v1}, LX/J43;-><init>(LX/J45;LX/J6P;)V

    invoke-virtual {v2, v4, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x684f4e99

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2649148
    invoke-super {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->onDestroy()V

    .line 2649149
    iput-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->n:Lcom/facebook/privacy/selector/AudienceFragmentDialog;

    .line 2649150
    iput-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->o:LX/8Q5;

    .line 2649151
    iput-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;->p:LX/8Qm;

    .line 2649152
    const/16 v1, 0x2b

    const v2, -0x43c53747

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
