.class public final Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final p:[LX/J4K;


# instance fields
.field private A:Z

.field private final B:LX/63W;

.field private final C:Landroid/view/View$OnClickListener;

.field private final D:Landroid/view/View$OnClickListener;

.field private final E:Landroid/view/View$OnClickListener;

.field private final F:LX/0hc;

.field public q:LX/J45;

.field private r:LX/23P;

.field private s:LX/J3m;

.field private t:LX/0ad;

.field private u:Lcom/facebook/widget/CustomViewPager;

.field public v:LX/J5y;

.field private w:LX/0h5;

.field private x:Landroid/widget/ScrollView;

.field public y:LX/J5r;

.field private z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2648818
    const/4 v0, 0x3

    new-array v0, v0, [LX/J4K;

    const/4 v1, 0x0

    sget-object v2, LX/J4K;->COMPOSER_STEP:LX/J4K;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/J4K;->PROFILE_STEP:LX/J4K;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/J4K;->APPS_STEP:LX/J4K;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->p:[LX/J4K;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2648819
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2648820
    new-instance v0, LX/J5l;

    invoke-direct {v0, p0}, LX/J5l;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->B:LX/63W;

    .line 2648821
    new-instance v0, LX/J5m;

    invoke-direct {v0, p0}, LX/J5m;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->C:Landroid/view/View$OnClickListener;

    .line 2648822
    new-instance v0, LX/J5n;

    invoke-direct {v0, p0}, LX/J5n;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->D:Landroid/view/View$OnClickListener;

    .line 2648823
    new-instance v0, LX/J5o;

    invoke-direct {v0, p0}, LX/J5o;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->E:Landroid/view/View$OnClickListener;

    .line 2648824
    new-instance v0, LX/J5p;

    invoke-direct {v0, p0}, LX/J5p;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->F:LX/0hc;

    .line 2648825
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2648826
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2648827
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->w:LX/0h5;

    .line 2648828
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->w:LX/0h5;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->B:LX/63W;

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2648829
    return-void
.end method

.method private a(LX/J45;LX/23P;LX/J3m;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2648689
    iput-object p1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    .line 2648690
    iput-object p2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->r:LX/23P;

    .line 2648691
    iput-object p3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->s:LX/J3m;

    .line 2648692
    iput-object p4, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->t:LX/0ad;

    .line 2648693
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;

    invoke-static {v3}, LX/J45;->a(LX/0QB;)LX/J45;

    move-result-object v0

    check-cast v0, LX/J45;

    invoke-static {v3}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v1

    check-cast v1, LX/23P;

    invoke-static {v3}, LX/J3m;->a(LX/0QB;)LX/J3m;

    move-result-object v2

    check-cast v2, LX/J3m;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->a(LX/J45;LX/23P;LX/J3m;LX/0ad;)V

    return-void
.end method

.method public static b(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V
    .locals 3

    .prologue
    .line 2648830
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->w:LX/0h5;

    if-nez v0, :cond_0

    .line 2648831
    :goto_0
    return-void

    .line 2648832
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    sget-object v1, LX/J5r;->CHECKUP_STEPS:LX/J5r;

    if-ne v0, v1, :cond_1

    .line 2648833
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->w:LX/0h5;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v:LX/J5y;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, LX/J5y;->f(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2648834
    :goto_1
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->l()Ljava/lang/String;

    move-result-object v1

    .line 2648835
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2648836
    move-object v0, v0

    .line 2648837
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->l()Ljava/lang/String;

    move-result-object v1

    .line 2648838
    iput-object v1, v0, LX/108;->j:Ljava/lang/String;

    .line 2648839
    move-object v0, v0

    .line 2648840
    const/4 v1, 0x0

    .line 2648841
    iput v1, v0, LX/108;->a:I

    .line 2648842
    move-object v0, v0

    .line 2648843
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2648844
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->w:LX/0h5;

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2648845
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->w:LX/0h5;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->D:Landroid/view/View$OnClickListener;

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2648846
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->w:LX/0h5;

    const v1, 0x7f0838d6

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    goto :goto_1
.end method

.method private l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2648847
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    sget-object v1, LX/J5r;->CONCLUSION:LX/J5r;

    if-ne v0, v1, :cond_0

    .line 2648848
    const v0, 0x7f080018

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2648849
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f080029

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private m()Z
    .locals 1

    .prologue
    .line 2648850
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 2648851
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v:LX/J5y;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2648808
    sget-object v0, LX/J5q;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    invoke-virtual {v1}, LX/J5r;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2648809
    :goto_0
    return-void

    .line 2648810
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomViewPager;->setVisibility(I)V

    .line 2648811
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2648812
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->r()V

    goto :goto_0

    .line 2648813
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomViewPager;->setVisibility(I)V

    .line 2648814
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2648815
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->s()V

    goto :goto_0

    .line 2648816
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomViewPager;->setVisibility(I)V

    .line 2648817
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static p(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V
    .locals 4

    .prologue
    .line 2648852
    sget-object v0, LX/J5q;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    invoke-virtual {v1}, LX/J5r;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2648853
    :goto_0
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->b(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    .line 2648854
    return-void

    .line 2648855
    :pswitch_0
    sget-object v0, LX/J5r;->CHECKUP_STEPS:LX/J5r;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    .line 2648856
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->o()V

    .line 2648857
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v:LX/J5y;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/J5y;->e(I)LX/J4K;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/J45;->a(LX/J4K;)V

    goto :goto_0

    .line 2648858
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    invoke-virtual {v0}, LX/J45;->b()V

    .line 2648859
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v:LX/J5y;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, LX/J5y;->e(I)LX/J4K;

    move-result-object v1

    .line 2648860
    sget-object v2, LX/J41;->b:[I

    invoke-virtual {v1}, LX/J4K;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 2648861
    :goto_1
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2648862
    sget-object v0, LX/J5r;->CONCLUSION:LX/J5r;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    .line 2648863
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->o()V

    .line 2648864
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    .line 2648865
    iget-object v1, v0, LX/J45;->e:LX/J49;

    sget-object v2, LX/5nj;->REVIEW_STEP_EXPOSED:LX/5nj;

    const-string v3, "navigation"

    invoke-virtual {v1, v2, v3}, LX/J49;->a(LX/5nj;Ljava/lang/String;)V

    .line 2648866
    goto :goto_0

    .line 2648867
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 2648868
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_REVIEW_STEP_CLOSE:LX/J5h;

    invoke-virtual {v0, v1}, LX/J45;->a(LX/J5h;)V

    .line 2648869
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->t(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    goto :goto_0

    .line 2648870
    :pswitch_3
    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_COMPOSER_STEP_NEXT:LX/J5h;

    invoke-virtual {v0, v2}, LX/J45;->a(LX/J5h;)V

    goto :goto_1

    .line 2648871
    :pswitch_4
    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_PROFILE_STEP_NEXT:LX/J5h;

    invoke-virtual {v0, v2}, LX/J45;->a(LX/J5h;)V

    goto :goto_1

    .line 2648872
    :pswitch_5
    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_NEXT:LX/J5h;

    invoke-virtual {v0, v2}, LX/J45;->a(LX/J5h;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static q(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V
    .locals 4

    .prologue
    .line 2648787
    sget-object v0, LX/J5q;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    invoke-virtual {v1}, LX/J5r;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2648788
    :goto_0
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->b(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    .line 2648789
    return-void

    .line 2648790
    :pswitch_0
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->t(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    goto :goto_0

    .line 2648791
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    invoke-virtual {v0}, LX/J45;->b()V

    .line 2648792
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v:LX/J5y;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, LX/J5y;->e(I)LX/J4K;

    move-result-object v1

    .line 2648793
    sget-object v2, LX/J41;->b:[I

    invoke-virtual {v1}, LX/J4K;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 2648794
    :goto_1
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2648795
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v()LX/J5r;

    move-result-object v0

    sget-object v1, LX/J5r;->INTRODUCTION:LX/J5r;

    if-ne v0, v1, :cond_0

    .line 2648796
    sget-object v0, LX/J5r;->INTRODUCTION:LX/J5r;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    .line 2648797
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->o()V

    .line 2648798
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    const-string v1, "navigation"

    invoke-virtual {v0, v1}, LX/J45;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2648799
    :cond_0
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->t(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    goto :goto_0

    .line 2648800
    :cond_1
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 2648801
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_REVIEW_STEP_PREVIOUS:LX/J5h;

    invoke-virtual {v0, v1}, LX/J45;->a(LX/J5h;)V

    .line 2648802
    sget-object v0, LX/J5r;->CHECKUP_STEPS:LX/J5r;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    .line 2648803
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v:LX/J5y;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v:LX/J5y;

    invoke-virtual {v2}, LX/0gG;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, LX/J5y;->e(I)LX/J4K;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/J45;->a(LX/J4K;)V

    .line 2648804
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->o()V

    goto :goto_0

    .line 2648805
    :pswitch_3
    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_COMPOSER_STEP_PREVIOUS:LX/J5h;

    invoke-virtual {v0, v2}, LX/J45;->a(LX/J5h;)V

    goto :goto_1

    .line 2648806
    :pswitch_4
    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_PROFILE_STEP_PREVIOUS:LX/J5h;

    invoke-virtual {v0, v2}, LX/J45;->a(LX/J5h;)V

    goto :goto_1

    .line 2648807
    :pswitch_5
    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_APP_STEP_PREVIOUS:LX/J5h;

    invoke-virtual {v0, v2}, LX/J45;->a(LX/J5h;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private r()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2648771
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d269c

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0838e9

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2648772
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d269d

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0838ea

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2648773
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d269e

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2648774
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d269f

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2648775
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d0525

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2648776
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->r:LX/23P;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2648777
    const v1, 0x7f08001a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2648778
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2648779
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d0526

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2648780
    const v1, 0x7f080018

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2648781
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->E:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2648782
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2648783
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d269b

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2648784
    const v1, 0x7f0214da

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2648785
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->fullScroll(I)Z

    .line 2648786
    return-void
.end method

.method private s()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2648754
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d269c

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0838eb

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2648755
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d269d

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0838ec

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2648756
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d269e

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2648757
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2648758
    const v1, 0x7f0838ee

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2648759
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d269f

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2648760
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d0525

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2648761
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->r:LX/23P;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2648762
    const v1, 0x7f080018

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2648763
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2648764
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d0526

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2648765
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2648766
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2648767
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const v1, 0x7f0d269b

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2648768
    const v1, 0x7f0214d9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2648769
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    const/16 v1, 0x21

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->fullScroll(I)Z

    .line 2648770
    return-void
.end method

.method public static t(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V
    .locals 5

    .prologue
    .line 2648744
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->A:Z

    .line 2648745
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    .line 2648746
    sget-object v2, LX/J41;->a:[I

    invoke-virtual {v1}, LX/J5r;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2648747
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "Exiting privacy checkup in one of the checkup steps"

    invoke-static {v2, v3}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2648748
    :goto_0
    iget-object v2, v0, LX/J45;->e:LX/J49;

    invoke-virtual {v2}, LX/J49;->a()V

    .line 2648749
    invoke-static {v0}, LX/J45;->g(LX/J45;)V

    .line 2648750
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->finish()V

    .line 2648751
    return-void

    .line 2648752
    :pswitch_0
    iget-object v2, v0, LX/J45;->e:LX/J49;

    sget-object v3, LX/5nj;->INTRO_STEP_CLOSED:LX/5nj;

    const-string v4, "navigation"

    invoke-virtual {v2, v3, v4}, LX/J49;->a(LX/5nj;Ljava/lang/String;)V

    goto :goto_0

    .line 2648753
    :pswitch_1
    iget-object v2, v0, LX/J45;->e:LX/J49;

    sget-object v3, LX/5nj;->REVIEW_STEP_CLOSED:LX/5nj;

    const-string v4, "navigation"

    invoke-virtual {v2, v3, v4}, LX/J49;->a(LX/5nj;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private u()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2648737
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->t:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-short v3, LX/J3i;->b:S

    invoke-interface {v1, v2, v3, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v1

    .line 2648738
    if-eqz v1, :cond_0

    const-string v1, "modal"

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->z:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2648739
    :cond_1
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->t:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-char v3, LX/J3i;->a:C

    const-string v4, ""

    invoke-interface {v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2648740
    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    .line 2648741
    iget-object v3, v2, LX/J45;->e:LX/J49;

    .line 2648742
    iput-object v1, v3, LX/J49;->s:Ljava/lang/String;

    .line 2648743
    return v0
.end method

.method private v()LX/J5r;
    .locals 1

    .prologue
    .line 2648734
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2648735
    sget-object v0, LX/J5r;->INTRODUCTION:LX/J5r;

    .line 2648736
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/J5r;->CHECKUP_STEPS:LX/J5r;

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2648704
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2648705
    invoke-static {p0, p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2648706
    iput-boolean v4, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->A:Z

    .line 2648707
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->z:Ljava/lang/String;

    .line 2648708
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2648709
    const-string v0, "unknown"

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->z:Ljava/lang/String;

    .line 2648710
    :cond_0
    if-eqz p1, :cond_4

    .line 2648711
    const-string v0, "privacy_checkup_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/J5r;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    .line 2648712
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    sget-object v1, LX/J5r;->INTRODUCTION:LX/J5r;

    if-ne v0, v1, :cond_1

    .line 2648713
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->s:LX/J3m;

    .line 2648714
    iget-object v1, v0, LX/J3m;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x160005

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2648715
    :cond_1
    const v0, 0x7f031013

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->setContentView(I)V

    .line 2648716
    const v0, 0x7f0d26a0

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewPager;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    .line 2648717
    new-instance v0, LX/J5y;

    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    sget-object v3, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->p:[LX/J4K;

    invoke-direct {v0, v1, v2, v3}, LX/J5y;-><init>(Landroid/content/res/Resources;LX/0gc;[LX/J4K;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v:LX/J5y;

    .line 2648718
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v:LX/J5y;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2648719
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->u:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->F:LX/0hc;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2648720
    const v0, 0x7f0d269a

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->x:Landroid/widget/ScrollView;

    .line 2648721
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->a()V

    .line 2648722
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->b(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    .line 2648723
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->o()V

    .line 2648724
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v()LX/J5r;

    move-result-object v1

    if-ne v0, v1, :cond_3

    .line 2648725
    if-nez p1, :cond_2

    .line 2648726
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v()LX/J5r;

    move-result-object v0

    sget-object v1, LX/J5r;->INTRODUCTION:LX/J5r;

    if-ne v0, v1, :cond_5

    .line 2648727
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/J45;->a(Ljava/lang/String;)V

    .line 2648728
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    sget-object v1, LX/J5r;->INTRODUCTION:LX/J5r;

    if-ne v0, v1, :cond_3

    .line 2648729
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->s:LX/J3m;

    .line 2648730
    iget-object v1, v0, LX/J3m;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x160005

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2648731
    :cond_3
    return-void

    .line 2648732
    :cond_4
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v()LX/J5r;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    goto/16 :goto_0

    .line 2648733
    :cond_5
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->v:LX/J5y;

    invoke-virtual {v1, v4}, LX/J5y;->e(I)LX/J4K;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/J45;->a(LX/J4K;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 2648702
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    .line 2648703
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2648699
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2648700
    const-string v0, "privacy_checkup_state"

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2648701
    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x372fd47f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2648694
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2648695
    iget-boolean v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->A:Z

    if-nez v1, :cond_0

    .line 2648696
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    .line 2648697
    iget-object v2, v1, LX/J45;->e:LX/J49;

    invoke-virtual {v2}, LX/J49;->a()V

    .line 2648698
    :cond_0
    const/16 v1, 0x23

    const v2, -0xbb08694

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
