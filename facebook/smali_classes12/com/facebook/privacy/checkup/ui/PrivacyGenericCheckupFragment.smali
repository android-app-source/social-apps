.class public Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:I

.field public c:Landroid/widget/ListView;

.field public d:LX/J3t;

.field public e:LX/1Ad;

.field public f:LX/1Kf;

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/J5k;

.field public i:LX/J6L;

.field public j:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

.field public final k:LX/J64;

.field public final l:Landroid/widget/AbsListView$OnScrollListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2649967
    const-class v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2650038
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2650039
    new-instance v0, LX/J6d;

    invoke-direct {v0, p0}, LX/J6d;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->k:LX/J64;

    .line 2650040
    new-instance v0, LX/J6f;

    invoke-direct {v0, p0}, LX/J6f;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->l:Landroid/widget/AbsListView$OnScrollListener;

    .line 2650041
    return-void
.end method

.method public static l(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2650037
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    invoke-virtual {v0, v1}, LX/J3t;->a(I)LX/J4L;

    move-result-object v0

    iget-object v0, v0, LX/J4L;->h:Ljava/lang/String;

    return-object v0
.end method

.method public static o(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2650036
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    invoke-virtual {v0, v1}, LX/J3t;->a(I)LX/J4L;

    move-result-object v0

    iget-object v0, v0, LX/J4L;->n:Ljava/lang/String;

    return-object v0
.end method

.method public static p(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2650035
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    invoke-virtual {v0, v1}, LX/J3t;->a(I)LX/J4L;

    move-result-object v0

    iget-object v0, v0, LX/J4L;->o:Ljava/lang/String;

    return-object v0
.end method

.method public static q(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2650034
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    invoke-virtual {v0, v1}, LX/J3t;->a(I)LX/J4L;

    move-result-object v0

    iget-object v0, v0, LX/J4L;->q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2650026
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2650027
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    const-class v3, LX/J6L;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/J6L;

    invoke-static {v0}, LX/J3t;->a(LX/0QB;)LX/J3t;

    move-result-object v4

    check-cast v4, LX/J3t;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v6

    check-cast v6, LX/1Kf;

    const/16 p1, 0x19c6

    invoke-static {v0, p1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/J5k;->b(LX/0QB;)LX/J5k;

    move-result-object v0

    check-cast v0, LX/J5k;

    iput-object v3, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->i:LX/J6L;

    iput-object v4, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iput-object v5, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->e:LX/1Ad;

    iput-object v6, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->f:LX/1Kf;

    iput-object p1, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->g:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->h:LX/J5k;

    .line 2650028
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2650029
    if-eqz v0, :cond_0

    .line 2650030
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2650031
    const-string v1, "stepIndex"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    .line 2650032
    return-void

    .line 2650033
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2650022
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2650023
    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2650024
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2650025
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7aae18be

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2650021
    const v1, 0x7f03101e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x307e2561

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2649968
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2649969
    const v0, 0x7f0d26aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->c:Landroid/widget/ListView;

    .line 2649970
    const/4 p2, 0x0

    .line 2649971
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031026

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2649972
    const v0, 0x7f0d04f9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2649973
    iget v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    move v1, v1

    .line 2649974
    iget-object p1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    invoke-virtual {p1}, LX/J3t;->c()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    if-ne v1, p1, :cond_0

    .line 2649975
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 2649976
    :cond_0
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget p1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    invoke-virtual {v1, p1}, LX/J3t;->a(I)LX/J4L;

    move-result-object v1

    iget-object v1, v1, LX/J4L;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2649977
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2649978
    const v0, 0x7f0d26ac

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2649979
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget p1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    invoke-virtual {v1, p1}, LX/J3t;->a(I)LX/J4L;

    move-result-object v1

    iget-object v1, v1, LX/J4L;->g:Ljava/lang/String;

    move-object v1, v1

    .line 2649980
    sget-boolean p1, LX/J3t;->j:Z

    move p1, p1

    .line 2649981
    if-nez p1, :cond_1

    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->q(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 2649982
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->q(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/String;

    move-result-object v1

    .line 2649983
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 2649984
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2649985
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    invoke-virtual {v0, v1}, LX/J3t;->a(I)LX/J4L;

    move-result-object v0

    iget-object v0, v0, LX/J4L;->e:Ljava/lang/String;

    move-object v1, v0

    .line 2649986
    if-eqz v1, :cond_2

    .line 2649987
    const v0, 0x7f0d14ab

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2649988
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 2649989
    invoke-static {p1}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v1

    .line 2649990
    iget-object p2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->e:LX/1Ad;

    invoke-virtual {p2, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object p2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 2649991
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2649992
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2649993
    :cond_2
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 2649994
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->l(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->l(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_7

    .line 2649995
    :cond_3
    :goto_1
    const/4 p1, 0x0

    .line 2649996
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03101c

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2649997
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->o(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->o(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2649998
    const v0, 0x7f0d26a7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2649999
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->o(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650000
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2650001
    const v0, 0x7f0d26a8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2650002
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->p(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->p(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2650003
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->p(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650004
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2650005
    :cond_4
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 2650006
    const v0, 0x7f0d032f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2650007
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    invoke-virtual {v1, v2}, LX/J3t;->a(I)LX/J4L;

    move-result-object v1

    iget-object v1, v1, LX/J4L;->p:LX/0Px;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    invoke-virtual {v1, v2}, LX/J3t;->a(I)LX/J4L;

    move-result-object v1

    iget-object v1, v1, LX/J4L;->p:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2650008
    const v1, 0x7f083902

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2650009
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2650010
    :cond_5
    new-instance v1, LX/J6g;

    invoke-direct {v1, p0}, LX/J6g;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2650011
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->i:LX/J6L;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->k:LX/J64;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    iget p1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->a:I

    invoke-virtual {v2, p1}, LX/J3t;->a(I)LX/J4L;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/J6L;->a(LX/J64;LX/J4L;)Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->j:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    .line 2650012
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->j:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2650013
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->l:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2650014
    return-void

    .line 2650015
    :cond_6
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650016
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2650017
    :cond_7
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031025

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->c:Landroid/widget/ListView;

    const/4 p1, 0x0

    invoke-virtual {v0, v1, v2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2650018
    const v0, 0x7f0d26a9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2650019
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->l(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2650020
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    goto/16 :goto_1
.end method
