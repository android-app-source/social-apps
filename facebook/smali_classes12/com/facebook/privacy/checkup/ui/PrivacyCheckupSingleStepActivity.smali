.class public final Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final p:Lcom/facebook/common/callercontext/CallerContext;

.field private static q:I

.field private static r:I


# instance fields
.field public A:Landroid/os/CountDownTimer;

.field public B:Ljava/lang/String;

.field private C:Landroid/widget/ListView;

.field public D:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

.field public E:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/1oT;",
            ">;"
        }
    .end annotation
.end field

.field public F:Landroid/view/View;

.field private G:Landroid/view/View;

.field public H:Landroid/view/View;

.field public I:Landroid/view/View;

.field public J:LX/J4L;

.field private K:J

.field private L:I

.field private M:I

.field private N:I

.field public final O:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "LX/J4L;",
            ">;"
        }
    .end annotation
.end field

.field private final P:Landroid/widget/AbsListView$OnScrollListener;

.field private final Q:LX/J64;

.field private final R:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private s:Landroid/content/Context;

.field public t:LX/J4E;

.field private u:LX/J6L;

.field private v:LX/J5k;

.field private w:LX/0SG;

.field private x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/0kL;

.field private z:LX/0W3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2649397
    const-class v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2649398
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2649399
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->K:J

    .line 2649400
    iput v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->L:I

    .line 2649401
    iput v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->M:I

    .line 2649402
    iput v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->N:I

    .line 2649403
    new-instance v0, LX/J62;

    invoke-direct {v0, p0}, LX/J62;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->O:LX/0Vd;

    .line 2649404
    new-instance v0, LX/J63;

    invoke-direct {v0, p0}, LX/J63;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->P:Landroid/widget/AbsListView$OnScrollListener;

    .line 2649405
    new-instance v0, LX/J65;

    invoke-direct {v0, p0}, LX/J65;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->Q:LX/J64;

    .line 2649406
    new-instance v0, LX/J66;

    invoke-direct {v0, p0}, LX/J66;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->R:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2649407
    const v0, 0x7f0d26a6

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->C:Landroid/widget/ListView;

    .line 2649408
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->m()V

    .line 2649409
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->b()V

    .line 2649410
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->u:LX/J6L;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->Q:LX/J64;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    invoke-virtual {v0, v1, v2}, LX/J6L;->a(LX/J64;LX/J4L;)Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->D:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    .line 2649411
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->C:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->D:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2649412
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->C:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->P:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2649413
    const v0, 0x7f0d26a5

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->F:Landroid/view/View;

    .line 2649414
    const v0, 0x7f0d175f

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->H:Landroid/view/View;

    .line 2649415
    const v0, 0x7f0d04fa

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->I:Landroid/view/View;

    .line 2649416
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->l()V

    .line 2649417
    return-void
.end method

.method private a(Landroid/content/Context;LX/J4E;LX/J6L;LX/J5k;LX/0SG;LX/0Ot;LX/0kL;LX/0W3;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/J4E;",
            "LX/J6L;",
            "LX/J5k;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0kL;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2649418
    iput-object p1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->s:Landroid/content/Context;

    .line 2649419
    iput-object p2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->t:LX/J4E;

    .line 2649420
    iput-object p3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->u:LX/J6L;

    .line 2649421
    iput-object p4, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->v:LX/J5k;

    .line 2649422
    iput-object p5, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->w:LX/0SG;

    .line 2649423
    iput-object p6, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->x:LX/0Ot;

    .line 2649424
    iput-object p7, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->y:LX/0kL;

    .line 2649425
    iput-object p8, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->z:LX/0W3;

    .line 2649426
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;

    const-class v1, Landroid/content/Context;

    invoke-interface {v8, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v8}, LX/J4E;->a(LX/0QB;)LX/J4E;

    move-result-object v2

    check-cast v2, LX/J4E;

    const-class v3, LX/J6L;

    invoke-interface {v8, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/J6L;

    invoke-static {v8}, LX/J5k;->b(LX/0QB;)LX/J5k;

    move-result-object v4

    check-cast v4, LX/J5k;

    invoke-static {v8}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const/16 v6, 0x259

    invoke-static {v8, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v8}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-static {v8}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v8

    check-cast v8, LX/0W3;

    invoke-direct/range {v0 .. v8}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->a(Landroid/content/Context;LX/J4E;LX/J6L;LX/J5k;LX/0SG;LX/0Ot;LX/0kL;LX/0W3;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;LX/J4L;)V
    .locals 1

    .prologue
    .line 2649456
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    invoke-virtual {v0, p1}, LX/J4L;->a(LX/J4L;)V

    .line 2649457
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->n()V

    .line 2649458
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->o()V

    .line 2649459
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->p()V

    .line 2649460
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->l()V

    .line 2649461
    return-void
.end method

.method public static a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;LX/J5h;Z)V
    .locals 10

    .prologue
    .line 2649427
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->v:LX/J5k;

    iget v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->L:I

    iget v3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->M:I

    iget v4, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->N:I

    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->w:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->K:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    :goto_0
    move-object v1, p1

    .line 2649428
    iget-object v6, v1, LX/J5h;->eventName:Ljava/lang/String;

    invoke-static {v6}, LX/J5k;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2649429
    const-string v7, "num_failures"

    invoke-virtual {v6, v7, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2649430
    const-string v7, "num_timeouts"

    invoke-virtual {v6, v7, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2649431
    const-string v7, "num_retries"

    invoke-virtual {v6, v7, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2649432
    if-eqz v5, :cond_0

    .line 2649433
    const-string v7, "round_trip_time"

    invoke-virtual {v6, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2649434
    :cond_0
    iget-object v7, v0, LX/J5k;->a:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2649435
    return-void

    .line 2649436
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;Landroid/support/v4/app/DialogFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2649437
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->t:LX/J4E;

    invoke-virtual {v0}, LX/J4E;->b()V

    .line 2649438
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2649439
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->M:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->M:I

    .line 2649440
    sget-object v0, LX/J5h;->PRIVACY_REVIEW_WRITE_TIMEOUT:LX/J5h;

    invoke-static {p0, v0, v2}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;LX/J5h;Z)V

    .line 2649441
    invoke-static {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->t(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)Z

    move-result v0

    .line 2649442
    if-nez v0, :cond_0

    .line 2649443
    new-instance v0, LX/0ju;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->s:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2649444
    invoke-virtual {v0, v2}, LX/0ju;->a(Z)LX/0ju;

    .line 2649445
    const v1, 0x7f0838f5

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    .line 2649446
    const v1, 0x7f080043

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/J60;

    invoke-direct {v2, p0}, LX/J60;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2649447
    const v1, 0x7f080018

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/J61;

    invoke-direct {v2, p0}, LX/J61;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2649448
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2649449
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2649450
    :cond_0
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2649451
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03101a

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->C:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->G:Landroid/view/View;

    .line 2649452
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->C:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->G:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 2649453
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->G:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2649454
    return-void
.end method

.method public static synthetic l(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)I
    .locals 2

    .prologue
    .line 2649455
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->L:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->L:I

    return v0
.end method

.method private l()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2649387
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v0, v0, LX/J4L;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2649388
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->H:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2649389
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->I:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2649390
    :goto_0
    return-void

    .line 2649391
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->H:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v1, v1, LX/J4L;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2649392
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->H:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->R:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2649393
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->H:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2649394
    const v0, 0x7f0d26a2

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2649395
    new-instance v1, LX/J67;

    invoke-direct {v1, p0}, LX/J67;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2649396
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 2649282
    const v0, 0x7f0d26a3

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2649283
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v1, v1, LX/J4L;->l:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f081305

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2649284
    new-instance v1, LX/J68;

    invoke-direct {v1, p0}, LX/J68;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2649285
    return-void

    .line 2649286
    :cond_0
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v1, v1, LX/J4L;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method private o()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2649293
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v0, v0, LX/J4L;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 2649294
    :goto_0
    const v0, 0x7f0d26a4

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2649295
    if-eqz v2, :cond_0

    .line 2649296
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v1, v1, LX/J4L;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2649297
    sget-object v4, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2649298
    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    sget-object v4, LX/1Up;->f:LX/1Up;

    invoke-virtual {v1, v4}, LX/1af;->a(LX/1Up;)V

    .line 2649299
    :cond_0
    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2649300
    return-void

    :cond_1
    move v2, v3

    .line 2649301
    goto :goto_0

    .line 2649302
    :cond_2
    const/16 v3, 0x8

    goto :goto_1
.end method

.method private p()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2649303
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->G:Landroid/view/View;

    if-nez v1, :cond_0

    .line 2649304
    :goto_0
    return-void

    .line 2649305
    :cond_0
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v1, v1, LX/J4L;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    move v2, v0

    .line 2649306
    :goto_1
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v1, v1, LX/J4L;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    move v5, v0

    .line 2649307
    :goto_2
    if-nez v2, :cond_3

    if-nez v5, :cond_3

    .line 2649308
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->G:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v2, v3

    .line 2649309
    goto :goto_1

    :cond_2
    move v5, v3

    .line 2649310
    goto :goto_2

    .line 2649311
    :cond_3
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->G:Landroid/view/View;

    const v1, 0x7f0d02c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2649312
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->G:Landroid/view/View;

    const v6, 0x7f0d0550

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2649313
    if-eqz v2, :cond_4

    .line 2649314
    iget-object v6, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v6, v6, LX/J4L;->f:Ljava/lang/String;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2649315
    :cond_4
    if-eqz v5, :cond_5

    .line 2649316
    iget-object v6, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v6, v6, LX/J4L;->g:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2649317
    :cond_5
    if-eqz v2, :cond_7

    move v2, v3

    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2649318
    if-eqz v5, :cond_6

    move v4, v3

    :cond_6
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2649319
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->G:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_7
    move v2, v4

    .line 2649320
    goto :goto_3
.end method

.method private q()LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2649321
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->E:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2649322
    const/4 v0, 0x0

    .line 2649323
    :goto_0
    return-object v0

    .line 2649324
    :cond_0
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2649325
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->E:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Map$Entry;

    .line 2649326
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2649327
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v1, v1, LX/J4L;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2649328
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->x:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v2, "privacy_checkup_single_step_manager_missing_privacy_edit"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not find key for object: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " in checkup data!"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2649329
    :cond_2
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    iget-object v1, v1, LX/J4L;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, LX/J4J;

    .line 2649330
    iget-object v1, v4, LX/J4J;->f:LX/8SR;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oT;

    invoke-virtual {v1, v0}, LX/8SR;->a(LX/1oT;)I

    move-result v0

    iget-object v1, v4, LX/J4J;->f:LX/8SR;

    iget-object v2, v4, LX/J4J;->f:LX/8SR;

    invoke-virtual {v2}, LX/8SR;->b()LX/1oT;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/8SR;->a(LX/1oT;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 2649331
    new-instance v0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;

    iget-object v1, v4, LX/J4J;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->w:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v4, v4, LX/J4J;->b:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1oT;

    invoke-interface {v5}, LX/1oT;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;-><init>(Ljava/lang/String;JLcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;Ljava/lang/String;)V

    .line 2649332
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_1

    .line 2649333
    :cond_3
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static r(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 2649334
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->q()LX/0Px;

    move-result-object v7

    .line 2649335
    if-eqz v7, :cond_0

    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2649336
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->finish()V

    .line 2649337
    :goto_0
    return-void

    .line 2649338
    :cond_1
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->u()V

    .line 2649339
    const v0, 0x7f081304

    const/4 v1, 0x0

    invoke-static {v0, v2, v2, v1}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v6

    .line 2649340
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2649341
    new-instance v0, LX/J69;

    sget v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->r:I

    int-to-long v2, v1

    sget v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->r:I

    int-to-long v4, v1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, LX/J69;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;JJLandroid/support/v4/app/DialogFragment;)V

    invoke-virtual {v0}, LX/J69;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->A:Landroid/os/CountDownTimer;

    .line 2649342
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->t:LX/J4E;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->B:Ljava/lang/String;

    new-instance v2, LX/J6A;

    invoke-direct {v2, p0, v6}, LX/J6A;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;Landroid/support/v4/app/DialogFragment;)V

    .line 2649343
    iget-object v3, v0, LX/J4E;->a:LX/1Ck;

    sget-object v4, LX/J4D;->SEND_PRIVACY_EDITS:LX/J4D;

    new-instance v5, LX/J4C;

    invoke-direct {v5, v0, v1, v7}, LX/J4C;-><init>(LX/J4E;Ljava/lang/String;LX/0Px;)V

    invoke-virtual {v3, v4, v5, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2649344
    goto :goto_0
.end method

.method private s()V
    .locals 3

    .prologue
    .line 2649345
    new-instance v0, LX/0ju;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->s:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2649346
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    .line 2649347
    const v1, 0x7f081306

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2649348
    const v1, 0x7f081307

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2649349
    const v1, 0x7f081308

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/J6B;

    invoke-direct {v2, p0}, LX/J6B;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2649350
    const v1, 0x7f081309

    invoke-virtual {p0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/J5z;

    invoke-direct {v2, p0}, LX/J5z;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2649351
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2649352
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2649353
    return-void
.end method

.method public static t(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;)Z
    .locals 3

    .prologue
    .line 2649354
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->L:I

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->M:I

    add-int/2addr v0, v1

    sget v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->q:I

    if-le v0, v1, :cond_0

    .line 2649355
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->y:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0838f4

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2649356
    const/4 v0, 0x1

    .line 2649357
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2649287
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->w:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->K:J

    .line 2649288
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->L:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->M:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->N:I

    if-nez v0, :cond_0

    .line 2649289
    sget-object v0, LX/J5h;->PRIVACY_REVIEW_WRITE_SENT:LX/J5h;

    invoke-static {p0, v0, v2}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;LX/J5h;Z)V

    .line 2649290
    :goto_0
    return-void

    .line 2649291
    :cond_0
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->N:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->N:I

    .line 2649292
    sget-object v0, LX/J5h;->PRIVACY_REVIEW_WRITE_RETRY:LX/J5h;

    invoke-static {p0, v0, v2}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;LX/J5h;Z)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2649358
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2649359
    invoke-static {p0, p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2649360
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->z:LX/0W3;

    sget-wide v2, LX/0X5;->gU:J

    const/4 v1, 0x3

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    sput v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->q:I

    .line 2649361
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->z:LX/0W3;

    sget-wide v2, LX/0X5;->gT:J

    const/16 v1, 0x2710

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    sput v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->r:I

    .line 2649362
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "checkup_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->B:Ljava/lang/String;

    .line 2649363
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->B:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "PrivacyCheckupSingleStepActivity started without type"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2649364
    const v0, 0x7f031019

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->setContentView(I)V

    .line 2649365
    new-instance v0, LX/J4L;

    sget-object v1, LX/J4K;->GENERIC_STEP:LX/J4K;

    invoke-direct {v0, v1}, LX/J4L;-><init>(LX/J4K;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    .line 2649366
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->E:Ljava/util/HashMap;

    .line 2649367
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->a()V

    .line 2649368
    return-void

    .line 2649369
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2649370
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->t:LX/J4E;

    .line 2649371
    iget-object v1, v0, LX/J4E;->a:LX/1Ck;

    sget-object v2, LX/J4D;->FETCH_REVIEW_DATA:LX/J4D;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2649372
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->t:LX/J4E;

    invoke-virtual {v0}, LX/J4E;->b()V

    .line 2649373
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->q()LX/0Px;

    move-result-object v0

    .line 2649374
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2649375
    :cond_0
    sget-object v0, LX/J5h;->PRIVACY_REVIEW_CANCEL:LX/J5h;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;LX/J5h;Z)V

    .line 2649376
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->finish()V

    .line 2649377
    :goto_0
    return-void

    .line 2649378
    :cond_1
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->s()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x22

    const v1, 0x55dd366d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2649379
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2649380
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->A:Landroid/os/CountDownTimer;

    if-eqz v1, :cond_0

    .line 2649381
    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->A:Landroid/os/CountDownTimer;

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->cancel()V

    .line 2649382
    :cond_0
    iput-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->A:Landroid/os/CountDownTimer;

    .line 2649383
    iput-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->D:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    .line 2649384
    iput-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->G:Landroid/view/View;

    .line 2649385
    iput-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupSingleStepActivity;->J:LX/J4L;

    .line 2649386
    const/16 v1, 0x23

    const v2, 0x3ce293f4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
