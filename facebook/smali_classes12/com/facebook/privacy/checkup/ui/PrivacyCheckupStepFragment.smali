.class public Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0wM;

.field public b:LX/J45;

.field public c:LX/J6L;

.field public d:LX/J3m;

.field public e:LX/J4K;

.field public f:Landroid/widget/ListView;

.field public g:Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;

.field public h:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

.field public final i:LX/J64;

.field public final j:Landroid/widget/AbsListView$OnScrollListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2649049
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2649050
    new-instance v0, LX/J6M;

    invoke-direct {v0, p0}, LX/J6M;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->i:LX/J64;

    .line 2649051
    new-instance v0, LX/J6N;

    invoke-direct {v0, p0}, LX/J6N;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->j:Landroid/widget/AbsListView$OnScrollListener;

    .line 2649052
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {p0}, LX/J45;->a(LX/0QB;)LX/J45;

    move-result-object v2

    check-cast v2, LX/J45;

    const-class v3, LX/J6L;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/J6L;

    invoke-static {p0}, LX/J3m;->a(LX/0QB;)LX/J3m;

    move-result-object p0

    check-cast p0, LX/J3m;

    iput-object v1, p1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->a:LX/0wM;

    iput-object v2, p1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    iput-object v3, p1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->c:LX/J6L;

    iput-object p0, p1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->d:LX/J3m;

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2649053
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2649054
    const-class v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    invoke-static {v0, p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2649055
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2649056
    const-string v1, "extra_privacy_checkup_step"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/J4K;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    .line 2649057
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    if-nez v0, :cond_0

    .line 2649058
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No PCU step provided, cannot create fragment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2649059
    :cond_0
    sget-object v0, LX/J6O;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {v1}, LX/J4K;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2649060
    :goto_0
    return-void

    .line 2649061
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->d:LX/J3m;

    .line 2649062
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object p0, LX/J3m;->a:LX/J3k;

    invoke-interface {v1, p0}, LX/11i;->a(LX/0Pq;)LX/11o;

    move-result-object v1

    const-string p0, "PrivacyCheckupComposerViewCreation"

    const p1, -0xc787e1

    invoke-static {v1, p0, p1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2649063
    goto :goto_0

    .line 2649064
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->d:LX/J3m;

    .line 2649065
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object p0, LX/J3m;->b:LX/J3l;

    invoke-interface {v1, p0}, LX/11i;->a(LX/0Pq;)LX/11o;

    move-result-object v1

    const-string p0, "PrivacyCheckupProfileViewCreation"

    const p1, 0x3cdfce46

    invoke-static {v1, p0, p1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2649066
    goto :goto_0

    .line 2649067
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->d:LX/J3m;

    .line 2649068
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object p0, LX/J3m;->c:LX/J3j;

    invoke-interface {v1, p0}, LX/11i;->a(LX/0Pq;)LX/11o;

    move-result-object v1

    const-string p0, "PrivacyCheckupAppsViewCreation"

    const p1, 0x3aa667f5

    invoke-static {v1, p0, p1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2649069
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b()V
    .locals 4

    .prologue
    .line 2649070
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->c:LX/J6L;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->i:LX/J64;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    iget-object v3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {v2, v3}, LX/J45;->b(LX/J4K;)LX/J4L;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/J6L;->a(LX/J64;LX/J4L;)Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->h:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    .line 2649071
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->h:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2649072
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->j:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2649073
    return-void
.end method

.method public c()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2649074
    sget-object v0, LX/J6O;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {v1}, LX/J4K;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2649075
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PrivacyCheckupStepFragment: Unhandled fetch for step: %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {v4}, LX/J4K;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2649076
    :goto_0
    return-void

    .line 2649077
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    new-instance v1, LX/J6P;

    invoke-direct {v1, p0, v2}, LX/J6P;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;Z)V

    .line 2649078
    sget-object v2, LX/J4K;->APPS_STEP:LX/J4K;

    invoke-virtual {v0, v2}, LX/J45;->b(LX/J4K;)LX/J4L;

    move-result-object v2

    .line 2649079
    iget-boolean v3, v2, LX/J4L;->k:Z

    if-nez v3, :cond_0

    .line 2649080
    :goto_1
    goto :goto_0

    .line 2649081
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    new-instance v1, LX/J6P;

    invoke-direct {v1, p0, v2}, LX/J6P;-><init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;Z)V

    .line 2649082
    sget-object v2, LX/J4K;->PROFILE_STEP:LX/J4K;

    invoke-virtual {v0, v2}, LX/J45;->b(LX/J4K;)LX/J4L;

    move-result-object v2

    .line 2649083
    iget-boolean v3, v2, LX/J4L;->k:Z

    if-nez v3, :cond_1

    .line 2649084
    :goto_2
    goto :goto_0

    .line 2649085
    :cond_0
    iget-object v3, v0, LX/J45;->b:LX/1Ck;

    sget-object v4, LX/J44;->FETCH_APP_INFO:LX/J44;

    new-instance p0, LX/J3y;

    invoke-direct {p0, v0, v2}, LX/J3y;-><init>(LX/J45;LX/J4L;)V

    new-instance v2, LX/J43;

    invoke-direct {v2, v0, v1}, LX/J43;-><init>(LX/J45;LX/J6P;)V

    invoke-virtual {v3, v4, p0, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_1

    .line 2649086
    :cond_1
    iget-object v3, v0, LX/J45;->b:LX/1Ck;

    sget-object v4, LX/J44;->FETCH_PROFILE_INFO:LX/J44;

    new-instance p0, LX/J40;

    invoke-direct {p0, v0, v2}, LX/J40;-><init>(LX/J45;LX/J4L;)V

    new-instance v2, LX/J43;

    invoke-direct {v2, v0, v1}, LX/J43;-><init>(LX/J45;LX/J6P;)V

    invoke-virtual {v3, v4, p0, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x202080c2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2649087
    const v1, 0x7f03101e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x77bb0f9d

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2649088
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2649089
    const v0, 0x7f0d26aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->f:Landroid/widget/ListView;

    .line 2649090
    const/4 p1, 0x0

    .line 2649091
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03101f

    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v1, v2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2649092
    const v0, 0x7f0d26ab

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->g:Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;

    .line 2649093
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->g:Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;

    const/4 v2, 0x0

    .line 2649094
    sget-object v3, LX/J6O;->a:[I

    iget-object p2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {p2}, LX/J4K;->ordinal()I

    move-result p2

    aget v3, v3, p2

    packed-switch v3, :pswitch_data_0

    .line 2649095
    const-class v3, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    const-string p2, "Unexpected step in PCM!"

    invoke-static {v3, p2}, LX/01m;->c(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2649096
    :goto_0
    :pswitch_0
    move v2, v2

    .line 2649097
    invoke-virtual {v0, v2}, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->setCurrentStep(I)V

    .line 2649098
    const v0, 0x7f0d04f9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2649099
    sget-object v2, LX/J6O;->a:[I

    iget-object v3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {v3}, LX/J4K;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 2649100
    const/4 v2, 0x0

    :goto_1
    move-object v2, v2

    .line 2649101
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2649102
    const v0, 0x7f0d26ac

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2649103
    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {v2}, LX/J4K;->ordinal()I

    .line 2649104
    const/4 v2, 0x0

    move-object v2, v2

    .line 2649105
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2649106
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2649107
    :goto_2
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 2649108
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2649109
    sget-object v0, LX/J6O;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {v1}, LX/J4K;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    .line 2649110
    const/4 v0, 0x0

    :goto_3
    move-object v1, v0

    .line 2649111
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2649112
    :goto_4
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b()V

    .line 2649113
    sget-object v0, LX/J6O;->a:[I

    iget-object v1, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {v1}, LX/J4K;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    .line 2649114
    :goto_5
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->c()V

    .line 2649115
    return-void

    .line 2649116
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->d:LX/J3m;

    .line 2649117
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object v2, LX/J3m;->a:LX/J3k;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 2649118
    if-eqz v1, :cond_0

    .line 2649119
    const-string v2, "PrivacyCheckupComposerViewCreation"

    const v3, -0x3c6ee211

    invoke-static {v1, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v1

    const-string v2, "PrivacyCheckupComposerDataFetch"

    const v3, -0x20e87f01

    invoke-static {v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2649120
    :cond_0
    goto :goto_5

    .line 2649121
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->d:LX/J3m;

    .line 2649122
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object v2, LX/J3m;->b:LX/J3l;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 2649123
    if-eqz v1, :cond_1

    .line 2649124
    const-string v2, "PrivacyCheckupProfileViewCreation"

    const v3, 0x110f3ec9    # 1.1300058E-28f

    invoke-static {v1, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v1

    const-string v2, "PrivacyCheckupProfileDataFetch"

    const v3, 0x1a856d87

    invoke-static {v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2649125
    :cond_1
    goto :goto_5

    .line 2649126
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->d:LX/J3m;

    .line 2649127
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object v2, LX/J3m;->c:LX/J3j;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 2649128
    if-eqz v1, :cond_2

    .line 2649129
    const-string v2, "PrivacyCheckupAppsViewCreation"

    const v3, 0x57f3be52

    invoke-static {v1, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v1

    const-string v2, "PrivacyCheckupAppsDataFetch"

    const v3, 0x31fa062f

    invoke-static {v1, v2, v3}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2649130
    :cond_2
    goto :goto_5

    .line 2649131
    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2649132
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2649133
    :pswitch_4
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2649134
    :pswitch_5
    const/4 v2, 0x2

    goto/16 :goto_0

    .line 2649135
    :pswitch_6
    const v2, 0x7f0838e0

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 2649136
    :pswitch_7
    const v2, 0x7f0838dd

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 2649137
    :pswitch_8
    const v2, 0x7f0838da

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 2649138
    :cond_4
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03101d

    iget-object v3, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->f:Landroid/widget/ListView;

    const/4 p1, 0x0

    invoke-virtual {v0, v2, v3, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2649139
    const v0, 0x7f0d26a9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2649140
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2649141
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    goto/16 :goto_4

    .line 2649142
    :pswitch_9
    const v0, 0x7f0838dc

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2649143
    :pswitch_a
    const v0, 0x7f0838de

    new-array v1, v2, [Ljava/lang/Object;

    const v2, 0x7f0838df

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2649144
    :pswitch_b
    const v0, 0x7f0838e1

    new-array v1, v2, [Ljava/lang/Object;

    const v2, 0x7f0838e2

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
