.class public Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/ShapeDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private k:Landroid/graphics/Paint;

.field private l:Landroid/graphics/Paint;

.field private m:I

.field private n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2650126
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2650127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2650123
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2650124
    invoke-direct {p0, p2}, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->a(Landroid/util/AttributeSet;)V

    .line 2650125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2650120
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2650121
    invoke-direct {p0, p2}, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->a(Landroid/util/AttributeSet;)V

    .line 2650122
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 2650109
    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->c:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->i:Ljava/util/List;

    .line 2650110
    iget-boolean v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->h:Z

    if-eqz v0, :cond_0

    .line 2650111
    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->d:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->j:Ljava/util/List;

    .line 2650112
    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->b:Landroid/content/res/Resources;

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2650113
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->c:I

    if-ge v0, v2, :cond_2

    .line 2650114
    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v3, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v3}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 2650115
    iget-object v3, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->i:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2650116
    iget-boolean v2, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->h:Z

    if-eqz v2, :cond_1

    .line 2650117
    iget-object v2, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->j:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->a:LX/0wM;

    const v4, 0x7f0207d6

    invoke-virtual {v3, v4, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2650118
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2650119
    :cond_2
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2650092
    const-class v0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;

    invoke-static {v0, p0}, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2650093
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->b:Landroid/content/res/Resources;

    const v1, 0x7f0a00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->m:I

    .line 2650094
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->b:Landroid/content/res/Resources;

    const v1, 0x7f0a00e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->n:I

    .line 2650095
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->k:Landroid/graphics/Paint;

    .line 2650096
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->k:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->m:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2650097
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->l:Landroid/graphics/Paint;

    .line 2650098
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->n:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2650099
    if-eqz p1, :cond_0

    .line 2650100
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->StepByStepCircleProgressBarView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2650101
    const/16 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->c:I

    .line 2650102
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->d:I

    .line 2650103
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    .line 2650104
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->g:I

    .line 2650105
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->f:I

    .line 2650106
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->h:Z

    .line 2650107
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2650108
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;LX/0wM;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 2650128
    iput-object p1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->a:LX/0wM;

    iput-object p2, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->b:Landroid/content/res/Resources;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0, v0, v1}, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->a(Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;LX/0wM;Landroid/content/res/Resources;)V

    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 2650068
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->a()V

    .line 2650069
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->g:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float v2, v0, v7

    .line 2650070
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->g:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    div-float v4, v0, v7

    .line 2650071
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->d:I

    if-lez v0, :cond_0

    .line 2650072
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    int-to-float v0, v0

    div-float v1, v0, v7

    .line 2650073
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    int-to-float v0, v0

    div-float/2addr v0, v7

    iget v3, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    iget v5, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->f:I

    add-int/2addr v3, v5

    iget v5, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->d:I

    mul-int/2addr v3, v5

    int-to-float v3, v3

    add-float/2addr v3, v0

    .line 2650074
    iget-object v5, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->k:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2650075
    :cond_0
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->d:I

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->c:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 2650076
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    int-to-float v0, v0

    div-float/2addr v0, v7

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    iget v3, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->f:I

    add-int/2addr v1, v3

    iget v3, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->d:I

    mul-int/2addr v1, v3

    int-to-float v1, v1

    add-float/2addr v1, v0

    .line 2650077
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    int-to-float v0, v0

    div-float/2addr v0, v7

    iget v3, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    iget v5, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->f:I

    add-int/2addr v3, v5

    iget v5, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->c:I

    add-int/lit8 v5, v5, -0x1

    mul-int/2addr v3, v5

    int-to-float v3, v3

    add-float/2addr v3, v0

    .line 2650078
    iget-object v5, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->l:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_1
    move v1, v6

    .line 2650079
    :goto_0
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->c:I

    if-ge v1, v0, :cond_4

    .line 2650080
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    iget v2, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->f:I

    add-int/2addr v0, v2

    mul-int v3, v1, v0

    .line 2650081
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/ShapeDrawable;

    .line 2650082
    iget v2, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    add-int/2addr v2, v3

    iget v4, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    invoke-virtual {v0, v3, v6, v2, v4}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 2650083
    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v4

    iget v2, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->d:I

    if-gt v1, v2, :cond_3

    iget v2, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->m:I

    :goto_1
    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2650084
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 2650085
    iget-boolean v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->h:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->d:I

    if-ge v1, v0, :cond_2

    .line 2650086
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 2650087
    iget v2, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    add-int/2addr v2, v3

    iget v4, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    invoke-virtual {v0, v3, v6, v2, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2650088
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2650089
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2650090
    :cond_3
    iget v2, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->n:I

    goto :goto_1

    .line 2650091
    :cond_4
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2650062
    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->f:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->c:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->f:I

    sub-int/2addr v0, v1

    .line 2650063
    iget v1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->e:I

    .line 2650064
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2650065
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2650066
    invoke-static {v0, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    invoke-static {v1, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->setMeasuredDimension(II)V

    .line 2650067
    return-void
.end method

.method public setCurrentStep(I)V
    .locals 2

    .prologue
    .line 2650054
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->c:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Current step must be between [0, mNumSteps-1], inclusive."

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2650055
    iput p1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->d:I

    .line 2650056
    return-void

    .line 2650057
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setNumSteps(I)V
    .locals 1

    .prologue
    .line 2650058
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2650059
    iput p1, p0, Lcom/facebook/privacy/checkup/ui/StepByStepCircleProgressBar;->c:I

    .line 2650060
    return-void

    .line 2650061
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
