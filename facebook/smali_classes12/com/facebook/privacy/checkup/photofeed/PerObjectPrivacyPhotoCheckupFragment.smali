.class public final Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;
.super Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;
.source ""

# interfaces
.implements LX/J4O;


# instance fields
.field private final A:LX/0g1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0g1",
            "<",
            "LX/J4j;",
            ">;"
        }
    .end annotation
.end field

.field private final B:LX/0g1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0g1",
            "<",
            "LX/1U6;",
            ">;"
        }
    .end annotation
.end field

.field public final C:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field private final D:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public a:LX/J4p;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/J5k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedHeaderPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedRootPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/J4i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/IC2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1DL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1QH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/J4j;

.field private o:LX/0g8;

.field private p:LX/1Qq;

.field public q:LX/1Qq;

.field private r:LX/62C;

.field private s:LX/1Jg;

.field private t:LX/2jf;

.field private u:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/1oT;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/lang/String;

.field private w:I

.field private x:Z

.field private y:LX/1oT;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2644360
    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;-><init>()V

    .line 2644361
    new-instance v0, LX/J4S;

    invoke-direct {v0, p0}, LX/J4S;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->A:LX/0g1;

    .line 2644362
    new-instance v0, LX/J4T;

    invoke-direct {v0, p0}, LX/J4T;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->B:LX/0g1;

    .line 2644363
    new-instance v0, LX/J4U;

    invoke-direct {v0, p0}, LX/J4U;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->C:LX/0Vd;

    .line 2644364
    new-instance v0, LX/J4V;

    invoke-direct {v0, p0}, LX/J4V;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->D:LX/0Vd;

    .line 2644365
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    invoke-static {p0}, LX/J4p;->a(LX/0QB;)LX/J4p;

    move-result-object v2

    check-cast v2, LX/J4p;

    invoke-static {p0}, LX/J5k;->b(LX/0QB;)LX/J5k;

    move-result-object v3

    check-cast v3, LX/J5k;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v5

    check-cast v5, LX/1Db;

    invoke-static {p0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v6

    check-cast v6, LX/1DS;

    const/16 v7, 0xfb9

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2fd2

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const-class v9, LX/J4i;

    invoke-interface {p0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/J4i;

    new-instance v11, LX/IC2;

    invoke-static {p0}, LX/1nH;->a(LX/0QB;)LX/1nH;

    move-result-object v10

    check-cast v10, LX/1nI;

    invoke-direct {v11, v10}, LX/IC2;-><init>(LX/1nI;)V

    move-object v10, v11

    check-cast v10, LX/IC2;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v11

    check-cast v11, LX/0kL;

    invoke-static {p0}, LX/1DL;->a(LX/0QB;)LX/1DL;

    move-result-object v12

    check-cast v12, LX/1DL;

    const-class v13, LX/1QH;

    invoke-interface {p0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/1QH;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    iput-object v2, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->a:LX/J4p;

    iput-object v3, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->b:LX/J5k;

    iput-object v4, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->c:LX/1Ck;

    iput-object v5, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->d:LX/1Db;

    iput-object v6, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->e:LX/1DS;

    iput-object v7, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->f:LX/0Ot;

    iput-object v8, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->g:LX/0Ot;

    iput-object v9, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->h:LX/J4i;

    iput-object v10, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->i:LX/IC2;

    iput-object v11, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->j:LX/0kL;

    iput-object v12, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->k:LX/1DL;

    iput-object v13, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->l:LX/1QH;

    iput-object p0, v1, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->m:LX/0W3;

    return-void
.end method

.method public static m(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V
    .locals 4

    .prologue
    .line 2644357
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b247d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2644358
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->c:LX/1Ck;

    sget-object v2, LX/J4a;->FETCH_PHOTOS:LX/J4a;

    new-instance v3, LX/J4X;

    invoke-direct {v3, p0, v0}, LX/J4X;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;I)V

    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->D:LX/0Vd;

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2644359
    return-void
.end method

.method private n()V
    .locals 9

    .prologue
    .line 2644338
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->k:LX/1DL;

    invoke-virtual {v0}, LX/1DL;->a()LX/1Jg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->s:LX/1Jg;

    .line 2644339
    new-instance v8, LX/J4Y;

    invoke-direct {v8, p0}, LX/J4Y;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V

    .line 2644340
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->h:LX/J4i;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    .line 2644341
    new-instance v4, LX/J4h;

    invoke-static {v0}, LX/CaG;->b(LX/0QB;)LX/CaG;

    move-result-object v3

    check-cast v3, LX/CaG;

    invoke-direct {v4, v3, v1, v2}, LX/J4h;-><init>(LX/CaG;Landroid/content/Context;LX/9hN;)V

    .line 2644342
    move-object v5, v4

    .line 2644343
    new-instance v0, LX/J4e;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment$8;

    invoke-direct {v2, p0}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment$8;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V

    .line 2644344
    sget-object v3, LX/J4g;->a:LX/J4g;

    move-object v3, v3

    .line 2644345
    iget-object v4, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->l:LX/1QH;

    iget-object v6, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->s:LX/1Jg;

    invoke-virtual {v4, v6}, LX/1QH;->a(LX/1Jg;)LX/1QW;

    move-result-object v4

    iget-object v6, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->i:LX/IC2;

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, LX/J4e;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PT;LX/1QW;LX/J4h;LX/IC2;LX/J4O;)V

    .line 2644346
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->e:LX/1DS;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->f:LX/0Ot;

    invoke-static {v2}, LX/5On;->a(LX/0Ot;)LX/0Ot;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->A:LX/0g1;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 2644347
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 2644348
    move-object v1, v1

    .line 2644349
    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->p:LX/1Qq;

    .line 2644350
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->e:LX/1DS;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->g:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->B:LX/0g1;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 2644351
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 2644352
    move-object v0, v1

    .line 2644353
    invoke-virtual {v0, v8}, LX/1Ql;->b(LX/99g;)LX/1Ql;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->q:LX/1Qq;

    .line 2644354
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->s:LX/1Jg;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->q:LX/1Qq;

    invoke-interface {v1}, LX/1Qr;->e()LX/1R4;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Jg;->a(LX/1R4;)V

    .line 2644355
    const/4 v0, 0x2

    new-array v0, v0, [LX/1Cw;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->p:LX/1Qq;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->q:LX/1Qq;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/62C;->a([LX/1Cw;)LX/62C;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->r:LX/62C;

    .line 2644356
    return-void
.end method

.method public static o(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)I
    .locals 2

    .prologue
    .line 2644331
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->n:LX/J4j;

    .line 2644332
    iget-object v1, v0, LX/J4j;->c:LX/0Px;

    move-object v0, v1

    .line 2644333
    if-nez v0, :cond_0

    .line 2644334
    const/4 v0, 0x0

    .line 2644335
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->n:LX/J4j;

    .line 2644336
    iget-object v1, v0, LX/J4j;->c:LX/0Px;

    move-object v0, v1

    .line 2644337
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method

.method public static p(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V
    .locals 1

    .prologue
    .line 2644328
    iget-boolean v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->z:Z

    if-nez v0, :cond_0

    .line 2644329
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->q:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2644330
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)LX/9hP;
    .locals 1

    .prologue
    .line 2644327
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 2644314
    invoke-super {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(Landroid/os/Bundle;)V

    .line 2644315
    const-class v0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    invoke-static {v0, p0}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2644316
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2644317
    const-string v1, "checkup_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2644318
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v7

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2644319
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2644320
    const-string v2, "checkup_source"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2644321
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2644322
    const-string v2, "unknown"

    .line 2644323
    :cond_0
    new-instance v0, LX/J4j;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v7}, LX/J4j;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/1Fb;LX/1Fb;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->n:LX/J4j;

    .line 2644324
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->n()V

    .line 2644325
    return-void

    .line 2644326
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/1oT;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2644298
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->n:LX/J4j;

    .line 2644299
    iget-object v1, v0, LX/J4j;->d:Ljava/util/Map;

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2644300
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->u:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2644301
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->a:LX/J4p;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->u:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->C:LX/0Vd;

    invoke-virtual {v0, v1, v2}, LX/J4p;->a(Ljava/util/Map;LX/0Vd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2644302
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->u:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2644303
    :cond_0
    invoke-interface {p2}, LX/1oT;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->v:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2644304
    iget v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->w:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->w:I

    .line 2644305
    :goto_0
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->m:LX/0W3;

    sget-wide v2, LX/0X5;->gV:J

    const/16 v1, 0x3e8

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    .line 2644306
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->m:LX/0W3;

    sget-wide v2, LX/0X5;->gW:J

    const/16 v4, 0x7d0

    invoke-interface {v1, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v1

    .line 2644307
    invoke-static {p0}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->o(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)I

    move-result v2

    if-lt v2, v1, :cond_1

    iget v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->w:I

    if-ne v1, v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->x:Z

    if-nez v0, :cond_1

    .line 2644308
    iput-boolean v5, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->x:Z

    .line 2644309
    const/4 v0, 0x0

    const/4 p1, 0x1

    const/4 v5, 0x0

    .line 2644310
    new-instance v1, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0838fc

    new-array v3, p1, [Ljava/lang/Object;

    invoke-interface {p2}, LX/1oT;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f0838fd

    new-array v3, p1, [Ljava/lang/Object;

    invoke-interface {p2}, LX/1oT;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f0838fe

    new-instance v3, LX/J4R;

    invoke-direct {v3, p0, v0, p2}, LX/J4R;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;Ljava/lang/String;LX/1oT;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f0838ff

    new-instance v3, LX/J4Z;

    invoke-direct {v3, p0}, LX/J4Z;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2644311
    :cond_1
    return-void

    .line 2644312
    :cond_2
    invoke-interface {p2}, LX/1oT;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->v:Ljava/lang/String;

    .line 2644313
    iput v5, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->w:I

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2644294
    iput-boolean p1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->z:Z

    .line 2644295
    if-nez p1, :cond_0

    .line 2644296
    invoke-static {p0}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->p(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V

    .line 2644297
    :cond_0
    return-void
.end method

.method public final a(LX/9eC;)Z
    .locals 1

    .prologue
    .line 2644366
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2644293
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2644292
    sget-object v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final g_(Ljava/lang/String;)LX/1oT;
    .locals 1

    .prologue
    .line 2644289
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->n:LX/J4j;

    .line 2644290
    iget-object p0, v0, LX/J4j;->d:Ljava/util/Map;

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1oT;

    move-object v0, p0

    .line 2644291
    return-object v0
.end method

.method public final mN_()LX/1oT;
    .locals 1

    .prologue
    .line 2644288
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->y:LX/1oT;

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2644283
    invoke-super {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2644284
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->q:LX/1Qq;

    if-eqz v0, :cond_0

    .line 2644285
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->q:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 2644286
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->z:Z

    .line 2644287
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x548065de

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2644282
    const v1, 0x7f030fe9

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x66cb2c02

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x381fd6cc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2644269
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->c:LX/1Ck;

    sget-object v2, LX/J4a;->FETCH_PHOTOS:LX/J4a;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2644270
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->u:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2644271
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->a:LX/J4p;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->u:Ljava/util/HashMap;

    invoke-virtual {v1, v2, v3}, LX/J4p;->a(Ljava/util/Map;LX/0Vd;)Z

    .line 2644272
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->u:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 2644273
    :cond_0
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onDestroyView()V

    .line 2644274
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->q:LX/1Qq;

    if-eqz v1, :cond_1

    .line 2644275
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->q:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2644276
    iput-object v3, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->q:LX/1Qq;

    .line 2644277
    :cond_1
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->p:LX/1Qq;

    if-eqz v1, :cond_2

    .line 2644278
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->p:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2644279
    iput-object v3, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->p:LX/1Qq;

    .line 2644280
    :cond_2
    iput-object v3, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->r:LX/62C;

    .line 2644281
    const/16 v1, 0x2b

    const v2, -0x4e7d1c19

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x59df0fa5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2644264
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onPause()V

    .line 2644265
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->s:LX/1Jg;

    invoke-interface {v1}, LX/1Jg;->b()V

    .line 2644266
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->o:LX/0g8;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->s:LX/1Jg;

    invoke-interface {v2}, LX/1Jg;->a()LX/0fx;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 2644267
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->o:LX/0g8;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->t:LX/2jf;

    invoke-interface {v1, v2}, LX/0g8;->c(LX/0fx;)V

    .line 2644268
    const/16 v1, 0x2b

    const v2, 0x5a9a9057

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1a0dbee2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2644259
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onResume()V

    .line 2644260
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->s:LX/1Jg;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->o:LX/0g8;

    invoke-interface {v1, v2}, LX/1Jg;->a(LX/0g8;)V

    .line 2644261
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->o:LX/0g8;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->s:LX/1Jg;

    invoke-interface {v2}, LX/1Jg;->a()LX/0fx;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0g8;->b(LX/0fx;)V

    .line 2644262
    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->o:LX/0g8;

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->t:LX/2jf;

    invoke-interface {v1, v2}, LX/0g8;->b(LX/0fx;)V

    .line 2644263
    const/16 v1, 0x2b

    const v2, -0x4e1980ec

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2644251
    invoke-super {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2644252
    const-string v0, "pending_privacy_edits"

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->u:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2644253
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2644254
    const-string v0, "last_selected_privacy"

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2644255
    :cond_0
    const-string v0, "has_prompted_bulk_edit"

    iget-boolean v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2644256
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->y:LX/1oT;

    if-eqz v0, :cond_1

    .line 2644257
    const-string v0, "locked_privacy"

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->y:LX/1oT;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2644258
    :cond_1
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2644231
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->u:Ljava/util/HashMap;

    .line 2644232
    iput-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->v:Ljava/lang/String;

    .line 2644233
    iput v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->w:I

    .line 2644234
    iput-boolean v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->x:Z

    .line 2644235
    iput-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->y:LX/1oT;

    .line 2644236
    if-eqz p2, :cond_1

    .line 2644237
    const-string v0, "pending_privacy_edits"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2644238
    const-string v0, "pending_privacy_edits"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->u:Ljava/util/HashMap;

    .line 2644239
    :cond_0
    const-string v0, "last_selected_privacy"

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->v:Ljava/lang/String;

    .line 2644240
    const-string v0, "has_prompted_bulk_edit"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->x:Z

    .line 2644241
    const-string v0, "locked_privacy"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2644242
    const-string v0, "locked_privacy"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oT;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->y:LX/1oT;

    .line 2644243
    :cond_1
    const v0, 0x7f0d265c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2644244
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2644245
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->o:LX/0g8;

    .line 2644246
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->o:LX/0g8;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->r:LX/62C;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2644247
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->o:LX/0g8;

    iget-object v1, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->d:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(LX/1St;)V

    .line 2644248
    new-instance v0, LX/J4W;

    invoke-direct {v0, p0}, LX/J4W;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->t:LX/2jf;

    .line 2644249
    invoke-static {p0}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;->m(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;)V

    .line 2644250
    return-void
.end method
