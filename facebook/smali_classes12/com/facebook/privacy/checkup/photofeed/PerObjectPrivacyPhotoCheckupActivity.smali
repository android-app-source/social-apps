.class public final Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2644119
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2644120
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2644121
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2644122
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2644123
    const v1, 0x7f0838f6

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 2644124
    new-instance v1, LX/J4P;

    invoke-direct {v1, p0}, LX/J4P;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2644125
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080031

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2644126
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2644127
    move-object v1, v1

    .line 2644128
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080031

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2644129
    iput-object v2, v1, LX/108;->j:Ljava/lang/String;

    .line 2644130
    move-object v1, v1

    .line 2644131
    const/4 v2, 0x1

    .line 2644132
    iput-boolean v2, v1, LX/108;->d:Z

    .line 2644133
    move-object v1, v1

    .line 2644134
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2644135
    new-instance v2, LX/J4Q;

    invoke-direct {v2, p0}, LX/J4Q;-><init>(Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;)V

    invoke-interface {v0, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2644136
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2644137
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2644138
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2644139
    const v0, 0x7f030f3c

    invoke-virtual {p0, v0}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;->setContentView(I)V

    .line 2644140
    invoke-direct {p0}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;->a()V

    .line 2644141
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;->p:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    .line 2644142
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;->p:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    if-nez v0, :cond_0

    .line 2644143
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2644144
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2644145
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2644146
    new-instance v2, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    invoke-direct {v2}, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;-><init>()V

    .line 2644147
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2644148
    move-object v0, v2

    .line 2644149
    check-cast v0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;->p:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    .line 2644150
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupActivity;->p:Lcom/facebook/privacy/checkup/photofeed/PerObjectPrivacyPhotoCheckupFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2644151
    :cond_0
    return-void
.end method
