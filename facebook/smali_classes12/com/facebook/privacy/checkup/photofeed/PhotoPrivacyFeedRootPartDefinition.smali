.class public Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "LX/J4e;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/1T5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1T5",
            "<",
            "LX/J4e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedContentRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2644443
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2644444
    invoke-static {}, LX/1T5;->a()LX/1T5;

    move-result-object v0

    const-class v1, LX/1U6;

    invoke-virtual {v0, v1, p1}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;

    invoke-virtual {v0, v1, p2}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/api/feed/data/EndOfFeedSentinel$EndOfFeedSentinelFeedUnit;

    .line 2644445
    move-object v2, p4

    .line 2644446
    invoke-virtual {v0, v1, v2}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Ljava/lang/Object;

    invoke-virtual {v0, v1, p3}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedRootPartDefinition;->a:LX/1T5;

    .line 2644447
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedRootPartDefinition;
    .locals 7

    .prologue
    .line 2644429
    const-class v1, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedRootPartDefinition;

    monitor-enter v1

    .line 2644430
    :try_start_0
    sget-object v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedRootPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2644431
    sput-object v2, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedRootPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2644432
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2644433
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2644434
    new-instance v3, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedRootPartDefinition;

    const/16 v4, 0xfb8

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1d1d

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x6fa

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x6f3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedRootPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2644435
    move-object v0, v3

    .line 2644436
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2644437
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2644438
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2644439
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2644441
    iget-object v0, p0, Lcom/facebook/privacy/checkup/photofeed/PhotoPrivacyFeedRootPartDefinition;->a:LX/1T5;

    invoke-virtual {v0, p1, p2}, LX/1T5;->a(LX/1RF;Ljava/lang/Object;)Z

    .line 2644442
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2644440
    const/4 v0, 0x1

    return v0
.end method
