.class public final Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2564393
    const-class v0, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;

    new-instance v1, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2564394
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2564395
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2564396
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2564397
    const/4 v2, 0x0

    .line 2564398
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2564399
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564400
    :goto_0
    move v1, v2

    .line 2564401
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2564402
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2564403
    new-instance v1, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;

    invoke-direct {v1}, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;-><init>()V

    .line 2564404
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2564405
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2564406
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2564407
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2564408
    :cond_0
    return-object v1

    .line 2564409
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564410
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2564411
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2564412
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2564413
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2564414
    const-string v4, "geocode_address_data"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2564415
    const/4 v3, 0x0

    .line 2564416
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2564417
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564418
    :goto_2
    move v1, v3

    .line 2564419
    goto :goto_1

    .line 2564420
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2564421
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2564422
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2564423
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564424
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 2564425
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2564426
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2564427
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2564428
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2564429
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2564430
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 2564431
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2564432
    const/4 v5, 0x0

    .line 2564433
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 2564434
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564435
    :goto_5
    move v4, v5

    .line 2564436
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2564437
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2564438
    goto :goto_3

    .line 2564439
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2564440
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2564441
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 2564442
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564443
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_c

    .line 2564444
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2564445
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2564446
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_b

    if-eqz v6, :cond_b

    .line 2564447
    const-string v7, "node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2564448
    const/4 v6, 0x0

    .line 2564449
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_14

    .line 2564450
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564451
    :goto_7
    move v4, v6

    .line 2564452
    goto :goto_6

    .line 2564453
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2564454
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 2564455
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_6

    .line 2564456
    :cond_e
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564457
    :cond_f
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_13

    .line 2564458
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2564459
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2564460
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_f

    if-eqz v10, :cond_f

    .line 2564461
    const-string p0, "address"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_10

    .line 2564462
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_8

    .line 2564463
    :cond_10
    const-string p0, "city"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_11

    .line 2564464
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_8

    .line 2564465
    :cond_11
    const-string p0, "latitude"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_12

    .line 2564466
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_8

    .line 2564467
    :cond_12
    const-string p0, "longitude"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 2564468
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_8

    .line 2564469
    :cond_13
    const/4 v10, 0x4

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 2564470
    invoke-virtual {v0, v6, v9}, LX/186;->b(II)V

    .line 2564471
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v8}, LX/186;->b(II)V

    .line 2564472
    const/4 v6, 0x2

    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 2564473
    const/4 v6, 0x3

    invoke-virtual {v0, v6, v4}, LX/186;->b(II)V

    .line 2564474
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_7

    :cond_14
    move v4, v6

    move v7, v6

    move v8, v6

    move v9, v6

    goto :goto_8
.end method
