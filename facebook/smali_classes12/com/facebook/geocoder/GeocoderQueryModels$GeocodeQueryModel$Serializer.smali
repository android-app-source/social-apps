.class public final Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2564475
    const-class v0, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;

    new-instance v1, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2564476
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2564477
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2564478
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2564479
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2564480
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2564481
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2564482
    if-eqz v2, :cond_7

    .line 2564483
    const-string v3, "geocode_address_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2564484
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2564485
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2564486
    if-eqz v3, :cond_6

    .line 2564487
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2564488
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2564489
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_5

    .line 2564490
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 2564491
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2564492
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2564493
    if-eqz v0, :cond_4

    .line 2564494
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2564495
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2564496
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2564497
    if-eqz v2, :cond_0

    .line 2564498
    const-string p0, "address"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2564499
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2564500
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2564501
    if-eqz v2, :cond_1

    .line 2564502
    const-string p0, "city"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2564503
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2564504
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2564505
    if-eqz v2, :cond_2

    .line 2564506
    const-string p0, "latitude"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2564507
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2564508
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2564509
    if-eqz v2, :cond_3

    .line 2564510
    const-string p0, "longitude"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2564511
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2564512
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2564513
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2564514
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2564515
    :cond_5
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2564516
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2564517
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2564518
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2564519
    check-cast p1, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel$Serializer;->a(Lcom/facebook/geocoder/GeocoderQueryModels$GeocodeQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
