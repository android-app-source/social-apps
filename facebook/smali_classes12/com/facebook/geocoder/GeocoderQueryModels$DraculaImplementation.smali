.class public final Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2564315
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2564316
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2564317
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2564318
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2564319
    if-nez p1, :cond_0

    .line 2564320
    :goto_0
    return v0

    .line 2564321
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2564322
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2564323
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2564324
    const v2, 0x6c4e2f62

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 2564325
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2564326
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2564327
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2564328
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2564329
    const v2, 0x2deb19be

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2564330
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2564331
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2564332
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2564333
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2564334
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2564335
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2564336
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2564337
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2564338
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2564339
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2564340
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2564341
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2564342
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2564343
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2564344
    invoke-virtual {p3, v7, v3}, LX/186;->b(II)V

    .line 2564345
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 2564346
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2564347
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2564348
    const v2, 0x4b895c84    # 1.8004232E7f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v1

    .line 2564349
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2564350
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2564351
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2564352
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2564353
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2564354
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2564355
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2564356
    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 2564357
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2564358
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 2564359
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2aaceace -> :sswitch_0
        0x2deb19be -> :sswitch_2
        0x4b895c84 -> :sswitch_4
        0x5479bec8 -> :sswitch_3
        0x6c4e2f62 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2564360
    new-instance v0, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2564361
    sparse-switch p2, :sswitch_data_0

    .line 2564362
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2564363
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2564364
    const v1, 0x6c4e2f62

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2564365
    :goto_0
    :sswitch_1
    return-void

    .line 2564366
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2564367
    const v1, 0x2deb19be

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 2564368
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2564369
    const v1, 0x4b895c84    # 1.8004232E7f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2aaceace -> :sswitch_0
        0x2deb19be -> :sswitch_1
        0x4b895c84 -> :sswitch_1
        0x5479bec8 -> :sswitch_3
        0x6c4e2f62 -> :sswitch_2
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2564370
    if-nez p1, :cond_0

    move v0, v1

    .line 2564371
    :goto_0
    return v0

    .line 2564372
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 2564373
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2564374
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2564375
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 2564376
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 2564377
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2564378
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2564379
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2564380
    if-eqz p1, :cond_0

    .line 2564381
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 2564382
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2564383
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 2564384
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2564385
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2564386
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2564387
    if-eqz p1, :cond_0

    .line 2564388
    invoke-static {p0, p1, p2}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2564389
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;

    .line 2564390
    if-eq v0, v1, :cond_0

    .line 2564391
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2564392
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2564312
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2564313
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2564314
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2564307
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2564308
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2564309
    :cond_0
    iput-object p1, p0, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2564310
    iput p2, p0, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->b:I

    .line 2564311
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2564306
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2564305
    new-instance v0, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2564302
    iget v0, p0, LX/1vt;->c:I

    .line 2564303
    move v0, v0

    .line 2564304
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2564299
    iget v0, p0, LX/1vt;->c:I

    .line 2564300
    move v0, v0

    .line 2564301
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2564296
    iget v0, p0, LX/1vt;->b:I

    .line 2564297
    move v0, v0

    .line 2564298
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2564293
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2564294
    move-object v0, v0

    .line 2564295
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2564284
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2564285
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2564286
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2564287
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2564288
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2564289
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2564290
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2564291
    invoke-static {v3, v9, v2}, Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/geocoder/GeocoderQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2564292
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2564281
    iget v0, p0, LX/1vt;->c:I

    .line 2564282
    move v0, v0

    .line 2564283
    return v0
.end method
