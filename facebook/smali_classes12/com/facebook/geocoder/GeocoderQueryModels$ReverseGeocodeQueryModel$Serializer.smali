.class public final Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2564642
    const-class v0, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;

    new-instance v1, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2564643
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2564641
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2564613
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2564614
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2564615
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2564616
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2564617
    if-eqz v2, :cond_4

    .line 2564618
    const-string v3, "reverse_geocode_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2564619
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2564620
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2564621
    if-eqz v3, :cond_3

    .line 2564622
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2564623
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2564624
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_2

    .line 2564625
    invoke-virtual {v1, v3, p0}, LX/15i;->q(II)I

    move-result v0

    .line 2564626
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2564627
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2564628
    if-eqz v2, :cond_0

    .line 2564629
    const-string p2, "address"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2564630
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2564631
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2564632
    if-eqz v2, :cond_1

    .line 2564633
    const-string p2, "city"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2564634
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2564635
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2564636
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 2564637
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2564638
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2564639
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2564640
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2564612
    check-cast p1, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel$Serializer;->a(Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
