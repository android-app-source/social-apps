.class public final Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2564550
    const-class v0, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;

    new-instance v1, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2564551
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2564552
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2564553
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2564554
    const/4 v2, 0x0

    .line 2564555
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2564556
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564557
    :goto_0
    move v1, v2

    .line 2564558
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2564559
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2564560
    new-instance v1, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;

    invoke-direct {v1}, Lcom/facebook/geocoder/GeocoderQueryModels$ReverseGeocodeQueryModel;-><init>()V

    .line 2564561
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2564562
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2564563
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2564564
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2564565
    :cond_0
    return-object v1

    .line 2564566
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564567
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2564568
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2564569
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2564570
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2564571
    const-string v4, "reverse_geocode_data"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2564572
    const/4 v3, 0x0

    .line 2564573
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2564574
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564575
    :goto_2
    move v1, v3

    .line 2564576
    goto :goto_1

    .line 2564577
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2564578
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2564579
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2564580
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564581
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 2564582
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2564583
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2564584
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2564585
    const-string v5, "nodes"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2564586
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2564587
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 2564588
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2564589
    const/4 v5, 0x0

    .line 2564590
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_e

    .line 2564591
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564592
    :goto_5
    move v4, v5

    .line 2564593
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2564594
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2564595
    goto :goto_3

    .line 2564596
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2564597
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2564598
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 2564599
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2564600
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_d

    .line 2564601
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2564602
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2564603
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v7, :cond_b

    .line 2564604
    const-string p0, "address"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 2564605
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_6

    .line 2564606
    :cond_c
    const-string p0, "city"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2564607
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_6

    .line 2564608
    :cond_d
    const/4 v7, 0x2

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2564609
    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 2564610
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 2564611
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_e
    move v4, v5

    move v6, v5

    goto :goto_6
.end method
