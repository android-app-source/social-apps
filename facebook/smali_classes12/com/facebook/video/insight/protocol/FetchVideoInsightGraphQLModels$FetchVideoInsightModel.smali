.class public final Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x578b17ef
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:I

.field private j:I

.field private k:I

.field private l:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2665481
    const-class v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2665480
    const-class v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2665478
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2665479
    return-void
.end method

.method private o()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2665475
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2665476
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2665477
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 2665458
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2665459
    invoke-direct {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->o()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2665460
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->j()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2665461
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->m()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2665462
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->n()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2665463
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2665464
    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 2665465
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2665466
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2665467
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2665468
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->i:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 2665469
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->j:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 2665470
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->k:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 2665471
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2665472
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2665473
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2665474
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()J
    .locals 2

    .prologue
    .line 2665456
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2665457
    iget-wide v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->f:J

    return-wide v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2665438
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2665439
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->j()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2665440
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->j()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    .line 2665441
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->j()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2665442
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;

    .line 2665443
    iput-object v0, v1, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->g:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    .line 2665444
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->m()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2665445
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->m()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    .line 2665446
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->m()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2665447
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;

    .line 2665448
    iput-object v0, v1, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->l:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    .line 2665449
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->n()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2665450
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->n()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    .line 2665451
    invoke-virtual {p0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->n()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2665452
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;

    .line 2665453
    iput-object v0, v1, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->m:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    .line 2665454
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2665455
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2665437
    new-instance v0, LX/JE7;

    invoke-direct {v0, p1}, LX/JE7;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2665482
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2665483
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->f:J

    .line 2665484
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->h:Z

    .line 2665485
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->i:I

    .line 2665486
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->j:I

    .line 2665487
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v4}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->k:I

    .line 2665488
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2665435
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2665436
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2665434
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2665431
    new-instance v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;

    invoke-direct {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;-><init>()V

    .line 2665432
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2665433
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2665430
    const v0, 0x474c8d9d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2665429
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2665427
    iget-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->g:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    iput-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->g:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    .line 2665428
    iget-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->g:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 2665425
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2665426
    iget-boolean v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->h:Z

    return v0
.end method

.method public final l()I
    .locals 2

    .prologue
    .line 2665423
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2665424
    iget v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->k:I

    return v0
.end method

.method public final m()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2665421
    iget-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->l:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->l:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    .line 2665422
    iget-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->l:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    return-object v0
.end method

.method public final n()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2665419
    iget-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->m:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    iput-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->m:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    .line 2665420
    iget-object v0, p0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->m:Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    return-object v0
.end method
