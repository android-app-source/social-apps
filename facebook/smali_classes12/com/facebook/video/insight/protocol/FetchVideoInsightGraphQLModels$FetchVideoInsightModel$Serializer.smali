.class public final Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2665162
    const-class v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;

    new-instance v1, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2665163
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2665164
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 2665165
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2665166
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 2665167
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2665168
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 2665169
    if-eqz v2, :cond_0

    .line 2665170
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2665171
    invoke-static {v1, v0, v5, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2665172
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2665173
    cmp-long v4, v2, v6

    if-eqz v4, :cond_1

    .line 2665174
    const-string v4, "created_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2665175
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 2665176
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2665177
    if-eqz v2, :cond_2

    .line 2665178
    const-string v3, "creation_story"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2665179
    invoke-static {v1, v2, p1, p2}, LX/JEB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2665180
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 2665181
    if-eqz v2, :cond_3

    .line 2665182
    const-string v3, "is_video_broadcast"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2665183
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 2665184
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 2665185
    if-eqz v2, :cond_4

    .line 2665186
    const-string v3, "live_viewer_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2665187
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 2665188
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 2665189
    if-eqz v2, :cond_5

    .line 2665190
    const-string v3, "peak_viewer_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2665191
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 2665192
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, v5}, LX/15i;->a(III)I

    move-result v2

    .line 2665193
    if-eqz v2, :cond_6

    .line 2665194
    const-string v3, "playable_duration_in_ms"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2665195
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 2665196
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2665197
    if-eqz v2, :cond_7

    .line 2665198
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2665199
    invoke-static {v1, v2, p1}, LX/JEC;->a(LX/15i;ILX/0nX;)V

    .line 2665200
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2665201
    if-eqz v2, :cond_8

    .line 2665202
    const-string v3, "video_insights"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2665203
    invoke-static {v1, v2, p1, p2}, LX/JEG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2665204
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2665205
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2665206
    check-cast p1, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$Serializer;->a(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;LX/0nX;LX/0my;)V

    return-void
.end method
