.class public Lcom/facebook/video/insight/view/VideoInsightInsightPartView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Lcom/facebook/video/insight/view/VideoInsightCardTitleView;

.field private d:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2665899
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2665900
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2665901
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2665902
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2665903
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2665904
    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0315a4

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2665905
    const v0, 0x7f0d30ca

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2665906
    const v0, 0x7f0d30c8

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2665907
    const v0, 0x7f0d30c7

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->c:Lcom/facebook/video/insight/view/VideoInsightCardTitleView;

    .line 2665908
    const v0, 0x7f0d30c9

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2665909
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2665910
    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->k()Z

    move-result v0

    .line 2665911
    if-eqz v0, :cond_0

    const v3, 0x7f083928

    .line 2665912
    :goto_0
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->c:Lcom/facebook/video/insight/view/VideoInsightCardTitleView;

    const v1, 0x7f0209e1

    const v2, 0x7f0208ec

    const v4, 0x7f083930

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->a(IIIILandroid/view/View$OnClickListener;)V

    .line 2665913
    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->n()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;->a()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;

    move-result-object v0

    .line 2665914
    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;->a()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel$ImageModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2665915
    iget-object v1, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2665916
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;->l()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2665917
    iget-object v1, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v8}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2665918
    :goto_2
    new-instance v1, LX/JEQ;

    invoke-direct {v1, v0}, LX/JEQ;-><init>(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;)V

    .line 2665919
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 2665920
    new-instance v2, LX/62X;

    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00fb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, LX/62X;-><init>(II)V

    .line 2665921
    iput-boolean v7, v2, LX/62X;->e:Z

    .line 2665922
    iget-object v3, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2665923
    iget-object v2, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2665924
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2665925
    return-void

    .line 2665926
    :cond_0
    const v3, 0x7f083929    # 1.810718E38f

    goto :goto_0

    .line 2665927
    :cond_1
    iget-object v1, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;->a()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel$ImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2665928
    iget-object v1, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_1

    .line 2665929
    :cond_2
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2665930
    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2665931
    const-string v2, " "

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2665932
    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2665933
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v6, v3, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2665934
    iget-object v2, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2665935
    iget-object v1, p0, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_2
.end method
