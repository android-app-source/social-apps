.class public Lcom/facebook/video/insight/view/VideoInsightCardTitleView;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private c:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2665813
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2665814
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2665815
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2665816
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2665817
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2665818
    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0315a1

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2665819
    const v0, 0x7f0d30be

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2665820
    const v0, 0x7f0d30bd

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2665821
    const v0, 0x7f0d30bf

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2665822
    return-void
.end method


# virtual methods
.method public final a(IIIILandroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2665823
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2665824
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2665825
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2665826
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2665827
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p5}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2665828
    return-void
.end method
