.class public Lcom/facebook/video/insight/view/VideoInsightVideoPartView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2665977
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2665978
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2665979
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2665980
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2665981
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2665982
    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0315a6

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2665983
    const v0, 0x7f0d30ce

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2665984
    const v0, 0x7f0d251a

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2665985
    const v0, 0x7f0d30cf

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2665986
    const v0, 0x7f0d30d0

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2665987
    const v0, 0x7f0d30d1

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2665988
    return-void
.end method

.method private static a(I)Ljava/lang/String;
    .locals 10

    .prologue
    const-wide/32 v8, 0xea60

    const/16 v6, 0xa

    .line 2665989
    int-to-long v0, p0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 2665990
    div-long v2, v0, v8

    long-to-int v2, v2

    .line 2665991
    int-to-long v4, v2

    mul-long/2addr v4, v8

    sub-long/2addr v0, v4

    long-to-int v3, v0

    .line 2665992
    if-le v2, v6, :cond_0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2665993
    :goto_0
    if-le v3, v6, :cond_1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 2665994
    :goto_1
    const-string v2, "%s:%s"

    invoke-static {v2, v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2665995
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2665996
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "0"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;Lcom/facebook/common/callercontext/CallerContext;LX/11S;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2665997
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->m()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2665998
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->l()I

    move-result v1

    invoke-static {v1}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2665999
    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->k()Z

    move-result v2

    .line 2666000
    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->j()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel$ActorsModel;

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel$ActorsModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2666001
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2666002
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08392b

    new-array v4, v5, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v0, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2666003
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v3, v0, v6, v1, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2666004
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666005
    sget-object v0, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-interface {p3, v0, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    .line 2666006
    iget-object v1, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666007
    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->j()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;->j()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel$MessageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2666008
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->j()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel;->j()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel$MessageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$CreationStoryModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666009
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2666010
    :goto_1
    return-void

    :cond_0
    move-object v0, v1

    .line 2666011
    goto :goto_0

    .line 2666012
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->e:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1
.end method
