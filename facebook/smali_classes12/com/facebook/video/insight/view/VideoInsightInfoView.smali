.class public Lcom/facebook/video/insight/view/VideoInsightInfoView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private b:Lcom/facebook/video/insight/view/VideoInsightCardTitleView;

.field private c:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2665895
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/video/insight/view/VideoInsightInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2665896
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2665897
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/video/insight/view/VideoInsightInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2665898
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2665889
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2665890
    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0315a3

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2665891
    const v0, 0x7f0d30c6

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightInfoView;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2665892
    const v0, 0x7f0d30c4

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightInfoView;->b:Lcom/facebook/video/insight/view/VideoInsightCardTitleView;

    .line 2665893
    const v0, 0x7f0d30c5

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/view/VideoInsightInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightInfoView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2665894
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;Landroid/view/View$OnClickListener;)V
    .locals 6

    .prologue
    .line 2665882
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightInfoView;->b:Lcom/facebook/video/insight/view/VideoInsightCardTitleView;

    const v1, 0x7f0208ed

    const v2, 0x7f02081b

    const v3, 0x7f08392c

    const v4, 0x7f083931

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/video/insight/view/VideoInsightCardTitleView;->a(IIIILandroid/view/View$OnClickListener;)V

    .line 2665883
    iget-object v0, p0, Lcom/facebook/video/insight/view/VideoInsightInfoView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08392d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2665884
    new-instance v0, LX/JEL;

    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->n()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;->a()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;

    move-result-object v1

    invoke-direct {v0, v1, p2}, LX/JEL;-><init>(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;Landroid/view/View$OnClickListener;)V

    .line 2665885
    new-instance v1, LX/1P0;

    invoke-virtual {p0}, Lcom/facebook/video/insight/view/VideoInsightInfoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P0;-><init>(Landroid/content/Context;)V

    .line 2665886
    iget-object v2, p0, Lcom/facebook/video/insight/view/VideoInsightInfoView;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2665887
    iget-object v1, p0, Lcom/facebook/video/insight/view/VideoInsightInfoView;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2665888
    return-void
.end method
