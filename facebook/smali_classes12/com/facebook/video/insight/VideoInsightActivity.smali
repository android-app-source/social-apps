.class public Lcom/facebook/video/insight/VideoInsightActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/JDx;


# static fields
.field private static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private q:LX/JE0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/11R;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:LX/JET;

.field private u:Ljava/lang/String;

.field private v:LX/JDz;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2664894
    const-class v0, Lcom/facebook/video/insight/VideoInsightActivity;

    const-string v1, "video_insight"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/video/insight/VideoInsightActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2664893
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2664890
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/video/insight/VideoInsightActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2664891
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2664892
    return-object v0
.end method

.method private static a(Landroid/content/Intent;Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2664889
    if-eqz p1, :cond_0

    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "video_id"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/video/insight/VideoInsightActivity;LX/JE0;LX/11R;LX/0Zb;)V
    .locals 0

    .prologue
    .line 2664888
    iput-object p1, p0, Lcom/facebook/video/insight/VideoInsightActivity;->q:LX/JE0;

    iput-object p2, p0, Lcom/facebook/video/insight/VideoInsightActivity;->r:LX/11R;

    iput-object p3, p0, Lcom/facebook/video/insight/VideoInsightActivity;->s:LX/0Zb;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/video/insight/VideoInsightActivity;

    const-class v0, LX/JE0;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/JE0;

    invoke-static {v2}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11R;

    invoke-static {v2}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/video/insight/VideoInsightActivity;->a(Lcom/facebook/video/insight/VideoInsightActivity;LX/JE0;LX/11R;LX/0Zb;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2664885
    iget-object v0, p0, Lcom/facebook/video/insight/VideoInsightActivity;->t:LX/JET;

    .line 2664886
    iget-object v1, v0, LX/JET;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const-string v2, "fail to fetch video insight for "

    new-instance p0, LX/JER;

    invoke-direct {p0, v0}, LX/JER;-><init>(LX/JET;)V

    invoke-virtual {v1, v2, p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2664887
    return-void
.end method

.method public final a(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;)V
    .locals 4

    .prologue
    .line 2664877
    iget-object v0, p0, Lcom/facebook/video/insight/VideoInsightActivity;->t:LX/JET;

    .line 2664878
    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->n()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;->n()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel;->a()Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel$VideoInsightsModel$SummaryModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2664879
    :cond_0
    :goto_0
    return-void

    .line 2664880
    :cond_1
    new-instance v1, LX/JES;

    invoke-direct {v1, v0}, LX/JES;-><init>(LX/JET;)V

    .line 2664881
    iget-object v2, v0, LX/JET;->d:Lcom/facebook/video/insight/view/VideoInsightVideoPartView;

    iget-object v3, v0, LX/JET;->f:Lcom/facebook/common/callercontext/CallerContext;

    iget-object p0, v0, LX/JET;->g:LX/11S;

    invoke-virtual {v2, p1, v3, p0}, Lcom/facebook/video/insight/view/VideoInsightVideoPartView;->a(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;Lcom/facebook/common/callercontext/CallerContext;LX/11S;)V

    .line 2664882
    iget-object v2, v0, LX/JET;->b:Lcom/facebook/video/insight/view/VideoInsightInsightPartView;

    iget-object v3, v0, LX/JET;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, p1, v3, v1}, Lcom/facebook/video/insight/view/VideoInsightInsightPartView;->a(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;Lcom/facebook/common/callercontext/CallerContext;Landroid/view/View$OnClickListener;)V

    .line 2664883
    iget-object v2, v0, LX/JET;->c:Lcom/facebook/video/insight/view/VideoInsightInfoView;

    invoke-virtual {v2, p1, v1}, Lcom/facebook/video/insight/view/VideoInsightInfoView;->a(Lcom/facebook/video/insight/protocol/FetchVideoInsightGraphQLModels$FetchVideoInsightModel;Landroid/view/View$OnClickListener;)V

    .line 2664884
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/JET;->a(LX/JET;Z)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2664843
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2664844
    invoke-static {p0, p0}, Lcom/facebook/video/insight/VideoInsightActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2664845
    invoke-virtual {p0}, Lcom/facebook/video/insight/VideoInsightActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/video/insight/VideoInsightActivity;->a(Landroid/content/Intent;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/insight/VideoInsightActivity;->u:Ljava/lang/String;

    .line 2664846
    invoke-virtual {p0}, Lcom/facebook/video/insight/VideoInsightActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "module_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2664847
    iget-object v1, p0, Lcom/facebook/video/insight/VideoInsightActivity;->s:LX/0Zb;

    const-string v2, "open_video_insight"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2664848
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2664849
    const-string v2, "video_id"

    iget-object v3, p0, Lcom/facebook/video/insight/VideoInsightActivity;->u:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2664850
    const-string v2, "open_source"

    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2664851
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2664852
    :cond_0
    new-instance v0, LX/JET;

    invoke-direct {v0, p0}, LX/JET;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/video/insight/VideoInsightActivity;->t:LX/JET;

    .line 2664853
    iget-object v0, p0, Lcom/facebook/video/insight/VideoInsightActivity;->t:LX/JET;

    invoke-virtual {p0, v0}, Lcom/facebook/video/insight/VideoInsightActivity;->setContentView(Landroid/view/View;)V

    .line 2664854
    iget-object v0, p0, Lcom/facebook/video/insight/VideoInsightActivity;->t:LX/JET;

    sget-object v1, Lcom/facebook/video/insight/VideoInsightActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v2, p0, Lcom/facebook/video/insight/VideoInsightActivity;->r:LX/11R;

    new-instance v3, LX/JDw;

    invoke-direct {v3, p0}, LX/JDw;-><init>(Lcom/facebook/video/insight/VideoInsightActivity;)V

    .line 2664855
    iput-object v1, v0, LX/JET;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 2664856
    iput-object v2, v0, LX/JET;->g:LX/11S;

    .line 2664857
    iget-object p1, v0, LX/JET;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2664858
    const/4 p1, 0x1

    invoke-static {v0, p1}, LX/JET;->a(LX/JET;Z)V

    .line 2664859
    iget-object v0, p0, Lcom/facebook/video/insight/VideoInsightActivity;->q:LX/JE0;

    sget-object v1, Lcom/facebook/video/insight/VideoInsightActivity;->p:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v2, p0, Lcom/facebook/video/insight/VideoInsightActivity;->u:Ljava/lang/String;

    .line 2664860
    new-instance v4, LX/JDz;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    move-object v8, v1

    move-object v9, p0

    move-object v10, v2

    invoke-direct/range {v4 .. v10}, LX/JDz;-><init>(LX/0tX;LX/1Ck;Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;LX/JDx;Ljava/lang/String;)V

    .line 2664861
    move-object v0, v4

    .line 2664862
    iput-object v0, p0, Lcom/facebook/video/insight/VideoInsightActivity;->v:LX/JDz;

    .line 2664863
    iget-object v0, p0, Lcom/facebook/video/insight/VideoInsightActivity;->v:LX/JDz;

    .line 2664864
    iget-object v4, v0, LX/JDz;->b:LX/1Ck;

    const-string v5, "fetch_video_insight"

    .line 2664865
    new-instance v8, LX/JE3;

    invoke-direct {v8}, LX/JE3;-><init>()V

    move-object v8, v8

    .line 2664866
    const-string v9, "video_id"

    iget-object v10, v0, LX/JDz;->e:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2664867
    const-string v9, "duration"

    const-string v10, "ALL_TIME"

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2664868
    const-string v9, "video_image_size"

    iget-object v10, v0, LX/JDz;->f:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b24a6

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2664869
    iget-object v9, v0, LX/JDz;->a:LX/0tX;

    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    iget-object v10, v0, LX/JDz;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2664870
    iput-object v10, v8, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2664871
    move-object v8, v8

    .line 2664872
    sget-object v10, LX/0zS;->c:LX/0zS;

    invoke-virtual {v8, v10}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v8

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v8

    .line 2664873
    move-object v6, v8

    .line 2664874
    new-instance v7, LX/JDy;

    invoke-direct {v7, v0}, LX/JDy;-><init>(LX/JDz;)V

    invoke-virtual {v4, v5, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2664875
    invoke-virtual {p0}, Lcom/facebook/video/insight/VideoInsightActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/video/insight/VideoInsightActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {v0, v1}, LX/470;->a(Landroid/view/Window;I)V

    .line 2664876
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x57b42d87

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2664838
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2664839
    iget-object v1, p0, Lcom/facebook/video/insight/VideoInsightActivity;->v:LX/JDz;

    .line 2664840
    iget-object v2, v1, LX/JDz;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2664841
    const/4 v2, 0x0

    iput-object v2, v1, LX/JDz;->d:LX/JDx;

    .line 2664842
    const/16 v1, 0x23

    const v2, 0x454b6a4d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2664835
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2664836
    const-string v0, "video_id"

    iget-object v1, p0, Lcom/facebook/video/insight/VideoInsightActivity;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2664837
    return-void
.end method
