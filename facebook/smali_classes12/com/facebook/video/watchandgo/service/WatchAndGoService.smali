.class public Lcom/facebook/video/watchandgo/service/WatchAndGoService;
.super LX/0te;
.source ""


# instance fields
.field private a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Hf5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/0pu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Hf5;

.field private d:LX/0Yd;

.field private e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0q0;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2490816
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2490817
    new-instance v0, LX/Hej;

    invoke-direct {v0, p0}, LX/Hej;-><init>(Lcom/facebook/video/watchandgo/service/WatchAndGoService;)V

    iput-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->h:LX/0q0;

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2490813
    iget-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c:LX/Hf5;

    if-eqz v0, :cond_0

    .line 2490814
    iget-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c:LX/Hf5;

    invoke-virtual {v0}, LX/Hf5;->f()V

    .line 2490815
    :cond_0
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2490806
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2490807
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2490808
    sget-object v1, LX/D7n;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2490809
    invoke-direct {p0, p1}, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->b(Landroid/content/Intent;)V

    .line 2490810
    :cond_0
    :goto_0
    return-void

    .line 2490811
    :cond_1
    sget-object v1, LX/D7n;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2490812
    invoke-direct {p0}, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->a()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/video/watchandgo/service/WatchAndGoService;LX/0Or;LX/0pu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/watchandgo/service/WatchAndGoService;",
            "LX/0Or",
            "<",
            "LX/Hf5;",
            ">;",
            "LX/0pu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2490805
    iput-object p1, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->b:LX/0pu;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;

    const/16 v1, 0x3855

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0}, LX/0pu;->a(LX/0QB;)LX/0pu;

    move-result-object v0

    check-cast v0, LX/0pu;

    invoke-static {p0, v1, v0}, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->a(Lcom/facebook/video/watchandgo/service/WatchAndGoService;LX/0Or;LX/0pu;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2490802
    iget-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c:LX/Hf5;

    if-nez v0, :cond_0

    .line 2490803
    invoke-direct {p0}, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->d()V

    .line 2490804
    :cond_0
    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 2490783
    const-string v0, "com.facebook.katana.EXTRA_STORY_PROPS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2490784
    const-string v0, "com.facebook.katana.EXTRA_VIDEO_PROPS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2490785
    const-string v0, "com.facebook.katana.EXTRA_STORY_ATTACHMENT_PROPS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2490786
    invoke-direct {p0}, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->b()V

    .line 2490787
    const-string v0, "com.facebook.katana.EXTRA_ASPECT_RATIO"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v6

    .line 2490788
    const-string v0, "com.facebook.katana.EXTRA_SEEK_POSITION"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 2490789
    const-string v0, "com.facebook.katana.EXTRA_LAST_START_POSITION"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 2490790
    const-string v0, "com.facebook.katana.EXTRA_PLAYER_ORIGIN"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2490791
    const-string v1, "com.facebook.katana.EXTRA_ORIGINAL_PLAYER_TYPE"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2490792
    const-string v2, "com.facebook.katana.EXTRA_EVENT_TRIGGER"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2490793
    const-string v3, "com.facebook.katana.EXTRA_VIDEO_TYPE"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, LX/D7o;

    .line 2490794
    invoke-static {v0}, LX/0Tp;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2490795
    invoke-static {v1}, LX/0Tp;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2490796
    invoke-static {v2}, LX/0Tp;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 2490797
    invoke-static {v0}, LX/04D;->asPlayerOrigin(Ljava/lang/String;)LX/04D;

    move-result-object v10

    .line 2490798
    invoke-static {v1}, LX/04G;->fromString(Ljava/lang/String;)LX/04G;

    move-result-object v11

    .line 2490799
    invoke-static {v2}, LX/04g;->asEventTriggerType(Ljava/lang/String;)LX/04g;

    move-result-object v12

    .line 2490800
    iget-object v1, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c:LX/Hf5;

    iget-object v2, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual/range {v1 .. v12}, LX/Hf5;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/D7o;DIILX/04D;LX/04G;LX/04g;)V

    .line 2490801
    return-void
.end method

.method public static c(Lcom/facebook/video/watchandgo/service/WatchAndGoService;)V
    .locals 6

    .prologue
    .line 2490766
    iget-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c:LX/Hf5;

    if-eqz v0, :cond_0

    .line 2490767
    iget-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c:LX/Hf5;

    const/4 v2, 0x0

    .line 2490768
    sput-object v2, LX/13l;->b:LX/13l;

    .line 2490769
    iget-object v1, v0, LX/Hf5;->k:LX/Hex;

    invoke-virtual {v1, v2}, LX/Hex;->setTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2490770
    iget-object v1, v0, LX/Hf5;->k:LX/Hex;

    const/4 v5, 0x0

    .line 2490771
    iget-object v3, v1, LX/Hex;->f:LX/Hew;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/Hew;->removeMessages(I)V

    .line 2490772
    iput-object v5, v1, LX/Hex;->f:LX/Hew;

    .line 2490773
    iget-object v3, v1, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    sget-object v4, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v3, v4}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 2490774
    iput-object v5, v1, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    .line 2490775
    iput-object v2, v0, LX/Hf5;->k:LX/Hex;

    .line 2490776
    iget-object v1, v0, LX/Hf5;->l:LX/Hey;

    invoke-virtual {v1}, LX/HeO;->b()V

    .line 2490777
    iput-object v2, v0, LX/Hf5;->n:LX/Hep;

    .line 2490778
    iget-object v1, v0, LX/Hf5;->m:LX/Heq;

    invoke-virtual {v1}, LX/HeO;->b()V

    .line 2490779
    iget-object v1, v0, LX/Hf5;->g:LX/378;

    iget-object v2, v0, LX/Hf5;->B:LX/Hf2;

    invoke-virtual {v1, v2}, LX/378;->b(LX/7Ru;)V

    .line 2490780
    const/4 v1, 0x0

    iput-object v1, v0, LX/Hf5;->B:LX/Hf2;

    .line 2490781
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c:LX/Hf5;

    .line 2490782
    :cond_0
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    .line 2490738
    iget-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hf5;

    iput-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c:LX/Hf5;

    .line 2490739
    iget-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c:LX/Hf5;

    .line 2490740
    invoke-static {}, LX/Her;->values()[LX/Her;

    move-result-object v1

    iget-object v2, v0, LX/Hf5;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/D7y;->a:LX/0Tn;

    sget-object v4, LX/Her;->TOP_LEFT:LX/Her;

    invoke-virtual {v4}, LX/Her;->ordinal()I

    move-result v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, LX/Hf5;->s:LX/Her;

    .line 2490741
    iget-object v1, v0, LX/Hf5;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2490742
    const v2, 0x7f0b1be2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, LX/Hf5;->u:I

    .line 2490743
    const v2, 0x7f0b1be4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, LX/Hf5;->w:I

    .line 2490744
    const v2, 0x7f0b1be3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, LX/Hf5;->v:I

    .line 2490745
    const v2, 0x7f0b1be5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, LX/Hf5;->x:I

    .line 2490746
    const v2, 0x7f0b1be6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, LX/Hf5;->t:I

    .line 2490747
    const/4 v4, 0x0

    .line 2490748
    new-instance v1, LX/Hep;

    iget-object v2, v0, LX/Hf5;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/Hep;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, LX/Hf5;->n:LX/Hep;

    .line 2490749
    iget-object v1, v0, LX/Hf5;->n:LX/Hep;

    iget v2, v0, LX/Hf5;->t:I

    .line 2490750
    iput v2, v1, LX/Hep;->c:I

    .line 2490751
    iget-object v1, v0, LX/Hf5;->n:LX/Hep;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, LX/Hep;->setVisibility(I)V

    .line 2490752
    new-instance v1, LX/Heq;

    iget-object v2, v0, LX/Hf5;->b:Landroid/view/WindowManager;

    iget-object v3, v0, LX/Hf5;->n:LX/Hep;

    invoke-direct {v1, v2, v3}, LX/Heq;-><init>(Landroid/view/WindowManager;Landroid/view/View;)V

    iput-object v1, v0, LX/Hf5;->m:LX/Heq;

    .line 2490753
    iget-object v1, v0, LX/Hf5;->m:LX/Heq;

    invoke-virtual {v1}, LX/HeO;->a()V

    .line 2490754
    iget-object v1, v0, LX/Hf5;->m:LX/Heq;

    invoke-virtual {v1, v4}, LX/HeO;->a(I)V

    .line 2490755
    iget-object v1, v0, LX/Hf5;->m:LX/Heq;

    invoke-virtual {v1, v4}, LX/HeO;->b(I)V

    .line 2490756
    iget-object v1, v0, LX/Hf5;->n:LX/Hep;

    invoke-virtual {v1}, LX/Hep;->b()V

    .line 2490757
    new-instance v1, LX/Hex;

    iget-object v2, v0, LX/Hf5;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/Hex;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, LX/Hf5;->k:LX/Hex;

    .line 2490758
    new-instance v1, LX/Hey;

    iget-object v2, v0, LX/Hf5;->b:Landroid/view/WindowManager;

    iget-object v3, v0, LX/Hf5;->k:LX/Hex;

    iget-object v4, v0, LX/Hf5;->D:LX/0wW;

    invoke-direct {v1, v2, v3, v4}, LX/Hey;-><init>(Landroid/view/WindowManager;LX/Hex;LX/0wW;)V

    iput-object v1, v0, LX/Hf5;->l:LX/Hey;

    .line 2490759
    iget-object v1, v0, LX/Hf5;->l:LX/Hey;

    invoke-virtual {v1}, LX/HeO;->a()V

    .line 2490760
    iget-object v1, v0, LX/Hf5;->B:LX/Hf2;

    if-nez v1, :cond_0

    .line 2490761
    new-instance v1, LX/Hf2;

    invoke-direct {v1, v0}, LX/Hf2;-><init>(LX/Hf5;)V

    iput-object v1, v0, LX/Hf5;->B:LX/Hf2;

    .line 2490762
    :cond_0
    iget-object v1, v0, LX/Hf5;->g:LX/378;

    iget-object v2, v0, LX/Hf5;->B:LX/Hf2;

    invoke-virtual {v1, v2}, LX/378;->a(LX/7Ru;)V

    .line 2490763
    iget-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c:LX/Hf5;

    new-instance v1, LX/Hem;

    invoke-direct {v1, p0}, LX/Hem;-><init>(Lcom/facebook/video/watchandgo/service/WatchAndGoService;)V

    .line 2490764
    iput-object v1, v0, LX/Hf5;->A:LX/Hem;

    .line 2490765
    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2490711
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2490732
    invoke-super {p0, p1}, LX/0te;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2490733
    iget-object v0, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c:LX/Hf5;

    .line 2490734
    iget-object p0, v0, LX/Hf5;->b:Landroid/view/WindowManager;

    if-nez p0, :cond_0

    .line 2490735
    :goto_0
    return-void

    .line 2490736
    :cond_0
    iget-object p0, v0, LX/Hf5;->b:Landroid/view/WindowManager;

    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p0

    iget-object p1, v0, LX/Hf5;->H:Landroid/util/DisplayMetrics;

    invoke-virtual {p0, p1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 2490737
    iget-object p0, v0, LX/Hf5;->s:LX/Her;

    const/4 p1, 0x0

    invoke-static {v0, p0, p1}, LX/Hf5;->a(LX/Hf5;LX/Her;Z)V

    goto :goto_0
.end method

.method public final onFbCreate()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x24

    const v1, 0x8bddc0b

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2490723
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2490724
    invoke-static {p0, p0}, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2490725
    iget-object v1, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->b:LX/0pu;

    iget-object v2, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->h:LX/0q0;

    invoke-virtual {v1, v2}, LX/0pu;->a(LX/0q0;)V

    .line 2490726
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 2490727
    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2490728
    const-string v2, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2490729
    new-instance v2, LX/0Yd;

    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    const-string v4, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    new-instance v5, LX/Hel;

    invoke-direct {v5, p0}, LX/Hel;-><init>(Lcom/facebook/video/watchandgo/service/WatchAndGoService;)V

    invoke-virtual {v3, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v4, "android.intent.action.USER_PRESENT"

    new-instance v5, LX/Hek;

    invoke-direct {v5, p0}, LX/Hek;-><init>(Lcom/facebook/video/watchandgo/service/WatchAndGoService;)V

    invoke-virtual {v3, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v2, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->d:LX/0Yd;

    .line 2490730
    iget-object v2, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->d:LX/0Yd;

    invoke-virtual {p0, v2, v1}, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2490731
    const/16 v1, 0x25

    const v2, -0x19023dfd

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x357b1496

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2490716
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 2490717
    iget-object v1, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->d:LX/0Yd;

    if-eqz v1, :cond_0

    .line 2490718
    iget-object v1, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->d:LX/0Yd;

    invoke-virtual {p0, v1}, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2490719
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->d:LX/0Yd;

    .line 2490720
    :cond_0
    iget-object v1, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->b:LX/0pu;

    iget-object v2, p0, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->h:LX/0q0;

    invoke-virtual {v1, v2}, LX/0pu;->b(LX/0q0;)V

    .line 2490721
    invoke-static {p0}, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->c(Lcom/facebook/video/watchandgo/service/WatchAndGoService;)V

    .line 2490722
    const/16 v1, 0x25

    const v2, -0x6584b6d1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x128b92d0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2490714
    invoke-direct {p0, p1}, Lcom/facebook/video/watchandgo/service/WatchAndGoService;->a(Landroid/content/Intent;)V

    .line 2490715
    const/16 v1, 0x25

    const v2, 0x5aebb79f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v3
.end method

.method public final startActivity(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2490712
    invoke-super {p0, p1}, LX/0te;->startActivity(Landroid/content/Intent;)V

    .line 2490713
    return-void
.end method
