.class public Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f8;


# instance fields
.field private final A:LX/394;

.field private p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iM;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iL;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iI;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iQ;",
            ">;"
        }
    .end annotation
.end field

.field private t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private v:LX/0hE;

.field private w:Lcom/facebook/graphql/model/GraphQLVideo;

.field private x:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private y:I

.field private z:LX/0hE;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2490902
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2490903
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2490904
    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->p:LX/0Ot;

    .line 2490905
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2490906
    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->q:LX/0Ot;

    .line 2490907
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2490908
    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->r:LX/0Ot;

    .line 2490909
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2490910
    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->s:LX/0Ot;

    .line 2490911
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2490912
    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->t:LX/0Ot;

    .line 2490913
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2490914
    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->u:LX/0Ot;

    .line 2490915
    new-instance v0, LX/Heu;

    invoke-direct {v0, p0}, LX/Heu;-><init>(Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;)V

    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->A:LX/394;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;I)Landroid/content/Intent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            ">;I)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 2490896
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2490897
    const-string v1, "com.facebook.katana.EXTRA_ATTACHMENT_PROPS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2490898
    const-string v1, "com.facebook.katana.EXTRA_SEEK_TIME"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2490899
    const-string v1, "com.facebook.katana.EXTRA_VIDEO_PROPS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2490900
    return-object v0
.end method

.method private static a(Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;",
            "LX/0Ot",
            "<",
            "LX/0iM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2490901
    iput-object p1, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->p:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->q:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->r:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->s:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->t:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->u:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;

    const/16 v1, 0x1390

    invoke-static {v6, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x1302

    invoke-static {v6, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x78e

    invoke-static {v6, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xbc3

    invoke-static {v6, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xc49

    invoke-static {v6, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v7, 0x455

    invoke-static {v6, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->a(Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0hE;)V
    .locals 0

    .prologue
    .line 2490938
    iput-object p1, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    .line 2490939
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2490916
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2490917
    invoke-static {p0, p0}, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2490918
    invoke-virtual {p0}, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.facebook.katana.EXTRA_VIDEO_PROPS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2490919
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2490920
    check-cast v0, Lcom/facebook/graphql/model/GraphQLVideo;

    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->w:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 2490921
    invoke-virtual {p0}, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.facebook.katana.EXTRA_SEEK_TIME"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->y:I

    .line 2490922
    invoke-virtual {p0}, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.facebook.katana.EXTRA_ATTACHMENT_PROPS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2490923
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->w:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_1

    .line 2490924
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->finish()V

    .line 2490925
    :cond_1
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->w:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v1

    .line 2490926
    new-instance v2, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2490927
    new-instance v0, LX/395;

    new-instance v3, LX/0AV;

    invoke-direct {v3, v1}, LX/0AV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, LX/0AV;->a()Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    move-result-object v1

    new-instance v3, LX/0AW;

    invoke-direct {v3, v2}, LX/0AW;-><init>(LX/162;)V

    invoke-virtual {v3}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->w:Lcom/facebook/graphql/model/GraphQLVideo;

    iget-object v5, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct/range {v0 .. v5}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2490928
    iget v1, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->y:I

    invoke-virtual {v0, v1}, LX/395;->a(I)LX/395;

    .line 2490929
    invoke-virtual {p0}, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->i()LX/0hE;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->z:LX/0hE;

    .line 2490930
    iget-object v1, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->z:LX/0hE;

    invoke-interface {v1, v0}, LX/0hE;->a(LX/395;)V

    .line 2490931
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    iget-object v1, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->A:LX/394;

    invoke-interface {v0, v1}, LX/0hE;->a(LX/394;)Ljava/lang/Object;

    .line 2490932
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    sget-object v1, LX/0ax;->je:Ljava/lang/String;

    invoke-interface {v0, p0, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2490933
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 2490934
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2490935
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()LX/0hE;
    .locals 1

    .prologue
    .line 2490936
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iI;

    invoke-virtual {v0, p0}, LX/0iI;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    .line 2490937
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    return-object v0
.end method

.method public final l()LX/0hE;
    .locals 1

    .prologue
    .line 2490892
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iL;

    invoke-virtual {v0, p0}, LX/0iL;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    .line 2490893
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    return-object v0
.end method

.method public final m()LX/0hE;
    .locals 1

    .prologue
    .line 2490894
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iM;

    invoke-virtual {v0, p0}, LX/0iM;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    .line 2490895
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    return-object v0
.end method

.method public final o()LX/0hE;
    .locals 1

    .prologue
    .line 2490890
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iQ;

    invoke-virtual {v0, p0}, LX/0iQ;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    .line 2490891
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    return-object v0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2490885
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    invoke-interface {v0}, LX/0hE;->b()Z

    .line 2490886
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2490887
    :goto_0
    return-void

    .line 2490888
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2490889
    invoke-virtual {p0}, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->finish()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x7dcf0db8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2490882
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2490883
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    .line 2490884
    const/16 v1, 0x23

    const v2, -0x648da97f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x43b13d4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2490879
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2490880
    iget-object v1, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    invoke-interface {v1}, LX/0hE;->g()V

    .line 2490881
    const/16 v1, 0x23

    const v2, -0x6bbb3a33

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x5f42994c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2490876
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2490877
    iget-object v1, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    invoke-interface {v1}, LX/0hE;->f()V

    .line 2490878
    const/16 v1, 0x23

    const v2, 0x5e0742bf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x18cb583f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2490873
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2490874
    iget-object v1, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    invoke-interface {v1}, LX/0hE;->e()V

    .line 2490875
    const/16 v1, 0x23

    const v2, 0x4e733759

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x4cd152ac    # 1.09745504E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2490870
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2490871
    iget-object v1, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    invoke-interface {v1}, LX/0hE;->h()V

    .line 2490872
    const/16 v1, 0x23

    const v2, 0x75d80de6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2490867
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2490868
    iget-object v0, p0, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->v:LX/0hE;

    invoke-interface {v0}, LX/0hE;->b()Z

    move-result v0

    .line 2490869
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
