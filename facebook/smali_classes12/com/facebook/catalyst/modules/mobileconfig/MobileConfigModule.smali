.class public Lcom/facebook/catalyst/modules/mobileconfig/MobileConfigModule;
.super Lcom/facebook/react/cxxbridge/CxxModuleWrapper;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2667069
    const-string v0, "catalyst-mobileconfigmodule"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 2667070
    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/33u;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0WV;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/29a;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Yb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2667071
    invoke-interface/range {p7 .. p7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tigon/iface/TigonServiceHolder;

    invoke-interface {p6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/29a;

    invoke-virtual {v1}, LX/29a;->a()Lcom/facebook/xanalytics/XAnalyticsNative;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/33u;

    invoke-virtual {v4}, LX/33u;->c()LX/33y;

    move-result-object v4

    invoke-virtual {v4}, LX/33y;->a()LX/5qI;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/facebook/catalyst/modules/mobileconfig/MobileConfigModule;->a(Landroid/content/Context;LX/5qI;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0WV;

    invoke-virtual {v5}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0dC;

    invoke-virtual {v6}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v7}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v7

    sget-boolean v8, LX/0AN;->a:Z

    if-nez v8, :cond_0

    sget-boolean v8, LX/0AN;->b:Z

    if-eqz v8, :cond_1

    :cond_0
    const/4 v8, 0x1

    :goto_0
    invoke-static/range {v0 .. v8}, Lcom/facebook/catalyst/modules/mobileconfig/MobileConfigModule;->initHybrid(Lcom/facebook/tigon/iface/TigonServiceHolder;Lcom/facebook/xanalytics/XAnalyticsHolder;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/jni/HybridData;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/react/cxxbridge/CxxModuleWrapper;-><init>(Lcom/facebook/jni/HybridData;)V

    .line 2667072
    return-void

    .line 2667073
    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;LX/5qI;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2667074
    invoke-interface {p1}, LX/5qI;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2667075
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ReactMobileConfigMetadata.json"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2667076
    invoke-interface {p1}, LX/5qI;->f()Ljava/io/File;

    move-result-object v0

    .line 2667077
    if-eqz v0, :cond_0

    .line 2667078
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 2667079
    :goto_0
    return-object v0

    .line 2667080
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "ReactMobileConfigMetadata.json"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v0

    .line 2667081
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/mobileconfig"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2667082
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2667083
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2667084
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2667085
    const-string v0, "React"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to create directory to store mobileconfig metadata: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2667086
    const-string v0, ""

    goto :goto_0

    .line 2667087
    :cond_1
    new-instance v2, Ljava/io/File;

    const-string v3, "ReactMobileConfigMetadata.json"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2667088
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2667089
    const/16 v3, 0x400

    new-array v3, v3, [B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2667090
    :goto_1
    :try_start_1
    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-lez v4, :cond_2

    .line 2667091
    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5, v4}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2667092
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2667093
    :catch_0
    move-exception v0

    .line 2667094
    const-string v1, "React"

    const-string v2, "Unable to process mobileconfig metadata file"

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2667095
    const-string v0, ""

    goto :goto_0

    .line 2667096
    :cond_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 2667097
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/catalyst/modules/mobileconfig/MobileConfigModule;
    .locals 8

    .prologue
    .line 2667098
    new-instance v0, Lcom/facebook/catalyst/modules/mobileconfig/MobileConfigModule;

    const/16 v1, 0x53c

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x3e5

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x4cd

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x19e

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x13b7

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xb8b

    invoke-static {p0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/catalyst/modules/mobileconfig/MobileConfigModule;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;)V

    .line 2667099
    return-object v0
.end method

.method private static native initHybrid(Lcom/facebook/tigon/iface/TigonServiceHolder;Lcom/facebook/xanalytics/XAnalyticsHolder;Lcom/facebook/common/jniexecutors/AndroidAsyncExecutorFactory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/jni/HybridData;
.end method
