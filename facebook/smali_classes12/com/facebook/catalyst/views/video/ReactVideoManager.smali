.class public Lcom/facebook/catalyst/views/video/ReactVideoManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTVideo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "LX/JHW;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2673058
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    return-void
.end method

.method private a(LX/5rJ;LX/JHW;)V
    .locals 1

    .prologue
    .line 2673031
    new-instance v0, LX/JHU;

    invoke-direct {v0, p0, p2, p1}, LX/JHU;-><init>(Lcom/facebook/catalyst/views/video/ReactVideoManager;LX/JHW;LX/5rJ;)V

    .line 2673032
    iput-object v0, p2, LX/JHW;->h:LX/JHU;

    .line 2673033
    return-void
.end method

.method private a(LX/JHW;)V
    .locals 3

    .prologue
    .line 2673049
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/SimpleViewManager;->b(Landroid/view/View;)V

    .line 2673050
    iget-boolean v0, p1, LX/JHW;->n:Z

    if-nez v0, :cond_0

    .line 2673051
    invoke-static {p1}, LX/JHW;->h(LX/JHW;)V

    .line 2673052
    :cond_0
    iget-boolean v0, p1, LX/JHW;->k:Z

    if-eqz v0, :cond_1

    .line 2673053
    iget-object v0, p1, LX/JHW;->g:LX/0Kx;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2673054
    iget-object v0, p1, LX/JHW;->j:LX/0GT;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2673055
    iget-object v0, p1, LX/JHW;->g:LX/0Kx;

    iget-object v1, p1, LX/JHW;->j:LX/0GT;

    const/4 v2, 0x1

    iget p0, p1, LX/JHW;->l:F

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-interface {v0, v1, v2, p0}, LX/0Kx;->a(LX/0GS;ILjava/lang/Object;)V

    .line 2673056
    const/4 v0, 0x0

    iput-boolean v0, p1, LX/JHW;->k:Z

    .line 2673057
    :cond_1
    return-void
.end method

.method private static a(LX/JHW;ILX/5pC;)V
    .locals 1
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2673045
    packed-switch p1, :pswitch_data_0

    .line 2673046
    :goto_0
    return-void

    .line 2673047
    :pswitch_0
    if-eqz p2, :cond_0

    invoke-interface {p2, v0}, LX/5pC;->getInt(I)I

    move-result v0

    .line 2673048
    :cond_0
    invoke-virtual {p0, v0}, LX/JHW;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static b(LX/5rJ;)LX/JHW;
    .locals 1

    .prologue
    .line 2673042
    new-instance v0, LX/JHW;

    invoke-direct {v0, p0}, LX/JHW;-><init>(Landroid/content/Context;)V

    .line 2673043
    invoke-virtual {p0, v0}, LX/5pX;->a(LX/5pQ;)V

    .line 2673044
    return-object v0
.end method

.method private static b(LX/JHW;)V
    .locals 1

    .prologue
    .line 2673039
    invoke-virtual {p0}, LX/JHW;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    invoke-virtual {v0, p0}, LX/5pX;->b(LX/5pQ;)V

    .line 2673040
    invoke-virtual {p0}, LX/JHW;->b()V

    .line 2673041
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2673038
    invoke-static {p1}, Lcom/facebook/catalyst/views/video/ReactVideoManager;->b(LX/5rJ;)LX/JHW;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/5rJ;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2673037
    check-cast p2, LX/JHW;

    invoke-direct {p0, p1, p2}, Lcom/facebook/catalyst/views/video/ReactVideoManager;->a(LX/5rJ;LX/JHW;)V

    return-void
.end method

.method public final synthetic a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2673036
    check-cast p1, LX/JHW;

    invoke-static {p1}, Lcom/facebook/catalyst/views/video/ReactVideoManager;->b(LX/JHW;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2673035
    check-cast p1, LX/JHW;

    invoke-static {p1, p2, p3}, Lcom/facebook/catalyst/views/video/ReactVideoManager;->a(LX/JHW;ILX/5pC;)V

    return-void
.end method

.method public final synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2673059
    check-cast p1, LX/JHW;

    invoke-direct {p0, p1}, Lcom/facebook/catalyst/views/video/ReactVideoManager;->a(LX/JHW;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2673015
    const-string v0, "RCTVideo"

    return-object v0
.end method

.method public resizeMode(LX/JHW;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "resizeMode"
    .end annotation

    .prologue
    .line 2673016
    iput-object p2, p1, LX/JHW;->m:Ljava/lang/String;

    .line 2673017
    return-void
.end method

.method public startPosition(LX/JHW;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x0
        name = "startPosition"
    .end annotation

    .prologue
    .line 2673018
    iput p2, p1, LX/JHW;->d:I

    .line 2673019
    return-void
.end method

.method public udpatePaused(LX/JHW;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "isPaused"
    .end annotation

    .prologue
    .line 2673020
    if-eqz p2, :cond_0

    .line 2673021
    invoke-virtual {p1}, LX/JHW;->g()V

    .line 2673022
    :goto_0
    return-void

    .line 2673023
    :cond_0
    invoke-virtual {p1}, LX/JHW;->f()V

    goto :goto_0
.end method

.method public updateSource(LX/JHW;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "src"
    .end annotation

    .prologue
    .line 2673024
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    iput-object p0, p1, LX/JHW;->e:Landroid/net/Uri;

    .line 2673025
    return-void
.end method

.method public updateVolume(LX/JHW;F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "volume"
    .end annotation

    .prologue
    .line 2673026
    const/4 p0, 0x1

    iput-boolean p0, p1, LX/JHW;->k:Z

    .line 2673027
    iput p2, p1, LX/JHW;->l:F

    .line 2673028
    return-void
.end method

.method public final v()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2673029
    const-string v0, "seekTo"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/util/Map;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2673030
    const-string v0, "topVideoStateChange"

    const-string v1, "registrationName"

    const-string v2, "onStateChange"

    invoke-static {v1, v2}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    const-string v2, "topVideoProgress"

    const-string v3, "registrationName"

    const-string v4, "onProgress"

    invoke-static {v3, v4}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    const-string v4, "topVideoSizeDetected"

    const-string v5, "registrationName"

    const-string v6, "onVideoSizeDetected"

    invoke-static {v5, v6}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final y()Ljava/util/Map;
    .locals 15
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2673034
    const-string v14, "State"

    const-string v0, "Idle"

    sget-object v1, LX/JHX;->IDLE:LX/JHX;

    invoke-virtual {v1}, LX/JHX;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "Preparing"

    sget-object v3, LX/JHX;->PREPARING:LX/JHX;

    invoke-virtual {v3}, LX/JHX;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "Ready"

    sget-object v5, LX/JHX;->READY:LX/JHX;

    invoke-virtual {v5}, LX/JHX;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "Buffering"

    sget-object v7, LX/JHX;->BUFFERING:LX/JHX;

    invoke-virtual {v7}, LX/JHX;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string v8, "Playing"

    sget-object v9, LX/JHX;->PLAYING:LX/JHX;

    invoke-virtual {v9}, LX/JHX;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const-string v10, "Ended"

    sget-object v11, LX/JHX;->ENDED:LX/JHX;

    invoke-virtual {v11}, LX/JHX;->ordinal()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const-string v12, "Error"

    sget-object v13, LX/JHX;->ERROR:LX/JHX;

    invoke-virtual {v13}, LX/JHX;->ordinal()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-static/range {v0 .. v13}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v14, v0}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
