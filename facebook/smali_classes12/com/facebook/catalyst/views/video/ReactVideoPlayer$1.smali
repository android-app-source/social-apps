.class public final Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/JHW;


# direct methods
.method public constructor <init>(LX/JHW;)V
    .locals 0

    .prologue
    .line 2673060
    iput-object p1, p0, Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;->a:LX/JHW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2673061
    iget-object v0, p0, Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;->a:LX/JHW;

    iget-object v0, v0, LX/JHW;->g:LX/0Kx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;->a:LX/JHW;

    iget-object v0, v0, LX/JHW;->h:LX/JHU;

    if-eqz v0, :cond_0

    .line 2673062
    iget-object v0, p0, Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;->a:LX/JHW;

    iget-object v0, v0, LX/JHW;->g:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->f()J

    move-result-wide v0

    long-to-int v0, v0

    div-int/lit16 v0, v0, 0x3e8

    .line 2673063
    iget-object v1, p0, Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;->a:LX/JHW;

    iget-object v1, v1, LX/JHW;->g:LX/0Kx;

    invoke-interface {v1}, LX/0Kx;->e()J

    move-result-wide v2

    long-to-int v1, v2

    div-int/lit16 v1, v1, 0x3e8

    .line 2673064
    iget-object v2, p0, Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;->a:LX/JHW;

    iget-object v2, v2, LX/JHW;->h:LX/JHU;

    .line 2673065
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v4

    .line 2673066
    const-string v3, "target"

    iget-object v5, v2, LX/JHU;->a:LX/JHW;

    invoke-virtual {v5}, LX/JHW;->getId()I

    move-result v5

    invoke-interface {v4, v3, v5}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2673067
    const-string v3, "position"

    invoke-interface {v4, v3, v0}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2673068
    const-string v3, "duration"

    invoke-interface {v4, v3, v1}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2673069
    iget-object v3, v2, LX/JHU;->b:LX/5rJ;

    const-class v5, Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    invoke-virtual {v3, v5}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v3

    check-cast v3, Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    iget-object v5, v2, LX/JHU;->a:LX/JHW;

    invoke-virtual {v5}, LX/JHW;->getId()I

    move-result v5

    const-string v6, "topVideoProgress"

    invoke-interface {v3, v5, v6, v4}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2673070
    iget-object v0, p0, Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;->a:LX/JHW;

    iget-boolean v0, v0, LX/JHW;->i:Z

    if-eqz v0, :cond_0

    .line 2673071
    iget-object v0, p0, Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;->a:LX/JHW;

    iget-object v0, v0, LX/JHW;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;->a:LX/JHW;

    iget-object v1, v1, LX/JHW;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    const v4, -0x4cf5ec24

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2673072
    :cond_0
    return-void
.end method
