.class public Lcom/facebook/catalyst/views/gradient/ReactAxialGradientManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTAxialGradientView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "LX/JH6;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2672399
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    return-void
.end method

.method private static b(LX/5rJ;)LX/JH6;
    .locals 1

    .prologue
    .line 2672367
    new-instance v0, LX/JH6;

    invoke-direct {v0, p0}, LX/JH6;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2672398
    invoke-static {p1}, Lcom/facebook/catalyst/views/gradient/ReactAxialGradientManager;->b(LX/5rJ;)LX/JH6;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2672395
    check-cast p1, LX/JH6;

    .line 2672396
    invoke-virtual {p1}, LX/JH6;->invalidate()V

    .line 2672397
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2672394
    const-string v0, "RCTAxialGradientView"

    return-object v0
.end method

.method public setColors(LX/JH6;LX/5pC;)V
    .locals 4
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        customType = "ColorArray"
        name = "colors"
    .end annotation

    .prologue
    .line 2672386
    if-eqz p2, :cond_0

    invoke-interface {p2}, LX/5pC;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 2672387
    :cond_0
    new-instance v0, LX/5pA;

    const-string v1, "The gradient must contain at least 2 colors"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2672388
    :cond_1
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v0

    new-array v1, v0, [I

    .line 2672389
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 2672390
    invoke-interface {p2, v0}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    double-to-int v2, v2

    aput v2, v1, v0

    .line 2672391
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2672392
    :cond_2
    iput-object v1, p1, LX/JH6;->e:[I

    .line 2672393
    return-void
.end method

.method public setEndX(LX/JH6;F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "endX"
    .end annotation

    .prologue
    .line 2672384
    iput p2, p1, LX/JH6;->c:F

    .line 2672385
    return-void
.end method

.method public setEndY(LX/JH6;F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "endY"
    .end annotation

    .prologue
    .line 2672382
    iput p2, p1, LX/JH6;->d:F

    .line 2672383
    return-void
.end method

.method public setLocations(LX/JH6;LX/5pC;)V
    .locals 4
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "locations"
    .end annotation

    .prologue
    .line 2672372
    if-nez p2, :cond_0

    .line 2672373
    const/4 v0, 0x0

    .line 2672374
    iput-object v0, p1, LX/JH6;->f:[F

    .line 2672375
    :goto_0
    return-void

    .line 2672376
    :cond_0
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v0

    new-array v1, v0, [F

    .line 2672377
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2672378
    invoke-interface {p2, v0}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    double-to-float v2, v2

    aput v2, v1, v0

    .line 2672379
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2672380
    :cond_1
    iput-object v1, p1, LX/JH6;->f:[F

    .line 2672381
    goto :goto_0
.end method

.method public setStartX(LX/JH6;F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "startX"
    .end annotation

    .prologue
    .line 2672370
    iput p2, p1, LX/JH6;->a:F

    .line 2672371
    return-void
.end method

.method public setStartY(LX/JH6;F)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "startY"
    .end annotation

    .prologue
    .line 2672368
    iput p2, p1, LX/JH6;->b:F

    .line 2672369
    return-void
.end method
