.class public Lcom/facebook/catalyst/views/maps/ReactMapViewManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "LX/7au;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Landroid/os/Bundle;

.field public static b:Z

.field public static d:Z

.field public static e:Z

.field public static f:Z

.field public static g:Z


# instance fields
.field private final c:LX/5pX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2672889
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->a:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>(LX/5pX;)V
    .locals 0

    .prologue
    .line 2672904
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    .line 2672905
    iput-object p1, p0, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->c:LX/5pX;

    .line 2672906
    return-void
.end method

.method private static a(LX/5pG;)Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 14

    .prologue
    .line 2672897
    const-string v0, "latitude"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "latitudeDelta"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "longitude"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "longitudeDelta"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2672898
    :cond_0
    new-instance v0, LX/5pA;

    const-string v1, "Region description is invalid"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2672899
    :cond_1
    const-string v0, "latitude"

    invoke-interface {p0, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 2672900
    const-string v2, "longitude"

    invoke-interface {p0, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 2672901
    const-string v4, "latitudeDelta"

    invoke-interface {p0, v4}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 2672902
    const-string v6, "longitudeDelta"

    invoke-interface {p0, v6}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 2672903
    invoke-static {}, Lcom/google/android/gms/maps/model/LatLngBounds;->b()LX/7c9;

    move-result-object v8

    new-instance v9, Lcom/google/android/gms/maps/model/LatLng;

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v10, v4

    sub-double v10, v0, v10

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v12, v6

    sub-double v12, v2, v12

    invoke-direct {v9, v10, v11, v12, v13}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v8, v9}, LX/7c9;->a(Lcom/google/android/gms/maps/model/LatLng;)LX/7c9;

    move-result-object v8

    new-instance v9, Lcom/google/android/gms/maps/model/LatLng;

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v10

    add-double/2addr v0, v4

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    invoke-direct {v9, v0, v1, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v8, v9}, LX/7c9;->a(Lcom/google/android/gms/maps/model/LatLng;)LX/7c9;

    move-result-object v0

    invoke-virtual {v0}, LX/7c9;->a()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/7au;)V
    .locals 2

    .prologue
    .line 2672893
    invoke-virtual {p0}, LX/7au;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    move-object v1, p0

    check-cast v1, LX/JHM;

    invoke-virtual {v0, v1}, LX/5pX;->b(LX/5pQ;)V

    .line 2672894
    invoke-virtual {p0}, LX/7au;->d()V

    .line 2672895
    invoke-virtual {p0}, LX/7au;->f()V

    .line 2672896
    return-void
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2672890
    :try_start_0
    sget-object v1, LX/1vX;->c:LX/1vX;

    move-object v1, v1

    .line 2672891
    invoke-virtual {v1, p0}, LX/1od;->a(Landroid/content/Context;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 2672892
    :cond_0
    :goto_0
    return v0

    :catch_0
    goto :goto_0
.end method

.method private b(LX/5rJ;)LX/7au;
    .locals 2

    .prologue
    .line 2672878
    sget-object v0, LX/1vX;->c:LX/1vX;

    move-object v0, v0

    .line 2672879
    invoke-virtual {v0, p1}, LX/1od;->a(Landroid/content/Context;)I

    move-result v0

    .line 2672880
    if-eqz v0, :cond_0

    .line 2672881
    new-instance v0, LX/5p9;

    const-string v1, "Google Maps are not available on this device"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2672882
    :cond_0
    invoke-static {p1}, LX/7av;->a(Landroid/content/Context;)I

    .line 2672883
    new-instance v0, LX/JHM;

    invoke-direct {v0, p1}, LX/JHM;-><init>(Landroid/content/Context;)V

    .line 2672884
    sget-object v1, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, LX/7au;->a(Landroid/os/Bundle;)V

    .line 2672885
    invoke-virtual {v0}, LX/7au;->b()V

    .line 2672886
    new-instance v1, LX/JHN;

    invoke-direct {v1, p0, v0}, LX/JHN;-><init>(Lcom/facebook/catalyst/views/maps/ReactMapViewManager;LX/JHM;)V

    invoke-virtual {v0, v1}, LX/7au;->a(LX/6an;)V

    .line 2672887
    invoke-virtual {p1, v0}, LX/5pX;->a(LX/5pQ;)V

    .line 2672888
    return-object v0
.end method

.method public static b(LX/7ax;)V
    .locals 1

    .prologue
    .line 2672873
    invoke-virtual {p0}, LX/7ax;->b()Z

    move-result v0

    sput-boolean v0, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->d:Z

    .line 2672874
    invoke-virtual {p0}, LX/7ax;->d()Z

    move-result v0

    sput-boolean v0, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->e:Z

    .line 2672875
    invoke-virtual {p0}, LX/7ax;->a()Z

    move-result v0

    sput-boolean v0, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->f:Z

    .line 2672876
    invoke-virtual {p0}, LX/7ax;->c()Z

    move-result v0

    sput-boolean v0, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->g:Z

    .line 2672877
    return-void
.end method

.method public static setRotateEnabled(LX/7au;Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "rotateEnabled"
    .end annotation

    .prologue
    .line 2672871
    new-instance v0, LX/JHQ;

    invoke-direct {v0, p1}, LX/JHQ;-><init>(Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, LX/7au;->a(LX/6an;)V

    .line 2672872
    return-void
.end method

.method public static setScrollEnabled(LX/7au;Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "scrollEnabled"
    .end annotation

    .prologue
    .line 2672907
    new-instance v0, LX/JHR;

    invoke-direct {v0, p1}, LX/JHR;-><init>(Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, LX/7au;->a(LX/6an;)V

    .line 2672908
    return-void
.end method

.method public static setShowsUserLocation(LX/7au;Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "showsUserLocation"
    .end annotation

    .prologue
    .line 2672869
    new-instance v0, LX/JHO;

    invoke-direct {v0, p1}, LX/JHO;-><init>(Z)V

    invoke-virtual {p0, v0}, LX/7au;->a(LX/6an;)V

    .line 2672870
    return-void
.end method

.method public static setZoomEnabled(LX/7au;Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "zoomEnabled"
    .end annotation

    .prologue
    .line 2672867
    new-instance v0, LX/JHP;

    invoke-direct {v0, p1}, LX/JHP;-><init>(Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, LX/7au;->a(LX/6an;)V

    .line 2672868
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2672866
    invoke-direct {p0, p1}, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->b(LX/5rJ;)LX/7au;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2672865
    check-cast p1, LX/7au;

    invoke-static {p1}, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->a(LX/7au;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2672847
    const-string v0, "RCTMap"

    return-object v0
.end method

.method public setActive(LX/7au;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "active"
    .end annotation

    .prologue
    .line 2672861
    if-eqz p2, :cond_0

    .line 2672862
    invoke-virtual {p1}, LX/7au;->b()V

    .line 2672863
    :goto_0
    return-void

    .line 2672864
    :cond_0
    invoke-virtual {p1}, LX/7au;->d()V

    goto :goto_0
.end method

.method public setPitchEnabled(LX/7au;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "pitchEnabled"
    .end annotation

    .prologue
    .line 2672859
    new-instance v0, LX/JHS;

    invoke-direct {v0, p0, p2}, LX/JHS;-><init>(Lcom/facebook/catalyst/views/maps/ReactMapViewManager;Ljava/lang/Boolean;)V

    invoke-virtual {p1, v0}, LX/7au;->a(LX/6an;)V

    .line 2672860
    return-void
.end method

.method public setRegion(LX/7au;LX/5pG;)V
    .locals 1
    .param p2    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "region"
    .end annotation

    .prologue
    .line 2672851
    if-eqz p2, :cond_0

    .line 2672852
    check-cast p1, LX/JHM;

    invoke-static {p2}, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->a(LX/5pG;)Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    .line 2672853
    invoke-virtual {p1}, LX/JHM;->getWidth()I

    move-result p0

    .line 2672854
    invoke-virtual {p1}, LX/JHM;->getHeight()I

    move-result p2

    .line 2672855
    if-lez p0, :cond_1

    if-lez p2, :cond_1

    .line 2672856
    invoke-static {p1, v0, p0, p2}, LX/JHM;->a(LX/JHM;Lcom/google/android/gms/maps/model/LatLngBounds;II)V

    .line 2672857
    :cond_0
    :goto_0
    return-void

    .line 2672858
    :cond_1
    iput-object v0, p1, LX/JHM;->a:Lcom/google/android/gms/maps/model/LatLngBounds;

    goto :goto_0
.end method

.method public final y()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2672848
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2672849
    const-string v1, "IsGoogleMapsAvailable"

    iget-object v2, p0, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->c:LX/5pX;

    invoke-static {v2}, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;->a(Landroid/content/Context;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2672850
    return-object v0
.end method
