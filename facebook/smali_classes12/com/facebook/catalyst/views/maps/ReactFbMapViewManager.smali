.class public Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTFbMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "Lcom/facebook/android/maps/MapView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Landroid/os/Bundle;

.field public static b:Z

.field public static c:Z

.field public static d:Z

.field public static e:Z

.field public static f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2672703
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;->a:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2672701
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    .line 2672702
    return-void
.end method

.method private static a(LX/5pG;)LX/697;
    .locals 14

    .prologue
    .line 2672694
    const-string v0, "latitude"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "latitudeDelta"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "longitude"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "longitudeDelta"

    invoke-interface {p0, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2672695
    :cond_0
    new-instance v0, LX/5pA;

    const-string v1, "Region description is invalid"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2672696
    :cond_1
    const-string v0, "latitude"

    invoke-interface {p0, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 2672697
    const-string v2, "longitude"

    invoke-interface {p0, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 2672698
    const-string v4, "latitudeDelta"

    invoke-interface {p0, v4}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 2672699
    const-string v6, "longitudeDelta"

    invoke-interface {p0, v6}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 2672700
    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v8

    new-instance v9, Lcom/facebook/android/maps/model/LatLng;

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v10, v4

    sub-double v10, v0, v10

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v12, v6

    sub-double v12, v2, v12

    invoke-direct {v9, v10, v11, v12, v13}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v8, v9}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v8

    new-instance v9, Lcom/facebook/android/maps/model/LatLng;

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v10

    add-double/2addr v0, v4

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    invoke-direct {v9, v0, v1, v2, v3}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v8, v9}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v0

    invoke-virtual {v0}, LX/696;->a()LX/697;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/android/maps/MapView;)V
    .locals 1

    .prologue
    .line 2672692
    invoke-virtual {p0}, Lcom/facebook/android/maps/MapView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    check-cast p0, LX/JHB;

    invoke-virtual {v0, p0}, LX/5pX;->b(LX/5pQ;)V

    .line 2672693
    return-void
.end method

.method private b(LX/5rJ;)Lcom/facebook/android/maps/MapView;
    .locals 2

    .prologue
    .line 2672687
    new-instance v0, LX/JHB;

    invoke-direct {v0, p1}, LX/JHB;-><init>(Landroid/content/Context;)V

    .line 2672688
    sget-object v1, Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/MapView;->a(Landroid/os/Bundle;)V

    .line 2672689
    new-instance v1, LX/JHD;

    invoke-direct {v1, p0, v0}, LX/JHD;-><init>(Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;LX/JHB;)V

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2672690
    invoke-virtual {p1, v0}, LX/5pX;->a(LX/5pQ;)V

    .line 2672691
    return-object v0
.end method

.method public static b(LX/68Q;)V
    .locals 1

    .prologue
    .line 2672678
    iget-boolean v0, p0, LX/68Q;->e:Z

    move v0, v0

    .line 2672679
    sput-boolean v0, Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;->c:Z

    .line 2672680
    iget-boolean v0, p0, LX/68Q;->b:Z

    move v0, v0

    .line 2672681
    sput-boolean v0, Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;->d:Z

    .line 2672682
    iget-boolean v0, p0, LX/68Q;->c:Z

    move v0, v0

    .line 2672683
    sput-boolean v0, Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;->e:Z

    .line 2672684
    iget-boolean v0, p0, LX/68Q;->d:Z

    move v0, v0

    .line 2672685
    sput-boolean v0, Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;->f:Z

    .line 2672686
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2672677
    invoke-direct {p0, p1}, Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;->b(LX/5rJ;)Lcom/facebook/android/maps/MapView;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2672676
    check-cast p1, Lcom/facebook/android/maps/MapView;

    invoke-static {p1}, Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;->a(Lcom/facebook/android/maps/MapView;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2672704
    const-string v0, "RCTFbMap"

    return-object v0
.end method

.method public setActive(Lcom/facebook/android/maps/MapView;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "active"
    .end annotation

    .prologue
    .line 2672675
    return-void
.end method

.method public setAnnotations(Lcom/facebook/android/maps/MapView;LX/5pC;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "annotations"
    .end annotation

    .prologue
    .line 2672673
    new-instance v0, LX/JHJ;

    invoke-direct {v0, p0, p2, p1}, LX/JHJ;-><init>(Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;LX/5pC;Lcom/facebook/android/maps/MapView;)V

    invoke-virtual {p1, v0}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2672674
    return-void
.end method

.method public setPitchEnabled(Lcom/facebook/android/maps/MapView;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "pitchEnabled"
    .end annotation

    .prologue
    .line 2672671
    new-instance v0, LX/JHI;

    invoke-direct {v0, p0, p2}, LX/JHI;-><init>(Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;Ljava/lang/Boolean;)V

    invoke-virtual {p1, v0}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2672672
    return-void
.end method

.method public setRegion(Lcom/facebook/android/maps/MapView;LX/5pG;)V
    .locals 1
    .param p2    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "region"
    .end annotation

    .prologue
    .line 2672667
    if-eqz p2, :cond_0

    .line 2672668
    check-cast p1, LX/JHB;

    invoke-static {p2}, Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;->a(LX/5pG;)LX/697;

    move-result-object v0

    .line 2672669
    new-instance p0, LX/JH9;

    invoke-direct {p0, p1, v0}, LX/JH9;-><init>(LX/JHB;LX/697;)V

    invoke-virtual {p1, p0}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2672670
    :cond_0
    return-void
.end method

.method public setRotateEnabled(Lcom/facebook/android/maps/MapView;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "rotateEnabled"
    .end annotation

    .prologue
    .line 2672665
    new-instance v0, LX/JHG;

    invoke-direct {v0, p0, p2}, LX/JHG;-><init>(Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;Ljava/lang/Boolean;)V

    invoke-virtual {p1, v0}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2672666
    return-void
.end method

.method public setScrollEnabled(Lcom/facebook/android/maps/MapView;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "scrollEnabled"
    .end annotation

    .prologue
    .line 2672663
    new-instance v0, LX/JHH;

    invoke-direct {v0, p0, p2}, LX/JHH;-><init>(Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;Ljava/lang/Boolean;)V

    invoke-virtual {p1, v0}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2672664
    return-void
.end method

.method public setShowsUserLocation(Lcom/facebook/android/maps/MapView;Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "showsUserLocation"
    .end annotation

    .prologue
    .line 2672661
    new-instance v0, LX/JHE;

    invoke-direct {v0, p0, p2}, LX/JHE;-><init>(Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;Z)V

    invoke-virtual {p1, v0}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2672662
    return-void
.end method

.method public setZoomEnabled(Lcom/facebook/android/maps/MapView;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "zoomEnabled"
    .end annotation

    .prologue
    .line 2672659
    new-instance v0, LX/JHF;

    invoke-direct {v0, p0, p2}, LX/JHF;-><init>(Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;Ljava/lang/Boolean;)V

    invoke-virtual {p1, v0}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2672660
    return-void
.end method
