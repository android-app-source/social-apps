.class public final Lcom/facebook/catalyst/shadow/flat/RCTImageViewManager;
.super Lcom/facebook/catalyst/shadow/flat/FlatViewManager;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2670020
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatViewManager;-><init>()V

    return-void
.end method

.method private static B()Lcom/facebook/catalyst/shadow/flat/RCTImageView;
    .locals 2

    .prologue
    .line 2670019
    new-instance v0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;

    new-instance v1, LX/JGW;

    invoke-direct {v1}, LX/JGW;-><init>()V

    invoke-direct {v0, v1}, Lcom/facebook/catalyst/shadow/flat/RCTImageView;-><init>(LX/JGN;)V

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2670018
    const-string v0, "RCTImageView"

    return-object v0
.end method

.method public final synthetic h()Lcom/facebook/react/uimanager/LayoutShadowNode;
    .locals 1

    .prologue
    .line 2670015
    invoke-static {}, Lcom/facebook/catalyst/shadow/flat/RCTImageViewManager;->B()Lcom/facebook/catalyst/shadow/flat/RCTImageView;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/catalyst/shadow/flat/RCTImageView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2670017
    const-class v0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;

    return-object v0
.end method

.method public final synthetic s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 2670016
    invoke-static {}, Lcom/facebook/catalyst/shadow/flat/RCTImageViewManager;->B()Lcom/facebook/catalyst/shadow/flat/RCTImageView;

    move-result-object v0

    return-object v0
.end method
