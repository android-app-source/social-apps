.class public Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;
.super Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;
.source ""


# instance fields
.field private d:LX/JGt;

.field private e:LX/JH1;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2670474
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;-><init>()V

    .line 2670475
    sget-object v0, LX/JGt;->a:LX/JGt;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670476
    sget-object v0, LX/JH1;->a:LX/JH1;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    return-void
.end method

.method private final ak()LX/JH1;
    .locals 7

    .prologue
    .line 2670477
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    .line 2670478
    iget-boolean v1, v0, LX/JH1;->f:Z

    move v0, v1

    .line 2670479
    if-eqz v0, :cond_0

    .line 2670480
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    .line 2670481
    new-instance v1, LX/JH1;

    iget v2, v0, LX/JH1;->b:F

    iget v3, v0, LX/JH1;->c:F

    iget v4, v0, LX/JH1;->d:F

    iget v5, v0, LX/JH1;->e:I

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/JH1;-><init>(FFFIZ)V

    move-object v0, v1

    .line 2670482
    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    .line 2670483
    :cond_0
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2670484
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const-string v0, "00"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x31

    if-lt v0, v1, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    mul-int/lit8 v0, v0, 0x64

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static h(F)I
    .locals 2

    .prologue
    .line 2670485
    invoke-static {p0}, LX/5r2;->b(F)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method public final a(LX/JH2;)V
    .locals 3

    .prologue
    .line 2670486
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2670487
    invoke-virtual {p0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;

    .line 2670488
    invoke-virtual {v0, p1}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->a(LX/JH2;)V

    .line 2670489
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2670490
    :cond_0
    return-void
.end method

.method public final a(Landroid/text/SpannableStringBuilder;IIZ)V
    .locals 3

    .prologue
    .line 2670491
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670492
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/JGt;->j:Z

    .line 2670493
    if-eqz p4, :cond_1

    .line 2670494
    const/16 v0, 0x21

    .line 2670495
    :goto_0
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    invoke-virtual {p1, v1, p2, p3, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2670496
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    .line 2670497
    iget v2, v1, LX/JH1;->e:I

    move v1, v2

    .line 2670498
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    .line 2670499
    iget v2, v1, LX/JH1;->d:F

    move v1, v2

    .line 2670500
    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 2670501
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    .line 2670502
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/JH1;->f:Z

    .line 2670503
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    invoke-virtual {p1, v1, p2, p3, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2670504
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_3

    .line 2670505
    invoke-virtual {p0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;

    .line 2670506
    invoke-virtual {v0, p1, p4}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->a(Landroid/text/SpannableStringBuilder;Z)V

    .line 2670507
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2670508
    :cond_1
    if-nez p2, :cond_2

    const/16 v0, 0x12

    goto :goto_0

    :cond_2
    const/16 v0, 0x22

    goto :goto_0

    .line 2670509
    :cond_3
    return-void
.end method

.method public a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V
    .locals 1

    .prologue
    .line 2670353
    invoke-super {p0, p1, p2}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 2670354
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670355
    return-void
.end method

.method public af()I
    .locals 1

    .prologue
    .line 2670510
    const/4 v0, -0x1

    return v0
.end method

.method public final ag()I
    .locals 1

    .prologue
    .line 2670511
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670512
    iget p0, v0, LX/JGt;->f:I

    move v0, p0

    .line 2670513
    return v0
.end method

.method public final ah()I
    .locals 1

    .prologue
    .line 2670514
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670515
    iget p0, v0, LX/JGt;->g:I

    move v0, p0

    .line 2670516
    if-ltz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ai()LX/JGt;
    .locals 13

    .prologue
    .line 2670467
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670468
    iget-boolean v1, v0, LX/JGt;->j:Z

    move v0, v1

    .line 2670469
    if-eqz v0, :cond_0

    .line 2670470
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670471
    new-instance v2, LX/JGt;

    iget-wide v3, v0, LX/JGt;->b:D

    iget v5, v0, LX/JGt;->c:I

    iget v6, v0, LX/JGt;->f:I

    iget v7, v0, LX/JGt;->g:I

    iget v8, v0, LX/JGt;->h:I

    iget-boolean v9, v0, LX/JGt;->d:Z

    iget-boolean v10, v0, LX/JGt;->e:Z

    iget-object v11, v0, LX/JGt;->i:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-direct/range {v2 .. v12}, LX/JGt;-><init>(DIIIIZZLjava/lang/String;Z)V

    move-object v0, v2

    .line 2670472
    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670473
    :cond_0
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    return-object v0
.end method

.method public final aj()Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 2670517
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2670518
    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->a(Landroid/text/SpannableStringBuilder;)V

    .line 2670519
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->ae()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->a(Landroid/text/SpannableStringBuilder;Z)V

    .line 2670520
    return-object v0
.end method

.method public b(Landroid/text/SpannableStringBuilder;)V
    .locals 3

    .prologue
    .line 2670462
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2670463
    invoke-virtual {p0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;

    .line 2670464
    invoke-virtual {v0, p1}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->a(Landroid/text/SpannableStringBuilder;)V

    .line 2670465
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2670466
    :cond_0
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2

    .prologue
    .line 2670453
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2670454
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670455
    iget v1, v0, LX/JGt;->c:I

    move v0, v1

    .line 2670456
    if-eq v0, p1, :cond_0

    .line 2670457
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ai()LX/JGt;

    move-result-object v0

    .line 2670458
    iput p1, v0, LX/JGt;->c:I

    .line 2670459
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670460
    :cond_0
    :goto_0
    return-void

    .line 2670461
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public setColor(D)V
    .locals 5
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        a = NaN
        name = "color"
    .end annotation

    .prologue
    .line 2670446
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670447
    iget-wide v3, v0, LX/JGt;->b:D

    move-wide v0, v3

    .line 2670448
    cmpl-double v0, v0, p1

    if-eqz v0, :cond_0

    .line 2670449
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ai()LX/JGt;

    move-result-object v0

    .line 2670450
    iput-wide p1, v0, LX/JGt;->b:D

    .line 2670451
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670452
    :cond_0
    return-void
.end method

.method public setFontFamily(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fontFamily"
    .end annotation

    .prologue
    .line 2670439
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670440
    iget-object v1, v0, LX/JGt;->i:Ljava/lang/String;

    move-object v0, v1

    .line 2670441
    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2670442
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ai()LX/JGt;

    move-result-object v0

    .line 2670443
    iput-object p1, v0, LX/JGt;->i:Ljava/lang/String;

    .line 2670444
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670445
    :cond_0
    return-void
.end method

.method public setFontSize(F)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        b = NaNf
        name = "fontSize"
    .end annotation

    .prologue
    .line 2670429
    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2670430
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->af()I

    move-result v0

    .line 2670431
    :goto_0
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670432
    iget p1, v1, LX/JGt;->f:I

    move v1, p1

    .line 2670433
    if-eq v1, v0, :cond_0

    .line 2670434
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ai()LX/JGt;

    move-result-object v1

    .line 2670435
    iput v0, v1, LX/JGt;->f:I

    .line 2670436
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670437
    :cond_0
    return-void

    .line 2670438
    :cond_1
    invoke-static {p1}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->h(F)I

    move-result v0

    goto :goto_0
.end method

.method public setFontStyle(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fontStyle"
    .end annotation

    .prologue
    .line 2670415
    if-nez p1, :cond_1

    .line 2670416
    const/4 v0, -0x1

    .line 2670417
    :goto_0
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670418
    iget v2, v1, LX/JGt;->g:I

    move v1, v2

    .line 2670419
    if-eq v1, v0, :cond_0

    .line 2670420
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ai()LX/JGt;

    move-result-object v1

    .line 2670421
    iput v0, v1, LX/JGt;->g:I

    .line 2670422
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670423
    :cond_0
    return-void

    .line 2670424
    :cond_1
    const-string v0, "italic"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2670425
    const/4 v0, 0x2

    goto :goto_0

    .line 2670426
    :cond_2
    const-string v0, "normal"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2670427
    const/4 v0, 0x0

    goto :goto_0

    .line 2670428
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid font style "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setFontWeight(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fontWeight"
    .end annotation

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x1

    .line 2670400
    if-nez p1, :cond_1

    .line 2670401
    :goto_0
    iget-object v2, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670402
    iget p1, v2, LX/JGt;->h:I

    move v2, p1

    .line 2670403
    if-eq v2, v0, :cond_0

    .line 2670404
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ai()LX/JGt;

    move-result-object v2

    .line 2670405
    iput v0, v2, LX/JGt;->h:I

    .line 2670406
    invoke-virtual {p0, v1}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670407
    :cond_0
    return-void

    .line 2670408
    :cond_1
    const-string v2, "bold"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 2670409
    goto :goto_0

    .line 2670410
    :cond_2
    const-string v2, "normal"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2670411
    invoke-static {p1}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->b(Ljava/lang/String;)I

    move-result v2

    .line 2670412
    if-ne v2, v0, :cond_3

    .line 2670413
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid font weight "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2670414
    :cond_3
    const/16 v0, 0x1f4

    if-lt v2, v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTextDecorationLine(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "textDecorationLine"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2670384
    if-eqz p1, :cond_2

    .line 2670385
    const-string v2, " "

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 2670386
    const-string v7, "underline"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v2, v1

    .line 2670387
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2670388
    :cond_1
    const-string v7, "line-through"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v0, v1

    .line 2670389
    goto :goto_1

    :cond_2
    move v2, v0

    .line 2670390
    :cond_3
    iget-object v3, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670391
    iget-boolean v4, v3, LX/JGt;->d:Z

    move v3, v4

    .line 2670392
    if-ne v2, v3, :cond_4

    iget-object v3, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->d:LX/JGt;

    .line 2670393
    iget-boolean v4, v3, LX/JGt;->e:Z

    move v3, v4

    .line 2670394
    if-eq v0, v3, :cond_5

    .line 2670395
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ai()LX/JGt;

    move-result-object v3

    .line 2670396
    iput-boolean v2, v3, LX/JGt;->d:Z

    .line 2670397
    iput-boolean v0, v3, LX/JGt;->e:Z

    .line 2670398
    invoke-virtual {p0, v1}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670399
    :cond_5
    return-void
.end method

.method public setTextShadowColor(I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x55000000
        customType = "Color"
        name = "textShadowColor"
    .end annotation

    .prologue
    .line 2670377
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    .line 2670378
    iget v1, v0, LX/JH1;->e:I

    move v0, v1

    .line 2670379
    if-eq v0, p1, :cond_0

    .line 2670380
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ak()LX/JH1;

    move-result-object v0

    .line 2670381
    iput p1, v0, LX/JH1;->e:I

    .line 2670382
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670383
    :cond_0
    return-void
.end method

.method public setTextShadowOffset(LX/5pG;)V
    .locals 4
    .param p1    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "textShadowOffset"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2670364
    if-eqz p1, :cond_3

    .line 2670365
    const-string v0, "width"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2670366
    const-string v0, "width"

    invoke-interface {p1, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v0

    .line 2670367
    :goto_0
    const-string v2, "height"

    invoke-interface {p1, v2}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2670368
    const-string v1, "height"

    invoke-interface {p1, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v1

    .line 2670369
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    .line 2670370
    iget v3, v2, LX/JH1;->b:F

    cmpl-float v3, v3, v0

    if-nez v3, :cond_4

    iget v3, v2, LX/JH1;->c:F

    cmpl-float v3, v3, v1

    if-nez v3, :cond_4

    const/4 v3, 0x1

    :goto_2
    move v2, v3

    .line 2670371
    if-nez v2, :cond_1

    .line 2670372
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ak()LX/JH1;

    move-result-object v2

    .line 2670373
    iput v0, v2, LX/JH1;->b:F

    .line 2670374
    iput v1, v2, LX/JH1;->c:F

    .line 2670375
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670376
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public setTextShadowRadius(F)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "textShadowRadius"
    .end annotation

    .prologue
    .line 2670356
    invoke-static {p1}, LX/5r2;->a(F)F

    move-result v0

    .line 2670357
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->e:LX/JH1;

    .line 2670358
    iget p1, v1, LX/JH1;->d:F

    move v1, p1

    .line 2670359
    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    .line 2670360
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ak()LX/JH1;

    move-result-object v1

    .line 2670361
    iput v0, v1, LX/JH1;->d:F

    .line 2670362
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670363
    :cond_0
    return-void
.end method
