.class public final Lcom/facebook/catalyst/shadow/flat/RCTRawText;
.super Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;
.source ""


# instance fields
.field private d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2670219
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/JH2;)V
    .locals 0

    .prologue
    .line 2670220
    return-void
.end method

.method public final a(Landroid/text/SpannableStringBuilder;IIZ)V
    .locals 1

    .prologue
    .line 2670217
    const/16 v0, 0x11

    invoke-virtual {p1, p0, p2, p3, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2670218
    return-void
.end method

.method public final b(Landroid/text/SpannableStringBuilder;)V
    .locals 1

    .prologue
    .line 2670214
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTRawText;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2670215
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTRawText;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2670216
    :cond_0
    return-void
.end method

.method public final setText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "text"
    .end annotation

    .prologue
    .line 2670211
    iput-object p1, p0, Lcom/facebook/catalyst/shadow/flat/RCTRawText;->d:Ljava/lang/String;

    .line 2670212
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670213
    return-void
.end method
