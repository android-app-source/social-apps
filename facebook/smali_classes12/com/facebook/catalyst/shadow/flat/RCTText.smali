.class public final Lcom/facebook/catalyst/shadow/flat/RCTText;
.super Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;
.source ""

# interfaces
.implements Lcom/facebook/csslayout/YogaMeasureFunction;


# static fields
.field private static final d:LX/1nq;


# instance fields
.field private e:Ljava/lang/CharSequence;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/JGX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:F

.field private h:F

.field private i:I

.field private j:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2670577
    new-instance v0, LX/1nq;

    invoke-direct {v0}, LX/1nq;-><init>()V

    const/4 v1, 0x0

    .line 2670578
    iput-boolean v1, v0, LX/1nq;->e:Z

    .line 2670579
    move-object v0, v0

    .line 2670580
    const/4 v1, 0x1

    .line 2670581
    iput-boolean v1, v0, LX/1nq;->f:Z

    .line 2670582
    move-object v0, v0

    .line 2670583
    new-instance v1, LX/JMc;

    invoke-direct {v1}, LX/JMc;-><init>()V

    .line 2670584
    iput-object v1, v0, LX/1nq;->d:LX/1WA;

    .line 2670585
    move-object v0, v0

    .line 2670586
    sput-object v0, Lcom/facebook/catalyst/shadow/flat/RCTText;->d:LX/1nq;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2670587
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;-><init>()V

    .line 2670588
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->g:F

    .line 2670589
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->h:F

    .line 2670590
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->i:I

    .line 2670591
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->j:I

    .line 2670592
    invoke-virtual {p0, p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaMeasureFunction;)V

    .line 2670593
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ai()LX/JGt;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTText;->af()I

    move-result v1

    .line 2670594
    iput v1, v0, LX/JGt;->f:I

    .line 2670595
    return-void
.end method

.method private static a(ILcom/facebook/csslayout/YogaMeasureMode;Landroid/text/TextUtils$TruncateAt;ZIZLjava/lang/CharSequence;IFFILandroid/text/Layout$Alignment;)Landroid/text/Layout;
    .locals 3

    .prologue
    .line 2670596
    sget-object v0, LX/JH0;->a:[I

    invoke-virtual {p1}, Lcom/facebook/csslayout/YogaMeasureMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2670597
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected size mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2670598
    :pswitch_0
    const/4 v0, 0x0

    .line 2670599
    :goto_0
    sget-object v1, Lcom/facebook/catalyst/shadow/flat/RCTText;->d:LX/1nq;

    invoke-virtual {v1, p2}, LX/1nq;->a(Landroid/text/TextUtils$TruncateAt;)LX/1nq;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/1nq;->f(I)LX/1nq;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/1nq;->b(Z)LX/1nq;

    move-result-object v1

    invoke-virtual {v1, p6}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    move-result-object v1

    invoke-virtual {v1, p7}, LX/1nq;->b(I)LX/1nq;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, LX/1nq;->a(II)LX/1nq;

    .line 2670600
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/RCTText;->d:LX/1nq;

    invoke-virtual {v0, p10}, LX/1nq;->e(I)LX/1nq;

    .line 2670601
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/RCTText;->d:LX/1nq;

    sget-object v1, LX/0zo;->c:LX/0zr;

    invoke-virtual {v0, v1}, LX/1nq;->a(LX/0zr;)LX/1nq;

    .line 2670602
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/RCTText;->d:LX/1nq;

    invoke-virtual {v0, p3}, LX/1nq;->a(Z)LX/1nq;

    .line 2670603
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/RCTText;->d:LX/1nq;

    invoke-virtual {v0, p8}, LX/1nq;->a(F)LX/1nq;

    .line 2670604
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/RCTText;->d:LX/1nq;

    invoke-virtual {v0, p9}, LX/1nq;->b(F)LX/1nq;

    .line 2670605
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/RCTText;->d:LX/1nq;

    invoke-virtual {v0, p11}, LX/1nq;->a(Landroid/text/Layout$Alignment;)LX/1nq;

    .line 2670606
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/RCTText;->d:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v0

    .line 2670607
    sget-object v1, Lcom/facebook/catalyst/shadow/flat/RCTText;->d:LX/1nq;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    .line 2670608
    return-object v0

    .line 2670609
    :pswitch_1
    const/4 v0, 0x1

    .line 2670610
    goto :goto_0

    .line 2670611
    :pswitch_2
    const/4 v0, 0x2

    .line 2670612
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private ak()Landroid/text/Layout$Alignment;
    .locals 4

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x3

    .line 2670613
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->F()Lcom/facebook/csslayout/YogaDirection;

    move-result-object v0

    sget-object v3, Lcom/facebook/csslayout/YogaDirection;->RTL:Lcom/facebook/csslayout/YogaDirection;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    .line 2670614
    :goto_0
    iget v3, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->j:I

    sparse-switch v3, :sswitch_data_0

    .line 2670615
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    :goto_1
    return-object v0

    .line 2670616
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2670617
    :sswitch_0
    if-eqz v0, :cond_1

    move v0, v1

    .line 2670618
    :goto_2
    invoke-static {}, Landroid/text/Layout$Alignment;->values()[Landroid/text/Layout$Alignment;

    move-result-object v1

    aget-object v0, v1, v0

    goto :goto_1

    :cond_1
    move v0, v2

    .line 2670619
    goto :goto_2

    .line 2670620
    :sswitch_1
    if-eqz v0, :cond_2

    .line 2670621
    :goto_3
    invoke-static {}, Landroid/text/Layout$Alignment;->values()[Landroid/text/Layout$Alignment;

    move-result-object v0

    aget-object v0, v0, v2

    goto :goto_1

    :cond_2
    move v2, v1

    .line 2670622
    goto :goto_3

    .line 2670623
    :sswitch_2
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x5 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public final a(FFFFZ)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2670521
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    move-object v0, v0

    .line 2670522
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    if-nez v1, :cond_1

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 2670523
    invoke-virtual/range {v0 .. v5}, LX/JGu;->a(FFFFZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2670524
    new-instance v0, LX/JH3;

    .line 2670525
    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v5, v1

    .line 2670526
    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, LX/JH3;-><init>(FFFFIZLandroid/text/Layout;)V

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(LX/JGu;)V

    .line 2670527
    :cond_0
    :goto_0
    return-void

    .line 2670528
    :cond_1
    instance-of v1, v0, LX/JH3;

    if-eqz v1, :cond_3

    move-object v1, v0

    .line 2670529
    check-cast v1, LX/JH3;

    .line 2670530
    iget-object v2, v1, LX/JH3;->e:Landroid/text/Layout;

    move-object v7, v2

    .line 2670531
    move-object v6, v7

    .line 2670532
    :goto_1
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    .line 2670533
    iget-object v2, v1, LX/JGX;->c:Landroid/text/Layout;

    move-object v7, v2

    .line 2670534
    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    .line 2670535
    invoke-virtual/range {v0 .. v5}, LX/JGu;->a(FFFFZ)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eq v6, v7, :cond_0

    .line 2670536
    :cond_2
    new-instance v0, LX/JH3;

    .line 2670537
    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v5, v1

    .line 2670538
    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, LX/JH3;-><init>(FFFFIZLandroid/text/Layout;)V

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(LX/JGu;)V

    goto :goto_0

    :cond_3
    move-object v6, v7

    goto :goto_1
.end method

.method public final a(LX/JH2;FFFFFFFF)V
    .locals 13

    .prologue
    .line 2670624
    invoke-super/range {p0 .. p9}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->a(LX/JH2;FFFFFFFF)V

    .line 2670625
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    .line 2670626
    sub-float v0, p5, p3

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    sub-float v0, p4, p2

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 2670627
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->aj()Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2670628
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2670629
    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->e:Ljava/lang/CharSequence;

    .line 2670630
    :cond_0
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    .line 2670631
    :goto_0
    return-void

    .line 2670632
    :cond_1
    const/4 v0, 0x0

    .line 2670633
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    if-nez v1, :cond_4

    .line 2670634
    new-instance v12, LX/JGX;

    sub-float v0, p4, p2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    sget-object v1, Lcom/facebook/csslayout/YogaMeasureMode;->EXACTLY:Lcom/facebook/csslayout/YogaMeasureMode;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    const/4 v3, 0x1

    iget v4, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->i:I

    iget v5, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->i:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    const/4 v5, 0x1

    :goto_1
    iget-object v6, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->e:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ag()I

    move-result v7

    iget v8, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->h:F

    iget v9, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->g:F

    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ah()I

    move-result v10

    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTText;->ak()Landroid/text/Layout$Alignment;

    move-result-object v11

    invoke-static/range {v0 .. v11}, Lcom/facebook/catalyst/shadow/flat/RCTText;->a(ILcom/facebook/csslayout/YogaMeasureMode;Landroid/text/TextUtils$TruncateAt;ZIZLjava/lang/CharSequence;IFFILandroid/text/Layout$Alignment;)Landroid/text/Layout;

    move-result-object v0

    invoke-direct {v12, v0}, LX/JGX;-><init>(Landroid/text/Layout;)V

    iput-object v12, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    .line 2670635
    const/4 v0, 0x1

    move v9, v0

    .line 2670636
    :goto_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v0

    add-float v1, p2, v0

    .line 2670637
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v0

    add-float v2, p3, v0

    .line 2670638
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    invoke-virtual {v0}, LX/JGX;->b()F

    move-result v0

    add-float v3, v1, v0

    .line 2670639
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    invoke-virtual {v0}, LX/JGX;->c()F

    move-result v0

    add-float v4, v2, v0

    .line 2670640
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    invoke-virtual/range {v0 .. v8}, LX/JGN;->a(FFFFFFFF)LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGX;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    .line 2670641
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    invoke-virtual {p1, v0}, LX/JH2;->a(LX/JGN;)V

    .line 2670642
    if-eqz v9, :cond_2

    .line 2670643
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->S()LX/JGu;

    move-result-object v0

    .line 2670644
    instance-of v1, v0, LX/JH3;

    if-eqz v1, :cond_2

    .line 2670645
    check-cast v0, LX/JH3;

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    invoke-virtual {v1}, LX/JGX;->a()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/JH3;->a(Landroid/text/Layout;)V

    .line 2670646
    :cond_2
    invoke-virtual {p0, p1}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->a(LX/JH2;)V

    goto/16 :goto_0

    .line 2670647
    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    :cond_4
    move v9, v0

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2670575
    const/4 v0, 0x0

    return v0
.end method

.method public final af()I
    .locals 1

    .prologue
    .line 2670576
    const/high16 v0, 0x41600000    # 14.0f

    invoke-static {v0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->h(F)I

    move-result v0

    return v0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 2670573
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->h()V

    .line 2670574
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2670572
    const/4 v0, 0x1

    return v0
.end method

.method public final measure(LX/1mn;FLcom/facebook/csslayout/YogaMeasureMode;FLcom/facebook/csslayout/YogaMeasureMode;)J
    .locals 12

    .prologue
    .line 2670560
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->aj()Landroid/text/SpannableStringBuilder;

    move-result-object v6

    .line 2670561
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2670562
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->e:Ljava/lang/CharSequence;

    .line 2670563
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1Dr;->a(II)J

    move-result-wide v0

    .line 2670564
    :goto_0
    return-wide v0

    .line 2670565
    :cond_0
    iput-object v6, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->e:Ljava/lang/CharSequence;

    .line 2670566
    float-to-double v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    const/4 v3, 0x1

    iget v4, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->i:I

    iget v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->i:I

    const/4 v5, 0x1

    if-ne v1, v5, :cond_1

    const/4 v5, 0x1

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ag()I

    move-result v7

    iget v8, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->h:F

    iget v9, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->g:F

    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ah()I

    move-result v10

    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTText;->ak()Landroid/text/Layout$Alignment;

    move-result-object v11

    move-object v1, p3

    invoke-static/range {v0 .. v11}, Lcom/facebook/catalyst/shadow/flat/RCTText;->a(ILcom/facebook/csslayout/YogaMeasureMode;Landroid/text/TextUtils$TruncateAt;ZIZLjava/lang/CharSequence;IFFILandroid/text/Layout$Alignment;)Landroid/text/Layout;

    move-result-object v0

    .line 2670567
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    invoke-virtual {v1}, LX/JGN;->h()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2670568
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    invoke-virtual {v1, v0}, LX/JGX;->a(Landroid/text/Layout;)V

    .line 2670569
    :goto_2
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    invoke-virtual {v0}, LX/JGX;->b()F

    move-result v0

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    invoke-virtual {v1}, LX/JGX;->c()F

    move-result v1

    invoke-static {v0, v1}, LX/1Dr;->a(FF)J

    move-result-wide v0

    goto :goto_0

    .line 2670570
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 2670571
    :cond_2
    new-instance v1, LX/JGX;

    invoke-direct {v1, v0}, LX/JGX;-><init>(Landroid/text/Layout;)V

    iput-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->f:LX/JGX;

    goto :goto_2
.end method

.method public final setLineHeight(D)V
    .locals 3
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        a = NaN
        name = "lineHeight"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2670553
    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2670554
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->g:F

    .line 2670555
    iput v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->h:F

    .line 2670556
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/RCTText;->b(Z)V

    .line 2670557
    return-void

    .line 2670558
    :cond_0
    iput v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->g:F

    .line 2670559
    double-to-float v0, p1

    invoke-static {v0}, LX/5r2;->b(F)F

    move-result v0

    iput v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->h:F

    goto :goto_0
.end method

.method public final setNumberOfLines(I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x7fffffff
        name = "numberOfLines"
    .end annotation

    .prologue
    .line 2670550
    iput p1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->i:I

    .line 2670551
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/RCTText;->b(Z)V

    .line 2670552
    return-void
.end method

.method public final setTextAlign(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "textAlign"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2670539
    if-eqz p1, :cond_0

    const-string v0, "auto"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2670540
    :cond_0
    iput v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->j:I

    .line 2670541
    :goto_0
    invoke-virtual {p0, v1}, Lcom/facebook/catalyst/shadow/flat/RCTText;->b(Z)V

    .line 2670542
    return-void

    .line 2670543
    :cond_1
    const-string v0, "left"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2670544
    const/4 v0, 0x3

    iput v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->j:I

    goto :goto_0

    .line 2670545
    :cond_2
    const-string v0, "right"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2670546
    const/4 v0, 0x5

    iput v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->j:I

    goto :goto_0

    .line 2670547
    :cond_3
    const-string v0, "center"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2670548
    const/16 v0, 0x11

    iput v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTText;->j:I

    goto :goto_0

    .line 2670549
    :cond_4
    new-instance v0, LX/5pA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid textAlign: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0
.end method
