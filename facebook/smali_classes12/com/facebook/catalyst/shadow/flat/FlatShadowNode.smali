.class public Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;
.super Lcom/facebook/react/uimanager/LayoutShadowNode;
.source ""


# static fields
.field public static final a:[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

.field private static final d:Landroid/graphics/Rect;

.field private static final e:LX/JGY;


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field public b:F

.field public c:Z

.field private f:[LX/JGM;

.field private g:[LX/JGQ;

.field private h:[LX/JGu;

.field public i:[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

.field public j:LX/JGu;

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:Z

.field private q:LX/JGY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:LX/JGU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Z

.field private t:Z

.field private u:F

.field private v:F

.field private w:F

.field private x:F

.field private y:Z

.field private z:Landroid/graphics/Rect;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2667982
    new-array v0, v1, [Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    sput-object v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a:[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2667983
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->d:Landroid/graphics/Rect;

    .line 2667984
    new-instance v0, LX/JGY;

    invoke-direct {v0, v1}, LX/JGY;-><init>(I)V

    sput-object v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->e:LX/JGY;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2667985
    invoke-direct {p0}, Lcom/facebook/react/uimanager/LayoutShadowNode;-><init>()V

    .line 2667986
    sget-object v0, LX/JGM;->b:[LX/JGM;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->f:[LX/JGM;

    .line 2667987
    sget-object v0, LX/JGQ;->a:[LX/JGQ;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->g:[LX/JGQ;

    .line 2667988
    sget-object v0, LX/JGu;->a:[LX/JGu;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->h:[LX/JGu;

    .line 2667989
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a:[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->i:[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2667990
    sget-object v0, LX/JGu;->b:LX/JGu;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    .line 2667991
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->s:Z

    .line 2667992
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->d:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->z:Landroid/graphics/Rect;

    .line 2667993
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->c:Z

    return-void
.end method


# virtual methods
.method public final B()I
    .locals 1

    .prologue
    .line 2667994
    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->l:I

    return v0
.end method

.method public final C()I
    .locals 1

    .prologue
    .line 2667995
    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->m:I

    return v0
.end method

.method public final D()I
    .locals 2

    .prologue
    .line 2667996
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2667997
    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->n:I

    iget v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->l:I

    sub-int/2addr v0, v1

    .line 2667998
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    .line 2667999
    iget v1, v0, LX/JGu;->g:F

    move v0, v1

    .line 2668000
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    .line 2668001
    iget p0, v1, LX/JGu;->e:F

    move v1, p0

    .line 2668002
    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0
.end method

.method public final E()I
    .locals 2

    .prologue
    .line 2668003
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2668004
    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->o:I

    iget v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->m:I

    sub-int/2addr v0, v1

    .line 2668005
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    .line 2668006
    iget v1, v0, LX/JGu;->h:F

    move v0, v1

    .line 2668007
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    .line 2668008
    iget p0, v1, LX/JGu;->f:F

    move v1, p0

    .line 2668009
    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0
.end method

.method public final H()V
    .locals 4

    .prologue
    .line 2668010
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->t:Z

    if-eqz v0, :cond_1

    .line 2668011
    :cond_0
    return-void

    .line 2668012
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->t:Z

    .line 2668013
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v2

    move v1, v0

    :goto_0
    if-eq v1, v2, :cond_0

    .line 2668014
    invoke-virtual {p0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 2668015
    instance-of v3, v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    if-eqz v3, :cond_2

    .line 2668016
    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    invoke-virtual {v0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2668017
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final I()Z
    .locals 1

    .prologue
    .line 2668018
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->c:Z

    return v0
.end method

.method public final J()V
    .locals 1

    .prologue
    .line 2668019
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2668020
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->s:Z

    if-eqz v0, :cond_1

    .line 2668021
    :cond_0
    return-void

    .line 2668022
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->s:Z

    .line 2668023
    :cond_2
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 2668024
    if-eqz v0, :cond_0

    .line 2668025
    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    move-object p0, v0

    .line 2668026
    goto :goto_0
.end method

.method public final K()Z
    .locals 1

    .prologue
    .line 2668027
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->s:Z

    return v0
.end method

.method public final L()V
    .locals 1

    .prologue
    .line 2668028
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->s:Z

    .line 2668029
    return-void
.end method

.method public final M()[LX/JGM;
    .locals 1

    .prologue
    .line 2668030
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->f:[LX/JGM;

    return-object v0
.end method

.method public final N()[LX/JGQ;
    .locals 1

    .prologue
    .line 2668031
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->g:[LX/JGQ;

    return-object v0
.end method

.method public final O()[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;
    .locals 1

    .prologue
    .line 2668032
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->i:[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    return-object v0
.end method

.method public final Q()[LX/JGu;
    .locals 1

    .prologue
    .line 2668033
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->h:[LX/JGu;

    return-object v0
.end method

.method public final R()V
    .locals 14

    .prologue
    .line 2668034
    const/4 v1, 0x0

    .line 2668035
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    .line 2668036
    iget v2, v0, LX/JGu;->g:F

    move v0, v2

    .line 2668037
    iget-object v2, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    .line 2668038
    iget v3, v2, LX/JGu;->e:F

    move v2, v3

    .line 2668039
    sub-float/2addr v0, v2

    float-to-int v7, v0

    .line 2668040
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    .line 2668041
    iget v2, v0, LX/JGu;->h:F

    move v0, v2

    .line 2668042
    iget-object v2, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    .line 2668043
    iget v3, v2, LX/JGu;->f:F

    move v2, v3

    .line 2668044
    sub-float/2addr v0, v2

    float-to-int v8, v0

    .line 2668045
    const/4 v6, 0x0

    .line 2668046
    int-to-float v2, v7

    .line 2668047
    const/4 v3, 0x0

    .line 2668048
    int-to-float v4, v8

    .line 2668049
    const/4 v5, 0x0

    .line 2668050
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->c:Z

    if-nez v0, :cond_7

    if-lez v8, :cond_7

    if-lez v7, :cond_7

    .line 2668051
    iget-object v9, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->h:[LX/JGu;

    array-length v10, v9

    const/4 v0, 0x0

    move v13, v0

    move v0, v6

    move v6, v13

    :goto_0
    if-ge v6, v10, :cond_1

    aget-object v11, v9, v6

    .line 2668052
    iget v12, v11, LX/JGu;->e:F

    move v12, v12

    .line 2668053
    cmpg-float v12, v12, v0

    if-gez v12, :cond_0

    .line 2668054
    iget v0, v11, LX/JGu;->e:F

    move v0, v0

    .line 2668055
    const/4 v1, 0x1

    .line 2668056
    :cond_0
    iget v12, v11, LX/JGu;->g:F

    move v12, v12

    .line 2668057
    cmpl-float v12, v12, v2

    if-lez v12, :cond_a

    .line 2668058
    iget v1, v11, LX/JGu;->g:F

    move v1, v1

    .line 2668059
    const/4 v2, 0x1

    .line 2668060
    :goto_1
    iget v12, v11, LX/JGu;->f:F

    move v12, v12

    .line 2668061
    cmpg-float v12, v12, v3

    if-gez v12, :cond_9

    .line 2668062
    iget v2, v11, LX/JGu;->f:F

    move v2, v2

    .line 2668063
    const/4 v3, 0x1

    .line 2668064
    :goto_2
    iget v12, v11, LX/JGu;->h:F

    move v12, v12

    .line 2668065
    cmpl-float v12, v12, v4

    if-lez v12, :cond_8

    .line 2668066
    iget v3, v11, LX/JGu;->h:F

    move v3, v3

    .line 2668067
    const/4 v4, 0x1

    .line 2668068
    :goto_3
    add-int/lit8 v6, v6, 0x1

    move v13, v3

    move v3, v2

    move v2, v1

    move v1, v4

    move v4, v13

    goto :goto_0

    .line 2668069
    :cond_1
    if-eqz v1, :cond_7

    .line 2668070
    new-instance v5, Landroid/graphics/Rect;

    float-to-int v0, v0

    float-to-int v3, v3

    int-to-float v6, v7

    sub-float/2addr v2, v6

    float-to-int v2, v2

    int-to-float v6, v8

    sub-float/2addr v4, v6

    float-to-int v4, v4

    invoke-direct {v5, v0, v3, v2, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v2, v5

    move v3, v1

    .line 2668071
    :goto_4
    if-nez v3, :cond_2

    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    sget-object v1, LX/JGu;->b:LX/JGu;

    if-eq v0, v1, :cond_2

    .line 2668072
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v5

    .line 2668073
    const/4 v0, 0x0

    move v4, v0

    :goto_5
    if-ge v4, v5, :cond_2

    .line 2668074
    invoke-virtual {p0, v4}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 2668075
    instance-of v0, v1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    if-eqz v0, :cond_6

    move-object v0, v1

    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    iget-boolean v0, v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->y:Z

    if-eqz v0, :cond_6

    .line 2668076
    check-cast v1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    iget-object v1, v1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->z:Landroid/graphics/Rect;

    .line 2668077
    if-nez v2, :cond_5

    .line 2668078
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2668079
    :goto_6
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 2668080
    const/4 v1, 0x1

    move-object v2, v0

    move v0, v1

    .line 2668081
    :goto_7
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v3, v0

    goto :goto_5

    .line 2668082
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->y:Z

    if-eq v0, v3, :cond_4

    .line 2668083
    iput-boolean v3, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->y:Z

    .line 2668084
    if-nez v2, :cond_3

    sget-object v2, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->d:Landroid/graphics/Rect;

    :cond_3
    iput-object v2, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->z:Landroid/graphics/Rect;

    .line 2668085
    :cond_4
    return-void

    :cond_5
    move-object v0, v2

    goto :goto_6

    :cond_6
    move v0, v3

    goto :goto_7

    :cond_7
    move-object v2, v5

    move v3, v1

    goto :goto_4

    :cond_8
    move v13, v4

    move v4, v3

    move v3, v13

    goto :goto_3

    :cond_9
    move v13, v3

    move v3, v2

    move v2, v13

    goto :goto_2

    :cond_a
    move v13, v2

    move v2, v1

    move v1, v13

    goto/16 :goto_1
.end method

.method public final S()LX/JGu;
    .locals 1

    .prologue
    .line 2668086
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    return-object v0
.end method

.method public final X()V
    .locals 1

    .prologue
    .line 2667975
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2667976
    :cond_0
    :goto_0
    return-void

    .line 2667977
    :cond_1
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->q:LX/JGY;

    if-nez v0, :cond_0

    .line 2667978
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->e:LX/JGY;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->q:LX/JGY;

    .line 2667979
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->J()V

    .line 2667980
    sget-object v0, LX/JGu;->b:LX/JGu;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    goto :goto_0
.end method

.method public final Y()Z
    .locals 1

    .prologue
    .line 2667981
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->q:LX/JGY;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(FFFFFFFF)LX/JGY;
    .locals 14

    .prologue
    .line 2667903
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->q:LX/JGY;

    sget-object v1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->e:LX/JGY;

    if-ne v0, v1, :cond_0

    .line 2667904
    new-instance v0, LX/JGY;

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->l()I

    move-result v1

    invoke-direct {v0, v1}, LX/JGY;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->q:LX/JGY;

    .line 2667905
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->c:Z

    if-eqz v0, :cond_1

    iget v13, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->b:F

    .line 2667906
    :goto_0
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->q:LX/JGY;

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->z:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    add-float v5, p1, v1

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->z:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    add-float v6, p2, v1

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->z:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    add-float v7, p3, v1

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->z:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    add-float v8, p4, v1

    move v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v9, p5

    move/from16 v10, p6

    move/from16 v11, p7

    move/from16 v12, p8

    invoke-virtual/range {v0 .. v13}, LX/JGY;->a(FFFFFFFFFFFFF)LX/JGY;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->q:LX/JGY;

    .line 2667907
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->q:LX/JGY;

    return-object v0

    .line 2667908
    :cond_1
    const/4 v13, 0x0

    goto :goto_0
.end method

.method public a(FFFFZ)V
    .locals 7

    .prologue
    .line 2667909
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, LX/JGu;->a(FFFFZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2667910
    new-instance v0, LX/JGu;

    .line 2667911
    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v5, v1

    .line 2667912
    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, LX/JGu;-><init>(FFFFIZ)V

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(LX/JGu;)V

    .line 2667913
    :cond_0
    return-void
.end method

.method public final a(IIII)V
    .locals 0

    .prologue
    .line 2667914
    iput p1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->l:I

    .line 2667915
    iput p2, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->m:I

    .line 2667916
    iput p3, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->n:I

    .line 2667917
    iput p4, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->o:I

    .line 2667918
    return-void
.end method

.method public final a(LX/JGu;)V
    .locals 0

    .prologue
    .line 2667919
    iput-object p1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    .line 2667920
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->R()V

    .line 2667921
    return-void
.end method

.method public a(LX/JH2;FFFFFFFF)V
    .locals 9

    .prologue
    .line 2667922
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->r:LX/JGU;

    if-eqz v0, :cond_0

    .line 2667923
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->r:LX/JGU;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    invoke-virtual/range {v0 .. v8}, LX/JGN;->a(FFFFFFFF)LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGU;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->r:LX/JGU;

    .line 2667924
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->r:LX/JGU;

    invoke-virtual {p1, v0}, LX/JH2;->a(LX/JGN;)V

    .line 2667925
    :cond_0
    return-void
.end method

.method public a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V
    .locals 1

    .prologue
    .line 2667971
    invoke-super {p0, p1, p2}, Lcom/facebook/react/uimanager/LayoutShadowNode;->a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 2667972
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->t:Z

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    if-eqz v0, :cond_0

    .line 2667973
    check-cast p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    invoke-virtual {p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2667974
    :cond_0
    return-void
.end method

.method public final a([LX/JGM;)V
    .locals 0

    .prologue
    .line 2667926
    iput-object p1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->f:[LX/JGM;

    .line 2667927
    return-void
.end method

.method public final a([LX/JGQ;)V
    .locals 0

    .prologue
    .line 2667928
    iput-object p1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->g:[LX/JGQ;

    .line 2667929
    return-void
.end method

.method public final a([LX/JGu;)V
    .locals 0

    .prologue
    .line 2667930
    iput-object p1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->h:[LX/JGu;

    .line 2667931
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->R()V

    .line 2667932
    return-void
.end method

.method public final a(FFFF)Z
    .locals 1

    .prologue
    .line 2667933
    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->u:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->v:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->w:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->x:F

    cmpl-float v0, v0, p4

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final aa()V
    .locals 1

    .prologue
    .line 2667901
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->p:Z

    .line 2667902
    return-void
.end method

.method public ab()Z
    .locals 1

    .prologue
    .line 2667934
    const/4 v0, 0x0

    return v0
.end method

.method public ac()Z
    .locals 1

    .prologue
    .line 2667935
    const/4 v0, 0x0

    return v0
.end method

.method public final b(IIII)LX/5r1;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2667936
    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->A:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->B:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->C:I

    if-ne v0, p3, :cond_0

    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->D:I

    if-ne v0, p4, :cond_0

    .line 2667937
    const/4 v0, 0x0

    .line 2667938
    :goto_0
    return-object v0

    .line 2667939
    :cond_0
    iput p1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->A:I

    .line 2667940
    iput p2, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->B:I

    .line 2667941
    iput p3, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->C:I

    .line 2667942
    iput p4, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->D:I

    .line 2667943
    iget v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, v0

    .line 2667944
    invoke-static {v0, p1, p2, p3, p4}, LX/5r1;->a(IIIII)LX/5r1;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(FFFF)V
    .locals 0

    .prologue
    .line 2667945
    iput p1, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->u:F

    .line 2667946
    iput p2, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->v:F

    .line 2667947
    iput p3, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->w:F

    .line 2667948
    iput p4, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->x:F

    .line 2667949
    return-void
.end method

.method public b(LX/5rC;)V
    .locals 1

    .prologue
    .line 2667950
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2667951
    const-string v0, "decomposedMatrix"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "opacity"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "renderToHardwareTextureAndroid"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "testID"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "accessibilityLabel"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "accessibilityComponentType"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "accessibilityLiveRegion"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "transform"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "importantForAccessibility"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "removeClippedSubviews"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2667952
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2667953
    :cond_1
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 2667954
    invoke-super {p0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->f()V

    .line 2667955
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->s:Z

    .line 2667956
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->J()V

    .line 2667957
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "backgroundColor"
    .end annotation

    .prologue
    .line 2667958
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->r:LX/JGU;

    .line 2667959
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->J()V

    .line 2667960
    return-void

    .line 2667961
    :cond_0
    new-instance v0, LX/JGU;

    invoke-direct {v0, p1}, LX/JGU;-><init>(I)V

    goto :goto_0
.end method

.method public setOverflow(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2667962
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setOverflow(Ljava/lang/String;)V

    .line 2667963
    const-string v0, "hidden"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->c:Z

    .line 2667964
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->c:Z

    if-eqz v0, :cond_1

    .line 2667965
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->y:Z

    .line 2667966
    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->b:F

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 2667967
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2667968
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->J()V

    .line 2667969
    return-void

    .line 2667970
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->R()V

    goto :goto_0
.end method
