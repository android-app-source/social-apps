.class public final Lcom/facebook/catalyst/shadow/flat/RCTView$$PropsSetter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ShadowNodeSetter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ShadowNodeSetter",
        "<",
        "Lcom/facebook/catalyst/shadow/flat/RCTView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2671551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/catalyst/shadow/flat/RCTView;Ljava/lang/String;LX/5rC;)V
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/high16 v6, 0x7fc00000    # NaNf

    .line 2671434
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2671435
    :goto_1
    return-void

    .line 2671436
    :sswitch_0
    const-string v5, "alignItems"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v5, "alignSelf"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v5, "aspectRatio"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v3

    goto :goto_0

    :sswitch_3
    const-string v5, "backgroundColor"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v4

    goto :goto_0

    :sswitch_4
    const-string v5, "borderBottomColor"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v5, "borderBottomWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v5, "borderColor"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v5, "borderLeftColor"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v5, "borderLeftWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v5, "borderRadius"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v5, "borderRightColor"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v5, "borderRightWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_c
    const-string v5, "borderStyle"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v5, "borderTopColor"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v5, "borderTopWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v5, "borderWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v5, "bottom"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v5, "flex"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v5, "flexBasis"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v5, "flexDirection"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v5, "flexGrow"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v5, "flexShrink"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v5, "flexWrap"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v5, "height"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v5, "hitSlop"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string v5, "justifyContent"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string v5, "left"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v5, "margin"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string v5, "marginBottom"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1c

    goto/16 :goto_0

    :sswitch_1d
    const-string v5, "marginHorizontal"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1d

    goto/16 :goto_0

    :sswitch_1e
    const-string v5, "marginLeft"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1e

    goto/16 :goto_0

    :sswitch_1f
    const-string v5, "marginRight"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1f

    goto/16 :goto_0

    :sswitch_20
    const-string v5, "marginTop"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x20

    goto/16 :goto_0

    :sswitch_21
    const-string v5, "marginVertical"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x21

    goto/16 :goto_0

    :sswitch_22
    const-string v5, "maxHeight"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x22

    goto/16 :goto_0

    :sswitch_23
    const-string v5, "maxWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x23

    goto/16 :goto_0

    :sswitch_24
    const-string v5, "minHeight"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x24

    goto/16 :goto_0

    :sswitch_25
    const-string v5, "minWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x25

    goto/16 :goto_0

    :sswitch_26
    const-string v5, "nativeBackgroundAndroid"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x26

    goto/16 :goto_0

    :sswitch_27
    const-string v5, "onLayout"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x27

    goto/16 :goto_0

    :sswitch_28
    const-string v5, "overflow"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x28

    goto/16 :goto_0

    :sswitch_29
    const-string v5, "padding"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x29

    goto/16 :goto_0

    :sswitch_2a
    const-string v5, "paddingBottom"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x2a

    goto/16 :goto_0

    :sswitch_2b
    const-string v5, "paddingHorizontal"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x2b

    goto/16 :goto_0

    :sswitch_2c
    const-string v5, "paddingLeft"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x2c

    goto/16 :goto_0

    :sswitch_2d
    const-string v5, "paddingRight"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x2d

    goto/16 :goto_0

    :sswitch_2e
    const-string v5, "paddingTop"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x2e

    goto/16 :goto_0

    :sswitch_2f
    const-string v5, "paddingVertical"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x2f

    goto/16 :goto_0

    :sswitch_30
    const-string v5, "pointerEvents"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x30

    goto/16 :goto_0

    :sswitch_31
    const-string v5, "position"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x31

    goto/16 :goto_0

    :sswitch_32
    const-string v5, "right"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x32

    goto/16 :goto_0

    :sswitch_33
    const-string v5, "top"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x33

    goto/16 :goto_0

    :sswitch_34
    const-string v5, "width"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x34

    goto/16 :goto_0

    .line 2671437
    :pswitch_0
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setAlignItems(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2671438
    :pswitch_1
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setAlignSelf(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2671439
    :pswitch_2
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2671440
    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->g(F)V

    .line 2671441
    goto/16 :goto_1

    .line 2671442
    :pswitch_3
    invoke-virtual {p2, p1, v1}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 2671443
    :pswitch_4
    const/4 v0, 0x4

    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    invoke-virtual {p2, p1, v2, v3}, LX/5rC;->a(Ljava/lang/String;D)D

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderColor(ID)V

    goto/16 :goto_1

    .line 2671444
    :pswitch_5
    const/4 v0, 0x4

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2671445
    :pswitch_6
    const-wide/high16 v2, 0x7ff8000000000000L    # NaN

    invoke-virtual {p2, p1, v2, v3}, LX/5rC;->a(Ljava/lang/String;D)D

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderColor(ID)V

    goto/16 :goto_1

    .line 2671446
    :pswitch_7
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    invoke-virtual {p2, p1, v0, v1}, LX/5rC;->a(Ljava/lang/String;D)D

    move-result-wide v0

    invoke-virtual {p0, v2, v0, v1}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderColor(ID)V

    goto/16 :goto_1

    .line 2671447
    :pswitch_8
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2671448
    :pswitch_9
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderRadius(F)V

    goto/16 :goto_1

    .line 2671449
    :pswitch_a
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    invoke-virtual {p2, p1, v0, v1}, LX/5rC;->a(Ljava/lang/String;D)D

    move-result-wide v0

    invoke-virtual {p0, v3, v0, v1}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderColor(ID)V

    goto/16 :goto_1

    .line 2671450
    :pswitch_b
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2671451
    :pswitch_c
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderStyle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2671452
    :pswitch_d
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    invoke-virtual {p2, p1, v0, v1}, LX/5rC;->a(Ljava/lang/String;D)D

    move-result-wide v0

    invoke-virtual {p0, v4, v0, v1}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderColor(ID)V

    goto/16 :goto_1

    .line 2671453
    :pswitch_e
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v4, v0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2671454
    :pswitch_f
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2671455
    :pswitch_10
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v4, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2671456
    :pswitch_11
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlex(F)V

    goto/16 :goto_1

    .line 2671457
    :pswitch_12
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexBasis(F)V

    goto/16 :goto_1

    .line 2671458
    :pswitch_13
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setFlexDirection(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2671459
    :pswitch_14
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexGrow(F)V

    goto/16 :goto_1

    .line 2671460
    :pswitch_15
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexShrink(F)V

    goto/16 :goto_1

    .line 2671461
    :pswitch_16
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setFlexWrap(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2671462
    :pswitch_17
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setHeight(F)V

    goto/16 :goto_1

    .line 2671463
    :pswitch_18
    invoke-virtual {p2, p1}, LX/5rC;->e(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setHitSlop(LX/5pG;)V

    goto/16 :goto_1

    .line 2671464
    :pswitch_19
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setJustifyContent(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2671465
    :pswitch_1a
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2671466
    :pswitch_1b
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2671467
    :pswitch_1c
    const/4 v0, 0x6

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2671468
    :pswitch_1d
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2671469
    :pswitch_1e
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v4, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2671470
    :pswitch_1f
    const/4 v0, 0x4

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2671471
    :pswitch_20
    const/4 v0, 0x5

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2671472
    :pswitch_21
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2671473
    :pswitch_22
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMaxHeight(F)V

    goto/16 :goto_1

    .line 2671474
    :pswitch_23
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMaxWidth(F)V

    goto/16 :goto_1

    .line 2671475
    :pswitch_24
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMinHeight(F)V

    goto/16 :goto_1

    .line 2671476
    :pswitch_25
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMinWidth(F)V

    goto/16 :goto_1

    .line 2671477
    :pswitch_26
    invoke-virtual {p2, p1}, LX/5rC;->e(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->setHotspot(LX/5pG;)V

    goto/16 :goto_1

    .line 2671478
    :pswitch_27
    invoke-virtual {p2, p1, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 2671479
    iput-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->e:Z

    .line 2671480
    goto/16 :goto_1

    .line 2671481
    :pswitch_28
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setOverflow(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2671482
    :pswitch_29
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2671483
    :pswitch_2a
    const/4 v0, 0x6

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2671484
    :pswitch_2b
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2671485
    :pswitch_2c
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v4, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2671486
    :pswitch_2d
    const/4 v0, 0x4

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2671487
    :pswitch_2e
    const/4 v0, 0x5

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2671488
    :pswitch_2f
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2671489
    :pswitch_30
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    .line 2671490
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2671491
    goto/16 :goto_1

    .line 2671492
    :pswitch_31
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPosition(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2671493
    :pswitch_32
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2671494
    :pswitch_33
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2671495
    :pswitch_34
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setWidth(F)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7696880d -> :sswitch_a
        -0x757f89aa -> :sswitch_b
        -0x719cd38e -> :sswitch_1d
        -0x6a52083b -> :sswitch_12
        -0x597a2048 -> :sswitch_2c
        -0x57ab08a6 -> :sswitch_d
        -0x56940a43 -> :sswitch_e
        -0x527265d5 -> :sswitch_10
        -0x5201456c -> :sswitch_25
        -0x4e0397d4 -> :sswitch_4
        -0x4cec9971 -> :sswitch_5
        -0x48c76ed9 -> :sswitch_17
        -0x40737a52 -> :sswitch_1b
        -0x3f600445 -> :sswitch_0
        -0x3e464339 -> :sswitch_20
        -0x3a1ff07a -> :sswitch_13
        -0x36017855 -> :sswitch_22
        -0x300fc3ef -> :sswitch_29
        -0x15737ceb -> :sswitch_2b
        -0x117e564a -> :sswitch_30
        -0x113c6e87 -> :sswitch_1c
        -0xe70d730 -> :sswitch_7
        -0xd59d8cd -> :sswitch_8
        -0x7f661e7 -> :sswitch_24
        0x1c155 -> :sswitch_33
        0x2ffff9 -> :sswitch_11
        0x32a007 -> :sswitch_1a
        0x55f4784 -> :sswitch_2e
        0x677c21c -> :sswitch_32
        0x6be2dc6 -> :sswitch_34
        0xc0fb19c -> :sswitch_2a
        0x17dd56c2 -> :sswitch_23
        0x1f91b402 -> :sswitch_28
        0x2a8c788b -> :sswitch_2d
        0x2b158697 -> :sswitch_6
        0x2bf974e5 -> :sswitch_c
        0x2c2c84fa -> :sswitch_f
        0x2c929929 -> :sswitch_31
        0x373ef02d -> :sswitch_18
        0x3a1ea90e -> :sswitch_1f
        0x3d759362 -> :sswitch_15
        0x41194293 -> :sswitch_2
        0x4cb7f6d5 -> :sswitch_3
        0x4ccfd1e9 -> :sswitch_27
        0x501666a7 -> :sswitch_2f
        0x506afbde -> :sswitch_9
        0x5551c344 -> :sswitch_21
        0x67ef5bac -> :sswitch_14
        0x67f69fe3 -> :sswitch_16
        0x682c2a0a -> :sswitch_26
        0x6953cff1 -> :sswitch_1
        0x6ee75fc9 -> :sswitch_19
        0x757a12d5 -> :sswitch_1e
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/facebook/react/uimanager/ReactShadowNode;Ljava/lang/String;LX/5rC;)V
    .locals 0

    .prologue
    .line 2671550
    check-cast p1, Lcom/facebook/catalyst/shadow/flat/RCTView;

    invoke-static {p1, p2, p3}, Lcom/facebook/catalyst/shadow/flat/RCTView$$PropsSetter;->a(Lcom/facebook/catalyst/shadow/flat/RCTView;Ljava/lang/String;LX/5rC;)V

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2671496
    const-string v0, "alignItems"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671497
    const-string v0, "alignSelf"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671498
    const-string v0, "aspectRatio"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671499
    const-string v0, "backgroundColor"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671500
    const-string v0, "borderBottomColor"

    const-string v1, "Color"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671501
    const-string v0, "borderBottomWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671502
    const-string v0, "borderColor"

    const-string v1, "Color"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671503
    const-string v0, "borderLeftColor"

    const-string v1, "Color"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671504
    const-string v0, "borderLeftWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671505
    const-string v0, "borderRadius"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671506
    const-string v0, "borderRightColor"

    const-string v1, "Color"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671507
    const-string v0, "borderRightWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671508
    const-string v0, "borderStyle"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671509
    const-string v0, "borderTopColor"

    const-string v1, "Color"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671510
    const-string v0, "borderTopWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671511
    const-string v0, "borderWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671512
    const-string v0, "bottom"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671513
    const-string v0, "flex"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671514
    const-string v0, "flexBasis"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671515
    const-string v0, "flexDirection"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671516
    const-string v0, "flexGrow"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671517
    const-string v0, "flexShrink"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671518
    const-string v0, "flexWrap"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671519
    const-string v0, "height"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671520
    const-string v0, "hitSlop"

    const-string v1, "Map"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671521
    const-string v0, "justifyContent"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671522
    const-string v0, "left"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671523
    const-string v0, "margin"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671524
    const-string v0, "marginBottom"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671525
    const-string v0, "marginHorizontal"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671526
    const-string v0, "marginLeft"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671527
    const-string v0, "marginRight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671528
    const-string v0, "marginTop"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671529
    const-string v0, "marginVertical"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671530
    const-string v0, "maxHeight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671531
    const-string v0, "maxWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671532
    const-string v0, "minHeight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671533
    const-string v0, "minWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671534
    const-string v0, "nativeBackgroundAndroid"

    const-string v1, "Map"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671535
    const-string v0, "onLayout"

    const-string v1, "boolean"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671536
    const-string v0, "overflow"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671537
    const-string v0, "padding"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671538
    const-string v0, "paddingBottom"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671539
    const-string v0, "paddingHorizontal"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671540
    const-string v0, "paddingLeft"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671541
    const-string v0, "paddingRight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671542
    const-string v0, "paddingTop"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671543
    const-string v0, "paddingVertical"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671544
    const-string v0, "pointerEvents"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671545
    const-string v0, "position"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671546
    const-string v0, "right"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671547
    const-string v0, "top"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671548
    const-string v0, "width"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671549
    return-void
.end method
