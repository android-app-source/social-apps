.class public Lcom/facebook/catalyst/shadow/flat/RCTImageView;
.super Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "LX/JGN;",
        ":",
        "Lcom/facebook/catalyst/shadow/flat/DrawImage;",
        ">",
        "Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;"
    }
.end annotation


# static fields
.field public static d:Ljava/lang/Object;


# instance fields
.field private e:LX/JGN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2669955
    const-class v0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;

    sput-object v0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/JGN;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 2669952
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;-><init>()V

    .line 2669953
    iput-object p1, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    .line 2669954
    return-void
.end method

.method private ae()LX/JGN;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 2669946
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    .line 2669947
    iget-boolean v1, v0, LX/JGN;->g:Z

    move v0, v1

    .line 2669948
    if-eqz v0, :cond_0

    .line 2669949
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    invoke-virtual {v0}, LX/JGN;->ng_()LX/JGN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    .line 2669950
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->J()V

    .line 2669951
    :cond_0
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    return-object v0
.end method


# virtual methods
.method public final a(LX/JH2;FFFFFFFF)V
    .locals 9

    .prologue
    .line 2669940
    invoke-super/range {p0 .. p9}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(LX/JH2;FFFFFFFF)V

    .line 2669941
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    check-cast v0, LX/JGW;

    invoke-virtual {v0}, LX/JGW;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2669942
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    invoke-virtual/range {v0 .. v8}, LX/JGN;->a(FFFFFFFF)LX/JGN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    .line 2669943
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    invoke-virtual {p1, v0}, LX/JH2;->a(LX/JGN;)V

    .line 2669944
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    check-cast v0, LX/JGQ;

    invoke-virtual {p1, v0}, LX/JH2;->a(LX/JGQ;)V

    .line 2669945
    :cond_0
    return-void
.end method

.method public final d(IF)V
    .locals 1

    .prologue
    .line 2669933
    invoke-super {p0, p1, p2}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->d(IF)V

    .line 2669934
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    check-cast v0, LX/JGW;

    .line 2669935
    iget p1, v0, LX/JGW;->g:F

    move v0, p1

    .line 2669936
    cmpl-float v0, v0, p2

    if-eqz v0, :cond_0

    .line 2669937
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->ae()LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGW;

    .line 2669938
    iput p2, v0, LX/JGW;->g:F

    .line 2669939
    :cond_0
    return-void
.end method

.method public setBorderColor(I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        customType = "Color"
        name = "borderColor"
    .end annotation

    .prologue
    .line 2669927
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    check-cast v0, LX/JGW;

    .line 2669928
    iget v1, v0, LX/JGW;->i:I

    move v0, v1

    .line 2669929
    if-eq v0, p1, :cond_0

    .line 2669930
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->ae()LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGW;

    .line 2669931
    iput p1, v0, LX/JGW;->i:I

    .line 2669932
    :cond_0
    return-void
.end method

.method public setBorderRadius(F)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "borderRadius"
    .end annotation

    .prologue
    .line 2669956
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    check-cast v0, LX/JGW;

    .line 2669957
    iget v1, v0, LX/JGW;->h:F

    move v0, v1

    .line 2669958
    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 2669959
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->ae()LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGW;

    invoke-static {p1}, LX/5r2;->a(F)F

    move-result v1

    .line 2669960
    iput v1, v0, LX/JGW;->h:F

    .line 2669961
    :cond_0
    return-void
.end method

.method public setFadeDuration(I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "fadeDuration"
    .end annotation

    .prologue
    .line 2669924
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->ae()LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGW;

    .line 2669925
    iput p1, v0, LX/JGW;->l:I

    .line 2669926
    return-void
.end method

.method public setProgressiveRenderingEnabled(Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "progressiveRenderingEnabled"
    .end annotation

    .prologue
    .line 2669921
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->ae()LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGW;

    .line 2669922
    iput-boolean p1, v0, LX/JGW;->k:Z

    .line 2669923
    return-void
.end method

.method public setResizeMode(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "resizeMode"
    .end annotation

    .prologue
    .line 2669914
    invoke-static {p1}, LX/K0M;->a(Ljava/lang/String;)LX/1Up;

    move-result-object v1

    .line 2669915
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->e:LX/JGN;

    check-cast v0, LX/JGW;

    .line 2669916
    iget-object p1, v0, LX/JGW;->f:LX/1Up;

    move-object v0, p1

    .line 2669917
    if-eq v0, v1, :cond_0

    .line 2669918
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->ae()LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGW;

    .line 2669919
    iput-object v1, v0, LX/JGW;->f:LX/1Up;

    .line 2669920
    :cond_0
    return-void
.end method

.method public setShouldNotifyLoadEvents(Z)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "shouldNotifyLoadEvents"
    .end annotation

    .prologue
    .line 2669909
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->ae()LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGW;

    if-eqz p1, :cond_0

    .line 2669910
    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v1

    .line 2669911
    :goto_0
    iput v1, v0, LX/JGW;->j:I

    .line 2669912
    return-void

    .line 2669913
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSource(LX/5pC;)V
    .locals 11
    .param p1    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "src"
    .end annotation

    .prologue
    .line 2669898
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->ae()LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGW;

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v1

    const/4 v2, 0x0

    .line 2669899
    iget-object v3, v0, LX/JGW;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 2669900
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/5pC;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 2669901
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 2669902
    invoke-interface {p1, v2}, LX/5pC;->a(I)LX/5pG;

    move-result-object v2

    .line 2669903
    iget-object v3, v0, LX/JGW;->c:Ljava/util/List;

    new-instance v4, LX/9nR;

    const-string v5, "uri"

    invoke-interface {v2, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v1, v2}, LX/9nR;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2669904
    :cond_0
    return-void

    .line 2669905
    :cond_1
    :goto_0
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 2669906
    invoke-interface {p1, v2}, LX/5pC;->a(I)LX/5pG;

    move-result-object v4

    .line 2669907
    iget-object v10, v0, LX/JGW;->c:Ljava/util/List;

    new-instance v3, LX/9nR;

    const-string v5, "uri"

    invoke-interface {v4, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "width"

    invoke-interface {v4, v6}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    const-string v8, "height"

    invoke-interface {v4, v8}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    move-object v4, v1

    invoke-direct/range {v3 .. v9}, LX/9nR;-><init>(Landroid/content/Context;Ljava/lang/String;DD)V

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2669908
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setTintColor(I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "tintColor"
    .end annotation

    .prologue
    .line 2669893
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTImageView;->ae()LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGW;

    .line 2669894
    if-nez p1, :cond_0

    .line 2669895
    const/4 v1, 0x0

    iput-object v1, v0, LX/JGW;->e:Landroid/graphics/PorterDuffColorFilter;

    .line 2669896
    :goto_0
    return-void

    .line 2669897
    :cond_0
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    sget-object p0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, p1, p0}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v1, v0, LX/JGW;->e:Landroid/graphics/PorterDuffColorFilter;

    goto :goto_0
.end method
