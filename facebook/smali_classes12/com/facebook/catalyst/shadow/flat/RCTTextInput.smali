.class public Lcom/facebook/catalyst/shadow/flat/RCTTextInput;
.super Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;
.source ""

# interfaces
.implements LX/JGP;
.implements Lcom/facebook/csslayout/YogaMeasureFunction;


# instance fields
.field private d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I

.field private f:Z

.field private g:I

.field private h:Landroid/widget/EditText;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2670969
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;-><init>()V

    .line 2670970
    iput v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->e:I

    .line 2670971
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->f:Z

    .line 2670972
    iput v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->g:I

    .line 2670973
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2670974
    invoke-virtual {p0, p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaMeasureFunction;)V

    .line 2670975
    return-void
.end method


# virtual methods
.method public final a(LX/5rJ;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v2, -0x2

    .line 2670951
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->a(LX/5rJ;)V

    .line 2670952
    new-instance v0, Landroid/widget/EditText;

    invoke-direct {v0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->h:Landroid/widget/EditText;

    .line 2670953
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->h:Landroid/widget/EditText;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2670954
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getPaddingStart()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(IF)V

    .line 2670955
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(IF)V

    .line 2670956
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getPaddingEnd()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(IF)V

    .line 2670957
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->h:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(IF)V

    .line 2670958
    return-void
.end method

.method public final a(LX/5rl;)V
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 2670959
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->a(LX/5rl;)V

    .line 2670960
    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->e:I

    if-eq v0, v8, :cond_0

    .line 2670961
    new-instance v0, LX/K10;

    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->aj()Landroid/text/SpannableStringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->e:I

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v5

    const/4 v6, 0x5

    invoke-virtual {p0, v6}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v6

    const/4 v7, 0x3

    invoke-virtual {p0, v7}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v7

    invoke-direct/range {v0 .. v8}, LX/K10;-><init>(Landroid/text/Spannable;IZFFFFI)V

    .line 2670962
    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v1

    .line 2670963
    invoke-virtual {p1, v1, v0}, LX/5rl;->a(ILjava/lang/Object;)V

    .line 2670964
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2670965
    const/4 v0, 0x0

    return v0
.end method

.method public final bridge synthetic ab()Z
    .locals 1

    .prologue
    .line 2670966
    invoke-super {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ab()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic ac()Z
    .locals 1

    .prologue
    .line 2670968
    invoke-super {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ac()Z

    move-result v0

    return v0
.end method

.method public final ad()Z
    .locals 1

    .prologue
    .line 2670967
    const/4 v0, 0x1

    return v0
.end method

.method public final ae()Z
    .locals 1

    .prologue
    .line 2670976
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Landroid/text/SpannableStringBuilder;)V
    .locals 1

    .prologue
    .line 2670977
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2670978
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2670979
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->b(Landroid/text/SpannableStringBuilder;)V

    .line 2670980
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 2670943
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->b(Z)V

    .line 2670944
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2670945
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2670946
    const/4 v0, 0x1

    return v0
.end method

.method public final c(IF)V
    .locals 1

    .prologue
    .line 2670947
    invoke-super {p0, p1, p2}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->c(IF)V

    .line 2670948
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->f:Z

    .line 2670949
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->h()V

    .line 2670950
    return-void
.end method

.method public final bridge synthetic f()V
    .locals 0

    .prologue
    .line 2670919
    invoke-super {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->f()V

    return-void
.end method

.method public final measure(LX/1mn;FLcom/facebook/csslayout/YogaMeasureMode;FLcom/facebook/csslayout/YogaMeasureMode;)J
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 2670920
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->h:Landroid/widget/EditText;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 2670921
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->ag()I

    move-result v1

    .line 2670922
    const/4 v2, 0x0

    if-ne v1, v6, :cond_1

    const/high16 v1, 0x41600000    # 14.0f

    invoke-static {v1}, LX/5r2;->b(F)F

    move-result v1

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v1, v4

    int-to-float v1, v1

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/widget/EditText;->setTextSize(IF)V

    .line 2670923
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    const/4 v3, 0x5

    invoke-virtual {p0, v3}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 2670924
    iget v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->g:I

    if-eq v1, v6, :cond_0

    .line 2670925
    iget v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setLines(I)V

    .line 2670926
    :cond_0
    invoke-static {p2, p3}, LX/9nU;->a(FLcom/facebook/csslayout/YogaMeasureMode;)I

    move-result v1

    invoke-static {p4, p5}, LX/9nU;->a(FLcom/facebook/csslayout/YogaMeasureMode;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->measure(II)V

    .line 2670927
    invoke-virtual {v0}, Landroid/widget/EditText;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/EditText;->getMeasuredHeight()I

    move-result v0

    invoke-static {v1, v0}, LX/1Dr;->a(II)J

    move-result-wide v0

    return-wide v0

    .line 2670928
    :cond_1
    int-to-float v1, v1

    goto :goto_0
.end method

.method public final nh_()Z
    .locals 1

    .prologue
    .line 2670929
    const/4 v0, 0x0

    return v0
.end method

.method public final ni_()Z
    .locals 1

    .prologue
    .line 2670930
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->f:Z

    return v0
.end method

.method public final nj_()V
    .locals 1

    .prologue
    .line 2670931
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->f:Z

    .line 2670932
    return-void
.end method

.method public final setBackgroundColor(I)V
    .locals 0

    .prologue
    .line 2670933
    return-void
.end method

.method public setMostRecentEventCount(I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "mostRecentEventCount"
    .end annotation

    .prologue
    .line 2670934
    iput p1, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->e:I

    .line 2670935
    return-void
.end method

.method public setNumberOfLines(I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x7fffffff
        name = "numberOfLines"
    .end annotation

    .prologue
    .line 2670936
    iput p1, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->g:I

    .line 2670937
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->b(Z)V

    .line 2670938
    return-void
.end method

.method public final bridge synthetic setOverflow(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2670939
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/RCTVirtualText;->setOverflow(Ljava/lang/String;)V

    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "text"
    .end annotation

    .prologue
    .line 2670940
    iput-object p1, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->d:Ljava/lang/String;

    .line 2670941
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/RCTTextInput;->b(Z)V

    .line 2670942
    return-void
.end method
