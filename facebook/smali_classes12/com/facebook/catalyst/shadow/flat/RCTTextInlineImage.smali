.class public Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;
.super Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;
.source ""


# instance fields
.field private d:LX/JGx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2670746
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;-><init>()V

    .line 2670747
    new-instance v0, LX/JGx;

    invoke-direct {v0}, LX/JGx;-><init>()V

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->d:LX/JGx;

    return-void
.end method

.method private af()LX/JGx;
    .locals 5

    .prologue
    .line 2670748
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->d:LX/JGx;

    .line 2670749
    iget-boolean v1, v0, LX/JGx;->g:Z

    move v0, v1

    .line 2670750
    if-eqz v0, :cond_0

    .line 2670751
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->d:LX/JGx;

    .line 2670752
    new-instance v1, LX/JGx;

    iget-object v2, v0, LX/JGx;->c:LX/JGz;

    iget v3, v0, LX/JGx;->e:F

    iget v4, v0, LX/JGx;->f:F

    invoke-direct {v1, v2, v3, v4}, LX/JGx;-><init>(LX/JGz;FF)V

    move-object v0, v1

    .line 2670753
    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->d:LX/JGx;

    .line 2670754
    :cond_0
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->d:LX/JGx;

    return-object v0
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 2670755
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->a(F)V

    .line 2670756
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->d:LX/JGx;

    .line 2670757
    iget v1, v0, LX/JGx;->e:F

    move v0, v1

    .line 2670758
    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 2670759
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->af()LX/JGx;

    move-result-object v0

    .line 2670760
    iput p1, v0, LX/JGx;->e:F

    .line 2670761
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670762
    :cond_0
    return-void
.end method

.method public final a(LX/JH2;)V
    .locals 1

    .prologue
    .line 2670763
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->d:LX/JGx;

    invoke-virtual {p1, v0}, LX/JH2;->a(LX/JGQ;)V

    .line 2670764
    return-void
.end method

.method public final a(Landroid/text/SpannableStringBuilder;IIZ)V
    .locals 2

    .prologue
    .line 2670765
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->d:LX/JGx;

    .line 2670766
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/JGx;->g:Z

    .line 2670767
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->d:LX/JGx;

    const/16 v1, 0x11

    invoke-virtual {p1, v0, p2, p3, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2670768
    return-void
.end method

.method public final b(Landroid/text/SpannableStringBuilder;)V
    .locals 1

    .prologue
    .line 2670769
    const-string v0, "I"

    invoke-virtual {p1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2670770
    return-void
.end method

.method public final d(F)V
    .locals 2

    .prologue
    .line 2670771
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->d(F)V

    .line 2670772
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->d:LX/JGx;

    .line 2670773
    iget v1, v0, LX/JGx;->f:F

    move v0, v1

    .line 2670774
    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 2670775
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->af()LX/JGx;

    move-result-object v0

    .line 2670776
    iput p1, v0, LX/JGx;->f:F

    .line 2670777
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2670778
    :cond_0
    return-void
.end method

.method public setSource(LX/5pC;)V
    .locals 4
    .param p1    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "src"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2670779
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/5pC;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v2, v1

    .line 2670780
    :goto_0
    if-nez v2, :cond_2

    move-object v0, v1

    .line 2670781
    :goto_1
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTTextInlineImage;->af()LX/JGx;

    move-result-object v2

    if-nez v0, :cond_3

    .line 2670782
    :goto_2
    if-nez v1, :cond_4

    .line 2670783
    const/4 v0, 0x0

    iput-object v0, v2, LX/JGx;->c:LX/JGz;

    .line 2670784
    :goto_3
    return-void

    .line 2670785
    :cond_1
    const/4 v0, 0x0

    invoke-interface {p1, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v0

    const-string v2, "uri"

    invoke-interface {v0, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 2670786
    :cond_2
    new-instance v0, LX/9nR;

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v3

    invoke-direct {v0, v3, v2}, LX/9nR;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 2670787
    :cond_3
    invoke-virtual {v0}, LX/9nR;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    goto :goto_2

    .line 2670788
    :cond_4
    new-instance v0, LX/JGz;

    invoke-direct {v0, v1}, LX/JGz;-><init>(LX/1bf;)V

    iput-object v0, v2, LX/JGx;->c:LX/JGz;

    goto :goto_3
.end method
