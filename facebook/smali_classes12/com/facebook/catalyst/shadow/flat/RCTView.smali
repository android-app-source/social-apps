.class public final Lcom/facebook/catalyst/shadow/flat/RCTView;
.super Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;
.source ""


# static fields
.field private static final f:[I


# instance fields
.field public d:Z

.field public e:Z

.field private g:LX/JGV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2671619
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/catalyst/shadow/flat/RCTView;->f:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x8
        0x0
        0x2
        0x1
        0x3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2671643
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;-><init>()V

    return-void
.end method

.method private ad()LX/JGV;
    .locals 2

    .prologue
    .line 2671635
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->g:LX/JGV;

    if-nez v0, :cond_1

    .line 2671636
    new-instance v0, LX/JGV;

    invoke-direct {v0}, LX/JGV;-><init>()V

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->g:LX/JGV;

    .line 2671637
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->J()V

    .line 2671638
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->g:LX/JGV;

    return-object v0

    .line 2671639
    :cond_1
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->g:LX/JGV;

    .line 2671640
    iget-boolean v1, v0, LX/JGN;->g:Z

    move v0, v1

    .line 2671641
    if-eqz v0, :cond_0

    .line 2671642
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->g:LX/JGV;

    invoke-virtual {v0}, LX/JGN;->ng_()LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGV;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->g:LX/JGV;

    goto :goto_0
.end method


# virtual methods
.method public final a(FFFFZ)V
    .locals 8

    .prologue
    .line 2671626
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    move-object v0, v0

    .line 2671627
    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, LX/JGu;->a(FFFFZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2671628
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->h:Landroid/graphics/Rect;

    if-nez v0, :cond_1

    new-instance v0, LX/JGu;

    .line 2671629
    iget v1, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v5, v1

    .line 2671630
    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, LX/JGu;-><init>(FFFFIZ)V

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(LX/JGu;)V

    .line 2671631
    :cond_0
    return-void

    .line 2671632
    :cond_1
    new-instance v0, LX/JGv;

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->h:Landroid/graphics/Rect;

    .line 2671633
    iget v2, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v6, v2

    .line 2671634
    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, LX/JGv;-><init>(Landroid/graphics/Rect;FFFFIZ)V

    goto :goto_0
.end method

.method public final a(LX/JH2;FFFFFFFF)V
    .locals 9

    .prologue
    .line 2671621
    invoke-super/range {p0 .. p9}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(LX/JH2;FFFFFFFF)V

    .line 2671622
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->g:LX/JGV;

    if-eqz v0, :cond_0

    .line 2671623
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->g:LX/JGV;

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move/from16 v8, p9

    invoke-virtual/range {v0 .. v8}, LX/JGN;->a(FFFFFFFF)LX/JGN;

    move-result-object v0

    check-cast v0, LX/JGV;

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->g:LX/JGV;

    .line 2671624
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->g:LX/JGV;

    invoke-virtual {p1, v0}, LX/JH2;->a(LX/JGN;)V

    .line 2671625
    :cond_0
    return-void
.end method

.method public final ab()Z
    .locals 1

    .prologue
    .line 2671620
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->d:Z

    return v0
.end method

.method public final b(LX/5rC;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2671613
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->d:Z

    if-nez v0, :cond_0

    const-string v0, "removeClippedSubviews"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "removeClippedSubviews"

    invoke-virtual {p1, v0, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->d:Z

    .line 2671614
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->d:Z

    if-eqz v0, :cond_3

    .line 2671615
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->e:Z

    if-nez v0, :cond_1

    const-string v0, "horizontal"

    invoke-virtual {p1, v0}, LX/5rC;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "horizontal"

    invoke-virtual {p1, v0, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    iput-boolean v1, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->e:Z

    .line 2671616
    :cond_3
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->b(LX/5rC;)V

    .line 2671617
    return-void

    :cond_4
    move v0, v1

    .line 2671618
    goto :goto_0
.end method

.method public final setBackgroundColor(I)V
    .locals 1

    .prologue
    .line 2671644
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->ad()LX/JGV;

    move-result-object v0

    .line 2671645
    iput p1, v0, LX/JGV;->n:I

    .line 2671646
    return-void
.end method

.method public final setBorderColor(ID)V
    .locals 4
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactPropGroup;
        a = {
            "borderColor",
            "borderLeftColor",
            "borderRightColor",
            "borderTopColor",
            "borderBottomColor"
        }
        c = NaN
        customType = "Color"
    .end annotation

    .prologue
    .line 2671588
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/RCTView;->f:[I

    aget v0, v0, p1

    .line 2671589
    invoke-static {p2, p3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2671590
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->ad()LX/JGV;

    move-result-object v1

    .line 2671591
    packed-switch v0, :pswitch_data_0

    .line 2671592
    :goto_0
    :pswitch_0
    return-void

    .line 2671593
    :cond_0
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->ad()LX/JGV;

    move-result-object v1

    double-to-int v2, p2

    .line 2671594
    packed-switch v0, :pswitch_data_1

    .line 2671595
    :goto_1
    :pswitch_1
    goto :goto_0

    .line 2671596
    :pswitch_2
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, LX/JGO;->d(I)V

    goto :goto_0

    .line 2671597
    :pswitch_3
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, LX/JGO;->d(I)V

    goto :goto_0

    .line 2671598
    :pswitch_4
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, LX/JGO;->d(I)V

    goto :goto_0

    .line 2671599
    :pswitch_5
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, LX/JGO;->d(I)V

    goto :goto_0

    .line 2671600
    :pswitch_6
    const/high16 v2, -0x1000000

    .line 2671601
    iput v2, v1, LX/JGO;->f:I

    .line 2671602
    goto :goto_0

    .line 2671603
    :pswitch_7
    iput v2, v1, LX/JGV;->i:I

    .line 2671604
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, LX/JGO;->c(I)V

    goto :goto_1

    .line 2671605
    :pswitch_8
    iput v2, v1, LX/JGV;->j:I

    .line 2671606
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, LX/JGO;->c(I)V

    goto :goto_1

    .line 2671607
    :pswitch_9
    iput v2, v1, LX/JGV;->k:I

    .line 2671608
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, LX/JGO;->c(I)V

    goto :goto_1

    .line 2671609
    :pswitch_a
    iput v2, v1, LX/JGV;->l:I

    .line 2671610
    const/16 v3, 0x10

    invoke-virtual {v1, v3}, LX/JGO;->c(I)V

    goto :goto_1

    .line 2671611
    :pswitch_b
    iput v2, v1, LX/JGO;->f:I

    .line 2671612
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_b
    .end packed-switch
.end method

.method public final setBorderRadius(F)V
    .locals 2
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "borderRadius"
    .end annotation

    .prologue
    .line 2671552
    iput p1, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->b:F

    .line 2671553
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->c:Z

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f000000    # 0.5f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 2671554
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2671555
    :cond_0
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->ad()LX/JGV;

    move-result-object v0

    invoke-static {p1}, LX/5r2;->a(F)F

    move-result v1

    .line 2671556
    iput v1, v0, LX/JGO;->h:F

    .line 2671557
    const/4 p0, 0x1

    invoke-virtual {v0, p0}, LX/JGO;->c(I)V

    .line 2671558
    return-void
.end method

.method public final setBorderStyle(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "borderStyle"
    .end annotation

    .prologue
    .line 2671580
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->ad()LX/JGV;

    move-result-object v0

    .line 2671581
    const-string p0, "dotted"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2671582
    const/4 p0, 0x1

    iput p0, v0, LX/JGV;->m:I

    .line 2671583
    :goto_0
    const/16 p0, 0x20

    invoke-virtual {v0, p0}, LX/JGO;->c(I)V

    .line 2671584
    return-void

    .line 2671585
    :cond_0
    const-string p0, "dashed"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2671586
    const/4 p0, 0x2

    iput p0, v0, LX/JGV;->m:I

    goto :goto_0

    .line 2671587
    :cond_1
    const/4 p0, 0x0

    iput p0, v0, LX/JGV;->m:I

    goto :goto_0
.end method

.method public final setBorderWidths(IF)V
    .locals 3

    .prologue
    .line 2671568
    invoke-super {p0, p1, p2}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->setBorderWidths(IF)V

    .line 2671569
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/RCTView;->f:[I

    aget v0, v0, p1

    .line 2671570
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/RCTView;->ad()LX/JGV;

    move-result-object v1

    invoke-static {p2}, LX/5r2;->a(F)F

    move-result v2

    .line 2671571
    packed-switch v0, :pswitch_data_0

    .line 2671572
    :goto_0
    :pswitch_0
    return-void

    .line 2671573
    :pswitch_1
    iput v2, v1, LX/JGV;->e:F

    goto :goto_0

    .line 2671574
    :pswitch_2
    iput v2, v1, LX/JGV;->f:F

    goto :goto_0

    .line 2671575
    :pswitch_3
    iput v2, v1, LX/JGV;->g:F

    goto :goto_0

    .line 2671576
    :pswitch_4
    iput v2, v1, LX/JGV;->h:F

    goto :goto_0

    .line 2671577
    :pswitch_5
    iput v2, v1, LX/JGO;->g:F

    .line 2671578
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, LX/JGO;->c(I)V

    .line 2671579
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public final setHitSlop(LX/5pG;)V
    .locals 6
    .param p1    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "hitSlop"
    .end annotation

    .prologue
    .line 2671564
    if-nez p1, :cond_0

    .line 2671565
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->h:Landroid/graphics/Rect;

    .line 2671566
    :goto_0
    return-void

    .line 2671567
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    const-string v1, "left"

    invoke-interface {p1, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v1

    float-to-int v1, v1

    const-string v2, "top"

    invoke-interface {p1, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v2

    float-to-int v2, v2

    const-string v3, "right"

    invoke-interface {p1, v3}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, LX/5r2;->a(D)F

    move-result v3

    float-to-int v3, v3

    const-string v4, "bottom"

    invoke-interface {p1, v4}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, LX/5r2;->a(D)F

    move-result v4

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/RCTView;->h:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public final setHotspot(LX/5pG;)V
    .locals 0
    .param p1    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "nativeBackgroundAndroid"
    .end annotation

    .prologue
    .line 2671561
    if-eqz p1, :cond_0

    .line 2671562
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2671563
    :cond_0
    return-void
.end method

.method public final setPointerEvents(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "pointerEvents"
    .end annotation

    .prologue
    .line 2671559
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2671560
    return-void
.end method
