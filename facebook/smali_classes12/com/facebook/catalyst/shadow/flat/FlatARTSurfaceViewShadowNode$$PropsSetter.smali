.class public final Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode$$PropsSetter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ShadowNodeSetter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ShadowNodeSetter",
        "<",
        "Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2667850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;Ljava/lang/String;LX/5rC;)V
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/high16 v6, 0x7fc00000    # NaNf

    .line 2667851
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2667852
    :goto_1
    return-void

    .line 2667853
    :sswitch_0
    const-string v5, "alignItems"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v5, "alignSelf"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v5, "aspectRatio"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v3

    goto :goto_0

    :sswitch_3
    const-string v5, "backgroundColor"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v4

    goto :goto_0

    :sswitch_4
    const-string v5, "borderBottomWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v5, "borderLeftWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v5, "borderRightWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v5, "borderTopWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v5, "borderWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v5, "bottom"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v5, "flex"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v5, "flexBasis"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_c
    const-string v5, "flexDirection"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v5, "flexGrow"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v5, "flexShrink"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v5, "flexWrap"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v5, "height"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v5, "justifyContent"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v5, "left"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v5, "margin"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v5, "marginBottom"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x14

    goto/16 :goto_0

    :sswitch_15
    const-string v5, "marginHorizontal"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x15

    goto/16 :goto_0

    :sswitch_16
    const-string v5, "marginLeft"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x16

    goto/16 :goto_0

    :sswitch_17
    const-string v5, "marginRight"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x17

    goto/16 :goto_0

    :sswitch_18
    const-string v5, "marginTop"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x18

    goto/16 :goto_0

    :sswitch_19
    const-string v5, "marginVertical"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x19

    goto/16 :goto_0

    :sswitch_1a
    const-string v5, "maxHeight"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1a

    goto/16 :goto_0

    :sswitch_1b
    const-string v5, "maxWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1b

    goto/16 :goto_0

    :sswitch_1c
    const-string v5, "minHeight"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1c

    goto/16 :goto_0

    :sswitch_1d
    const-string v5, "minWidth"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1d

    goto/16 :goto_0

    :sswitch_1e
    const-string v5, "onLayout"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1e

    goto/16 :goto_0

    :sswitch_1f
    const-string v5, "overflow"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x1f

    goto/16 :goto_0

    :sswitch_20
    const-string v5, "padding"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x20

    goto/16 :goto_0

    :sswitch_21
    const-string v5, "paddingBottom"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x21

    goto/16 :goto_0

    :sswitch_22
    const-string v5, "paddingHorizontal"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x22

    goto/16 :goto_0

    :sswitch_23
    const-string v5, "paddingLeft"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x23

    goto/16 :goto_0

    :sswitch_24
    const-string v5, "paddingRight"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x24

    goto/16 :goto_0

    :sswitch_25
    const-string v5, "paddingTop"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x25

    goto/16 :goto_0

    :sswitch_26
    const-string v5, "paddingVertical"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x26

    goto/16 :goto_0

    :sswitch_27
    const-string v5, "position"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x27

    goto/16 :goto_0

    :sswitch_28
    const-string v5, "right"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x28

    goto/16 :goto_0

    :sswitch_29
    const-string v5, "top"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x29

    goto/16 :goto_0

    :sswitch_2a
    const-string v5, "width"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v0, 0x2a

    goto/16 :goto_0

    .line 2667854
    :pswitch_0
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setAlignItems(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2667855
    :pswitch_1
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setAlignSelf(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2667856
    :pswitch_2
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2667857
    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->g(F)V

    .line 2667858
    goto/16 :goto_1

    .line 2667859
    :pswitch_3
    invoke-virtual {p2, p1, v1}, LX/5rC;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 2667860
    :pswitch_4
    const/4 v0, 0x4

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2667861
    :pswitch_5
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2667862
    :pswitch_6
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2667863
    :pswitch_7
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v4, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2667864
    :pswitch_8
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setBorderWidths(IF)V

    goto/16 :goto_1

    .line 2667865
    :pswitch_9
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v4, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2667866
    :pswitch_a
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlex(F)V

    goto/16 :goto_1

    .line 2667867
    :pswitch_b
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexBasis(F)V

    goto/16 :goto_1

    .line 2667868
    :pswitch_c
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setFlexDirection(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2667869
    :pswitch_d
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexGrow(F)V

    goto/16 :goto_1

    .line 2667870
    :pswitch_e
    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->setFlexShrink(F)V

    goto/16 :goto_1

    .line 2667871
    :pswitch_f
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setFlexWrap(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2667872
    :pswitch_10
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setHeight(F)V

    goto/16 :goto_1

    .line 2667873
    :pswitch_11
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setJustifyContent(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2667874
    :pswitch_12
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2667875
    :pswitch_13
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2667876
    :pswitch_14
    const/4 v0, 0x6

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2667877
    :pswitch_15
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2667878
    :pswitch_16
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v4, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2667879
    :pswitch_17
    const/4 v0, 0x4

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2667880
    :pswitch_18
    const/4 v0, 0x5

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2667881
    :pswitch_19
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMargins(IF)V

    goto/16 :goto_1

    .line 2667882
    :pswitch_1a
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMaxHeight(F)V

    goto/16 :goto_1

    .line 2667883
    :pswitch_1b
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMaxWidth(F)V

    goto/16 :goto_1

    .line 2667884
    :pswitch_1c
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMinHeight(F)V

    goto/16 :goto_1

    .line 2667885
    :pswitch_1d
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setMinWidth(F)V

    goto/16 :goto_1

    .line 2667886
    :pswitch_1e
    invoke-virtual {p2, p1, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 2667887
    iput-boolean v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->e:Z

    .line 2667888
    goto/16 :goto_1

    .line 2667889
    :pswitch_1f
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setOverflow(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2667890
    :pswitch_20
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2667891
    :pswitch_21
    const/4 v0, 0x6

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2667892
    :pswitch_22
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2667893
    :pswitch_23
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v4, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2667894
    :pswitch_24
    const/4 v0, 0x4

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2667895
    :pswitch_25
    const/4 v0, 0x5

    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2667896
    :pswitch_26
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPaddings(IF)V

    goto/16 :goto_1

    .line 2667897
    :pswitch_27
    invoke-virtual {p2, p1}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPosition(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2667898
    :pswitch_28
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2667899
    :pswitch_29
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setPositionValues(IF)V

    goto/16 :goto_1

    .line 2667900
    :pswitch_2a
    invoke-virtual {p2, p1, v6}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/LayoutShadowNode;->setWidth(F)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x757f89aa -> :sswitch_6
        -0x719cd38e -> :sswitch_15
        -0x6a52083b -> :sswitch_b
        -0x597a2048 -> :sswitch_23
        -0x56940a43 -> :sswitch_7
        -0x527265d5 -> :sswitch_9
        -0x5201456c -> :sswitch_1d
        -0x4cec9971 -> :sswitch_4
        -0x48c76ed9 -> :sswitch_10
        -0x40737a52 -> :sswitch_13
        -0x3f600445 -> :sswitch_0
        -0x3e464339 -> :sswitch_18
        -0x3a1ff07a -> :sswitch_c
        -0x36017855 -> :sswitch_1a
        -0x300fc3ef -> :sswitch_20
        -0x15737ceb -> :sswitch_22
        -0x113c6e87 -> :sswitch_14
        -0xd59d8cd -> :sswitch_5
        -0x7f661e7 -> :sswitch_1c
        0x1c155 -> :sswitch_29
        0x2ffff9 -> :sswitch_a
        0x32a007 -> :sswitch_12
        0x55f4784 -> :sswitch_25
        0x677c21c -> :sswitch_28
        0x6be2dc6 -> :sswitch_2a
        0xc0fb19c -> :sswitch_21
        0x17dd56c2 -> :sswitch_1b
        0x1f91b402 -> :sswitch_1f
        0x2a8c788b -> :sswitch_24
        0x2c2c84fa -> :sswitch_8
        0x2c929929 -> :sswitch_27
        0x3a1ea90e -> :sswitch_17
        0x3d759362 -> :sswitch_e
        0x41194293 -> :sswitch_2
        0x4cb7f6d5 -> :sswitch_3
        0x4ccfd1e9 -> :sswitch_1e
        0x501666a7 -> :sswitch_26
        0x5551c344 -> :sswitch_19
        0x67ef5bac -> :sswitch_d
        0x67f69fe3 -> :sswitch_f
        0x6953cff1 -> :sswitch_1
        0x6ee75fc9 -> :sswitch_11
        0x757a12d5 -> :sswitch_16
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/facebook/react/uimanager/ReactShadowNode;Ljava/lang/String;LX/5rC;)V
    .locals 0

    .prologue
    .line 2667849
    check-cast p1, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;

    invoke-static {p1, p2, p3}, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode$$PropsSetter;->a(Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;Ljava/lang/String;LX/5rC;)V

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2667805
    const-string v0, "alignItems"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667806
    const-string v0, "alignSelf"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667807
    const-string v0, "aspectRatio"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667808
    const-string v0, "backgroundColor"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667809
    const-string v0, "borderBottomWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667810
    const-string v0, "borderLeftWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667811
    const-string v0, "borderRightWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667812
    const-string v0, "borderTopWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667813
    const-string v0, "borderWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667814
    const-string v0, "bottom"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667815
    const-string v0, "flex"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667816
    const-string v0, "flexBasis"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667817
    const-string v0, "flexDirection"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667818
    const-string v0, "flexGrow"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667819
    const-string v0, "flexShrink"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667820
    const-string v0, "flexWrap"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667821
    const-string v0, "height"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667822
    const-string v0, "justifyContent"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667823
    const-string v0, "left"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667824
    const-string v0, "margin"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667825
    const-string v0, "marginBottom"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667826
    const-string v0, "marginHorizontal"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667827
    const-string v0, "marginLeft"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667828
    const-string v0, "marginRight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667829
    const-string v0, "marginTop"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667830
    const-string v0, "marginVertical"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667831
    const-string v0, "maxHeight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667832
    const-string v0, "maxWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667833
    const-string v0, "minHeight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667834
    const-string v0, "minWidth"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667835
    const-string v0, "onLayout"

    const-string v1, "boolean"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667836
    const-string v0, "overflow"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667837
    const-string v0, "padding"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667838
    const-string v0, "paddingBottom"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667839
    const-string v0, "paddingHorizontal"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667840
    const-string v0, "paddingLeft"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667841
    const-string v0, "paddingRight"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667842
    const-string v0, "paddingTop"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667843
    const-string v0, "paddingVertical"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667844
    const-string v0, "position"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667845
    const-string v0, "right"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667846
    const-string v0, "top"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667847
    const-string v0, "width"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2667848
    return-void
.end method
