.class public Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;
.super Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;
.source ""

# interfaces
.implements LX/JGP;


# instance fields
.field private final d:Landroid/graphics/Point;

.field private final e:Landroid/graphics/Point;

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2668342
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;-><init>()V

    .line 2668343
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;->d:Landroid/graphics/Point;

    .line 2668344
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;->e:Landroid/graphics/Point;

    .line 2668345
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2668346
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->H()V

    .line 2668347
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 2668323
    invoke-super {p0, p1, p2}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 2668324
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v0

    .line 2668325
    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 2668326
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2668327
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;->d:Landroid/graphics/Point;

    iget-object v2, p0, Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v2}, Landroid/view/Display;->getCurrentSizeRange(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 2668328
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 2668329
    if-eqz v0, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2668330
    :cond_0
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;->d:Landroid/graphics/Point;

    iget v1, v0, Landroid/graphics/Point;->x:I

    .line 2668331
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    .line 2668332
    :goto_0
    int-to-float v1, v1

    invoke-virtual {p1, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(F)V

    .line 2668333
    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->d(F)V

    .line 2668334
    return-void

    .line 2668335
    :cond_1
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;->e:Landroid/graphics/Point;

    iget v1, v0, Landroid/graphics/Point;->x:I

    .line 2668336
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;->d:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method public final c(IF)V
    .locals 1

    .prologue
    .line 2668337
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v0

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_0

    .line 2668338
    invoke-super {p0, p1, p2}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->c(IF)V

    .line 2668339
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;->f:Z

    .line 2668340
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2668341
    :cond_0
    return-void
.end method

.method public final nh_()Z
    .locals 1

    .prologue
    .line 2668348
    const/4 v0, 0x0

    return v0
.end method

.method public final ni_()Z
    .locals 1

    .prologue
    .line 2668320
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;->f:Z

    return v0
.end method

.method public final nj_()V
    .locals 1

    .prologue
    .line 2668321
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatReactModalShadowNode;->f:Z

    .line 2668322
    return-void
.end method
