.class public final Lcom/facebook/catalyst/shadow/flat/RCTViewManager$$PropsSetter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ViewManagerSetter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/react/uimanager/ViewManagerPropertyUpdater$ViewManagerSetter",
        "<",
        "Lcom/facebook/catalyst/shadow/flat/RCTViewManager;",
        "LX/JGs;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2671713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/catalyst/shadow/flat/RCTViewManager;LX/JGs;Ljava/lang/String;LX/5rC;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2671670
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2671671
    :goto_1
    return-void

    .line 2671672
    :sswitch_0
    const-string v2, "accessibilityComponentType"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v2, "accessibilityLabel"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "accessibilityLiveRegion"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "backgroundColor"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v2, "elevation"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v2, "hitSlop"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v2, "importantForAccessibility"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v2, "nativeBackgroundAndroid"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v2, "needsOffscreenAlphaCompositing"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v2, "opacity"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v2, "pointerEvents"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v2, "removeClippedSubviews"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_c
    const-string v2, "renderToHardwareTextureAndroid"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v2, "rotation"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v2, "scaleX"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v2, "scaleY"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v2, "testID"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_0

    :sswitch_11
    const-string v2, "transform"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x11

    goto/16 :goto_0

    :sswitch_12
    const-string v2, "translateX"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x12

    goto/16 :goto_0

    :sswitch_13
    const-string v2, "translateY"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x13

    goto/16 :goto_0

    :sswitch_14
    const-string v2, "zIndex"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x14

    goto/16 :goto_0

    .line 2671673
    :pswitch_0
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2671674
    invoke-static {p1, v0}, LX/5qk;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 2671675
    goto/16 :goto_1

    .line 2671676
    :pswitch_1
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2671677
    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2671678
    goto/16 :goto_1

    .line 2671679
    :pswitch_2
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setAccessibilityLiveRegion(Landroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2671680
    :pswitch_3
    invoke-virtual {p3, p2, v1}, LX/5rC;->a(Ljava/lang/String;I)I

    .line 2671681
    goto/16 :goto_1

    .line 2671682
    :pswitch_4
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setElevation(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 2671683
    :pswitch_5
    invoke-virtual {p3, p2}, LX/5rC;->e(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;->setHitSlop(LX/JGs;LX/5pG;)V

    goto/16 :goto_1

    .line 2671684
    :pswitch_6
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setImportantForAccessibility(Landroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2671685
    :pswitch_7
    invoke-virtual {p3, p2}, LX/5rC;->e(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;->setHotspot(LX/JGs;LX/5pG;)V

    goto/16 :goto_1

    .line 2671686
    :pswitch_8
    invoke-virtual {p3, p2, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 2671687
    iput-boolean v0, p1, LX/JGs;->p:Z

    .line 2671688
    goto/16 :goto_1

    .line 2671689
    :pswitch_9
    invoke-virtual {p3, p2, v4}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2671690
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2671691
    goto/16 :goto_1

    .line 2671692
    :pswitch_a
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;->setPointerEvents(LX/JGs;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2671693
    :pswitch_b
    invoke-virtual {p3, p2, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 2671694
    invoke-virtual {p1, v0}, LX/JGs;->b(Z)V

    .line 2671695
    goto/16 :goto_1

    .line 2671696
    :pswitch_c
    invoke-virtual {p3, p2, v1}, LX/5rC;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setRenderToHardwareTexture(Landroid/view/View;Z)V

    goto/16 :goto_1

    .line 2671697
    :pswitch_d
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2671698
    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    .line 2671699
    goto/16 :goto_1

    .line 2671700
    :pswitch_e
    invoke-virtual {p3, p2, v4}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2671701
    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 2671702
    goto/16 :goto_1

    .line 2671703
    :pswitch_f
    invoke-virtual {p3, p2, v4}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2671704
    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 2671705
    goto/16 :goto_1

    .line 2671706
    :pswitch_10
    invoke-virtual {p3, p2}, LX/5rC;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2671707
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2671708
    goto/16 :goto_1

    .line 2671709
    :pswitch_11
    invoke-virtual {p3, p2}, LX/5rC;->d(Ljava/lang/String;)LX/5pC;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setTransform(Landroid/view/View;LX/5pC;)V

    goto/16 :goto_1

    .line 2671710
    :pswitch_12
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setTranslateX(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 2671711
    :pswitch_13
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setTranslateY(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 2671712
    :pswitch_14
    invoke-virtual {p3, p2, v3}, LX/5rC;->a(Ljava/lang/String;F)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/react/uimanager/BaseViewManager;->setZIndex(Landroid/view/View;F)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x66a2c736 -> :sswitch_12
        -0x66a2c735 -> :sswitch_13
        -0x658128d7 -> :sswitch_0
        -0x4b8807f5 -> :sswitch_9
        -0x3621dfb2 -> :sswitch_e
        -0x3621dfb1 -> :sswitch_f
        -0x34488ed3 -> :sswitch_10
        -0x2b988b88 -> :sswitch_14
        -0x117e564a -> :sswitch_a
        -0xf06d417 -> :sswitch_b
        -0x4d24f13 -> :sswitch_c
        -0x266f082 -> :sswitch_d
        -0x42d1a3 -> :sswitch_4
        0x22936ee -> :sswitch_2
        0x2c861b47 -> :sswitch_6
        0x373ef02d -> :sswitch_5
        0x3ebe6b6c -> :sswitch_11
        0x445b6e46 -> :sswitch_1
        0x4cb7f6d5 -> :sswitch_3
        0x636835e4 -> :sswitch_8
        0x682c2a0a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/facebook/react/uimanager/ViewManager;Landroid/view/View;Ljava/lang/String;LX/5rC;)V
    .locals 0

    .prologue
    .line 2671647
    check-cast p1, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;

    check-cast p2, LX/JGs;

    invoke-static {p1, p2, p3, p4}, Lcom/facebook/catalyst/shadow/flat/RCTViewManager$$PropsSetter;->a(Lcom/facebook/catalyst/shadow/flat/RCTViewManager;LX/JGs;Ljava/lang/String;LX/5rC;)V

    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2671648
    const-string v0, "accessibilityComponentType"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671649
    const-string v0, "accessibilityLabel"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671650
    const-string v0, "accessibilityLiveRegion"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671651
    const-string v0, "backgroundColor"

    const-string v1, "Color"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671652
    const-string v0, "elevation"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671653
    const-string v0, "hitSlop"

    const-string v1, "Map"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671654
    const-string v0, "importantForAccessibility"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671655
    const-string v0, "nativeBackgroundAndroid"

    const-string v1, "Map"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671656
    const-string v0, "needsOffscreenAlphaCompositing"

    const-string v1, "boolean"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671657
    const-string v0, "opacity"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671658
    const-string v0, "pointerEvents"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671659
    const-string v0, "removeClippedSubviews"

    const-string v1, "boolean"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671660
    const-string v0, "renderToHardwareTextureAndroid"

    const-string v1, "boolean"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671661
    const-string v0, "rotation"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671662
    const-string v0, "scaleX"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671663
    const-string v0, "scaleY"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671664
    const-string v0, "testID"

    const-string v1, "String"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671665
    const-string v0, "transform"

    const-string v1, "Array"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671666
    const-string v0, "translateX"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671667
    const-string v0, "translateY"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671668
    const-string v0, "zIndex"

    const-string v1, "number"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2671669
    return-void
.end method
