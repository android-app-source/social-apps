.class public Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewManager;
.super Lcom/facebook/react/uimanager/BaseViewManager;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/BaseViewManager",
        "<",
        "LX/K0B;",
        "Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/csslayout/YogaMeasureFunction;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2667804
    new-instance v0, LX/JGc;

    invoke-direct {v0}, LX/JGc;-><init>()V

    sput-object v0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewManager;->a:Lcom/facebook/csslayout/YogaMeasureFunction;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2667803
    invoke-direct {p0}, Lcom/facebook/react/uimanager/BaseViewManager;-><init>()V

    return-void
.end method

.method private static a(LX/K0B;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2667801
    check-cast p1, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;

    invoke-virtual {p0, p1}, LX/K0B;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 2667802
    return-void
.end method

.method private static b(LX/5rJ;)LX/K0B;
    .locals 1

    .prologue
    .line 2667792
    new-instance v0, LX/K0B;

    invoke-direct {v0, p0}, LX/K0B;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private static h()Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;
    .locals 2

    .prologue
    .line 2667798
    new-instance v0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;

    invoke-direct {v0}, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;-><init>()V

    .line 2667799
    sget-object v1, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewManager;->a:Lcom/facebook/csslayout/YogaMeasureFunction;

    invoke-virtual {v0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaMeasureFunction;)V

    .line 2667800
    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2667797
    invoke-static {p1}, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewManager;->b(LX/5rJ;)LX/K0B;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2667796
    check-cast p1, LX/K0B;

    invoke-static {p1, p2}, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewManager;->a(LX/K0B;Ljava/lang/Object;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2667795
    const-string v0, "ARTSurfaceView"

    return-object v0
.end method

.method public final i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2667794
    const-class v0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;

    return-object v0
.end method

.method public final synthetic s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 2667793
    invoke-static {}, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewManager;->h()Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;

    move-result-object v0

    return-object v0
.end method
