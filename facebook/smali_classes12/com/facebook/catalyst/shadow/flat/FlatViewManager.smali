.class public abstract Lcom/facebook/catalyst/shadow/flat/FlatViewManager;
.super Lcom/facebook/react/uimanager/ViewGroupManager;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewGroupManager",
        "<",
        "LX/JGs;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2669364
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;-><init>()V

    return-void
.end method

.method private static b(LX/5rJ;)LX/JGs;
    .locals 1

    .prologue
    .line 2669365
    new-instance v0, LX/JGs;

    invoke-direct {v0, p0}, LX/JGs;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2669366
    invoke-static {p1}, Lcom/facebook/catalyst/shadow/flat/FlatViewManager;->b(LX/5rJ;)LX/JGs;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic c(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2669367
    check-cast p1, LX/JGs;

    .line 2669368
    invoke-virtual {p1}, LX/JGs;->removeAllViewsInLayout()V

    .line 2669369
    return-void
.end method

.method public final bridge synthetic setBackgroundColor(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 2669370
    return-void
.end method
