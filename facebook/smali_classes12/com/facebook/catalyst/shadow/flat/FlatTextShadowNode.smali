.class public abstract Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;
.super Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;
.source ""


# instance fields
.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2668614
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(LX/JH2;)V
.end method

.method public final a(Landroid/text/SpannableStringBuilder;)V
    .locals 1

    .prologue
    .line 2668610
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    iput v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->d:I

    .line 2668611
    invoke-virtual {p0, p1}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Landroid/text/SpannableStringBuilder;)V

    .line 2668612
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    iput v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->e:I

    .line 2668613
    return-void
.end method

.method public abstract a(Landroid/text/SpannableStringBuilder;IIZ)V
.end method

.method public final a(Landroid/text/SpannableStringBuilder;Z)V
    .locals 2

    .prologue
    .line 2668606
    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->d:I

    iget v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->e:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->ad()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2668607
    :cond_0
    iget v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->d:I

    iget v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->e:I

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->a(Landroid/text/SpannableStringBuilder;IIZ)V

    .line 2668608
    :cond_1
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 2668609
    const/4 v0, 0x1

    return v0
.end method

.method public ad()Z
    .locals 1

    .prologue
    .line 2668605
    const/4 v0, 0x0

    return v0
.end method

.method public ae()Z
    .locals 1

    .prologue
    .line 2668604
    const/4 v0, 0x0

    return v0
.end method

.method public abstract b(Landroid/text/SpannableStringBuilder;)V
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 2668600
    iget-object v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->h:Lcom/facebook/react/uimanager/ReactShadowNode;

    move-object v0, v0

    .line 2668601
    instance-of v1, v0, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;

    if-eqz v1, :cond_0

    .line 2668602
    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;

    invoke-virtual {v0, p1}, Lcom/facebook/catalyst/shadow/flat/FlatTextShadowNode;->b(Z)V

    .line 2668603
    :cond_0
    return-void
.end method
