.class public final Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;
.super Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;
.source ""

# interfaces
.implements LX/JGP;


# instance fields
.field private final d:Lcom/facebook/react/uimanager/ReactShadowNode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Z

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/facebook/react/uimanager/ViewManager;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2669707
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;-><init>()V

    .line 2669708
    iput-boolean v2, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->f:Z

    .line 2669709
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ViewManager;->s()Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    .line 2669710
    instance-of v1, v0, Lcom/facebook/csslayout/YogaMeasureFunction;

    if-eqz v1, :cond_0

    .line 2669711
    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->d:Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 2669712
    check-cast v0, Lcom/facebook/csslayout/YogaMeasureFunction;

    invoke-virtual {p0, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaMeasureFunction;)V

    .line 2669713
    :goto_0
    instance-of v0, p1, Lcom/facebook/react/uimanager/ViewGroupManager;

    if-eqz v0, :cond_1

    .line 2669714
    check-cast p1, Lcom/facebook/react/uimanager/ViewGroupManager;

    .line 2669715
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ViewGroupManager;->t()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->e:Z

    .line 2669716
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ViewGroupManager;->u()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->g:Z

    .line 2669717
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2669718
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->H()V

    .line 2669719
    return-void

    .line 2669720
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->d:Lcom/facebook/react/uimanager/ReactShadowNode;

    goto :goto_0

    .line 2669721
    :cond_1
    iput-boolean v2, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->e:Z

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/5rJ;)V
    .locals 1

    .prologue
    .line 2669732
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(LX/5rJ;)V

    .line 2669733
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->d:Lcom/facebook/react/uimanager/ReactShadowNode;

    if-eqz v0, :cond_0

    .line 2669734
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->d:Lcom/facebook/react/uimanager/ReactShadowNode;

    invoke-virtual {v0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(LX/5rJ;)V

    .line 2669735
    :cond_0
    return-void
.end method

.method public final a(LX/5rl;)V
    .locals 2

    .prologue
    .line 2669726
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->d:Lcom/facebook/react/uimanager/ReactShadowNode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->d:Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 2669727
    iget-boolean v1, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->f:Z

    move v0, v1

    .line 2669728
    if-eqz v0, :cond_0

    .line 2669729
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->d:Lcom/facebook/react/uimanager/ReactShadowNode;

    invoke-virtual {v0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(LX/5rl;)V

    .line 2669730
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->e()V

    .line 2669731
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V
    .locals 1

    .prologue
    .line 2669722
    invoke-super {p0, p1, p2}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(Lcom/facebook/react/uimanager/ReactShadowNode;I)V

    .line 2669723
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->g:Z

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    if-eqz v0, :cond_0

    .line 2669724
    check-cast p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    invoke-virtual {p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->H()V

    .line 2669725
    :cond_0
    return-void
.end method

.method public final b(LX/5rC;)V
    .locals 1

    .prologue
    .line 2669703
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->d:Lcom/facebook/react/uimanager/ReactShadowNode;

    if-eqz v0, :cond_0

    .line 2669704
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->d:Lcom/facebook/react/uimanager/ReactShadowNode;

    .line 2669705
    invoke-static {v0, p1}, LX/5rp;->a(Lcom/facebook/react/uimanager/ReactShadowNode;LX/5rC;)V

    .line 2669706
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 2669736
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->c(I)V

    .line 2669737
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->d:Lcom/facebook/react/uimanager/ReactShadowNode;

    if-eqz v0, :cond_0

    .line 2669738
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->d:Lcom/facebook/react/uimanager/ReactShadowNode;

    invoke-virtual {v0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c(I)V

    .line 2669739
    :cond_0
    return-void
.end method

.method public final c(IF)V
    .locals 1

    .prologue
    .line 2669698
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v0

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_0

    .line 2669699
    invoke-super {p0, p1, p2}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->c(IF)V

    .line 2669700
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->f:Z

    .line 2669701
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2669702
    :cond_0
    return-void
.end method

.method public final nh_()Z
    .locals 1

    .prologue
    .line 2669693
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->e:Z

    return v0
.end method

.method public final ni_()Z
    .locals 1

    .prologue
    .line 2669697
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->f:Z

    return v0
.end method

.method public final nj_()V
    .locals 1

    .prologue
    .line 2669695
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/NativeViewWrapper;->f:Z

    .line 2669696
    return-void
.end method

.method public final setBackgroundColor(I)V
    .locals 0

    .prologue
    .line 2669694
    return-void
.end method
