.class public final Lcom/facebook/catalyst/shadow/flat/RCTViewManager;
.super Lcom/facebook/catalyst/shadow/flat/FlatViewManager;
.source ""


# static fields
.field private static final b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2671768
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;->b:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2671767
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatViewManager;-><init>()V

    return-void
.end method

.method private static B()Lcom/facebook/catalyst/shadow/flat/RCTView;
    .locals 1

    .prologue
    .line 2671766
    new-instance v0, Lcom/facebook/catalyst/shadow/flat/RCTView;

    invoke-direct {v0}, Lcom/facebook/catalyst/shadow/flat/RCTView;-><init>()V

    return-object v0
.end method

.method private static a(Ljava/lang/String;)LX/5r3;
    .locals 2
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2671758
    if-eqz p0, :cond_1

    .line 2671759
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2671760
    :cond_1
    sget-object v0, LX/5r3;->AUTO:LX/5r3;

    :goto_1
    return-object v0

    .line 2671761
    :sswitch_0
    const-string v1, "none"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "auto"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "box-none"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v1, "box-only"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 2671762
    :pswitch_0
    sget-object v0, LX/5r3;->NONE:LX/5r3;

    goto :goto_1

    .line 2671763
    :pswitch_1
    sget-object v0, LX/5r3;->AUTO:LX/5r3;

    goto :goto_1

    .line 2671764
    :pswitch_2
    sget-object v0, LX/5r3;->BOX_NONE:LX/5r3;

    goto :goto_1

    .line 2671765
    :pswitch_3
    sget-object v0, LX/5r3;->BOX_ONLY:LX/5r3;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7c85c606 -> :sswitch_2
        -0x7c855592 -> :sswitch_3
        0x2dddaf -> :sswitch_1
        0x33af38 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(LX/JGs;ILX/5pC;)V
    .locals 5
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 2671746
    packed-switch p1, :pswitch_data_0

    .line 2671747
    :cond_0
    :goto_0
    return-void

    .line 2671748
    :pswitch_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, LX/5pC;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 2671749
    :cond_1
    new-instance v0, LX/5pA;

    const-string v1, "Illegal number of arguments for \'updateHotspot\' command"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2671750
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 2671751
    sget-object v0, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;->b:[I

    invoke-virtual {p0, v0}, LX/JGs;->getLocationOnScreen([I)V

    .line 2671752
    invoke-interface {p2, v2}, LX/5pC;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, LX/5r2;->a(D)F

    move-result v0

    sget-object v1, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;->b:[I

    aget v1, v1, v2

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 2671753
    invoke-interface {p2, v4}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v1

    sget-object v2, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;->b:[I

    aget v2, v2, v4

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 2671754
    invoke-virtual {p0, v0, v1}, LX/JGs;->drawableHotspotChanged(FF)V

    goto :goto_0

    .line 2671755
    :pswitch_1
    if-eqz p2, :cond_3

    invoke-interface {p2}, LX/5pC;->size()I

    move-result v0

    if-eq v0, v4, :cond_4

    .line 2671756
    :cond_3
    new-instance v0, LX/5pA;

    const-string v1, "Illegal number of arguments for \'setPressed\' command"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2671757
    :cond_4
    invoke-interface {p2, v2}, LX/5pC;->getBoolean(I)Z

    move-result v0

    invoke-virtual {p0, v0}, LX/JGs;->setPressed(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2671745
    check-cast p1, LX/JGs;

    invoke-static {p1, p2, p3}, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;->a(LX/JGs;ILX/5pC;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2671744
    const-string v0, "RCTView"

    return-object v0
.end method

.method public final synthetic h()Lcom/facebook/react/uimanager/LayoutShadowNode;
    .locals 1

    .prologue
    .line 2671743
    invoke-static {}, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;->B()Lcom/facebook/catalyst/shadow/flat/RCTView;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/catalyst/shadow/flat/RCTView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2671714
    const-class v0, Lcom/facebook/catalyst/shadow/flat/RCTView;

    return-object v0
.end method

.method public final synthetic s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 2671742
    invoke-static {}, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;->B()Lcom/facebook/catalyst/shadow/flat/RCTView;

    move-result-object v0

    return-object v0
.end method

.method public final setHitSlop(LX/JGs;LX/5pG;)V
    .locals 6
    .param p2    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "hitSlop"
    .end annotation

    .prologue
    .line 2671735
    if-nez p2, :cond_0

    .line 2671736
    const/4 v0, 0x0

    .line 2671737
    iput-object v0, p1, LX/JGs;->w:Landroid/graphics/Rect;

    .line 2671738
    :goto_0
    return-void

    .line 2671739
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    const-string v1, "left"

    invoke-interface {p2, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v1

    float-to-int v1, v1

    const-string v2, "top"

    invoke-interface {p2, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v2

    float-to-int v2, v2

    const-string v3, "right"

    invoke-interface {p2, v3}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, LX/5r2;->a(D)F

    move-result v3

    float-to-int v3, v3

    const-string v4, "bottom"

    invoke-interface {p2, v4}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-static {v4, v5}, LX/5r2;->a(D)F

    move-result v4

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2671740
    iput-object v0, p1, LX/JGs;->w:Landroid/graphics/Rect;

    .line 2671741
    goto :goto_0
.end method

.method public final setHotspot(LX/JGs;LX/5pG;)V
    .locals 1
    .param p2    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "nativeBackgroundAndroid"
    .end annotation

    .prologue
    .line 2671723
    if-nez p2, :cond_2

    const/4 v0, 0x0

    .line 2671724
    :goto_0
    iget-object p0, p1, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    if-eqz p0, :cond_0

    .line 2671725
    iget-object p0, p1, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2671726
    iget-object p0, p1, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, p0}, LX/JGs;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2671727
    :cond_0
    if-eqz v0, :cond_1

    .line 2671728
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2671729
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2671730
    invoke-virtual {p1}, LX/JGs;->getDrawableState()[I

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 2671731
    :cond_1
    iput-object v0, p1, LX/JGs;->q:Landroid/graphics/drawable/Drawable;

    .line 2671732
    invoke-virtual {p1}, LX/JGs;->invalidate()V

    .line 2671733
    return-void

    .line 2671734
    :cond_2
    invoke-virtual {p1}, LX/JGs;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, LX/9nV;->a(Landroid/content/Context;LX/5pG;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final setNeedsOffscreenAlphaCompositing(LX/JGs;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "needsOffscreenAlphaCompositing"
    .end annotation

    .prologue
    .line 2671721
    iput-boolean p2, p1, LX/JGs;->p:Z

    .line 2671722
    return-void
.end method

.method public final setPointerEvents(LX/JGs;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "pointerEvents"
    .end annotation

    .prologue
    .line 2671718
    invoke-static {p2}, Lcom/facebook/catalyst/shadow/flat/RCTViewManager;->a(Ljava/lang/String;)LX/5r3;

    move-result-object v0

    .line 2671719
    iput-object v0, p1, LX/JGs;->r:LX/5r3;

    .line 2671720
    return-void
.end method

.method public final setRemoveClippedSubviews(LX/JGs;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "removeClippedSubviews"
    .end annotation

    .prologue
    .line 2671716
    invoke-virtual {p1, p2}, LX/JGs;->b(Z)V

    .line 2671717
    return-void
.end method

.method public final v()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2671715
    const-string v0, "hotspotUpdate"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "setPressed"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
