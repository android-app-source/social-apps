.class public Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;
.super Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;
.source ""

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements LX/JGP;


# instance fields
.field private d:Z

.field private e:Landroid/view/Surface;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2668131
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;-><init>()V

    .line 2668132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->d:Z

    .line 2668133
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->X()V

    .line 2668134
    invoke-virtual {p0}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->H()V

    .line 2668135
    return-void
.end method

.method private ad()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2668115
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->e:Landroid/view/Surface;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->e:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2668116
    :cond_0
    invoke-direct {p0, p0}, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->e(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 2668117
    :cond_1
    :goto_0
    return-void

    .line 2668118
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->e:Landroid/view/Surface;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v2

    .line 2668119
    const/4 v1, 0x0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2668120
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    move v1, v0

    .line 2668121
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2668122
    invoke-virtual {p0, v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/views/art/ARTVirtualNode;

    .line 2668123
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v3, v4}, Lcom/facebook/react/views/art/ARTVirtualNode;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;F)V

    .line 2668124
    invoke-virtual {v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->e()V

    .line 2668125
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2668126
    :cond_3
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->e:Landroid/view/Surface;

    if-eqz v0, :cond_1

    .line 2668127
    iget-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->e:Landroid/view/Surface;

    invoke-virtual {v0, v2}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 2668128
    :catch_0
    move-exception v0

    .line 2668129
    :goto_2
    const-string v1, "React"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " in Surface.unlockCanvasAndPost"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2668130
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private e(Lcom/facebook/react/uimanager/ReactShadowNode;)V
    .locals 2

    .prologue
    .line 2668109
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2668110
    invoke-virtual {p1, v0}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v1

    .line 2668111
    invoke-virtual {v1}, Lcom/facebook/react/uimanager/ReactShadowNode;->e()V

    .line 2668112
    invoke-direct {p0, v1}, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->e(Lcom/facebook/react/uimanager/ReactShadowNode;)V

    .line 2668113
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2668114
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/5rl;)V
    .locals 1

    .prologue
    .line 2668105
    invoke-super {p0, p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(LX/5rl;)V

    .line 2668106
    iget v0, p0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, v0

    .line 2668107
    invoke-virtual {p1, v0, p0}, LX/5rl;->a(ILjava/lang/Object;)V

    .line 2668108
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2668104
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2668097
    const/4 v0, 0x1

    return v0
.end method

.method public final c(IF)V
    .locals 1

    .prologue
    .line 2668099
    invoke-virtual {p0, p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->e(I)F

    move-result v0

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_0

    .line 2668100
    invoke-super {p0, p1, p2}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->c(IF)V

    .line 2668101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->d:Z

    .line 2668102
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->f()V

    .line 2668103
    :cond_0
    return-void
.end method

.method public final nh_()Z
    .locals 1

    .prologue
    .line 2668136
    const/4 v0, 0x0

    return v0
.end method

.method public final ni_()Z
    .locals 1

    .prologue
    .line 2668098
    iget-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->d:Z

    return v0
.end method

.method public final nj_()V
    .locals 1

    .prologue
    .line 2668095
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->d:Z

    .line 2668096
    return-void
.end method

.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 2668092
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->e:Landroid/view/Surface;

    .line 2668093
    invoke-direct {p0}, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->ad()V

    .line 2668094
    return-void
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 2668089
    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->release()V

    .line 2668090
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/catalyst/shadow/flat/FlatARTSurfaceViewShadowNode;->e:Landroid/view/Surface;

    .line 2668091
    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 2668088
    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 2668087
    return-void
.end method
