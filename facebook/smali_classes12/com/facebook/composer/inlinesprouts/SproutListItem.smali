.class public Lcom/facebook/composer/inlinesprouts/SproutListItem;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""


# instance fields
.field private j:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:LX/Hsa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2516941
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2516942
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2516939
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2516940
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2516937
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2516938
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/facebook/composer/inlinesprouts/SproutListItem;
    .locals 3

    .prologue
    .line 2516936
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0313a5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/inlinesprouts/SproutListItem;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2516932
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubtitleText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2516933
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2516934
    :goto_0
    return-void

    .line 2516935
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubtitleText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 2516931
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/SproutListItem;->k:LX/Hsa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/SproutListItem;->k:LX/Hsa;

    invoke-interface {v0}, LX/Hsa;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, -0x2

    .line 2516907
    invoke-virtual {p0}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/inlinesprouts/SproutListItem;->k:LX/Hsa;

    invoke-interface {v1}, LX/Hsa;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2516908
    invoke-virtual {p0}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0b66

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2516909
    new-instance v2, LX/6VC;

    invoke-direct {v2, v3, v3}, LX/6VC;-><init>(II)V

    .line 2516910
    const/4 v3, 0x1

    iput-boolean v3, v2, LX/6VC;->b:Z

    .line 2516911
    iput v1, v2, LX/6VC;->rightMargin:I

    .line 2516912
    invoke-static {v2, v1}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;I)V

    .line 2516913
    invoke-virtual {p0, v0, v2}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2516914
    return-object v0
.end method


# virtual methods
.method public setNuxProvider(LX/Hsa;)V
    .locals 2

    .prologue
    .line 2516921
    iput-object p1, p0, Lcom/facebook/composer/inlinesprouts/SproutListItem;->k:LX/Hsa;

    .line 2516922
    invoke-direct {p0}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2516923
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/SproutListItem;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2516924
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/SproutListItem;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2516925
    :cond_0
    :goto_0
    return-void

    .line 2516926
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/SproutListItem;->j:Landroid/view/View;

    if-nez v0, :cond_2

    .line 2516927
    invoke-direct {p0}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->g()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/inlinesprouts/SproutListItem;->j:Landroid/view/View;

    .line 2516928
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/SproutListItem;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2516929
    invoke-interface {p1}, LX/Hsa;->c()Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2516930
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/SproutListItem;->j:Landroid/view/View;

    new-instance v1, LX/Htu;

    invoke-direct {v1, p0, p1}, LX/Htu;-><init>(Lcom/facebook/composer/inlinesprouts/SproutListItem;LX/Hsa;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setSubtitleText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 2516918
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2516919
    invoke-direct {p0}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->e()V

    .line 2516920
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 2516915
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2516916
    invoke-direct {p0}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->e()V

    .line 2516917
    return-void
.end method
