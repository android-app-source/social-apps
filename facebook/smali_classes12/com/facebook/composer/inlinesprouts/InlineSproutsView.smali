.class public Lcom/facebook/composer/inlinesprouts/InlineSproutsView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wW;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field public c:Lcom/facebook/widget/listview/BetterListView;

.field private d:LX/Htp;

.field public e:LX/Hti;

.field private f:LX/0wd;

.field private final g:Landroid/view/View$OnLayoutChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2516810
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2516811
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2516812
    iput-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->a:LX/0Ot;

    .line 2516813
    new-instance v0, LX/Htn;

    invoke-direct {v0, p0}, LX/Htn;-><init>(Lcom/facebook/composer/inlinesprouts/InlineSproutsView;)V

    iput-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->g:Landroid/view/View$OnLayoutChangeListener;

    .line 2516814
    invoke-direct {p0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->b()V

    .line 2516815
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2516783
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2516784
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2516785
    iput-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->a:LX/0Ot;

    .line 2516786
    new-instance v0, LX/Htn;

    invoke-direct {v0, p0}, LX/Htn;-><init>(Lcom/facebook/composer/inlinesprouts/InlineSproutsView;)V

    iput-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->g:Landroid/view/View$OnLayoutChangeListener;

    .line 2516787
    invoke-direct {p0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->b()V

    .line 2516788
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2516804
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2516805
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2516806
    iput-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->a:LX/0Ot;

    .line 2516807
    new-instance v0, LX/Htn;

    invoke-direct {v0, p0}, LX/Htn;-><init>(Lcom/facebook/composer/inlinesprouts/InlineSproutsView;)V

    iput-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->g:Landroid/view/View$OnLayoutChangeListener;

    .line 2516808
    invoke-direct {p0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->b()V

    .line 2516809
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    const/16 v1, 0x11fa

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->a:LX/0Ot;

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2516800
    const-class v0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    invoke-static {v0, p0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2516801
    const v0, 0x7f03091c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2516802
    const v0, 0x7f0d175c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->c:Lcom/facebook/widget/listview/BetterListView;

    .line 2516803
    return-void
.end method

.method public static e(Lcom/facebook/composer/inlinesprouts/InlineSproutsView;)V
    .locals 4

    .prologue
    .line 2516792
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->d:LX/Htp;

    sget-object v1, LX/Htp;->FROM_TOP:LX/Htp;

    if-ne v0, v1, :cond_0

    .line 2516793
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->c:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setPivotY(F)V

    .line 2516794
    :goto_0
    invoke-direct {p0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->getSpring()LX/0wd;

    move-result-object v0

    const/4 v1, 0x0

    .line 2516795
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 2516796
    move-object v0, v0

    .line 2516797
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2516798
    return-void

    .line 2516799
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->c:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->c:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setPivotY(F)V

    goto :goto_0
.end method

.method public static f(Lcom/facebook/composer/inlinesprouts/InlineSproutsView;)V
    .locals 1

    .prologue
    .line 2516789
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->e:LX/Hti;

    if-eqz v0, :cond_0

    .line 2516790
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->e:LX/Hti;

    invoke-interface {v0}, LX/Hti;->a()V

    .line 2516791
    :cond_0
    return-void
.end method

.method private getSpring()LX/0wd;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2516816
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->f:LX/0wd;

    if-eqz v0, :cond_0

    .line 2516817
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->f:LX/0wd;

    .line 2516818
    :goto_0
    return-object v0

    .line 2516819
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4044000000000000L    # 40.0

    const-wide/high16 v4, 0x401c000000000000L    # 7.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    new-instance v1, LX/Hto;

    invoke-direct {v1, p0}, LX/Hto;-><init>(Lcom/facebook/composer/inlinesprouts/InlineSproutsView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->f:LX/0wd;

    .line 2516820
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->f:LX/0wd;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2516775
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->setVisibility(I)V

    .line 2516776
    return-void
.end method

.method public final a(LX/Htp;)V
    .locals 1

    .prologue
    .line 2516767
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->setVisibility(I)V

    .line 2516768
    iput-object p1, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->d:LX/Htp;

    .line 2516769
    sget-object v0, LX/Htp;->FROM_BOTTOM:LX/Htp;

    if-ne p1, v0, :cond_0

    .line 2516770
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->g:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {p0, v0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2516771
    :goto_0
    return-void

    .line 2516772
    :cond_0
    sget-object v0, LX/Htp;->FROM_TOP:LX/Htp;

    if-ne p1, v0, :cond_1

    .line 2516773
    invoke-static {p0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->e(Lcom/facebook/composer/inlinesprouts/InlineSproutsView;)V

    goto :goto_0

    .line 2516774
    :cond_1
    invoke-static {p0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->f(Lcom/facebook/composer/inlinesprouts/InlineSproutsView;)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2516759
    iget v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->b:I

    if-eqz v0, :cond_0

    .line 2516760
    iget v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->b:I

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 2516761
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2516762
    return-void
.end method

.method public setExpandedMaxHeight(I)V
    .locals 1

    .prologue
    .line 2516763
    iget v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->b:I

    if-ne v0, p1, :cond_0

    .line 2516764
    :goto_0
    return-void

    .line 2516765
    :cond_0
    iput p1, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->b:I

    .line 2516766
    invoke-virtual {p0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->requestLayout()V

    goto :goto_0
.end method

.method public setSproutAdapter(LX/Hts;)V
    .locals 1

    .prologue
    .line 2516777
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->c:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2516778
    return-void
.end method

.method public setSproutItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    .prologue
    .line 2516779
    iget-object v0, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->c:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2516780
    return-void
.end method

.method public setStateChangeListener(LX/Hti;)V
    .locals 0

    .prologue
    .line 2516781
    iput-object p1, p0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->e:LX/Hti;

    .line 2516782
    return-void
.end method
