.class public Lcom/facebook/composer/album/activity/AlbumSelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/Hrz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/Hry;

.field public r:Ljava/lang/String;

.field public s:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public u:Z

.field public v:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2513678
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()LX/Hro;
    .locals 1

    .prologue
    .line 2513677
    new-instance v0, LX/Hro;

    invoke-direct {v0, p0}, LX/Hro;-><init>(Lcom/facebook/composer/album/activity/AlbumSelectorActivity;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/graphql/model/GraphQLAlbum;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2513670
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2513671
    const-string v1, "extra_composer_session_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2513672
    const-string v1, "extra_target_data"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2513673
    const-string v1, "extra_viewer_context"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2513674
    const-string v1, "extra_initial_album"

    invoke-static {v0, v1, p4}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2513675
    const-string v1, "should_hide_shared_albums"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2513676
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;

    const-class v1, LX/Hrz;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/Hrz;

    iput-object v0, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->p:LX/Hrz;

    return-void
.end method

.method private b()LX/Hrp;
    .locals 1

    .prologue
    .line 2513669
    new-instance v0, LX/Hrp;

    invoke-direct {v0, p0}, LX/Hrp;-><init>(Lcom/facebook/composer/album/activity/AlbumSelectorActivity;)V

    return-object v0
.end method

.method private l()V
    .locals 3

    .prologue
    .line 2513606
    iget-boolean v0, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->v:Z

    if-eqz v0, :cond_0

    .line 2513607
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2513608
    const-string v1, "extra_canceled_creation"

    iget-boolean v2, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->v:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2513609
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 2513610
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->finish()V

    .line 2513611
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 2513634
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2513635
    invoke-static {p0, p0}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2513636
    const v0, 0x7f0300d0

    invoke-virtual {p0, v0}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->setContentView(I)V

    .line 2513637
    invoke-virtual {p0}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2513638
    const-string v0, "extra_initial_album"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->s:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2513639
    const-string v0, "extra_composer_session_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->r:Ljava/lang/String;

    .line 2513640
    const-string v0, "extra_target_data"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->t:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2513641
    const-string v0, "should_hide_shared_albums"

    invoke-virtual {v1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->u:Z

    .line 2513642
    const-string v0, "extra_viewer_context"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2513643
    iget-object v1, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->p:LX/Hrz;

    invoke-direct {p0}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->b()LX/Hrp;

    move-result-object v2

    invoke-direct {p0}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->a()LX/Hro;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/Hrz;->a(LX/Hrp;LX/Hro;Landroid/support/v4/app/Fragment;Lcom/facebook/auth/viewercontext/ViewerContext;)LX/Hry;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->q:LX/Hry;

    .line 2513644
    iget-object v0, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->q:LX/Hry;

    const v1, 0x7f0d051b

    invoke-virtual {p0, v1}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2513645
    iget-boolean v2, v0, LX/Hry;->q:Z

    if-eqz v2, :cond_1

    .line 2513646
    :goto_0
    invoke-static {v0, v1}, LX/Hry;->b(LX/Hry;Landroid/view/View;)V

    .line 2513647
    const v2, 0x7f0d00bc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, LX/0h5;

    .line 2513648
    iget-object v3, v0, LX/Hry;->e:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2513649
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    const/4 v6, 0x1

    .line 2513650
    iput v6, v4, LX/108;->a:I

    .line 2513651
    move-object v4, v4

    .line 2513652
    iget-object v6, v0, LX/Hry;->t:LX/23P;

    const v7, 0x7f08149b

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v3, v2

    check-cast v3, Landroid/view/View;

    invoke-virtual {v6, v7, v3}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2513653
    iput-object v3, v4, LX/108;->g:Ljava/lang/String;

    .line 2513654
    move-object v3, v4

    .line 2513655
    const/4 v4, -0x2

    .line 2513656
    iput v4, v3, LX/108;->h:I

    .line 2513657
    move-object v3, v3

    .line 2513658
    invoke-virtual {v3}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v3

    .line 2513659
    const v4, 0x7f081430

    invoke-interface {v2, v4}, LX/0h5;->setTitle(I)V

    .line 2513660
    new-instance v4, LX/Hru;

    invoke-direct {v4, v0}, LX/Hru;-><init>(LX/Hry;)V

    invoke-interface {v2, v4}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2513661
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2513662
    new-instance v3, LX/Hrv;

    invoke-direct {v3, v0}, LX/Hrv;-><init>(LX/Hry;)V

    invoke-interface {v2, v3}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2513663
    if-eqz p1, :cond_0

    const-string v0, "extra_canceled_creation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2513664
    const-string v0, "extra_canceled_creation"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->v:Z

    .line 2513665
    :cond_0
    return-void

    .line 2513666
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/Hry;->q:Z

    .line 2513667
    new-instance v2, LX/Hrx;

    invoke-direct {v2, v0}, LX/Hrx;-><init>(LX/Hry;)V

    iput-object v2, v0, LX/Hry;->i:LX/Hrx;

    .line 2513668
    invoke-virtual {v0}, LX/Hry;->a()V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2513625
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2513626
    const/16 v0, 0x908

    if-ne p1, v0, :cond_1

    .line 2513627
    iget-object v0, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->q:LX/Hry;

    .line 2513628
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 2513629
    const-string v1, "extra_album"

    invoke-static {p3, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2513630
    iget-object v2, v0, LX/Hry;->h:LX/Hrp;

    const/4 p1, 0x1

    invoke-virtual {v2, v1, p1}, LX/Hrp;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Z)V

    .line 2513631
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2513632
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->v:Z

    .line 2513633
    :cond_1
    return-void
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 2513623
    invoke-direct {p0}, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->l()V

    .line 2513624
    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x6fb5ecc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2513618
    iget-object v1, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->q:LX/Hry;

    .line 2513619
    iget-boolean v2, v1, LX/Hry;->q:Z

    if-nez v2, :cond_0

    .line 2513620
    :goto_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2513621
    const/16 v1, 0x23

    const v2, -0x5109d226

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2513622
    :cond_0
    iget-object v2, v1, LX/Hry;->a:LX/9bY;

    iget-object v4, v1, LX/Hry;->i:LX/Hrx;

    invoke-virtual {v2, v4}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x435742a9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2513615
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2513616
    iget-object v1, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->q:LX/Hry;

    invoke-virtual {v1}, LX/Hry;->a()V

    .line 2513617
    const/16 v1, 0x23

    const v2, -0x293c9ce4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2513612
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2513613
    const-string v0, "extra_canceled_creation"

    iget-boolean v1, p0, Lcom/facebook/composer/album/activity/AlbumSelectorActivity;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2513614
    return-void
.end method
