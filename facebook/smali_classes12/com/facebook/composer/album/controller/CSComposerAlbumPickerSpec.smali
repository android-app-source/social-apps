.class public Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile e:Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;


# instance fields
.field public b:LX/5KP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0lB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Hpc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2513986
    const-class v0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2513987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2513988
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;
    .locals 6

    .prologue
    .line 2513989
    sget-object v0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->e:Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;

    if-nez v0, :cond_1

    .line 2513990
    const-class v1, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;

    monitor-enter v1

    .line 2513991
    :try_start_0
    sget-object v0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->e:Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2513992
    if-eqz v2, :cond_0

    .line 2513993
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2513994
    new-instance p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;

    invoke-direct {p0}, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;-><init>()V

    .line 2513995
    const-class v3, LX/5KP;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/5KP;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lB;

    const-class v5, LX/Hpc;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Hpc;

    .line 2513996
    iput-object v3, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->b:LX/5KP;

    iput-object v4, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->c:LX/0lB;

    iput-object v5, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->d:LX/Hpc;

    .line 2513997
    move-object v0, p0

    .line 2513998
    sput-object v0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->e:Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2513999
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2514000
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2514001
    :cond_1
    sget-object v0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->e:Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;

    return-object v0

    .line 2514002
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2514003
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
