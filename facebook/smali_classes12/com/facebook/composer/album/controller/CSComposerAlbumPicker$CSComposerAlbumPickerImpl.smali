.class public final Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Hs4;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/common/callercontext/CallerContext;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Boolean;

.field public g:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAlbum;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/java2js/JSContext;

.field public final synthetic i:LX/Hs4;


# direct methods
.method public constructor <init>(LX/Hs4;)V
    .locals 1

    .prologue
    .line 2513912
    iput-object p1, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->i:LX/Hs4;

    .line 2513913
    move-object v0, p1

    .line 2513914
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2513915
    sget-object v0, Lcom/facebook/composer/album/controller/CSComposerAlbumPickerSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2513916
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2513917
    const-string v0, "CSComposerAlbumPicker"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2513918
    if-ne p0, p1, :cond_1

    .line 2513919
    :cond_0
    :goto_0
    return v0

    .line 2513920
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2513921
    goto :goto_0

    .line 2513922
    :cond_3
    check-cast p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;

    .line 2513923
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2513924
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2513925
    if-eq v2, v3, :cond_0

    .line 2513926
    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2513927
    goto :goto_0

    .line 2513928
    :cond_5
    iget-object v2, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_4

    .line 2513929
    :cond_6
    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2513930
    goto :goto_0

    .line 2513931
    :cond_8
    iget-object v2, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2513932
    :cond_9
    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2513933
    goto :goto_0

    .line 2513934
    :cond_b
    iget-object v2, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2513935
    :cond_c
    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2513936
    goto :goto_0

    .line 2513937
    :cond_e
    iget-object v2, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->d:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2513938
    :cond_f
    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->e:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 2513939
    goto :goto_0

    .line 2513940
    :cond_11
    iget-object v2, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->e:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 2513941
    :cond_12
    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->f:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->f:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 2513942
    goto/16 :goto_0

    .line 2513943
    :cond_14
    iget-object v2, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->f:Ljava/lang/Boolean;

    if-nez v2, :cond_13

    .line 2513944
    :cond_15
    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->g:LX/0QK;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->g:LX/0QK;

    iget-object v3, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->g:LX/0QK;

    invoke-interface {v2, v3}, LX/0QK;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 2513945
    goto/16 :goto_0

    .line 2513946
    :cond_17
    iget-object v2, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->g:LX/0QK;

    if-nez v2, :cond_16

    .line 2513947
    :cond_18
    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->h:Lcom/facebook/java2js/JSContext;

    if-eqz v2, :cond_19

    iget-object v2, p0, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->h:Lcom/facebook/java2js/JSContext;

    iget-object v3, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->h:Lcom/facebook/java2js/JSContext;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2513948
    goto/16 :goto_0

    .line 2513949
    :cond_19
    iget-object v2, p1, Lcom/facebook/composer/album/controller/CSComposerAlbumPicker$CSComposerAlbumPickerImpl;->h:Lcom/facebook/java2js/JSContext;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
