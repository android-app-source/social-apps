.class public Lcom/facebook/composer/header/ComposerHeaderViewController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesLastXyTagChangeTime;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j8;",
        ":",
        "LX/0j4;",
        ":",
        "LX/0jD;",
        ":",
        "LX/0j6;",
        ":",
        "LX/0ip;",
        ":",
        "LX/0jG;",
        "DerivedData::",
        "LX/5Qz;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Lcom/facebook/composer/header/ComposerHeaderView;

.field private final c:LX/1Ad;

.field private final d:LX/8zF;

.field private final e:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/Htb;

.field private final g:LX/7l0;

.field private final h:LX/0Uh;

.field private final i:Lcom/facebook/user/model/User;

.field private final j:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final k:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/Hr0;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/HtU;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:LX/1ln;

.field private p:Landroid/net/Uri;

.field private q:Landroid/text/SpannedString;

.field private r:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/fbui/facepile/FacepileView;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:Landroid/text/style/ClickableSpan;

.field private u:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2516435
    const-class v0, Lcom/facebook/composer/header/ComposerHeaderView;

    const-string v1, "composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/header/ComposerHeaderViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Landroid/content/res/Resources;Landroid/content/Context;LX/0ad;Lcom/facebook/user/model/User;LX/Htc;LX/7l0;LX/0Uh;LX/0il;Landroid/view/ViewStub;LX/Hr0;)V
    .locals 3
    .param p5    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p9    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # LX/Hr0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ad;",
            "Landroid/content/res/Resources;",
            "Landroid/content/Context;",
            "LX/0ad;",
            "Lcom/facebook/user/model/User;",
            "LX/Htc;",
            "LX/7l0;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "TServices;",
            "Landroid/view/ViewStub;",
            "LX/Hr0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2516416
    new-instance v0, LX/HtU;

    invoke-direct {v0, p0}, LX/HtU;-><init>(Lcom/facebook/composer/header/ComposerHeaderViewController;)V

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->l:LX/HtU;

    .line 2516417
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2516418
    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->s:LX/0Px;

    .line 2516419
    iput-object p1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->c:LX/1Ad;

    .line 2516420
    iput-object p5, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->i:Lcom/facebook/user/model/User;

    .line 2516421
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->l:LX/HtU;

    .line 2516422
    new-instance p1, LX/Htb;

    const-class v1, Landroid/content/Context;

    invoke-interface {p6, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p6}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    const/16 p5, 0x198e

    invoke-static {p6, p5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p5

    invoke-direct {p1, v1, v2, p5, v0}, LX/Htb;-><init>(Landroid/content/Context;Lcom/facebook/user/model/User;LX/0Ot;LX/HtU;)V

    .line 2516423
    move-object v0, p1

    .line 2516424
    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->f:LX/Htb;

    .line 2516425
    iput-object p7, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->g:LX/7l0;

    .line 2516426
    iput-object p8, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->h:LX/0Uh;

    .line 2516427
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p9}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->j:Ljava/lang/ref/WeakReference;

    .line 2516428
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p11}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->k:Ljava/lang/ref/WeakReference;

    .line 2516429
    new-instance v0, LX/8zF;

    invoke-direct {v0, p2}, LX/8zF;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->d:LX/8zF;

    .line 2516430
    new-instance v0, LX/1Uo;

    invoke-direct {v0, p2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2516431
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-static {v0, p3}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->e:LX/1aX;

    .line 2516432
    sget-short v0, LX/1EB;->at:S

    const/4 v1, 0x0

    invoke-interface {p4, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-direct {p0, p10, v0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->a(Landroid/view/ViewStub;Z)Lcom/facebook/composer/header/ComposerHeaderView;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    .line 2516433
    invoke-direct {p0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->e()V

    .line 2516434
    return-void
.end method

.method private a(Landroid/view/ViewStub;Z)Lcom/facebook/composer/header/ComposerHeaderView;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2516401
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/header/ComposerHeaderView;

    .line 2516402
    invoke-virtual {v0, p2}, Lcom/facebook/composer/header/ComposerHeaderView;->a(Z)V

    .line 2516403
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->e:LX/1aX;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/header/ComposerHeaderView;->setMinutiaeIconHolder(LX/1aX;)V

    .line 2516404
    iget-object v1, v0, Lcom/facebook/composer/header/ComposerHeaderView;->s:LX/0zw;

    move-object v1, v1

    .line 2516405
    iput-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->r:LX/0zw;

    .line 2516406
    iget-object v1, v0, Lcom/facebook/composer/header/ComposerHeaderView;->t:Landroid/view/View;

    move-object v1, v1

    .line 2516407
    if-eqz v1, :cond_0

    .line 2516408
    new-instance v2, LX/HtV;

    invoke-direct {v2, p0}, LX/HtV;-><init>(Lcom/facebook/composer/header/ComposerHeaderViewController;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2516409
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 2516410
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldShowPageVoiceSwitcher()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2516411
    invoke-direct {p0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->h()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/header/ComposerHeaderView;->setProfileImageClickListener(Landroid/view/View$OnClickListener;)V

    .line 2516412
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/header/ComposerHeaderView;->setFocusable(Z)V

    .line 2516413
    :goto_0
    return-object v0

    .line 2516414
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/composer/header/ComposerHeaderView;->setFocusable(Z)V

    goto :goto_0
.end method

.method private a(LX/0il;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2516394
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEditTagEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    move v6, v7

    .line 2516395
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->f:LX/Htb;

    invoke-direct {p0, p1}, Lcom/facebook/composer/header/ComposerHeaderViewController;->b(LX/0il;)LX/0Px;

    move-result-object v1

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0jG;

    invoke-interface {v2}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v2

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j8;

    invoke-interface {v3}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v3

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0j8;

    invoke-interface {v4}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    check-cast v5, LX/0ip;

    invoke-interface {v5}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v5

    if-nez v6, :cond_2

    move v6, v7

    :goto_1
    invoke-virtual/range {v0 .. v6}, LX/Htb;->a(LX/0Px;Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/lang/String;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Z)Landroid/text/SpannedString;

    move-result-object v0

    .line 2516396
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->q:Landroid/text/SpannedString;

    invoke-virtual {v0, v1}, Landroid/text/SpannedString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2516397
    invoke-direct {p0, v0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->a(Landroid/text/SpannedString;)V

    .line 2516398
    :cond_0
    return-void

    :cond_1
    move v6, v8

    .line 2516399
    goto :goto_0

    :cond_2
    move v6, v8

    .line 2516400
    goto :goto_1
.end method

.method private a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 2516388
    iput-object p1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->p:Landroid/net/Uri;

    .line 2516389
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->c:LX/1Ad;

    sget-object v1, Lcom/facebook/composer/header/ComposerHeaderViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->e:LX/1aX;

    .line 2516390
    iget-object v2, v1, LX/1aX;->f:LX/1aZ;

    move-object v1, v2

    .line 2516391
    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    new-instance v1, LX/HtW;

    invoke-direct {v1, p0}, LX/HtW;-><init>(Lcom/facebook/composer/header/ComposerHeaderViewController;)V

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2516392
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->e:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 2516393
    return-void
.end method

.method private a(Landroid/text/SpannedString;)V
    .locals 1

    .prologue
    .line 2516382
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->q:Landroid/text/SpannedString;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->q:Landroid/text/SpannedString;

    invoke-virtual {v0, p1}, Landroid/text/SpannedString;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2516383
    :cond_0
    :goto_0
    return-void

    .line 2516384
    :cond_1
    iput-object p1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->q:Landroid/text/SpannedString;

    .line 2516385
    invoke-direct {p0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->d()V

    .line 2516386
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->o:LX/1ln;

    if-eqz v0, :cond_0

    .line 2516387
    invoke-direct {p0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->f()V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/composer/header/ComposerHeaderViewController;LX/1ln;)V
    .locals 0

    .prologue
    .line 2516302
    iput-object p1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->o:LX/1ln;

    .line 2516303
    invoke-direct {p0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->f()V

    .line 2516304
    return-void
.end method

.method private b(LX/0il;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2516379
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0jD;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v1

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->g:LX/7l0;

    iget-object v3, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->i:Lcom/facebook/user/model/User;

    .line 2516380
    iget-object v4, v3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2516381
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v1, v0, v2, v4, v5}, LX/7ky;->a(LX/0Px;LX/0Px;LX/7l0;J)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2516372
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    .line 2516373
    iget-object v1, v0, Lcom/facebook/composer/header/ComposerHeaderView;->t:Landroid/view/View;

    move-object v1, v1

    .line 2516374
    if-eqz v1, :cond_0

    .line 2516375
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2516376
    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5Qz;

    invoke-interface {v0}, LX/5Qz;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2516377
    :cond_0
    return-void

    .line 2516378
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private c(LX/0il;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2516353
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->r:LX/0zw;

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0jD;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->s:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2516354
    :cond_0
    :goto_0
    return-void

    .line 2516355
    :cond_1
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0jD;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v5

    .line 2516356
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->s:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    move v0, v2

    :goto_1
    move v4, v3

    .line 2516357
    :goto_2
    if-nez v0, :cond_4

    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->s:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v4, v1, :cond_4

    .line 2516358
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->s:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 2516359
    :goto_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_2
    move v0, v3

    .line 2516360
    goto :goto_1

    :cond_3
    move v0, v3

    .line 2516361
    goto :goto_3

    .line 2516362
    :cond_4
    if-eqz v0, :cond_0

    .line 2516363
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2516364
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0jD;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v3

    :goto_4
    if-ge v1, v5, :cond_5

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 2516365
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2516366
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2516367
    :cond_5
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->s:LX/0Px;

    .line 2516368
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->s:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2516369
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->r:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2516370
    :cond_6
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->r:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->s:LX/0Px;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 2516371
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->r:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 2516342
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2516343
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldShowPageVoiceSwitcher()Z

    move-result v1

    .line 2516344
    iget-object v2, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->m:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->g()Landroid/text/style/ClickableSpan;

    move-result-object v0

    :goto_0
    invoke-static {v2, v0}, LX/94g;->a(Ljava/lang/String;Landroid/text/style/ClickableSpan;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2516345
    iget-object v2, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->q:Landroid/text/SpannedString;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->q:Landroid/text/SpannedString;

    invoke-virtual {v2}, Landroid/text/SpannedString;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 2516346
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->q:Landroid/text/SpannedString;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2516347
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    invoke-virtual {v1}, Lcom/facebook/composer/header/ComposerHeaderView;->e()V

    .line 2516348
    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/header/ComposerHeaderView;->setText(Landroid/text/SpannableStringBuilder;)V

    .line 2516349
    return-void

    .line 2516350
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2516351
    :cond_2
    if-eqz v1, :cond_0

    .line 2516352
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    invoke-virtual {v1}, Lcom/facebook/composer/header/ComposerHeaderView;->d()V

    goto :goto_1
.end method

.method private e()V
    .locals 6

    .prologue
    .line 2516317
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2516318
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j4;

    invoke-interface {v1}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-static {v1}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v4

    .line 2516319
    if-eqz v4, :cond_5

    .line 2516320
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->h:LX/0Uh;

    sget v2, LX/7l1;->a:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2516321
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j4;

    invoke-interface {v1}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPageName()Ljava/lang/String;

    move-result-object v2

    .line 2516322
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j4;

    invoke-interface {v1}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPageProfilePicUrl()Ljava/lang/String;

    move-result-object v1

    move-object v3, v2

    move-object v2, v1

    .line 2516323
    :goto_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j3;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldShowPageVoiceSwitcher()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2516324
    iget-object v5, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    if-eqz v4, :cond_6

    const v1, 0x7f0814ba

    :goto_1
    invoke-virtual {v5, v1}, Lcom/facebook/composer/header/ComposerHeaderView;->setProfileImageContentDescription(I)V

    .line 2516325
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->m:Ljava/lang/String;

    invoke-static {v3, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2516326
    iput-object v3, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->m:Ljava/lang/String;

    .line 2516327
    invoke-direct {p0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->d()V

    .line 2516328
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->n:Ljava/lang/String;

    invoke-static {v2, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2516329
    iput-object v2, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->n:Ljava/lang/String;

    .line 2516330
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    iget-object v2, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->n:Ljava/lang/String;

    invoke-static {v2}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/composer/header/ComposerHeaderViewController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/composer/header/ComposerHeaderView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2516331
    :cond_2
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0ip;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0ip;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->p:Landroid/net/Uri;

    if-eq v1, v2, :cond_3

    .line 2516332
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0ip;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/composer/header/ComposerHeaderViewController;->a(Landroid/net/Uri;)V

    .line 2516333
    :cond_3
    invoke-direct {p0, v0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->a(LX/0il;)V

    .line 2516334
    invoke-direct {p0, v0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->c(LX/0il;)V

    .line 2516335
    invoke-direct {p0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->c()V

    .line 2516336
    return-void

    .line 2516337
    :cond_4
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v2, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    .line 2516338
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j6;

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetProfilePicUrl:Ljava/lang/String;

    move-object v3, v2

    move-object v2, v1

    goto/16 :goto_0

    .line 2516339
    :cond_5
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->i:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v2

    .line 2516340
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->i:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    move-object v3, v2

    move-object v2, v1

    goto/16 :goto_0

    .line 2516341
    :cond_6
    const v1, 0x7f0814b9

    goto/16 :goto_1
.end method

.method private f()V
    .locals 5

    .prologue
    .line 2516313
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->o:LX/1ln;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2516314
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->e:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0}, LX/1af;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    invoke-virtual {v2}, Lcom/facebook/composer/header/ComposerHeaderView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->o:LX/1ln;

    iget-object v3, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->p:Landroid/net/Uri;

    iget-object v4, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    invoke-virtual {v4}, Lcom/facebook/composer/header/ComposerHeaderView;->getAscentForText()F

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, LX/8zF;->a(Landroid/graphics/drawable/Drawable;Landroid/text/SpannableStringBuilder;LX/1ln;Landroid/net/Uri;F)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 2516315
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/header/ComposerHeaderView;->setText(Landroid/text/SpannableStringBuilder;)V

    .line 2516316
    return-void
.end method

.method private g()Landroid/text/style/ClickableSpan;
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 2516310
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->t:Landroid/text/style/ClickableSpan;

    if-nez v0, :cond_0

    .line 2516311
    new-instance v0, LX/HtX;

    invoke-direct {v0, p0}, LX/HtX;-><init>(Lcom/facebook/composer/header/ComposerHeaderViewController;)V

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->t:Landroid/text/style/ClickableSpan;

    .line 2516312
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->t:Landroid/text/style/ClickableSpan;

    return-object v0
.end method

.method private h()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2516307
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->u:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2516308
    new-instance v0, LX/HtY;

    invoke-direct {v0, p0}, LX/HtY;-><init>(Lcom/facebook/composer/header/ComposerHeaderViewController;)V

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->u:Landroid/view/View$OnClickListener;

    .line 2516309
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderViewController;->u:Landroid/view/View$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 2516306
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2516305
    invoke-direct {p0}, Lcom/facebook/composer/header/ComposerHeaderViewController;->e()V

    return-void
.end method
