.class public Lcom/facebook/composer/header/ComposerHeaderView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field private j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private k:Lcom/facebook/resources/ui/FbTextView;

.field public l:Landroid/view/ViewStub;

.field public m:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field public q:Landroid/view/ViewStub;

.field private r:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/fbui/facepile/FacepileView;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2516242
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/composer/header/ComposerHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2516243
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2516244
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2516245
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2516246
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2516247
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2516248
    if-eqz p1, :cond_0

    .line 2516249
    const v0, 0x7f030316

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2516250
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0a74

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderView;->s:LX/0zw;

    .line 2516251
    const v0, 0x7f0d0a76

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->t:Landroid/view/View;

    .line 2516252
    :goto_0
    const v0, 0x7f0d0a80

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2516253
    const v0, 0x7f0d0a81

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2516254
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->k:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/Be3;

    invoke-direct {v1}, LX/Be3;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2516255
    const v0, 0x7f0d0a82

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->l:Landroid/view/ViewStub;

    .line 2516256
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0a84

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderView;->m:LX/0zw;

    .line 2516257
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0a86

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderView;->n:LX/0zw;

    .line 2516258
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0a88

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderView;->o:LX/0zw;

    .line 2516259
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0a8a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderView;->p:LX/0zw;

    .line 2516260
    const v0, 0x7f0d0a8c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->q:Landroid/view/ViewStub;

    .line 2516261
    return-void

    .line 2516262
    :cond_0
    const v0, 0x7f030324

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2516263
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/composer/header/ComposerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020a13

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v3, v3, v1, v3}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2516264
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2516265
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0, v1, v1, v1, v1}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2516266
    return-void
.end method

.method public getAlbumPillViewStub()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 2516267
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->q:Landroid/view/ViewStub;

    return-object v0
.end method

.method public getAscentForText()F
    .locals 1

    .prologue
    .line 2516268
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    return v0
.end method

.method public getFixedPrivacyPillView()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2516269
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->o:LX/0zw;

    return-object v0
.end method

.method public getImplicitLocationPillView()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2516212
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->p:LX/0zw;

    return-object v0
.end method

.method public getLoadingPrivacyPillViewStub()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 2516270
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->l:Landroid/view/ViewStub;

    return-object v0
.end method

.method public getSelectablePrivacyPillView()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2516241
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->m:LX/0zw;

    return-object v0
.end method

.method public getTagButton()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2516271
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->t:Landroid/view/View;

    return-object v0
.end method

.method public getTagExpansionPillView()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2516240
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->n:LX/0zw;

    return-object v0
.end method

.method public getTagPictureList()LX/0zw;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0zw",
            "<",
            "Lcom/facebook/fbui/facepile/FacepileView;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2516239
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->s:LX/0zw;

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2516238
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x3c362dfa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2516234
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onAttachedToWindow()V

    .line 2516235
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderView;->r:LX/1aX;

    if-eqz v1, :cond_0

    .line 2516236
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderView;->r:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 2516237
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x583a540c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7e25c490

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2516230
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onDetachedFromWindow()V

    .line 2516231
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderView;->r:LX/1aX;

    if-eqz v1, :cond_0

    .line 2516232
    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderView;->r:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 2516233
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x3f57dca3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2516226
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->onFinishTemporaryDetach()V

    .line 2516227
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->r:LX/1aX;

    if-eqz v0, :cond_0

    .line 2516228
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->r:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2516229
    :cond_0
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2516222
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->onStartTemporaryDetach()V

    .line 2516223
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->r:LX/1aX;

    if-eqz v0, :cond_0

    .line 2516224
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->r:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 2516225
    :cond_0
    return-void
.end method

.method public setMinutiaeIconHolder(LX/1aX;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2516219
    iput-object p1, p0, Lcom/facebook/composer/header/ComposerHeaderView;->r:LX/1aX;

    .line 2516220
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->r:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->h()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0}, LX/1af;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/header/ComposerHeaderView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2516221
    return-void
.end method

.method public setProfileImageClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2516217
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2516218
    return-void
.end method

.method public setProfileImageContentDescription(I)V
    .locals 2

    .prologue
    .line 2516215
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/composer/header/ComposerHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2516216
    return-void
.end method

.method public setText(Landroid/text/SpannableStringBuilder;)V
    .locals 1

    .prologue
    .line 2516213
    iget-object v0, p0, Lcom/facebook/composer/header/ComposerHeaderView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2516214
    return-void
.end method
