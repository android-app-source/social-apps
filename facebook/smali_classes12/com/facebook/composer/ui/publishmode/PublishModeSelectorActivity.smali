.class public Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/Hvw;


# instance fields
.field public p:LX/Hvy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/Hw9;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/Hw8;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2520031
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/5Rn;LX/0Px;Ljava/lang/Long;)Landroid/content/Intent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/5Rn;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "Ljava/lang/Long;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 2520025
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2520026
    const-string v1, "publishMode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2520027
    if-eqz p3, :cond_0

    .line 2520028
    const-string v1, "scheduleTime"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2520029
    :cond_0
    const-string v1, "composerAttachments"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2520030
    return-object v0
.end method

.method private static a(Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;LX/Hvy;LX/Hw9;)V
    .locals 0

    .prologue
    .line 2520065
    iput-object p1, p0, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->p:LX/Hvy;

    iput-object p2, p0, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->q:LX/Hw9;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;

    invoke-static {v1}, LX/Hvy;->b(LX/0QB;)LX/Hvy;

    move-result-object v0

    check-cast v0, LX/Hvy;

    const-class v2, LX/Hw9;

    invoke-interface {v1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Hw9;

    invoke-static {p0, v0, v1}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->a(Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;LX/Hvy;LX/Hw9;)V

    return-void
.end method

.method private static a(LX/5Rn;LX/0Px;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5Rn;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2520066
    sget-object v1, LX/5Rn;->SAVE_DRAFT:LX/5Rn;

    if-eq p0, v1, :cond_1

    .line 2520067
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, LX/7kq;->j(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;LX/5Rn;J)V
    .locals 2

    .prologue
    .line 2520059
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2520060
    const-string v1, "selectedPublishMode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2520061
    const-string v1, "scheduleTime"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2520062
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 2520063
    invoke-virtual {p0}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->finish()V

    .line 2520064
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2520055
    sget-object v0, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    iget-object v1, p0, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->r:LX/Hw8;

    .line 2520056
    iget-object v4, v1, LX/Hw8;->d:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    move-wide v2, v4

    .line 2520057
    invoke-static {p0, v0, v2, v3}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->a$redex0(Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;LX/5Rn;J)V

    .line 2520058
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 2520032
    invoke-static {p0, p0}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2520033
    invoke-virtual {p0}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "publishMode"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5Rn;

    .line 2520034
    invoke-virtual {p0}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "scheduleTime"

    const-wide/16 v6, -0x1

    invoke-virtual {v1, v2, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 2520035
    invoke-virtual {p0}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "composerAttachments"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    .line 2520036
    iget-object v1, p0, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->q:LX/Hw9;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, LX/Hw9;->a(LX/Hvw;Ljava/lang/Long;)LX/Hw8;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->r:LX/Hw8;

    .line 2520037
    const v1, 0x7f0310a3

    invoke-virtual {p0, v1}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->setContentView(I)V

    .line 2520038
    const v1, 0x7f0d00bc

    invoke-virtual {p0, v1}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/0h5;

    .line 2520039
    const v2, 0x7f081455

    invoke-interface {v1, v2}, LX/0h5;->setTitle(I)V

    .line 2520040
    new-instance v2, LX/Hvu;

    invoke-direct {v2, p0}, LX/Hvu;-><init>(Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;)V

    invoke-interface {v1, v2}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2520041
    const v1, 0x7f0d27a5

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2520042
    invoke-static {}, LX/5Rn;->values()[LX/5Rn;

    move-result-object v8

    array-length v9, v8

    move v3, v4

    :goto_0
    if-ge v3, v9, :cond_3

    aget-object v10, v8, v3

    .line 2520043
    invoke-static {v10, v5}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->a(LX/5Rn;LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2520044
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v11, 0x7f0310a2

    invoke-virtual {v2, v11, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2520045
    iget-object v11, p0, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->p:LX/Hvy;

    invoke-virtual {v11, v10}, LX/Hvy;->a(LX/5Rn;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2520046
    if-ne v10, v0, :cond_0

    .line 2520047
    const v11, 0x7f0e0652

    invoke-virtual {v2, v11}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2520048
    :cond_0
    new-instance v11, LX/Hvv;

    invoke-direct {v11, p0, v10}, LX/Hvv;-><init>(Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;LX/5Rn;)V

    invoke-virtual {v2, v11}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2520049
    sget-object v11, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    if-ne v10, v11, :cond_1

    const-wide/16 v10, 0x0

    cmp-long v10, v6, v10

    if-lez v10, :cond_1

    .line 2520050
    sget-object v10, LX/6VF;->MEDIUM:LX/6VF;

    invoke-virtual {v2, v10}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2520051
    iget-object v10, p0, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->p:LX/Hvy;

    invoke-virtual {v10, v6, v7}, LX/Hvy;->a(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2520052
    :cond_1
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 2520053
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2520054
    :cond_3
    return-void
.end method
