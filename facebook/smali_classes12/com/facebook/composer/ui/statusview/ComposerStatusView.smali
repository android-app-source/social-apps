.class public Lcom/facebook/composer/ui/statusview/ComposerStatusView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/view/ViewStub;

.field public b:Landroid/view/ViewStub;

.field public c:Lcom/facebook/composer/ui/text/ComposerEditText;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Landroid/view/ViewStub;

.field public e:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/HvU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/0w3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Hqd;

.field private j:Z

.field private k:Z

.field public l:LX/Hqg;

.field public m:LX/Hqi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2520192
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2520193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->j:Z

    .line 2520194
    invoke-direct {p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->e()V

    .line 2520195
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2520233
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2520234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->j:Z

    .line 2520235
    invoke-direct {p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->e()V

    .line 2520236
    return-void
.end method

.method private static a(Lcom/facebook/composer/ui/statusview/ComposerStatusView;Landroid/view/inputmethod/InputMethodManager;LX/HvU;LX/0ad;LX/0w3;)V
    .locals 0

    .prologue
    .line 2520237
    iput-object p1, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->e:Landroid/view/inputmethod/InputMethodManager;

    iput-object p2, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->f:LX/HvU;

    iput-object p3, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->g:LX/0ad;

    iput-object p4, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->h:LX/0w3;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-static {v3}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const-class v1, LX/HvU;

    invoke-interface {v3, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/HvU;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {v3}, LX/0w3;->a(LX/0QB;)LX/0w3;

    move-result-object v3

    check-cast v3, LX/0w3;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->a(Lcom/facebook/composer/ui/statusview/ComposerStatusView;Landroid/view/inputmethod/InputMethodManager;LX/HvU;LX/0ad;LX/0w3;)V

    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 2520238
    const-class v0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-static {v0, p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2520239
    const v0, 0x7f03034b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2520240
    invoke-virtual {p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b03ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2520241
    invoke-virtual {p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0, v1, v0, v2, v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->setPadding(IIII)V

    .line 2520242
    const v0, 0x7f0d0adc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->a:Landroid/view/ViewStub;

    .line 2520243
    const v0, 0x7f0d0ae2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->b:Landroid/view/ViewStub;

    .line 2520244
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->h:LX/0w3;

    .line 2520245
    iget-boolean v1, v0, LX/0w3;->f:Z

    move v0, v1

    .line 2520246
    iput-boolean v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->k:Z

    .line 2520247
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 2520248
    new-instance v0, LX/HwB;

    invoke-direct {v0, p0}, LX/HwB;-><init>(Lcom/facebook/composer/ui/statusview/ComposerStatusView;)V

    .line 2520249
    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2520250
    new-instance v0, LX/HwC;

    invoke-direct {v0, p0}, LX/HwC;-><init>(Lcom/facebook/composer/ui/statusview/ComposerStatusView;)V

    .line 2520251
    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2520252
    new-instance v0, LX/HwD;

    invoke-direct {v0, p0}, LX/HwD;-><init>(Lcom/facebook/composer/ui/statusview/ComposerStatusView;)V

    .line 2520253
    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2520254
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 2520255
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->l:LX/Hqg;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->k:Z

    iget-object v1, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->h:LX/0w3;

    .line 2520256
    iget-boolean v2, v1, LX/0w3;->f:Z

    move v1, v2

    .line 2520257
    if-ne v0, v1, :cond_1

    .line 2520258
    :cond_0
    :goto_0
    return-void

    .line 2520259
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->h:LX/0w3;

    .line 2520260
    iget-boolean v1, v0, LX/0w3;->f:Z

    move v0, v1

    .line 2520261
    iput-boolean v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->k:Z

    .line 2520262
    iget-boolean v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->k:Z

    if-eqz v0, :cond_2

    .line 2520263
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->l:LX/Hqg;

    invoke-interface {v0}, LX/Hqg;->a()V

    goto :goto_0

    .line 2520264
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->l:LX/Hqg;

    invoke-interface {v0}, LX/Hqg;->b()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2520265
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    .line 2520266
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->requestFocus()Z

    .line 2520267
    :cond_0
    return-void
.end method

.method public final a(LX/Be0;)V
    .locals 1

    .prologue
    .line 2520302
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    .line 2520303
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/text/ComposerEditText;->a(LX/Be0;)V

    .line 2520304
    :cond_0
    return-void
.end method

.method public final a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2520268
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    .line 2520269
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/text/ComposerEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2520270
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    .line 2520271
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->b:Landroid/view/ViewStub;

    const v1, 0x7f0313bf

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2520272
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->b:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 2520273
    const v0, 0x7f0d1913

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/text/ComposerEditText;

    iput-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    .line 2520274
    const v0, 0x7f0d2d92

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2520275
    iget-object v2, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v2, v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->a(Landroid/view/View;)V

    .line 2520276
    invoke-virtual {p0, p1}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->b(Z)Landroid/graphics/Rect;

    move-result-object v0

    .line 2520277
    iget-object v2, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    iget v3, v0, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/graphics/Rect;->top:I

    iget v5, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->setPadding(IIII)V

    .line 2520278
    invoke-direct {p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->f()V

    .line 2520279
    const v0, 0x7f0d2d93

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->d:Landroid/view/ViewStub;

    .line 2520280
    new-instance v0, LX/HwA;

    invoke-direct {v0, p0}, LX/HwA;-><init>(Lcom/facebook/composer/ui/statusview/ComposerStatusView;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2520281
    return-void
.end method

.method public final b(Z)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 2520282
    invoke-virtual {p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2520283
    const v1, 0x7f0b0c7f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2520284
    if-eqz p1, :cond_0

    const v2, 0x7f0b0ca2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2520285
    :goto_0
    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v0, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v2

    .line 2520286
    :cond_0
    const v2, 0x7f0b0c7e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2520287
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-nez v0, :cond_0

    .line 2520288
    :goto_0
    return-void

    .line 2520289
    :cond_0
    new-instance v0, Lcom/facebook/composer/ui/statusview/ComposerStatusView$5;

    invoke-direct {v0, p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView$5;-><init>(Lcom/facebook/composer/ui/statusview/ComposerStatusView;)V

    .line 2520290
    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public final b(LX/Be0;)V
    .locals 1

    .prologue
    .line 2520291
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    .line 2520292
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/text/ComposerEditText;->b(LX/Be0;)V

    .line 2520293
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2520294
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->e:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2520295
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2520230
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    .line 2520231
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/text/ComposerEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2520232
    :cond_0
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2520296
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2520297
    iget-boolean v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->j:Z

    if-eqz v0, :cond_0

    .line 2520298
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->j:Z

    .line 2520299
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->i:LX/Hqd;

    if-eqz v0, :cond_0

    .line 2520300
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->i:LX/Hqd;

    invoke-interface {v0}, LX/Hqd;->a()V

    .line 2520301
    :cond_0
    return-void
.end method

.method public getEditTextDefaultTextSizeSp()F
    .locals 2

    .prologue
    .line 2520191
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getTextSize()F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEditTextDefaultTypeface()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 2520196
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public getEditTextLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2520197
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEditTextViewSetter()LX/Be2;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2520198
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    return-object v0
.end method

.method public getPluginHeaderViewStub()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 2520199
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->a:Landroid/view/ViewStub;

    return-object v0
.end method

.method public getPluginStatusTextViewStub()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 2520200
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->b:Landroid/view/ViewStub;

    return-object v0
.end method

.method public getShowsTagExpansionInfo()LX/8p2;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2520201
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    return-object v0
.end method

.method public getTransliterationViewStub()Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 2520202
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->d:Landroid/view/ViewStub;

    return-object v0
.end method

.method public final onMeasure(II)V
    .locals 0

    .prologue
    .line 2520203
    invoke-direct {p0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->g()V

    .line 2520204
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 2520205
    return-void
.end method

.method public setAfterFirstDrawListener(LX/Hqd;)V
    .locals 0

    .prologue
    .line 2520206
    iput-object p1, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->i:LX/Hqd;

    .line 2520207
    return-void
.end method

.method public setFriendTaggingEnabled(Z)V
    .locals 1

    .prologue
    .line 2520208
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    .line 2520209
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setIncludeFriends(Z)V

    .line 2520210
    :cond_0
    return-void
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2520211
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    .line 2520212
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/text/ComposerEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2520213
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->c()V

    .line 2520214
    :cond_0
    return-void
.end method

.method public setOnEditTextTopPositionChangeListener(LX/Hqi;)V
    .locals 0

    .prologue
    .line 2520215
    iput-object p1, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->m:LX/Hqi;

    .line 2520216
    return-void
.end method

.method public setOnKeyboardStateChangeListener(LX/Hqg;)V
    .locals 0

    .prologue
    .line 2520217
    iput-object p1, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->l:LX/Hqg;

    .line 2520218
    return-void
.end method

.method public setStatusText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2520219
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    .line 2520220
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/text/ComposerEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2520221
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 2520222
    iget-object v1, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v1}, Lcom/facebook/composer/ui/text/ComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1, v0, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 2520223
    :cond_0
    return-void
.end method

.method public setTagTypeaheadDataSourceMetadata(Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;)V
    .locals 1

    .prologue
    .line 2520224
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    .line 2520225
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setTagTypeaheadDataSourceMetadata(Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;)V

    .line 2520226
    :cond_0
    return-void
.end method

.method public setTaggingGroupId(J)V
    .locals 3

    .prologue
    .line 2520227
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    if-eqz v0, :cond_0

    .line 2520228
    iget-object v0, p0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    sget-object v2, LX/7Gm;->COMPOSER:LX/7Gm;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->a(Ljava/lang/Long;LX/7Gm;)V

    .line 2520229
    :cond_0
    return-void
.end method
