.class public Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2520324
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2520323
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "modal_underwood_data_input"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Intent;)Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;
    .locals 3

    .prologue
    .line 2520319
    const-string v0, "modal_underwood_data_output"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;

    .line 2520320
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "The argument intent didn\'t have a modal underwood result.  Are you sure this is a result from the modal underwood fragment?"

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2520321
    return-object v0

    .line 2520322
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2520305
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2520306
    const v0, 0x7f030b2a

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;->setContentView(I)V

    .line 2520307
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    sget-object v1, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;->p:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    .line 2520308
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;->p:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    if-nez v0, :cond_1

    .line 2520309
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2520310
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2520311
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2520312
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2520313
    :cond_0
    new-instance v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    invoke-direct {v0}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;->p:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    .line 2520314
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;->p:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2520315
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1c24

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;->p:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    sget-object v3, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2520316
    :cond_1
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2520317
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;->p:Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->b()V

    .line 2520318
    return-void
.end method
