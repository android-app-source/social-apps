.class public Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/ATP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/HwF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7kt;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HwW;",
            ">;"
        }
    .end annotation
.end field

.field public f:Landroid/view/inputmethod/InputMethodManager;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/HwU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/HwE;

.field public i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

.field private j:Landroid/view/View;

.field public k:Lcom/facebook/widget/ScrollingAwareScrollView;

.field public l:LX/ATO;

.field public final m:LX/HrF;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2520576
    const-class v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2520570
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2520571
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2520572
    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->d:LX/0Ot;

    .line 2520573
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2520574
    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->e:LX/0Ot;

    .line 2520575
    new-instance v0, LX/HwK;

    invoke-direct {v0, p0}, LX/HwK;-><init>(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->m:LX/HrF;

    return-void
.end method

.method public static a(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;LX/0Px;)LX/7ks;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/7ks;"
        }
    .end annotation

    .prologue
    .line 2520565
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kt;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520566
    iget-object v2, v1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v1, v2

    .line 2520567
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/7kt;->a(LX/0Px;Z)LX/7ks;

    move-result-object v0

    .line 2520568
    iget-object v1, v0, LX/7ks;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Errors with attachments: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, LX/7ks;->b:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2520569
    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;LX/0Px;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 2520556
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a(Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;)LX/HwN;

    move-result-object v0

    .line 2520557
    iput-object p1, v0, LX/HwN;->a:LX/0Px;

    .line 2520558
    move-object v0, v0

    .line 2520559
    invoke-virtual {v0}, LX/HwN;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520560
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    .line 2520561
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    iget-object p1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520562
    iget-object p0, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    move-object p1, p0

    .line 2520563
    invoke-virtual {v0, p1, p3}, LX/ATO;->a(LX/0Px;Z)V

    .line 2520564
    :cond_1
    return-void
.end method

.method public static n(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;)V
    .locals 4

    .prologue
    .line 2520550
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-static {v0}, LX/HwS;->a(LX/0il;)LX/8AA;

    move-result-object v0

    .line 2520551
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520552
    iget-object v3, v2, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->o:Ljava/lang/String;

    move-object v2, v3

    .line 2520553
    invoke-static {v1, v0, v2}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2520554
    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2520555
    return-void
.end method

.method public static o(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;)V
    .locals 3

    .prologue
    .line 2520547
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2520548
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->f:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->k:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2520549
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2520432
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2520433
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v2, p0

    check-cast v2, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;

    const-class v3, LX/ATP;

    invoke-interface {v8, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/ATP;

    const-class v4, LX/HwF;

    invoke-interface {v8, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/HwF;

    const/16 v5, 0x1961

    invoke-static {v8, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x19e5

    invoke-static {v8, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v8}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v7

    check-cast v7, Landroid/view/inputmethod/InputMethodManager;

    const-class v0, LX/HwU;

    invoke-interface {v8, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/HwU;

    iput-object v3, v2, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->b:LX/ATP;

    iput-object v4, v2, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->c:LX/HwF;

    iput-object v5, v2, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->d:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->e:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->f:Landroid/view/inputmethod/InputMethodManager;

    iput-object v8, v2, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->g:LX/HwU;

    .line 2520434
    if-eqz p1, :cond_0

    .line 2520435
    const-string v0, "state_modal_underwood_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520436
    :goto_0
    new-instance v0, LX/HwG;

    invoke-direct {v0, p0}, LX/HwG;-><init>(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;)V

    .line 2520437
    new-instance v1, LX/HwE;

    invoke-direct {v1, v0}, LX/HwE;-><init>(LX/HwG;)V

    .line 2520438
    move-object v0, v1

    .line 2520439
    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    .line 2520440
    return-void

    .line 2520441
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2520442
    const-string v1, "modal_underwood_data_input"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 2520520
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    .line 2520521
    iget v1, v0, LX/ATO;->I:I

    move v0, v1

    .line 2520522
    if-eq v0, v4, :cond_0

    .line 2520523
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {v1}, LX/HwE;->getAttachments()LX/0Px;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {v2}, LX/HwE;->getAttachments()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {v1, v0}, LX/7kq;->a(LX/0Px;Lcom/facebook/composer/attachments/ComposerAttachment;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0, v3, v3}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->a$redex0(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;LX/0Px;ZZ)V

    .line 2520524
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    .line 2520525
    iget-object v1, v0, LX/ATO;->G:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->k()LX/0wd;

    .line 2520526
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520527
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    move-object v0, v1

    .line 2520528
    invoke-static {p0, v0}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->a(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;LX/0Px;)LX/7ks;

    move-result-object v0

    .line 2520529
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    .line 2520530
    iget-boolean v2, v1, LX/ATO;->Q:Z

    if-eqz v2, :cond_1

    .line 2520531
    iget-object v2, v1, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ASn;

    .line 2520532
    invoke-interface {v2}, LX/ASn;->i()V

    goto :goto_0

    .line 2520533
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2520534
    const-string v2, "modal_underwood_data_output"

    .line 2520535
    new-instance v3, LX/HwP;

    invoke-direct {v3}, LX/HwP;-><init>()V

    move-object v3, v3

    .line 2520536
    iget-object v0, v0, LX/7ks;->a:LX/0Px;

    .line 2520537
    iput-object v0, v3, LX/HwP;->a:LX/0Px;

    .line 2520538
    move-object v0, v3

    .line 2520539
    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    invoke-virtual {v3}, LX/ATO;->a()LX/0P1;

    move-result-object v3

    .line 2520540
    iput-object v3, v0, LX/HwP;->b:LX/0P1;

    .line 2520541
    move-object v0, v0

    .line 2520542
    new-instance v3, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;

    invoke-direct {v3, v0}, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;-><init>(LX/HwP;)V

    move-object v0, v3

    .line 2520543
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2520544
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v4, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2520545
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2520546
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2520490
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2520491
    packed-switch p1, :pswitch_data_0

    .line 2520492
    :goto_0
    return-void

    .line 2520493
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->g:LX/HwU;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {v0, v1}, LX/HwU;->a(LX/0il;)LX/HwT;

    move-result-object v1

    .line 2520494
    if-nez p2, :cond_1

    .line 2520495
    invoke-virtual {v1}, LX/HwT;->a()V

    .line 2520496
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    invoke-virtual {v0}, LX/ATO;->m()Z

    .line 2520497
    :cond_0
    goto :goto_0

    .line 2520498
    :pswitch_1
    const/4 v2, 0x1

    .line 2520499
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520500
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    move-object v0, v1

    .line 2520501
    invoke-static {p2, p3, v0}, LX/HwY;->a(ILandroid/content/Intent;LX/0Px;)LX/HwX;

    move-result-object v0

    .line 2520502
    if-nez v0, :cond_3

    .line 2520503
    :goto_2
    goto :goto_0

    .line 2520504
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {v0}, LX/HwE;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->l(LX/0Px;)Z

    move-result v2

    .line 2520505
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-virtual {v0}, LX/HwE;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->j(LX/0Px;)Z

    move-result v3

    .line 2520506
    invoke-static {p3}, LX/HwS;->a(Landroid/content/Intent;)LX/0Px;

    move-result-object v4

    .line 2520507
    invoke-virtual {v1, v4, v2}, LX/HwT;->a(LX/0Px;Z)V

    .line 2520508
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2520509
    if-eqz v4, :cond_2

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_2

    .line 2520510
    invoke-static {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2520511
    :cond_2
    invoke-static {p0, v0}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->a(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;LX/0Px;)LX/7ks;

    move-result-object v0

    .line 2520512
    iget-object v0, v0, LX/7ks;->a:LX/0Px;

    .line 2520513
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2520514
    iget-object v4, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520515
    iget-object p1, v4, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    move-object v4, p1

    .line 2520516
    invoke-static {v0, v4}, LX/7kq;->a(LX/0Px;LX/0Px;)LX/0Px;

    move-result-object v0

    const/4 v4, 0x1

    const/4 p1, 0x0

    invoke-static {p0, v0, v4, p1}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->a$redex0(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;LX/0Px;ZZ)V

    .line 2520517
    invoke-virtual {v1, v2, v3}, LX/HwT;->a(ZZ)V

    goto :goto_1

    .line 2520518
    :cond_3
    iget-object v1, v0, LX/HwX;->a:LX/0Px;

    invoke-static {p0, v1, v2, v2}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->a$redex0(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;LX/0Px;ZZ)V

    .line 2520519
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    iget-object v0, v0, LX/HwX;->b:LX/0Px;

    sget-object v2, LX/03R;->NO:LX/03R;

    invoke-virtual {v1, v0, v2}, LX/ATO;->a(LX/0Px;LX/03R;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5fa9c46f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2520488
    const v1, 0x7f030b2b

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->j:Landroid/view/View;

    .line 2520489
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->j:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x7daca62c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3614d85f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2520485
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2520486
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    invoke-virtual {v1}, LX/ATO;->e()V

    .line 2520487
    const/16 v1, 0x2b

    const v2, -0x7823bc88

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x66929342

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2520478
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2520479
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->o(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;)V

    .line 2520480
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    invoke-static {v1}, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a(Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;)LX/HwN;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    invoke-virtual {v2}, LX/ATO;->a()LX/0P1;

    move-result-object v2

    .line 2520481
    iput-object v2, v1, LX/HwN;->c:LX/0P1;

    .line 2520482
    move-object v1, v1

    .line 2520483
    invoke-virtual {v1}, LX/HwN;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520484
    const/16 v1, 0x2b

    const v2, -0x7da0ee59

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2520476
    const-string v0, "state_modal_underwood_data"

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2520477
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2520443
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2520444
    const/4 p2, 0x1

    .line 2520445
    const v0, 0x7f0d1c25

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2520446
    const v1, 0x7f0814ae

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2520447
    invoke-virtual {v0, p2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setHasBackButton(Z)V

    .line 2520448
    new-instance v1, LX/HwH;

    invoke-direct {v1, p0}, LX/HwH;-><init>(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2520449
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const p1, 0x7f0813f7

    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2520450
    iput-object p1, v1, LX/108;->g:Ljava/lang/String;

    .line 2520451
    move-object v1, v1

    .line 2520452
    iput-boolean p2, v1, LX/108;->q:Z

    .line 2520453
    move-object v1, v1

    .line 2520454
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2520455
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 2520456
    new-instance v1, LX/HwI;

    invoke-direct {v1, p0}, LX/HwI;-><init>(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setOnToolbarButtonListener(LX/63W;)V

    .line 2520457
    const v0, 0x7f0d1c26

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ScrollingAwareScrollView;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->k:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 2520458
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->k:Lcom/facebook/widget/ScrollingAwareScrollView;

    new-instance v1, LX/HwQ;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->m:LX/HrF;

    const/4 p1, 0x0

    iget-object p2, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    invoke-direct {v1, v2, p1, p2}, LX/HwQ;-><init>(LX/HrF;ZLX/0il;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2520459
    const/4 v5, 0x0

    .line 2520460
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->b:LX/ATP;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->k:Lcom/facebook/widget/ScrollingAwareScrollView;

    const v4, 0x7f0d1c27

    invoke-virtual {p0, v4}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->h:LX/HwE;

    const/4 v8, 0x1

    move v6, v5

    invoke-virtual/range {v1 .. v8}, LX/ATP;->a(LX/0gc;Lcom/facebook/widget/ScrollingAwareScrollView;Landroid/widget/LinearLayout;ZZLjava/lang/Object;Z)LX/ATO;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    .line 2520461
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520462
    iget-boolean v3, v2, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->d:Z

    move v2, v3

    .line 2520463
    invoke-virtual {v1, v2}, LX/ATO;->c(Z)V

    .line 2520464
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520465
    iget-object v2, v1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->c:LX/0P1;

    move-object v1, v2

    .line 2520466
    invoke-virtual {v1}, LX/0P1;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2520467
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    invoke-virtual {v2, v1}, LX/ATO;->a(LX/0P1;)V

    .line 2520468
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    .line 2520469
    new-instance v1, LX/HwJ;

    invoke-direct {v1, p0}, LX/HwJ;-><init>(Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;)V

    move-object v1, v1

    .line 2520470
    iput-object v1, v0, LX/ATO;->B:LX/ASe;

    .line 2520471
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->i:Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520472
    iget-object v2, v1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    move-object v1, v2

    .line 2520473
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/ATO;->a(LX/0Px;Z)V

    .line 2520474
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodFragment;->l:LX/ATO;

    invoke-virtual {v0}, LX/ATO;->m()Z

    .line 2520475
    return-void
.end method
