.class public Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public final j:Z

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:Ljava/lang/String;

.field public final p:Z

.field public final q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2520810
    new-instance v0, LX/HwM;

    invoke-direct {v0}, LX/HwM;-><init>()V

    sput-object v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/HwN;)V
    .locals 1

    .prologue
    .line 2520790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520791
    iget-object v0, p1, LX/HwN;->a:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    .line 2520792
    iget-object v0, p1, LX/HwN;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2520793
    iget-object v0, p1, LX/HwN;->c:LX/0P1;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->c:LX/0P1;

    .line 2520794
    iget-boolean v0, p1, LX/HwN;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->d:Z

    .line 2520795
    iget-boolean v0, p1, LX/HwN;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->e:Z

    .line 2520796
    iget-boolean v0, p1, LX/HwN;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->f:Z

    .line 2520797
    iget-boolean v0, p1, LX/HwN;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->g:Z

    .line 2520798
    iget-boolean v0, p1, LX/HwN;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->h:Z

    .line 2520799
    iget-boolean v0, p1, LX/HwN;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->i:Z

    .line 2520800
    iget-boolean v0, p1, LX/HwN;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->j:Z

    .line 2520801
    iget-boolean v0, p1, LX/HwN;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->k:Z

    .line 2520802
    iget-boolean v0, p1, LX/HwN;->l:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->l:Z

    .line 2520803
    iget-boolean v0, p1, LX/HwN;->m:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->m:Z

    .line 2520804
    iget-object v0, p1, LX/HwN;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2520805
    iget-object v0, p1, LX/HwN;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->o:Ljava/lang/String;

    .line 2520806
    iget-boolean v0, p1, LX/HwN;->p:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->p:Z

    .line 2520807
    iget-object v0, p1, LX/HwN;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2520808
    iget-object v0, p1, LX/HwN;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2520809
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 2520739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520740
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [Lcom/facebook/composer/attachments/ComposerAttachment;

    move v1, v2

    .line 2520741
    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_0

    .line 2520742
    sget-object v0, Lcom/facebook/composer/attachments/ComposerAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2520743
    aput-object v0, v4, v1

    .line 2520744
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2520745
    :cond_0
    invoke-static {v4}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    .line 2520746
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    .line 2520747
    iput-object v7, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2520748
    :goto_1
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2520749
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    move v1, v2

    .line 2520750
    :goto_2
    if-ge v1, v5, :cond_2

    .line 2520751
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 2520752
    sget-object v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    .line 2520753
    invoke-interface {v4, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2520754
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2520755
    :cond_1
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    goto :goto_1

    .line 2520756
    :cond_2
    invoke-static {v4}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->c:LX/0P1;

    .line 2520757
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_3

    move v0, v3

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->d:Z

    .line 2520758
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_4

    move v0, v3

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->e:Z

    .line 2520759
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_5

    move v0, v3

    :goto_5
    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->f:Z

    .line 2520760
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_6

    move v0, v3

    :goto_6
    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->g:Z

    .line 2520761
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_7

    move v0, v3

    :goto_7
    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->h:Z

    .line 2520762
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_8

    move v0, v3

    :goto_8
    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->i:Z

    .line 2520763
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_9

    move v0, v3

    :goto_9
    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->j:Z

    .line 2520764
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_a

    move v0, v3

    :goto_a
    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->k:Z

    .line 2520765
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_b

    move v0, v3

    :goto_b
    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->l:Z

    .line 2520766
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_c

    move v0, v3

    :goto_c
    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->m:Z

    .line 2520767
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_d

    .line 2520768
    iput-object v7, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2520769
    :goto_d
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->o:Ljava/lang/String;

    .line 2520770
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v3, :cond_e

    :goto_e
    iput-boolean v3, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->p:Z

    .line 2520771
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_f

    .line 2520772
    iput-object v7, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2520773
    :goto_f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_10

    .line 2520774
    iput-object v7, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2520775
    :goto_10
    return-void

    :cond_3
    move v0, v2

    .line 2520776
    goto :goto_3

    :cond_4
    move v0, v2

    .line 2520777
    goto :goto_4

    :cond_5
    move v0, v2

    .line 2520778
    goto :goto_5

    :cond_6
    move v0, v2

    .line 2520779
    goto :goto_6

    :cond_7
    move v0, v2

    .line 2520780
    goto :goto_7

    :cond_8
    move v0, v2

    .line 2520781
    goto :goto_8

    :cond_9
    move v0, v2

    .line 2520782
    goto :goto_9

    :cond_a
    move v0, v2

    .line 2520783
    goto :goto_a

    :cond_b
    move v0, v2

    .line 2520784
    goto :goto_b

    :cond_c
    move v0, v2

    .line 2520785
    goto :goto_c

    .line 2520786
    :cond_d
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerPageData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    goto :goto_d

    :cond_e
    move v3, v2

    .line 2520787
    goto :goto_e

    .line 2520788
    :cond_f
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    goto :goto_f

    .line 2520789
    :cond_10
    const-class v0, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    goto :goto_10
.end method

.method public static a(Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;)LX/HwN;
    .locals 2

    .prologue
    .line 2520738
    new-instance v0, LX/HwN;

    invoke-direct {v0, p0}, LX/HwN;-><init>(Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2520737
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2520696
    if-ne p0, p1, :cond_1

    .line 2520697
    :cond_0
    :goto_0
    return v0

    .line 2520698
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    if-nez v2, :cond_2

    move v0, v1

    .line 2520699
    goto :goto_0

    .line 2520700
    :cond_2
    check-cast p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    .line 2520701
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    iget-object v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2520702
    goto :goto_0

    .line 2520703
    :cond_3
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iget-object v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2520704
    goto :goto_0

    .line 2520705
    :cond_4
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->c:LX/0P1;

    iget-object v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->c:LX/0P1;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 2520706
    goto :goto_0

    .line 2520707
    :cond_5
    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->d:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 2520708
    goto :goto_0

    .line 2520709
    :cond_6
    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->e:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2520710
    goto :goto_0

    .line 2520711
    :cond_7
    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->f:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->f:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2520712
    goto :goto_0

    .line 2520713
    :cond_8
    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->g:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->g:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 2520714
    goto :goto_0

    .line 2520715
    :cond_9
    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->h:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->h:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2520716
    goto :goto_0

    .line 2520717
    :cond_a
    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->i:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->i:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 2520718
    goto :goto_0

    .line 2520719
    :cond_b
    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->j:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->j:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 2520720
    goto :goto_0

    .line 2520721
    :cond_c
    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->k:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->k:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 2520722
    goto :goto_0

    .line 2520723
    :cond_d
    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->l:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->l:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 2520724
    goto :goto_0

    .line 2520725
    :cond_e
    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->m:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->m:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 2520726
    goto :goto_0

    .line 2520727
    :cond_f
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iget-object v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 2520728
    goto/16 :goto_0

    .line 2520729
    :cond_10
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->o:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 2520730
    goto/16 :goto_0

    .line 2520731
    :cond_11
    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->p:Z

    iget-boolean v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->p:Z

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 2520732
    goto/16 :goto_0

    .line 2520733
    :cond_12
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 2520734
    goto/16 :goto_0

    .line 2520735
    :cond_13
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    iget-object v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2520736
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2520647
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->c:LX/0P1;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->f:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->h:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->i:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->j:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->m:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->o:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->p:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2520648
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520649
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v1, v3

    :goto_0
    if-ge v1, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2520650
    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/attachments/ComposerAttachment;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2520651
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2520652
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-nez v0, :cond_1

    .line 2520653
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520654
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520655
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 2520656
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2520657
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_2

    .line 2520658
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520659
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1

    .line 2520660
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->d:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520661
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->e:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520662
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->f:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520663
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->g:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520664
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->h:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520665
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->i:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520666
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->j:Z

    if-eqz v0, :cond_9

    move v0, v2

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520667
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->k:Z

    if-eqz v0, :cond_a

    move v0, v2

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520668
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->l:Z

    if-eqz v0, :cond_b

    move v0, v2

    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520669
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->m:Z

    if-eqz v0, :cond_c

    move v0, v2

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520670
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-nez v0, :cond_d

    .line 2520671
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520672
    :goto_d
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2520673
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->p:Z

    if-eqz v0, :cond_e

    move v0, v2

    :goto_e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520674
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-nez v0, :cond_f

    .line 2520675
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520676
    :goto_f
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    if-nez v0, :cond_10

    .line 2520677
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520678
    :goto_10
    return-void

    :cond_3
    move v0, v3

    .line 2520679
    goto :goto_3

    :cond_4
    move v0, v3

    .line 2520680
    goto :goto_4

    :cond_5
    move v0, v3

    .line 2520681
    goto :goto_5

    :cond_6
    move v0, v3

    .line 2520682
    goto :goto_6

    :cond_7
    move v0, v3

    .line 2520683
    goto :goto_7

    :cond_8
    move v0, v3

    .line 2520684
    goto :goto_8

    :cond_9
    move v0, v3

    .line 2520685
    goto :goto_9

    :cond_a
    move v0, v3

    .line 2520686
    goto :goto_a

    :cond_b
    move v0, v3

    .line 2520687
    goto :goto_b

    :cond_c
    move v0, v3

    .line 2520688
    goto :goto_c

    .line 2520689
    :cond_d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520690
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_d

    :cond_e
    move v0, v3

    .line 2520691
    goto :goto_e

    .line 2520692
    :cond_f
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520693
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_f

    .line 2520694
    :cond_10
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520695
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->r:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_10
.end method
