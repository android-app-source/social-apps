.class public Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2520859
    new-instance v0, LX/HwO;

    invoke-direct {v0}, LX/HwO;-><init>()V

    sput-object v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/HwP;)V
    .locals 1

    .prologue
    .line 2520855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520856
    iget-object v0, p1, LX/HwP;->a:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->a:LX/0Px;

    .line 2520857
    iget-object v0, p1, LX/HwP;->b:LX/0P1;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->b:LX/0P1;

    .line 2520858
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2520839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520840
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v3, v0, [Lcom/facebook/composer/attachments/ComposerAttachment;

    move v1, v2

    .line 2520841
    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    .line 2520842
    sget-object v0, Lcom/facebook/composer/attachments/ComposerAttachment;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2520843
    aput-object v0, v3, v1

    .line 2520844
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2520845
    :cond_0
    invoke-static {v3}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->a:LX/0Px;

    .line 2520846
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2520847
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 2520848
    :goto_1
    if-ge v2, v3, :cond_1

    .line 2520849
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 2520850
    sget-object v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    .line 2520851
    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2520852
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2520853
    :cond_1
    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->b:LX/0P1;

    .line 2520854
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2520860
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2520830
    if-ne p0, p1, :cond_1

    .line 2520831
    :cond_0
    :goto_0
    return v0

    .line 2520832
    :cond_1
    instance-of v2, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;

    if-nez v2, :cond_2

    move v0, v1

    .line 2520833
    goto :goto_0

    .line 2520834
    :cond_2
    check-cast p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;

    .line 2520835
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->a:LX/0Px;

    iget-object v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->a:LX/0Px;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2520836
    goto :goto_0

    .line 2520837
    :cond_3
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->b:LX/0P1;

    iget-object v3, p1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->b:LX/0P1;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2520838
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2520829
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->a:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->b:LX/0P1;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 2520820
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520821
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2520822
    invoke-virtual {v0, p1, p2}, Lcom/facebook/composer/attachments/ComposerAttachment;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2520823
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2520824
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2520825
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->b:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 2520826
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2520827
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_1

    .line 2520828
    :cond_1
    return-void
.end method
