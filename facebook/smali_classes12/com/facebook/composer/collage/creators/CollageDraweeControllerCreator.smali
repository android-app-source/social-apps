.class public Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/util/List",
            "<",
            "LX/HsC;",
            ">;",
            "LX/0Px",
            "<",
            "LX/HsC;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:LX/1VL;

.field public final e:LX/1Ad;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8GV;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/75F;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/74n;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8GX;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2514071
    const-class v0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;

    const-string v1, "composer"

    const-string v2, "composer"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2514072
    new-instance v0, LX/Hs9;

    invoke-direct {v0}, LX/Hs9;-><init>()V

    sput-object v0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->b:LX/0QK;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1VL;LX/1Ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0il;)V
    .locals 2
    .param p8    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1VL;",
            "LX/1Ad;",
            "LX/0Ot",
            "<",
            "LX/8GV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/75F;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/74n;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8GX;",
            ">;TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2514073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2514074
    iput-object p1, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->c:Landroid/content/Context;

    .line 2514075
    iput-object p2, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->d:LX/1VL;

    .line 2514076
    iput-object p3, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->e:LX/1Ad;

    .line 2514077
    iput-object p4, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->f:LX/0Ot;

    .line 2514078
    iput-object p5, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->g:LX/0Ot;

    .line 2514079
    iput-object p6, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->h:LX/0Ot;

    .line 2514080
    iput-object p7, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->i:LX/0Ot;

    .line 2514081
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->j:Ljava/lang/ref/WeakReference;

    .line 2514082
    return-void
.end method

.method private static a(Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;III)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/1bf;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2514083
    iget-object v0, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2514084
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2514085
    iget-object v0, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/74n;

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/74j;->DEFAULT:LX/74j;

    invoke-virtual {v0, v1, v2}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 2514086
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/75F;

    new-instance v2, Lcom/facebook/ipc/media/MediaIdKey;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v3

    .line 2514087
    iget-object v6, v1, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v1, v6

    .line 2514088
    iget-wide v8, v1, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v6, v8

    .line 2514089
    invoke-direct {v2, v3, v6, v7}, Lcom/facebook/ipc/media/MediaIdKey;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v2}, LX/75F;->a(Lcom/facebook/ipc/media/MediaIdKey;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 2514090
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    invoke-static {v0}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GX;

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/8GX;->a(Landroid/net/Uri;)I

    move-result v0

    invoke-static {v1, v2, v0}, LX/9ic;->a(Ljava/util/List;Landroid/graphics/RectF;I)Ljava/util/List;

    move-result-object v0

    move-object v3, v0

    .line 2514091
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GV;

    .line 2514092
    iget-object v1, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->c:Landroid/content/Context;

    invoke-static {v1}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v1

    .line 2514093
    const/16 v2, 0x7de

    if-lt v1, v2, :cond_4

    .line 2514094
    const/4 v1, 0x0

    .line 2514095
    :goto_2
    const/high16 v2, 0x3f800000    # 1.0f

    int-to-float v1, v1

    const v6, 0x3e4ccccd    # 0.2f

    mul-float/2addr v1, v6

    sub-float v1, v2, v1

    move v1, v1

    .line 2514096
    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v2

    if-eqz v3, :cond_2

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    :goto_3
    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    :goto_4
    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/8GV;->a(FLcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Landroid/net/Uri;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/HsB;

    invoke-direct {v1, p0, p2, p3}, LX/HsB;-><init>(Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;II)V

    .line 2514097
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2514098
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v1, v5

    .line 2514099
    goto :goto_0

    :cond_1
    move-object v3, v5

    .line 2514100
    goto :goto_1

    :cond_2
    move-object v3, v5

    .line 2514101
    goto :goto_3

    :cond_3
    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v4

    goto :goto_4

    .line 2514102
    :cond_4
    const/16 v2, 0x7dd

    if-ne v1, v2, :cond_5

    .line 2514103
    const/4 v1, 0x1

    goto :goto_2

    .line 2514104
    :cond_5
    const/16 v2, 0x7dc

    if-ne v1, v2, :cond_6

    .line 2514105
    const/4 v1, 0x2

    goto :goto_2

    .line 2514106
    :cond_6
    const/16 v2, 0x7db

    if-ne v1, v2, :cond_7

    .line 2514107
    const/4 v1, 0x3

    goto :goto_2

    .line 2514108
    :cond_7
    const/4 v1, 0x4

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/26O;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "LX/26M;",
            ">(",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "LX/26O",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "LX/HsC;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2514109
    iget-object v0, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2514110
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    .line 2514111
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    .line 2514112
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2514113
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 2514114
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    invoke-static {v0}, LX/5iB;->e(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, LX/5iL;->AE08bit:LX/5iL;

    invoke-virtual {v4}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    invoke-static {v0}, LX/5iB;->f(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-static {v0}, LX/5iB;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2514115
    :cond_1
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2514116
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2514117
    :cond_3
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    move-object v3, v0

    .line 2514118
    invoke-virtual {p2}, LX/26O;->a()LX/0Px;

    move-result-object v4

    .line 2514119
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 2514120
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 2514121
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26M;

    .line 2514122
    iget-object v2, v0, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v2

    .line 2514123
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 2514124
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2514125
    iget-object v2, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->d:LX/1VL;

    invoke-virtual {v2, p1, v0}, LX/1VL;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v2

    .line 2514126
    int-to-double v6, v2

    invoke-static {v0}, LX/1VL;->e(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v8

    int-to-double v8, v8

    invoke-static {v0}, LX/1VL;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v10

    int-to-double v10, v10

    div-double/2addr v8, v10

    mul-double/2addr v6, v8

    double-to-int v6, v6

    .line 2514127
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 2514128
    invoke-virtual {v3, v7}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2514129
    invoke-virtual {v3, v7}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p0, v0, v2, v6}, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->a(Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;III)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v2, v0

    .line 2514130
    :goto_2
    iget-object v0, p0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2514131
    new-instance v6, LX/HsA;

    invoke-direct {v6, p0, v3, v7, v0}, LX/HsA;-><init>(Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;LX/0P1;Landroid/net/Uri;LX/0il;)V

    .line 2514132
    sget-object v0, LX/131;->INSTANCE:LX/131;

    move-object v0, v0

    .line 2514133
    invoke-static {v2, v6, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2514134
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2514135
    :cond_4
    invoke-static {v7}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v8, LX/1o9;

    invoke-direct {v8, v2, v6}, LX/1o9;-><init>(II)V

    .line 2514136
    iput-object v8, v0, LX/1bX;->c:LX/1o9;

    .line 2514137
    move-object v0, v0

    .line 2514138
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v2, v0

    goto :goto_2

    .line 2514139
    :cond_5
    invoke-static {v5}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    sget-object v1, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->b:LX/0QK;

    .line 2514140
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2514141
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
