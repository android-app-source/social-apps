.class public Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:LX/Aj7;

.field private b:Lcom/facebook/feed/collage/ui/CollageAttachmentView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/fbui/widget/text/GlyphWithTextView;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/HsF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2514349
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2514350
    new-instance v0, LX/HsN;

    invoke-direct {v0, p0}, LX/HsN;-><init>(Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->a:LX/Aj7;

    .line 2514351
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->e:I

    .line 2514352
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->a(Landroid/util/AttributeSet;)V

    .line 2514353
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2514354
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2514355
    new-instance v0, LX/HsN;

    invoke-direct {v0, p0}, LX/HsN;-><init>(Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->a:LX/Aj7;

    .line 2514356
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->e:I

    .line 2514357
    invoke-direct {p0, p2}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->a(Landroid/util/AttributeSet;)V

    .line 2514358
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, -0x1

    .line 2514359
    new-instance v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {p0}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->b:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 2514360
    iget-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->b:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iget-object v1, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->a:LX/Aj7;

    .line 2514361
    iput-object v1, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 2514362
    iget-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->b:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->b()V

    .line 2514363
    iget-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->b:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2514364
    invoke-virtual {p0}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030310

    invoke-static {v0, v1, p0}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2514365
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0a6c

    invoke-virtual {p0, v0}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->c:LX/0zw;

    .line 2514366
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2514367
    iget-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->b:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a()V

    .line 2514368
    return-void
.end method

.method public final a(LX/26O;[LX/1aZ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/26O",
            "<",
            "LX/26M;",
            ">;[",
            "LX/1aZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2514369
    iget-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->b:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->a(LX/26O;[LX/1aZ;)V

    .line 2514370
    return-void
.end method

.method public setInvisiblePhotoCount(I)V
    .locals 1

    .prologue
    .line 2514371
    iget-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->b:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 2514372
    iput p1, v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->l:I

    .line 2514373
    return-void
.end method

.method public setOnCollageTappedListener(LX/HsF;)V
    .locals 0
    .param p1    # LX/HsF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2514374
    iput-object p1, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->d:LX/HsF;

    .line 2514375
    return-void
.end method

.method public setTaggedUserCount(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2514376
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Please set a valid non-zero integer for number of tagged users"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2514377
    iget v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->e:I

    if-ne v0, p1, :cond_1

    .line 2514378
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 2514379
    goto :goto_0

    .line 2514380
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2514381
    iget-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    if-nez p1, :cond_3

    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2514382
    iget-object v0, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->c:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    if-nez p1, :cond_2

    const/16 v2, 0x8

    :cond_2
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2514383
    iput p1, p0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->e:I

    goto :goto_1

    .line 2514384
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0181

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, p1, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method
