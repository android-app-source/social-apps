.class public Lcom/facebook/composer/system/model/ComposerModelImplSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/system/model/ComposerModelImpl;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2518676
    const-class v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    new-instance v1, Lcom/facebook/composer/system/model/ComposerModelImplSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/system/model/ComposerModelImplSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2518677
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2518678
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2518679
    if-nez p0, :cond_0

    .line 2518680
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2518681
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2518682
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/system/model/ComposerModelImplSerializer;->b(Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0nX;LX/0my;)V

    .line 2518683
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2518684
    return-void
.end method

.method private static b(Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2518685
    const-string v0, "app_attribution"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518686
    const-string v0, "attachments"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2518687
    const-string v0, "composer_session_logging_data"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518688
    const-string v0, "configuration"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518689
    const-string v0, "date_info"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518690
    const-string v0, "has_privacy_changed"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->hasPrivacyChanged()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2518691
    const-string v0, "is_backout_draft"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->isBackoutDraft()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2518692
    const-string v0, "is_feed_only_post"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->isFeedOnlyPost()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2518693
    const-string v0, "is_target_album_new"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->isTargetAlbumNew()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2518694
    const-string v0, "last_xy_tag_change_time"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getLastXyTagChangeTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2518695
    const-string v0, "location_info"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518696
    const-string v0, "marketplace_id"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getMarketplaceId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2518697
    const-string v0, "minutiae_object"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518698
    const-string v0, "page_data"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518699
    const-string v0, "privacy_override"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPrivacyOverride()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518700
    const-string v0, "product_item_attachment"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518701
    const-string v0, "prompt_analytics"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPromptAnalytics()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518702
    const-string v0, "publish_mode"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPublishMode()LX/5Rn;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518703
    const-string v0, "publish_schedule_time"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2518704
    const-string v0, "rating"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRating()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2518705
    const-string v0, "referenced_sticker_data"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518706
    const-string v0, "removed_urls"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRemovedUrls()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2518707
    const-string v0, "rich_text_style"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518708
    const-string v0, "saved_session_load_attempts"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSavedSessionLoadAttempts()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2518709
    const-string v0, "session_id"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2518710
    const-string v0, "share_params"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518711
    const-string v0, "slideshow_data"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518712
    const-string v0, "storyline_data"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518713
    const-string v0, "tagged_users"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getTaggedUsers()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2518714
    const-string v0, "target_album"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518715
    const-string v0, "target_data"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518716
    const-string v0, "text_with_entities"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518717
    const-string v0, "user_selected_tags"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->isUserSelectedTags()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2518718
    const-string v0, "viewer_coordinates"

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getViewerCoordinates()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518719
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2518720
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/system/model/ComposerModelImplSerializer;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;LX/0nX;LX/0my;)V

    return-void
.end method
