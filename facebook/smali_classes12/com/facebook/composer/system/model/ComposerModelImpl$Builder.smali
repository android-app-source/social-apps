.class public final Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/system/model/ComposerModelImpl_BuilderDeserializer;
.end annotation


# static fields
.field private static final a:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

.field private static final b:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

.field private static final c:Lcom/facebook/friendsharing/inspiration/model/CameraState;

.field private static final d:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

.field private static final e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field private static final f:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

.field private static final g:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

.field private static final h:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

.field private static final i:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

.field private static final j:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

.field private static final k:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

.field private static final l:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

.field private static final m:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

.field private static final n:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field private static final o:LX/5Rn;

.field private static final p:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field private static final q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;


# instance fields
.field public A:Lcom/facebook/ipc/composer/model/ComposerDateInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Z

.field public C:Z

.field public D:Z

.field public E:Z

.field public F:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

.field public G:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

.field public I:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

.field public J:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

.field public K:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

.field public L:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public M:Z

.field public N:Z

.field public O:Z

.field public P:Z

.field public Q:Z

.field public R:Z

.field public S:J

.field public T:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public U:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

.field public V:J

.field public W:I

.field public X:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public aa:Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ad:LX/5Rn;

.field public ae:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public af:I

.field public ag:Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ah:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public ai:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aj:I

.field public ak:Ljava/lang/String;

.field public al:J

.field public am:Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public an:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ao:Lcom/facebook/ipc/composer/model/ComposerStorylineData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ap:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation
.end field

.field public aq:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ar:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public at:Z

.field public au:Lcom/facebook/ipc/composer/model/ComposerLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/share/model/ComposerAppAttribution;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

.field public u:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

.field public v:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/friendsharing/inspiration/model/CameraState;

.field public x:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

.field public y:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field public z:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2518422
    const-class v0, Lcom/facebook/composer/system/model/ComposerModelImpl_BuilderDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2518423
    new-instance v0, LX/AP4;

    invoke-direct {v0}, LX/AP4;-><init>()V

    .line 2518424
    new-instance v0, LX/AP3;

    invoke-direct {v0}, LX/AP3;-><init>()V

    invoke-virtual {v0}, LX/AP3;->a()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v0

    move-object v0, v0

    .line 2518425
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 2518426
    new-instance v0, LX/ASd;

    invoke-direct {v0}, LX/ASd;-><init>()V

    .line 2518427
    invoke-static {}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->newBuilder()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->a()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v0

    move-object v0, v0

    .line 2518428
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->b:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    .line 2518429
    new-instance v0, LX/86s;

    invoke-direct {v0}, LX/86s;-><init>()V

    .line 2518430
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    move-object v0, v0

    .line 2518431
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->c:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    .line 2518432
    new-instance v0, LX/5MD;

    invoke-direct {v0}, LX/5MD;-><init>()V

    .line 2518433
    invoke-static {}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->newBuilder()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->a()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v0

    move-object v0, v0

    .line 2518434
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->d:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 2518435
    new-instance v0, LX/89B;

    invoke-direct {v0}, LX/89B;-><init>()V

    .line 2518436
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    move-object v0, v0

    .line 2518437
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2518438
    new-instance v0, LX/AQD;

    invoke-direct {v0}, LX/AQD;-><init>()V

    .line 2518439
    invoke-static {}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->newBuilder()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->a()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    move-object v0, v0

    .line 2518440
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->f:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    .line 2518441
    new-instance v0, LX/86z;

    invoke-direct {v0}, LX/86z;-><init>()V

    .line 2518442
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v0

    move-object v0, v0

    .line 2518443
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->g:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    .line 2518444
    new-instance v0, LX/873;

    invoke-direct {v0}, LX/873;-><init>()V

    .line 2518445
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    move-object v0, v0

    .line 2518446
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->h:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    .line 2518447
    new-instance v0, LX/87A;

    invoke-direct {v0}, LX/87A;-><init>()V

    .line 2518448
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v0

    move-object v0, v0

    .line 2518449
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->i:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    .line 2518450
    new-instance v0, LX/87H;

    invoke-direct {v0}, LX/87H;-><init>()V

    .line 2518451
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    move-object v0, v0

    .line 2518452
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->j:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    .line 2518453
    new-instance v0, LX/87J;

    invoke-direct {v0}, LX/87J;-><init>()V

    .line 2518454
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    move-object v0, v0

    .line 2518455
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->k:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    .line 2518456
    new-instance v0, LX/87M;

    invoke-direct {v0}, LX/87M;-><init>()V

    .line 2518457
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    move-object v0, v0

    .line 2518458
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    .line 2518459
    new-instance v0, LX/5RP;

    invoke-direct {v0}, LX/5RP;-><init>()V

    .line 2518460
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v0

    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    move-object v0, v0

    .line 2518461
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->m:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 2518462
    new-instance v0, LX/7lQ;

    invoke-direct {v0}, LX/7lQ;-><init>()V

    .line 2518463
    new-instance v0, LX/7lP;

    invoke-direct {v0}, LX/7lP;-><init>()V

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    move-object v0, v0

    .line 2518464
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->n:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 2518465
    new-instance v0, LX/2rv;

    invoke-direct {v0}, LX/2rv;-><init>()V

    .line 2518466
    sget-object v0, LX/5Rn;->NORMAL:LX/5Rn;

    move-object v0, v0

    .line 2518467
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->o:LX/5Rn;

    .line 2518468
    new-instance v0, LX/89J;

    invoke-direct {v0}, LX/89J;-><init>()V

    .line 2518469
    sget-object v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v0, v0

    .line 2518470
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->p:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2518471
    new-instance v0, LX/2rj;

    invoke-direct {v0}, LX/2rj;-><init>()V

    .line 2518472
    sget-object v0, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-object v0, v0

    .line 2518473
    sput-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2518474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518475
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2518476
    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->s:LX/0Px;

    .line 2518477
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->t:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 2518478
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->b:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->u:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    .line 2518479
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->c:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->w:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    .line 2518480
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->d:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->x:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 2518481
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->y:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2518482
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2518483
    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->z:LX/0P1;

    .line 2518484
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->f:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->F:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    .line 2518485
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->g:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->G:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    .line 2518486
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->h:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->H:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    .line 2518487
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->i:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->I:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    .line 2518488
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->j:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->J:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    .line 2518489
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->k:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->K:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    .line 2518490
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->l:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->L:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    .line 2518491
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->m:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->U:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 2518492
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->n:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Z:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 2518493
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->o:LX/5Rn;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ad:LX/5Rn;

    .line 2518494
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2518495
    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ah:LX/0Px;

    .line 2518496
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ak:Ljava/lang/String;

    .line 2518497
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2518498
    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ap:LX/0Px;

    .line 2518499
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->p:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ar:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2518500
    sget-object v0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->q:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2518501
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/system/model/ComposerModelImpl;)V
    .locals 2

    .prologue
    .line 2518502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518503
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518504
    instance-of v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    if-eqz v0, :cond_0

    .line 2518505
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 2518506
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->r:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 2518507
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->s:LX/0Px;

    .line 2518508
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->c:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->t:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 2518509
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->d:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->u:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    .line 2518510
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->e:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->v:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 2518511
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->f:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->w:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    .line 2518512
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->g:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->x:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 2518513
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->h:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->y:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2518514
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->i:LX/0P1;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->z:LX/0P1;

    .line 2518515
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->j:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->A:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 2518516
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->k:Z

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->B:Z

    .line 2518517
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->l:Z

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->C:Z

    .line 2518518
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->m:Z

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->D:Z

    .line 2518519
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->n:Z

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->E:Z

    .line 2518520
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->o:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->F:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    .line 2518521
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->p:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->G:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    .line 2518522
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->q:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->H:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    .line 2518523
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->r:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->I:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    .line 2518524
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->s:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->J:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    .line 2518525
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->t:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->K:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    .line 2518526
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->u:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->L:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    .line 2518527
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->v:Z

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->M:Z

    .line 2518528
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->w:Z

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->N:Z

    .line 2518529
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->x:Z

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->O:Z

    .line 2518530
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->y:Z

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->P:Z

    .line 2518531
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->z:Z

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Q:Z

    .line 2518532
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->A:Z

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->R:Z

    .line 2518533
    iget-wide v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->B:J

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->S:J

    .line 2518534
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->C:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->T:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2518535
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->D:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->U:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 2518536
    iget-wide v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->E:J

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->V:J

    .line 2518537
    iget v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->F:I

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->W:I

    .line 2518538
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->G:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->X:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 2518539
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->H:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Y:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2518540
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->I:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Z:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 2518541
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->J:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2518542
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->K:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ab:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 2518543
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->L:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ac:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 2518544
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->M:LX/5Rn;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ad:LX/5Rn;

    .line 2518545
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->N:Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ae:Ljava/lang/Long;

    .line 2518546
    iget v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->O:I

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->af:I

    .line 2518547
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->P:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ag:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 2518548
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->Q:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ah:LX/0Px;

    .line 2518549
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->R:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ai:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 2518550
    iget v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->S:I

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aj:I

    .line 2518551
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->T:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ak:Ljava/lang/String;

    .line 2518552
    iget-wide v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->U:J

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->al:J

    .line 2518553
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->V:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->am:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 2518554
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->W:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->an:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 2518555
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->X:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ao:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 2518556
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->Y:LX/0Px;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ap:LX/0Px;

    .line 2518557
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aq:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2518558
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->aa:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ar:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2518559
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->ab:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2518560
    iget-boolean v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->ac:Z

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->at:Z

    .line 2518561
    iget-object v0, p1, Lcom/facebook/composer/system/model/ComposerModelImpl;->ad:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->au:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 2518562
    :goto_0
    return-void

    .line 2518563
    :cond_0
    invoke-interface {p1}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->r:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 2518564
    invoke-interface {p1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->s:LX/0Px;

    .line 2518565
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->t:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 2518566
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->t()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->u:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    .line 2518567
    invoke-interface {p1}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->v:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 2518568
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->w:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    .line 2518569
    invoke-interface {p1}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->x:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 2518570
    invoke-interface {p1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->y:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2518571
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->s()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->z:LX/0P1;

    .line 2518572
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->A:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 2518573
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->hasPrivacyChanged()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->B:Z

    .line 2518574
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->C:Z

    .line 2518575
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->D:Z

    .line 2518576
    invoke-interface {p1}, LX/0it;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->E:Z

    .line 2518577
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->F:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    .line 2518578
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->k()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->G:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    .line 2518579
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->H:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    .line 2518580
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->m()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->I:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    .line 2518581
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->J:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    .line 2518582
    invoke-interface {p1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->K:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    .line 2518583
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->p()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->L:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    .line 2518584
    invoke-interface {p1}, LX/0iu;->isBackoutDraft()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->M:Z

    .line 2518585
    invoke-interface {p1}, LX/0iv;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->N:Z

    .line 2518586
    invoke-interface {p1}, LX/0iw;->isFeedOnlyPost()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->O:Z

    .line 2518587
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->P:Z

    .line 2518588
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Q:Z

    .line 2518589
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->isTargetAlbumNew()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->R:Z

    .line 2518590
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getLastXyTagChangeTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->S:J

    .line 2518591
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->i()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->T:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2518592
    invoke-interface {p1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->U:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 2518593
    invoke-interface {p1}, LX/0iy;->getMarketplaceId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->V:J

    .line 2518594
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->g()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->W:I

    .line 2518595
    invoke-interface {p1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->X:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 2518596
    invoke-interface {p1}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Y:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2518597
    invoke-interface {p1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Z:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 2518598
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPrivacyOverride()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2518599
    invoke-interface {p1}, LX/0jE;->getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ab:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 2518600
    invoke-interface {p1}, LX/0jH;->getPromptAnalytics()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ac:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 2518601
    invoke-interface {p1}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ad:LX/5Rn;

    .line 2518602
    invoke-interface {p1}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ae:Ljava/lang/Long;

    .line 2518603
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRating()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->af:I

    .line 2518604
    invoke-interface {p1}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ag:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 2518605
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRemovedUrls()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ah:LX/0Px;

    .line 2518606
    invoke-interface {p1}, LX/0j9;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ai:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 2518607
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSavedSessionLoadAttempts()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aj:I

    .line 2518608
    invoke-interface {p1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ak:Ljava/lang/String;

    .line 2518609
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->al:J

    .line 2518610
    invoke-interface {p1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->am:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 2518611
    invoke-interface {p1}, LX/0jA;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->an:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 2518612
    invoke-interface {p1}, LX/0jC;->getStorylineData()Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ao:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 2518613
    invoke-interface {p1}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ap:LX/0Px;

    .line 2518614
    invoke-interface {p1}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aq:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2518615
    invoke-interface {p1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ar:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2518616
    invoke-interface {p1}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2518617
    invoke-interface {p1}, LX/0ix;->isUserSelectedTags()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->at:Z

    .line 2518618
    invoke-interface {p1}, LX/0j7;->getViewerCoordinates()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->au:Lcom/facebook/ipc/composer/model/ComposerLocation;

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518619
    iput p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->W:I

    .line 2518620
    return-object p0
.end method

.method public final a(J)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518621
    iput-wide p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->al:J

    .line 2518622
    return-object p0
.end method

.method public final a(LX/0P1;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;)",
            "Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;"
        }
    .end annotation

    .prologue
    .line 2518623
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->z:LX/0P1;

    .line 2518624
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518625
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->t:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 2518626
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518627
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->F:Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    .line 2518628
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518629
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->T:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 2518630
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518631
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Z:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 2518632
    return-object p0
.end method

.method public final a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518633
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->u:Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    .line 2518634
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518635
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->w:Lcom/facebook/friendsharing/inspiration/model/CameraState;

    .line 2518636
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518637
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->G:Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    .line 2518638
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518639
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->H:Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    .line 2518640
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518643
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->I:Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    .line 2518644
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518668
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->J:Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    .line 2518669
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518666
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->K:Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    .line 2518667
    return-object p0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518664
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->L:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    .line 2518665
    return-object p0
.end method

.method public final a(Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518662
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->v:Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 2518663
    return-object p0
.end method

.method public final a(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518660
    iput-boolean p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->C:Z

    .line 2518661
    return-object p0
.end method

.method public final a()Lcom/facebook/composer/system/model/ComposerModelImpl;
    .locals 2

    .prologue
    .line 2518659
    new-instance v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-direct {v0, p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;-><init>(Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;)V

    return-object v0
.end method

.method public final b(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518657
    iput-boolean p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->D:Z

    .line 2518658
    return-object p0
.end method

.method public final c(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518641
    iput-boolean p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->E:Z

    .line 2518642
    return-object p0
.end method

.method public final d(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518655
    iput-boolean p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->N:Z

    .line 2518656
    return-object p0
.end method

.method public final e(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518653
    iput-boolean p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->P:Z

    .line 2518654
    return-object p0
.end method

.method public final f(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518651
    iput-boolean p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Q:Z

    .line 2518652
    return-object p0
.end method

.method public setAppAttribution(Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "app_attribution"
    .end annotation

    .prologue
    .line 2518649
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->r:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 2518650
    return-object p0
.end method

.method public setAttachments(LX/0Px;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attachments"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;"
        }
    .end annotation

    .prologue
    .line 2518647
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->s:LX/0Px;

    .line 2518648
    return-object p0
.end method

.method public setComposerSessionLoggingData(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_session_logging_data"
    .end annotation

    .prologue
    .line 2518420
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->x:Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    .line 2518421
    return-object p0
.end method

.method public setConfiguration(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "configuration"
    .end annotation

    .prologue
    .line 2518645
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->y:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2518646
    return-object p0
.end method

.method public setDateInfo(Lcom/facebook/ipc/composer/model/ComposerDateInfo;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerDateInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "date_info"
    .end annotation

    .prologue
    .line 2518362
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->A:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 2518363
    return-object p0
.end method

.method public setHasPrivacyChanged(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_privacy_changed"
    .end annotation

    .prologue
    .line 2518386
    iput-boolean p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->B:Z

    .line 2518387
    return-object p0
.end method

.method public setIsBackoutDraft(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_backout_draft"
    .end annotation

    .prologue
    .line 2518384
    iput-boolean p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->M:Z

    .line 2518385
    return-object p0
.end method

.method public setIsFeedOnlyPost(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_feed_only_post"
    .end annotation

    .prologue
    .line 2518382
    iput-boolean p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->O:Z

    .line 2518383
    return-object p0
.end method

.method public setIsTargetAlbumNew(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_target_album_new"
    .end annotation

    .prologue
    .line 2518388
    iput-boolean p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->R:Z

    .line 2518389
    return-object p0
.end method

.method public setLastXyTagChangeTime(J)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_xy_tag_change_time"
    .end annotation

    .prologue
    .line 2518380
    iput-wide p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->S:J

    .line 2518381
    return-object p0
.end method

.method public setLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "location_info"
    .end annotation

    .prologue
    .line 2518378
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->U:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 2518379
    return-object p0
.end method

.method public setMarketplaceId(J)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "marketplace_id"
    .end annotation

    .prologue
    .line 2518376
    iput-wide p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->V:J

    .line 2518377
    return-object p0
.end method

.method public setMinutiaeObject(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/composer/minutiae/model/MinutiaeObject;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "minutiae_object"
    .end annotation

    .prologue
    .line 2518374
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->X:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 2518375
    return-object p0
.end method

.method public setPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/intent/ComposerPageData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_data"
    .end annotation

    .prologue
    .line 2518372
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->Y:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2518373
    return-object p0
.end method

.method public setPrivacyOverride(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "privacy_override"
    .end annotation

    .prologue
    .line 2518370
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aa:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2518371
    return-object p0
.end method

.method public setProductItemAttachment(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ProductItemAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "product_item_attachment"
    .end annotation

    .prologue
    .line 2518368
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ab:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 2518369
    return-object p0
.end method

.method public setPromptAnalytics(Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_analytics"
    .end annotation

    .prologue
    .line 2518366
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ac:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 2518367
    return-object p0
.end method

.method public setPublishMode(LX/5Rn;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "publish_mode"
    .end annotation

    .prologue
    .line 2518364
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ad:LX/5Rn;

    .line 2518365
    return-object p0
.end method

.method public setPublishScheduleTime(Ljava/lang/Long;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "publish_schedule_time"
    .end annotation

    .prologue
    .line 2518360
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ae:Ljava/lang/Long;

    .line 2518361
    return-object p0
.end method

.method public setRating(I)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rating"
    .end annotation

    .prologue
    .line 2518390
    iput p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->af:I

    .line 2518391
    return-object p0
.end method

.method public setReferencedStickerData(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerStickerData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "referenced_sticker_data"
    .end annotation

    .prologue
    .line 2518392
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ag:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 2518393
    return-object p0
.end method

.method public setRemovedUrls(LX/0Px;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "removed_urls"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;"
        }
    .end annotation

    .prologue
    .line 2518394
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ah:LX/0Px;

    .line 2518395
    return-object p0
.end method

.method public setRichTextStyle(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rich_text_style"
    .end annotation

    .prologue
    .line 2518396
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ai:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 2518397
    return-object p0
.end method

.method public setSavedSessionLoadAttempts(I)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "saved_session_load_attempts"
    .end annotation

    .prologue
    .line 2518398
    iput p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aj:I

    .line 2518399
    return-object p0
.end method

.method public setSessionId(Ljava/lang/String;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "session_id"
    .end annotation

    .prologue
    .line 2518400
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ak:Ljava/lang/String;

    .line 2518401
    return-object p0
.end method

.method public setShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/intent/ComposerShareParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "share_params"
    .end annotation

    .prologue
    .line 2518402
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->am:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 2518403
    return-object p0
.end method

.method public setSlideshowData(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "slideshow_data"
    .end annotation

    .prologue
    .line 2518404
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->an:Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 2518405
    return-object p0
.end method

.method public setStorylineData(Lcom/facebook/ipc/composer/model/ComposerStorylineData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerStorylineData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "storyline_data"
    .end annotation

    .prologue
    .line 2518406
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ao:Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 2518407
    return-object p0
.end method

.method public setTaggedUsers(LX/0Px;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tagged_users"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;)",
            "Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;"
        }
    .end annotation

    .prologue
    .line 2518408
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ap:LX/0Px;

    .line 2518409
    return-object p0
.end method

.method public setTargetAlbum(Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLAlbum;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_album"
    .end annotation

    .prologue
    .line 2518410
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->aq:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2518411
    return-object p0
.end method

.method public setTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_data"
    .end annotation

    .prologue
    .line 2518412
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->ar:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2518413
    return-object p0
.end method

.method public setTextWithEntities(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_with_entities"
    .end annotation

    .prologue
    .line 2518414
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->as:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2518415
    return-object p0
.end method

.method public setUserSelectedTags(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "user_selected_tags"
    .end annotation

    .prologue
    .line 2518416
    iput-boolean p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->at:Z

    .line 2518417
    return-object p0
.end method

.method public setViewerCoordinates(Lcom/facebook/ipc/composer/model/ComposerLocation;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;
    .locals 0
    .param p1    # Lcom/facebook/ipc/composer/model/ComposerLocation;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "viewer_coordinates"
    .end annotation

    .prologue
    .line 2518418
    iput-object p1, p0, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->au:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 2518419
    return-object p0
.end method
