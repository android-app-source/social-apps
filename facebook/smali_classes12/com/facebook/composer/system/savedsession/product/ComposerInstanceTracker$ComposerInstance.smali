.class public final Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceSerializer;
.end annotation


# instance fields
.field public final configuration:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "configuration"
    .end annotation
.end field

.field public final lastSavedSession:Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_saved_session"
    .end annotation
.end field

.field public final startTime:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_time"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2518912
    const-class v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2518911
    const-class v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2518906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518907
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;->startTime:J

    .line 2518908
    iput-object v2, p0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;->configuration:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2518909
    iput-object v2, p0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;->lastSavedSession:Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    .line 2518910
    return-void
.end method

.method public constructor <init>(JLcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V
    .locals 1

    .prologue
    .line 2518901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518902
    iput-wide p1, p0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;->startTime:J

    .line 2518903
    iput-object p3, p0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;->configuration:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2518904
    iput-object p4, p0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;->lastSavedSession:Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    .line 2518905
    return-void
.end method
