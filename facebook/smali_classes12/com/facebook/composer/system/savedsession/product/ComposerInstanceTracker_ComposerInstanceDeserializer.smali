.class public Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceDeserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/common/json/FbJsonField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2518913
    const-class v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;

    new-instance v1, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceDeserializer;

    invoke-direct {v1}, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceDeserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2518914
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2518915
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    .line 2518916
    const-class v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;

    invoke-virtual {p0, v0}, Lcom/facebook/common/json/FbJsonDeserializer;->init(Ljava/lang/Class;)V

    .line 2518917
    return-void
.end method


# virtual methods
.method public final getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    .locals 3

    .prologue
    .line 2518918
    const-class v1, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceDeserializer;

    monitor-enter v1

    .line 2518919
    :try_start_0
    sget-object v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceDeserializer;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 2518920
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceDeserializer;->a:Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2518921
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2518922
    invoke-super {p0, p1}, Lcom/facebook/common/json/FbJsonDeserializer;->getField(Ljava/lang/String;)Lcom/facebook/common/json/FbJsonField;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :try_start_2
    monitor-exit v1

    .line 2518923
    :goto_1
    return-object v0

    .line 2518924
    :cond_2
    sget-object v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceDeserializer;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/json/FbJsonField;

    .line 2518925
    if-eqz v0, :cond_0

    .line 2518926
    monitor-exit v1

    goto :goto_1

    .line 2518927
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2518928
    :sswitch_0
    :try_start_3
    const-string v2, "start_time"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "configuration"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "last_saved_session"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 2518929
    :pswitch_0
    const-class v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;

    const-string v2, "startTime"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2518930
    :goto_2
    :try_start_4
    sget-object v2, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceDeserializer;->a:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518931
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 2518932
    :pswitch_1
    :try_start_5
    const-class v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;

    const-string v2, "configuration"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;

    move-result-object v0

    goto :goto_2

    .line 2518933
    :pswitch_2
    const-class v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;

    const-string v2, "lastSavedSession"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/json/FbJsonField;->jsonField(Ljava/lang/reflect/Field;)Lcom/facebook/common/json/FbJsonField;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    goto :goto_2

    .line 2518934
    :catch_0
    move-exception v0

    .line 2518935
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x5dc44b76 -> :sswitch_0
        0x5a5446f5 -> :sswitch_2
        0x733374f6 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
