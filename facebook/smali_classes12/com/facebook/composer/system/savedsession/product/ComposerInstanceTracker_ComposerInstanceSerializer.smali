.class public Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2518936
    const-class v0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;

    new-instance v1, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2518937
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2518938
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2518939
    if-nez p0, :cond_0

    .line 2518940
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2518941
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2518942
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceSerializer;->b(Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;LX/0nX;LX/0my;)V

    .line 2518943
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2518944
    return-void
.end method

.method private static b(Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2518945
    const-string v0, "start_time"

    iget-wide v2, p0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;->startTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2518946
    const-string v0, "configuration"

    iget-object v1, p0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;->configuration:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518947
    const-string v0, "last_saved_session"

    iget-object v1, p0, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;->lastSavedSession:Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518948
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2518949
    check-cast p1, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker_ComposerInstanceSerializer;->a(Lcom/facebook/composer/system/savedsession/product/ComposerInstanceTracker$ComposerInstance;LX/0nX;LX/0my;)V

    return-void
.end method
