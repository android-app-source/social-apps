.class public Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/composer/system/savedsession/model/ComposerSavedSessionDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/composer/system/savedsession/model/ComposerSavedSessionSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final creationTimeMs:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "creation_time_ms"
    .end annotation
.end field

.field public final model:Lcom/facebook/composer/system/model/ComposerModelImpl;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "model"
    .end annotation
.end field

.field public final pluginState:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "plugin_state"
    .end annotation
.end field

.field public final version:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "version"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2518861
    const-class v0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSessionDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2518860
    const-class v0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSessionSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2518859
    new-instance v0, LX/2s0;

    invoke-direct {v0}, LX/2s0;-><init>()V

    sput-object v0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 3
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2518853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518854
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->version:I

    .line 2518855
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->creationTimeMs:J

    .line 2518856
    iput-object v2, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 2518857
    iput-object v2, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->pluginState:Ljava/lang/String;

    .line 2518858
    return-void
.end method

.method public constructor <init>(LX/HvJ;)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518848
    iget v0, p1, LX/HvJ;->a:I

    iput v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->version:I

    .line 2518849
    iget-wide v0, p1, LX/HvJ;->b:J

    iput-wide v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->creationTimeMs:J

    .line 2518850
    iget-object v0, p1, LX/HvJ;->c:Lcom/facebook/composer/system/model/ComposerModelImpl;

    iput-object v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 2518851
    iget-object v0, p1, LX/HvJ;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->pluginState:Ljava/lang/String;

    .line 2518852
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518835
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518836
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->version:I

    .line 2518837
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->creationTimeMs:J

    .line 2518838
    const-class v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    iput-object v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 2518839
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->pluginState:Ljava/lang/String;

    .line 2518840
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518846
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 2518841
    iget v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->version:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2518842
    iget-wide v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->creationTimeMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2518843
    iget-object v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2518844
    iget-object v0, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->pluginState:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2518845
    return-void
.end method
