.class public Lcom/facebook/composer/system/savedsession/model/ComposerSavedSessionSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2518886
    const-class v0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    new-instance v1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSessionSerializer;

    invoke-direct {v1}, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSessionSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2518887
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2518888
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2518889
    if-nez p0, :cond_0

    .line 2518890
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2518891
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2518892
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSessionSerializer;->b(Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;LX/0nX;LX/0my;)V

    .line 2518893
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2518894
    return-void
.end method

.method private static b(Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2518895
    const-string v0, "version"

    iget v1, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->version:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2518896
    const-string v0, "creation_time_ms"

    iget-wide v2, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->creationTimeMs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2518897
    const-string v0, "model"

    iget-object v1, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->model:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2518898
    const-string v0, "plugin_state"

    iget-object v1, p0, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;->pluginState:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2518899
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2518900
    check-cast p1, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    invoke-static {p1, p2, p3}, Lcom/facebook/composer/system/savedsession/model/ComposerSavedSessionSerializer;->a(Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;LX/0nX;LX/0my;)V

    return-void
.end method
