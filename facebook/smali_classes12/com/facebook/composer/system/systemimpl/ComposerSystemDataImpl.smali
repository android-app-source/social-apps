.class public Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/composer/system/model/ComposerModelImpl;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2518971
    new-instance v0, LX/HvM;

    invoke-direct {v0}, LX/HvM;-><init>()V

    sput-object v0, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2518972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518973
    const-class v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    iput-object v0, p0, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 2518974
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->b:Ljava/lang/String;

    .line 2518975
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/system/model/ComposerModelImpl;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2518976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518977
    iput-object p1, p0, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 2518978
    iput-object p2, p0, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->b:Ljava/lang/String;

    .line 2518979
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/auth/viewercontext/ViewerContext;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2518980
    iget-object v0, p0, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2518981
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2518982
    iget-object v0, p0, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->a:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2518983
    iget-object v0, p0, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2518984
    return-void
.end method
