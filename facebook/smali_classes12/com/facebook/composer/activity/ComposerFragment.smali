.class public Lcom/facebook/composer/activity/ComposerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final ac:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final be:LX/0jK;


# instance fields
.field private A:LX/HqE;

.field private B:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hrk;",
            ">;"
        }
    .end annotation
.end field

.field public C:LX/AQ0;

.field private D:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/92E;",
            ">;"
        }
    .end annotation
.end field

.field private E:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/92B;",
            ">;"
        }
    .end annotation
.end field

.field private F:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/94e;",
            ">;"
        }
    .end annotation
.end field

.field private G:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7kn;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/925;",
            ">;"
        }
    .end annotation
.end field

.field private I:LX/APt;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public J:LX/HvN;

.field public K:LX/Hre;

.field public L:LX/AQ9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;"
        }
    .end annotation
.end field

.field private M:Z

.field public N:LX/BM1;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private O:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hvy;",
            ">;"
        }
    .end annotation
.end field

.field public P:LX/2zG;

.field public Q:LX/0jJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerMutator",
            "<",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;"
        }
    .end annotation
.end field

.field private R:LX/0lB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private S:LX/926;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private T:LX/Azb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private U:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BeH;",
            ">;"
        }
    .end annotation
.end field

.field private V:LX/Hvk;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private W:LX/HvX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private X:LX/0tO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private Y:LX/HvU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public Z:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EV;",
            ">;"
        }
    .end annotation
.end field

.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private aA:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Huy;",
            ">;"
        }
    .end annotation
.end field

.field public aB:LX/0gd;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public aC:LX/73w;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private aD:LX/3l1;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public aE:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;"
        }
    .end annotation
.end field

.field public aF:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qj;",
            ">;"
        }
    .end annotation
.end field

.field public aG:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public aH:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private aI:J

.field public aJ:Lcom/facebook/widget/ScrollingAwareScrollView;

.field public aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

.field private aL:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public aM:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/composer/activity/PostCompositionView;",
            ">;"
        }
    .end annotation
.end field

.field private aN:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9j9;",
            ">;"
        }
    .end annotation
.end field

.field private aO:LX/Hsp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public aP:Z

.field public aQ:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EQY;",
            ">;"
        }
    .end annotation
.end field

.field private aR:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private aS:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9j7;",
            ">;"
        }
    .end annotation
.end field

.field public aT:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DJ4;",
            ">;"
        }
    .end annotation
.end field

.field private aU:LX/ARD;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private aV:LX/AR7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private aW:LX/AR4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private aX:LX/AR2;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private aY:LX/Hq5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private aZ:LX/Hq4;

.field private aa:LX/Hv4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ab:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ix9;",
            ">;"
        }
    .end annotation
.end field

.field public ad:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public ae:LX/APn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private af:LX/APq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private ag:LX/Hvt;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private ah:LX/Hv8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ai:Z

.field private aj:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ak:LX/1Kj;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private al:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7kp;",
            ">;"
        }
    .end annotation
.end field

.field private am:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8RJ;",
            ">;"
        }
    .end annotation
.end field

.field private an:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2rb;",
            ">;"
        }
    .end annotation
.end field

.field public ao:Z

.field public ap:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public aq:LX/3iT;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public ar:Landroid/view/View;

.field public as:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/composer/privacy/common/SelectablePrivacyView;",
            ">;"
        }
    .end annotation
.end field

.field public at:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/composer/privacy/common/FixedPrivacyView;",
            ">;"
        }
    .end annotation
.end field

.field public au:Landroid/view/ViewStub;

.field public av:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public aw:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private ax:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7l0;",
            ">;"
        }
    .end annotation
.end field

.field private ay:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            ">;"
        }
    .end annotation
.end field

.field public az:LX/Hu6;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Hu6",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "LX/Hre;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1b2;",
            ">;"
        }
    .end annotation
.end field

.field private bA:LX/APe;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public bB:LX/74n;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bC:LX/APf;

.field private bD:LX/APg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public bE:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HwW;",
            ">;"
        }
    .end annotation
.end field

.field public bF:LX/HwU;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public bG:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;"
        }
    .end annotation
.end field

.field public bH:LX/ATP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bI:LX/ATO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/ATO",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "LX/Hre;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private bJ:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/ARb;",
            ">;"
        }
    .end annotation
.end field

.field private bK:LX/ARb;

.field public bL:Z

.field private final bM:LX/HqQ;

.field private final bN:LX/Hqb;

.field private final bO:LX/Hqs;

.field private bP:LX/Hr8;

.field private bQ:LX/HrJ;

.field private final bR:LX/Ar6;

.field public final bS:LX/Hra;

.field private bT:LX/Hrb;

.field private bU:LX/CfR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bV:LX/CfL;

.field public bW:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public bX:Landroid/view/View;

.field public bY:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ATQ;",
            ">;"
        }
    .end annotation
.end field

.field private bZ:LX/HtR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private ba:LX/ARW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public bb:Z

.field public bc:LX/Hu7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public bd:LX/AQ2;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bf:LX/Htl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Htl",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            "LX/Hre;",
            ">;"
        }
    .end annotation
.end field

.field private bg:LX/Htm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bh:LX/Huo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bi:LX/HuB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public bj:LX/Hux;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bk:LX/Huk;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bl:LX/Hus;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public bm:LX/HtZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public bn:Lcom/facebook/composer/header/ComposerHeaderViewController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/composer/header/ComposerHeaderViewController",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "LX/Hre;",
            ">;"
        }
    .end annotation
.end field

.field public bo:Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/composer/stickerpost/controller/ComposerStickerController",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            "LX/Hre;",
            ">;"
        }
    .end annotation
.end field

.field private bp:LX/Htg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bq:LX/Hv6;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private br:LX/Htr;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bs:LX/HuM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bt:LX/HuS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bu:LX/Htx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bv:LX/HsZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bw:LX/HuY;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bx:LX/Hv0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private by:LX/Hs8;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private bz:LX/Hrl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Hst;",
            ">;"
        }
    .end annotation
.end field

.field private final cA:LX/Be0;

.field public final cB:LX/Hqz;

.field public final cC:LX/Hr0;

.field private final cD:LX/Hr2;

.field private final cE:LX/Hr2;

.field private final cF:LX/Hr2;

.field private final cG:LX/Hr2;

.field private final cH:LX/Hr2;

.field private final cI:LX/Hr2;

.field private final cJ:LX/Hr2;

.field private final cK:LX/Hr2;

.field private final cL:LX/HrC;

.field private final cM:LX/Hr2;

.field private final cN:LX/HrE;

.field public final cO:LX/HrF;

.field public final cP:LX/93q;

.field private ca:LX/Hsy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private cb:LX/HtC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public cc:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9c3;",
            ">;"
        }
    .end annotation
.end field

.field private cd:LX/HuH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private ce:LX/HsM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private cf:LX/HuV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private cg:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final ch:LX/Hrc;

.field public ci:LX/11u;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public cj:LX/121;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final ck:LX/HqG;

.field private final cl:LX/8Rm;

.field private final cm:LX/93X;

.field public final cn:LX/HqJ;

.field public final co:LX/93W;

.field private final cp:LX/HqL;

.field private final cq:LX/HqM;

.field public final cr:LX/HqN;

.field private final cs:LX/HqO;

.field private final ct:LX/HqP;

.field private final cu:LX/HqR;

.field private final cv:LX/HqS;

.field private final cw:LX/HqT;

.field private final cx:LX/HqU;

.field public final cy:LX/HqV;

.field public final cz:LX/Hqt;

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/92o;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation runtime Lcom/facebook/contacts/annotations/IsAddressBookSyncEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public volatile f:LX/Hrs;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public volatile g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/Hqr;

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7lT;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/ASY;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/ASX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/ASX",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "LX/Hre;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/Hrd;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HwR;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private r:LX/75Q;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private s:LX/75F;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private t:LX/APZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/APY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/APY",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "LX/Hre;",
            ">;"
        }
    .end annotation
.end field

.field private v:LX/APs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/APs",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;",
            "LX/Hre;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/ARw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:LX/ARv;

.field public y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private z:LX/HqF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2513001
    const-class v0, Lcom/facebook/composer/activity/ComposerFragment;

    sput-object v0, Lcom/facebook/composer/activity/ComposerFragment;->ac:Ljava/lang/Class;

    .line 2513002
    const-class v0, Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2512881
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2512882
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512883
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->i:LX/0Ot;

    .line 2512884
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512885
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->m:LX/0Ot;

    .line 2512886
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512887
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->o:LX/0Ot;

    .line 2512888
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512889
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->p:LX/0Ot;

    .line 2512890
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512891
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->y:LX/0Ot;

    .line 2512892
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512893
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->B:LX/0Ot;

    .line 2512894
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512895
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->D:LX/0Ot;

    .line 2512896
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512897
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->E:LX/0Ot;

    .line 2512898
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512899
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->F:LX/0Ot;

    .line 2512900
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512901
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->G:LX/0Ot;

    .line 2512902
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512903
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->H:LX/0Ot;

    .line 2512904
    iput-boolean v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->M:Z

    .line 2512905
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512906
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->O:LX/0Ot;

    .line 2512907
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512908
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->U:LX/0Ot;

    .line 2512909
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512910
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Z:LX/0Ot;

    .line 2512911
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512912
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ab:LX/0Ot;

    .line 2512913
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512914
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ad:LX/0Ot;

    .line 2512915
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ai:Z

    .line 2512916
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512917
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->al:LX/0Ot;

    .line 2512918
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512919
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->am:LX/0Ot;

    .line 2512920
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512921
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->an:LX/0Ot;

    .line 2512922
    iput-boolean v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ao:Z

    .line 2512923
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512924
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ax:LX/0Ot;

    .line 2512925
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512926
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ay:LX/0Ot;

    .line 2512927
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512928
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aA:LX/0Ot;

    .line 2512929
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512930
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aE:LX/0Ot;

    .line 2512931
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512932
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aF:LX/0Ot;

    .line 2512933
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512934
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aG:LX/0Ot;

    .line 2512935
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512936
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    .line 2512937
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512938
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aN:LX/0Ot;

    .line 2512939
    iput-boolean v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aP:Z

    .line 2512940
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512941
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aQ:LX/0Ot;

    .line 2512942
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512943
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aS:LX/0Ot;

    .line 2512944
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512945
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aT:LX/0Ot;

    .line 2512946
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512947
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bE:LX/0Ot;

    .line 2512948
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512949
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bG:LX/0Ot;

    .line 2512950
    new-instance v0, LX/HqQ;

    invoke-direct {v0, p0}, LX/HqQ;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bM:LX/HqQ;

    .line 2512951
    new-instance v0, LX/Hqb;

    invoke-direct {v0, p0}, LX/Hqb;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bN:LX/Hqb;

    .line 2512952
    new-instance v0, LX/Hqs;

    invoke-direct {v0, p0}, LX/Hqs;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bO:LX/Hqs;

    .line 2512953
    new-instance v0, LX/Hr8;

    invoke-direct {v0, p0}, LX/Hr8;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bP:LX/Hr8;

    .line 2512954
    new-instance v0, LX/HrJ;

    invoke-direct {v0, p0}, LX/HrJ;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bQ:LX/HrJ;

    .line 2512955
    new-instance v0, LX/HrT;

    invoke-direct {v0, p0}, LX/HrT;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bR:LX/Ar6;

    .line 2512956
    new-instance v0, LX/Hra;

    invoke-direct {v0, p0}, LX/Hra;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bS:LX/Hra;

    .line 2512957
    new-instance v0, LX/Hrb;

    invoke-direct {v0, p0}, LX/Hrb;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bT:LX/Hrb;

    .line 2512958
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512959
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bW:LX/0Ot;

    .line 2512960
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512961
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bY:LX/0Ot;

    .line 2512962
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512963
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cc:LX/0Ot;

    .line 2512964
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2512965
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cg:LX/0Ot;

    .line 2512966
    new-instance v0, LX/Hrc;

    invoke-direct {v0, p0}, LX/Hrc;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ch:LX/Hrc;

    .line 2512967
    new-instance v0, LX/HqG;

    invoke-direct {v0, p0}, LX/HqG;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ck:LX/HqG;

    .line 2512968
    new-instance v0, LX/HqH;

    invoke-direct {v0, p0}, LX/HqH;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cl:LX/8Rm;

    .line 2512969
    new-instance v0, LX/HqI;

    invoke-direct {v0, p0}, LX/HqI;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cm:LX/93X;

    .line 2512970
    new-instance v0, LX/HqJ;

    invoke-direct {v0, p0}, LX/HqJ;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cn:LX/HqJ;

    .line 2512971
    new-instance v0, LX/93W;

    new-instance v1, LX/HqK;

    invoke-direct {v1, p0}, LX/HqK;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cm:LX/93X;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->cl:LX/8Rm;

    invoke-direct {v0, v1, v2, v3}, LX/93W;-><init>(LX/8Q5;LX/93X;LX/8Rm;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->co:LX/93W;

    .line 2512972
    new-instance v0, LX/HqL;

    invoke-direct {v0, p0}, LX/HqL;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cp:LX/HqL;

    .line 2512973
    new-instance v0, LX/HqM;

    invoke-direct {v0, p0}, LX/HqM;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cq:LX/HqM;

    .line 2512974
    new-instance v0, LX/HqN;

    invoke-direct {v0, p0}, LX/HqN;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cr:LX/HqN;

    .line 2512975
    new-instance v0, LX/HqO;

    invoke-direct {v0, p0}, LX/HqO;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cs:LX/HqO;

    .line 2512976
    new-instance v0, LX/HqP;

    invoke-direct {v0, p0}, LX/HqP;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ct:LX/HqP;

    .line 2512977
    new-instance v0, LX/HqR;

    invoke-direct {v0, p0}, LX/HqR;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cu:LX/HqR;

    .line 2512978
    new-instance v0, LX/HqS;

    invoke-direct {v0, p0}, LX/HqS;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cv:LX/HqS;

    .line 2512979
    new-instance v0, LX/HqT;

    invoke-direct {v0, p0}, LX/HqT;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cw:LX/HqT;

    .line 2512980
    new-instance v0, LX/HqU;

    invoke-direct {v0, p0}, LX/HqU;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cx:LX/HqU;

    .line 2512981
    new-instance v0, LX/HqV;

    invoke-direct {v0, p0}, LX/HqV;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cy:LX/HqV;

    .line 2512982
    new-instance v0, LX/Hqr;

    invoke-direct {v0, p0}, LX/Hqr;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->h:LX/Hqr;

    .line 2512983
    new-instance v0, LX/Hqt;

    invoke-direct {v0, p0}, LX/Hqt;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cz:LX/Hqt;

    .line 2512984
    new-instance v0, LX/Hqy;

    invoke-direct {v0, p0}, LX/Hqy;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cA:LX/Be0;

    .line 2512985
    new-instance v0, LX/Hqz;

    invoke-direct {v0, p0}, LX/Hqz;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cB:LX/Hqz;

    .line 2512986
    new-instance v0, LX/Hr1;

    invoke-direct {v0, p0}, LX/Hr1;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cC:LX/Hr0;

    .line 2512987
    new-instance v0, LX/Hr3;

    invoke-direct {v0, p0}, LX/Hr3;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cD:LX/Hr2;

    .line 2512988
    new-instance v0, LX/Hr4;

    invoke-direct {v0, p0}, LX/Hr4;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cE:LX/Hr2;

    .line 2512989
    new-instance v0, LX/Hr5;

    invoke-direct {v0, p0}, LX/Hr5;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cF:LX/Hr2;

    .line 2512990
    new-instance v0, LX/Hr6;

    invoke-direct {v0, p0}, LX/Hr6;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cG:LX/Hr2;

    .line 2512991
    new-instance v0, LX/Hr7;

    invoke-direct {v0, p0}, LX/Hr7;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cH:LX/Hr2;

    .line 2512992
    new-instance v0, LX/Hr9;

    invoke-direct {v0, p0}, LX/Hr9;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cI:LX/Hr2;

    .line 2512993
    new-instance v0, LX/HrA;

    invoke-direct {v0, p0}, LX/HrA;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cJ:LX/Hr2;

    .line 2512994
    new-instance v0, LX/HrB;

    invoke-direct {v0, p0}, LX/HrB;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cK:LX/Hr2;

    .line 2512995
    new-instance v0, LX/HrC;

    invoke-direct {v0, p0}, LX/HrC;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cL:LX/HrC;

    .line 2512996
    new-instance v0, LX/HrD;

    invoke-direct {v0, p0}, LX/HrD;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cM:LX/Hr2;

    .line 2512997
    new-instance v0, LX/HrE;

    invoke-direct {v0, p0}, LX/HrE;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cN:LX/HrE;

    .line 2512998
    new-instance v0, LX/HrG;

    invoke-direct {v0, p0}, LX/HrG;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cO:LX/HrF;

    .line 2512999
    new-instance v0, LX/HrX;

    invoke-direct {v0, p0}, LX/HrX;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cP:LX/93q;

    .line 2513000
    return-void
.end method

.method private B()V
    .locals 14

    .prologue
    const/4 v3, 0x0

    .line 2512869
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupAttachments"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512870
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2512871
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kn;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 2512872
    iget-object v6, v0, LX/7kn;->b:LX/11i;

    sget-object v7, LX/7kn;->a:LX/7km;

    const/4 v9, 0x0

    iget-object v8, v0, LX/7kn;->c:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v10

    move-object v8, v1

    invoke-interface/range {v6 .. v11}, LX/11i;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    .line 2512873
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kn;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v4

    .line 2512874
    iget-object v6, v0, LX/7kn;->b:LX/11i;

    sget-object v7, LX/7kn;->a:LX/7km;

    invoke-interface {v6, v7, v2}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v6

    invoke-static {v6}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    .line 2512875
    invoke-virtual {v6}, LX/0am;->isPresent()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2512876
    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/11o;

    const-string v7, "loadTopPhoto"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    const-string v9, "media_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v9

    iget-object v10, v0, LX/7kn;->c:LX/0So;

    invoke-interface {v10}, LX/0So;->now()J

    move-result-wide v10

    const v12, -0x19645c65

    invoke-static/range {v6 .. v12}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 2512877
    :cond_0
    invoke-static {p0, v3}, Lcom/facebook/composer/activity/ComposerFragment;->c$redex0(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 2512878
    invoke-static {p0, v3}, Lcom/facebook/composer/activity/ComposerFragment;->d$redex0(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 2512879
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupAttachments"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512880
    return-void
.end method

.method private G()V
    .locals 2

    .prologue
    .line 2512865
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2512866
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c()V

    .line 2512867
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->clearFocus()V

    .line 2512868
    return-void
.end method

.method private I()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2512846
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2512847
    iget-object v3, v0, LX/AQ9;->x:LX/ARN;

    move-object v0, v3

    .line 2512848
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2512849
    iget-object v3, v0, LX/AQ9;->x:LX/ARN;

    move-object v0, v3

    .line 2512850
    invoke-interface {v0}, LX/ARN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2512851
    :goto_0
    return v0

    .line 2512852
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->p(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aw:LX/0ad;

    sget-short v3, LX/1EB;->ap:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 2512853
    goto :goto_0

    .line 2512854
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2512855
    goto :goto_0

    .line 2512856
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v0}, LX/9J0;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 2512857
    goto :goto_0

    .line 2512858
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 2512859
    goto :goto_0

    .line 2512860
    :cond_4
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/94e;

    .line 2512861
    iget-object v3, v0, LX/94e;->a:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2512862
    iget-object v4, v0, LX/94e;->a:Landroid/content/res/Resources;

    const v5, 0x7f0b0c57

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2512863
    if-ge v3, v4, :cond_6

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 2512864
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldHideKeyboardIfReachedMinimumHeight()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static J(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 1

    .prologue
    .line 2512839
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2512840
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->a()V

    .line 2512841
    :cond_0
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->I()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    invoke-virtual {v0}, LX/ASX;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2512842
    :cond_1
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->G()V

    .line 2512843
    :cond_2
    :goto_0
    return-void

    .line 2512844
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iv;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2512845
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->ab$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    goto :goto_0
.end method

.method public static L(Lcom/facebook/composer/activity/ComposerFragment;)Z
    .locals 1

    .prologue
    .line 2512838
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static M$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 2

    .prologue
    .line 2512834
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bL:Z

    .line 2512835
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_composer_privacy"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2512836
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->v:LX/APs;

    invoke-virtual {v0}, LX/APs;->a()LX/93t;

    move-result-object v0

    invoke-virtual {v0}, LX/93s;->f()V

    .line 2512837
    return-void
.end method

.method public static N$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 3

    .prologue
    .line 2512823
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->L(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2512824
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aP:Z

    .line 2512825
    :cond_0
    :goto_0
    return-void

    .line 2512826
    :cond_1
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->Z(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2512827
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aw:LX/0ad;

    sget-short v1, LX/1EB;->ar:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->E()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2512828
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    goto :goto_0

    .line 2512829
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->am:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8RJ;

    sget-object v1, LX/8RI;->COMPOSER_FRAGMENT:LX/8RI;

    invoke-virtual {v0, v1}, LX/8RJ;->a(LX/8RI;)V

    .line 2512830
    new-instance v0, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;

    invoke-direct {v0}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;-><init>()V

    .line 2512831
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->co:LX/93W;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->a(LX/93W;)V

    .line 2512832
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 2512833
    const-string v2, "AUDIENCE_FRAGMENT_TAG"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static O$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 4

    .prologue
    .line 2512813
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2512814
    iget-object v1, v0, LX/AQ9;->A:LX/ARN;

    move-object v0, v1

    .line 2512815
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2512816
    iget-object v1, v0, LX/AQ9;->A:LX/ARN;

    move-object v0, v1

    .line 2512817
    invoke-interface {v0}, LX/ARN;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2512818
    :cond_1
    :goto_0
    return-void

    .line 2512819
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->as:LX/0zw;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->as:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_3

    .line 2512820
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->as:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;

    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aJ(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v2

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/composer/privacy/common/SelectablePrivacyView;->a(ZLcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    goto :goto_0

    .line 2512821
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->at:LX/0zw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->at:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-eqz v0, :cond_1

    .line 2512822
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->at:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/privacy/common/FixedPrivacyView;

    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aJ(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v2

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    iget-object v3, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/facebook/composer/privacy/common/FixedPrivacyView;->a(ZLcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;Lcom/facebook/graphql/model/GraphQLAlbum;)V

    goto/16 :goto_0
.end method

.method public static P$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 3

    .prologue
    .line 2512809
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const-class v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2512810
    const-string v2, "composer_target_data"

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2512811
    const/16 v0, 0x10

    invoke-virtual {p0, v1, v0}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2512812
    return-void
.end method

.method private static Q(Lcom/facebook/composer/activity/ComposerFragment;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/inlinesproutsinterfaces/InlineSproutItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2512789
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2512790
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2512791
    sget-object v1, LX/Hty;->BIRTHDAY:LX/Hty;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512792
    sget-object v1, LX/Hty;->PHOTO_GALLERY:LX/Hty;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512793
    sget-object v1, LX/Hty;->SLIDESHOW:LX/Hty;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512794
    sget-object v1, LX/Hty;->FACECAST:LX/Hty;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512795
    sget-object v1, LX/Hty;->LIGHTWEIGHT_LOCATION:LX/Hty;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512796
    sget-object v1, LX/Hty;->LOCATION:LX/Hty;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512797
    sget-object v1, LX/Hty;->BRANDED_CONTENT:LX/Hty;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512798
    sget-object v1, LX/Hty;->MINUTIAE:LX/Hty;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512799
    sget-object v1, LX/Hty;->STORYLINE:LX/Hty;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512800
    sget-object v1, LX/Hty;->TAG_PEOPLE:LX/Hty;

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512801
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v4, v0

    .line 2512802
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hty;

    .line 2512803
    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;LX/Hty;)LX/Hs6;

    move-result-object v1

    .line 2512804
    if-eqz v1, :cond_0

    .line 2512805
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512806
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2512807
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aG:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-string v6, "inline_sprouts_build_items"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Could not build item for type: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v6, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2512808
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static R$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 1

    .prologue
    .line 2512615
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bf:LX/Htl;

    invoke-virtual {v0}, LX/Htl;->a()V

    .line 2512616
    return-void
.end method

.method public static S(Lcom/facebook/composer/activity/ComposerFragment;)Z
    .locals 1

    .prologue
    .line 2512786
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isInlineSproutsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isCollapsible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static T(Lcom/facebook/composer/activity/ComposerFragment;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2512785
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v3, LX/5RF;->STICKER:LX/5RF;

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v3, LX/5RF;->MINUTIAE:LX/5RF;

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v3, LX/5RF;->MULTIMEDIA:LX/5RF;

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v3, LX/5RF;->MULTIPLE_VIDEOS:LX/5RF;

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v3, LX/5RF;->MULTIPLE_PHOTOS:LX/5RF;

    if-ne v0, v3, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->q:LX/0Uh;

    sget v3, LX/7l1;->b:I

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public static U$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 13

    .prologue
    .line 2512761
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ax:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7l0;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, LX/7ky;->a(LX/7l0;LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->a(LX/0Px;)LX/0Rf;

    move-result-object v3

    .line 2512762
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aj:LX/0Rf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aj:LX/0Rf;

    invoke-virtual {v0, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2512763
    :goto_0
    return-void

    .line 2512764
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    .line 2512765
    iget-object v10, v0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v10}, LX/0Sh;->a()V

    .line 2512766
    iget-object v10, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v10}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getLastXyTagChangeTime()J

    move-result-wide v10

    cmp-long v10, v10, v4

    if-eqz v10, :cond_2

    .line 2512767
    iget-object v10, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v10, :cond_1

    .line 2512768
    iget-object v10, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v10}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v10

    iput-object v10, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2512769
    :cond_1
    iget-object v10, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v10, v4, v5}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setLastXyTagChangeTime(J)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2512770
    iget-object v10, v0, LX/0jL;->a:LX/0cA;

    sget-object v11, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v10, v11}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2512771
    :cond_2
    move-object v0, v0

    .line 2512772
    check-cast v0, LX/0jL;

    .line 2512773
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v1

    .line 2512774
    invoke-static {v1, v3}, LX/0RA;->b(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v1

    invoke-virtual {v1}, LX/0Ro;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2512775
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2512776
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_4

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 2512777
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 2512778
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512779
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2512780
    :cond_4
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->b(LX/0Px;)Ljava/lang/Object;

    .line 2512781
    :cond_5
    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2512782
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->O$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512783
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->W$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512784
    iput-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aj:LX/0Rf;

    goto/16 :goto_0
.end method

.method public static W$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 2

    .prologue
    .line 2512759
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    invoke-interface {v1, v0}, LX/CfL;->a(LX/0Rf;)V

    .line 2512760
    return-void
.end method

.method private X()V
    .locals 10

    .prologue
    .line 2512734
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupFeedAttachment"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512735
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    const v1, 0x7f0d0ae3

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/view/ViewStub;

    .line 2512736
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ce:LX/HsM;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v0, v1}, LX/HsM;->a(Ljava/lang/Object;)LX/HsL;

    move-result-object v1

    .line 2512737
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ck:LX/HqG;

    .line 2512738
    iput-object v0, v1, LX/HsL;->k:LX/HqG;

    .line 2512739
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-virtual {v0, v1}, LX/HvN;->a(LX/0iK;)V

    .line 2512740
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ca:LX/Hsy;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->ct:LX/HqP;

    .line 2512741
    new-instance v5, LX/Hsx;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    check-cast v2, LX/0il;

    invoke-direct {v5, v4, v2, v3}, LX/Hsx;-><init>(LX/0Uh;LX/0il;LX/HqP;)V

    .line 2512742
    move-object v2, v5

    .line 2512743
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-virtual {v0, v2}, LX/HvN;->a(LX/0iK;)V

    .line 2512744
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cb:LX/HtC;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v0, v3}, LX/HtC;->a(Ljava/lang/Object;)LX/HtB;

    move-result-object v3

    .line 2512745
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-virtual {v0, v3}, LX/HvN;->a(LX/0iK;)V

    .line 2512746
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bZ:LX/HtR;

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v5, p0, Lcom/facebook/composer/activity/ComposerFragment;->cs:LX/HqO;

    invoke-virtual {v0, v4, v5}, LX/HtR;->a(Ljava/lang/Object;LX/HqO;)LX/HtP;

    move-result-object v4

    .line 2512747
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-virtual {v0, v4}, LX/HvN;->a(LX/0iK;)V

    .line 2512748
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cd:LX/HuH;

    iget-object v5, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v6, p0, Lcom/facebook/composer/activity/ComposerFragment;->cu:LX/HqR;

    invoke-virtual {v0, v5, v6}, LX/HuH;->a(Ljava/lang/Object;LX/HqR;)LX/HuG;

    move-result-object v5

    .line 2512749
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-virtual {v0, v5}, LX/HvN;->a(LX/0iK;)V

    .line 2512750
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cf:LX/HuV;

    iget-object v6, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    .line 2512751
    new-instance v9, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v8

    check-cast v8, LX/1Ad;

    invoke-direct {v9, v8, v6}, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;-><init>(LX/1Ad;LX/0il;)V

    .line 2512752
    move-object v0, v9

    .line 2512753
    iget-object v8, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v9, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    const/4 v6, 0x0

    new-array v6, v6, [LX/HsK;

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    .line 2512754
    new-instance v1, LX/Hso;

    check-cast v9, LX/0il;

    invoke-direct {v1, v9, v0, v7}, LX/Hso;-><init>(LX/0il;LX/0Rf;Landroid/view/ViewStub;)V

    .line 2512755
    move-object v0, v1

    .line 2512756
    invoke-virtual {v8, v0}, LX/HvN;->a(LX/0iK;)V

    .line 2512757
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupFeedAttachment"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512758
    return-void
.end method

.method public static Z(Lcom/facebook/composer/activity/ComposerFragment;)Z
    .locals 1

    .prologue
    .line 2512733
    iget-boolean v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aP:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aa()Lcom/facebook/ui/dialogs/FbDialogFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/0Px;)LX/0Rf;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/tagging/Tag;",
            ">;)",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2512727
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 2512728
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 2512729
    iget-wide v6, v0, Lcom/facebook/photos/base/tagging/Tag;->c:J

    move-wide v4, v6

    .line 2512730
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2512731
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2512732
    :cond_0
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/composer/activity/ComposerFragment;LX/Hty;)LX/Hs6;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2512682
    sget-object v0, LX/HrZ;->b:[I

    invoke-virtual {p1}, LX/Hty;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2512683
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2512684
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bp:LX/Htg;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cE:LX/Hr2;

    .line 2512685
    new-instance p1, LX/Htf;

    check-cast v1, LX/0il;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p1, v1, v2, v3, v4}, LX/Htf;-><init>(LX/0il;LX/Hr2;Landroid/content/res/Resources;LX/0ad;)V

    .line 2512686
    move-object v0, p1

    .line 2512687
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-virtual {v1, v0}, LX/HvN;->a(LX/0iK;)V

    goto :goto_0

    .line 2512688
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bq:LX/Hv6;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cF:LX/Hr2;

    .line 2512689
    new-instance v4, LX/Hv5;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    check-cast v1, LX/0il;

    invoke-direct {v4, v3, v1, v2}, LX/Hv5;-><init>(Landroid/content/res/Resources;LX/0il;LX/Hr2;)V

    .line 2512690
    move-object v0, v4

    .line 2512691
    goto :goto_0

    .line 2512692
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bv:LX/HsZ;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cD:LX/Hr2;

    .line 2512693
    new-instance v4, LX/HsY;

    move-object v5, v1

    check-cast v5, LX/0il;

    const-class v6, LX/Hsc;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Hsc;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v8

    check-cast v8, LX/0iA;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    move-object v6, v2

    invoke-direct/range {v4 .. v9}, LX/HsY;-><init>(LX/0il;LX/Hr2;LX/Hsc;LX/0iA;Landroid/content/res/Resources;)V

    .line 2512694
    move-object v0, v4

    .line 2512695
    goto :goto_0

    .line 2512696
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bt:LX/HuS;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cJ:LX/Hr2;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->cL:LX/HrC;

    .line 2512697
    new-instance v4, LX/HuR;

    move-object v5, v1

    check-cast v5, LX/0il;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/9j4;->a(LX/0QB;)LX/9j4;

    move-result-object v9

    check-cast v9, LX/9j4;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v0}, LX/Htv;->a(LX/0QB;)LX/Htv;

    move-result-object v11

    check-cast v11, LX/Htv;

    invoke-static {v0}, LX/HuU;->b(LX/0QB;)LX/HuU;

    move-result-object v12

    check-cast v12, LX/HuU;

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v12}, LX/HuR;-><init>(LX/0il;LX/Hr2;LX/HrC;Landroid/content/Context;LX/9j4;LX/0ad;LX/Htv;LX/HuU;)V

    .line 2512698
    move-object v0, v4

    .line 2512699
    goto/16 :goto_0

    .line 2512700
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bs:LX/HuM;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cJ:LX/Hr2;

    .line 2512701
    new-instance v5, LX/HuL;

    check-cast v1, LX/0il;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/HuU;->b(LX/0QB;)LX/HuU;

    move-result-object v4

    check-cast v4, LX/HuU;

    invoke-direct {v5, v1, v2, v3, v4}, LX/HuL;-><init>(LX/0il;LX/Hr2;Landroid/content/res/Resources;LX/HuU;)V

    .line 2512702
    move-object v0, v5

    .line 2512703
    goto/16 :goto_0

    .line 2512704
    :pswitch_5
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->br:LX/Htr;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cH:LX/Hr2;

    .line 2512705
    new-instance v4, LX/Htq;

    check-cast v1, LX/0il;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {v4, v1, v2, v3}, LX/Htq;-><init>(LX/0il;LX/Hr2;Landroid/content/res/Resources;)V

    .line 2512706
    move-object v0, v4

    .line 2512707
    goto/16 :goto_0

    .line 2512708
    :pswitch_6
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bu:LX/Htx;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cI:LX/Hr2;

    .line 2512709
    new-instance v5, LX/Htw;

    check-cast v1, LX/0il;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-direct {v5, v1, v2, v3, v4}, LX/Htw;-><init>(LX/0il;LX/Hr2;Landroid/content/res/Resources;Ljava/lang/Boolean;)V

    .line 2512710
    move-object v0, v5

    .line 2512711
    goto/16 :goto_0

    .line 2512712
    :pswitch_7
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bw:LX/HuY;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cK:LX/Hr2;

    .line 2512713
    new-instance v5, LX/HuX;

    .line 2512714
    new-instance v4, LX/HuZ;

    invoke-static {v0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v3

    check-cast v3, LX/3kp;

    invoke-direct {v4, v3}, LX/HuZ;-><init>(LX/3kp;)V

    .line 2512715
    move-object v3, v4

    .line 2512716
    check-cast v3, LX/HuZ;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {v5, v1, v2, v3, v4}, LX/HuX;-><init>(LX/0ik;LX/Hr2;LX/HuZ;Landroid/content/Context;)V

    .line 2512717
    move-object v0, v5

    .line 2512718
    goto/16 :goto_0

    .line 2512719
    :pswitch_8
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bx:LX/Hv0;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cG:LX/Hr2;

    .line 2512720
    new-instance v5, LX/Huz;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {v5, v1, v2, v3, v4}, LX/Huz;-><init>(LX/0ik;LX/Hr2;LX/0ad;Landroid/content/res/Resources;)V

    .line 2512721
    move-object v0, v5

    .line 2512722
    goto/16 :goto_0

    .line 2512723
    :pswitch_9
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->by:LX/Hs8;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cM:LX/Hr2;

    .line 2512724
    new-instance v4, LX/Hs7;

    check-cast v1, LX/0il;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v1, v2, v3}, LX/Hs7;-><init>(LX/0il;LX/Hr2;Landroid/content/Context;)V

    .line 2512725
    move-object v0, v4

    .line 2512726
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static a(Lcom/facebook/composer/activity/ComposerFragment;LX/8AA;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2512680
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p1, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2512681
    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 2512673
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupMoreOptionMenu"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512674
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aw:LX/0ad;

    sget-short v1, LX/1EB;->J:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aw:LX/0ad;

    sget-short v1, LX/1aO;->az:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2512675
    :cond_0
    new-instance v1, LX/Hqa;

    invoke-direct {v1, p0}, LX/Hqa;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512676
    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->bA:LX/APe;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    const v0, 0x7f0d0a90

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2512677
    new-instance v4, LX/APd;

    const-class v5, Landroid/content/Context;

    invoke-interface {v2, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v2}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v6

    check-cast v6, LX/1RW;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    move-object v9, v3

    check-cast v9, LX/0il;

    move-object v8, v1

    move-object v10, v0

    invoke-direct/range {v4 .. v10}, LX/APd;-><init>(Landroid/content/Context;LX/1RW;LX/0ad;LX/Hqa;LX/0il;Landroid/view/ViewStub;)V

    .line 2512678
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupMoreOptionMenu"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512679
    return-void
.end method

.method private a(Landroid/view/ViewGroup;Z)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2512654
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupTipManager"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512655
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2512656
    iget-object v1, v0, LX/AQ9;->T:LX/AQ4;

    move-object v0, v1

    .line 2512657
    if-eqz v0, :cond_2

    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    move-object v6, v0

    .line 2512658
    :goto_0
    if-nez v6, :cond_0

    .line 2512659
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 2512660
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->j:LX/ASY;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->cx:LX/HqU;

    iget-object v5, p0, Lcom/facebook/composer/activity/ComposerFragment;->bT:LX/Hrb;

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/ASY;->a(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Ljava/lang/Object;LX/HqU;LX/Hrb;LX/0Px;)LX/ASX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    .line 2512661
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p2, :cond_3

    .line 2512662
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    new-array v1, v8, [LX/ASW;

    sget-object v2, LX/ASW;->TAG_PEOPLE_FOR_CHECKIN:LX/ASW;

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, LX/ASX;->a([LX/ASW;)V

    .line 2512663
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupTipManager"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512664
    return-void

    :cond_2
    move-object v6, v2

    .line 2512665
    goto :goto_0

    .line 2512666
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2512667
    sget-object v0, LX/ASW;->TAG_PLACE_AFTER_PHOTO:LX/ASW;

    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;LX/ASW;)V

    goto :goto_1

    .line 2512668
    :cond_4
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2512669
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    new-array v1, v8, [LX/ASW;

    sget-object v2, LX/ASW;->FACECAST_ICON_NUX:LX/ASW;

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, LX/ASX;->a([LX/ASW;)V

    .line 2512670
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    new-array v1, v8, [LX/ASW;

    sget-object v2, LX/ASW;->LIVE_TOPIC_COMPOSER_NUX:LX/ASW;

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, LX/ASX;->a([LX/ASW;)V

    goto :goto_1

    .line 2512671
    :cond_5
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->B()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2512672
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    new-array v1, v8, [LX/ASW;

    sget-object v2, LX/ASW;->COMPOSER_TAG_EXPANSION_PILL_NUX:LX/ASW;

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, LX/ASX;->a([LX/ASW;)V

    goto :goto_1
.end method

.method private a(Landroid/view/ViewGroup;ZZ)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2512618
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v2, "ComposerSetupStatusView"

    invoke-virtual {v0, v2}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512619
    const v0, 0x7f0d0af1

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    .line 2512620
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    .line 2512621
    iget-object v3, v2, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->b:Landroid/view/ViewStub;

    move-object v2, v3

    .line 2512622
    invoke-virtual {v0, v2}, LX/AQ9;->b(Landroid/view/ViewStub;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2512623
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v2}, LX/2zG;->l()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->a(Z)V

    .line 2512624
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2512625
    iget-object v2, v0, LX/AQ9;->B:LX/ARN;

    move-object v0, v2

    .line 2512626
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2512627
    iget-object v2, v0, LX/AQ9;->B:LX/ARN;

    move-object v0, v2

    .line 2512628
    invoke-interface {v0}, LX/ARN;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2512629
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->setPadding(IIII)V

    .line 2512630
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    new-instance v2, LX/Hqe;

    invoke-direct {v2, p0, p2, p3}, LX/Hqe;-><init>(Lcom/facebook/composer/activity/ComposerFragment;ZZ)V

    .line 2512631
    iput-object v2, v0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->i:LX/Hqd;

    .line 2512632
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldDisableMentions()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2512633
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->d()V

    .line 2512634
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2512635
    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aq:LX/3iT;

    invoke-static {v0, v3}, LX/8of;->a(LX/175;LX/3iT;)LX/8of;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->setStatusText(Ljava/lang/CharSequence;)V

    .line 2512636
    :cond_3
    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-static {v0}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->setFriendTaggingEnabled(Z)V

    .line 2512637
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->GROUP:LX/2rw;

    if-ne v0, v1, :cond_4

    .line 2512638
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-wide v2, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual {v1, v2, v3}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->setTaggingGroupId(J)V

    .line 2512639
    :cond_4
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2512640
    iget-object v1, v0, LX/AQ9;->S:LX/AQ4;

    move-object v0, v1

    .line 2512641
    if-eqz v0, :cond_5

    .line 2512642
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->setTagTypeaheadDataSourceMetadata(Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;)V

    .line 2512643
    :cond_5
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/0Zb;

    .line 2512644
    iget-object v7, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    new-instance v8, LX/3zM;

    new-instance v0, LX/3zG;

    const-string v1, "composer"

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    iget-wide v4, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-wide v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->aI:J

    invoke-direct/range {v0 .. v5}, LX/3zG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    invoke-direct {v8, v6, v0}, LX/3zM;-><init>(LX/0Zb;LX/3zF;)V

    invoke-virtual {v7, v8}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->a(Landroid/text/TextWatcher;)V

    .line 2512645
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/Hqf;

    invoke-direct {v1, p0}, LX/Hqf;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2512646
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2512647
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    new-instance v1, LX/Hqh;

    invoke-direct {v1, p0}, LX/Hqh;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512648
    iput-object v1, v0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->l:LX/Hqg;

    .line 2512649
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    new-instance v1, LX/Hqj;

    invoke-direct {v1, p0}, LX/Hqj;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512650
    iput-object v1, v0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->m:LX/Hqi;

    .line 2512651
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupStatusView"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512652
    return-void

    :cond_6
    move v0, v1

    .line 2512653
    goto/16 :goto_0
.end method

.method private static a(Lcom/facebook/composer/activity/ComposerFragment;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/Hrs;LX/0Or;LX/0Ot;LX/ASY;LX/Hrd;LX/0Ot;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Ot;LX/0Ot;LX/0Uh;LX/75Q;LX/75F;LX/APZ;LX/ARw;LX/0Ot;LX/HqF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/APt;LX/BM1;LX/0Ot;LX/0lB;LX/926;LX/Azb;LX/0Ot;LX/Hvk;LX/HvX;LX/0tO;LX/HvU;LX/0Ot;LX/Hv4;LX/0Ot;LX/0Ot;LX/APn;LX/APq;LX/Hvt;LX/Hv8;LX/1Kj;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Xl;LX/3iT;LX/1Ck;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0gd;LX/73w;LX/3l1;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/Hsp;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/ARD;LX/AR7;LX/AR4;LX/AR2;LX/Hq5;LX/ARW;LX/Hu7;LX/AQ2;LX/Htm;LX/Huo;LX/HuB;LX/Hux;LX/Huk;LX/Hus;LX/HtZ;LX/Htg;LX/Hv6;LX/Htr;LX/HuM;LX/HuS;LX/Htx;LX/HsZ;LX/HuY;LX/Hv0;LX/Hs8;LX/Hrl;LX/APe;LX/74n;LX/APg;LX/0Ot;LX/HwU;LX/0Ot;LX/ATP;LX/0Or;LX/CfR;LX/0Ot;LX/0Ot;LX/HtR;LX/Hsy;LX/HtC;LX/0Ot;LX/HuH;LX/HsM;LX/HuV;LX/0Ot;LX/11u;LX/121;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/activity/ComposerFragment;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1b2;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Hst;",
            ">;",
            "LX/0Or",
            "<",
            "LX/92o;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/Hrs;",
            "LX/0Or",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7lT;",
            ">;",
            "LX/ASY;",
            "LX/Hrd;",
            "LX/0Ot",
            "<",
            "LX/HwR;",
            ">;",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/75Q;",
            "LX/75F;",
            "LX/APZ;",
            "LX/ARw;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/HqF;",
            "LX/0Ot",
            "<",
            "LX/Hrk;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/92E;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/92B;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/94e;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7kn;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/925;",
            ">;",
            "LX/APt;",
            "LX/BM1;",
            "LX/0Ot",
            "<",
            "LX/Hvy;",
            ">;",
            "LX/0lB;",
            "LX/926;",
            "LX/Azb;",
            "LX/0Ot",
            "<",
            "LX/BeH;",
            ">;",
            "LX/Hvk;",
            "LX/HvX;",
            "LX/0tO;",
            "LX/HvU;",
            "LX/0Ot",
            "<",
            "LX/1EV;",
            ">;",
            "LX/Hv4;",
            "LX/0Ot",
            "<",
            "LX/Ix9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;",
            "LX/APn;",
            "LX/APq;",
            "LX/Hvt;",
            "LX/Hv8;",
            "LX/1Kj;",
            "LX/0Ot",
            "<",
            "LX/7kp;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8RJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2rb;",
            ">;",
            "LX/0Xl;",
            "LX/3iT;",
            "LX/1Ck;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/7l0;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Huy;",
            ">;",
            "LX/0gd;",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            "LX/3l1;",
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0qj;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9j9;",
            ">;",
            "LX/Hsp;",
            "LX/0Ot",
            "<",
            "LX/EQY;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/9j7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DJ4;",
            ">;",
            "LX/ARD;",
            "LX/AR7;",
            "LX/AR4;",
            "LX/AR2;",
            "LX/Hq5;",
            "LX/ARW;",
            "LX/Hu7;",
            "LX/AQ2;",
            "LX/Htm;",
            "LX/Huo;",
            "LX/HuB;",
            "LX/Hux;",
            "LX/Huk;",
            "LX/Hus;",
            "LX/HtZ;",
            "LX/Htg;",
            "LX/Hv6;",
            "LX/Htr;",
            "LX/HuM;",
            "LX/HuS;",
            "LX/Htx;",
            "LX/HsZ;",
            "LX/HuY;",
            "LX/Hv0;",
            "LX/Hs8;",
            "LX/Hrl;",
            "LX/APe;",
            "LX/74n;",
            "LX/APg;",
            "LX/0Ot",
            "<",
            "LX/HwW;",
            ">;",
            "LX/HwU;",
            "LX/0Ot",
            "<",
            "LX/0yc;",
            ">;",
            "LX/ATP;",
            "LX/0Or",
            "<",
            "LX/ARb;",
            ">;",
            "Lcom/facebook/reaction/composer/ReactionComposerManagerProvider;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ATQ;",
            ">;",
            "LX/HtR;",
            "LX/Hsy;",
            "LX/HtC;",
            "LX/0Ot",
            "<",
            "LX/9c3;",
            ">;",
            "LX/HuH;",
            "LX/HsM;",
            "LX/HuV;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/11u;",
            "LX/121;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2512617
    iput-object p1, p0, Lcom/facebook/composer/activity/ComposerFragment;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/composer/activity/ComposerFragment;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/composer/activity/ComposerFragment;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/composer/activity/ComposerFragment;->d:LX/0Or;

    iput-object p5, p0, Lcom/facebook/composer/activity/ComposerFragment;->e:LX/0Or;

    iput-object p6, p0, Lcom/facebook/composer/activity/ComposerFragment;->f:LX/Hrs;

    iput-object p7, p0, Lcom/facebook/composer/activity/ComposerFragment;->g:LX/0Or;

    iput-object p8, p0, Lcom/facebook/composer/activity/ComposerFragment;->i:LX/0Ot;

    iput-object p9, p0, Lcom/facebook/composer/activity/ComposerFragment;->j:LX/ASY;

    iput-object p10, p0, Lcom/facebook/composer/activity/ComposerFragment;->l:LX/Hrd;

    iput-object p11, p0, Lcom/facebook/composer/activity/ComposerFragment;->m:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/composer/activity/ComposerFragment;->n:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p13, p0, Lcom/facebook/composer/activity/ComposerFragment;->o:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/composer/activity/ComposerFragment;->p:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->q:LX/0Uh;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->r:LX/75Q;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->s:LX/75F;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->t:LX/APZ;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->w:LX/ARw;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->y:LX/0Ot;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->z:LX/HqF;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->B:LX/0Ot;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->D:LX/0Ot;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->E:LX/0Ot;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->F:LX/0Ot;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->G:LX/0Ot;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->H:LX/0Ot;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->I:LX/APt;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->N:LX/BM1;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->O:LX/0Ot;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->R:LX/0lB;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->S:LX/926;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->T:LX/Azb;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->U:LX/0Ot;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->V:LX/Hvk;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->W:LX/HvX;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->X:LX/0tO;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Y:LX/HvU;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Z:LX/0Ot;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aa:LX/Hv4;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ab:LX/0Ot;

    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ad:LX/0Ot;

    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ae:LX/APn;

    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->af:LX/APq;

    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ag:LX/Hvt;

    move-object/from16 v0, p46

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ah:LX/Hv8;

    move-object/from16 v0, p47

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    move-object/from16 v0, p48

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->al:LX/0Ot;

    move-object/from16 v0, p49

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->am:LX/0Ot;

    move-object/from16 v0, p50

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->an:LX/0Ot;

    move-object/from16 v0, p51

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ap:LX/0Xl;

    move-object/from16 v0, p52

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aq:LX/3iT;

    move-object/from16 v0, p53

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->av:LX/1Ck;

    move-object/from16 v0, p54

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aw:LX/0ad;

    move-object/from16 v0, p55

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ax:LX/0Ot;

    move-object/from16 v0, p56

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ay:LX/0Ot;

    move-object/from16 v0, p57

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aA:LX/0Ot;

    move-object/from16 v0, p58

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    move-object/from16 v0, p59

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aC:LX/73w;

    move-object/from16 v0, p60

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aD:LX/3l1;

    move-object/from16 v0, p61

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aE:LX/0Ot;

    move-object/from16 v0, p62

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aF:LX/0Ot;

    move-object/from16 v0, p63

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aG:LX/0Ot;

    move-object/from16 v0, p64

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    move-object/from16 v0, p65

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aN:LX/0Ot;

    move-object/from16 v0, p66

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aO:LX/Hsp;

    move-object/from16 v0, p67

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aQ:LX/0Ot;

    move-object/from16 v0, p68

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aR:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, p69

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aS:LX/0Ot;

    move-object/from16 v0, p70

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aT:LX/0Ot;

    move-object/from16 v0, p71

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aU:LX/ARD;

    move-object/from16 v0, p72

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aV:LX/AR7;

    move-object/from16 v0, p73

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aW:LX/AR4;

    move-object/from16 v0, p74

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aX:LX/AR2;

    move-object/from16 v0, p75

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aY:LX/Hq5;

    move-object/from16 v0, p76

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ba:LX/ARW;

    move-object/from16 v0, p77

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bc:LX/Hu7;

    move-object/from16 v0, p78

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bd:LX/AQ2;

    move-object/from16 v0, p79

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bg:LX/Htm;

    move-object/from16 v0, p80

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bh:LX/Huo;

    move-object/from16 v0, p81

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bi:LX/HuB;

    move-object/from16 v0, p82

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bj:LX/Hux;

    move-object/from16 v0, p83

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bk:LX/Huk;

    move-object/from16 v0, p84

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bl:LX/Hus;

    move-object/from16 v0, p85

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bm:LX/HtZ;

    move-object/from16 v0, p86

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bp:LX/Htg;

    move-object/from16 v0, p87

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bq:LX/Hv6;

    move-object/from16 v0, p88

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->br:LX/Htr;

    move-object/from16 v0, p89

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bs:LX/HuM;

    move-object/from16 v0, p90

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bt:LX/HuS;

    move-object/from16 v0, p91

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bu:LX/Htx;

    move-object/from16 v0, p92

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bv:LX/HsZ;

    move-object/from16 v0, p93

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bw:LX/HuY;

    move-object/from16 v0, p94

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bx:LX/Hv0;

    move-object/from16 v0, p95

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->by:LX/Hs8;

    move-object/from16 v0, p96

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bz:LX/Hrl;

    move-object/from16 v0, p97

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bA:LX/APe;

    move-object/from16 v0, p98

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bB:LX/74n;

    move-object/from16 v0, p99

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bD:LX/APg;

    move-object/from16 v0, p100

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bE:LX/0Ot;

    move-object/from16 v0, p101

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bF:LX/HwU;

    move-object/from16 v0, p102

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bG:LX/0Ot;

    move-object/from16 v0, p103

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bH:LX/ATP;

    move-object/from16 v0, p104

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bJ:LX/0Or;

    move-object/from16 v0, p105

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bU:LX/CfR;

    move-object/from16 v0, p106

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bW:LX/0Ot;

    move-object/from16 v0, p107

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bY:LX/0Ot;

    move-object/from16 v0, p108

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bZ:LX/HtR;

    move-object/from16 v0, p109

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ca:LX/Hsy;

    move-object/from16 v0, p110

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cb:LX/HtC;

    move-object/from16 v0, p111

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cc:LX/0Ot;

    move-object/from16 v0, p112

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cd:LX/HuH;

    move-object/from16 v0, p113

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ce:LX/HsM;

    move-object/from16 v0, p114

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cf:LX/HuV;

    move-object/from16 v0, p115

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cg:LX/0Ot;

    move-object/from16 v0, p116

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ci:LX/11u;

    move-object/from16 v0, p117

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cj:LX/121;

    return-void
.end method

.method public static a(Lcom/facebook/composer/activity/ComposerFragment;LX/0ge;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2513227
    if-eqz p1, :cond_0

    .line 2513228
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2513229
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aC:LX/73w;

    .line 2513230
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2513231
    const-string p0, "composer_button_target"

    invoke-interface {v1, p0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2513232
    sget-object p0, LX/74R;->COMPOSER_BUTTON_CLICKED:LX/74R;

    const/4 p1, 0x0

    invoke-static {v0, p0, v1, p1}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2513233
    return-void
.end method

.method private static a(Lcom/facebook/composer/activity/ComposerFragment;LX/ASW;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2513003
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2513004
    :goto_0
    return-void

    .line 2513005
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 2513006
    iget-object v1, v0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 2513007
    iget-object v1, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a()Z

    move-result v1

    if-eq v1, v2, :cond_2

    .line 2513008
    iget-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v1, :cond_1

    .line 2513009
    iget-object v1, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v1

    iput-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2513010
    :cond_1
    iget-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v1, v2}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2513011
    iget-object v1, v0, LX/0jL;->a:LX/0cA;

    sget-object v3, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v1, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2513012
    :cond_2
    move-object v0, v0

    .line 2513013
    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2513014
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    new-array v1, v2, [LX/ASW;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, LX/ASX;->a([LX/ASW;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/composer/activity/ComposerFragment;Landroid/content/Intent;Z)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    .line 2513234
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->l(LX/0Px;)Z

    move-result v4

    .line 2513235
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->j(LX/0Px;)Z

    move-result v5

    .line 2513236
    invoke-static {p1}, LX/HwS;->a(Landroid/content/Intent;)LX/0Px;

    move-result-object v1

    .line 2513237
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bF:LX/HwU;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v0, v2}, LX/HwU;->a(LX/0il;)LX/HwT;

    move-result-object v6

    .line 2513238
    invoke-virtual {v6, v1, v4}, LX/HwT;->a(LX/0Px;Z)V

    .line 2513239
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2513240
    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2513241
    invoke-static {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v2, v0

    .line 2513242
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HwR;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    invoke-virtual {v0, v7, v2, v1}, LX/HwR;->a(Ljava/lang/String;LX/0Px;Z)LX/7ks;

    move-result-object v0

    .line 2513243
    iget-object v1, v0, LX/7ks;->a:LX/0Px;

    .line 2513244
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2513245
    :goto_1
    return-void

    .line 2513246
    :cond_0
    const-string v1, "creative_cam_result_extra"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 2513247
    const-string v0, "creative_cam_result_extra"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;

    .line 2513248
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bB:LX/74n;

    .line 2513249
    iget-object v2, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->b:Landroid/net/Uri;

    move-object v2, v2

    .line 2513250
    sget-object v7, LX/74j;->CREATIVECAM_MEDIA:LX/74j;

    invoke-virtual {v1, v2, v7}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 2513251
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    .line 2513252
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 2513253
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v9

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    if-ge v2, v9, :cond_2

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2513254
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v10

    .line 2513255
    iget-object v11, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->b:Landroid/net/Uri;

    move-object v11, v11

    .line 2513256
    invoke-virtual {v10, v11}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2513257
    invoke-static {v1}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v1

    .line 2513258
    iget-object v10, v0, Lcom/facebook/ipc/creativecam/CreativeCamResult;->c:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v10, v10

    .line 2513259
    iput-object v10, v1, LX/7kv;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 2513260
    invoke-virtual {v1}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    invoke-virtual {v8, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2513261
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 2513262
    :cond_2
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2513263
    move-object v2, v0

    goto/16 :goto_0

    .line 2513264
    :cond_3
    invoke-static {p0, v1}, Lcom/facebook/composer/activity/ComposerFragment;->b(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;)V

    .line 2513265
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v1, v0}, LX/7kq;->a(LX/0Px;LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0, v3, p2}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;ZZ)V

    .line 2513266
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v2, LX/5RF;->SLIDESHOW:LX/5RF;

    if-eq v0, v2, :cond_4

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    :goto_3
    invoke-virtual {v1, v0}, LX/ATO;->c(Z)V

    .line 2513267
    invoke-virtual {v6, v4, v5}, LX/HwT;->a(ZZ)V

    .line 2513268
    sget-object v0, LX/ASW;->TAG_PLACE_AFTER_PHOTO:LX/ASW;

    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;LX/ASW;)V

    goto/16 :goto_1

    .line 2513269
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    :cond_5
    move-object v2, v0

    goto/16 :goto_0
.end method

.method public static a(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/ipc/composer/model/ComposerStickerData;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2513270
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2513271
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p1}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, v2}, LX/0jL;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2513272
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v0

    invoke-interface {v0, v2}, LX/CfL;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 2513273
    :goto_0
    return-void

    .line 2513274
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p1}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V
    .locals 5
    .param p0    # Lcom/facebook/composer/activity/ComposerFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2513214
    if-eqz p1, :cond_0

    .line 2513215
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->U:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BeH;

    new-instance v1, LX/BeI;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android_feather_post_compose"

    invoke-direct {v1, v2, v3}, LX/BeI;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    .line 2513216
    iget-object v2, v1, LX/BeI;->a:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, LX/BeI;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2513217
    :cond_0
    :goto_0
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v0

    invoke-interface {v0, p1}, LX/CfL;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2513218
    return-void

    .line 2513219
    :cond_1
    iget-object v2, v0, LX/BeH;->c:LX/0Uh;

    const/16 v3, 0x31a

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/BeH;->c:LX/0Uh;

    const/16 v3, 0x319

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2513220
    iget-object v2, v0, LX/BeH;->e:LX/0if;

    sget-object v3, LX/0ig;->au:LX/0ih;

    invoke-virtual {v2, v3}, LX/0if;->a(LX/0ih;)V

    .line 2513221
    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/BeH;->a(LX/BeH;LX/BeI;Z)Z

    goto :goto_0
.end method

.method public static a(Lcom/facebook/composer/activity/ComposerFragment;Ljava/lang/String;Z)V
    .locals 10

    .prologue
    .line 2513275
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5Qv;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2513276
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->u:LX/APY;

    new-instance v1, LX/HrN;

    invoke-direct {v1, p0}, LX/HrN;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    const/4 p2, 0x0

    const/4 p1, 0x1

    const/4 v9, 0x0

    .line 2513277
    iget-object v2, v0, LX/APY;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    .line 2513278
    iput-boolean v9, v0, LX/APY;->e:Z

    .line 2513279
    iget-object v4, v0, LX/APY;->d:LX/ATy;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j0;

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/ATy;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2513280
    sget-object v2, LX/8K5;->COMPOSER_CANCEL_DIALOG:LX/8K5;

    invoke-virtual {v0, v2}, LX/APY;->a(LX/8K5;)V

    .line 2513281
    iput-boolean p1, v0, LX/APY;->e:Z

    .line 2513282
    invoke-virtual {v1}, LX/HrN;->a()V

    .line 2513283
    :goto_0
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_DISCARD_DIALOG_DISPLAYED:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-wide v4, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/0gd;->b(LX/0ge;Ljava/lang/String;JLX/2rt;)V

    .line 2513284
    return-void

    .line 2513285
    :cond_0
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2513286
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, LX/0ju;->a(Z)LX/0ju;

    .line 2513287
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2513288
    iget-object v2, v0, LX/AQ9;->I:LX/AQ4;

    move-object v0, v2

    .line 2513289
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2513290
    iget-object v2, v0, LX/AQ9;->I:LX/AQ4;

    move-object v0, v2

    .line 2513291
    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2513292
    invoke-virtual {v1, p1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2513293
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2513294
    iget-object v2, v0, LX/AQ9;->G:LX/AQ4;

    move-object v0, v2

    .line 2513295
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2513296
    iget-object v2, v0, LX/AQ9;->G:LX/AQ4;

    move-object v0, v2

    .line 2513297
    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_2
    new-instance v2, LX/HrO;

    invoke-direct {v2, p0}, LX/HrO;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2513298
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2513299
    iget-object v2, v0, LX/AQ9;->F:LX/AQ4;

    move-object v0, v2

    .line 2513300
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2513301
    iget-object v2, v0, LX/AQ9;->F:LX/AQ4;

    move-object v0, v2

    .line 2513302
    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_3
    new-instance v2, LX/HrP;

    invoke-direct {v2, p0}, LX/HrP;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2513303
    new-instance v0, LX/HrQ;

    invoke-direct {v0, p0}, LX/HrQ;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {v1, v0}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    .line 2513304
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2513305
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 2513306
    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 2513307
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    goto/16 :goto_0

    .line 2513308
    :cond_1
    const v0, 0x7f081439    # 1.8088E38f

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2513309
    :cond_2
    const v0, 0x7f081436

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2513310
    :cond_3
    const v0, 0x7f08143d

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 2513311
    :cond_4
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setRequestedOrientation(I)V

    goto/16 :goto_0

    .line 2513312
    :cond_5
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j0;

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, LX/APY;->i:Ljava/lang/String;

    .line 2513313
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    invoke-interface {v3}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    iput v3, v0, LX/APY;->j:I

    .line 2513314
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j2;

    invoke-interface {v2}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v2

    iput v2, v0, LX/APY;->k:I

    .line 2513315
    const-string v2, "open_dialog"

    invoke-static {v0, v2}, LX/APY;->a$redex0(LX/APY;Ljava/lang/String;)V

    .line 2513316
    new-instance v2, LX/APR;

    invoke-direct {v2, v0, v1}, LX/APR;-><init>(LX/APY;LX/HrN;)V

    .line 2513317
    new-instance v3, LX/APS;

    invoke-direct {v3, v0, v1}, LX/APS;-><init>(LX/APY;LX/HrN;)V

    .line 2513318
    new-instance v4, LX/APT;

    invoke-direct {v4, v0}, LX/APT;-><init>(LX/APY;)V

    .line 2513319
    iget-object v5, v1, LX/HrN;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    move-object v5, v5

    .line 2513320
    iget-object v6, v0, LX/APY;->b:LX/0ad;

    sget-char v7, LX/1aO;->g:C

    const v8, 0x7f0814a1

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, LX/APY;->f:Ljava/lang/String;

    .line 2513321
    iget-object v6, v0, LX/APY;->b:LX/0ad;

    sget-char v7, LX/1aO;->j:C

    const v8, 0x7f0814a2

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, LX/APY;->g:Ljava/lang/String;

    .line 2513322
    iget-object v6, v0, LX/APY;->b:LX/0ad;

    sget-char v7, LX/1aO;->e:C

    const v8, 0x7f0814a4

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, LX/APY;->h:Ljava/lang/String;

    .line 2513323
    new-instance v6, LX/0ju;

    invoke-direct {v6, v5}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2513324
    invoke-virtual {v6, p1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v7

    iget-object v8, v0, LX/APY;->h:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    .line 2513325
    iget-object v4, v0, LX/APY;->b:LX/0ad;

    sget-short v7, LX/1aO;->d:S

    invoke-interface {v4, v7, v9}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2513326
    const-string v2, "layout_inflater"

    invoke-virtual {v5, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 2513327
    const v3, 0x7f030311

    invoke-virtual {v2, v3, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 2513328
    const v4, 0x7f030312

    invoke-virtual {v2, v4, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 2513329
    iget-object v2, v0, LX/APY;->b:LX/0ad;

    sget-short v4, LX/1aO;->h:S

    invoke-interface {v2, v4, v9}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2513330
    invoke-virtual {v3, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2513331
    :cond_6
    const v2, 0x7f0d0a6f

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/fig/button/FigButton;

    .line 2513332
    const v4, 0x7f0d0a6e

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/fig/button/FigButton;

    .line 2513333
    iget-object v7, v0, LX/APY;->g:Ljava/lang/String;

    invoke-virtual {v2, v7}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2513334
    iget-object v7, v0, LX/APY;->f:Ljava/lang/String;

    invoke-virtual {v4, v7}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2513335
    invoke-virtual {v6, v5}, LX/0ju;->a(Landroid/view/View;)LX/0ju;

    .line 2513336
    invoke-virtual {v6, v3}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    .line 2513337
    invoke-virtual {v6}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    .line 2513338
    invoke-virtual {v3}, LX/2EJ;->show()V

    .line 2513339
    new-instance v5, LX/APU;

    invoke-direct {v5, v0, v1, v3}, LX/APU;-><init>(LX/APY;LX/HrN;LX/2EJ;)V

    invoke-virtual {v2, v5}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2513340
    new-instance v2, LX/APV;

    invoke-direct {v2, v0, v1, v3}, LX/APV;-><init>(LX/APY;LX/HrN;LX/2EJ;)V

    invoke-virtual {v4, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2513341
    const v2, 0x7f0d0a71

    invoke-virtual {v3, v2}, LX/2EJ;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 2513342
    new-instance v4, LX/APW;

    invoke-direct {v4, v0, v3}, LX/APW;-><init>(LX/APY;LX/2EJ;)V

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2513343
    :cond_7
    iget-object v4, v0, LX/APY;->f:Ljava/lang/String;

    invoke-virtual {v6, v4, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    iget-object v4, v0, LX/APY;->g:Ljava/lang/String;

    invoke-virtual {v2, v4, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2513344
    iget-object v2, v0, LX/APY;->b:LX/0ad;

    sget-short v3, LX/1aO;->i:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v2, v2

    .line 2513345
    if-eqz v2, :cond_8

    .line 2513346
    const-string v2, "layout_inflater"

    invoke-virtual {v5, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 2513347
    const v3, 0x7f030312

    invoke-virtual {v2, v3, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2513348
    invoke-virtual {v6, v2}, LX/0ju;->a(Landroid/view/View;)LX/0ju;

    .line 2513349
    invoke-virtual {v6}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    .line 2513350
    invoke-virtual {v3}, LX/2EJ;->show()V

    .line 2513351
    const v2, 0x7f0d0a71

    invoke-virtual {v3, v2}, LX/2EJ;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 2513352
    new-instance v4, LX/APX;

    invoke-direct {v4, v0, v3}, LX/APX;-><init>(LX/APY;LX/2EJ;)V

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2513353
    :cond_8
    const v2, 0x7f0814a3

    invoke-virtual {v6, v2}, LX/0ju;->a(I)LX/0ju;

    .line 2513354
    invoke-virtual {v6}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    .line 2513355
    invoke-virtual {v2}, LX/2EJ;->show()V

    goto/16 :goto_0
.end method

.method public static a(Lcom/facebook/composer/activity/ComposerFragment;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2513356
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/photos/taggablegallery/TaggableGalleryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "extra_taggable_gallery_photo_list"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_session_id"

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_taggable_gallery_photo_item_id"

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2513357
    const/16 v1, 0x83

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2513358
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 120

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v119

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/composer/activity/ComposerFragment;

    const/16 v3, 0x12cb

    move-object/from16 v0, v119

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x533

    move-object/from16 v0, v119

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1980

    move-object/from16 v0, v119

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x19b7

    move-object/from16 v0, v119

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1477

    move-object/from16 v0, v119

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const-class v8, LX/Hrs;

    move-object/from16 v0, v119

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Hrs;

    const/16 v9, 0xbc

    move-object/from16 v0, v119

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x19c2

    move-object/from16 v0, v119

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const-class v11, LX/ASY;

    move-object/from16 v0, v119

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/ASY;

    invoke-static/range {v119 .. v119}, LX/Hrd;->a(LX/0QB;)LX/Hrd;

    move-result-object v12

    check-cast v12, LX/Hrd;

    const/16 v13, 0x19e4

    move-object/from16 v0, v119

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v119 .. v119}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v14

    check-cast v14, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    const/16 v15, 0x97

    move-object/from16 v0, v119

    invoke-static {v0, v15}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x2db

    move-object/from16 v0, v119

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    invoke-static/range {v119 .. v119}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v17

    check-cast v17, LX/0Uh;

    invoke-static/range {v119 .. v119}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v18

    check-cast v18, LX/75Q;

    invoke-static/range {v119 .. v119}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v19

    check-cast v19, LX/75F;

    const-class v20, LX/APZ;

    move-object/from16 v0, v119

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/APZ;

    const-class v21, LX/ARw;

    move-object/from16 v0, v119

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/ARw;

    const/16 v22, 0x271

    move-object/from16 v0, v119

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const-class v23, LX/HqF;

    move-object/from16 v0, v119

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/HqF;

    const/16 v24, 0x1959

    move-object/from16 v0, v119

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x19ae

    move-object/from16 v0, v119

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0x19ab

    move-object/from16 v0, v119

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v26

    const/16 v27, 0x19e3

    move-object/from16 v0, v119

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v27

    const/16 v28, 0x195f

    move-object/from16 v0, v119

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v28

    const/16 v29, 0x19a8

    move-object/from16 v0, v119

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v29

    const-class v30, LX/APt;

    move-object/from16 v0, v119

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v30

    check-cast v30, LX/APt;

    invoke-static/range {v119 .. v119}, LX/BM1;->a(LX/0QB;)LX/BM1;

    move-result-object v31

    check-cast v31, LX/BM1;

    const/16 v32, 0x19de

    move-object/from16 v0, v119

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v32

    invoke-static/range {v119 .. v119}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v33

    check-cast v33, LX/0lB;

    invoke-static/range {v119 .. v119}, LX/926;->a(LX/0QB;)LX/926;

    move-result-object v34

    check-cast v34, LX/926;

    invoke-static/range {v119 .. v119}, LX/Azb;->a(LX/0QB;)LX/Azb;

    move-result-object v35

    check-cast v35, LX/Azb;

    const/16 v36, 0x1a41

    move-object/from16 v0, v119

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v36

    const-class v37, LX/Hvk;

    move-object/from16 v0, v119

    move-object/from16 v1, v37

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v37

    check-cast v37, LX/Hvk;

    const-class v38, LX/HvX;

    move-object/from16 v0, v119

    move-object/from16 v1, v38

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v38

    check-cast v38, LX/HvX;

    invoke-static/range {v119 .. v119}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v39

    check-cast v39, LX/0tO;

    const-class v40, LX/HvU;

    move-object/from16 v0, v119

    move-object/from16 v1, v40

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v40

    check-cast v40, LX/HvU;

    const/16 v41, 0xb2a

    move-object/from16 v0, v119

    move/from16 v1, v41

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v41

    const-class v42, LX/Hv4;

    move-object/from16 v0, v119

    move-object/from16 v1, v42

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v42

    check-cast v42, LX/Hv4;

    const/16 v43, 0x2c11

    move-object/from16 v0, v119

    move/from16 v1, v43

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v43

    const/16 v44, 0x2e3

    move-object/from16 v0, v119

    move/from16 v1, v44

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v44

    const-class v45, LX/APn;

    move-object/from16 v0, v119

    move-object/from16 v1, v45

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v45

    check-cast v45, LX/APn;

    const-class v46, LX/APq;

    move-object/from16 v0, v119

    move-object/from16 v1, v46

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v46

    check-cast v46, LX/APq;

    const-class v47, LX/Hvt;

    move-object/from16 v0, v119

    move-object/from16 v1, v47

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v47

    check-cast v47, LX/Hvt;

    const-class v48, LX/Hv8;

    move-object/from16 v0, v119

    move-object/from16 v1, v48

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v48

    check-cast v48, LX/Hv8;

    invoke-static/range {v119 .. v119}, LX/1Kj;->a(LX/0QB;)LX/1Kj;

    move-result-object v49

    check-cast v49, LX/1Kj;

    const/16 v50, 0x1960

    move-object/from16 v0, v119

    move/from16 v1, v50

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v50

    const/16 v51, 0x2fe5

    move-object/from16 v0, v119

    move/from16 v1, v51

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v51

    const/16 v52, 0x3c2

    move-object/from16 v0, v119

    move/from16 v1, v52

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v52

    invoke-static/range {v119 .. v119}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v53

    check-cast v53, LX/0Xl;

    invoke-static/range {v119 .. v119}, LX/3iT;->a(LX/0QB;)LX/3iT;

    move-result-object v54

    check-cast v54, LX/3iT;

    invoke-static/range {v119 .. v119}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v55

    check-cast v55, LX/1Ck;

    invoke-static/range {v119 .. v119}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v56

    check-cast v56, LX/0ad;

    const/16 v57, 0x1962

    move-object/from16 v0, v119

    move/from16 v1, v57

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v57

    const/16 v58, 0x2fc7

    move-object/from16 v0, v119

    move/from16 v1, v58

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v58

    const/16 v59, 0x19cf

    move-object/from16 v0, v119

    move/from16 v1, v59

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v59

    invoke-static/range {v119 .. v119}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v60

    check-cast v60, LX/0gd;

    invoke-static/range {v119 .. v119}, LX/73w;->a(LX/0QB;)LX/73w;

    move-result-object v61

    check-cast v61, LX/73w;

    invoke-static/range {v119 .. v119}, LX/3l1;->a(LX/0QB;)LX/3l1;

    move-result-object v62

    check-cast v62, LX/3l1;

    const/16 v63, 0xf39

    move-object/from16 v0, v119

    move/from16 v1, v63

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v63

    const/16 v64, 0x24d

    move-object/from16 v0, v119

    move/from16 v1, v64

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v64

    const/16 v65, 0x259

    move-object/from16 v0, v119

    move/from16 v1, v65

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v65

    const/16 v66, 0x455

    move-object/from16 v0, v119

    move/from16 v1, v66

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v66

    const/16 v67, 0x2f22

    move-object/from16 v0, v119

    move/from16 v1, v67

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v67

    const-class v68, LX/Hsp;

    move-object/from16 v0, v119

    move-object/from16 v1, v68

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v68

    check-cast v68, LX/Hsp;

    const/16 v69, 0x37a5

    move-object/from16 v0, v119

    move/from16 v1, v69

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v69

    invoke-static/range {v119 .. v119}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v70

    check-cast v70, Ljava/util/concurrent/ExecutorService;

    const/16 v71, 0x2f21

    move-object/from16 v0, v119

    move/from16 v1, v71

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v71

    const/16 v72, 0x2366

    move-object/from16 v0, v119

    move/from16 v1, v72

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v72

    const-class v73, LX/ARD;

    move-object/from16 v0, v119

    move-object/from16 v1, v73

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v73

    check-cast v73, LX/ARD;

    const-class v74, LX/AR7;

    move-object/from16 v0, v119

    move-object/from16 v1, v74

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v74

    check-cast v74, LX/AR7;

    const-class v75, LX/AR4;

    move-object/from16 v0, v119

    move-object/from16 v1, v75

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v75

    check-cast v75, LX/AR4;

    const-class v76, LX/AR2;

    move-object/from16 v0, v119

    move-object/from16 v1, v76

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v76

    check-cast v76, LX/AR2;

    const-class v77, LX/Hq5;

    move-object/from16 v0, v119

    move-object/from16 v1, v77

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v77

    check-cast v77, LX/Hq5;

    const-class v78, LX/ARW;

    move-object/from16 v0, v119

    move-object/from16 v1, v78

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v78

    check-cast v78, LX/ARW;

    const-class v79, LX/Hu7;

    move-object/from16 v0, v119

    move-object/from16 v1, v79

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v79

    check-cast v79, LX/Hu7;

    const-class v80, LX/AQ2;

    move-object/from16 v0, v119

    move-object/from16 v1, v80

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v80

    check-cast v80, LX/AQ2;

    const-class v81, LX/Htm;

    move-object/from16 v0, v119

    move-object/from16 v1, v81

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v81

    check-cast v81, LX/Htm;

    const-class v82, LX/Huo;

    move-object/from16 v0, v119

    move-object/from16 v1, v82

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v82

    check-cast v82, LX/Huo;

    const-class v83, LX/HuB;

    move-object/from16 v0, v119

    move-object/from16 v1, v83

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v83

    check-cast v83, LX/HuB;

    const-class v84, LX/Hux;

    move-object/from16 v0, v119

    move-object/from16 v1, v84

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v84

    check-cast v84, LX/Hux;

    const-class v85, LX/Huk;

    move-object/from16 v0, v119

    move-object/from16 v1, v85

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v85

    check-cast v85, LX/Huk;

    const-class v86, LX/Hus;

    move-object/from16 v0, v119

    move-object/from16 v1, v86

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v86

    check-cast v86, LX/Hus;

    const-class v87, LX/HtZ;

    move-object/from16 v0, v119

    move-object/from16 v1, v87

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v87

    check-cast v87, LX/HtZ;

    const-class v88, LX/Htg;

    move-object/from16 v0, v119

    move-object/from16 v1, v88

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v88

    check-cast v88, LX/Htg;

    const-class v89, LX/Hv6;

    move-object/from16 v0, v119

    move-object/from16 v1, v89

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v89

    check-cast v89, LX/Hv6;

    const-class v90, LX/Htr;

    move-object/from16 v0, v119

    move-object/from16 v1, v90

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v90

    check-cast v90, LX/Htr;

    const-class v91, LX/HuM;

    move-object/from16 v0, v119

    move-object/from16 v1, v91

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v91

    check-cast v91, LX/HuM;

    const-class v92, LX/HuS;

    move-object/from16 v0, v119

    move-object/from16 v1, v92

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v92

    check-cast v92, LX/HuS;

    const-class v93, LX/Htx;

    move-object/from16 v0, v119

    move-object/from16 v1, v93

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v93

    check-cast v93, LX/Htx;

    const-class v94, LX/HsZ;

    move-object/from16 v0, v119

    move-object/from16 v1, v94

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v94

    check-cast v94, LX/HsZ;

    const-class v95, LX/HuY;

    move-object/from16 v0, v119

    move-object/from16 v1, v95

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v95

    check-cast v95, LX/HuY;

    const-class v96, LX/Hv0;

    move-object/from16 v0, v119

    move-object/from16 v1, v96

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v96

    check-cast v96, LX/Hv0;

    const-class v97, LX/Hs8;

    move-object/from16 v0, v119

    move-object/from16 v1, v97

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v97

    check-cast v97, LX/Hs8;

    invoke-static/range {v119 .. v119}, LX/Hrl;->a(LX/0QB;)LX/Hrl;

    move-result-object v98

    check-cast v98, LX/Hrl;

    const-class v99, LX/APe;

    move-object/from16 v0, v119

    move-object/from16 v1, v99

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v99

    check-cast v99, LX/APe;

    invoke-static/range {v119 .. v119}, LX/74n;->a(LX/0QB;)LX/74n;

    move-result-object v100

    check-cast v100, LX/74n;

    const-class v101, LX/APg;

    move-object/from16 v0, v119

    move-object/from16 v1, v101

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v101

    check-cast v101, LX/APg;

    const/16 v102, 0x19e5

    move-object/from16 v0, v119

    move/from16 v1, v102

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v102

    const-class v103, LX/HwU;

    move-object/from16 v0, v119

    move-object/from16 v1, v103

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v103

    check-cast v103, LX/HwU;

    const/16 v104, 0x4e0

    move-object/from16 v0, v119

    move/from16 v1, v104

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v104

    const-class v105, LX/ATP;

    move-object/from16 v0, v119

    move-object/from16 v1, v105

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v105

    check-cast v105, LX/ATP;

    const/16 v106, 0x19d9

    move-object/from16 v0, v119

    move/from16 v1, v106

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v106

    invoke-static/range {v119 .. v119}, LX/CfO;->a(LX/0QB;)LX/CfR;

    move-result-object v107

    check-cast v107, LX/CfR;

    const/16 v108, 0x12c4

    move-object/from16 v0, v119

    move/from16 v1, v108

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v108

    const/16 v109, 0x19e1

    move-object/from16 v0, v119

    move/from16 v1, v109

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v109

    const-class v110, LX/HtR;

    move-object/from16 v0, v119

    move-object/from16 v1, v110

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v110

    check-cast v110, LX/HtR;

    const-class v111, LX/Hsy;

    move-object/from16 v0, v119

    move-object/from16 v1, v111

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v111

    check-cast v111, LX/Hsy;

    const-class v112, LX/HtC;

    move-object/from16 v0, v119

    move-object/from16 v1, v112

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v112

    check-cast v112, LX/HtC;

    const/16 v113, 0x2e22

    move-object/from16 v0, v119

    move/from16 v1, v113

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v113

    const-class v114, LX/HuH;

    move-object/from16 v0, v119

    move-object/from16 v1, v114

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v114

    check-cast v114, LX/HuH;

    const-class v115, LX/HsM;

    move-object/from16 v0, v119

    move-object/from16 v1, v115

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v115

    check-cast v115, LX/HsM;

    const-class v116, LX/HuV;

    move-object/from16 v0, v119

    move-object/from16 v1, v116

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v116

    check-cast v116, LX/HuV;

    const/16 v117, 0x19c6

    move-object/from16 v0, v119

    move/from16 v1, v117

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v117

    invoke-static/range {v119 .. v119}, LX/11u;->a(LX/0QB;)LX/11u;

    move-result-object v118

    check-cast v118, LX/11u;

    invoke-static/range {v119 .. v119}, LX/128;->a(LX/0QB;)LX/128;

    move-result-object v119

    check-cast v119, LX/121;

    invoke-static/range {v2 .. v119}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/Hrs;LX/0Or;LX/0Ot;LX/ASY;LX/Hrd;LX/0Ot;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/0Ot;LX/0Ot;LX/0Uh;LX/75Q;LX/75F;LX/APZ;LX/ARw;LX/0Ot;LX/HqF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/APt;LX/BM1;LX/0Ot;LX/0lB;LX/926;LX/Azb;LX/0Ot;LX/Hvk;LX/HvX;LX/0tO;LX/HvU;LX/0Ot;LX/Hv4;LX/0Ot;LX/0Ot;LX/APn;LX/APq;LX/Hvt;LX/Hv8;LX/1Kj;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Xl;LX/3iT;LX/1Ck;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0gd;LX/73w;LX/3l1;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/Hsp;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/ARD;LX/AR7;LX/AR4;LX/AR2;LX/Hq5;LX/ARW;LX/Hu7;LX/AQ2;LX/Htm;LX/Huo;LX/HuB;LX/Hux;LX/Huk;LX/Hus;LX/HtZ;LX/Htg;LX/Hv6;LX/Htr;LX/HuM;LX/HuS;LX/Htx;LX/HsZ;LX/HuY;LX/Hv0;LX/Hs8;LX/Hrl;LX/APe;LX/74n;LX/APg;LX/0Ot;LX/HwU;LX/0Ot;LX/ATP;LX/0Or;LX/CfR;LX/0Ot;LX/0Ot;LX/HtR;LX/Hsy;LX/HtC;LX/0Ot;LX/HuH;LX/HsM;LX/HuV;LX/0Ot;LX/11u;LX/121;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/activity/ComposerFragment;ILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2513359
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 2513360
    :goto_0
    return-void

    .line 2513361
    :cond_0
    const-string v0, "extra_composer_target_data"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2513362
    const-string v1, "extra_composer_page_data"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2513363
    invoke-static {p0, v0, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/intent/ComposerPageData;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;ZZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 2513364
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 2513365
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2513366
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Ljava/lang/Object;

    .line 2513367
    :cond_0
    invoke-virtual {v0, p1}, LX/0jL;->c(LX/0Px;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2513368
    if-eqz p2, :cond_1

    .line 2513369
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->r(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2513370
    invoke-static {p0, p3}, Lcom/facebook/composer/activity/ComposerFragment;->c$redex0(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 2513371
    :cond_1
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->O$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2513372
    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/93G;)V
    .locals 9

    .prologue
    .line 2513373
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->S:LX/926;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    sget-object v2, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2513374
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0j0;

    check-cast v4, LX/0j8;

    invoke-interface {v4}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v4

    .line 2513375
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bw_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2513376
    invoke-static {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v4

    .line 2513377
    new-instance v5, LX/5m9;

    invoke-direct {v5}, LX/5m9;-><init>()V

    .line 2513378
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    iput-object v6, v5, LX/5m9;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2513379
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->o()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v6

    iput-object v6, v5, LX/5m9;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    .line 2513380
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->p()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v6

    iput-object v6, v5, LX/5m9;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    .line 2513381
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->e()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5m9;->d:Ljava/lang/String;

    .line 2513382
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->q()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v6

    iput-object v6, v5, LX/5m9;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    .line 2513383
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5m9;->f:Ljava/lang/String;

    .line 2513384
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->r()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v6

    iput-object v6, v5, LX/5m9;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    .line 2513385
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5m9;->h:Ljava/lang/String;

    .line 2513386
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->s()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v6

    iput-object v6, v5, LX/5m9;->i:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    .line 2513387
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->m()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5m9;->j:Ljava/lang/String;

    .line 2513388
    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->n()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v6

    iput-object v6, v5, LX/5m9;->k:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 2513389
    move-object v4, v5

    .line 2513390
    const/4 v5, 0x0

    .line 2513391
    iput-object v5, v4, LX/5m9;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    .line 2513392
    move-object v4, v4

    .line 2513393
    invoke-virtual {v4}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v6

    move-object v4, v1

    .line 2513394
    check-cast v4, LX/0im;

    invoke-interface {v4}, LX/0im;->c()LX/0jJ;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v4

    check-cast v4, LX/0jL;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0j0;

    check-cast v5, LX/0j8;

    invoke-interface {v5}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/5RO;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v5

    invoke-virtual {v5}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0jL;

    invoke-virtual {v4}, LX/0jL;->a()V

    move-object v5, v6

    .line 2513395
    :goto_0
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0j0;

    check-cast v4, LX/0ip;

    invoke-interface {v4}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v6

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0j0;

    invoke-interface {v4}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0j0;

    check-cast v4, LX/0j3;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-static {v5}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v5

    const/4 v8, 0x0

    invoke-static {v6, v7, v4, v5, v8}, LX/938;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Z)LX/937;

    move-result-object v4

    .line 2513396
    iput-object p1, v4, LX/937;->m:LX/93G;

    .line 2513397
    move-object v4, v4

    .line 2513398
    invoke-virtual {v4}, LX/937;->a()Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;

    move-result-object v4

    .line 2513399
    invoke-virtual {v0, v4, v3}, LX/926;->a(Lcom/facebook/composer/minutiae/util/MinutiaeConfiguration;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    .line 2513400
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0j0;

    check-cast v4, LX/0j4;

    invoke-interface {v4}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v4

    invoke-static {v4}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2513401
    const-string v6, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0j0;

    check-cast v4, LX/0j4;

    invoke-interface {v4}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2513402
    :cond_0
    move-object v1, v5

    .line 2513403
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2513404
    return-void

    :cond_1
    move-object v5, v4

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V
    .locals 2

    .prologue
    .line 2513405
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2513406
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p1}, LX/0jL;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2513407
    :goto_0
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v0

    invoke-interface {v0, p1}, LX/CfL;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 2513408
    return-void

    .line 2513409
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p1}, LX/0jL;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V
    .locals 8
    .param p0    # Lcom/facebook/composer/activity/ComposerFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2511775
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bL:Z

    .line 2511776
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->am:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8RJ;

    sget-object v1, LX/8RI;->COMPOSER_FRAGMENT:LX/8RI;

    invoke-virtual {v0, v1}, LX/8RJ;->a(LX/8RI;)V

    .line 2511777
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bK:LX/ARb;

    new-instance v1, LX/Hqx;

    invoke-direct {v1, p0, p1}, LX/Hqx;-><init>(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    const/16 v5, 0x80

    .line 2511778
    iget-object v2, v0, LX/ARb;->d:LX/7mI;

    invoke-interface {v2}, LX/7mI;->a()LX/03R;

    move-result-object v2

    sget-object v3, LX/03R;->UNSET:LX/03R;

    if-eq v2, v3, :cond_0

    .line 2511779
    invoke-virtual {v1}, LX/Hqx;->a()V

    .line 2511780
    :goto_0
    return-void

    .line 2511781
    :cond_0
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2511782
    const-string v2, "adminedPagesPrefetchParams"

    new-instance v3, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Params;

    invoke-direct {v3, v5}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchMethod$Params;-><init>(I)V

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2511783
    const-string v2, "pagesAccessTokenPrefetchParams"

    new-instance v3, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;

    invoke-direct {v3, v5}, Lcom/facebook/pages/adminedpages/protocol/PagesAccessTokenPrefetchMethod$Params;-><init>(I)V

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2511784
    const-string v2, "loadAdminedPagesParam"

    new-instance v3, Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-direct {v3, v5}, Lcom/facebook/pages/adminedpages/service/LoadAdminedPagesParams;-><init>(LX/0rS;)V

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2511785
    iget-object v2, v0, LX/ARb;->a:LX/0aG;

    const-string v3, "admined_pages_prefetch"

    sget-object v5, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v6, Lcom/facebook/pages/adminedpages/backgroundtasks/AdminedPagesPrefetchBackgroundTask;

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    const v7, -0xd03a77b

    invoke-static/range {v2 .. v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    .line 2511786
    iget-object v3, v0, LX/ARb;->b:LX/0Sh;

    new-instance v4, LX/ARa;

    invoke-direct {v4, v0, v1}, LX/ARa;-><init>(LX/ARb;LX/Hqx;)V

    invoke-virtual {v3, v2, v4}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 2

    .prologue
    .line 2513410
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p1}, LX/0jL;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2513411
    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/intent/ComposerPageData;)V
    .locals 4
    .param p1    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2513222
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p1}, LX/0jL;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p2}, LX/0jL;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2513223
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-wide v2, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, LX/CfL;->a(Ljava/lang/Long;)V

    .line 2513224
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->J(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2513225
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v1}, LX/2zG;->t()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/ATO;->c(Z)V

    .line 2513226
    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2513200
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_2

    move v2, v3

    .line 2513201
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object v1

    .line 2513202
    iput-boolean v4, v1, LX/5RO;->b:Z

    .line 2513203
    move-object v1, v1

    .line 2513204
    invoke-virtual {v1, p1, p2}, LX/5RO;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;)LX/5RO;

    move-result-object v1

    invoke-virtual {v1}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2513205
    if-eqz p3, :cond_0

    .line 2513206
    invoke-static {p0, p3}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 2513207
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2513208
    :goto_1
    if-eqz v2, :cond_1

    if-nez v3, :cond_1

    .line 2513209
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_REMOVE:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2513210
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2513211
    return-void

    :cond_2
    move v2, v4

    .line 2513212
    goto :goto_0

    :cond_3
    move v3, v4

    .line 2513213
    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 2513158
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v1

    .line 2513159
    new-instance v0, LX/7lP;

    invoke-direct {v0, v1}, LX/7lP;-><init>(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 2513160
    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-eqz v1, :cond_0

    .line 2513161
    invoke-virtual {v0, v2}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    .line 2513162
    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-static {p0, v1, v2}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/intent/ComposerPageData;)V

    .line 2513163
    :cond_0
    invoke-virtual {v0, p1}, LX/7lP;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    .line 2513164
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->cP:LX/93q;

    invoke-interface {v1, v0, v7}, LX/93q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)V

    .line 2513165
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v8, v0

    .line 2513166
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_CHANGE_PRIVACY:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-wide v4, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/0gd;->a(LX/0ge;Ljava/lang/String;JLX/2rt;)V

    .line 2513167
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aK(Lcom/facebook/composer/activity/ComposerFragment;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v7

    .line 2513168
    :goto_0
    iget-boolean v1, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v1, v1

    .line 2513169
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/Hrk;->a(ZZLcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;)LX/AP5;

    move-result-object v1

    .line 2513170
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v0

    sget-object v2, LX/2rt;->SHARE:LX/2rt;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2513171
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a()LX/AP3;

    move-result-object v0

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v2

    .line 2513172
    iput-object v2, v0, LX/AP3;->c:Ljava/lang/String;

    .line 2513173
    move-object v0, v0

    .line 2513174
    iput-object v1, v0, LX/AP3;->d:LX/AP5;

    .line 2513175
    move-object v0, v0

    .line 2513176
    invoke-virtual {v0}, LX/AP3;->a()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v1

    .line 2513177
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v2, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2513178
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->k(Lcom/facebook/composer/activity/ComposerFragment;)LX/HqE;

    move-result-object v0

    .line 2513179
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/HqE;->b:Z

    .line 2513180
    invoke-virtual {v0, v8}, LX/HqE;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2513181
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hrk;

    .line 2513182
    iget-boolean v1, v0, LX/Hrk;->e:Z

    if-nez v1, :cond_5

    .line 2513183
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->f()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2513184
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->J(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2513185
    :cond_3
    return-void

    .line 2513186
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2513187
    :cond_5
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Hrk;->e:Z

    .line 2513188
    iget-object v1, v0, LX/Hrk;->i:LX/8Ps;

    sget-object v2, LX/5nc;->CHOSE_OTHER_OPTION:LX/5nc;

    iget-object v3, v0, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    iget-object v3, v3, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mTriggerPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const-string v4, "traditional_composer"

    invoke-virtual {v1, v2, v3, v8, v4}, LX/8Ps;->a(LX/5nc;LX/1oU;LX/1oU;Ljava/lang/String;)V

    .line 2513189
    invoke-static {v0}, LX/Hrk;->d(LX/Hrk;)LX/HqM;

    move-result-object v1

    .line 2513190
    iget-object v2, v1, LX/HqM;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a()LX/AP3;

    move-result-object v2

    sget-object v3, LX/2by;->INLINE_PRIVACY_SURVEY:LX/2by;

    .line 2513191
    iput-object v3, v2, LX/AP3;->e:LX/2by;

    .line 2513192
    move-object v2, v2

    .line 2513193
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v3

    .line 2513194
    iput-object v3, v2, LX/AP3;->c:Ljava/lang/String;

    .line 2513195
    move-object v2, v2

    .line 2513196
    invoke-virtual {v2}, LX/AP3;->a()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v3

    .line 2513197
    iget-object v2, v1, LX/HqM;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v4, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v2, v4}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v2

    check-cast v2, LX/0jL;

    invoke-virtual {v2, v3}, LX/0jL;->a(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jL;

    invoke-virtual {v2}, LX/0jL;->a()V

    .line 2513198
    iget-object v2, v1, LX/HqM;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    sget-object v3, LX/5L2;->ON_PRIVACY_CHANGE_FROM_INLINE_PRIVACY_SURVEY:LX/5L2;

    invoke-virtual {v2, v3}, LX/HvN;->a(LX/5L2;)V

    .line 2513199
    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Z)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 2513144
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    invoke-virtual {v0, v2}, LX/ASX;->a(Z)V

    .line 2513145
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0, v2}, LX/0jL;->f(Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2513146
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->G()V

    .line 2513147
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->clearFocus()V

    .line 2513148
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aZ:LX/Hq4;

    if-nez v0, :cond_0

    .line 2513149
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aY:LX/Hq5;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cN:LX/HrE;

    .line 2513150
    new-instance v3, LX/Hq4;

    move-object v4, v1

    check-cast v4, LX/0il;

    const-class v5, LX/Hq8;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Hq8;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v8

    check-cast v8, LX/0gd;

    move-object v5, v2

    invoke-direct/range {v3 .. v8}, LX/Hq4;-><init>(LX/0il;LX/HrE;LX/Hq8;Landroid/content/Context;LX/0gd;)V

    .line 2513151
    move-object v0, v3

    .line 2513152
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aZ:LX/Hq4;

    .line 2513153
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aZ:LX/Hq4;

    invoke-virtual {v0, v1}, LX/HvN;->a(LX/0iK;)V

    .line 2513154
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aM:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/activity/PostCompositionView;

    invoke-virtual {v0}, Lcom/facebook/composer/activity/PostCompositionView;->bringToFront()V

    .line 2513155
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aM:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/activity/PostCompositionView;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aZ:LX/Hq4;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/composer/activity/PostCompositionView;->a(LX/Hq4;Z)V

    .line 2513156
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aL(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2513157
    return-void
.end method

.method public static aA(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 3

    .prologue
    .line 2513132
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92E;

    .line 2513133
    const v1, 0x430001

    const-string v2, "minutiae_verb_picker_time_to_init"

    invoke-virtual {v0, v1, v2}, LX/92A;->c(ILjava/lang/String;)V

    .line 2513134
    const v1, 0x430003

    const-string v2, "minutiae_verb_picker_time_to_fetch_end"

    invoke-virtual {v0, v1, v2}, LX/92A;->c(ILjava/lang/String;)V

    .line 2513135
    const v1, 0x43000b

    const-string v2, "minutiae_verb_picker_time_to_verbs_shown"

    invoke-virtual {v0, v1, v2}, LX/92A;->c(ILjava/lang/String;)V

    .line 2513136
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/92B;

    .line 2513137
    const v1, 0xc50001

    const-string v2, "minutiae_feelings_selector_time_to_init"

    invoke-virtual {v0, v1, v2}, LX/92A;->c(ILjava/lang/String;)V

    .line 2513138
    const v1, 0xc50002

    const-string v2, "minutiae_feelings_selector_time_to_fetch_start"

    invoke-virtual {v0, v1, v2}, LX/92A;->c(ILjava/lang/String;)V

    .line 2513139
    const v1, 0xc50003

    const-string v2, "minutiae_feelings_selector_time_to_fetch_end"

    invoke-virtual {v0, v1, v2}, LX/92A;->c(ILjava/lang/String;)V

    .line 2513140
    const v1, 0xc50004

    const-string v2, "minutiae_feelings_selector_time_to_results_shown"

    invoke-virtual {v0, v1, v2}, LX/92A;->c(ILjava/lang/String;)V

    .line 2513141
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_MINUTIAE_CLICK:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2513142
    sget-object v0, LX/93G;->FEELINGS_TAB:LX/93G;

    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/93G;)V

    .line 2513143
    return-void
.end method

.method private aD()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2513131
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ax:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7l0;

    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aO()J

    move-result-wide v4

    invoke-static {v1, v2, v0, v4, v5}, LX/7ky;->a(LX/0Px;LX/0Px;LX/7l0;J)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static aE(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 6

    .prologue
    .line 2513116
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-static {v0}, LX/HwS;->a(LX/0il;)LX/8AA;

    move-result-object v1

    .line 2513117
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    .line 2513118
    iget-object v2, v0, LX/2zG;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/APD;

    invoke-virtual {v0}, LX/2zG;->t()Z

    move-result v3

    invoke-static {v0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v4

    invoke-interface {v4}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-static {v0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v5

    invoke-interface {v5}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v5

    invoke-static {v5}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, LX/APD;->a(ZLX/2rw;Z)Z

    move-result v2

    move v0, v2

    .line 2513119
    if-nez v0, :cond_0

    .line 2513120
    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    .line 2513121
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getShouldPickerSupportLiveCamera()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2513122
    invoke-virtual {v1}, LX/8AA;->e()LX/8AA;

    .line 2513123
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2513124
    const/4 v0, 0x3

    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/8AA;->a(II)LX/8AA;

    .line 2513125
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5RE;->I()LX/5RF;

    move-result-object v0

    sget-object v2, LX/5RF;->SLIDESHOW:LX/5RF;

    if-ne v0, v2, :cond_3

    .line 2513126
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, LX/8AA;->a(Z)LX/8AA;

    .line 2513127
    :cond_3
    move-object v0, v1

    .line 2513128
    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2513129
    const/16 v1, 0x7f

    invoke-static {p0, v1, v0}, Lcom/facebook/composer/activity/ComposerFragment;->r(Lcom/facebook/composer/activity/ComposerFragment;ILandroid/content/Intent;)V

    .line 2513130
    return-void
.end method

.method public static aJ(Lcom/facebook/composer/activity/ComposerFragment;)Z
    .locals 1

    .prologue
    .line 2513115
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aK(Lcom/facebook/composer/activity/ComposerFragment;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static aK(Lcom/facebook/composer/activity/ComposerFragment;)LX/0Rf;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2513114
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aO()J

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->ax:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7l0;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/7ky;->a(JLX/7l0;LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public static aL(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 10

    .prologue
    .line 2513054
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 2513055
    if-eqz v0, :cond_0

    .line 2513056
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->k(Lcom/facebook/composer/activity/ComposerFragment;)LX/HqE;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->bP:LX/Hr8;

    const/4 v5, 0x1

    .line 2513057
    iget-object v3, v0, LX/HqE;->c:LX/339;

    .line 2513058
    iget-boolean v6, v3, LX/339;->g:Z

    move v3, v6

    .line 2513059
    if-eqz v3, :cond_1

    .line 2513060
    :cond_0
    :goto_0
    return-void

    .line 2513061
    :cond_1
    iget-object v3, v0, LX/HqE;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/HqT;

    .line 2513062
    if-nez v3, :cond_2

    .line 2513063
    goto :goto_0

    .line 2513064
    :cond_2
    const/4 v7, 0x0

    .line 2513065
    const/4 v8, 0x0

    .line 2513066
    iget-object v6, v0, LX/HqE;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/HqT;

    .line 2513067
    if-nez v6, :cond_a

    move v6, v8

    .line 2513068
    :goto_1
    move v6, v6

    .line 2513069
    if-nez v6, :cond_8

    move-object v6, v7

    .line 2513070
    :cond_3
    :goto_2
    move-object v6, v6

    .line 2513071
    if-eqz v6, :cond_7

    .line 2513072
    iget-object v7, v0, LX/HqE;->c:LX/339;

    invoke-virtual {v3}, LX/HqT;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/339;->a(Ljava/lang/String;)V

    .line 2513073
    iget-object v7, v0, LX/HqE;->c:LX/339;

    invoke-virtual {v3}, LX/HqT;->e()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2513074
    iget-object v8, v3, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-virtual {v8}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v8

    iput-object v8, v7, LX/339;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2513075
    iget-object v8, v3, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v8, v9}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v8

    iput-object v8, v7, LX/339;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2513076
    iget-object v8, v3, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ONLY_ME:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v8, v9}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v8

    iput-object v8, v7, LX/339;->l:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2513077
    iget-object v8, v7, LX/339;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v8, :cond_4

    iget-object v8, v7, LX/339;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v8, :cond_4

    iget-object v8, v7, LX/339;->l:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-nez v8, :cond_e

    .line 2513078
    :cond_4
    iget-object v8, v7, LX/339;->f:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/03V;

    const-string v9, "audience_educator_manager_save_suggested"

    const-string p0, "Can\'t find option of type widest, friends, or only me."

    invoke-virtual {v8, v9, p0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2513079
    const/4 v8, 0x0

    .line 2513080
    :goto_3
    move v3, v8

    .line 2513081
    if-nez v3, :cond_5

    .line 2513082
    goto :goto_0

    .line 2513083
    :cond_5
    new-instance v3, Landroid/content/Intent;

    invoke-interface {v6}, LX/HqA;->c()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2513084
    invoke-interface {v6}, LX/HqA;->b()LX/2by;

    move-result-object v4

    iget-object v7, v0, LX/HqE;->c:LX/339;

    invoke-interface {v6}, LX/HqA;->b()LX/2by;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/339;->c(LX/2by;)Z

    move-result v7

    .line 2513085
    new-instance v9, LX/AP3;

    iget-object v8, v2, LX/Hr8;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v8, v8, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v8}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v8}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v8

    invoke-direct {v9, v8}, LX/AP3;-><init>(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)V

    .line 2513086
    iput-object v4, v9, LX/AP3;->e:LX/2by;

    .line 2513087
    move-object v8, v9

    .line 2513088
    iput-boolean v7, v8, LX/AP3;->h:Z

    .line 2513089
    move-object v8, v8

    .line 2513090
    invoke-virtual {v8}, LX/AP3;->a()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v9

    .line 2513091
    iget-object v8, v2, LX/Hr8;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v8, v8, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object p0, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v8, p0}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v8

    check-cast v8, LX/0jL;

    invoke-virtual {v8, v9}, LX/0jL;->a(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0jL;

    invoke-virtual {v8}, LX/0jL;->a()V

    .line 2513092
    iget-object v4, v0, LX/HqE;->c:LX/339;

    .line 2513093
    iput-boolean v5, v4, LX/339;->g:Z

    .line 2513094
    const-string v4, "extra_audience_educator_type"

    invoke-interface {v6}, LX/HqA;->b()LX/2by;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2513095
    const-string v4, "audience_educator_source_extra"

    const-string v7, "traditional_composer"

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2513096
    iget-object v4, v0, LX/HqE;->c:LX/339;

    invoke-interface {v6}, LX/HqA;->b()LX/2by;

    move-result-object v6

    .line 2513097
    iget-object v7, v4, LX/339;->m:LX/0P1;

    invoke-virtual {v7, v6}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/2c0;

    .line 2513098
    if-nez v7, :cond_6

    .line 2513099
    iget-object v7, v4, LX/339;->f:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/03V;

    const-string v8, "audience_educator_manager_activity_launch"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string p0, "Adding data to intent for unrecognized educator type: "

    invoke-direct {v9, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, LX/2by;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2513100
    :cond_6
    iget-object v4, v2, LX/Hr8;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v4, v4, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    const/4 v6, 0x7

    iget-object v7, v2, LX/Hr8;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-interface {v4, v3, v6, v7}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2513101
    iget-object v4, v2, LX/Hr8;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-virtual {v4}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    const/high16 v6, 0x10a0000

    const v7, 0x10a0001

    invoke-virtual {v4, v6, v7}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 2513102
    goto/16 :goto_0

    .line 2513103
    :cond_7
    goto/16 :goto_0

    .line 2513104
    :cond_8
    iget-object v6, v0, LX/HqE;->o:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v9

    const/4 v6, 0x0

    move v8, v6

    :goto_4
    if-ge v8, v9, :cond_9

    iget-object v6, v0, LX/HqE;->o:LX/0Px;

    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/HqA;

    .line 2513105
    invoke-interface {v6}, LX/HqA;->a()Z

    move-result p0

    if-nez p0, :cond_3

    .line 2513106
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_4

    :cond_9
    move-object v6, v7

    .line 2513107
    goto/16 :goto_2

    :cond_a
    iget-object v9, v0, LX/HqE;->d:LX/0kb;

    invoke-virtual {v9}, LX/0kb;->d()Z

    move-result v9

    if-eqz v9, :cond_c

    iget-object v9, v0, LX/HqE;->c:LX/339;

    invoke-virtual {v6}, LX/HqT;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v9, p0}, LX/339;->b(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_c

    const/4 v9, 0x0

    .line 2513108
    invoke-virtual {v6}, LX/HqT;->d()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object p0

    .line 2513109
    if-nez p0, :cond_d

    .line 2513110
    :cond_b
    :goto_5
    move v6, v9

    .line 2513111
    if-eqz v6, :cond_c

    invoke-static {v0}, LX/HqE;->e(LX/HqE;)Z

    move-result v6

    if-eqz v6, :cond_c

    const/4 v6, 0x1

    goto/16 :goto_1

    :cond_c
    move v6, v8

    goto/16 :goto_1

    .line 2513112
    :cond_d
    iget-object p0, v6, LX/HqT;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object p0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {p0}, LX/0it;->d()Z

    move-result p0

    move p0, p0

    .line 2513113
    if-nez p0, :cond_b

    const/4 v9, 0x1

    goto :goto_5

    :cond_e
    const/4 v8, 0x1

    goto/16 :goto_3
.end method

.method private aN()V
    .locals 4

    .prologue
    .line 2513051
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2513052
    :cond_0
    :goto_0
    return-void

    .line 2513053
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->N:LX/BM1;

    const-string v2, "on_composer_post_published"

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/BM1;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private aO()J
    .locals 2

    .prologue
    .line 2513048
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2513049
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2513050
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/ATO",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "LX/Hre;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2513028
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bI:LX/ATO;

    if-nez v0, :cond_1

    .line 2513029
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bH:LX/ATP;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aJ:Lcom/facebook/widget/ScrollingAwareScrollView;

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    const v5, 0x7f0d0ae5

    invoke-virtual {v4, v5}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iget-boolean v5, p0, Lcom/facebook/composer/activity/ComposerFragment;->ai:Z

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, LX/ATP;->a(LX/0gc;Lcom/facebook/widget/ScrollingAwareScrollView;Landroid/widget/LinearLayout;ZZLjava/lang/Object;Z)LX/ATO;

    move-result-object v2

    .line 2513030
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-virtual {v1, v2}, LX/HvN;->a(LX/0iK;)V

    .line 2513031
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v1}, LX/2zG;->t()Z

    move-result v1

    invoke-virtual {v2, v1}, LX/ATO;->c(Z)V

    .line 2513032
    new-instance v1, LX/HrU;

    invoke-direct {v1, p0}, LX/HrU;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    move-object v1, v1

    .line 2513033
    iput-object v1, v2, LX/ATO;->B:LX/ASe;

    .line 2513034
    new-instance v1, LX/HrV;

    invoke-direct {v1, p0}, LX/HrV;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    move-object v1, v1

    .line 2513035
    iput-object v1, v2, LX/ATO;->C:LX/7yZ;

    .line 2513036
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->h:LX/Hqr;

    .line 2513037
    iput-object v1, v2, LX/ATO;->M:LX/Hqr;

    .line 2513038
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->s()LX/0P1;

    move-result-object v1

    invoke-virtual {v1}, LX/0P1;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2513039
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->s()LX/0P1;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/ATO;->a(LX/0P1;)V

    .line 2513040
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bY:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ATQ;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aC:LX/73w;

    .line 2513041
    iget-object v4, v3, LX/73w;->j:Ljava/lang/String;

    move-object v3, v4

    .line 2513042
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2513043
    iget-object v4, v1, LX/ATQ;->a:LX/73w;

    invoke-virtual {v4, v3}, LX/73w;->a(Ljava/lang/String;)V

    .line 2513044
    move-object v0, v2

    .line 2513045
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bI:LX/ATO;

    .line 2513046
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bI:LX/ATO;

    return-object v0

    .line 2513047
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private aa()Lcom/facebook/ui/dialogs/FbDialogFragment;
    .locals 2

    .prologue
    .line 2512787
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2512788
    const-string v1, "AUDIENCE_FRAGMENT_TAG"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/dialogs/FbDialogFragment;

    return-object v0
.end method

.method public static ab$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 3

    .prologue
    .line 2513015
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    if-nez v0, :cond_1

    .line 2513016
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aG:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "composer_keyboard"

    const-string v2, "Tip Manager not available"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2513017
    :cond_0
    :goto_0
    return-void

    .line 2513018
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2513019
    iget-object v1, v0, LX/AQ9;->E:LX/ARN;

    move-object v0, v1

    .line 2513020
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    invoke-virtual {v0}, LX/ASX;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    invoke-static {v0}, LX/9J0;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 2513021
    :goto_1
    if-eqz v0, :cond_0

    .line 2513022
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    invoke-virtual {v0}, LX/AQ9;->b()V

    .line 2513023
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->b()V

    .line 2513024
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->a()V

    goto :goto_0

    .line 2513025
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2513026
    iget-object v1, v0, LX/AQ9;->E:LX/ARN;

    move-object v0, v1

    .line 2513027
    invoke-interface {v0}, LX/ARN;->a()Z

    move-result v0

    goto :goto_1
.end method

.method public static ac(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 3

    .prologue
    .line 2511285
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2511286
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object v1

    const/4 v2, 0x0

    .line 2511287
    iput-boolean v2, v1, LX/5RO;->d:Z

    .line 2511288
    move-object v1, v1

    .line 2511289
    invoke-virtual {v1}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511290
    return-void
.end method

.method private ae()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2511769
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2511770
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 2511771
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2511772
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/2addr v0, v1

    .line 2511773
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2511774
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static ag(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 14

    .prologue
    .line 2511688
    const/4 v5, 0x0

    .line 2511689
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aK(Lcom/facebook/composer/activity/ComposerFragment;)LX/0Rf;

    move-result-object v7

    .line 2511690
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2511691
    iget-object v6, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, v6

    .line 2511692
    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2511693
    iget-object v6, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, v6

    .line 2511694
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v7}, LX/0Rf;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_d

    :cond_0
    move v4, v5

    .line 2511695
    :goto_0
    move v0, v4

    .line 2511696
    if-eqz v0, :cond_2

    .line 2511697
    :cond_1
    :goto_1
    return-void

    .line 2511698
    :cond_2
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->ao()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2511699
    const/4 v1, 0x0

    .line 2511700
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EV;

    invoke-virtual {v0}, LX/1EV;->d()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aw:LX/0ad;

    sget-short v2, LX/DIp;->h:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2511701
    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSavedSessionLoadAttempts()I

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aT:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJ4;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    const/4 v3, 0x0

    .line 2511702
    iget-object v4, v0, LX/DJ4;->b:LX/0Uh;

    sget v5, LX/7l1;->j:I

    invoke-virtual {v4, v5, v3}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getCurrencyCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 2511703
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 2511704
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getCurrencyCode()Ljava/lang/String;

    move-result-object v5

    .line 2511705
    invoke-static {v5}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v6

    .line 2511706
    iget-object v7, v0, LX/DJ4;->c:LX/0W9;

    invoke-virtual {v7}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Currency;->getSymbol(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 2511707
    invoke-static {v5, v4}, LX/DJ4;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 2511708
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getInterceptWords()LX/0Px;

    move-result-object v6

    .line 2511709
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2511710
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 2511711
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v10

    const/4 v7, 0x0

    move v8, v7

    :goto_3
    if-ge v8, v10, :cond_4

    invoke-virtual {v6, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 2511712
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\\b"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\\b"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 2511713
    invoke-virtual {v0, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 2511714
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2511715
    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2511716
    :cond_3
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_3

    .line 2511717
    :cond_4
    move-object v6, v9

    .line 2511718
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getCommerceInfo()Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/ipc/composer/model/ComposerCommerceInfo;->getInterceptWordsAfterNumber()LX/0Px;

    move-result-object v7

    .line 2511719
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2511720
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 2511721
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v0

    const/4 v8, 0x0

    move v9, v8

    :goto_4
    if-ge v9, v0, :cond_6

    invoke-virtual {v7, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 2511722
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "\\b\\d+\\s?"

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "\\b"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x2

    invoke-static {v2, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 2511723
    invoke-virtual {v2, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 2511724
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2511725
    invoke-interface {v10, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2511726
    :cond_5
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_4

    .line 2511727
    :cond_6
    move-object v4, v10

    .line 2511728
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_8

    .line 2511729
    :cond_7
    const/4 v3, 0x1

    .line 2511730
    :cond_8
    move v0, v3

    .line 2511731
    if-nez v0, :cond_13

    .line 2511732
    :cond_9
    const/4 v0, 0x0

    .line 2511733
    :goto_5
    move v0, v0

    .line 2511734
    if-nez v0, :cond_1

    .line 2511735
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v4

    sget-object v5, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    if-ne v4, v5, :cond_a

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v4

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->ad:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v8

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->bW:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0kL;

    invoke-static {v6, v7, v8, v9, v4}, LX/Hw8;->a(JJLX/0kL;)Z

    move-result v4

    if-eqz v4, :cond_15

    :cond_a
    const/4 v4, 0x1

    :goto_6
    move v0, v4

    .line 2511736
    if-eqz v0, :cond_1

    .line 2511737
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hst;

    .line 2511738
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    new-instance v3, Lcom/facebook/composer/activity/ComposerFragment$60;

    invoke-direct {v3, p0}, Lcom/facebook/composer/activity/ComposerFragment$60;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {v0, v2, v1, v3}, LX/Hst;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Lcom/facebook/ipc/composer/intent/ComposerShareParams;Ljava/lang/Runnable;)LX/2EJ;

    move-result-object v0

    .line 2511739
    if-eqz v0, :cond_b

    .line 2511740
    invoke-virtual {v0}, LX/2EJ;->show()V

    goto/16 :goto_1

    .line 2511741
    :cond_b
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    invoke-virtual {v0}, LX/AQ9;->C()LX/ARN;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    invoke-virtual {v0}, LX/AQ9;->C()LX/ARN;

    move-result-object v0

    invoke-interface {v0}, LX/ARN;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2511742
    :cond_c
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->ak(Lcom/facebook/composer/activity/ComposerFragment;)V

    goto/16 :goto_1

    .line 2511743
    :cond_d
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2511744
    iget-object v6, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, v6

    .line 2511745
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v8

    .line 2511746
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    .line 2511747
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v10

    move v6, v5

    :goto_7
    if-ge v6, v10, :cond_f

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 2511748
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v7, v11}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 2511749
    invoke-virtual {v9, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2511750
    :cond_e
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_7

    .line 2511751
    :cond_f
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 2511752
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_10

    .line 2511753
    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2511754
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    if-ne v5, v9, :cond_11

    .line 2511755
    const v6, 0x7f08148b

    new-array v7, v9, [Ljava/lang/Object;

    invoke-virtual {v4, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v8

    invoke-virtual {p0, v6, v7}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2511756
    :goto_8
    new-instance v6, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v5

    invoke-virtual {v5, v9}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v5

    const v6, 0x7f080016

    invoke-virtual {p0, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, LX/HrS;

    invoke-direct {v7, p0}, LX/HrS;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {v5, v6, v7}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    const v6, 0x7f080017

    invoke-virtual {p0, v6}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, LX/HrR;

    invoke-direct {v7, p0}, LX/HrR;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {v5, v6, v7}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    invoke-virtual {v5}, LX/0ju;->a()LX/2EJ;

    move-result-object v5

    invoke-virtual {v5}, LX/2EJ;->show()V

    .line 2511757
    const/4 v4, 0x1

    goto/16 :goto_0

    :cond_10
    move v4, v5

    .line 2511758
    goto/16 :goto_0

    .line 2511759
    :cond_11
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    if-ne v5, v7, :cond_12

    .line 2511760
    const v6, 0x7f08148c

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v4, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v8

    invoke-virtual {v4, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v9

    invoke-virtual {p0, v6, v7}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_8

    .line 2511761
    :cond_12
    const v6, 0x7f08148d

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v4, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v8

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v9

    invoke-virtual {p0, v6, v7}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_8

    .line 2511762
    :cond_13
    new-instance v0, Lcom/facebook/groupcommerce/ui/GroupsSalePostInterceptDialogFragment;

    invoke-direct {v0}, Lcom/facebook/groupcommerce/ui/GroupsSalePostInterceptDialogFragment;-><init>()V

    .line 2511763
    new-instance v1, LX/HrK;

    invoke-direct {v1, p0}, LX/HrK;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2511764
    iput-object v1, v0, Lcom/facebook/groupcommerce/ui/GroupsSalePostInterceptDialogFragment;->m:Landroid/content/DialogInterface$OnClickListener;

    .line 2511765
    new-instance v1, LX/HrL;

    invoke-direct {v1, p0}, LX/HrL;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2511766
    iput-object v1, v0, Lcom/facebook/groupcommerce/ui/GroupsSalePostInterceptDialogFragment;->n:Landroid/content/DialogInterface$OnClickListener;

    .line 2511767
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "FOR_SALE_INTERCEPT_DIALOG"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2511768
    const/4 v0, 0x1

    goto/16 :goto_5

    :cond_14
    move v0, v1

    goto/16 :goto_2

    :cond_15
    const/4 v4, 0x0

    goto/16 :goto_6
.end method

.method public static ak(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 20

    .prologue
    .line 2511634
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    invoke-virtual {v2}, LX/1Kj;->c()V

    .line 2511635
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2511636
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v4, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v3, v4}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v3

    check-cast v3, LX/0jL;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0ir;->getComposerSessionLoggingData()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->h()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->ad:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v10

    invoke-static {v6, v8, v9, v10, v11}, LX/5ME;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;JJ)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->setCompositionDuration(J)Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData$Builder;->a()Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0jL;->a(Lcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0jL;

    invoke-virtual {v3}, LX/0jL;->a()V

    .line 2511637
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-static {v3}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v3

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v13

    .line 2511638
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-nez v2, :cond_a

    :cond_0
    const/16 v17, 0x0

    .line 2511639
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v5

    iget-object v5, v5, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v6}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v6

    iget-wide v6, v6, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/composer/activity/ComposerFragment;->av:LX/1Ck;

    invoke-virtual {v8}, LX/1Ck;->b()LX/1M1;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/composer/activity/ComposerFragment;->bC:LX/APf;

    invoke-virtual {v9}, LX/APf;->a()LX/0P1;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v10}, LX/5RE;->I()LX/5RF;

    move-result-object v10

    invoke-virtual {v10}, LX/5RF;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {p0 .. p0}, Lcom/facebook/composer/activity/ComposerFragment;->ae()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v12}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v12}, LX/0jB;->getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v12

    if-eqz v12, :cond_b

    const/4 v12, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v14}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v14}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v14

    invoke-virtual {v14}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v14

    invoke-static/range {p0 .. p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v15

    invoke-virtual {v15}, LX/ATO;->b()Ljava/lang/String;

    move-result-object v15

    invoke-static/range {p0 .. p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, LX/ATO;->c()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->T:LX/Azb;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface/range {v18 .. v18}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/Azb;->a(LX/0Px;)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v18

    invoke-virtual/range {v2 .. v18}, LX/0gd;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;LX/2rw;JLX/1M1;LX/0P1;Ljava/lang/String;IZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;)V

    .line 2511640
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->cc:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9c3;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-virtual {v2, v4, v3}, LX/9c3;->b(Ljava/lang/String;I)V

    .line 2511641
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v2}, LX/5RE;->I()LX/5RF;

    move-result-object v2

    sget-object v3, LX/5RF;->SLIDESHOW:LX/5RF;

    if-ne v2, v3, :cond_1

    .line 2511642
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->aD:LX/3l1;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v3, v4, v2}, LX/3l1;->a(Ljava/lang/String;I)V

    .line 2511643
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    sget-object v3, LX/5L2;->ON_USER_POST:LX/5L2;

    invoke-virtual {v2, v3}, LX/HvN;->a(LX/5L2;)V

    .line 2511644
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7kq;->j(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2511645
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->aC:LX/73w;

    invoke-virtual {v2}, LX/73w;->c()V

    .line 2511646
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v3

    .line 2511647
    if-eqz v3, :cond_3

    .line 2511648
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2, v3}, LX/0gd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2511649
    :cond_3
    invoke-static/range {p0 .. p0}, Lcom/facebook/composer/activity/ComposerFragment;->k(Lcom/facebook/composer/activity/ComposerFragment;)LX/HqE;

    move-result-object v2

    invoke-virtual {v2}, LX/HqE;->a()V

    .line 2511650
    invoke-static/range {p0 .. p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/ATO;->b(Z)V

    .line 2511651
    invoke-static/range {p0 .. p0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v2

    invoke-interface {v2}, LX/CfL;->c()V

    .line 2511652
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->e()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2511653
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->az:LX/Hu6;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/Hu6;->c(Z)V

    .line 2511654
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    invoke-virtual {v2}, LX/AQ9;->H()LX/AQ4;

    move-result-object v2

    .line 2511655
    if-nez v2, :cond_e

    .line 2511656
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->aX:LX/AR2;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    invoke-virtual {v4}, LX/AQ9;->l()LX/ARN;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/AR2;->a(Ljava/lang/Object;LX/ARN;)LX/AR1;

    move-result-object v3

    .line 2511657
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2511658
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->aV:LX/AR7;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v2, v4, v3}, LX/AR7;->a(LX/0il;LX/AR1;)LX/AR6;

    move-result-object v2

    invoke-virtual {v2}, LX/AR6;->a()Landroid/content/Intent;

    move-result-object v2

    move-object v3, v2

    .line 2511659
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getReactionUnitId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 2511660
    const-string v4, "reaction_unit_id"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getReactionUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2511661
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isFireAndForget()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "is_uploading_media"

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_6

    .line 2511662
    const-string v4, "extra_actor_viewer_context"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    :goto_5
    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2511663
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/facebook/composer/publish/ComposerPublishService;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 2511664
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/facebook/composer/activity/ComposerFragment;->al()V

    .line 2511665
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v2

    sget-object v4, LX/2rt;->STATUS:LX/2rt;

    if-ne v2, v4, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    if-nez v2, :cond_7

    .line 2511666
    const-string v2, "try_show_survey_on_result_extra_data"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/facebook/composer/activity/ComposerFragment;->b(ZZ)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2511667
    const-string v2, "try_show_survey_on_result_integration_point_id"

    const-string v4, "1573843042880095"

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2511668
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->shouldUsePublishExperiment()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2511669
    const-string v2, "extra_composer_has_published"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2511670
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->i()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v2

    if-eqz v2, :cond_10

    .line 2511671
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->cg:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v2, v3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->d(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2511672
    :cond_8
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    const/4 v4, -0x1

    invoke-virtual {v2, v4, v3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2511673
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 2511674
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->ao:Z

    .line 2511675
    invoke-direct/range {p0 .. p0}, Lcom/facebook/composer/activity/ComposerFragment;->aN()V

    .line 2511676
    return-void

    .line 2511677
    :cond_9
    invoke-virtual {v2}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_0

    .line 2511678
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_1

    .line 2511679
    :cond_b
    const/4 v12, 0x0

    goto/16 :goto_2

    .line 2511680
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2511681
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->az:LX/Hu6;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/Hu6;->c(Z)V

    goto/16 :goto_3

    .line 2511682
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->aW:LX/AR4;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v2, v4}, LX/AR4;->a(Ljava/lang/Object;)LX/AR3;

    move-result-object v2

    .line 2511683
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->aU:LX/ARD;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v4, v5, v2, v3}, LX/ARD;->a(Ljava/lang/Object;LX/AR3;LX/AR1;)LX/ARC;

    move-result-object v2

    invoke-virtual {v2}, LX/ARC;->b()Landroid/content/Intent;

    move-result-object v2

    move-object v3, v2

    .line 2511684
    goto/16 :goto_4

    .line 2511685
    :cond_e
    invoke-interface {v2}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    move-object v3, v2

    goto/16 :goto_4

    .line 2511686
    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 2511687
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->cg:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v2, v3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_6
.end method

.method private al()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2511618
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-boolean v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->c:Z

    if-nez v0, :cond_1

    .line 2511619
    :cond_0
    :goto_0
    return-void

    .line 2511620
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2511621
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2511622
    iget-object v1, v0, LX/AQ9;->t:LX/ARN;

    move-object v0, v1

    .line 2511623
    if-eqz v0, :cond_2

    invoke-interface {v0}, LX/ARN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2511624
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2511625
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 2511626
    invoke-static {v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;

    move-result-object v1

    .line 2511627
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2511628
    iget-object v2, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v2

    .line 2511629
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-eq v2, v3, :cond_3

    .line 2511630
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 2511631
    iget-object v2, v1, LX/8QP;->a:LX/4YH;

    .line 2511632
    iput-object v0, v2, LX/4YH;->b:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    .line 2511633
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ay:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-virtual {v1}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    goto/16 :goto_0
.end method

.method private am()I
    .locals 3

    .prologue
    .line 2511617
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method

.method private ao()Z
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 2511567
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2511568
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 2511569
    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 2511570
    :goto_0
    return v0

    .line 2511571
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hrk;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 2511572
    iput-object v1, v0, LX/Hrk;->a:Ljava/lang/String;

    .line 2511573
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hrk;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->cp:LX/HqL;

    .line 2511574
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, v0, LX/Hrk;->b:Ljava/lang/ref/WeakReference;

    .line 2511575
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hrk;

    const/4 v3, 0x0

    .line 2511576
    iget-object v1, v0, LX/Hrk;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2511577
    iget-object v1, v0, LX/Hrk;->b:Ljava/lang/ref/WeakReference;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2511578
    iget-object v1, v0, LX/Hrk;->k:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    invoke-virtual {v1, v3}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v3

    .line 2511579
    :goto_1
    move v0, v1

    .line 2511580
    if-nez v0, :cond_2

    move v0, v2

    .line 2511581
    goto :goto_0

    .line 2511582
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hrk;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->cq:LX/HqM;

    .line 2511583
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, LX/Hrk;->c:Ljava/lang/ref/WeakReference;

    .line 2511584
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hrk;

    .line 2511585
    iget-object v3, v0, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2511586
    iget-object v3, v0, LX/Hrk;->a:Ljava/lang/String;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2511587
    iget-object v3, v0, LX/Hrk;->b:Ljava/lang/ref/WeakReference;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2511588
    iget-object v3, v0, LX/Hrk;->c:Ljava/lang/ref/WeakReference;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2511589
    invoke-static {v0}, LX/Hrk;->c(LX/Hrk;)LX/HqL;

    move-result-object v3

    .line 2511590
    invoke-static {v0}, LX/Hrk;->d(LX/Hrk;)LX/HqM;

    move-result-object v4

    .line 2511591
    if-eqz v3, :cond_3

    if-nez v4, :cond_9

    .line 2511592
    :cond_3
    :goto_2
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2511593
    :cond_4
    iget-object v1, v0, LX/Hrk;->j:LX/33A;

    invoke-virtual {v1}, LX/33A;->a()Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    move-result-object v1

    iput-object v1, v0, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    .line 2511594
    iget-object v1, v0, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    if-eqz v1, :cond_5

    iget-object v1, v0, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    iget-boolean v1, v1, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mEligible:Z

    if-nez v1, :cond_6

    :cond_5
    move v1, v3

    .line 2511595
    goto :goto_1

    .line 2511596
    :cond_6
    iget-object v1, v0, LX/Hrk;->f:LX/339;

    iget-object v4, v0, LX/Hrk;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, LX/339;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2511597
    iget-object v1, v0, LX/Hrk;->l:LX/2c9;

    sget-object v4, LX/2Zv;->INLINE_PRIVACY_SURVEY_HIDE_TO_DEDUP:LX/2Zv;

    invoke-virtual {v1, v4}, LX/2c9;->a(LX/2Zv;)V

    move v1, v3

    .line 2511598
    goto :goto_1

    .line 2511599
    :cond_7
    invoke-static {v0}, LX/Hrk;->c(LX/Hrk;)LX/HqL;

    move-result-object v1

    .line 2511600
    if-nez v1, :cond_8

    move v1, v3

    .line 2511601
    goto :goto_1

    .line 2511602
    :cond_8
    invoke-virtual {v1}, LX/HqL;->a()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 2511603
    iget-object v3, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v3

    .line 2511604
    iget-object v3, v0, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    iget-object v3, v3, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mTriggerPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v1, v3}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v1

    goto/16 :goto_1

    .line 2511605
    :cond_9
    new-instance v5, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;

    invoke-direct {v5}, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;-><init>()V

    .line 2511606
    const/4 v6, 0x1

    const v7, 0x7f0e0c13

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 2511607
    move-object v5, v5

    .line 2511608
    iget-object v6, v0, LX/Hrk;->m:LX/Hrg;

    .line 2511609
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/Hrg;

    iput-object v7, v5, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->o:LX/Hrg;

    .line 2511610
    new-instance v6, LX/Hrh;

    invoke-direct {v6, v0, v5, v3, v4}, LX/Hrh;-><init>(LX/Hrk;Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;LX/HqL;LX/HqM;)V

    .line 2511611
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Hrh;

    iput-object v3, v5, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->p:LX/Hrh;

    .line 2511612
    iget-object v3, v0, LX/Hrk;->g:Landroid/content/Context;

    check-cast v3, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v3

    const-string v4, "inline_privacy_survey_dialog"

    invoke-virtual {v5, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2511613
    iget-object v3, v0, LX/Hrk;->i:LX/8Ps;

    iget-object v4, v0, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    iget-object v4, v4, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mTriggerPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const-string v5, "traditional_composer"

    .line 2511614
    iget-object v7, v3, LX/8Ps;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    sget-object v8, LX/5nc;->EXPOSED:LX/5nc;

    invoke-static {v3}, LX/8Ps;->a(LX/8Ps;)Ljava/lang/Long;

    move-result-object v9

    invoke-static {v4}, LX/8Ps;->a(LX/1oU;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    move-object v12, v5

    invoke-virtual/range {v7 .. v12}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/5nc;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2511615
    iget-object v3, v0, LX/Hrk;->j:LX/33A;

    invoke-virtual {v3}, LX/33A;->b()V

    .line 2511616
    iget-object v3, v0, LX/Hrk;->f:LX/339;

    iget-object v4, v0, LX/Hrk;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/339;->a(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public static ar(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 2511519
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5Qz;->C()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2511520
    :goto_0
    return-void

    .line 2511521
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->al:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kp;

    .line 2511522
    iget-object v1, v0, LX/7kp;->b:LX/11i;

    sget-object v2, LX/7kp;->a:LX/7ko;

    invoke-interface {v1, v2}, LX/11i;->a(LX/0Pq;)LX/11o;

    .line 2511523
    const-string v1, "ComposerFragmentPausing"

    invoke-static {v0, v1}, LX/7kp;->a(LX/7kp;Ljava/lang/String;)V

    .line 2511524
    sget-object v0, LX/0ge;->COMPOSER_FRIEND_TAG_CLICK:LX/0ge;

    sget-object v1, LX/CIp;->b:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;LX/0ge;Ljava/lang/String;)V

    .line 2511525
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->GROUP:LX/2rw;

    if-ne v0, v1, :cond_2

    .line 2511526
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 2511527
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aD()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v8, v6

    :goto_1
    if-ge v8, v11, :cond_1

    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 2511528
    new-instance v1, Lcom/facebook/ipc/model/FacebookProfile;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v1 .. v6}, Lcom/facebook/ipc/model/FacebookProfile;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v9, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2511529
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    .line 2511530
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-wide v2, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v3, v4, v0}, Lcom/facebook/profilelist/ProfilesListActivity;->a(Landroid/content/Context;JLX/0Px;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2511531
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->al:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kp;

    invoke-virtual {v0}, LX/7kp;->c()V

    .line 2511532
    invoke-virtual {p0, v1, v7}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2511533
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2511534
    iget-object v1, v0, LX/AQ9;->S:LX/AQ4;

    move-object v1, v1

    .line 2511535
    invoke-static {}, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->newBuilder()LX/A5S;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aD()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 2511536
    iput-object v2, v0, LX/A5S;->j:LX/0Px;

    .line 2511537
    move-object v2, v0

    .line 2511538
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 2511539
    iput-object v0, v2, LX/A5S;->k:Ljava/lang/String;

    .line 2511540
    move-object v2, v2

    .line 2511541
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->s:LX/75F;

    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aO()J

    move-result-wide v4

    invoke-static {v0, v3, v4, v5}, LX/7ky;->a(LX/0Px;LX/75F;J)LX/0Px;

    move-result-object v0

    .line 2511542
    iput-object v0, v2, LX/A5S;->m:LX/0Px;

    .line 2511543
    move-object v2, v2

    .line 2511544
    if-eqz v1, :cond_6

    invoke-interface {v1}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2511545
    :goto_2
    iput-object v0, v2, LX/A5S;->n:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2511546
    move-object v1, v2

    .line 2511547
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->B()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    invoke-static {v0}, LX/94d;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v6, v7

    .line 2511548
    :cond_3
    iput-boolean v6, v1, LX/A5S;->l:Z

    .line 2511549
    move-object v2, v1

    .line 2511550
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    invoke-virtual {v0}, LX/ATO;->n()V

    .line 2511551
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->t()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->setForceStopAutoTagging(Z)Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->a()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511552
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j7;->getViewerCoordinates()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aN:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9j9;

    const/4 v1, 0x1

    .line 2511553
    iget-object v3, v0, LX/9j9;->e:LX/1e2;

    invoke-virtual {v3}, LX/1e2;->a()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2511554
    :cond_4
    :goto_3
    move v0, v1

    .line 2511555
    if-eqz v0, :cond_5

    .line 2511556
    iput-boolean v7, v2, LX/A5S;->g:Z

    .line 2511557
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->ay()LX/9jG;

    move-result-object v0

    .line 2511558
    iput-object v0, v2, LX/A5S;->i:LX/9jG;

    .line 2511559
    :cond_5
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->al:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kp;

    invoke-virtual {v0}, LX/7kp;->c()V

    .line 2511560
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2}, LX/A5S;->a()Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->a(Landroid/content/Context;Lcom/facebook/tagging/conversion/FriendSelectorConfig;)Landroid/content/Intent;

    move-result-object v0

    .line 2511561
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 2511562
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 2511563
    :cond_7
    iget-object v3, v0, LX/9j9;->c:LX/9jB;

    invoke-virtual {v3}, LX/9jB;->c()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2511564
    iget-object v3, v0, LX/9j9;->f:LX/9jN;

    if-eqz v3, :cond_8

    iget-object v3, v0, LX/9j9;->f:LX/9jN;

    .line 2511565
    iget-boolean v0, v3, LX/9jN;->h:Z

    move v3, v0

    .line 2511566
    if-nez v3, :cond_4

    :cond_8
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public static av(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 2

    .prologue
    .line 2511516
    sget-object v0, LX/0ge;->COMPOSER_ADD_LOCATION_CLICK:LX/0ge;

    sget-object v1, LX/CIp;->a:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;LX/0ge;Ljava/lang/String;)V

    .line 2511517
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->h(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 2511518
    return-void
.end method

.method private ay()LX/9jG;
    .locals 1

    .prologue
    .line 2511508
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->l(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2511509
    sget-object v0, LX/9jG;->PHOTO:LX/9jG;

    .line 2511510
    :goto_0
    return-object v0

    .line 2511511
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->j(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2511512
    sget-object v0, LX/9jG;->VIDEO:LX/9jG;

    goto :goto_0

    .line 2511513
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2511514
    sget-object v0, LX/9jG;->CHECKIN:LX/9jG;

    goto :goto_0

    .line 2511515
    :cond_2
    sget-object v0, LX/9jG;->STATUS:LX/9jG;

    goto :goto_0
.end method

.method private static b(Landroid/content/Intent;)LX/0P1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2511494
    const-string v0, "full_profiles"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2511495
    if-nez v0, :cond_0

    .line 2511496
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2511497
    :goto_0
    return-object v0

    .line 2511498
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2511499
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 2511500
    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 2511501
    iput-object v5, v4, LX/5Rc;->b:Ljava/lang/String;

    .line 2511502
    move-object v4, v4

    .line 2511503
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 2511504
    iput-object v0, v4, LX/5Rc;->c:Ljava/lang/String;

    .line 2511505
    move-object v0, v4

    .line 2511506
    invoke-virtual {v0}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 2511507
    :cond_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method

.method private b(ZZ)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 2511484
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2511485
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ah:LX/Hv8;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    .line 2511486
    new-instance v3, LX/Hv7;

    const/16 p0, 0x1962

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    check-cast v1, LX/0il;

    invoke-direct {v3, p0, v1, p1, p2}, LX/Hv7;-><init>(LX/0Ot;LX/0il;ZZ)V

    .line 2511487
    const/16 p0, 0x12cb

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2511488
    iput-object p0, v3, LX/Hv7;->a:LX/0Or;

    .line 2511489
    move-object v0, v3

    .line 2511490
    invoke-virtual {v0}, LX/Hv7;->a()LX/0P1;

    move-result-object v0

    .line 2511491
    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2511492
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2511493
    :cond_0
    return-object v2
.end method

.method private b(Landroid/view/ViewGroup;Z)V
    .locals 8

    .prologue
    .line 2511472
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupInlineSprouts"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511473
    new-instance v2, LX/0zw;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bX:Landroid/view/View;

    const v1, 0x7f0d0a5c

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    .line 2511474
    new-instance v3, LX/0zw;

    const v0, 0x7f0d0afd

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v3, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    .line 2511475
    new-instance v4, LX/0zw;

    const v0, 0x7f0d0aff

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v4, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    .line 2511476
    new-instance v5, LX/0zw;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bX:Landroid/view/View;

    const v1, 0x7f0d0a5a

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v5, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    .line 2511477
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bg:LX/Htm;

    new-instance v1, LX/HrH;

    invoke-direct {v1, p0}, LX/HrH;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->Q(Lcom/facebook/composer/activity/ComposerFragment;)LX/0Px;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/Htm;->a(Landroid/view/View$OnClickListener;Lcom/facebook/composer/inlinesprouts/InlineSproutsView;Landroid/widget/TextView;LX/0zw;LX/0zw;Ljava/lang/Object;LX/0Px;)LX/Htl;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bf:LX/Htl;

    .line 2511478
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bf:LX/Htl;

    invoke-virtual {v0, v1}, LX/HvN;->a(LX/0iK;)V

    .line 2511479
    invoke-static {p0, p1}, Lcom/facebook/composer/activity/ComposerFragment;->b(Lcom/facebook/composer/activity/ComposerFragment;Landroid/view/View;)V

    .line 2511480
    if-eqz p2, :cond_0

    .line 2511481
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->b(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 2511482
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupInlineSprouts"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2511483
    return-void
.end method

.method private static b(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2511468
    invoke-static {p1}, LX/7kq;->j(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2511469
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v0

    sget-object v1, LX/5Rn;->SAVE_DRAFT:LX/5Rn;

    if-ne v0, v1, :cond_0

    .line 2511470
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    sget-object v1, LX/5Rn;->NORMAL:LX/5Rn;

    invoke-virtual {v0, v1}, LX/0jL;->a(LX/5Rn;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511471
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/composer/activity/ComposerFragment;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2511278
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bn:Lcom/facebook/composer/header/ComposerHeaderViewController;

    if-nez v0, :cond_0

    .line 2511279
    :goto_0
    return-void

    .line 2511280
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/HrY;

    invoke-direct {v1, p0, p1}, LX/HrY;-><init>(Lcom/facebook/composer/activity/ComposerFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/composer/activity/ComposerFragment;Ljava/lang/String;)V
    .locals 3
    .param p0    # Lcom/facebook/composer/activity/ComposerFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2511281
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0, p1}, Lcom/facebook/friendsharing/birthdaystickers/BirthdayStickerPickerActivity;->a(Landroid/content/Context;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2511282
    if-eqz v1, :cond_0

    .line 2511283
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x11

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2511284
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/composer/activity/ComposerFragment;Z)V
    .locals 2

    .prologue
    .line 2511291
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/ASX;->a(Z)V

    .line 2511292
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->clearFocus()V

    .line 2511293
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->G()V

    .line 2511294
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bf:LX/Htl;

    .line 2511295
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Htl;->k:Z

    .line 2511296
    if-eqz p1, :cond_2

    .line 2511297
    iget-object v1, v0, LX/Htl;->b:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, LX/Htp;->FROM_TOP:LX/Htp;

    :goto_0
    iput-object v1, v0, LX/Htl;->m:LX/Htp;

    .line 2511298
    :goto_1
    iget-object v1, v0, LX/Htl;->m:LX/Htp;

    sget-object p0, LX/Htp;->FROM_TOP:LX/Htp;

    if-eq v1, p0, :cond_0

    .line 2511299
    iget-object v1, v0, LX/Htl;->m:LX/Htp;

    invoke-static {v0, v1}, LX/Htl;->a(LX/Htl;LX/Htp;)V

    .line 2511300
    :cond_0
    return-void

    .line 2511301
    :cond_1
    sget-object v1, LX/Htp;->FROM_BOTTOM:LX/Htp;

    goto :goto_0

    .line 2511302
    :cond_2
    sget-object v1, LX/Htp;->NO_ANIMATION:LX/Htp;

    iput-object v1, v0, LX/Htl;->m:LX/Htp;

    goto :goto_1
.end method

.method private c(ILandroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2511303
    if-nez p1, :cond_1

    .line 2511304
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_FRIEND_TAG_CANCEL:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2511305
    :cond_0
    :goto_0
    return-void

    .line 2511306
    :cond_1
    const/4 v0, -0x1

    if-eq p1, v0, :cond_2

    .line 2511307
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_FRIEND_TAG_FAILURE:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    goto :goto_0

    .line 2511308
    :cond_2
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v3, LX/0ge;->COMPOSER_FRIEND_TAG:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2511309
    const-string v0, "tag_people_after_tag_place"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2511310
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v3, LX/0ge;->COMPOSER_FRIEND_TAG_SUGGESTIONS_SHOWN:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2511311
    :cond_3
    invoke-static {p2}, Lcom/facebook/composer/activity/ComposerFragment;->b(Landroid/content/Intent;)LX/0P1;

    move-result-object v3

    .line 2511312
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v4

    .line 2511313
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ax:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7l0;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aO()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v5}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v0, v1, v5, v6}, LX/7ky;->a(LX/7l0;LX/0Px;LX/0Rf;Z)LX/0Px;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->a(LX/0Px;)LX/0Rf;

    move-result-object v1

    .line 2511314
    invoke-virtual {v3}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-static {v0, v1}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v5

    .line 2511315
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2511316
    invoke-virtual {v5}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2511317
    invoke-virtual {v3, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2511318
    :cond_4
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v7, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v7}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/0jL;->b(LX/0Px;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511319
    invoke-virtual {v3}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-static {v1, v0}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v3

    .line 2511320
    invoke-virtual {v3}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2511321
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->r:LX/75Q;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->b(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, LX/75Q;->a(LX/0Px;LX/0Rf;)V

    .line 2511322
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v1, v2

    .line 2511323
    :goto_2
    if-ge v1, v7, :cond_6

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2511324
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 2511325
    iget-object v8, p0, Lcom/facebook/composer/activity/ComposerFragment;->r:LX/75Q;

    invoke-static {v0}, LX/7ky;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/0Px;

    move-result-object v0

    invoke-virtual {v8, v0, v3}, LX/75Q;->a(LX/0Px;LX/0Rf;)V

    .line 2511326
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2511327
    :cond_6
    invoke-virtual {v4, v5}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v3}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2511328
    :cond_7
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->b(LX/0Px;)LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v1, v2

    :goto_3
    if-ge v1, v7, :cond_8

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 2511329
    iget-object v8, p0, Lcom/facebook/composer/activity/ComposerFragment;->r:LX/75Q;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8, v9, v0}, LX/75Q;->a(Landroid/content/Context;Lcom/facebook/photos/base/media/PhotoItem;)V

    .line 2511330
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2511331
    :cond_8
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->O$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2511332
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->W$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2511333
    invoke-static {v4, v5}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    invoke-virtual {v0}, LX/0Ro;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {v3}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2511334
    :cond_9
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v3, LX/0ge;->COMPOSER_FRIEND_TAG_REMOVE:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2511335
    :cond_a
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->b(LX/0Px;)LX/0Px;

    move-result-object v0

    sget-object v3, LX/03R;->NO:LX/03R;

    invoke-virtual {v1, v0, v3}, LX/ATO;->a(LX/0Px;LX/03R;)V

    .line 2511336
    const-string v0, "tag_place_after_tag_people"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2511337
    invoke-static {p0, p1, p2}, Lcom/facebook/composer/activity/ComposerFragment;->d(Lcom/facebook/composer/activity/ComposerFragment;ILandroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private static c$redex0(Lcom/facebook/composer/activity/ComposerFragment;Z)V
    .locals 6

    .prologue
    .line 2511338
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->T(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2511339
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    .line 2511340
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2511341
    invoke-virtual {v0, v1, p1}, LX/ATO;->a(LX/0Px;Z)V

    .line 2511342
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5Qz;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2511343
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->U$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2511344
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5Qz;->C()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2511345
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aE:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-static {v3}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    :goto_1
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    iget-wide v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual/range {v0 .. v5}, LX/1EZ;->a(LX/0Px;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;J)V

    .line 2511346
    :cond_1
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    invoke-virtual {v0}, LX/ATO;->p()V

    .line 2511347
    return-void

    .line 2511348
    :cond_2
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0, p1}, LX/ATO;->a(LX/0Px;Z)V

    goto/16 :goto_0

    .line 2511349
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private d(Landroid/view/ViewGroup;)V
    .locals 13

    .prologue
    .line 2511350
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupPrivacyViews"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511351
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2511352
    iget-object v1, v0, LX/AQ9;->A:LX/ARN;

    move-object v0, v1

    .line 2511353
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2511354
    iget-object v1, v0, LX/AQ9;->A:LX/ARN;

    move-object v0, v1

    .line 2511355
    invoke-interface {v0}, LX/ARN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2511356
    :goto_0
    return-void

    .line 2511357
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2511358
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bh:LX/Huo;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->bn:Lcom/facebook/composer/header/ComposerHeaderViewController;

    .line 2511359
    iget-object v4, v3, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    move-object v3, v4

    .line 2511360
    iget-object v4, v3, Lcom/facebook/composer/header/ComposerHeaderView;->l:Landroid/view/ViewStub;

    move-object v3, v4

    .line 2511361
    new-instance p1, LX/Hun;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    check-cast v2, LX/0il;

    invoke-direct {p1, v4, v2, v3}, LX/Hun;-><init>(Landroid/content/res/Resources;LX/0il;Landroid/view/ViewStub;)V

    .line 2511362
    move-object v1, p1

    .line 2511363
    invoke-virtual {v0, v1}, LX/HvN;->a(LX/0iK;)V

    .line 2511364
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bl:LX/Hus;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->bn:Lcom/facebook/composer/header/ComposerHeaderViewController;

    .line 2511365
    iget-object v4, v3, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    move-object v3, v4

    .line 2511366
    iget-object v4, v3, Lcom/facebook/composer/header/ComposerHeaderView;->m:LX/0zw;

    move-object v3, v4

    .line 2511367
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->bM:LX/HqQ;

    invoke-virtual {v1, v2, v3, v4}, LX/Hus;->a(Ljava/lang/Object;LX/0zw;LX/HqQ;)LX/Hur;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HvN;->a(LX/0iK;)V

    .line 2511368
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bk:LX/Huk;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->bn:Lcom/facebook/composer/header/ComposerHeaderViewController;

    .line 2511369
    iget-object v4, v3, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    move-object v3, v4

    .line 2511370
    iget-object v4, v3, Lcom/facebook/composer/header/ComposerHeaderView;->o:LX/0zw;

    move-object v3, v4

    .line 2511371
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->bN:LX/Hqb;

    .line 2511372
    new-instance v5, LX/Huj;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v6

    check-cast v6, LX/0wM;

    const/16 v7, 0x259

    invoke-static {v1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    move-object v8, v2

    check-cast v8, LX/0il;

    invoke-static {v1}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v11

    check-cast v11, LX/8Sa;

    const-class v9, Landroid/content/Context;

    invoke-interface {v1, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/Context;

    move-object v9, v3

    move-object v10, v4

    invoke-direct/range {v5 .. v12}, LX/Huj;-><init>(LX/0wM;LX/0Ot;LX/0il;LX/0zw;LX/Hqb;LX/8Sa;Landroid/content/Context;)V

    .line 2511373
    move-object v1, v5

    .line 2511374
    invoke-virtual {v0, v1}, LX/HvN;->a(LX/0iK;)V

    .line 2511375
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupPrivacyViews"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 2511376
    :cond_1
    const v0, 0x7f0d0af0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ar:Landroid/view/View;

    .line 2511377
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0ac3

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    new-instance v2, LX/Hqo;

    invoke-direct {v2, p0}, LX/Hqo;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-direct {v1, v0, v2}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->as:LX/0zw;

    .line 2511378
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0ac4

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->at:LX/0zw;

    .line 2511379
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ar:Landroid/view/View;

    const v1, 0x7f0d0ac5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->au:Landroid/view/ViewStub;

    .line 2511380
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->O$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    goto :goto_1
.end method

.method public static d(Lcom/facebook/composer/activity/ComposerFragment;ILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2511381
    if-nez p1, :cond_1

    .line 2511382
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_CANCEL:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2511383
    :cond_0
    :goto_0
    return-void

    .line 2511384
    :cond_1
    const/4 v0, -0x1

    if-eq p1, v0, :cond_2

    .line 2511385
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_FAILURE:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    goto :goto_0

    .line 2511386
    :cond_2
    const-string v0, "tag_place_after_tag_people"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2511387
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION_AFTER_TAG_PEOPLE:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2511388
    :cond_3
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->ac(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2511389
    const-string v0, "extra_xed_location"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2511390
    if-eqz v0, :cond_4

    .line 2511391
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object v1

    invoke-virtual {v1}, LX/5RO;->a()LX/5RO;

    move-result-object v1

    invoke-virtual {v1}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511392
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2511393
    :goto_1
    const-string v0, "tag_people_after_tag_place"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2511394
    invoke-direct {p0, p1, p2}, Lcom/facebook/composer/activity/ComposerFragment;->c(ILandroid/content/Intent;)V

    goto :goto_0

    .line 2511395
    :cond_4
    const-string v0, "text_only_place"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2511396
    const-string v0, "text_only_place"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2511397
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v2, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/5RO;->a(Ljava/lang/String;)LX/5RO;

    move-result-object v2

    invoke-virtual {v2}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 2511398
    goto :goto_1

    .line 2511399
    :cond_5
    const-string v0, "extra_place"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    const-string v1, "extra_implicit_location"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    const-string v2, "minutiae_object"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    goto :goto_1

    .line 2511400
    :cond_6
    const-string v0, "tag_place_after_tag_people"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2511401
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    const/4 v1, 0x1

    new-array v1, v1, [LX/ASW;

    sget-object v2, LX/ASW;->TAG_PEOPLE_FOR_CHECKIN:LX/ASW;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, LX/ASX;->a([LX/ASW;)V

    goto/16 :goto_0
.end method

.method public static d$redex0(Lcom/facebook/composer/activity/ComposerFragment;Z)V
    .locals 1

    .prologue
    .line 2511402
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    invoke-virtual {v0}, LX/ATO;->m()Z

    move-result v0

    .line 2511403
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2511404
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->s()V

    .line 2511405
    :cond_0
    return-void
.end method

.method private e(ILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2511406
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 2511407
    :cond_0
    :goto_0
    return-void

    .line 2511408
    :cond_1
    const-string v0, "tag_branded_content"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2511409
    const-string v0, "tag_branded_content"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 2511410
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v2, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    .line 2511411
    iget-object v2, v1, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2511412
    iget-object v2, v1, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v2

    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2511413
    iget-object v2, v1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v2, :cond_2

    .line 2511414
    iget-object v2, v1, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v2

    iput-object v2, v1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2511415
    :cond_2
    iget-object v2, v1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v2, v0}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2511416
    iget-object v2, v1, LX/0jL;->a:LX/0cA;

    sget-object p0, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v2, p0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2511417
    :cond_3
    move-object v0, v1

    .line 2511418
    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    goto :goto_0
.end method

.method private e(Landroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 2511419
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupFeedOnlyPostController"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511420
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->af:LX/APq;

    const v0, 0x7f0d0aef

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    .line 2511421
    new-instance p1, LX/APp;

    const-class v4, Landroid/content/Context;

    invoke-interface {v2, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    check-cast v3, LX/0il;

    invoke-direct {p1, v4, v5, v0, v3}, LX/APp;-><init>(Landroid/content/Context;LX/0ad;Landroid/view/ViewStub;LX/0il;)V

    .line 2511422
    move-object v0, p1

    .line 2511423
    invoke-virtual {v1, v0}, LX/HvN;->a(LX/0iK;)V

    .line 2511424
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupFeedOnlyPostController"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2511425
    return-void
.end method

.method private f(Landroid/view/ViewGroup;)V
    .locals 8

    .prologue
    .line 2511426
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupDynamicAndRichTextStyleController"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511427
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Y:LX/HvU;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    .line 2511428
    iget-object v3, v2, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    move-object v2, v3

    .line 2511429
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->X:LX/0tO;

    invoke-virtual {v3}, LX/0tO;->a()Z

    move-result v3

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v4}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getEditTextDefaultTextSizeSp()F

    move-result v4

    iget-object v5, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v5}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getEditTextDefaultTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/HvU;->a(Ljava/lang/Object;LX/Be1;ZFLandroid/graphics/Typeface;)LX/HvT;

    move-result-object v0

    .line 2511430
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-virtual {v1, v0}, LX/HvN;->a(LX/0iK;)V

    .line 2511431
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v0}, LX/Hre;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getAllowRichTextStyle()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->X:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    .line 2511432
    iget-object v1, v0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    move-object v0, v1

    .line 2511433
    if-eqz v0, :cond_0

    .line 2511434
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->X:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->l()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    new-instance v1, LX/0zw;

    const v0, 0x7f0d0af2

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    move-object v0, v1

    .line 2511435
    :goto_0
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->V:LX/Hvk;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    .line 2511436
    iget-object v3, v2, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    move-object v2, v3

    .line 2511437
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v1, v0, v2, v3}, LX/Hvk;->a(Landroid/view/View;LX/Be2;Ljava/lang/Object;)LX/Hvj;

    move-result-object v3

    .line 2511438
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->W:LX/HvX;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    .line 2511439
    iget-object v4, v2, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    move-object v2, v4

    .line 2511440
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iget-object v5, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v5}, LX/2zG;->l()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->b(Z)Landroid/graphics/Rect;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v5}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getEditTextLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v5}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->getEditTextLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    :goto_1
    invoke-virtual/range {v0 .. v5}, LX/HvX;->a(Ljava/lang/Object;LX/Be2;LX/Hvj;Landroid/graphics/Rect;Landroid/view/ViewGroup$LayoutParams;)Lcom/facebook/composer/textstyle/ComposerRichTextController;

    .line 2511441
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-virtual {v0, v3}, LX/HvN;->a(LX/0iK;)V

    .line 2511442
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupDynamicAndRichTextStyleController"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2511443
    return-void

    .line 2511444
    :cond_1
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0af9

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    move-object v0, v1

    goto :goto_0

    .line 2511445
    :cond_2
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    goto :goto_1
.end method

.method public static f(Lcom/facebook/composer/activity/ComposerFragment;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2511446
    invoke-static {p0, p1}, Lcom/facebook/composer/activity/ComposerFragment;->g(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 2511447
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cc:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9c3;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, v3, v1}, LX/9c3;->c(Ljava/lang/String;I)V

    .line 2511448
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->k(Lcom/facebook/composer/activity/ComposerFragment;)LX/HqE;

    move-result-object v0

    invoke-virtual {v0}, LX/HqE;->b()V

    .line 2511449
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/ATO;->b(Z)V

    .line 2511450
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    invoke-virtual {v0}, LX/ATO;->l()V

    .line 2511451
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    sget-object v1, LX/5L2;->ON_USER_CANCEL:LX/5L2;

    invoke-virtual {v0, v1}, LX/HvN;->a(LX/5L2;)V

    .line 2511452
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aE:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1EZ;->b(Ljava/lang/String;)V

    .line 2511453
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2511454
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v0

    sget-object v3, LX/2rt;->STATUS:LX/2rt;

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2511455
    if-eqz p1, :cond_2

    .line 2511456
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->u:LX/APY;

    .line 2511457
    iget-boolean v3, v0, LX/APY;->e:Z

    move v0, v3

    .line 2511458
    :goto_0
    const-string v3, "try_show_survey_on_result_extra_data"

    invoke-direct {p0, p1, v0}, Lcom/facebook/composer/activity/ComposerFragment;->b(ZZ)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2511459
    const-string v0, "try_show_survey_on_result_integration_point_id"

    const-string v3, "1437658533199157"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2511460
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2511461
    iget-object v3, v0, LX/AQ9;->N:LX/AQ4;

    move-object v0, v3

    .line 2511462
    if-eqz v0, :cond_1

    .line 2511463
    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2511464
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2511465
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v0

    invoke-interface {v0}, LX/CfL;->b()V

    .line 2511466
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2511467
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private g(Landroid/view/ViewGroup;)V
    .locals 12

    .prologue
    .line 2512334
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupStickerUpsell"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512335
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aw:LX/0ad;

    sget-short v1, LX/IEy;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2512336
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0afb

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    .line 2512337
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->aa:LX/Hv4;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    new-instance v4, LX/Hqq;

    invoke-direct {v4, p0}, LX/Hqq;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512338
    new-instance v5, LX/Hv3;

    move-object v6, v3

    check-cast v6, LX/0il;

    .line 2512339
    new-instance v8, LX/IF2;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-direct {v8, v7}, LX/IF2;-><init>(LX/0ad;)V

    .line 2512340
    move-object v9, v8

    .line 2512341
    check-cast v9, LX/IF2;

    const-class v7, LX/IF1;

    invoke-interface {v2, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/IF1;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    move-object v7, v1

    move-object v8, v4

    invoke-direct/range {v5 .. v11}, LX/Hv3;-><init>(LX/0il;LX/0zw;LX/Hqq;LX/IF2;LX/IF1;LX/0ad;)V

    .line 2512342
    move-object v1, v5

    .line 2512343
    invoke-virtual {v0, v1}, LX/HvN;->a(LX/0iK;)V

    .line 2512344
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupStickerUpsell"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512345
    return-void
.end method

.method public static g(Lcom/facebook/composer/activity/ComposerFragment;Z)V
    .locals 22

    .prologue
    .line 2512612
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-direct/range {p0 .. p0}, Lcom/facebook/composer/activity/ComposerFragment;->am()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v6}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v6

    iget-wide v6, v6, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/facebook/composer/activity/ComposerFragment;->aI:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/composer/activity/ComposerFragment;->av:LX/1Ck;

    invoke-virtual {v10}, LX/1Ck;->b()LX/1M1;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v11}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v11}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v11

    iget-boolean v11, v11, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->e:Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v12}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v12}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRating()I

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v13}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v13}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v13

    invoke-virtual {v13}, LX/0Px;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_0

    const/4 v13, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/composer/activity/ComposerFragment;->p:LX/0Ot;

    invoke-interface {v14}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LX/0So;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v15}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v15}, Lcom/facebook/composer/system/model/ComposerModelImpl;->t()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v15

    invoke-static {v14, v15}, LX/ASb;->a(LX/0So;Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v15}, LX/2zG;->d()Z

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, LX/5Qu;->c()Z

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, LX/2zG;->a()Z

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->bC:LX/APf;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, LX/APf;->a()LX/0P1;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, LX/5RE;->I()LX/5RF;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, LX/5RF;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {p0 .. p0}, Lcom/facebook/composer/activity/ComposerFragment;->ae()I

    move-result v20

    move/from16 v21, p1

    invoke-virtual/range {v2 .. v21}, LX/0gd;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;IJJLX/1M1;ZIZZZZZLX/0P1;Ljava/lang/String;IZ)V

    .line 2512613
    return-void

    .line 2512614
    :cond_0
    const/4 v13, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/composer/activity/ComposerFragment;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 2512551
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->ay()LX/9jG;

    move-result-object v2

    .line 2512552
    iput-object v2, v0, LX/9jF;->q:LX/9jG;

    .line 2512553
    move-object v2, v0

    .line 2512554
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 2512555
    iput-object v0, v2, LX/9jF;->g:Ljava/lang/String;

    .line 2512556
    move-object v2, v2

    .line 2512557
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2512558
    iput-object v0, v2, LX/9jF;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2512559
    move-object v0, v2

    .line 2512560
    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v2}, LX/2zG;->p()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    move v2, v2

    .line 2512561
    iput-boolean v2, v0, LX/9jF;->t:Z

    .line 2512562
    move-object v2, v0

    .line 2512563
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 2512564
    iput-object v0, v2, LX/9jF;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2512565
    move-object v2, v2

    .line 2512566
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v0

    .line 2512567
    iput-object v0, v2, LX/9jF;->B:Ljava/lang/String;

    .line 2512568
    move-object v2, v2

    .line 2512569
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j7;->getViewerCoordinates()Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v0

    .line 2512570
    iput-object v0, v2, LX/9jF;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 2512571
    move-object v0, v2

    .line 2512572
    iput-boolean p1, v0, LX/9jF;->u:Z

    .line 2512573
    move-object v0, v0

    .line 2512574
    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v2}, LX/2zG;->m()Z

    move-result v2

    .line 2512575
    iput-boolean v2, v0, LX/9jF;->i:Z

    .line 2512576
    move-object v2, v0

    .line 2512577
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->an:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2rb;

    invoke-static {v3, v0}, LX/7kq;->a(LX/0Px;LX/2rb;)Landroid/location/Location;

    move-result-object v3

    .line 2512578
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->l(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 2512579
    invoke-static {v3}, Lcom/facebook/ipc/composer/model/ComposerLocation;->a(Landroid/location/Location;)Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-result-object v0

    .line 2512580
    iput-object v0, v2, LX/9jF;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 2512581
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v0}, LX/5Qz;->C()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2512582
    iput-boolean v1, v2, LX/9jF;->x:Z

    .line 2512583
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2512584
    iget-object v4, v0, LX/AQ9;->S:LX/AQ4;

    move-object v0, v4

    .line 2512585
    if-eqz v0, :cond_1

    .line 2512586
    invoke-interface {v0}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2512587
    iput-object v0, v2, LX/9jF;->A:Lcom/facebook/tagging/data/TagTypeaheadDataSourceMetadata;

    .line 2512588
    :cond_1
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->az:LX/Hu6;

    if-eqz v3, :cond_3

    move v0, v1

    .line 2512589
    :goto_1
    iget-object v1, v4, LX/Hu6;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 2512590
    iget-object v3, v4, LX/Hu6;->e:LX/7kl;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    .line 2512591
    iput-object v1, v3, LX/7kl;->d:Ljava/lang/String;

    .line 2512592
    iget-object v1, v4, LX/Hu6;->e:LX/7kl;

    .line 2512593
    if-eqz v0, :cond_5

    const-string v3, "use_prefixed_location"

    .line 2512594
    :goto_2
    iget-object v5, v1, LX/7kl;->c:LX/7kk;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string v4, "location_pin_clicked_"

    invoke-direct {p1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, LX/7kk;->a$redex0(LX/7kk;Ljava/lang/String;)V

    .line 2512595
    iget-object v3, v1, LX/7kl;->c:LX/7kk;

    .line 2512596
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "composer_location_pin_clicked_stats"

    invoke-direct {v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object p1, v3, LX/7kk;->a:LX/7kl;

    iget-object p1, p1, LX/7kl;->d:Ljava/lang/String;

    .line 2512597
    iput-object p1, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2512598
    move-object v4, v5

    .line 2512599
    iget-object v5, v3, LX/7kk;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/Pair;

    .line 2512600
    iget-object p1, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    iget-object v5, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v4, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_3

    .line 2512601
    :cond_2
    iget-object v5, v3, LX/7kk;->a:LX/7kl;

    iget-object v5, v5, LX/7kl;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2512602
    iget-object v5, v3, LX/7kk;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 2512603
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aS:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9j7;

    const v4, 0x150016

    .line 2512604
    iget-object v1, v0, LX/9j7;->b:LX/0id;

    const-string v3, "LocationPin"

    invoke-virtual {v1, v3}, LX/0id;->a(Ljava/lang/String;)V

    .line 2512605
    iget-object v1, v0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x150005

    invoke-interface {v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2512606
    iget-object v1, v0, LX/9j7;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2512607
    sget-object v1, LX/9jG;->STATUS:LX/9jG;

    invoke-static {v0, v4, v1}, LX/9j7;->a(LX/9j7;ILX/9jG;)V

    .line 2512608
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v2}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v2

    invoke-static {v1, v2}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2512609
    return-void

    .line 2512610
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2512611
    :cond_5
    const-string v3, "use_device_location"

    goto/16 :goto_2
.end method

.method public static k(Lcom/facebook/composer/activity/ComposerFragment;)LX/HqE;
    .locals 15

    .prologue
    .line 2512545
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->A:LX/HqE;

    if-nez v0, :cond_0

    .line 2512546
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->z:LX/HqF;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->cw:LX/HqT;

    .line 2512547
    new-instance v2, LX/HqE;

    invoke-static {v0}, LX/339;->a(LX/0QB;)LX/339;

    move-result-object v3

    check-cast v3, LX/339;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2fc7

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2fd8

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1b

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x34c

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x34b

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x349

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const/16 v12, 0x34a

    invoke-static {v0, v12}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-static {v0}, LX/0pY;->a(LX/0QB;)LX/0pY;

    move-result-object v13

    check-cast v13, LX/0pZ;

    move-object v14, v1

    invoke-direct/range {v2 .. v14}, LX/HqE;-><init>(LX/339;LX/0kb;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0pZ;LX/HqT;)V

    .line 2512548
    move-object v0, v2

    .line 2512549
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->A:LX/HqE;

    .line 2512550
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->A:LX/HqE;

    return-object v0
.end method

.method private k(ILandroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2512516
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 2512517
    :cond_0
    :goto_0
    return-void

    .line 2512518
    :cond_1
    if-eqz p2, :cond_4

    const-string v0, "extra_storyline_tagged_friends"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2512519
    const-string v0, "extra_storyline_tagged_friends"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2512520
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2512521
    iget-object v6, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v6}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v6

    .line 2512522
    invoke-virtual {v7, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2512523
    invoke-static {v6}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v6

    invoke-static {v6}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v8

    .line 2512524
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/ipc/model/FacebookProfile;

    .line 2512525
    iget-wide v10, v6, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 2512526
    iget-wide v10, v6, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v10, v11}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v10

    iget-object v11, v6, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 2512527
    iput-object v11, v10, LX/5Rc;->b:Ljava/lang/String;

    .line 2512528
    move-object v10, v10

    .line 2512529
    iget-object v6, v6, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 2512530
    iput-object v6, v10, LX/5Rc;->c:Ljava/lang/String;

    .line 2512531
    move-object v6, v10

    .line 2512532
    invoke-virtual {v6}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2512533
    :cond_3
    iget-object v6, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v8, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v6, v8}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v6

    check-cast v6, LX/0jL;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0jL;->b(LX/0Px;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0jL;

    invoke-virtual {v6}, LX/0jL;->a()V

    .line 2512534
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->O$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512535
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->W$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512536
    :cond_4
    const-string v0, "extra_storyline_data"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerStorylineData;

    .line 2512537
    const-string v1, "extra_storyline_exported_media_item"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/media/MediaItem;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    .line 2512538
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HwR;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v4}, LX/HwR;->a(Ljava/lang/String;LX/0Px;Z)LX/7ks;

    move-result-object v1

    .line 2512539
    iget-object v1, v1, LX/7ks;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2512540
    invoke-static {p0, v3}, Lcom/facebook/composer/activity/ComposerFragment;->b(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;)V

    .line 2512541
    invoke-static {p0, v3, v5, v5}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;ZZ)V

    .line 2512542
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/ATO;->c(Z)V

    .line 2512543
    sget-object v1, LX/ASW;->TAG_PLACE_AFTER_PHOTO:LX/ASW;

    invoke-static {p0, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;LX/ASW;)V

    .line 2512544
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v2, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerStorylineData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    goto/16 :goto_0
.end method

.method public static n(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 4

    .prologue
    .line 2512512
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    invoke-virtual {v0}, LX/ATO;->n()V

    .line 2512513
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->b(LX/0Px;)LX/0Px;

    move-result-object v0

    sget-object v2, LX/03R;->NO:LX/03R;

    invoke-virtual {v1, v0, v2}, LX/ATO;->a(LX/0Px;LX/03R;)V

    .line 2512514
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->av:LX/1Ck;

    sget-object v1, LX/7mH;->ENABLE_POST_AFTER_AUTOTAGGING:LX/7mH;

    new-instance v2, LX/HqY;

    invoke-direct {v2, p0}, LX/HqY;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    new-instance v3, LX/HqZ;

    invoke-direct {v3, p0}, LX/HqZ;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2512515
    return-void
.end method

.method public static p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;
    .locals 5

    .prologue
    .line 2512504
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bV:LX/CfL;

    if-nez v0, :cond_0

    .line 2512505
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getSavedSessionLoadAttempts()I

    move-result v0

    if-nez v0, :cond_2

    .line 2512506
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getReactionSurface()Ljava/lang/String;

    move-result-object v1

    .line 2512507
    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->bU:LX/CfR;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v4

    if-nez v1, :cond_1

    const-string v0, "ANDROID_COMPOSER"

    :goto_0
    invoke-virtual {v2, v3, v4, v0}, LX/CfR;->a(Ljava/lang/String;ZLjava/lang/String;)LX/CfL;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bV:LX/CfL;

    .line 2512508
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bV:LX/CfL;

    return-object v0

    :cond_1
    move-object v0, v1

    .line 2512509
    goto :goto_0

    .line 2512510
    :cond_2
    new-instance v0, LX/Cfz;

    invoke-direct {v0}, LX/Cfz;-><init>()V

    move-object v0, v0

    .line 2512511
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bV:LX/CfL;

    goto :goto_1
.end method

.method private q()V
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 2512455
    const-string v5, ""

    .line 2512456
    :try_start_0
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->R:LX/0lB;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 2512457
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getEntryPicker()LX/5RI;

    move-result-object v0

    .line 2512458
    sget-object v1, LX/5RI;->MEDIA_PICKER:LX/5RI;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/5RI;->PLACE_PICKER:LX/5RI;

    if-eq v0, v1, :cond_0

    .line 2512459
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 2512460
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->bC:LX/APf;

    invoke-virtual {v3}, LX/APf;->a()LX/0P1;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v4}, LX/5RE;->I()LX/5RF;

    move-result-object v4

    invoke-virtual {v4}, LX/5RF;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2512461
    sget-object v7, LX/0ge;->COMPOSER_INIT:LX/0ge;

    invoke-static {v7, v1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v8

    .line 2512462
    invoke-static {v0, v8, v2}, LX/0gd;->a(LX/0gd;LX/324;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 2512463
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getExternalRefName()Ljava/lang/String;

    move-result-object v7

    .line 2512464
    iget-object v9, v8, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "external_ref_name"

    invoke-virtual {v9, v1, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2512465
    move-object v7, v8

    .line 2512466
    invoke-virtual {v7, v3}, LX/324;->a(LX/0P1;)LX/324;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/324;->j(Ljava/lang/String;)LX/324;

    move-result-object v7

    .line 2512467
    iget-object v9, v7, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "configuration"

    invoke-virtual {v9, v1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2512468
    move-object v9, v7

    .line 2512469
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v7

    iget-object v7, v7, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-nez v7, :cond_8

    :cond_1
    const/4 v7, 0x0

    .line 2512470
    :goto_1
    iget-object v1, v9, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "prefilled_share_id"

    invoke-virtual {v1, v2, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2512471
    iget-object v7, v0, LX/0gd;->a:LX/0Zb;

    .line 2512472
    iget-object v9, v8, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v8, v9

    .line 2512473
    invoke-interface {v7, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2512474
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->cc:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9c3;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2512475
    invoke-static {v2}, LX/1bi;->a(Ljava/lang/Object;)I

    move-result v7

    iget v8, v0, LX/9c3;->d:I

    rem-int/2addr v7, v8

    if-nez v7, :cond_a

    const/4 v7, 0x1

    :goto_2
    move v7, v7

    .line 2512476
    if-nez v7, :cond_9

    .line 2512477
    :goto_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2512478
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2512479
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v6

    :goto_4
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 2512480
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2512481
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2512482
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2512483
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aG:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "composer_entry_configuration_json_failed"

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 2512484
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2512485
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2512486
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/7kq;->j(LX/0Px;)Z

    move-result v4

    move v5, v6

    invoke-virtual/range {v0 .. v5}, LX/0gd;->a(Ljava/lang/String;LX/0Px;LX/0Px;ZZ)V

    .line 2512487
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getEntryPicker()LX/5RI;

    move-result-object v0

    sget-object v1, LX/5RI;->MEDIA_PICKER:LX/5RI;

    if-ne v0, v1, :cond_4

    .line 2512488
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aC:LX/73w;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 2512489
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 2512490
    const-string v3, "camera_flow"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2512491
    sget-object v3, LX/74R;->CAMERA_FLOW:LX/74R;

    const/4 v4, 0x0

    invoke-static {v1, v3, v2, v4}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2512492
    :cond_4
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v0

    sget-object v1, LX/21D;->THIRD_PARTY_APP_VIA_INTENT:LX/21D;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->j(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2512493
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aC:LX/73w;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->h(LX/0Px;)Landroid/net/Uri;

    move-result-object v0

    .line 2512494
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 2512495
    if-eqz v0, :cond_5

    .line 2512496
    const-string v3, "uri"

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2512497
    :cond_5
    sget-object v3, LX/74R;->EXTERNAL_VIDEO:LX/74R;

    const/4 v4, 0x0

    invoke-static {v1, v3, v2, v4}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2512498
    :cond_6
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2512499
    :cond_7
    :goto_5
    return-void

    .line 2512500
    :cond_8
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v7

    iget-object v7, v7, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_1

    .line 2512501
    :cond_9
    const-string v7, "numOfAttachments"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v10

    .line 2512502
    iget-object v7, v0, LX/9c3;->b:LX/11i;

    sget-object v8, LX/9c3;->a:LX/9c1;

    iget-object v9, v0, LX/9c3;->c:LX/0So;

    invoke-interface {v9}, LX/0So;->now()J

    move-result-wide v11

    move-object v9, v2

    invoke-interface/range {v7 .. v12}, LX/11i;->a(LX/0Pq;Ljava/lang/String;LX/0P1;J)LX/11o;

    goto/16 :goto_3

    :cond_a
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 2512503
    :cond_b
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->N:LX/BM1;

    const-string v2, "on_composer_entered"

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jI;->getAppAttribution()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/BM1;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method private q(ILandroid/content/Intent;)V
    .locals 10

    .prologue
    .line 2512408
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->k(Lcom/facebook/composer/activity/ComposerFragment;)LX/HqE;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bQ:LX/HrJ;

    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 2512409
    iget-object v2, v0, LX/HqE;->c:LX/339;

    .line 2512410
    iput-boolean v3, v2, LX/339;->g:Z

    .line 2512411
    const/4 v2, -0x1

    if-eq p1, v2, :cond_1

    .line 2512412
    :cond_0
    :goto_0
    return-void

    .line 2512413
    :cond_1
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, LX/HqE;->a:Ljava/lang/ref/WeakReference;

    .line 2512414
    const-string v2, "audience_educator_composer_action"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2512415
    const-string v2, "audience_educator_composer_action"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, LX/8Pq;

    .line 2512416
    const-string v2, "audience_educator_privacy_type_extra"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, LX/8Pr;

    .line 2512417
    iget-object v2, v0, LX/HqE;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HqT;

    .line 2512418
    if-eqz v2, :cond_6

    .line 2512419
    invoke-virtual {v2}, LX/HqT;->d()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v7

    .line 2512420
    :goto_1
    if-nez v7, :cond_3

    .line 2512421
    iget-object v3, v0, LX/HqE;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03V;

    const-string v4, "audience_educator_controller_no_educator_data"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "AudienceEducatorController: educator data is empty. "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v2, "Provider was null"

    :goto_2
    invoke-virtual {v3, v4, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "Provider was not null"

    goto :goto_2

    .line 2512422
    :cond_3
    sget-object v2, LX/Hq9;->a:[I

    invoke-virtual {v4}, LX/8Pq;->ordinal()I

    move-result v9

    aget v2, v2, v9

    packed-switch v2, :pswitch_data_0

    .line 2512423
    :goto_3
    invoke-static {v0, v4, v1, v5}, LX/HqE;->a(LX/HqE;LX/8Pq;LX/HrJ;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2512424
    if-eqz v8, :cond_0

    .line 2512425
    iget-object v2, v0, LX/HqE;->c:LX/339;

    iget-object v3, v7, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->e:LX/2by;

    .line 2512426
    iget-object v4, v2, LX/339;->m:LX/0P1;

    invoke-virtual {v4, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2c0;

    .line 2512427
    if-nez v4, :cond_7

    .line 2512428
    :goto_4
    goto :goto_0

    .line 2512429
    :pswitch_0
    iget-object v2, v0, LX/HqE;->c:LX/339;

    .line 2512430
    iget-object v3, v2, LX/339;->k:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v5, v3

    .line 2512431
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    move-object v3, v1

    .line 2512432
    invoke-static/range {v2 .. v7}, LX/HqE;->a(LX/HqE;LX/HrJ;LX/8Pq;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/8Pr;Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)V

    goto :goto_3

    .line 2512433
    :pswitch_1
    iget-object v2, v1, LX/HrJ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v2}, Lcom/facebook/composer/activity/ComposerFragment;->N$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512434
    iget-object v2, v1, LX/HrJ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a()LX/AP3;

    move-result-object v2

    const/4 v6, 0x1

    .line 2512435
    iput-boolean v6, v2, LX/AP3;->f:Z

    .line 2512436
    move-object v2, v2

    .line 2512437
    invoke-virtual {v2}, LX/AP3;->a()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v6

    .line 2512438
    iget-object v2, v1, LX/HrJ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v2, v2, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v9, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v2, v9}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v2

    check-cast v2, LX/0jL;

    invoke-virtual {v2, v6}, LX/0jL;->a(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jL;

    invoke-virtual {v2}, LX/0jL;->a()V

    .line 2512439
    iget-object v2, v0, LX/HqE;->c:LX/339;

    iget-object v6, v7, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->e:LX/2by;

    invoke-virtual {v2, v6}, LX/339;->c(LX/2by;)Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v3

    .line 2512440
    :goto_5
    iput-boolean v8, v0, LX/HqE;->b:Z

    move v8, v2

    .line 2512441
    goto :goto_3

    .line 2512442
    :pswitch_2
    iget-object v2, v0, LX/HqE;->c:LX/339;

    .line 2512443
    iget-object v3, v2, LX/339;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v5, v3

    .line 2512444
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    move-object v3, v1

    .line 2512445
    invoke-static/range {v2 .. v7}, LX/HqE;->a(LX/HqE;LX/HrJ;LX/8Pq;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/8Pr;Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)V

    goto :goto_3

    .line 2512446
    :pswitch_3
    iget-object v2, v0, LX/HqE;->c:LX/339;

    .line 2512447
    iget-object v3, v2, LX/339;->l:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v5, v3

    .line 2512448
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    move-object v3, v1

    .line 2512449
    invoke-static/range {v2 .. v7}, LX/HqE;->a(LX/HqE;LX/HrJ;LX/8Pq;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/8Pr;Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)V

    goto :goto_3

    .line 2512450
    :pswitch_4
    const-string v2, "privacy_option"

    invoke-static {p2, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2512451
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    move-object v3, v1

    .line 2512452
    invoke-static/range {v2 .. v7}, LX/HqE;->a(LX/HqE;LX/HrJ;LX/8Pq;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/8Pr;Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)V

    goto/16 :goto_3

    .line 2512453
    :cond_4
    iget-object v2, v0, LX/HqE;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v3, "audience_educator_controller_activity_result_missing_field"

    const-string v4, "AudienceEducatorActivity didn\'t have AUDIENCE_EDUCATOR_ACTION_EXTRA."

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v2, v8

    goto :goto_5

    :cond_6
    move-object v7, v5

    goto/16 :goto_1

    .line 2512454
    :cond_7
    iget-object v4, v2, LX/339;->m:LX/0P1;

    invoke-virtual {v4, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2c0;

    invoke-interface {v4}, LX/2c0;->d()V

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static r(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 4

    .prologue
    .line 2512390
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iv;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2512391
    :goto_0
    return-void

    .line 2512392
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->az:LX/Hu6;

    const/4 p0, 0x0

    .line 2512393
    iget-object v1, v0, LX/Hu6;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 2512394
    invoke-static {v0}, LX/Hu6;->e(LX/Hu6;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j8;

    invoke-interface {v2}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2512395
    :cond_1
    :goto_1
    goto :goto_0

    .line 2512396
    :cond_2
    iget-object v2, v0, LX/Hu6;->i:LX/HqN;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2512397
    iget-object v3, v0, LX/Hu6;->e:LX/7kl;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    .line 2512398
    iput-object v2, v3, LX/7kl;->d:Ljava/lang/String;

    .line 2512399
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    iget-object v1, v0, LX/Hu6;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2rb;

    invoke-static {v2, v1}, LX/7kq;->a(LX/0Px;LX/2rb;)Landroid/location/Location;

    move-result-object v1

    .line 2512400
    if-eqz v1, :cond_3

    .line 2512401
    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/Hu6;->a$redex0(LX/Hu6;Landroid/location/Location;Z)V

    .line 2512402
    :cond_3
    iget-object v2, v0, LX/Hu6;->g:LX/A5m;

    invoke-virtual {v2, p0, p0}, LX/A5m;->a(LX/0TF;LX/0TF;)V

    .line 2512403
    iget-object v2, v0, LX/Hu6;->d:LX/0y3;

    invoke-virtual {v2}, LX/0y3;->b()LX/1rv;

    move-result-object v2

    iget-object v2, v2, LX/1rv;->a:LX/0yG;

    sget-object v3, LX/0yG;->OKAY:LX/0yG;

    if-ne v2, v3, :cond_1

    .line 2512404
    iget-boolean v2, v0, LX/Hu6;->m:Z

    if-nez v2, :cond_4

    if-nez v1, :cond_4

    const/4 v2, 0x1

    :goto_2
    move v3, v2

    .line 2512405
    iget-object v2, v0, LX/Hu6;->e:LX/7kl;

    sget-object p0, LX/2tG;->REQUESTED:LX/2tG;

    invoke-virtual {v2, p0}, LX/7kl;->a(LX/2tG;)V

    .line 2512406
    iget-object v2, v0, LX/Hu6;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    new-instance p0, LX/Hu4;

    invoke-direct {p0, v0, v3}, LX/Hu4;-><init>(LX/Hu6;Z)V

    invoke-virtual {v2, p0}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->a(LX/0TF;)V

    .line 2512407
    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public static r(Lcom/facebook/composer/activity/ComposerFragment;ILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2512382
    sget-object v0, LX/0ge;->COMPOSER_ATTACH_MEDIA_CLICK:LX/0ge;

    sget-object v1, LX/CIp;->d:Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;LX/0ge;Ljava/lang/String;)V

    .line 2512383
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c()V

    .line 2512384
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aC:LX/73w;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v0

    iget-object v0, v0, LX/2rt;->analyticsName:Ljava/lang/String;

    .line 2512385
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 2512386
    const-string v3, "composer_type"

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2512387
    sget-object v3, LX/74R;->LAUNCHED_MULTIPICKER:LX/74R;

    const/4 v4, 0x0

    invoke-static {v1, v3, v2, v4}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2512388
    invoke-virtual {p0, p2, p1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2512389
    return-void
.end method

.method private s()V
    .locals 6

    .prologue
    .line 2512376
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 2512377
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2512378
    :goto_0
    return-void

    .line 2512379
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->t()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->setPostDelayStartTime(J)Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->a()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2512380
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->av:LX/1Ck;

    sget-object v1, LX/7mH;->TIMEOUT_AUTOTAGGING:LX/7mH;

    new-instance v2, LX/HqW;

    invoke-direct {v2, p0}, LX/HqW;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    new-instance v3, LX/HqX;

    invoke-direct {v3, p0}, LX/HqX;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2512381
    goto :goto_0
.end method

.method private s(ILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 2512357
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 2512358
    :cond_0
    :goto_0
    return-void

    .line 2512359
    :cond_1
    const-string v0, "selectedPublishMode"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5Rn;

    .line 2512360
    const-string v1, "scheduleTime"

    const-wide/16 v2, 0x0

    invoke-virtual {p2, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 2512361
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object v1

    if-ne v1, v0, :cond_2

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2512362
    :cond_2
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v2, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1, v0}, LX/0jL;->a(LX/5Rn;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    sget-object v2, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    if-ne v0, v2, :cond_3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :goto_1
    invoke-virtual {v1, v2}, LX/0jL;->a(Ljava/lang/Long;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 2512363
    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LX/5Rn;->getContentType()Ljava/lang/String;

    move-result-object v3

    .line 2512364
    sget-object p1, LX/0ge;->PUBLISH_MODE_OPTION_SELECTED:LX/0ge;

    invoke-static {p1, v1}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object p1

    .line 2512365
    iget-object p2, p1, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "post_option_type"

    invoke-virtual {p2, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2512366
    move-object p1, p1

    .line 2512367
    iget-object p2, p1, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object p1, p2

    .line 2512368
    iget-object p2, v2, LX/0gd;->a:LX/0Zb;

    invoke-interface {p2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2512369
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v1}, LX/2zG;->u()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2512370
    sget-object v1, LX/5Rn;->SCHEDULE_POST:LX/5Rn;

    if-ne v0, v1, :cond_4

    .line 2512371
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aL:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2512372
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2512373
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->O:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Hvy;

    invoke-virtual {v1, v4, v5}, LX/Hvy;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2512374
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 2512375
    :cond_4
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aL:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    goto/16 :goto_0
.end method

.method private t()V
    .locals 10

    .prologue
    .line 2512346
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupImplicitLocationPill"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512347
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v0}, LX/2zG;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2512348
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bi:LX/HuB;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->bn:Lcom/facebook/composer/header/ComposerHeaderViewController;

    .line 2512349
    iget-object v3, v2, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    move-object v2, v3

    .line 2512350
    iget-object v3, v2, Lcom/facebook/composer/header/ComposerHeaderView;->p:LX/0zw;

    move-object v2, v3

    .line 2512351
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    .line 2512352
    new-instance v4, LX/HuA;

    invoke-static {v1}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v5

    check-cast v5, LX/0gd;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v6

    check-cast v6, LX/0wM;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    move-object v9, v3

    check-cast v9, LX/0il;

    move-object v8, v2

    invoke-direct/range {v4 .. v9}, LX/HuA;-><init>(LX/0gd;LX/0wM;Landroid/content/res/Resources;LX/0zw;LX/0il;)V

    .line 2512353
    move-object v1, v4

    .line 2512354
    invoke-virtual {v0, v1}, LX/HvN;->a(LX/0iK;)V

    .line 2512355
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupImplicitLocationPill"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512356
    return-void
.end method

.method private v()V
    .locals 11

    .prologue
    .line 2511787
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupAlbumPill"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511788
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    .line 2511789
    iget-object v1, v0, LX/2zG;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/APA;

    invoke-static {v0}, LX/2zG;->K(LX/2zG;)LX/AQ9;

    move-result-object v2

    .line 2511790
    iget-object v1, v2, LX/AQ9;->Q:LX/ARN;

    move-object v2, v1

    .line 2511791
    invoke-static {v2}, LX/APA;->c(LX/ARN;)Z

    move-result v1

    move v0, v1

    .line 2511792
    if-eqz v0, :cond_0

    .line 2511793
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->f:LX/Hrs;

    .line 2511794
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->bO:LX/Hqs;

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->bn:Lcom/facebook/composer/header/ComposerHeaderViewController;

    .line 2511795
    iget-object v5, v4, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    move-object v4, v5

    .line 2511796
    iget-object v5, v4, Lcom/facebook/composer/header/ComposerHeaderView;->q:Landroid/view/ViewStub;

    move-object v4, v5

    .line 2511797
    new-instance v5, LX/Hrr;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v7

    check-cast v7, LX/0wM;

    move-object v8, v2

    check-cast v8, LX/0il;

    move-object v9, v3

    move-object v10, v4

    invoke-direct/range {v5 .. v10}, LX/Hrr;-><init>(Landroid/content/res/Resources;LX/0wM;LX/0il;LX/Hqs;Landroid/view/ViewStub;)V

    .line 2511798
    move-object v0, v5

    .line 2511799
    invoke-virtual {v1, v0}, LX/HvN;->a(LX/0iK;)V

    .line 2511800
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupAlbumPill"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2511801
    return-void
.end method

.method private x()V
    .locals 10

    .prologue
    .line 2512325
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupStickersView"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512326
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ba:LX/ARW;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    const v3, 0x7f0d0ae0

    invoke-virtual {v0, v3}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    new-instance v3, LX/Hqp;

    invoke-direct {v3, p0}, LX/Hqp;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512327
    new-instance v4, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;

    move-object v5, v2

    check-cast v5, LX/0il;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v8

    check-cast v8, LX/1Ad;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    move-object v6, v0

    move-object v7, v3

    invoke-direct/range {v4 .. v9}, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;-><init>(LX/0il;Landroid/view/ViewStub;LX/Hqp;LX/1Ad;Landroid/content/res/Resources;)V

    .line 2512328
    move-object v0, v4

    .line 2512329
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bo:Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;

    .line 2512330
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bo:Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;

    invoke-virtual {v0}, Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;->a()V

    .line 2512331
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bo:Lcom/facebook/composer/stickerpost/controller/ComposerStickerController;

    invoke-virtual {v0, v1}, LX/HvN;->a(LX/0iK;)V

    .line 2512332
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerSetupStickersView"

    invoke-virtual {v0, v1}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512333
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 2512255
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2512256
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2512257
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    .line 2512258
    iget-object v1, v0, LX/1Kj;->d:LX/11i;

    sget-object v2, LX/1Kj;->a:LX/0Pq;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 2512259
    if-eqz v1, :cond_0

    .line 2512260
    const-string v2, "ComposerDIPhase"

    const v4, -0x6c947c54

    invoke-static {v1, v2, v4}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v1

    const-string v2, "ComposerFragmentSetup"

    const v4, 0x5169f368

    invoke-static {v1, v2, v4}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v1

    const-string v2, "ComposerFragmentOnCreate"

    const v4, 0x79013f09    # 4.19428E34f

    invoke-static {v1, v2, v4}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2512261
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1Kj;->g:Z

    .line 2512262
    iput-boolean v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->bb:Z

    .line 2512263
    if-eqz p1, :cond_1

    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->Z(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2512264
    iput-boolean v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aP:Z

    .line 2512265
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aa()Lcom/facebook/ui/dialogs/FbDialogFragment;

    move-result-object v0

    .line 2512266
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2512267
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->l:LX/Hrd;

    .line 2512268
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2512269
    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->bR:LX/Ar6;

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2512270
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2512271
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2512272
    if-eqz p1, :cond_b

    .line 2512273
    const-string v4, "system_data"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    move-object v7, v4

    .line 2512274
    :goto_0
    if-eqz v7, :cond_c

    move v4, v5

    :goto_1
    const-string v8, "no data, savedInstanceState is %snull"

    new-array v9, v5, [Ljava/lang/Object;

    if-eqz p1, :cond_d

    const-string v5, "not "

    :goto_2
    aput-object v5, v9, v6

    invoke-static {v4, v8, v9}, LX/0PB;->checkArgument(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2512275
    iget-object v4, v0, LX/Hrd;->a:LX/ARX;

    invoke-interface {v4, v7, v2}, LX/ARX;->a(Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;LX/Ar6;)LX/HvN;

    move-result-object v4

    move-object v0, v4

    .line 2512276
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    .line 2512277
    new-instance v0, LX/Hre;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-direct {v0, v1}, LX/Hre;-><init>(LX/HvN;)V

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    .line 2512278
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    .line 2512279
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    .line 2512280
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0in;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AQ9;

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2512281
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    .line 2512282
    new-instance v1, LX/APf;

    check-cast v0, LX/0il;

    invoke-direct {v1, v0}, LX/APf;-><init>(LX/0il;)V

    .line 2512283
    move-object v0, v1

    .line 2512284
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bC:LX/APf;

    .line 2512285
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->t:LX/APZ;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    .line 2512286
    new-instance v6, LX/APY;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {v0}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v4

    check-cast v4, LX/1RW;

    invoke-static {v0}, LX/ATy;->b(LX/0QB;)LX/ATy;

    move-result-object v5

    check-cast v5, LX/ATy;

    invoke-direct {v6, v2, v1, v4, v5}, LX/APY;-><init>(LX/0ad;LX/0il;LX/1RW;LX/ATy;)V

    .line 2512287
    move-object v0, v6

    .line 2512288
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->u:LX/APY;

    .line 2512289
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->q()V

    .line 2512290
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aC:LX/73w;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/73w;->a(Ljava/lang/String;)V

    .line 2512291
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v0

    .line 2512292
    sget-object v1, LX/21D;->THIRD_PARTY_APP_VIA_INTENT:LX/21D;

    if-eq v0, v1, :cond_2

    sget-object v1, LX/21D;->THIRD_PARTY_APP_VIA_FB_API:LX/21D;

    if-ne v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->l(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2512293
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aC:LX/73w;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2512294
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 2512295
    const-string v4, "media_attachment_count"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2512296
    sget-object v4, LX/74R;->EXTERNAL_PHOTO:LX/74R;

    const/4 v5, 0x0

    invoke-static {v1, v4, v2, v5}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2512297
    :cond_3
    if-nez p1, :cond_a

    .line 2512298
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2512299
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_ADD_LOCATION:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2512300
    :cond_4
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2512301
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_FRIEND_TAG:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2512302
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    .line 2512303
    iget-object v1, v0, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xe0008

    const-string v3, "ComposerActionButtonPressed"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2512304
    iget-object v1, v0, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0xe0009

    const-string v3, "ComposerSelectedPrivacyAvailable"

    invoke-interface {v1, v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2512305
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aI:J

    .line 2512306
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2512307
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v2, LX/0ge;->COMPOSER_MINUTIAE:LX/0ge;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2512308
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 2512309
    :cond_6
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2512310
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2512311
    :cond_7
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->I:LX/APt;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cv:LX/HqS;

    invoke-virtual {v0, v1, v2}, LX/APt;->a(Ljava/lang/Object;LX/HqS;)LX/APs;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->v:LX/APs;

    .line 2512312
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->v:LX/APs;

    invoke-virtual {v0, v1}, LX/HvN;->a(LX/0iK;)V

    .line 2512313
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bJ:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ARb;

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->bK:LX/ARb;

    .line 2512314
    if-nez p1, :cond_8

    .line 2512315
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->r:LX/75Q;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/75Q;->a(Ljava/lang/String;)V

    .line 2512316
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->s:LX/75F;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/75F;->a(Ljava/lang/String;)V

    .line 2512317
    :cond_8
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    .line 2512318
    iget-object v1, v0, LX/1Kj;->d:LX/11i;

    sget-object v2, LX/1Kj;->a:LX/0Pq;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 2512319
    if-eqz v1, :cond_9

    .line 2512320
    const-string v2, "ComposerFragmentOnCreate"

    const v3, 0x492805d1

    invoke-static {v1, v2, v3}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2512321
    :cond_9
    return-void

    .line 2512322
    :cond_a
    iput-boolean v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->ai:Z

    goto/16 :goto_3

    .line 2512323
    :cond_b
    const-string v4, "extra_composer_system_data"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    move-object v7, v4

    goto/16 :goto_0

    :cond_c
    move v4, v6

    .line 2512324
    goto/16 :goto_1

    :cond_d
    const-string v5, ""

    goto/16 :goto_2
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 2512049
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2512050
    if-nez p3, :cond_0

    .line 2512051
    new-instance p3, Landroid/content/Intent;

    invoke-direct {p3}, Landroid/content/Intent;-><init>()V

    .line 2512052
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    invoke-virtual {v1, p1, p2, p3}, LX/AQ9;->a(IILandroid/content/Intent;)V

    .line 2512053
    sparse-switch p1, :sswitch_data_0

    :goto_0
    move v1, v0

    .line 2512054
    :goto_1
    if-eqz v0, :cond_e

    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->S(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2512055
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->R$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512056
    :cond_1
    :goto_2
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->Z(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    const/4 p1, 0x1

    const/4 v3, 0x0

    .line 2512057
    iget-boolean v2, v0, LX/ASX;->i:Z

    if-eqz v2, :cond_f

    move v2, v3

    .line 2512058
    :goto_3
    move v0, v2

    .line 2512059
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->f()Z

    move-result v0

    if-nez v0, :cond_2

    if-nez v1, :cond_2

    .line 2512060
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->ab$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512061
    :cond_2
    return-void

    .line 2512062
    :sswitch_0
    invoke-static {p0, p2, p3}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;ILandroid/content/Intent;)V

    move v1, v0

    .line 2512063
    goto :goto_1

    .line 2512064
    :sswitch_1
    invoke-direct {p0, p2, p3}, Lcom/facebook/composer/activity/ComposerFragment;->c(ILandroid/content/Intent;)V

    move v1, v0

    .line 2512065
    goto :goto_1

    .line 2512066
    :sswitch_2
    invoke-static {p0, p2, p3}, Lcom/facebook/composer/activity/ComposerFragment;->d(Lcom/facebook/composer/activity/ComposerFragment;ILandroid/content/Intent;)V

    move v1, v0

    .line 2512067
    goto :goto_1

    .line 2512068
    :sswitch_3
    invoke-direct {p0, p2, p3}, Lcom/facebook/composer/activity/ComposerFragment;->e(ILandroid/content/Intent;)V

    move v1, v0

    .line 2512069
    goto :goto_1

    .line 2512070
    :sswitch_4
    const/4 v6, 0x0

    .line 2512071
    const/4 v1, -0x1

    if-eq p2, v1, :cond_13

    .line 2512072
    :goto_4
    move v1, v0

    .line 2512073
    goto :goto_1

    .line 2512074
    :sswitch_5
    if-nez p2, :cond_16

    .line 2512075
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v4, LX/0ge;->COMPOSER_MINUTIAE_CANCEL:LX/0ge;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2512076
    :cond_3
    :goto_5
    move v1, v0

    .line 2512077
    goto :goto_1

    .line 2512078
    :sswitch_6
    if-nez p2, :cond_19

    .line 2512079
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v4, LX/0ge;->COMPOSER_MINUTIAE_ICON_PICKER_CANCEL:LX/0ge;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2512080
    :cond_4
    :goto_6
    move v1, v0

    .line 2512081
    goto :goto_1

    .line 2512082
    :sswitch_7
    invoke-direct {p0, p2, p3}, Lcom/facebook/composer/activity/ComposerFragment;->q(ILandroid/content/Intent;)V

    move v1, v0

    .line 2512083
    goto :goto_1

    .line 2512084
    :sswitch_8
    if-nez p2, :cond_1a

    .line 2512085
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v4, LX/0ge;->COMPOSER_ATTACH_MOVIE_CANCEL:LX/0ge;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2512086
    :cond_5
    :goto_7
    move v1, v0

    move v0, v2

    .line 2512087
    goto/16 :goto_1

    .line 2512088
    :sswitch_9
    const/4 v4, 0x1

    .line 2512089
    if-nez p2, :cond_1d

    .line 2512090
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bF:LX/HwU;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v1, v3}, LX/HwU;->a(LX/0il;)LX/HwT;

    move-result-object v1

    invoke-virtual {v1}, LX/HwT;->a()V

    .line 2512091
    :goto_8
    invoke-static {p0, v4}, Lcom/facebook/composer/activity/ComposerFragment;->d$redex0(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 2512092
    move v1, v0

    move v0, v2

    .line 2512093
    goto/16 :goto_1

    .line 2512094
    :sswitch_a
    const/4 v6, 0x1

    .line 2512095
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {p2, p3, v1}, LX/HwY;->a(ILandroid/content/Intent;LX/0Px;)LX/HwX;

    move-result-object v1

    .line 2512096
    if-nez v1, :cond_20

    .line 2512097
    :goto_9
    move v1, v0

    move v0, v2

    .line 2512098
    goto/16 :goto_1

    .line 2512099
    :sswitch_b
    const-string v1, "extra_are_media_items_modified"

    const/4 v3, 0x0

    invoke-virtual {p3, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2512100
    if-nez v1, :cond_21

    .line 2512101
    :goto_a
    move v1, v0

    move v0, v2

    .line 2512102
    goto/16 :goto_1

    .line 2512103
    :sswitch_c
    const/4 v1, -0x1

    if-ne p2, v1, :cond_6

    .line 2512104
    const-string v1, "transliterated_text"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2512105
    const-string v1, "transliterated_text"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2512106
    invoke-static {v3}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 2512107
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v5, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v1, v5}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1, v4}, LX/0jL;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 2512108
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-static {v3}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->aq:LX/3iT;

    invoke-static {v3, v4}, LX/8of;->a(LX/175;LX/3iT;)LX/8of;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->setStatusText(Ljava/lang/CharSequence;)V

    .line 2512109
    :cond_6
    move v1, v0

    .line 2512110
    goto/16 :goto_1

    .line 2512111
    :sswitch_d
    const/4 v5, 0x0

    .line 2512112
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v3, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v1, v3}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    .line 2512113
    const-string v3, "extra_canceled_creation"

    invoke-virtual {p3, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 2512114
    const/4 v3, -0x1

    if-ne p2, v3, :cond_26

    .line 2512115
    const-string v3, "extra_selected_album"

    invoke-static {p3, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2512116
    const-string v4, "extra_selected_album_is_new"

    invoke-virtual {p3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 2512117
    if-eqz v3, :cond_7

    .line 2512118
    iget-object v8, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object p1, LX/0ge;->COMPOSER_SELECT_ALBUM:LX/0ge;

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, p1, v4}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2512119
    :cond_7
    if-eqz v7, :cond_24

    move v4, v5

    .line 2512120
    :goto_b
    invoke-virtual {v1, v3}, LX/0jL;->b(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/0jL;

    move-result-object v5

    move-object v3, v5

    .line 2512121
    check-cast v3, LX/0jL;

    .line 2512122
    iget-object v5, v3, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v5}, LX/0Sh;->a()V

    .line 2512123
    iget-object v5, v3, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v5}, Lcom/facebook/composer/system/model/ComposerModelImpl;->isTargetAlbumNew()Z

    move-result v5

    if-eq v5, v7, :cond_9

    .line 2512124
    iget-object v5, v3, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v5, :cond_8

    .line 2512125
    iget-object v5, v3, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v5}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v5

    iput-object v5, v3, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2512126
    :cond_8
    iget-object v5, v3, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v5, v7}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setIsTargetAlbumNew(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2512127
    iget-object v5, v3, LX/0jL;->a:LX/0cA;

    sget-object v6, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v5, v6}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2512128
    :cond_9
    :goto_c
    iget-object v3, v1, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->a()V

    .line 2512129
    iget-object v3, v1, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->b()Z

    move-result v3

    if-eq v3, v4, :cond_b

    .line 2512130
    iget-object v3, v1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v3, :cond_a

    .line 2512131
    iget-object v3, v1, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v3

    iput-object v3, v1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2512132
    :cond_a
    iget-object v3, v1, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v3, v4}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->b(Z)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2512133
    iget-object v3, v1, LX/0jL;->a:LX/0cA;

    sget-object v5, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v3, v5}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2512134
    :cond_b
    invoke-virtual {v1}, LX/0jL;->a()V

    .line 2512135
    move v1, v0

    .line 2512136
    goto/16 :goto_1

    .line 2512137
    :sswitch_e
    invoke-direct {p0, p2, p3}, Lcom/facebook/composer/activity/ComposerFragment;->s(ILandroid/content/Intent;)V

    move v1, v0

    .line 2512138
    goto/16 :goto_1

    .line 2512139
    :sswitch_f
    const/4 v1, -0x1

    if-ne p2, v1, :cond_27

    .line 2512140
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v1

    invoke-interface {v1}, LX/CfL;->b()V

    .line 2512141
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2512142
    const/4 v1, 0x1

    .line 2512143
    :goto_d
    move v1, v1

    .line 2512144
    goto/16 :goto_1

    .line 2512145
    :sswitch_10
    if-nez p2, :cond_28

    .line 2512146
    :goto_e
    move v1, v0

    .line 2512147
    goto/16 :goto_1

    .line 2512148
    :sswitch_11
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2512149
    if-nez p2, :cond_29

    .line 2512150
    :goto_f
    move v1, v0

    .line 2512151
    goto/16 :goto_1

    .line 2512152
    :sswitch_12
    const/4 v1, -0x1

    if-ne p2, v1, :cond_c

    .line 2512153
    const-string v1, "extra_birthday_sticker_data"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/stickers/model/Sticker;

    .line 2512154
    if-eqz v1, :cond_2b

    invoke-static {v1}, LX/5Rb;->a(Lcom/facebook/stickers/model/Sticker;)Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v1

    move-object v3, v1

    .line 2512155
    :goto_10
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v4, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v1, v4}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1, v3}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 2512156
    :cond_c
    move v1, v0

    .line 2512157
    goto/16 :goto_1

    .line 2512158
    :sswitch_13
    const/4 v1, -0x1

    if-ne p2, v1, :cond_d

    .line 2512159
    const-string v1, "extra_composer_page_data"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2512160
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v4, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v3, v4}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v3

    check-cast v3, LX/0jL;

    invoke-virtual {v3, v1}, LX/0jL;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 2512161
    :cond_d
    move v1, v0

    .line 2512162
    goto/16 :goto_1

    .line 2512163
    :sswitch_14
    invoke-direct {p0, p2, p3}, Lcom/facebook/composer/activity/ComposerFragment;->k(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 2512164
    :cond_e
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isInlineSproutsOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    .line 2512165
    goto/16 :goto_2

    .line 2512166
    :cond_f
    iget-object v2, v0, LX/ASX;->c:LX/0iA;

    sget-object p2, LX/ASX;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class p3, LX/3l5;

    invoke-virtual {v2, p2, p3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/3l5;

    .line 2512167
    if-eqz v2, :cond_10

    move v2, p1

    .line 2512168
    goto/16 :goto_3

    .line 2512169
    :cond_10
    iget-object v2, v0, LX/ASX;->j:Ljava/util/EnumSet;

    invoke-virtual {v2}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_11
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ASW;

    .line 2512170
    iget-object p3, v0, LX/ASX;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {p3, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Huu;

    .line 2512171
    if-eqz v2, :cond_11

    .line 2512172
    invoke-virtual {v2}, LX/Huu;->b()Z

    move-result v2

    if-eqz v2, :cond_11

    move v2, p1

    .line 2512173
    goto/16 :goto_3

    :cond_12
    move v2, v3

    .line 2512174
    goto/16 :goto_3

    .line 2512175
    :cond_13
    const-string v1, "startDate"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2512176
    const-string v3, "endDate"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2512177
    const-string v4, "isCurrent"

    invoke-virtual {p3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 2512178
    const-string v4, "hasGraduated"

    invoke-virtual {p3, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 2512179
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object p1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v4, p1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v4

    check-cast v4, LX/0jL;

    new-instance p1, LX/5RH;

    invoke-direct {p1}, LX/5RH;-><init>()V

    .line 2512180
    iput-object v1, p1, LX/5RH;->a:Lcom/facebook/uicontrib/datepicker/Date;

    .line 2512181
    move-object v1, p1

    .line 2512182
    iput-object v3, v1, LX/5RH;->b:Lcom/facebook/uicontrib/datepicker/Date;

    .line 2512183
    move-object v1, v1

    .line 2512184
    iput-boolean v5, v1, LX/5RH;->c:Z

    .line 2512185
    move-object v1, v1

    .line 2512186
    invoke-virtual {v1}, LX/5RH;->a()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v1

    .line 2512187
    iget-object v3, v4, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v3}, LX/0Sh;->a()V

    .line 2512188
    iget-object v3, v4, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v3

    invoke-static {v3, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    .line 2512189
    iget-object v3, v4, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v3, :cond_14

    .line 2512190
    iget-object v3, v4, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v3

    iput-object v3, v4, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2512191
    :cond_14
    iget-object v3, v4, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v3, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setDateInfo(Lcom/facebook/ipc/composer/model/ComposerDateInfo;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2512192
    iget-object v3, v4, LX/0jL;->a:LX/0cA;

    sget-object v5, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v3, v5}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2512193
    :cond_15
    move-object v1, v4

    .line 2512194
    check-cast v1, LX/0jL;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->i()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    invoke-virtual {v3}, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->l()LX/7l3;

    move-result-object v3

    .line 2512195
    iput-boolean v6, v3, LX/7l3;->h:Z

    .line 2512196
    move-object v3, v3

    .line 2512197
    invoke-virtual {v3}, LX/7l3;->a()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0jL;->a(Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    goto/16 :goto_4

    .line 2512198
    :cond_16
    const/4 v1, -0x1

    if-ne p2, v1, :cond_3

    .line 2512199
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v4, LX/0ge;->COMPOSER_MINUTIAE:LX/0ge;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2512200
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    if-eqz v1, :cond_18

    const/4 v1, 0x1

    move v3, v1

    .line 2512201
    :goto_11
    const-string v1, "minutiae_object"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 2512202
    invoke-static {p0, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 2512203
    const-string v1, "sticker_object"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 2512204
    invoke-static {p0, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/ipc/composer/model/ComposerStickerData;)V

    .line 2512205
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->O$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512206
    const-string v1, "extra_place"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 2512207
    invoke-static {p0, p2, p3}, Lcom/facebook/composer/activity/ComposerFragment;->d(Lcom/facebook/composer/activity/ComposerFragment;ILandroid/content/Intent;)V

    .line 2512208
    :cond_17
    if-eqz v3, :cond_3

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2512209
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v4, LX/0ge;->COMPOSER_MINUTIAE_REMOVE:LX/0ge;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 2512210
    :cond_18
    const/4 v1, 0x0

    move v3, v1

    goto :goto_11

    .line 2512211
    :cond_19
    const/4 v1, -0x1

    if-ne p2, v1, :cond_4

    .line 2512212
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v4, LX/0ge;->COMPOSER_MINUTIAE_ICON_PICKER_UPDATE:LX/0ge;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2512213
    const-string v1, "minutiae_object"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 2512214
    invoke-static {p0, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 2512215
    const-string v1, "sticker_object"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 2512216
    invoke-static {p0, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/ipc/composer/model/ComposerStickerData;)V

    .line 2512217
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->O$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    goto/16 :goto_6

    .line 2512218
    :cond_1a
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->j(LX/0Px;)Z

    move-result v1

    .line 2512219
    const/4 v3, 0x4

    if-ne p2, v3, :cond_1c

    .line 2512220
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2512221
    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {p0, v3, v4, v5}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;ZZ)V

    .line 2512222
    if-eqz v1, :cond_1b

    .line 2512223
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v4, LX/0ge;->COMPOSER_ATTACH_MOVIE_REMOVE:LX/0ge;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 2512224
    :cond_1b
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v4, LX/0ge;->COMPOSER_ATTACH_MOVIE_CANCEL:LX/0ge;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 2512225
    :cond_1c
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v1}, LX/5Qz;->C()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2512226
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->U$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    goto/16 :goto_7

    .line 2512227
    :cond_1d
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v5

    .line 2512228
    const-string v1, "extra_slideshow_data"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 2512229
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v6, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v3, v6}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v3

    check-cast v3, LX/0jL;

    invoke-virtual {v3, v1}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 2512230
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v1

    if-ne v1, v5, :cond_1e

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v1

    sget-object v3, LX/5RF;->SLIDESHOW:LX/5RF;

    if-ne v1, v3, :cond_1f

    :cond_1e
    move v1, v4

    :goto_12
    invoke-static {p0, p3, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;Landroid/content/Intent;Z)V

    goto/16 :goto_8

    :cond_1f
    const/4 v1, 0x0

    goto :goto_12

    .line 2512231
    :cond_20
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v3

    iget-object v4, v1, LX/HwX;->b:LX/0Px;

    sget-object v5, LX/03R;->NO:LX/03R;

    invoke-virtual {v3, v4, v5}, LX/ATO;->a(LX/0Px;LX/03R;)V

    .line 2512232
    iget-object v1, v1, LX/HwX;->a:LX/0Px;

    invoke-static {p0, v1, v6, v6}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;ZZ)V

    goto/16 :goto_9

    .line 2512233
    :cond_21
    const-string v1, "extra_photo_items_list"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2512234
    if-eqz v1, :cond_23

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_23

    .line 2512235
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v3

    .line 2512236
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_22
    :goto_13
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_23

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/base/media/PhotoItem;

    .line 2512237
    invoke-static {v3, v4}, LX/ATO;->b(LX/ATO;Lcom/facebook/photos/base/media/PhotoItem;)LX/ASn;

    move-result-object v6

    .line 2512238
    if-eqz v6, :cond_22

    .line 2512239
    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    const/4 p3, 0x0

    invoke-interface {v6, v4, p3}, LX/ASn;->a(Lcom/facebook/ipc/media/data/MediaData;Z)V

    goto :goto_13

    .line 2512240
    :cond_23
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->U$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    goto/16 :goto_a

    .line 2512241
    :cond_24
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v4

    if-eqz v4, :cond_25

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->isTargetAlbumNew()Z

    move-result v4

    if-eqz v4, :cond_25

    .line 2512242
    const/4 v4, 0x1

    goto/16 :goto_b

    :cond_25
    move v4, v6

    goto/16 :goto_b

    :cond_26
    move v4, v6

    goto/16 :goto_c

    :cond_27
    const/4 v1, 0x0

    goto/16 :goto_d

    .line 2512243
    :cond_28
    const-string v1, "extra_slideshow_data"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    .line 2512244
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v4, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v3, v4}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v3

    check-cast v3, LX/0jL;

    invoke-virtual {v3, v1}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerSlideshowData;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 2512245
    const/4 v1, 0x1

    invoke-static {p0, p3, v1}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;Landroid/content/Intent;Z)V

    goto/16 :goto_e

    .line 2512246
    :cond_29
    const/4 v1, -0x1

    if-ne p2, v1, :cond_2a

    move v1, v3

    :goto_14
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "We need to add support for modal underwood returning code "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2512247
    invoke-static {p3}, Lcom/facebook/composer/ui/underwood/modal/ModalUnderwoodActivity;->c(Landroid/content/Intent;)Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;

    move-result-object v1

    .line 2512248
    iget-object v5, v1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->a:LX/0Px;

    move-object v5, v5

    .line 2512249
    invoke-static {p0, v5, v3, v4}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;ZZ)V

    .line 2512250
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v3

    .line 2512251
    iget-object v4, v1, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodResult;->b:LX/0P1;

    move-object v1, v4

    .line 2512252
    invoke-virtual {v3, v1}, LX/ATO;->a(LX/0P1;)V

    goto/16 :goto_f

    :cond_2a
    move v1, v4

    .line 2512253
    goto :goto_14

    .line 2512254
    :cond_2b
    const/4 v1, 0x0

    move-object v3, v1

    goto/16 :goto_10

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_0
        0x4 -> :sswitch_5
        0x5 -> :sswitch_1
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_4
        0xb -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0x10 -> :sswitch_3
        0x11 -> :sswitch_12
        0x12 -> :sswitch_13
        0x7c -> :sswitch_8
        0x7e -> :sswitch_a
        0x7f -> :sswitch_9
        0x80 -> :sswitch_f
        0x82 -> :sswitch_10
        0x83 -> :sswitch_b
        0x84 -> :sswitch_11
        0x85 -> :sswitch_14
    .end sparse-switch
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2512045
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2512046
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2512047
    invoke-static {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->b(Lcom/facebook/composer/activity/ComposerFragment;Landroid/view/View;)V

    .line 2512048
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, -0x5c8214e7

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v5

    .line 2511957
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2511958
    :cond_0
    const/4 v0, 0x0

    const/16 v1, 0x2b

    const v2, -0x3e599ccf

    invoke-static {v6, v1, v2, v5}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2511959
    :goto_0
    return-object v0

    .line 2511960
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v1, "ComposerCreateViewPhase"

    invoke-virtual {v0, v1}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511961
    const v0, 0x7f030350

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2511962
    const-string v2, "composer"

    invoke-static {v0, v2, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 2511963
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->I()Z

    move-result v1

    if-nez v1, :cond_8

    move v2, v3

    .line 2511964
    :goto_1
    new-instance v6, LX/0zw;

    const v1, 0x7f0d0af4

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    invoke-direct {v6, v1}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v6, p0, Lcom/facebook/composer/activity/ComposerFragment;->aL:LX/0zw;

    .line 2511965
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v6, "ComposerSetupScrollView"

    invoke-virtual {v1, v6}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511966
    const v1, 0x7f0d0aee

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/ScrollingAwareScrollView;

    iput-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aJ:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 2511967
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aJ:Lcom/facebook/widget/ScrollingAwareScrollView;

    new-instance v6, LX/Hqc;

    invoke-direct {v6, p0}, LX/Hqc;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-virtual {v1, v6}, Lcom/facebook/widget/ScrollingAwareScrollView;->a(LX/4oV;)V

    .line 2511968
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aJ:Lcom/facebook/widget/ScrollingAwareScrollView;

    new-instance v6, LX/HwQ;

    iget-object v7, p0, Lcom/facebook/composer/activity/ComposerFragment;->cO:LX/HrF;

    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->T(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result p1

    iget-object p2, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-direct {v6, v7, p1, p2}, LX/HwQ;-><init>(LX/HrF;ZLX/0il;)V

    invoke-virtual {v1, v6}, Lcom/facebook/widget/ScrollingAwareScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2511969
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v6, "ComposerSetupScrollView"

    invoke-virtual {v1, v6}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2511970
    if-eqz p3, :cond_9

    move v1, v3

    :goto_2
    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/composer/activity/ComposerFragment;->a(Landroid/view/ViewGroup;ZZ)V

    .line 2511971
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v6, "ComposerSetupHeaderViewController"

    invoke-virtual {v1, v6}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511972
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v1}, LX/2zG;->l()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2511973
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aw:LX/0ad;

    sget-short v6, LX/1EB;->J:S

    const/4 v7, 0x0

    invoke-interface {v1, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2511974
    const v1, 0x7f0d0adf

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    const v6, 0x7f0d0a8f

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2511975
    :goto_3
    iget-object v6, p0, Lcom/facebook/composer/activity/ComposerFragment;->bm:LX/HtZ;

    iget-object v7, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object p1, p0, Lcom/facebook/composer/activity/ComposerFragment;->cC:LX/Hr0;

    invoke-virtual {v6, v7, v1, p1}, LX/HtZ;->a(Ljava/lang/Object;Landroid/view/ViewStub;LX/Hr0;)Lcom/facebook/composer/header/ComposerHeaderViewController;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bn:Lcom/facebook/composer/header/ComposerHeaderViewController;

    .line 2511976
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v6, p0, Lcom/facebook/composer/activity/ComposerFragment;->bn:Lcom/facebook/composer/header/ComposerHeaderViewController;

    invoke-virtual {v1, v6}, LX/HvN;->a(LX/0iK;)V

    .line 2511977
    :cond_2
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v6, "ComposerSetupHeaderViewController"

    invoke-virtual {v1, v6}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2511978
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->t()V

    .line 2511979
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->v()V

    .line 2511980
    if-eqz p3, :cond_a

    :goto_4
    invoke-direct {p0, v0, v3}, Lcom/facebook/composer/activity/ComposerFragment;->a(Landroid/view/ViewGroup;Z)V

    .line 2511981
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupTagExpansionPill"

    invoke-virtual {v1, v3}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511982
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v1}, LX/2zG;->B()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2511983
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->bj:LX/Hux;

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v6, p0, Lcom/facebook/composer/activity/ComposerFragment;->cn:LX/HqJ;

    iget-object v7, p0, Lcom/facebook/composer/activity/ComposerFragment;->bn:Lcom/facebook/composer/header/ComposerHeaderViewController;

    .line 2511984
    iget-object p1, v7, Lcom/facebook/composer/header/ComposerHeaderViewController;->b:Lcom/facebook/composer/header/ComposerHeaderView;

    move-object v7, p1

    .line 2511985
    iget-object p1, v7, Lcom/facebook/composer/header/ComposerHeaderView;->n:LX/0zw;

    move-object v7, p1

    .line 2511986
    iget-object p1, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    invoke-virtual {v3, v4, v6, v7, p1}, LX/Hux;->a(Ljava/lang/Object;LX/HqJ;LX/0zw;LX/ASX;)LX/Huw;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/HvN;->a(LX/0iK;)V

    .line 2511987
    :cond_3
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupTagExpansionPill"

    invoke-virtual {v1, v3}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2511988
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupPostComposition"

    invoke-virtual {v1, v3}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511989
    new-instance v3, LX/0zw;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bX:Landroid/view/View;

    const v4, 0x7f0d0a59

    invoke-static {v1, v4}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    new-instance v4, LX/Hqm;

    invoke-direct {v4, p0}, LX/Hqm;-><init>(Lcom/facebook/composer/activity/ComposerFragment;)V

    invoke-direct {v3, v1, v4}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->aM:LX/0zw;

    .line 2511990
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupPostComposition"

    invoke-virtual {v1, v3}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2511991
    invoke-direct {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->d(Landroid/view/ViewGroup;)V

    .line 2511992
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->x()V

    .line 2511993
    invoke-direct {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->e(Landroid/view/ViewGroup;)V

    .line 2511994
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupTitle"

    invoke-virtual {v1, v3}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511995
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->w:LX/ARw;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->cz:LX/Hqt;

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v1, v3, v4}, LX/ARw;->a(LX/Hqt;Ljava/lang/Object;)LX/ARv;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->x:LX/ARv;

    .line 2511996
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->ae:LX/APn;

    const v1, 0x7f0d0a7f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    iget-object v6, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v7, p0, Lcom/facebook/composer/activity/ComposerFragment;->x:LX/ARv;

    iget-object p1, p0, Lcom/facebook/composer/activity/ComposerFragment;->cB:LX/Hqz;

    invoke-virtual {v4, v1, v6, v7, p1}, LX/APn;->a(Landroid/view/ViewStub;Ljava/lang/Object;LX/ARv;LX/Hqz;)LX/APm;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/HvN;->a(LX/0iK;)V

    .line 2511997
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupTitle"

    invoke-virtual {v1, v3}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2511998
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupTitleIndicatorBars"

    invoke-virtual {v1, v3}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2511999
    const v1, 0x7f0d31d6

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2512000
    const v3, 0x7f0d0c8b

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewStub;

    .line 2512001
    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->ci:LX/11u;

    .line 2512002
    iput-object v1, v4, LX/11u;->e:Landroid/view/ViewStub;

    .line 2512003
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ci:LX/11u;

    invoke-virtual {v1, v3}, LX/11u;->b(Landroid/view/ViewStub;)V

    .line 2512004
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupTitleIndicatorBars"

    invoke-virtual {v1, v3}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512005
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->X()V

    .line 2512006
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupPlace"

    invoke-virtual {v1, v3}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512007
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2512008
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2512009
    :cond_4
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupPlace"

    invoke-virtual {v1, v3}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512010
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupTagging"

    invoke-virtual {v1, v3}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512011
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    const/4 v1, 0x1

    move v3, v1

    .line 2512012
    :goto_5
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v1

    if-nez v1, :cond_5

    if-eqz v3, :cond_6

    .line 2512013
    :cond_5
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->O$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512014
    if-eqz v3, :cond_6

    .line 2512015
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->W$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2512016
    :cond_6
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupTagging"

    invoke-virtual {v1, v3}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512017
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupHintController"

    invoke-virtual {v1, v3}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512018
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->bd:LX/AQ2;

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v6, p0, Lcom/facebook/composer/activity/ComposerFragment;->bS:LX/Hra;

    .line 2512019
    new-instance p1, LX/AQ1;

    check-cast v4, LX/0il;

    invoke-static {v3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-direct {p1, v4, v6, v7}, LX/AQ1;-><init>(LX/0il;LX/Hra;Landroid/content/res/Resources;)V

    .line 2512020
    move-object v3, p1

    .line 2512021
    invoke-virtual {v1, v3}, LX/HvN;->a(LX/0iK;)V

    .line 2512022
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupHintController"

    invoke-virtual {v1, v3}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512023
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->B()V

    .line 2512024
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupPluginViews"

    invoke-virtual {v1, v3}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512025
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->au:Landroid/view/ViewStub;

    if-eqz v1, :cond_7

    .line 2512026
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->au:Landroid/view/ViewStub;

    invoke-virtual {v1, v3}, LX/AQ9;->a(Landroid/view/ViewStub;)V

    .line 2512027
    :cond_7
    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    const v1, 0x7f0d0af5

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v3, v1}, LX/AQ9;->c(Landroid/view/ViewStub;)V

    .line 2512028
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v3, "ComposerSetupPluginViews"

    invoke-virtual {v1, v3}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512029
    invoke-direct {p0, v0, v2}, Lcom/facebook/composer/activity/ComposerFragment;->b(Landroid/view/ViewGroup;Z)V

    .line 2512030
    invoke-direct {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->a(Landroid/view/ViewGroup;)V

    .line 2512031
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v2, "ComposerSetupTagExpansionInfoForMentions"

    invoke-virtual {v1, v2}, LX/1Kj;->a(Ljava/lang/String;)V

    .line 2512032
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    new-instance v2, LX/Hug;

    iget-object v3, p0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    .line 2512033
    iget-object v6, v4, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    move-object v4, v6

    .line 2512034
    invoke-direct {v2, v3, v4}, LX/Hug;-><init>(LX/0il;LX/8p2;)V

    invoke-virtual {v1, v2}, LX/HvN;->a(LX/0iK;)V

    .line 2512035
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v2, "ComposerSetupTagExpansionInfoForMentions"

    invoke-virtual {v1, v2}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512036
    invoke-direct {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->f(Landroid/view/ViewGroup;)V

    .line 2512037
    invoke-direct {p0, v0}, Lcom/facebook/composer/activity/ComposerFragment;->g(Landroid/view/ViewGroup;)V

    .line 2512038
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v2, "ComposerCreateViewPhase"

    invoke-virtual {v1, v2}, LX/1Kj;->b(Ljava/lang/String;)V

    .line 2512039
    const v1, -0x1eb8a767

    invoke-static {v1, v5}, LX/02F;->f(II)V

    goto/16 :goto_0

    :cond_8
    move v2, v4

    .line 2512040
    goto/16 :goto_1

    :cond_9
    move v1, v4

    .line 2512041
    goto/16 :goto_2

    :cond_a
    move v3, v4

    .line 2512042
    goto/16 :goto_4

    .line 2512043
    :cond_b
    const v1, 0x7f0d0ade

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    goto/16 :goto_3

    .line 2512044
    :cond_c
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_5
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4c689fd6    # 6.098108E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2511942
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    .line 2511943
    iget-object v2, v0, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0xe0008

    const-string v5, "ComposerActionButtonPressed"

    invoke-interface {v2, v4, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 2511944
    iget-object v2, v0, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0xe0009

    const-string v5, "ComposerSelectedPrivacyAvailable"

    invoke-interface {v2, v4, v5}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;)V

    .line 2511945
    iget-object v2, v0, LX/1Kj;->d:LX/11i;

    sget-object v4, LX/1Kj;->b:LX/0Pq;

    invoke-interface {v2, v4}, LX/11i;->d(LX/0Pq;)V

    .line 2511946
    iget-object v2, v0, LX/1Kj;->e:LX/0id;

    invoke-virtual {v2}, LX/0id;->a()V

    .line 2511947
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->az:LX/Hu6;

    if-eqz v0, :cond_0

    .line 2511948
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->az:LX/Hu6;

    .line 2511949
    iget-object v2, v0, LX/Hu6;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    invoke-virtual {v2}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->b()V

    .line 2511950
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->av:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2511951
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-virtual {v0}, LX/HvN;->g()V

    .line 2511952
    iget-boolean v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->M:Z

    if-nez v0, :cond_1

    .line 2511953
    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->r:LX/75Q;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/75Q;->b(Ljava/lang/String;)V

    .line 2511954
    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->s:LX/75F;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/75F;->b(Ljava/lang/String;)V

    .line 2511955
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2511956
    const/16 v0, 0x2b

    const v2, 0x5fe89328

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xbafd4c1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2511928
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    if-eqz v1, :cond_0

    .line 2511929
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    .line 2511930
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/ASX;->a(Z)V

    .line 2511931
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/ASX;->i:Z

    .line 2511932
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    .line 2511933
    iget-object v2, v1, LX/ASX;->f:LX/0Rf;

    .line 2511934
    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3l6;

    .line 2511935
    invoke-interface {v4}, LX/3l6;->d()V

    goto :goto_0

    .line 2511936
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bI:LX/ATO;

    if-eqz v1, :cond_1

    .line 2511937
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->bI:LX/ATO;

    invoke-virtual {v1}, LX/ATO;->e()V

    .line 2511938
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    if-eqz v1, :cond_2

    .line 2511939
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    sget-object v2, LX/5L2;->ON_DESTROY_VIEW:LX/5L2;

    invoke-virtual {v1, v2}, LX/HvN;->a(LX/5L2;)V

    .line 2511940
    :cond_2
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2511941
    const/16 v1, 0x2b

    const v2, 0x30625285

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 11

    .prologue
    const/4 v8, 0x2

    const/16 v0, 0x2a

    const v1, -0x5c2d7911

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2511882
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2511883
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v3

    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    .line 2511884
    iget-wide v9, v0, LX/ATO;->T:D

    move-wide v4, v9

    .line 2511885
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    .line 2511886
    invoke-virtual {v0}, LX/ATO;->h()Ljava/util/List;

    move-result-object v6

    .line 2511887
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 2511888
    const/high16 v6, -0x40800000    # -1.0f

    .line 2511889
    :goto_0
    move v6, v6

    .line 2511890
    invoke-virtual/range {v1 .. v6}, LX/0gd;->a(Ljava/lang/String;LX/2rt;DF)V

    .line 2511891
    :cond_0
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    invoke-virtual {v0}, LX/ATO;->a()LX/0P1;

    move-result-object v1

    .line 2511892
    if-eqz v1, :cond_3

    invoke-virtual {v1}, LX/0P1;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2511893
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v2, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 2511894
    iget-object v2, v0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 2511895
    iget-object v2, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->s()LX/0P1;

    move-result-object v2

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2511896
    iget-object v2, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v2, :cond_1

    .line 2511897
    iget-object v2, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v2

    iput-object v2, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2511898
    :cond_1
    iget-object v2, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v2, v1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->a(LX/0P1;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2511899
    iget-object v2, v0, LX/0jL;->a:LX/0cA;

    sget-object v3, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v2, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2511900
    :cond_2
    move-object v0, v0

    .line 2511901
    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511902
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->av:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2511903
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/ASX;->a(Z)V

    .line 2511904
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->G()V

    .line 2511905
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    const-class v1, Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v1}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0jL;->d(Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2511906
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->cA:LX/Be0;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->b(LX/Be0;)V

    .line 2511907
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aA:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Huy;

    .line 2511908
    invoke-static {v0}, LX/Huy;->c(LX/Huy;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2511909
    iget-object v1, v0, LX/Huy;->d:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2511910
    const/4 v1, 0x0

    iput-object v1, v0, LX/Huy;->d:Ljava/util/concurrent/ScheduledFuture;

    .line 2511911
    :cond_4
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ci:LX/11u;

    invoke-virtual {v0}, LX/11u;->f()V

    .line 2511912
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    .line 2511913
    iget-object v1, v0, LX/ASX;->e:LX/ASB;

    const/4 v3, 0x0

    .line 2511914
    invoke-static {}, LX/AS4;->values()[LX/AS4;

    move-result-object v5

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_5

    aget-object v2, v5, v4

    .line 2511915
    iget-object v0, v1, LX/ASB;->b:LX/0iA;

    iget-object v2, v2, LX/AS4;->interstitialId:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/0iA;->a(Ljava/lang/String;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/3l5;

    .line 2511916
    invoke-interface {v2}, LX/3l6;->d()V

    .line 2511917
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 2511918
    :cond_5
    iget-object v2, v1, LX/ASB;->m:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    :goto_2
    if-ge v3, v4, :cond_6

    iget-object v2, v1, LX/ASB;->m:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3l4;

    .line 2511919
    invoke-interface {v2}, LX/3l6;->d()V

    .line 2511920
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2511921
    :cond_6
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    sget-object v1, LX/5L2;->ON_PAUSE:LX/5L2;

    invoke-virtual {v0, v1}, LX/HvN;->a(LX/5L2;)V

    .line 2511922
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2511923
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->al:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7kp;

    .line 2511924
    const-string v1, "ComposerFragmentPausing"

    invoke-static {v0, v1}, LX/7kp;->c(LX/7kp;Ljava/lang/String;)V

    .line 2511925
    const/16 v0, 0x2b

    const v1, 0x76317f27    # 9.000151E32f

    invoke-static {v8, v0, v1, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2511926
    :cond_7
    sget-object v9, LX/1zb;->a:LX/1zb;

    move-object v9, v9

    .line 2511927
    invoke-virtual {v9, v6}, LX/1sm;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    goto/16 :goto_0
.end method

.method public final onResume()V
    .locals 11

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x6b03f228

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2511838
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2511839
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->cA:LX/Be0;

    invoke-virtual {v0, v2}, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->a(LX/Be0;)V

    .line 2511840
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0iv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2511841
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    invoke-virtual {v0}, LX/ASX;->c()V

    .line 2511842
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->k(Lcom/facebook/composer/activity/ComposerFragment;)LX/HqE;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/HqE;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2511843
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    invoke-virtual {v0}, LX/ASX;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2511844
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->G()V

    .line 2511845
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aA:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Huy;

    .line 2511846
    invoke-static {v0}, LX/Huy;->c(LX/Huy;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2511847
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ci:LX/11u;

    invoke-virtual {v0}, LX/11u;->d()V

    .line 2511848
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    .line 2511849
    iget-object v2, v0, LX/1Kj;->d:LX/11i;

    sget-object v3, LX/1Kj;->a:LX/0Pq;

    invoke-interface {v2, v3}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v2

    .line 2511850
    if-eqz v2, :cond_2

    .line 2511851
    const-string v3, "ComposerFragmentSetup"

    const v6, -0x30b8972e

    invoke-static {v2, v3, v6}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v2

    const-string v3, "ComposerRenderPhase"

    const v6, 0x73a5cc0c

    invoke-static {v2, v3, v6}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2511852
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    .line 2511853
    iget-object v2, v0, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v3, 0xe0005

    const-string v6, "ComposerLaunchTTIExternalShare"

    invoke-interface {v2, v3, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2511854
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    .line 2511855
    iget-object v2, v0, LX/1Kj;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    const v3, 0xe0006

    const-string v6, "ComposerLaunchTTIPlatformShare"

    invoke-interface {v2, v3, v6}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2511856
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    sget-object v2, LX/5L2;->ON_RESUME:LX/5L2;

    invoke-virtual {v0, v2}, LX/HvN;->a(LX/5L2;)V

    .line 2511857
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->b(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 2511858
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aQ:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EQY;

    invoke-interface {v0}, LX/EQY;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2511859
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aR:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/composer/activity/ComposerFragment$58;

    invoke-direct {v3, p0, v2}, Lcom/facebook/composer/activity/ComposerFragment$58;-><init>(Lcom/facebook/composer/activity/ComposerFragment;LX/0Px;)V

    const v2, 0xec299f1

    invoke-static {v0, v3, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2511860
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aM:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aM:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/activity/PostCompositionView;

    invoke-virtual {v0}, Lcom/facebook/composer/activity/PostCompositionView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_5

    .line 2511861
    :cond_4
    invoke-static {p0, v4}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 2511862
    :cond_5
    iput-boolean v4, p0, Lcom/facebook/composer/activity/ComposerFragment;->M:Z

    .line 2511863
    const/16 v0, 0x2b

    const v2, -0x7315d958

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2511864
    :cond_6
    invoke-static {v0}, LX/Huy;->f(LX/Huy;)LX/Hrc;

    move-result-object v6

    .line 2511865
    if-eqz v6, :cond_1

    const/4 v8, 0x0

    .line 2511866
    iget-object v7, v6, LX/Hrc;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2511867
    iget-boolean v9, v7, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v7, v9

    .line 2511868
    if-nez v7, :cond_7

    move v7, v8

    .line 2511869
    :goto_1
    move v6, v7

    .line 2511870
    if-eqz v6, :cond_1

    .line 2511871
    new-instance v6, Lcom/facebook/composer/savedsession/ComposerSavedSessionController$1;

    invoke-direct {v6, v0}, Lcom/facebook/composer/savedsession/ComposerSavedSessionController$1;-><init>(LX/Huy;)V

    .line 2511872
    iget-object v7, v0, LX/Huy;->a:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v8, 0x5

    sget-object v10, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v7, v6, v8, v9, v10}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v6

    iput-object v6, v0, LX/Huy;->d:Ljava/util/concurrent/ScheduledFuture;

    goto/16 :goto_0

    .line 2511873
    :cond_7
    iget-object v7, v6, LX/Hrc;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v7, v7, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2511874
    iget-object v9, v7, LX/AQ9;->w:LX/ARN;

    move-object v7, v9

    .line 2511875
    if-eqz v7, :cond_8

    .line 2511876
    iget-object v7, v6, LX/Hrc;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v7, v7, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2511877
    iget-object v8, v7, LX/AQ9;->w:LX/ARN;

    move-object v7, v8

    .line 2511878
    invoke-interface {v7}, LX/ARN;->a()Z

    move-result v7

    goto :goto_1

    .line 2511879
    :cond_8
    iget-object v7, v6, LX/Hrc;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v7, v7, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v7}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v7}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v7

    if-eqz v7, :cond_9

    move v7, v8

    .line 2511880
    goto :goto_1

    .line 2511881
    :cond_9
    const/4 v7, 0x1

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2511827
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2511828
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    .line 2511829
    const-string v1, "system_data"

    .line 2511830
    new-instance v2, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;

    iget-object v3, v0, LX/HvN;->b:LX/0jJ;

    .line 2511831
    iget-object v4, v3, LX/0jJ;->h:Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-object v3, v4

    .line 2511832
    iget-object v4, v0, LX/HvN;->e:LX/AQ9;

    invoke-virtual {v4}, LX/AQ9;->a()LX/B5f;

    move-result-object v4

    .line 2511833
    iget-object v0, v4, LX/B5f;->b:Ljava/lang/String;

    move-object v4, v0

    .line 2511834
    invoke-direct {v2, v3, v4}, Lcom/facebook/composer/system/systemimpl/ComposerSystemDataImpl;-><init>(Lcom/facebook/composer/system/model/ComposerModelImpl;Ljava/lang/String;)V

    move-object v2, v2

    .line 2511835
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2511836
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->M:Z

    .line 2511837
    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3aac3c01

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2511821
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2511822
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->v:LX/APs;

    .line 2511823
    iget-object v2, v0, LX/APs;->a:LX/93Q;

    invoke-virtual {v2}, LX/93Q;->a()V

    .line 2511824
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->aA:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Huy;

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment;->ch:LX/Hrc;

    .line 2511825
    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    invoke-direct {v4, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, v0, LX/Huy;->c:Ljava/lang/ref/WeakReference;

    .line 2511826
    const/16 v0, 0x2b

    const v2, -0x2c511cb3

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x47675910    # 59225.062f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2511817
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment;->v:LX/APs;

    .line 2511818
    iget-object v2, v1, LX/APs;->a:LX/93Q;

    invoke-virtual {v2}, LX/93Q;->e()V

    .line 2511819
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2511820
    const/16 v1, 0x2b

    const v2, 0xd5ac4b3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2511802
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2511803
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    .line 2511804
    iget-object v1, v0, LX/ATO;->ah:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    .line 2511805
    iget-boolean v0, v1, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->p:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v1, v0

    .line 2511806
    move v0, v1

    .line 2511807
    if-eqz v0, :cond_0

    .line 2511808
    invoke-direct {p0}, Lcom/facebook/composer/activity/ComposerFragment;->s()V

    .line 2511809
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2511810
    iget-object v1, v0, LX/AQ9;->M:LX/ARN;

    move-object v0, v1

    .line 2511811
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2511812
    iget-object v1, v0, LX/AQ9;->M:LX/ARN;

    move-object v0, v1

    .line 2511813
    invoke-interface {v0}, LX/ARN;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2511814
    invoke-static {p0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v0

    const/4 v1, 0x0

    .line 2511815
    iput-boolean v1, v0, LX/ATO;->R:Z

    .line 2511816
    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
