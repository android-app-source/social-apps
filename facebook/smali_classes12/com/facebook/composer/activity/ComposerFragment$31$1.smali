.class public final Lcom/facebook/composer/activity/ComposerFragment$31$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Hqe;


# direct methods
.method public constructor <init>(LX/Hqe;)V
    .locals 0

    .prologue
    .line 2510616
    iput-object p1, p0, Lcom/facebook/composer/activity/ComposerFragment$31$1;->a:LX/Hqe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 2510617
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerFragment$31$1;->a:LX/Hqe;

    iget-object v0, v0, LX/Hqe;->c:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerFragment$31$1;->a:LX/Hqe;

    iget-boolean v1, v1, LX/Hqe;->a:Z

    iget-object v2, p0, Lcom/facebook/composer/activity/ComposerFragment$31$1;->a:LX/Hqe;

    iget-boolean v2, v2, LX/Hqe;->b:Z

    const/4 v6, 0x1

    .line 2510618
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v3, v3

    .line 2510619
    check-cast v3, Landroid/view/ViewGroup;

    .line 2510620
    if-nez v3, :cond_0

    .line 2510621
    :goto_0
    return-void

    .line 2510622
    :cond_0
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v4, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v3, v4}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v3

    check-cast v3, LX/0jL;

    .line 2510623
    invoke-virtual {v3, v6}, LX/0jL;->j(Z)LX/0jL;

    move-result-object v4

    move-object v3, v4

    .line 2510624
    check-cast v3, LX/0jL;

    invoke-virtual {v3}, LX/0jL;->a()V

    .line 2510625
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v4, "ComposerSetupTransliterationController"

    invoke-virtual {v3, v4}, LX/1Kj;->c(Ljava/lang/String;)V

    .line 2510626
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    .line 2510627
    iget-object v4, v3, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->d:Landroid/view/ViewStub;

    move-object v3, v4

    .line 2510628
    if-eqz v3, :cond_1

    .line 2510629
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->cy:LX/HqV;

    iget-object v5, v0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v7, v0, Lcom/facebook/composer/activity/ComposerFragment;->aK:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    .line 2510630
    iget-object v8, v7, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->d:Landroid/view/ViewStub;

    move-object v7, v8

    .line 2510631
    new-instance v8, LX/Hvs;

    check-cast v5, LX/0il;

    invoke-direct {v8, v4, v5, v7}, LX/Hvs;-><init>(LX/HqV;LX/0il;Landroid/view/ViewStub;)V

    .line 2510632
    move-object v4, v8

    .line 2510633
    invoke-virtual {v3, v4}, LX/HvN;->a(LX/0iK;)V

    .line 2510634
    :cond_1
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v4, "ComposerSetupTransliterationController"

    invoke-virtual {v3, v4}, LX/1Kj;->d(Ljava/lang/String;)V

    .line 2510635
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    invoke-virtual {v3, v4}, LX/HvN;->a(LX/0iK;)V

    .line 2510636
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    invoke-virtual {v3}, LX/ASX;->c()V

    .line 2510637
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    sget-object v4, LX/5L2;->ON_FIRST_DRAW:LX/5L2;

    invoke-virtual {v3, v4}, LX/HvN;->a(LX/5L2;)V

    .line 2510638
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->k(Lcom/facebook/composer/activity/ComposerFragment;)LX/HqE;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/HqE;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2510639
    if-nez v1, :cond_2

    if-nez v2, :cond_2

    .line 2510640
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->J(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2510641
    :cond_2
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->bc:LX/Hu7;

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    iget-object v5, v0, Lcom/facebook/composer/activity/ComposerFragment;->cr:LX/HqN;

    invoke-virtual {v3, v4, v5}, LX/Hu7;->a(Ljava/lang/Object;LX/HqN;)LX/Hu6;

    move-result-object v3

    iput-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->az:LX/Hu6;

    .line 2510642
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2510643
    iget-object v4, v3, LX/AQ9;->C:LX/ARN;

    move-object v3, v4

    .line 2510644
    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2510645
    iget-object v4, v3, LX/AQ9;->C:LX/ARN;

    move-object v3, v4

    .line 2510646
    invoke-interface {v3}, LX/ARN;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2510647
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->az:LX/Hu6;

    .line 2510648
    iput-boolean v6, v3, LX/Hu6;->m:Z

    .line 2510649
    :cond_3
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->r(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2510650
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v4, "ComposerMinutiaePrefetch"

    invoke-virtual {v3, v4}, LX/1Kj;->c(Ljava/lang/String;)V

    .line 2510651
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v4

    .line 2510652
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/92o;

    .line 2510653
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v3, v4}, LX/92o;->a(Ljava/lang/String;)V

    .line 2510654
    const/4 v4, 0x1

    .line 2510655
    iput-boolean v4, v3, LX/92o;->p:Z

    .line 2510656
    invoke-virtual {v3}, LX/92o;->a()V

    .line 2510657
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    const-string v4, "ComposerMinutiaePrefetch"

    invoke-virtual {v3, v4}, LX/1Kj;->d(Ljava/lang/String;)V

    .line 2510658
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 2510659
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v4

    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    iget-wide v5, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v4, v3}, LX/CfL;->a(Ljava/lang/Long;)V

    .line 2510660
    :cond_4
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v3

    sget-object v4, LX/2rt;->SHARE:LX/2rt;

    if-ne v3, v4, :cond_5

    .line 2510661
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v4

    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v3}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    invoke-interface {v4, v3}, LX/CfL;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)V

    .line 2510662
    :cond_5
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->e:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    .line 2510663
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2510664
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->i:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7lT;

    .line 2510665
    iget-object v7, v3, LX/7lT;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0aG;

    const-string v8, "sync_contacts_partial"

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    sget-object v10, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    invoke-static {v11}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v11

    const v12, 0x1c6c5beb

    invoke-static/range {v7 .. v12}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v7

    invoke-interface {v7}, LX/1MF;->start()LX/1ML;

    .line 2510666
    :cond_6
    iget-object v3, v0, Lcom/facebook/composer/activity/ComposerFragment;->ak:LX/1Kj;

    .line 2510667
    iget-object v4, v3, LX/1Kj;->d:LX/11i;

    sget-object v5, LX/1Kj;->b:LX/0Pq;

    invoke-interface {v4, v5}, LX/11i;->b(LX/0Pq;)V

    .line 2510668
    goto/16 :goto_0

    .line 2510669
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_1
.end method
