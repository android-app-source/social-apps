.class public Lcom/facebook/composer/activity/PostCompositionView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/0wd;

.field public c:LX/Hqk;

.field private d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public e:Z

.field public f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2513587
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2513588
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->c:LX/Hqk;

    .line 2513589
    invoke-direct {p0}, Lcom/facebook/composer/activity/PostCompositionView;->b()V

    .line 2513590
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2513583
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2513584
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->c:LX/Hqk;

    .line 2513585
    invoke-direct {p0}, Lcom/facebook/composer/activity/PostCompositionView;->b()V

    .line 2513586
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2513579
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2513580
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->c:LX/Hqk;

    .line 2513581
    invoke-direct {p0}, Lcom/facebook/composer/activity/PostCompositionView;->b()V

    .line 2513582
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/composer/activity/PostCompositionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/composer/activity/PostCompositionView;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->a:LX/0wW;

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 2513572
    const-class v0, Lcom/facebook/composer/activity/PostCompositionView;

    invoke-static {v0, p0}, Lcom/facebook/composer/activity/PostCompositionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2513573
    const v0, 0x7f030fef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2513574
    const v0, 0x7f0d2667

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2513575
    iget-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/62X;

    invoke-virtual {p0}, Lcom/facebook/composer/activity/PostCompositionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/composer/activity/PostCompositionView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0cae

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, LX/62X;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2513576
    invoke-direct {p0}, Lcom/facebook/composer/activity/PostCompositionView;->f()V

    .line 2513577
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/composer/activity/PostCompositionView;->setVisibility(I)V

    .line 2513578
    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2513528
    new-instance v0, LX/Hrm;

    invoke-direct {v0, p0}, LX/Hrm;-><init>(Lcom/facebook/composer/activity/PostCompositionView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/composer/activity/PostCompositionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2513529
    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 2513570
    iget-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4044000000000000L    # 40.0

    const-wide/high16 v4, 0x401c000000000000L    # 7.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, LX/0wd;->b(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->b:LX/0wd;

    .line 2513571
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2513562
    iput-boolean v1, p0, Lcom/facebook/composer/activity/PostCompositionView;->e:Z

    .line 2513563
    iget-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->b:LX/0wd;

    .line 2513564
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 2513565
    move-object v0, v0

    .line 2513566
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2513567
    iget-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->c:LX/Hqk;

    if-eqz v0, :cond_0

    .line 2513568
    iget-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->c:LX/Hqk;

    invoke-interface {v0}, LX/Hqk;->a()V

    .line 2513569
    :cond_0
    return-void
.end method

.method public final a(LX/Hq4;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 2513547
    iget-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2513548
    iput-object v0, p1, LX/Hq4;->f:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2513549
    new-instance v4, LX/62V;

    iget-object v5, p1, LX/Hq4;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/62V;-><init>(Landroid/content/Context;)V

    iput-object v4, p1, LX/Hq4;->e:LX/62V;

    .line 2513550
    iget-object v4, p1, LX/Hq4;->e:LX/62V;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/1P1;->b(I)V

    .line 2513551
    iget-object v4, p1, LX/Hq4;->f:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v5, p1, LX/Hq4;->e:LX/62V;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2513552
    iget-object v4, p1, LX/Hq4;->f:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v5, p1, LX/Hq4;->c:LX/Hq3;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2513553
    if-eqz p2, :cond_0

    .line 2513554
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->e:Z

    .line 2513555
    iget-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->b:LX/0wd;

    .line 2513556
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 2513557
    move-object v0, v0

    .line 2513558
    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2513559
    :goto_0
    invoke-virtual {p0, v1}, Lcom/facebook/composer/activity/PostCompositionView;->setVisibility(I)V

    .line 2513560
    return-void

    .line 2513561
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->b:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x1712df01

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2513543
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2513544
    invoke-direct {p0}, Lcom/facebook/composer/activity/PostCompositionView;->e()V

    .line 2513545
    iget-object v1, p0, Lcom/facebook/composer/activity/PostCompositionView;->b:LX/0wd;

    new-instance v2, LX/Hrn;

    invoke-direct {v2, p0}, LX/Hrn;-><init>(Lcom/facebook/composer/activity/PostCompositionView;)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2513546
    const/16 v1, 0x2d

    const v2, 0x455b292a

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x4793a9bf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2513539
    iget-object v1, p0, Lcom/facebook/composer/activity/PostCompositionView;->b:LX/0wd;

    if-eqz v1, :cond_0

    .line 2513540
    iget-object v1, p0, Lcom/facebook/composer/activity/PostCompositionView;->b:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->k()LX/0wd;

    .line 2513541
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2513542
    const/16 v1, 0x2d

    const v2, 0x23546617

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2513532
    iget v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->f:I

    if-nez v0, :cond_0

    .line 2513533
    invoke-virtual {p0}, Lcom/facebook/composer/activity/PostCompositionView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v1, 0x7f0d0a7f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2513534
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->f:I

    .line 2513535
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2513536
    invoke-virtual {p0}, Lcom/facebook/composer/activity/PostCompositionView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/composer/activity/PostCompositionView;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lcom/facebook/composer/activity/PostCompositionView;->f:I

    sub-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/composer/activity/PostCompositionView;->setMeasuredDimension(II)V

    .line 2513537
    iget v0, p0, Lcom/facebook/composer/activity/PostCompositionView;->f:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/composer/activity/PostCompositionView;->setY(F)V

    .line 2513538
    return-void
.end method

.method public setOnHideListener(LX/Hqk;)V
    .locals 0
    .param p1    # LX/Hqk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2513530
    iput-object p1, p0, Lcom/facebook/composer/activity/PostCompositionView;->c:LX/Hqk;

    .line 2513531
    return-void
.end method
