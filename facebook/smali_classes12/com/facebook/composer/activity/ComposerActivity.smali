.class public final Lcom/facebook/composer/activity/ComposerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# static fields
.field private static final p:Ljava/lang/String;

.field private static s:Z


# instance fields
.field private q:Lcom/facebook/composer/activity/ComposerFragment;

.field public r:LX/1Kj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2510546
    const-class v0, Lcom/facebook/composer/activity/ComposerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/activity/ComposerActivity;->p:Ljava/lang/String;

    .line 2510547
    const/4 v0, 0x0

    sput-boolean v0, Lcom/facebook/composer/activity/ComposerActivity;->s:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2510517
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/composer/activity/ComposerActivity;

    invoke-static {v0}, LX/1Kj;->a(LX/0QB;)LX/1Kj;

    move-result-object v0

    check-cast v0, LX/1Kj;

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerActivity;->r:LX/1Kj;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2510518
    const-string v0, "composer"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2510519
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2510520
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2510521
    invoke-static {p0, p0}, Lcom/facebook/composer/activity/ComposerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2510522
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerActivity;->r:LX/1Kj;

    sget-boolean v1, Lcom/facebook/composer/activity/ComposerActivity;->s:Z

    .line 2510523
    iget-object v4, v0, LX/1Kj;->d:LX/11i;

    sget-object v5, LX/1Kj;->a:LX/0Pq;

    invoke-interface {v4, v5}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v4

    .line 2510524
    if-eqz v4, :cond_0

    .line 2510525
    const-string v5, "RefHandOff"

    const v6, -0x7a39c1cc

    invoke-static {v4, v5, v6}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v4

    const-string v5, "ComposerLaunchPhase"

    const/4 v6, 0x0

    const-string v7, "is_warm_launch"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v7

    const v8, 0x43cbc670

    invoke-static {v4, v5, v6, v7, v8}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    move-result-object v5

    if-eqz v1, :cond_2

    const-string v4, "warm_launch"

    :goto_0
    const v6, -0x70087dd1

    invoke-static {v5, v4, v6}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    move-result-object v4

    const-string v5, "ComposerDIPhase"

    const v6, -0x84f2c83

    invoke-static {v4, v5, v6}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2510526
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/composer/activity/ComposerActivity;->s:Z

    .line 2510527
    invoke-virtual {p0}, Lcom/facebook/composer/activity/ComposerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2510528
    const v0, 0x7f030305

    invoke-virtual {p0, v0}, Lcom/facebook/composer/activity/ComposerActivity;->setContentView(I)V

    .line 2510529
    if-nez p1, :cond_1

    .line 2510530
    invoke-virtual {p0}, Lcom/facebook/composer/activity/ComposerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2510531
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2510532
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2510533
    new-instance v4, Lcom/facebook/composer/activity/ComposerFragment;

    invoke-direct {v4}, Lcom/facebook/composer/activity/ComposerFragment;-><init>()V

    .line 2510534
    invoke-virtual {v4, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2510535
    move-object v0, v4

    .line 2510536
    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerActivity;->q:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2510537
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0a58

    iget-object v4, p0, Lcom/facebook/composer/activity/ComposerActivity;->q:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-virtual {v0, v1, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    .line 2510538
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2510539
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerActivity;->q:Lcom/facebook/composer/activity/ComposerFragment;

    const v1, 0x7f0d0a58

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    .line 2510540
    iput-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->bX:Landroid/view/View;

    .line 2510541
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerActivity;->r:LX/1Kj;

    sget-object v1, Lcom/facebook/composer/activity/ComposerActivity;->p:Ljava/lang/String;

    .line 2510542
    iget-object v4, v0, LX/1Kj;->e:LX/0id;

    invoke-virtual {v4, v1, v2, v3}, LX/0id;->b(Ljava/lang/String;J)V

    .line 2510543
    return-void

    .line 2510544
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0a58

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/activity/ComposerFragment;

    iput-object v0, p0, Lcom/facebook/composer/activity/ComposerActivity;->q:Lcom/facebook/composer/activity/ComposerFragment;

    goto :goto_1

    .line 2510545
    :cond_2
    const-string v4, "cold_launch"

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 6

    .prologue
    .line 2510453
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerActivity;->q:Lcom/facebook/composer/activity/ComposerFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerActivity;->q:Lcom/facebook/composer/activity/ComposerFragment;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2510454
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2510455
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aM:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/activity/PostCompositionView;

    invoke-virtual {v1}, Lcom/facebook/composer/activity/PostCompositionView;->a()V

    .line 2510456
    :goto_0
    move v0, v4

    .line 2510457
    if-nez v0, :cond_0

    .line 2510458
    :goto_1
    return-void

    .line 2510459
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_1

    .line 2510460
    :cond_1
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aF:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0qj;

    invoke-virtual {v1}, LX/0qj;->a()Z

    move-result v1

    if-nez v1, :cond_2

    move v2, v3

    .line 2510461
    :goto_2
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->P:LX/2zG;

    invoke-virtual {v1}, LX/2zG;->d()Z

    move-result v1

    move v1, v1

    .line 2510462
    if-eqz v1, :cond_8

    .line 2510463
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->S(Lcom/facebook/composer/activity/ComposerFragment;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2510464
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->R$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    goto :goto_0

    :cond_2
    move v2, v4

    .line 2510465
    goto :goto_2

    .line 2510466
    :cond_3
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2510467
    iget-object v5, v1, LX/AQ9;->H:LX/AQ4;

    move-object v1, v5

    .line 2510468
    if-eqz v1, :cond_4

    .line 2510469
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2510470
    iget-object v2, v1, LX/AQ9;->H:LX/AQ4;

    move-object v1, v2

    .line 2510471
    invoke-interface {v1}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2510472
    :goto_3
    invoke-static {v0, v1, v3}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2510473
    :cond_4
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2510474
    const v1, 0x7f081435

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 2510475
    :cond_5
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->hasPrivacyChanged()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2510476
    const v1, 0x7f081434

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 2510477
    :cond_6
    if-eqz v2, :cond_7

    .line 2510478
    const v1, 0x7f081432

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 2510479
    :cond_7
    const v1, 0x7f081431

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    .line 2510480
    :cond_8
    if-eqz v2, :cond_9

    .line 2510481
    const v1, 0x7f081432

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/facebook/composer/activity/ComposerFragment;->a(Lcom/facebook/composer/activity/ComposerFragment;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 2510482
    :cond_9
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->ap:LX/0Xl;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v5, "backFromComposer"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v5, "backFromComposer"

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2510483
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2510484
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v5, "extra_media_items"

    invoke-virtual {v2, v5, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v2

    .line 2510485
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->L:LX/AQ9;

    .line 2510486
    iget-object v5, v1, LX/AQ9;->N:LX/AQ4;

    move-object v1, v5

    .line 2510487
    if-eqz v1, :cond_a

    .line 2510488
    invoke-interface {v1}, LX/AQ4;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2510489
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v4, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2510490
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->p(Lcom/facebook/composer/activity/ComposerFragment;)LX/CfL;

    move-result-object v1

    invoke-interface {v1}, LX/CfL;->b()V

    .line 2510491
    invoke-static {v0, v4}, Lcom/facebook/composer/activity/ComposerFragment;->g(Lcom/facebook/composer/activity/ComposerFragment;Z)V

    .line 2510492
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->cc:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9c3;

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v5

    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v2}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1, v5, v2}, LX/9c3;->c(Ljava/lang/String;I)V

    .line 2510493
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->k(Lcom/facebook/composer/activity/ComposerFragment;)LX/HqE;

    move-result-object v1

    invoke-virtual {v1}, LX/HqE;->b()V

    .line 2510494
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/ATO;->b(Z)V

    .line 2510495
    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->aQ(Lcom/facebook/composer/activity/ComposerFragment;)LX/ATO;

    move-result-object v1

    invoke-virtual {v1}, LX/ATO;->l()V

    .line 2510496
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    sget-object v2, LX/5L2;->ON_USER_CANCEL:LX/5L2;

    invoke-virtual {v1, v2}, LX/HvN;->a(LX/5L2;)V

    move v4, v3

    .line 2510497
    goto/16 :goto_0
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x78e2951d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2510498
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerActivity;->r:LX/1Kj;

    sget-object v2, Lcom/facebook/composer/activity/ComposerActivity;->p:Ljava/lang/String;

    .line 2510499
    iget-object v4, v1, LX/1Kj;->e:LX/0id;

    invoke-virtual {v4, v2}, LX/0id;->e(Ljava/lang/String;)V

    .line 2510500
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2510501
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerActivity;->r:LX/1Kj;

    sget-object v2, Lcom/facebook/composer/activity/ComposerActivity;->p:Ljava/lang/String;

    .line 2510502
    iget-object v4, v1, LX/1Kj;->e:LX/0id;

    invoke-virtual {v4, v2}, LX/0id;->f(Ljava/lang/String;)V

    .line 2510503
    const/16 v1, 0x23

    const v2, 0xac2ff3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x811253a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2510504
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerActivity;->r:LX/1Kj;

    sget-object v2, Lcom/facebook/composer/activity/ComposerActivity;->p:Ljava/lang/String;

    .line 2510505
    iget-object v4, v1, LX/1Kj;->e:LX/0id;

    invoke-virtual {v4, v2}, LX/0id;->c(Ljava/lang/String;)V

    .line 2510506
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2510507
    iget-object v1, p0, Lcom/facebook/composer/activity/ComposerActivity;->r:LX/1Kj;

    sget-object v2, Lcom/facebook/composer/activity/ComposerActivity;->p:Ljava/lang/String;

    .line 2510508
    iget-object v4, v1, LX/1Kj;->e:LX/0id;

    invoke-virtual {v4, v2}, LX/0id;->d(Ljava/lang/String;)V

    .line 2510509
    const/16 v1, 0x23

    const v2, -0x18240432

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onUserInteraction()V
    .locals 3

    .prologue
    .line 2510510
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerActivity;->q:Lcom/facebook/composer/activity/ComposerFragment;

    if-eqz v0, :cond_0

    .line 2510511
    iget-object v0, p0, Lcom/facebook/composer/activity/ComposerActivity;->q:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2510512
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v1, v1

    .line 2510513
    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->k:LX/ASX;

    if-nez v1, :cond_1

    .line 2510514
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onUserInteraction()V

    .line 2510515
    return-void

    .line 2510516
    :cond_1
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v2, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/0jL;->i(Z)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    goto :goto_0
.end method
