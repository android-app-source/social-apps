.class public Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Hvl;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2519852
    const-class v0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;

    const-class v1, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2519817
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2519818
    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const p1, 0x7f0311f6

    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2519819
    const v0, 0x7f0d0629

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2519820
    return-void
.end method

.method public static a(Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;I)LX/1af;
    .locals 3

    .prologue
    .line 2519843
    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v0

    .line 2519844
    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0ce5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, LX/4Ab;->a(F)LX/4Ab;

    .line 2519845
    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2519846
    iput-object v0, v1, LX/1Uo;->u:LX/4Ab;

    .line 2519847
    move-object v0, v1

    .line 2519848
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2519849
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2519850
    move-object v0, v0

    .line 2519851
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public setEnabled(Z)V
    .locals 0

    .prologue
    .line 2519842
    return-void
.end method

.method public setSelected(Z)V
    .locals 0

    .prologue
    .line 2519841
    return-void
.end method

.method public setTextStyle(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)V
    .locals 4

    .prologue
    .line 2519821
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 2519822
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2519823
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/87X;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    .line 2519824
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2519825
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 2519826
    new-instance v2, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-direct {v2, p1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2519827
    iput-object v0, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2519828
    move-object v2, v2

    .line 2519829
    invoke-virtual {v2}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    move-object v2, v2

    .line 2519830
    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2519831
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    sget-object p1, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2519832
    :goto_0
    return-void

    .line 2519833
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2519834
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v1

    .line 2519835
    iget-object v2, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p0, v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->a(Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;I)LX/1af;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2519836
    iget-object v2, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object p1, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2519837
    goto :goto_0

    .line 2519838
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p0, v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->a(Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;I)LX/1af;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2519839
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/composer/textstyle/RichTextStylePickerItemViewV2;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2519840
    goto :goto_0
.end method
