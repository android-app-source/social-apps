.class public Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Hvl;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2519771
    const-class v0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;

    const-class v1, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2519772
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2519773
    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const p1, 0x7f0311f5

    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2519774
    const v0, 0x7f0d0629

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2519775
    const v0, 0x7f0d2a2c

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->c:Landroid/widget/TextView;

    .line 2519776
    const v0, 0x7f0d156c

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->d:Landroid/view/View;

    .line 2519777
    return-void
.end method

.method public static a(Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;I)LX/1af;
    .locals 3

    .prologue
    .line 2519778
    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v0

    .line 2519779
    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0cd6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, LX/4Ab;->a(F)LX/4Ab;

    .line 2519780
    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a009e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2519781
    iput v1, v0, LX/4Ab;->f:I

    .line 2519782
    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0cdb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, LX/4Ab;->c(F)LX/4Ab;

    .line 2519783
    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2519784
    iput-object v0, v1, LX/1Uo;->u:LX/4Ab;

    .line 2519785
    move-object v0, v1

    .line 2519786
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2519787
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2519788
    move-object v0, v0

    .line 2519789
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public setSelected(Z)V
    .locals 2

    .prologue
    .line 2519790
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->d:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2519791
    return-void

    .line 2519792
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setTextStyle(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)V
    .locals 5

    .prologue
    .line 2519793
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 2519794
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2519795
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/87X;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    .line 2519796
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2519797
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 2519798
    new-instance v2, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2519799
    iput-object v0, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2519800
    move-object v2, v2

    .line 2519801
    invoke-virtual {v2}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    move-object v2, v2

    .line 2519802
    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2519803
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2519804
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 2519805
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2519806
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v0

    invoke-static {v0}, LX/87X;->a(LX/5RY;)I

    move-result v0

    .line 2519807
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->c:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2519808
    return-void

    .line 2519809
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2519810
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v1

    .line 2519811
    iget-object v2, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p0, v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->a(Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;I)LX/1af;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2519812
    iget-object v2, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2519813
    goto :goto_0

    .line 2519814
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p0, v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->a(Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;I)LX/1af;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2519815
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/composer/textstyle/RichTextStylePickerItemView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2519816
    goto :goto_0
.end method
