.class public final Lcom/facebook/composer/textstyle/RichTextStylePickerController$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Hvj;


# direct methods
.method public constructor <init>(LX/Hvj;)V
    .locals 0

    .prologue
    .line 2519412
    iput-object p1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerController$5;->a:LX/Hvj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2519403
    iget-object v0, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerController$5;->a:LX/Hvj;

    .line 2519404
    iput-boolean v1, v0, LX/Hvj;->E:Z

    .line 2519405
    iget-object v0, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerController$5;->a:LX/Hvj;

    iget-object v0, v0, LX/Hvj;->r:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2519406
    iget-object v0, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerController$5;->a:LX/Hvj;

    .line 2519407
    const/4 v1, 0x0

    :goto_0
    iget-object v2, v0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v2}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2519408
    iget-object v2, v0, LX/Hvj;->q:Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-virtual {v2, v1}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v0, v1}, LX/Hvj;->a(LX/Hvj;I)F

    move-result v3

    neg-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 2519409
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2519410
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerController$5;->a:LX/Hvj;

    invoke-static {v0}, LX/Hvj;->l(LX/Hvj;)V

    .line 2519411
    return-void
.end method
