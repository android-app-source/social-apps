.class public Lcom/facebook/composer/textstyle/RichTextStylePickerView;
.super Lcom/facebook/widget/recyclerview/BetterRecyclerView;
.source ""


# instance fields
.field public i:LX/0tO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2519882
    invoke-direct {p0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;)V

    .line 2519883
    invoke-direct {p0, p1}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->a(Landroid/content/Context;)V

    .line 2519884
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2519879
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2519880
    invoke-direct {p0, p1}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->a(Landroid/content/Context;)V

    .line 2519881
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2519876
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2519877
    invoke-direct {p0, p1}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->a(Landroid/content/Context;)V

    .line 2519878
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2519885
    const-class v0, Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-static {v0, p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2519886
    new-instance v0, LX/1P1;

    invoke-static {p1}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v1

    invoke-direct {v0, v3, v1}, LX/1P1;-><init>(IZ)V

    .line 2519887
    iget-object v1, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->i:LX/0tO;

    invoke-virtual {v1}, LX/0tO;->l()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 2519888
    invoke-virtual {p0, v3}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->setBackgroundColor(I)V

    .line 2519889
    new-instance v1, LX/Hvm;

    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0ce6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-direct {v1, p0, v2}, LX/Hvm;-><init>(Lcom/facebook/composer/textstyle/RichTextStylePickerView;I)V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2519890
    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2519891
    return-void

    .line 2519892
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a009a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->setBackgroundColor(I)V

    .line 2519893
    new-instance v1, LX/Hvm;

    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0cd7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-direct {v1, p0, v2}, LX/Hvm;-><init>(Lcom/facebook/composer/textstyle/RichTextStylePickerView;I)V

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/composer/textstyle/RichTextStylePickerView;

    invoke-static {v0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v0

    check-cast v0, LX/0tO;

    iput-object v0, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->i:LX/0tO;

    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 5

    .prologue
    .line 2519863
    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 2519864
    iget-object v0, p0, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->i:LX/0tO;

    const/4 v3, 0x0

    .line 2519865
    iget-object v1, v0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->y:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2519866
    iget-object v1, v0, LX/0tO;->c:Landroid/content/Context;

    iget-object v2, v0, LX/0tO;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0ce7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-static {v1, v2}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    .line 2519867
    :goto_0
    move v0, v1

    .line 2519868
    int-to-float v0, v0

    .line 2519869
    invoke-virtual {p0}, Lcom/facebook/composer/textstyle/RichTextStylePickerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 2519870
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 2519871
    invoke-super {p0, p1, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->onMeasure(II)V

    .line 2519872
    return-void

    .line 2519873
    :cond_0
    iget-object v1, v0, LX/0tO;->a:LX/0ad;

    sget-short v2, LX/0wk;->q:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2519874
    iget-object v1, v0, LX/0tO;->a:LX/0ad;

    sget v2, LX/0wk;->s:F

    iget-object v3, v0, LX/0tO;->c:Landroid/content/Context;

    iget-object v4, v0, LX/0tO;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p2, 0x7f0b0cd3

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-static {v3, v4}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v3

    int-to-float v3, v3

    invoke-interface {v1, v2, v3}, LX/0ad;->a(FF)F

    move-result v1

    float-to-int v1, v1

    goto :goto_0

    .line 2519875
    :cond_1
    iget-object v1, v0, LX/0tO;->c:Landroid/content/Context;

    iget-object v2, v0, LX/0tO;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0ce7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-static {v1, v2}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    goto :goto_0
.end method
