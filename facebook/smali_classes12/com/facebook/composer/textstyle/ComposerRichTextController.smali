.class public Lcom/facebook/composer/textstyle/ComposerRichTextController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j0;",
        ":",
        "LX/0j2;",
        ":",
        "LX/0j9;",
        "DerivedData::",
        "LX/5RE;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/ipc/composer/model/ComposerRichTextStyleSpec$SetsRichTextStyle",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/0Tn;

.field private static final b:LX/0jK;

.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final e:LX/Be2;

.field private final f:LX/Hvj;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Landroid/content/Context;

.field private final h:LX/0ad;

.field public final i:LX/IF5;

.field private final j:LX/1Ad;

.field private final k:LX/1Uo;

.field private final l:Landroid/view/ViewGroup$LayoutParams;

.field public final m:I

.field private final n:Landroid/graphics/Rect;

.field public final o:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final p:LX/0tO;

.field private q:I

.field private r:I

.field private s:Landroid/graphics/drawable/Drawable;

.field private final t:LX/HvV;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2519196
    sget-object v0, LX/0Tm;->d:LX/0Tn;

    const-string v1, "RICH_TEXT_STYLE_PREVIOUS_POSITION"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->a:LX/0Tn;

    .line 2519197
    const-class v0, Lcom/facebook/composer/textstyle/ComposerRichTextController;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->b:LX/0jK;

    .line 2519198
    const-class v0, Lcom/facebook/composer/textstyle/ComposerRichTextController;

    const-class v1, Lcom/facebook/composer/textstyle/ComposerRichTextController;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/Be2;LX/Hvj;Landroid/graphics/Rect;Landroid/view/ViewGroup$LayoutParams;Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;LX/IF5;LX/1Ad;LX/1Uo;LX/0tO;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Be2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Hvj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Rect;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/ViewGroup$LayoutParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/Be2;",
            "LX/Hvj;",
            "Landroid/graphics/Rect;",
            "Landroid/view/ViewGroup$LayoutParams;",
            "Landroid/content/Context;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0ad;",
            "LX/IF5;",
            "LX/1Ad;",
            "LX/1Uo;",
            "LX/0tO;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2519275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2519276
    new-instance v0, LX/HvV;

    invoke-direct {v0, p0}, LX/HvV;-><init>(Lcom/facebook/composer/textstyle/ComposerRichTextController;)V

    iput-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->t:LX/HvV;

    .line 2519277
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->d:Ljava/lang/ref/WeakReference;

    .line 2519278
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Be2;

    iput-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    .line 2519279
    iput-object p3, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->f:LX/Hvj;

    .line 2519280
    iput-object p4, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->n:Landroid/graphics/Rect;

    .line 2519281
    iput-object p6, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->g:Landroid/content/Context;

    .line 2519282
    iput-object p8, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->h:LX/0ad;

    .line 2519283
    iput-object p9, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->i:LX/IF5;

    .line 2519284
    iput-object p10, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->j:LX/1Ad;

    .line 2519285
    iput-object p11, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->k:LX/1Uo;

    .line 2519286
    iput-object p5, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->l:Landroid/view/ViewGroup$LayoutParams;

    .line 2519287
    iput-object p12, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->p:LX/0tO;

    .line 2519288
    iget-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->f:LX/Hvj;

    if-eqz v0, :cond_0

    .line 2519289
    iget-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->f:LX/Hvj;

    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->t:LX/HvV;

    .line 2519290
    iput-object v1, v0, LX/Hvj;->o:LX/HvV;

    .line 2519291
    iget-object p1, v0, LX/Hvj;->p:LX/Hva;

    if-eqz p1, :cond_0

    .line 2519292
    iget-object p1, v0, LX/Hvj;->p:LX/Hva;

    .line 2519293
    iput-object v1, p1, LX/Hva;->f:LX/HvV;

    .line 2519294
    iget-boolean p1, v0, LX/Hvj;->D:Z

    if-eqz p1, :cond_0

    .line 2519295
    invoke-static {v0}, LX/Hvj;->h(LX/Hvj;)V

    .line 2519296
    invoke-static {v0}, LX/Hvj;->t(LX/Hvj;)V

    .line 2519297
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->p:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->i()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->m:I

    .line 2519298
    sget-object v0, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->q:I

    .line 2519299
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    sget-object v1, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->s:Landroid/graphics/drawable/Drawable;

    .line 2519300
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->r:I

    .line 2519301
    iput-object p7, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->o:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2519302
    return-void
.end method

.method private static a(Lcom/facebook/composer/textstyle/ComposerRichTextController;IILandroid/graphics/drawable/TransitionDrawable;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2519267
    iget-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    const-string v1, "textColor"

    new-instance v2, Landroid/animation/ArgbEvaluator;

    invoke-direct {v2}, Landroid/animation/ArgbEvaluator;-><init>()V

    new-array v3, v8, [Ljava/lang/Object;

    iget v4, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->q:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v0, v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2519268
    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    new-array v2, v7, [Landroid/animation/PropertyValuesHolder;

    const-string v3, "minHeight"

    new-array v4, v8, [I

    iget v5, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->r:I

    aput v5, v4, v6

    aput p2, v4, v7

    invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 2519269
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2519270
    new-array v3, v8, [Landroid/animation/Animator;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 2519271
    iget v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->m:I

    int-to-long v0, v0

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 2519272
    new-instance v0, LX/HvW;

    invoke-direct {v0, p0, p3}, LX/HvW;-><init>(Lcom/facebook/composer/textstyle/ComposerRichTextController;Landroid/graphics/drawable/TransitionDrawable;)V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2519273
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 2519274
    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/textstyle/ComposerRichTextController;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)V
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 2519199
    const/4 v0, 0x2

    new-array v3, v0, [Landroid/graphics/drawable/Drawable;

    .line 2519200
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2519201
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v4}, LX/87X;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    .line 2519202
    :goto_0
    aput-object v0, v3, v5

    .line 2519203
    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->s:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_6

    aget-object v1, v3, v5

    :goto_1
    aput-object v1, v3, v2

    .line 2519204
    new-instance v4, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v4, v3}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 2519205
    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    invoke-interface {v1, v4}, LX/Be1;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2519206
    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    iget-object v3, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->n:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->n:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->n:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->n:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-interface {v1, v3, v5, v6, v7}, LX/Be1;->setPadding(IIII)V

    .line 2519207
    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v3

    .line 2519208
    sget-object v5, LX/87W;->a:[I

    invoke-virtual {v3}, LX/5RS;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2519209
    const v5, 0x800003

    :goto_2
    move v3, v5

    .line 2519210
    invoke-interface {v1, v3}, LX/Be1;->setGravity(I)V

    .line 2519211
    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->p:LX/0tO;

    invoke-virtual {v1}, LX/0tO;->n()I

    move-result v1

    int-to-float v1, v1

    .line 2519212
    iget-object v3, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    iget-object v5, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->p:LX/0tO;

    invoke-virtual {v5}, LX/0tO;->q()F

    move-result v5

    mul-float/2addr v5, v1

    invoke-static {v1, v5}, LX/87X;->a(FF)F

    move-result v1

    invoke-interface {v3, v8, v1}, LX/Be1;->setLineSpacing(FF)V

    .line 2519213
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    .line 2519214
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v9, :cond_0

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2519215
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->p:LX/0tO;

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 2519216
    iget-object v6, v1, LX/0tO;->a:LX/0ad;

    sget-short v7, LX/0wk;->y:S

    invoke-interface {v6, v7, v5}, LX/0ad;->a(SZ)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2519217
    iget-object v6, v1, LX/0tO;->a:LX/0ad;

    sget v7, LX/0wk;->w:F

    invoke-interface {v6, v7, v10}, LX/0ad;->a(FF)F

    move-result v6

    .line 2519218
    cmpl-float v7, v6, v10

    if-lez v7, :cond_1

    .line 2519219
    iget-object v5, v1, LX/0tO;->c:Landroid/content/Context;

    iget-object v7, v1, LX/0tO;->b:LX/0hB;

    invoke-virtual {v7}, LX/0hB;->f()I

    move-result v7

    int-to-float v7, v7

    invoke-static {v5, v7}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    float-to-int v5, v5

    .line 2519220
    :cond_1
    :goto_3
    move v1, v5

    .line 2519221
    int-to-float v1, v1

    .line 2519222
    cmpl-float v5, v1, v8

    if-lez v5, :cond_9

    .line 2519223
    iget-object v5, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->g:Landroid/content/Context;

    invoke-static {v5, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 2519224
    :goto_4
    iget-object v5, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    const/16 v6, 0xb2

    invoke-static {v6, v3}, LX/87X;->a(II)I

    move-result v6

    invoke-interface {v5, v6}, LX/Be1;->setHintTextColor(I)V

    .line 2519225
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x2

    invoke-direct {v5, v9, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2519226
    iget-object v6, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->g:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b03ef

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2519227
    iget-object v6, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    invoke-interface {v6, v5}, LX/Be1;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2519228
    iget-object v5, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    iget-object v6, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->p:LX/0tO;

    invoke-virtual {v6}, LX/0tO;->o()I

    move-result v6

    iget-object v7, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->p:LX/0tO;

    invoke-virtual {v7}, LX/0tO;->p()I

    move-result v7

    iget-object v8, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->p:LX/0tO;

    invoke-virtual {v8}, LX/0tO;->o()I

    move-result v8

    iget-object v9, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->p:LX/0tO;

    invoke-virtual {v9}, LX/0tO;->p()I

    move-result v9

    invoke-interface {v5, v6, v7, v8, v9}, LX/Be1;->setPadding(IIII)V

    .line 2519229
    :goto_5
    sget-object v5, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {p1, v5}, LX/87X;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 2519230
    iget-object v5, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    const/16 v6, 0xbf

    invoke-static {v6, v3}, LX/87X;->a(II)I

    move-result v6

    invoke-interface {v5, v6, v2}, LX/Be2;->a(II)V

    .line 2519231
    :goto_6
    invoke-static {p0, v3, v1, v4}, Lcom/facebook/composer/textstyle/ComposerRichTextController;->a(Lcom/facebook/composer/textstyle/ComposerRichTextController;IILandroid/graphics/drawable/TransitionDrawable;)V

    .line 2519232
    iput v3, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->q:I

    .line 2519233
    iput-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->s:Landroid/graphics/drawable/Drawable;

    .line 2519234
    iput v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->r:I

    .line 2519235
    iget-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2519236
    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, Lcom/facebook/composer/textstyle/ComposerRichTextController;->b:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 2519237
    iget-object v1, v0, LX/0jL;->d:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 2519238
    iget-object v1, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    move-result-object v1

    invoke-static {v1, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2519239
    iget-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    if-nez v1, :cond_2

    .line 2519240
    iget-object v1, v0, LX/0jL;->b:Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    move-result-object v1

    iput-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2519241
    :cond_2
    iget-object v1, v0, LX/0jL;->c:Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    invoke-virtual {v1, p1}, Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;->setRichTextStyle(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Lcom/facebook/composer/system/model/ComposerModelImpl$Builder;

    .line 2519242
    iget-object v1, v0, LX/0jL;->a:LX/0cA;

    sget-object v2, LX/5L2;->ON_DATASET_CHANGE:LX/5L2;

    invoke-virtual {v1, v2}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2519243
    :cond_3
    move-object v0, v0

    .line 2519244
    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2519245
    return-void

    .line 2519246
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2519247
    iget-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->j:LX/1Ad;

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/composer/textstyle/ComposerRichTextController;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 2519248
    iget-object v0, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->k:LX/1Uo;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-direct {v1, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2519249
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2519250
    move-object v0, v0

    .line 2519251
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->g:Landroid/content/Context;

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    .line 2519252
    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->j:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 2519253
    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/16 :goto_0

    .line 2519254
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    .line 2519255
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto/16 :goto_0

    .line 2519256
    :cond_6
    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->s:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_1

    .line 2519257
    :cond_7
    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    .line 2519258
    new-instance v5, Landroid/widget/EditText;

    iget-object v6, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->g:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 2519259
    invoke-virtual {v5}, Landroid/widget/EditText;->getCurrentHintTextColor()I

    move-result v5

    move v5, v5

    .line 2519260
    invoke-interface {v1, v5}, LX/Be1;->setHintTextColor(I)V

    .line 2519261
    iget-object v1, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    iget-object v5, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->l:Landroid/view/ViewGroup$LayoutParams;

    invoke-interface {v1, v5}, LX/Be1;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v1, v2

    goto/16 :goto_5

    .line 2519262
    :cond_8
    iget-object v2, p0, Lcom/facebook/composer/textstyle/ComposerRichTextController;->e:LX/Be2;

    const v5, -0x272016

    invoke-interface {v2, v3, v5}, LX/Be2;->a(II)V

    goto/16 :goto_6

    :cond_9
    move v1, v2

    goto/16 :goto_4

    .line 2519263
    :pswitch_0
    const/16 v5, 0x11

    goto/16 :goto_2

    .line 2519264
    :pswitch_1
    const v5, 0x800005

    goto/16 :goto_2

    .line 2519265
    :cond_a
    iget-object v6, v1, LX/0tO;->a:LX/0ad;

    sget-short v7, LX/0wk;->q:S

    invoke-interface {v6, v7, v5}, LX/0ad;->a(SZ)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2519266
    iget-object v6, v1, LX/0tO;->a:LX/0ad;

    sget v7, LX/0wk;->n:I

    invoke-interface {v6, v7, v5}, LX/0ad;->a(II)I

    move-result v5

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
