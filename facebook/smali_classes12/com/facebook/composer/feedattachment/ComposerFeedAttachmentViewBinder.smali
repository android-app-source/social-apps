.class public Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/1Ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2515113
    const-class v0, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2515110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2515111
    iput-object p1, p0, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->b:LX/1Ad;

    .line 2515112
    return-void
.end method

.method public static a(Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;Landroid/net/Uri;)LX/1aZ;
    .locals 2

    .prologue
    .line 2515109
    iget-object v0, p0, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->b:LX/1Ad;

    sget-object v1, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/ADz;Lcom/facebook/ipc/composer/model/ComposerReshareContext;LX/HtA;)V
    .locals 5

    .prologue
    .line 2515083
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2515084
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2515085
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->getOriginalShareActorName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2515086
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 2515087
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 2515088
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->getReshareMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2515089
    new-instance v2, Landroid/text/style/StyleSpan;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v3, 0x0

    const/16 v4, 0x11

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2515090
    iget-object v1, p0, LX/ADz;->e:Landroid/widget/TextView;

    invoke-static {v1, v0}, LX/35n;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 2515091
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->shouldIncludeReshareContext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2515092
    iget-object v0, p0, LX/ADz;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2515093
    :goto_0
    const/4 v4, 0x0

    .line 2515094
    invoke-virtual {p0}, LX/35n;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    .line 2515095
    invoke-virtual {v0, v4}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 2515096
    invoke-virtual {p0}, LX/35n;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    .line 2515097
    iget-object v1, v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v0, v1

    .line 2515098
    invoke-virtual {p0}, LX/ADz;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2515099
    const v2, 0x7f0b0061

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2515100
    const v3, 0x7f0b0063

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2515101
    invoke-virtual {v0, v2, v1, v2, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setPadding(IIII)V

    .line 2515102
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 2515103
    const v1, 0x7f0207e2

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageResource(I)V

    .line 2515104
    invoke-virtual {p0}, LX/ADz;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setGlyphColor(I)V

    .line 2515105
    new-instance v1, LX/Ht4;

    invoke-direct {v1, p0, p2}, LX/Ht4;-><init>(LX/ADz;LX/HtA;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2515106
    return-void

    .line 2515107
    :cond_0
    iget-object v0, p0, LX/ADz;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2515108
    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 2515082
    invoke-static {p0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;
    .locals 2

    .prologue
    .line 2515057
    new-instance v1, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-direct {v1, v0}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;-><init>(LX/1Ad;)V

    .line 2515058
    return-object v1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/HlK;)V
    .locals 1

    .prologue
    .line 2515078
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2515079
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2515080
    iget-object p0, p1, LX/HlK;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2515081
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/attachments/angora/AngoraAttachmentView;)V
    .locals 5

    .prologue
    .line 2515059
    const/4 v0, 0x0

    .line 2515060
    invoke-virtual {p2}, LX/35n;->a()V

    .line 2515061
    invoke-virtual {p2, v0}, LX/35n;->setTitle(Ljava/lang/CharSequence;)V

    .line 2515062
    invoke-virtual {p2, v0}, LX/35n;->setContextText(Ljava/lang/CharSequence;)V

    .line 2515063
    invoke-virtual {p2, v0}, LX/35n;->setSideImageController(LX/1aZ;)V

    .line 2515064
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2515065
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2515066
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\n"

    const-string v3, " "

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2515067
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x11

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2515068
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2515069
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 2515070
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2515071
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2515072
    :cond_2
    invoke-interface {p2, v0}, LX/2yX;->setTitle(Ljava/lang/CharSequence;)V

    .line 2515073
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2515074
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, LX/2yX;->setContextText(Ljava/lang/CharSequence;)V

    .line 2515075
    :cond_3
    invoke-static {p1}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2515076
    invoke-static {p1}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a(Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;Landroid/net/Uri;)LX/1aZ;

    move-result-object v0

    invoke-interface {p2, v0}, LX/2yY;->setSideImageController(LX/1aZ;)V

    .line 2515077
    :cond_4
    return-void
.end method
