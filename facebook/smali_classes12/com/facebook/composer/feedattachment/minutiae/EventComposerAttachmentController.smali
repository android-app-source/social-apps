.class public Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/HtM;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0tX;

.field private final c:LX/0sa;

.field private final d:Ljava/util/concurrent/Executor;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2516029
    const-class v0, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/0sa;Ljava/util/concurrent/Executor;LX/0Or;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/0sa;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2516031
    iput-object p1, p0, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->b:LX/0tX;

    .line 2516032
    iput-object p2, p0, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->c:LX/0sa;

    .line 2516033
    iput-object p4, p0, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->e:LX/0Or;

    .line 2516034
    iput-object p3, p0, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->d:Ljava/util/concurrent/Executor;

    .line 2516035
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13

    .prologue
    .line 2516036
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2516037
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2516038
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->iZ()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/5O7;->c(J)Ljava/util/Date;

    move-result-object v1

    .line 2516039
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->cI()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    .line 2516040
    invoke-static {v2}, LX/Bnh;->a(Lcom/facebook/graphql/model/GraphQLPlace;)Ljava/lang/String;

    move-result-object v3

    .line 2516041
    invoke-static {v2}, LX/Bnh;->b(Lcom/facebook/graphql/model/GraphQLPlace;)Ljava/lang/String;

    move-result-object v2

    .line 2516042
    invoke-static {v0}, LX/Bnh;->a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;

    move-result-object v4

    .line 2516043
    new-instance v5, Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/facebook/events/widget/eventcard/EventsCardView;-><init>(Landroid/content/Context;)V

    .line 2516044
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->cC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v7

    .line 2516045
    if-eqz v7, :cond_1

    .line 2516046
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v8

    .line 2516047
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    if-nez v9, :cond_2

    .line 2516048
    :cond_0
    iget-object v9, p0, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->e:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/1Ad;

    sget-object v10, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v9, v10}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v9

    invoke-virtual {v9}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v9

    .line 2516049
    :goto_0
    move-object v8, v9

    .line 2516050
    invoke-virtual {v5, v8}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoController(LX/1aZ;)V

    .line 2516051
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 2516052
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v7

    .line 2516053
    new-instance v8, Landroid/graphics/PointF;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v9

    double-to-float v9, v9

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v11

    double-to-float v7, v11

    invoke-direct {v8, v9, v7}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v5, v8}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoFocusPoint(Landroid/graphics/PointF;)V

    .line 2516054
    :cond_1
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setShouldHideNullCoverPhotoView(Z)V

    .line 2516055
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2516056
    invoke-virtual {v5, v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCalendarFormatStartDate(Ljava/util/Date;)V

    .line 2516057
    invoke-virtual {v5, v3, v2}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2516058
    invoke-virtual {v5, v4}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setSocialContextText(Ljava/lang/CharSequence;)V

    .line 2516059
    invoke-virtual {v5}, Lcom/facebook/events/widget/eventcard/EventsCardView;->d()V

    .line 2516060
    const v0, 0x7f020aa7

    invoke-virtual {v5, v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setBackgroundResource(I)V

    .line 2516061
    invoke-virtual {v5}, Lcom/facebook/events/widget/eventcard/EventsCardView;->g()V

    .line 2516062
    return-object v5

    .line 2516063
    :cond_2
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    .line 2516064
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    .line 2516065
    invoke-interface {v9}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 2516066
    iget-object v9, p0, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->e:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/1Ad;

    sget-object v12, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v9, v12}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v9

    invoke-interface {v10}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v9

    invoke-static {v11}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    move-result-object v9

    check-cast v9, LX/1Ad;

    invoke-virtual {v9}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v9

    goto :goto_0
.end method

.method public final a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2516067
    new-instance v0, LX/HtE;

    invoke-direct {v0}, LX/HtE;-><init>()V

    move-object v0, v0

    .line 2516068
    const-string v1, "angora_attachment_cover_image_size"

    iget-object v2, p0, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->c:LX/0sa;

    invoke-virtual {v2}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "event_id"

    iget-object v3, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2516069
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2516070
    iget-object v1, p0, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/HtL;

    invoke-direct {v1, p0}, LX/HtL;-><init>(Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;)V

    iget-object v2, p0, Lcom/facebook/composer/feedattachment/minutiae/EventComposerAttachmentController;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
