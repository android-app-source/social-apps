.class public final Lcom/facebook/composer/feedattachment/ShareComposerAttachment$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HtB;


# direct methods
.method public constructor <init>(LX/HtB;)V
    .locals 0

    .prologue
    .line 2515424
    iput-object p1, p0, Lcom/facebook/composer/feedattachment/ShareComposerAttachment$3;->a:LX/HtB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2515425
    iget-object v0, p0, Lcom/facebook/composer/feedattachment/ShareComposerAttachment$3;->a:LX/HtB;

    iget-object v0, v0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2515426
    iget-object v1, p0, Lcom/facebook/composer/feedattachment/ShareComposerAttachment$3;->a:LX/HtB;

    iget-object v2, v1, LX/HtB;->b:LX/Hsi;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0j5;

    invoke-interface {v0}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    .line 2515427
    iget-object v3, v2, LX/Hsi;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    .line 2515428
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v4, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    move v3, v3

    .line 2515429
    if-eqz v3, :cond_0

    .line 2515430
    sget-object v3, LX/Hsi;->b:Ljava/lang/IllegalStateException;

    invoke-static {v3}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2515431
    :goto_0
    move-object v0, v3

    .line 2515432
    return-object v0

    .line 2515433
    :cond_0
    iget-object v3, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v3, :cond_1

    .line 2515434
    iget-object v3, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v3}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_0

    .line 2515435
    :cond_1
    new-instance v3, LX/BON;

    invoke-direct {v3}, LX/BON;-><init>()V

    .line 2515436
    iget-object v4, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    if-eqz v4, :cond_2

    .line 2515437
    iget-object v4, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->shareable:Lcom/facebook/graphql/model/GraphQLEntity;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v4

    .line 2515438
    iput-object v4, v3, LX/BON;->a:Ljava/lang/String;

    .line 2515439
    :goto_1
    iput-object v1, v3, LX/BON;->c:Ljava/lang/String;

    .line 2515440
    move-object v3, v3

    .line 2515441
    invoke-virtual {v3}, LX/BON;->a()Lcom/facebook/share/protocol/LinksPreviewParams;

    move-result-object v3

    .line 2515442
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2515443
    const-string v4, "linksPreviewParams"

    invoke-virtual {v5, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2515444
    const-string v3, "overridden_viewer_context"

    iget-object v4, v2, LX/Hsi;->d:LX/0SI;

    invoke-interface {v4}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    invoke-virtual {v5, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2515445
    iget-object v3, v2, LX/Hsi;->c:LX/0aG;

    const-string v4, "csh_links_preview"

    sget-object v6, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v7

    const v8, 0x21c1004f

    invoke-static/range {v3 .. v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    new-instance v4, LX/Hsh;

    invoke-direct {v4, v2}, LX/Hsh;-><init>(LX/Hsi;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_0

    .line 2515446
    :cond_2
    iget-object v4, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2515447
    iget-object v4, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    .line 2515448
    iput-object v4, v3, LX/BON;->b:Ljava/lang/String;

    .line 2515449
    goto :goto_1

    .line 2515450
    :cond_3
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Both the shareable and the link for share are null!"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_0
.end method
