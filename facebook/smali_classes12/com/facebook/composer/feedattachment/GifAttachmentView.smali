.class public Lcom/facebook/composer/feedattachment/GifAttachmentView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Landroid/view/View;

.field public final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final e:LX/1Ai;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ai",
            "<",
            "Lcom/facebook/imagepipeline/image/ImageInfo;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2515199
    const-class v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2515184
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2515185
    new-instance v0, LX/Hsv;

    invoke-direct {v0, p0}, LX/Hsv;-><init>(Lcom/facebook/composer/feedattachment/GifAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->e:LX/1Ai;

    .line 2515186
    const-class v0, Lcom/facebook/composer/feedattachment/GifAttachmentView;

    invoke-static {v0, p0}, Lcom/facebook/composer/feedattachment/GifAttachmentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2515187
    const v0, 0x7f03031d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2515188
    const v0, 0x7f0d0a7e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->c:Landroid/view/View;

    .line 2515189
    const v0, 0x7f0d0a7d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2515190
    new-instance v0, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02187f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v2}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 2515191
    new-instance v1, LX/1Uo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2515192
    iput-object v0, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2515193
    move-object v0, v1

    .line 2515194
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2515195
    iget-object v1, p0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2515196
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/composer/feedattachment/GifAttachmentView;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p0

    check-cast p0, LX/1Ad;

    iput-object p0, p1, Lcom/facebook/composer/feedattachment/GifAttachmentView;->a:LX/1Ad;

    return-void
.end method


# virtual methods
.method public setRemoveButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2515197
    iget-object v0, p0, Lcom/facebook/composer/feedattachment/GifAttachmentView;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2515198
    return-void
.end method
