.class public Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/HsR;


# instance fields
.field private a:Lcom/facebook/uicontrib/datepicker/DatePicker;

.field public b:Lcom/facebook/uicontrib/datepicker/DatePicker;

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2514524
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2514525
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2514526
    iget-object v0, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->a:Lcom/facebook/uicontrib/datepicker/DatePicker;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedDate()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v0

    .line 2514527
    invoke-static {v0}, LX/HsS;->a(Lcom/facebook/uicontrib/datepicker/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2514528
    const/4 v0, 0x0

    .line 2514529
    :cond_0
    const-string v2, "startDate"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2514530
    const-string v0, "endDate"

    iget-object v2, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->b:Lcom/facebook/uicontrib/datepicker/DatePicker;

    invoke-virtual {v2}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedDate()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2514531
    const-string v0, "hasGraduated"

    iget-boolean v2, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->c:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2514532
    return-object v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3b0d9dc7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2514533
    const v1, 0x7f030467

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x2f538327

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2514534
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2514535
    const-string v0, "hasGraduated"

    iget-boolean v1, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2514536
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2514537
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2514538
    const v0, 0x7f0d0d42

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/DatePicker;

    iput-object v0, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->a:Lcom/facebook/uicontrib/datepicker/DatePicker;

    .line 2514539
    const v0, 0x7f0d0d44

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/DatePicker;

    iput-object v0, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->b:Lcom/facebook/uicontrib/datepicker/DatePicker;

    .line 2514540
    iget-object v0, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->a:Lcom/facebook/uicontrib/datepicker/DatePicker;

    new-instance v1, LX/HsT;

    invoke-direct {v1, p0}, LX/HsT;-><init>(Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;)V

    .line 2514541
    iput-object v1, v0, Lcom/facebook/uicontrib/datepicker/DatePicker;->b:LX/5zz;

    .line 2514542
    const v0, 0x7f0d0d45

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2514543
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f083980

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 2514544
    new-instance v1, LX/HsU;

    invoke-direct {v1, p0}, LX/HsU;-><init>(Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2514545
    if-nez p2, :cond_3

    .line 2514546
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2514547
    const-string v2, "minimumDate"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2514548
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2514549
    const-string v3, "startDate"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2514550
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2514551
    const-string v4, "endDate"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2514552
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2514553
    const-string v5, "hasGraduated"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->c:Z

    .line 2514554
    if-eqz v1, :cond_0

    .line 2514555
    iget-object v4, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->a:Lcom/facebook/uicontrib/datepicker/DatePicker;

    invoke-virtual {v4, v1}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setMinimumDate(Lcom/facebook/uicontrib/datepicker/Date;)V

    .line 2514556
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->a:Lcom/facebook/uicontrib/datepicker/DatePicker;

    if-nez v2, :cond_1

    sget-object v2, Lcom/facebook/uicontrib/datepicker/Date;->a:Lcom/facebook/uicontrib/datepicker/Date;

    :cond_1
    invoke-virtual {v1, v2}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setSelectedDate(Lcom/facebook/uicontrib/datepicker/Date;)V

    .line 2514557
    iget-object v1, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->b:Lcom/facebook/uicontrib/datepicker/DatePicker;

    invoke-virtual {v1, v3}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setSelectedDate(Lcom/facebook/uicontrib/datepicker/Date;)V

    .line 2514558
    iget-boolean v1, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->c:Z

    if-eqz v1, :cond_2

    .line 2514559
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2514560
    :cond_2
    :goto_0
    return-void

    .line 2514561
    :cond_3
    const-string v0, "hasGraduated"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;->c:Z

    goto :goto_0
.end method
