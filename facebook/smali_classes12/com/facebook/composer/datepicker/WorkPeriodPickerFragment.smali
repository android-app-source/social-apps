.class public Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/HsR;


# instance fields
.field private a:Lcom/facebook/uicontrib/datepicker/DatePicker;

.field public b:Lcom/facebook/uicontrib/datepicker/DatePicker;

.field public c:Landroid/widget/LinearLayout;

.field public d:Landroid/widget/TextView;

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2514576
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2514611
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2514612
    iget-object v0, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->a:Lcom/facebook/uicontrib/datepicker/DatePicker;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedDate()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v0

    .line 2514613
    invoke-static {v0}, LX/HsS;->a(Lcom/facebook/uicontrib/datepicker/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2514614
    const/4 v0, 0x0

    .line 2514615
    :cond_0
    const-string v2, "startDate"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2514616
    iget-boolean v0, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->e:Z

    if-eqz v0, :cond_1

    .line 2514617
    const-string v0, "isCurrent"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2514618
    :goto_0
    return-object v1

    .line 2514619
    :cond_1
    const-string v0, "endDate"

    iget-object v2, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->b:Lcom/facebook/uicontrib/datepicker/DatePicker;

    invoke-virtual {v2}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedDate()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5eefdb7c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2514610
    const v1, 0x7f03162a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x5f82ea5a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2514607
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2514608
    const-string v0, "isCurrent"

    iget-boolean v1, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2514609
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2514577
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2514578
    const v0, 0x7f0d0d42

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/DatePicker;

    iput-object v0, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->a:Lcom/facebook/uicontrib/datepicker/DatePicker;

    .line 2514579
    const v0, 0x7f0d0d44

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/DatePicker;

    iput-object v0, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->b:Lcom/facebook/uicontrib/datepicker/DatePicker;

    .line 2514580
    const v0, 0x7f0d31cc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->c:Landroid/widget/LinearLayout;

    .line 2514581
    iget-object v0, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->a:Lcom/facebook/uicontrib/datepicker/DatePicker;

    new-instance v1, LX/HsV;

    invoke-direct {v1, p0}, LX/HsV;-><init>(Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;)V

    .line 2514582
    iput-object v1, v0, Lcom/facebook/uicontrib/datepicker/DatePicker;->b:LX/5zz;

    .line 2514583
    const v0, 0x7f0d0d43

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->d:Landroid/widget/TextView;

    .line 2514584
    const v0, 0x7f0d31cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 2514585
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2514586
    const-string v2, "currentActionText"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 2514587
    new-instance v1, LX/HsW;

    invoke-direct {v1, p0}, LX/HsW;-><init>(Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2514588
    if-nez p2, :cond_5

    .line 2514589
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2514590
    const-string v2, "minimumDate"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2514591
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2514592
    const-string v3, "startDate"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2514593
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2514594
    const-string v4, "endDate"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2514595
    if-eqz v2, :cond_0

    .line 2514596
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2514597
    const-string v6, "isCurrent"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_0
    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->e:Z

    .line 2514598
    if-eqz v1, :cond_1

    .line 2514599
    iget-object v4, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->a:Lcom/facebook/uicontrib/datepicker/DatePicker;

    invoke-virtual {v4, v1}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setMinimumDate(Lcom/facebook/uicontrib/datepicker/Date;)V

    .line 2514600
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->a:Lcom/facebook/uicontrib/datepicker/DatePicker;

    if-nez v2, :cond_2

    sget-object v2, Lcom/facebook/uicontrib/datepicker/Date;->a:Lcom/facebook/uicontrib/datepicker/Date;

    :cond_2
    invoke-virtual {v1, v2}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setSelectedDate(Lcom/facebook/uicontrib/datepicker/Date;)V

    .line 2514601
    iget-object v1, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->b:Lcom/facebook/uicontrib/datepicker/DatePicker;

    invoke-virtual {v1, v3}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setSelectedDate(Lcom/facebook/uicontrib/datepicker/Date;)V

    .line 2514602
    iget-boolean v1, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->e:Z

    if-eqz v1, :cond_3

    .line 2514603
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2514604
    :cond_3
    :goto_1
    return-void

    .line 2514605
    :cond_4
    const/4 v4, 0x0

    goto :goto_0

    .line 2514606
    :cond_5
    const-string v0, "isCurrent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;->e:Z

    goto :goto_1
.end method
