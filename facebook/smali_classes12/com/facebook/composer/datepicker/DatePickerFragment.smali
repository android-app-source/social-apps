.class public Lcom/facebook/composer/datepicker/DatePickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/HsR;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2514483
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2514484
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2514485
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2514486
    const v2, 0x7f0d0a73

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/DatePicker;

    .line 2514487
    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/DatePicker;->getSelectedDate()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v0

    move-object v0, v0

    .line 2514488
    invoke-static {v0}, LX/HsS;->a(Lcom/facebook/uicontrib/datepicker/Date;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2514489
    const/4 v0, 0x0

    .line 2514490
    :cond_0
    const-string v2, "startDate"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2514491
    return-object v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x5550668f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2514492
    const v1, 0x7f030314

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7a502535

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2514493
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2514494
    const v0, 0x7f0d0a73

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/DatePicker;

    .line 2514495
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2514496
    const-string v2, "minimumDate"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2514497
    if-eqz v1, :cond_0

    .line 2514498
    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setMinimumDate(Lcom/facebook/uicontrib/datepicker/Date;)V

    .line 2514499
    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2514500
    const-string v2, "startDate"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2514501
    if-nez v1, :cond_1

    sget-object v1, Lcom/facebook/uicontrib/datepicker/Date;->a:Lcom/facebook/uicontrib/datepicker/Date;

    :cond_1
    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/datepicker/DatePicker;->setSelectedDate(Lcom/facebook/uicontrib/datepicker/Date;)V

    .line 2514502
    return-void
.end method
