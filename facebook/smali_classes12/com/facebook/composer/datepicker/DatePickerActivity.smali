.class public Lcom/facebook/composer/datepicker/DatePickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2514479
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2514480
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;)LX/HsQ;
    .locals 2

    .prologue
    .line 2514474
    sget-object v0, LX/HsP;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2514475
    sget-object v0, LX/HsQ;->DATE:LX/HsQ;

    :goto_0
    return-object v0

    .line 2514476
    :pswitch_0
    sget-object v0, LX/HsQ;->DATE:LX/HsQ;

    goto :goto_0

    .line 2514477
    :pswitch_1
    sget-object v0, LX/HsQ;->WORK_PERIOD:LX/HsQ;

    goto :goto_0

    .line 2514478
    :pswitch_2
    sget-object v0, LX/HsQ;->EDUCATION_PERIOD:LX/HsQ;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;Lcom/facebook/uicontrib/datepicker/Date;Lcom/facebook/uicontrib/datepicker/Date;Lcom/facebook/uicontrib/datepicker/Date;ZZ)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2514463
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/composer/datepicker/DatePickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2514464
    const-string v1, "mleType"

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2514465
    if-eqz p2, :cond_0

    .line 2514466
    const-string v1, "minimumDate"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2514467
    :cond_0
    if-eqz p3, :cond_1

    .line 2514468
    const-string v1, "startDate"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2514469
    :cond_1
    if-eqz p4, :cond_2

    .line 2514470
    const-string v1, "endDate"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2514471
    :cond_2
    const-string v1, "isCurrent"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2514472
    const-string v1, "hasGraduated"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2514473
    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2514457
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2514458
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2514459
    invoke-virtual {p0}, Lcom/facebook/composer/datepicker/DatePickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083979

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2514460
    invoke-direct {p0, v0}, Lcom/facebook/composer/datepicker/DatePickerActivity;->a(LX/0h5;)V

    .line 2514461
    new-instance v1, LX/HsO;

    invoke-direct {v1, p0}, LX/HsO;-><init>(Lcom/facebook/composer/datepicker/DatePickerActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2514462
    return-void
.end method

.method private a(LX/0h5;)V
    .locals 3

    .prologue
    .line 2514445
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const/4 v1, 0x1

    .line 2514446
    iput v1, v0, LX/108;->a:I

    .line 2514447
    move-object v0, v0

    .line 2514448
    invoke-virtual {p0}, Lcom/facebook/composer/datepicker/DatePickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08001e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2514449
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2514450
    move-object v0, v0

    .line 2514451
    const/4 v1, -0x2

    .line 2514452
    iput v1, v0, LX/108;->h:I

    .line 2514453
    move-object v0, v0

    .line 2514454
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2514455
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {p1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2514456
    return-void
.end method

.method public static b(Lcom/facebook/composer/datepicker/DatePickerActivity;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2514481
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d0a72

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2514482
    check-cast v0, LX/HsR;

    invoke-interface {v0}, LX/HsR;->b()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/os/Bundle;)Lcom/facebook/base/fragment/FbFragment;
    .locals 4

    .prologue
    .line 2514437
    new-instance v1, Lcom/facebook/composer/datepicker/DatePickerFragment;

    invoke-direct {v1}, Lcom/facebook/composer/datepicker/DatePickerFragment;-><init>()V

    .line 2514438
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2514439
    const-string v0, "startDate"

    const-string v3, "startDate"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2514440
    const-string v0, "minimumDate"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2514441
    if-eqz v0, :cond_0

    .line 2514442
    const-string v3, "minimumDate"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2514443
    :cond_0
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2514444
    return-object v1
.end method

.method private e(Landroid/os/Bundle;)Lcom/facebook/base/fragment/FbFragment;
    .locals 4

    .prologue
    .line 2514426
    new-instance v1, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;

    invoke-direct {v1}, Lcom/facebook/composer/datepicker/WorkPeriodPickerFragment;-><init>()V

    .line 2514427
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2514428
    const-string v0, "minimumDate"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2514429
    if-eqz v0, :cond_0

    .line 2514430
    const-string v3, "minimumDate"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2514431
    :cond_0
    const-string v0, "currentActionText"

    const-string v3, "mleType"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-direct {p0}, Lcom/facebook/composer/datepicker/DatePickerActivity;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2514432
    const-string v0, "isCurrent"

    const-string v3, "isCurrent"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2514433
    const-string v0, "startDate"

    const-string v3, "startDate"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2514434
    const-string v0, "endDate"

    const-string v3, "endDate"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2514435
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2514436
    return-object v1
.end method

.method private static f(Landroid/os/Bundle;)Lcom/facebook/base/fragment/FbFragment;
    .locals 4

    .prologue
    .line 2514416
    new-instance v1, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;

    invoke-direct {v1}, Lcom/facebook/composer/datepicker/EducationPeriodPickerFragment;-><init>()V

    .line 2514417
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2514418
    const-string v0, "minimumDate"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/datepicker/Date;

    .line 2514419
    if-eqz v0, :cond_0

    .line 2514420
    const-string v3, "minimumDate"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2514421
    :cond_0
    const-string v0, "hasGraduated"

    const-string v3, "hasGraduated"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2514422
    const-string v0, "startDate"

    const-string v3, "startDate"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2514423
    const-string v0, "endDate"

    const-string v3, "endDate"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2514424
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2514425
    return-object v1
.end method

.method private l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2514415
    invoke-virtual {p0}, Lcom/facebook/composer/datepicker/DatePickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08397d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2514400
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2514401
    const v0, 0x7f030313

    invoke-virtual {p0, v0}, Lcom/facebook/composer/datepicker/DatePickerActivity;->setContentView(I)V

    .line 2514402
    invoke-direct {p0}, Lcom/facebook/composer/datepicker/DatePickerActivity;->a()V

    .line 2514403
    if-nez p1, :cond_2

    .line 2514404
    invoke-virtual {p0}, Lcom/facebook/composer/datepicker/DatePickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2514405
    const-string v1, "mleType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/datepicker/DatePickerActivity;->p:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    .line 2514406
    iget-object v1, p0, Lcom/facebook/composer/datepicker/DatePickerActivity;->p:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-static {v1}, Lcom/facebook/composer/datepicker/DatePickerActivity;->a(Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;)LX/HsQ;

    move-result-object v1

    .line 2514407
    sget-object v2, LX/HsQ;->EDUCATION_PERIOD:LX/HsQ;

    invoke-virtual {v1, v2}, LX/HsQ;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2514408
    invoke-static {v0}, Lcom/facebook/composer/datepicker/DatePickerActivity;->f(Landroid/os/Bundle;)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    .line 2514409
    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d0a72

    invoke-virtual {v1, v2, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2514410
    :goto_1
    return-void

    .line 2514411
    :cond_0
    sget-object v2, LX/HsQ;->WORK_PERIOD:LX/HsQ;

    invoke-virtual {v1, v2}, LX/HsQ;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2514412
    invoke-direct {p0, v0}, Lcom/facebook/composer/datepicker/DatePickerActivity;->e(Landroid/os/Bundle;)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    goto :goto_0

    .line 2514413
    :cond_1
    invoke-static {v0}, Lcom/facebook/composer/datepicker/DatePickerActivity;->d(Landroid/os/Bundle;)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    goto :goto_0

    .line 2514414
    :cond_2
    const-string v0, "mleType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/datepicker/DatePickerActivity;->p:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2514397
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2514398
    const-string v0, "mleType"

    iget-object v1, p0, Lcom/facebook/composer/datepicker/DatePickerActivity;->p:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2514399
    return-void
.end method
