.class public Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/HsK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j3;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/HsK;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/attachments/angora/AngoraAttachmentView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:LX/1Ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2517652
    const-class v0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/0il;)V
    .locals 2
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ad;",
            "TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517654
    iput-object p1, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->d:LX/1Ad;

    .line 2517655
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->b:Ljava/lang/ref/WeakReference;

    .line 2517656
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)V
    .locals 4

    .prologue
    .line 2517657
    iget-object v0, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialComposerCallToAction()Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    move-result-object v0

    .line 2517658
    new-instance v1, Lcom/facebook/attachments/angora/AngoraAttachmentView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->c:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    .line 2517659
    iget-object v1, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->c:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2517660
    iget-object v1, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->c:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020a3d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2517661
    iget-object v1, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->c:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    iget-object v2, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->d:LX/1Ad;

    sget-object v3, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getLinkImage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->setLargeImageController(LX/1aZ;)V

    .line 2517662
    iget-object v1, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->c:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/35n;->setTitle(Ljava/lang/CharSequence;)V

    .line 2517663
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getLink()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2517664
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getLink()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2517665
    invoke-static {v1}, LX/2yp;->d(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2517666
    iget-object v2, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->c:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/35n;->setContextText(Ljava/lang/CharSequence;)V

    .line 2517667
    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->c:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    invoke-virtual {v1}, LX/35n;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 2517668
    iget-object v2, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v2, v2

    .line 2517669
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerCallToAction;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2517670
    const v0, 0x7f020a2f

    invoke-virtual {v1, v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setButtonBackgroundResource(I)V

    .line 2517671
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 2517672
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2517673
    iget-object v0, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialComposerCallToAction()Lcom/facebook/ipc/composer/model/ComposerCallToAction;

    move-result-object v0

    .line 2517674
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2517675
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/composer/pagecta/CallToActionComposerAttachment;->c:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    .line 2517676
    return-void
.end method
