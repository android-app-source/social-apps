.class public Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""

# interfaces
.implements LX/1RB;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;",
        "LX/1RB",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2698201
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2698202
    iput-object p2, p0, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;->a:Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;

    .line 2698203
    iput-object p1, p0, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;->b:Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;

    .line 2698204
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;
    .locals 5

    .prologue
    .line 2698190
    const-class v1, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;

    monitor-enter v1

    .line 2698191
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2698192
    sput-object v2, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2698193
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2698194
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2698195
    new-instance p0, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;)V

    .line 2698196
    move-object v0, p0

    .line 2698197
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2698198
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2698199
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2698200
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2698206
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2698207
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2698208
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2698209
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const p3, -0x658b3fa6

    if-ne v1, p3, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2698210
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;->b:Lcom/facebook/feedplugins/attachments/linkshare/ShareAttachmentImageFormatSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2698211
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/opengraph/OpenGraphAttachmentPartSelector;->a:Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2698212
    :cond_1
    const/4 v0, 0x0

    return-object v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2698205
    const/4 v0, 0x1

    return v0
.end method
