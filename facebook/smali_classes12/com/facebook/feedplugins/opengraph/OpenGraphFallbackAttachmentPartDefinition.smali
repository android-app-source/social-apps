.class public Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/attachments/angora/AngoraAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition",
            "<TE;",
            "Lcom/facebook/attachments/angora/AngoraAttachmentView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2698213
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2698214
    iput-object p1, p0, Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    .line 2698215
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;
    .locals 4

    .prologue
    .line 2698216
    const-class v1, Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;

    monitor-enter v1

    .line 2698217
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2698218
    sput-object v2, Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2698219
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2698220
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2698221
    new-instance p0, Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;)V

    .line 2698222
    move-object v0, p0

    .line 2698223
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2698224
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2698225
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2698226
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/attachments/angora/AngoraAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2698227
    sget-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2698228
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2698229
    iget-object v0, p0, Lcom/facebook/feedplugins/opengraph/OpenGraphFallbackAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2698230
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2698231
    const/4 v0, 0x1

    return v0
.end method
