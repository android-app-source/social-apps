.class public Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;LX/0Ot;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701881
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2701882
    iput-object p1, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;->a:Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;

    .line 2701883
    iput-object p2, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;->b:LX/0Ot;

    .line 2701884
    iput-object p3, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;->c:LX/0Ot;

    .line 2701885
    iput-object p4, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;->d:LX/0Uh;

    .line 2701886
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;
    .locals 7

    .prologue
    .line 2701887
    const-class v1, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;

    monitor-enter v1

    .line 2701888
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701889
    sput-object v2, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701890
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701891
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701892
    new-instance v5, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;

    const/16 v4, 0x21a0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v4, 0xa29

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, v3, v6, p0, v4}, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;-><init>(Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;LX/0Ot;LX/0Ot;LX/0Uh;)V

    .line 2701893
    move-object v0, v5

    .line 2701894
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701895
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701896
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701897
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2701877
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2701878
    iget-object v0, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;->a:Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2701879
    iget-object v0, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;->b:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;->c:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 2701880
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2701874
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2701875
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2701876
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedPartDefinition;->d:LX/0Uh;

    const/16 v2, 0x5cf

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
