.class public Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/JVv;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JVv;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701822
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2701823
    iput-object p2, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;->d:LX/JVv;

    .line 2701824
    iput-object p3, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;->e:LX/1V0;

    .line 2701825
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2701843
    new-instance v0, LX/1X6;

    sget-object v1, LX/1Ua;->i:LX/1Ua;

    sget-object v2, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v0, p2, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 2701844
    iget-object v1, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;->d:LX/JVv;

    const/4 v2, 0x0

    .line 2701845
    new-instance v3, LX/JVu;

    invoke-direct {v3, v1}, LX/JVu;-><init>(LX/JVv;)V

    .line 2701846
    iget-object v4, v1, LX/JVv;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JVt;

    .line 2701847
    if-nez v4, :cond_0

    .line 2701848
    new-instance v4, LX/JVt;

    invoke-direct {v4, v1}, LX/JVt;-><init>(LX/JVv;)V

    .line 2701849
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/JVt;->a$redex0(LX/JVt;LX/1De;IILX/JVu;)V

    .line 2701850
    move-object v3, v4

    .line 2701851
    move-object v2, v3

    .line 2701852
    move-object v1, v2

    .line 2701853
    iget-object v2, v1, LX/JVt;->a:LX/JVu;

    iput-object p2, v2, LX/JVu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2701854
    iget-object v2, v1, LX/JVt;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2701855
    move-object v1, v1

    .line 2701856
    iget-object v2, v1, LX/JVt;->a:LX/JVu;

    iput-object p3, v2, LX/JVu;->b:LX/1Pn;

    .line 2701857
    iget-object v2, v1, LX/JVt;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2701858
    move-object v1, v1

    .line 2701859
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2701860
    iget-object v2, p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;
    .locals 6

    .prologue
    .line 2701832
    const-class v1, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2701833
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701834
    sput-object v2, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701835
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701836
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701837
    new-instance p0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JVv;->a(LX/0QB;)LX/JVv;

    move-result-object v4

    check-cast v4, LX/JVv;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/JVv;LX/1V0;)V

    .line 2701838
    move-object v0, p0

    .line 2701839
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701840
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701841
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701842
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2701831
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2701830
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/psym/rows/PagesStoriesYouMissedHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2701829
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2701826
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2701827
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2701828
    check-cast v0, LX/0jW;

    return-object v0
.end method
