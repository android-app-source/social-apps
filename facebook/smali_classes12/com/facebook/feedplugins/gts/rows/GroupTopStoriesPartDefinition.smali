.class public Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

.field private final b:Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687708
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2687709
    iput-object p1, p0, Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    .line 2687710
    iput-object p2, p0, Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;->b:Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;

    .line 2687711
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;
    .locals 5

    .prologue
    .line 2687697
    const-class v1, Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;

    monitor-enter v1

    .line 2687698
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687699
    sput-object v2, Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687700
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687701
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687702
    new-instance p0, Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;)V

    .line 2687703
    move-object v0, p0

    .line 2687704
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687705
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687706
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687707
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2687712
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2687713
    iget-object v0, p0, Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;->b:Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2687714
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2687715
    if-nez v0, :cond_0

    .line 2687716
    :goto_0
    return-object v1

    .line 2687717
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/gts/rows/GroupTopStoriesPartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2687688
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x1

    .line 2687689
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2687690
    if-eqz v0, :cond_0

    .line 2687691
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2687692
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2687693
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2687694
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2687695
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2687696
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
