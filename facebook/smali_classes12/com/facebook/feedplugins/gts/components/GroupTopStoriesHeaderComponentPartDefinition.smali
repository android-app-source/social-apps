.class public Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/JOU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JOU",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JOU;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687670
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2687671
    iput-object p2, p0, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;->d:LX/JOU;

    .line 2687672
    iput-object p3, p0, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;->e:LX/1V0;

    .line 2687673
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2687627
    iget-object v0, p0, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;->d:LX/JOU;

    const/4 v1, 0x0

    .line 2687628
    new-instance v2, LX/JOT;

    invoke-direct {v2, v0}, LX/JOT;-><init>(LX/JOU;)V

    .line 2687629
    iget-object v3, v0, LX/JOU;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JOS;

    .line 2687630
    if-nez v3, :cond_0

    .line 2687631
    new-instance v3, LX/JOS;

    invoke-direct {v3, v0}, LX/JOS;-><init>(LX/JOU;)V

    .line 2687632
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JOS;->a$redex0(LX/JOS;LX/1De;IILX/JOT;)V

    .line 2687633
    move-object v2, v3

    .line 2687634
    move-object v1, v2

    .line 2687635
    move-object v0, v1

    .line 2687636
    iget-object v1, v0, LX/JOS;->a:LX/JOT;

    iput-object p2, v1, LX/JOT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2687637
    iget-object v1, v0, LX/JOS;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2687638
    move-object v1, v0

    .line 2687639
    move-object v0, p3

    check-cast v0, LX/1Pk;

    .line 2687640
    iget-object v2, v1, LX/JOS;->a:LX/JOT;

    iput-object v0, v2, LX/JOT;->b:LX/1Pk;

    .line 2687641
    iget-object v2, v1, LX/JOS;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2687642
    move-object v0, v1

    .line 2687643
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2687644
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->i:LX/1Ua;

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 2687645
    iget-object v2, p0, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;
    .locals 6

    .prologue
    .line 2687659
    const-class v1, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2687660
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687661
    sput-object v2, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687662
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687663
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687664
    new-instance p0, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JOU;->a(LX/0QB;)LX/JOU;

    move-result-object v4

    check-cast v4, LX/JOU;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/JOU;LX/1V0;)V

    .line 2687665
    move-object v0, p0

    .line 2687666
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687667
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687668
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687669
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2687658
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2687657
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/gts/components/GroupTopStoriesHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2687648
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x1

    .line 2687649
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2687650
    if-eqz v0, :cond_0

    .line 2687651
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2687652
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2687653
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2687654
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2687655
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2687656
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupTopStoriesFeedUnitStoriesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2687646
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2687647
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
