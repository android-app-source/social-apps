.class public Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/1DR;

.field private final c:LX/1nu;

.field public final d:LX/17W;

.field public final e:LX/0Zb;

.field public final f:LX/2g9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2692185
    const-class v0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1DR;LX/1nu;LX/17W;LX/0Zb;LX/2g9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2692179
    iput-object p1, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->b:LX/1DR;

    .line 2692180
    iput-object p2, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->c:LX/1nu;

    .line 2692181
    iput-object p3, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->d:LX/17W;

    .line 2692182
    iput-object p4, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->e:LX/0Zb;

    .line 2692183
    iput-object p5, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->f:LX/2g9;

    .line 2692184
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;
    .locals 9

    .prologue
    .line 2692155
    const-class v1, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;

    monitor-enter v1

    .line 2692156
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692157
    sput-object v2, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692158
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692159
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692160
    new-instance v3, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v4

    check-cast v4, LX/1DR;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v8

    check-cast v8, LX/2g9;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;-><init>(LX/1DR;LX/1nu;LX/17W;LX/0Zb;LX/2g9;)V

    .line 2692161
    move-object v0, v3

    .line 2692162
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692163
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692164
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692165
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/JQj;)LX/1Dg;
    .locals 10
    .param p2    # LX/JQj;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 2692166
    iget-object v0, p2, LX/JQj;->a:Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2692167
    const/4 v0, 0x0

    .line 2692168
    :goto_0
    return-object v0

    .line 2692169
    :cond_0
    new-instance v0, LX/JQP;

    iget-object v1, p2, LX/JQj;->a:Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v1

    invoke-direct {v0, v1}, LX/JQP;-><init>(Lcom/facebook/graphql/model/GraphQLJobOpening;)V

    .line 2692170
    iget-object v1, v0, LX/JQP;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2692171
    iget-object v2, v0, LX/JQP;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2692172
    iget-object v3, v0, LX/JQP;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2692173
    iget-object v4, v0, LX/JQP;->d:Ljava/lang/String;

    move-object v0, v4

    .line 2692174
    iget-object v4, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->b:LX/1DR;

    invoke-virtual {v4}, LX/1DR;->a()I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v4, v6

    double-to-int v4, v4

    .line 2692175
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f020a3c

    invoke-interface {v5, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->c:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v1

    sget-object v5, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v5}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v1

    sget-object v5, LX/1Up;->h:LX/1Up;

    invoke-virtual {v1, v5}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/16 v5, 0x8

    const/4 v6, 0x3

    invoke-interface {v1, v5, v6}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    const v5, 0x7f0b092c

    invoke-interface {v1, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v1, v5}, LX/1Di;->b(F)LX/1Di;

    move-result-object v1

    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/JQi;->d(LX/1De;)LX/1dQ;

    move-result-object v4

    invoke-interface {v1, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    const/16 p2, 0xc

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 2692176
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x7

    const/4 v6, 0x4

    invoke-interface {v4, v5, v6}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b092d

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x6

    const v7, 0x7f0b0917

    invoke-interface {v5, v6, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0050

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/1ne;->t(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/JQi;->d(LX/1De;)LX/1dQ;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b004e

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a043b

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/JQi;->d(LX/1De;)LX/1dQ;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b004e

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a043b

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/JQi;->d(LX/1De;)LX/1dQ;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feedplugins/jobsearch/JobSearchOpeningComponentSpec;->f:LX/2g9;

    invoke-virtual {v5, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v5

    const/16 v6, 0x101

    invoke-virtual {v5, v6}, LX/2gA;->h(I)LX/2gA;

    move-result-object v5

    const v6, 0x7f082993

    invoke-virtual {v5, v6}, LX/2gA;->i(I)LX/2gA;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, v9, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v5

    invoke-interface {v5, v8, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v5

    invoke-static {p1}, LX/JQi;->d(LX/1De;)LX/1dQ;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    move-object v0, v4

    .line 2692177
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0
.end method
