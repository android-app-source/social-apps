.class public Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;
.super LX/AnF;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static final f:LX/2eR;

.field private static w:LX/0Xm;


# instance fields
.field private final g:LX/1qa;

.field private final h:LX/1nA;

.field private final i:I

.field private final j:I

.field public final k:LX/1Ad;

.field private final l:LX/3iV;

.field public final m:LX/1Ck;

.field public final n:LX/6Ou;

.field public final o:LX/7vW;

.field public final p:LX/7vZ;

.field private final q:LX/38v;

.field private final r:LX/1nG;

.field private final s:LX/17V;

.field private final t:LX/0ad;

.field public final u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field public final v:LX/1nQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2685093
    const-class v0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2685094
    new-instance v0, LX/JMx;

    invoke-direct {v0}, LX/JMx;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->f:LX/2eR;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1qa;LX/0hB;LX/1nA;LX/1DR;LX/1Ad;LX/3iV;LX/1Ck;LX/6Ou;LX/7vW;LX/7vZ;LX/38v;LX/1nG;LX/17V;LX/0ad;LX/0Ot;LX/1nQ;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1qa;",
            "LX/0hB;",
            "LX/1nA;",
            "LX/1DR;",
            "LX/1Ad;",
            "LX/3iV;",
            "LX/1Ck;",
            "LX/6Ou;",
            "LX/7vW;",
            "LX/7vZ;",
            "LX/38v;",
            "LX/1nG;",
            "LX/17V;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/1nQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685075
    invoke-direct {p0, p1, p3, p5}, LX/AnF;-><init>(Landroid/content/Context;LX/0hB;LX/1DR;)V

    .line 2685076
    iput-object p2, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->g:LX/1qa;

    .line 2685077
    iput-object p4, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->h:LX/1nA;

    .line 2685078
    iput-object p6, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->k:LX/1Ad;

    .line 2685079
    iput-object p7, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->l:LX/3iV;

    .line 2685080
    iput-object p8, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->m:LX/1Ck;

    .line 2685081
    iput-object p9, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->n:LX/6Ou;

    .line 2685082
    iput-object p10, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->o:LX/7vW;

    .line 2685083
    iput-object p11, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->p:LX/7vZ;

    .line 2685084
    iput-object p12, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->q:LX/38v;

    .line 2685085
    iput-object p13, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->r:LX/1nG;

    .line 2685086
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->s:LX/17V;

    .line 2685087
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->t:LX/0ad;

    .line 2685088
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->u:LX/0Ot;

    .line 2685089
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->v:LX/1nQ;

    .line 2685090
    invoke-virtual {p0}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->f()I

    move-result v1

    iput v1, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->i:I

    .line 2685091
    invoke-virtual {p0}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->e()I

    move-result v1

    iput v1, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->j:I

    .line 2685092
    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;Lcom/facebook/graphql/model/GraphQLEvent;LX/JN3;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/AnC;)LX/Bni;
    .locals 7

    .prologue
    .line 2685074
    new-instance v0, LX/JN1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/JN1;-><init>(Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;Lcom/facebook/graphql/model/GraphQLEvent;LX/AnC;LX/JN3;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;
    .locals 3

    .prologue
    .line 2685066
    const-class v1, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    monitor-enter v1

    .line 2685067
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->w:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685068
    sput-object v2, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->w:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685069
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685070
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->b(LX/0QB;)Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685071
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685072
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685073
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2685053
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2685054
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2685055
    iget-object v1, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->r:LX/1nG;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {v0}, LX/2yc;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1nG;->a(Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$GetFeedStoryAttachmentFbLinkGraphQLModel;)Ljava/lang/String;

    move-result-object v1

    .line 2685056
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2685057
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 2685058
    :goto_0
    iget-object v2, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->s:LX/17V;

    const/4 v3, 0x0

    const-string v4, "native_newsfeed"

    invoke-virtual {v2, v1, v3, v0, v4}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2685059
    iget-object v2, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->h:LX/1nA;

    const-string v3, "event_ref_mechanism"

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->EVENT_CHAINING:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v4}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2685060
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p0

    .line 2685061
    invoke-virtual {p0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2685062
    invoke-virtual {p0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    move-object v1, p0

    .line 2685063
    invoke-virtual {v2, p2, v0, v1}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2685064
    return-void

    .line 2685065
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLEvent;LX/JN3;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/AnC;Z)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 2685034
    invoke-virtual {p2}, LX/JN3;->getEventActionView()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v0

    .line 2685035
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2685036
    :cond_0
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2685037
    :cond_1
    :goto_0
    return-void

    .line 2685038
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v1

    .line 2685039
    iput-object v1, p2, LX/JN3;->b:Ljava/lang/String;

    .line 2685040
    invoke-static/range {p0 .. p5}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->a(Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;Lcom/facebook/graphql/model/GraphQLEvent;LX/JN3;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/AnC;)LX/Bni;

    move-result-object v1

    .line 2685041
    iget-object v2, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->q:LX/38v;

    invoke-virtual {v2, v1}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v1

    .line 2685042
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v1

    .line 2685043
    if-eqz p6, :cond_3

    .line 2685044
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2685045
    iget-object v0, p2, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    .line 2685046
    iget-object p2, v0, Lcom/facebook/events/widget/eventcard/EventsCardView;->c:Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;

    move-object v0, p2

    .line 2685047
    move-object v0, v0

    .line 2685048
    if-eqz v0, :cond_1

    .line 2685049
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/Bne;->c(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)I

    move-result v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->a(LX/BnW;IZ)V

    .line 2685050
    new-instance v1, LX/JMy;

    invoke-direct {v1, p0, p1}, LX/JMy;-><init>(Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;Lcom/facebook/graphql/model/GraphQLEvent;)V

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2685051
    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2685052
    invoke-virtual {p2}, LX/JN3;->getEventActionView()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(LX/BnW;)V

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;
    .locals 20

    .prologue
    .line 2685032
    new-instance v2, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v4

    check-cast v4, LX/1qa;

    invoke-static/range {p0 .. p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v5

    check-cast v5, LX/0hB;

    invoke-static/range {p0 .. p0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v6

    check-cast v6, LX/1nA;

    invoke-static/range {p0 .. p0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v7

    check-cast v7, LX/1DR;

    invoke-static/range {p0 .. p0}, LX/1Ad;->a(LX/0QB;)LX/1Ad;

    move-result-object v8

    check-cast v8, LX/1Ad;

    invoke-static/range {p0 .. p0}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v9

    check-cast v9, LX/3iV;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/6Ou;->a(LX/0QB;)LX/6Ou;

    move-result-object v11

    check-cast v11, LX/6Ou;

    invoke-static/range {p0 .. p0}, LX/7vW;->a(LX/0QB;)LX/7vW;

    move-result-object v12

    check-cast v12, LX/7vW;

    invoke-static/range {p0 .. p0}, LX/7vZ;->a(LX/0QB;)LX/7vZ;

    move-result-object v13

    check-cast v13, LX/7vZ;

    const-class v14, LX/38v;

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/38v;

    invoke-static/range {p0 .. p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v15

    check-cast v15, LX/1nG;

    invoke-static/range {p0 .. p0}, LX/17V;->a(LX/0QB;)LX/17V;

    move-result-object v16

    check-cast v16, LX/17V;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    const/16 v18, 0x3be

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {p0 .. p0}, LX/1nQ;->a(LX/0QB;)LX/1nQ;

    move-result-object v19

    check-cast v19, LX/1nQ;

    invoke-direct/range {v2 .. v19}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;-><init>(Landroid/content/Context;LX/1qa;LX/0hB;LX/1nA;LX/1DR;LX/1Ad;LX/3iV;LX/1Ck;LX/6Ou;LX/7vW;LX/7vZ;LX/38v;LX/1nG;LX/17V;LX/0ad;LX/0Ot;LX/1nQ;)V

    .line 2685033
    return-object v2
.end method

.method public static b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2684990
    check-cast p0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;

    .line 2684991
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->k()LX/0Px;

    move-result-object v3

    .line 2684992
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2684993
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    move v2, v1

    .line 2684994
    :goto_0
    if-ge v1, v4, :cond_2

    .line 2684995
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2684996
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v6

    invoke-static {v6}, LX/Bnh;->b(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v6

    .line 2684997
    if-eqz v6, :cond_1

    .line 2684998
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2684999
    invoke-virtual {v5, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    .line 2685000
    :goto_1
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_0

    .line 2685001
    :cond_0
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    :cond_1
    move v0, v2

    goto :goto_1

    .line 2685002
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    move-result-object v0

    .line 2685003
    new-instance v1, LX/4WB;

    invoke-direct {v1}, LX/4WB;-><init>()V

    .line 2685004
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2685005
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;->a()LX/0Px;

    move-result-object v3

    iput-object v3, v1, LX/4WB;->b:LX/0Px;

    .line 2685006
    invoke-static {v1, v0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 2685007
    move-object v0, v1

    .line 2685008
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2685009
    iput-object v1, v0, LX/4WB;->b:LX/0Px;

    .line 2685010
    move-object v0, v0

    .line 2685011
    new-instance v1, Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;-><init>(LX/4WB;)V

    .line 2685012
    move-object v0, v1

    .line 2685013
    new-instance v9, LX/4WA;

    invoke-direct {v9}, LX/4WA;-><init>()V

    .line 2685014
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2685015
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->g()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4WA;->b:Ljava/lang/String;

    .line 2685016
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->E_()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4WA;->c:Ljava/lang/String;

    .line 2685017
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    iput-object v8, v9, LX/4WA;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2685018
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->F_()J

    move-result-wide v10

    iput-wide v10, v9, LX/4WA;->e:J

    .line 2685019
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->o()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4WA;->f:Ljava/lang/String;

    .line 2685020
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    move-result-object v8

    iput-object v8, v9, LX/4WA;->g:Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    .line 2685021
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    iput-object v8, v9, LX/4WA;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2685022
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->c()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, LX/4WA;->i:Ljava/lang/String;

    .line 2685023
    invoke-static {v9, p0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 2685024
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;->L_()LX/0x2;

    move-result-object v8

    invoke-virtual {v8}, LX/0x2;->clone()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0x2;

    iput-object v8, v9, LX/4WA;->j:LX/0x2;

    .line 2685025
    move-object v1, v9

    .line 2685026
    iput-object v0, v1, LX/4WA;->g:Lcom/facebook/graphql/model/GraphQLEventCollectionToItemConnection;

    .line 2685027
    move-object v0, v1

    .line 2685028
    new-instance v1, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;

    invoke-direct {v1, v0}, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;-><init>(LX/4WA;)V

    .line 2685029
    move-object v0, v1

    .line 2685030
    invoke-static {v0, v2}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 2685031
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 2684989
    invoke-static {p1}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/2eR;
    .locals 1

    .prologue
    .line 2685095
    sget-object v0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->f:LX/2eR;

    return-object v0
.end method

.method public final a(LX/AnE;Landroid/support/v4/view/ViewPager;Landroid/view/View;Ljava/lang/Object;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 2

    .prologue
    .line 2684983
    iget-object v0, p1, LX/AnE;->c:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    .line 2684984
    if-eqz v0, :cond_0

    .line 2684985
    iget-object v1, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->l:LX/3iV;

    invoke-virtual {v1, v0, p5}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2684986
    :cond_0
    invoke-interface {p5}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v0

    .line 2684987
    invoke-virtual {p1, v0}, LX/99W;->a(Ljava/util/List;)V

    .line 2684988
    return-void

    :cond_1
    iget-object v0, p1, LX/AnE;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2684980
    check-cast p1, LX/JN3;

    .line 2684981
    iget-object p0, p1, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a()V

    .line 2684982
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;LX/99X;LX/AnC;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 9

    .prologue
    .line 2684940
    move-object v3, p2

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2684941
    invoke-static {p5}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2684942
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/Bnh;->b(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v1

    .line 2684943
    if-nez v1, :cond_0

    .line 2684944
    :goto_0
    return-void

    .line 2684945
    :cond_0
    iget-object v2, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-static {v2, v3}, LX/Bng;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/Bnf;

    move-result-object v4

    move-object v2, p1

    .line 2684946
    check-cast v2, LX/JN3;

    .line 2684947
    iget v5, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->i:I

    iget v6, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->j:I

    .line 2684948
    iget-object v7, v2, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v7}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    .line 2684949
    iput v5, v7, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2684950
    iput v6, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2684951
    iget-object v8, v2, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v8, v7}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2684952
    invoke-virtual {p0, p1, p3}, LX/AnF;->a(Landroid/view/View;LX/99X;)V

    .line 2684953
    iget-object v5, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->v:LX/1nQ;

    .line 2684954
    iget-object v6, v2, LX/JN3;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2684955
    const-string v7, "native_newsfeed"

    sget-object v8, Lcom/facebook/events/common/ActionMechanism;->EVENT_CHAINING:Lcom/facebook/events/common/ActionMechanism;

    .line 2684956
    iget-object p1, v5, LX/1nQ;->i:LX/0Zb;

    const-string p2, "event_feed_chaining_impression"

    const/4 p3, 0x0

    invoke-interface {p1, p2, p3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p1

    .line 2684957
    invoke-virtual {p1}, LX/0oG;->a()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 2684958
    const-string p2, "native_newsfeed"

    invoke-virtual {p1, p2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p1

    iget-object p2, v5, LX/1nQ;->j:LX/0kv;

    iget-object p3, v5, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {p2, p3}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object p1

    const-string p2, "event_id"

    invoke-virtual {p1, p2, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p1

    const-string p2, "source_module"

    invoke-virtual {p1, p2, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p1

    const-string p2, "mechanism"

    invoke-virtual {p1, p2, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object p1

    invoke-virtual {p1}, LX/0oG;->d()V

    .line 2684959
    :cond_1
    iget-object v5, v2, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    move-object v5, v5

    .line 2684960
    invoke-virtual {v5}, Lcom/facebook/events/widget/eventcard/EventsCardView;->d()V

    .line 2684961
    invoke-virtual {v5}, Lcom/facebook/events/widget/eventcard/EventsCardView;->e()V

    .line 2684962
    const v6, 0x3ff745d1

    invoke-virtual {v5, v6}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoAspectRatio(F)V

    .line 2684963
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    invoke-static {v6}, LX/4Za;->b(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2684964
    :cond_2
    const/4 v6, 0x0

    .line 2684965
    :goto_1
    move-object v6, v6

    .line 2684966
    invoke-virtual {v5, v6}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoController(LX/1aZ;)V

    .line 2684967
    iget-object v6, v4, LX/Bnf;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2684968
    iget-object v6, v4, LX/Bnf;->d:Ljava/util/Date;

    invoke-virtual {v5, v6}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCalendarFormatStartDate(Ljava/util/Date;)V

    .line 2684969
    iget-object v6, v4, LX/Bnf;->b:Ljava/lang/CharSequence;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2684970
    iget-object v4, v4, LX/Bnf;->c:Ljava/lang/String;

    invoke-virtual {v5, v4}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setSocialContextText(Ljava/lang/CharSequence;)V

    .line 2684971
    iget-object v4, v2, LX/JN3;->c:Lcom/facebook/events/widget/eventcard/EventsCardView;

    move-object v4, v4

    .line 2684972
    invoke-direct {p0, v4, v0}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2684973
    iget-object v0, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->t:LX/0ad;

    sget-short v4, LX/347;->c:S

    const/4 v6, 0x0

    invoke-interface {v0, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v6

    .line 2684974
    if-eqz v6, :cond_3

    .line 2684975
    invoke-virtual {v5}, Lcom/facebook/events/widget/eventcard/EventsCardView;->h()V

    .line 2684976
    invoke-virtual {v5}, Lcom/facebook/events/widget/eventcard/EventsCardView;->f()V

    :cond_3
    move-object v0, p0

    move-object v4, p5

    move-object v5, p4

    .line 2684977
    invoke-direct/range {v0 .. v6}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->a(Lcom/facebook/graphql/model/GraphQLEvent;LX/JN3;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/AnC;Z)V

    goto/16 :goto_0

    .line 2684978
    :cond_4
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    invoke-static {v6}, LX/4Za;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    .line 2684979
    iget-object v7, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->k:LX/1Ad;

    sget-object v8, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, v8}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v7

    invoke-static {v6}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v6

    check-cast v6, LX/1Ad;

    invoke-virtual {v6}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v6

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/widget/TextView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;",
            "Landroid/widget/TextView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2684933
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2684934
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 2684935
    invoke-interface {v0}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2684936
    invoke-interface {v0}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2684937
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2684938
    :goto_0
    return-void

    .line 2684939
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2684932
    const-class v0, Lcom/facebook/graphql/model/GraphQLEventCollectionFeedUnit;

    return-object v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2684931
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f37

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 5

    .prologue
    .line 2684928
    iget-object v0, p0, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->t:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-object v2, LX/0c1;->Off:LX/0c1;

    sget-short v3, LX/347;->c:S

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0b0f34

    .line 2684929
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->f()I

    move-result v1

    invoke-static {v1}, LX/AnB;->a(I)I

    move-result v1

    iget-object v2, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, v1

    return v0

    .line 2684930
    :cond_0
    const v0, 0x7f0b0f33

    goto :goto_0
.end method

.method public final f()I
    .locals 4

    .prologue
    .line 2684927
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f32

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, LX/AnF;->b:LX/0hB;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->d()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/feedplugins/eventschaining/controllers/EventJoinChainingViewController;->g()I

    move-result v3

    invoke-static {v1, v2, v3}, LX/AnB;->a(LX/0hB;II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 2684926
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f35

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method
