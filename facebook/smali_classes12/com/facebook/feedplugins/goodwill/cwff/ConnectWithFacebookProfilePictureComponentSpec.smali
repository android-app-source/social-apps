.class public Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/JNi;

.field public final d:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2687114
    const-class v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/JNi;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2687110
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;->b:LX/1nu;

    .line 2687111
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;->c:LX/JNi;

    .line 2687112
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;->d:Landroid/content/res/Resources;

    .line 2687113
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;
    .locals 6

    .prologue
    .line 2687098
    const-class v1, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;

    monitor-enter v1

    .line 2687099
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687100
    sput-object v2, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687101
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687102
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687103
    new-instance p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/JNi;->a(LX/0QB;)LX/JNi;

    move-result-object v4

    check-cast v4, LX/JNi;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;-><init>(LX/1nu;LX/JNi;Landroid/content/res/Resources;)V

    .line 2687104
    move-object v0, p0

    .line 2687105
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687106
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookProfilePictureComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687107
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687108
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
