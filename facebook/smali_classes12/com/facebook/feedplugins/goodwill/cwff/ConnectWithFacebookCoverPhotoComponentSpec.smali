.class public Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/JNi;

.field public final d:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

.field public final e:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2686576
    const-class v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/JNi;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686570
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2686571
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->b:LX/1nu;

    .line 2686572
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->c:LX/JNi;

    .line 2686573
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->d:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    .line 2686574
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->e:LX/0Zb;

    .line 2686575
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;
    .locals 7

    .prologue
    .line 2686577
    const-class v1, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;

    monitor-enter v1

    .line 2686578
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686579
    sput-object v2, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686580
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686581
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686582
    new-instance p0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/JNi;->a(LX/0QB;)LX/JNi;

    move-result-object v4

    check-cast v4, LX/JNi;

    invoke-static {v0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    move-result-object v5

    check-cast v5, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;-><init>(LX/1nu;LX/JNi;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/0Zb;)V

    .line 2686583
    move-object v0, p0

    .line 2686584
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686585
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686586
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686587
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
