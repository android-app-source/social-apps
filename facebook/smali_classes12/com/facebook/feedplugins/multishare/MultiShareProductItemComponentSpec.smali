.class public Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/ApZ;

.field public final b:LX/2sO;

.field public final c:LX/3hk;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2693688
    const-class v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/ApZ;LX/2sO;LX/3hk;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2693690
    iput-object p1, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;->a:LX/ApZ;

    .line 2693691
    iput-object p2, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;->b:LX/2sO;

    .line 2693692
    iput-object p3, p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;->c:LX/3hk;

    .line 2693693
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;
    .locals 6

    .prologue
    .line 2693694
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;

    monitor-enter v1

    .line 2693695
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2693696
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2693697
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2693698
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2693699
    new-instance p0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;

    invoke-static {v0}, LX/ApZ;->a(LX/0QB;)LX/ApZ;

    move-result-object v3

    check-cast v3, LX/ApZ;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v4

    check-cast v4, LX/2sO;

    invoke-static {v0}, LX/3hk;->a(LX/0QB;)LX/3hk;

    move-result-object v5

    check-cast v5, LX/3hk;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;-><init>(LX/ApZ;LX/2sO;LX/3hk;)V

    .line 2693700
    move-object v0, p0

    .line 2693701
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2693702
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2693703
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2693704
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
