.class public final Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3593c7ef
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2694048
    const-class v0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2693993
    const-class v0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2694046
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2694047
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2694032
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2694033
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->a()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 2694034
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x4b0ae7be

    invoke-static {v2, v1, v3}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2694035
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->k()Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2694036
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2694037
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2694038
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2694039
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2694040
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2694041
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2694042
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2694043
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2694044
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2694045
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2694012
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2694013
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->a()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2694014
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2694015
    if-eqz v1, :cond_3

    .line 2694016
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;

    .line 2694017
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->e:LX/3Sb;

    move-object v1, v0

    .line 2694018
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2694019
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4b0ae7be

    invoke-static {v2, v0, v3}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2694020
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2694021
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;

    .line 2694022
    iput v3, v0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->f:I

    move-object v1, v0

    .line 2694023
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->k()Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2694024
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->k()Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    .line 2694025
    invoke-virtual {p0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->k()Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2694026
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;

    .line 2694027
    iput-object v0, v1, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->g:Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    .line 2694028
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2694029
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    .line 2694030
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 2694031
    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActionLinks"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2694010
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x5b0d5703

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->e:LX/3Sb;

    .line 2694011
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2694007
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2694008
    const/4 v0, 0x1

    const v1, -0x4b0ae7be

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->f:I

    .line 2694009
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2694004
    new-instance v0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;-><init>()V

    .line 2694005
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2694006
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2694003
    const v0, -0x1000cda0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2694002
    const v0, -0x4b900828

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDescription"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2694000
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2694001
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMedia"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2693998
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->g:Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    iput-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->g:Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    .line 2693999
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->g:Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2693996
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->h:Ljava/lang/String;

    .line 2693997
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2693994
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->i:Ljava/lang/String;

    .line 2693995
    iget-object v0, p0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->i:Ljava/lang/String;

    return-object v0
.end method
