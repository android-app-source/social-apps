.class public Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field public static final c:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field public final d:I

.field public final e:LX/1nu;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2693532
    const-class v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2693533
    const v0, 0x7f0e0129

    sput v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->b:I

    .line 2693534
    const v0, 0x7f0e012b

    sput v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->c:I

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1DR;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2693528
    iput-object p1, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->e:LX/1nu;

    .line 2693529
    invoke-virtual {p2}, LX/1DR;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0xc

    .line 2693530
    int-to-float v0, v0

    const v1, 0x3f46e979    # 0.777f

    mul-float/2addr v0, v1

    const/high16 v1, 0x41a00000    # 20.0f

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->d:I

    .line 2693531
    return-void
.end method

.method public static a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;
    .locals 3
    .param p2    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2693526
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, v2, p2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;
    .locals 5

    .prologue
    .line 2693515
    const-class v1, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;

    monitor-enter v1

    .line 2693516
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2693517
    sput-object v2, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2693518
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2693519
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2693520
    new-instance p0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v4

    check-cast v4, LX/1DR;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;-><init>(LX/1nu;LX/1DR;)V

    .line 2693521
    move-object v0, p0

    .line 2693522
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2693523
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2693524
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2693525
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
