.class public Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35q;",
        ":",
        "LX/35r;",
        ":",
        "LX/3VQ;",
        ":",
        "LX/3VR;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3YK;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

.field private final b:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691671
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2691672
    iput-object p1, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 2691673
    iput-object p2, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    .line 2691674
    iput-object p3, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    .line 2691675
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 2691676
    const-class v1, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;

    monitor-enter v1

    .line 2691677
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691678
    sput-object v2, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691679
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691680
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691681
    new-instance p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;)V

    .line 2691682
    move-object v0, p0

    .line 2691683
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691684
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691685
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691686
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3YK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2691687
    sget-object v0, LX/3YK;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2691688
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691689
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2691690
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2691691
    iget-object v1, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691692
    iget-object v1, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691693
    iget-object v1, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691694
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2691695
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691696
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2691697
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2691698
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v0

    return v0
.end method
