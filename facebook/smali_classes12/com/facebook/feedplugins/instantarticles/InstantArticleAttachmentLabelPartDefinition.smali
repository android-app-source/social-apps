.class public Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/35o;",
        ":",
        "LX/35r;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3YI;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final d:LX/1qb;

.field public final e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;LX/1qb;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691664
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2691665
    iput-object p1, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 2691666
    iput-object p2, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2691667
    iput-object p3, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    .line 2691668
    iput-object p4, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->d:LX/1qb;

    .line 2691669
    sget-short v0, LX/2yD;->q:S

    const/4 v1, 0x1

    invoke-interface {p5, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->e:Z

    .line 2691670
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;
    .locals 9

    .prologue
    .line 2691653
    const-class v1, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;

    monitor-enter v1

    .line 2691654
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691655
    sput-object v2, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691656
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691657
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691658
    new-instance v3, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v7

    check-cast v7, LX/1qb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;LX/1qb;LX/0ad;)V

    .line 2691659
    move-object v0, v3

    .line 2691660
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691661
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691662
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691663
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2691652
    sget-object v0, LX/3YI;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2691612
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 2691613
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2691614
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2691615
    iget-object v1, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    new-instance v2, LX/2yZ;

    iget-object v3, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->d:LX/1qb;

    invoke-static {v3, v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(LX/1qb;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v3

    invoke-static {v0}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/2yZ;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691616
    iget-object v0, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->a:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v1, LX/2ya;

    invoke-direct {v1, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691617
    sget-object v0, LX/1Ua;->g:LX/1Ua;

    .line 2691618
    iget-object v1, v0, LX/1Ua;->t:LX/1Ub;

    move-object v0, v1

    .line 2691619
    iget-object v1, v0, LX/1Ub;->d:LX/1Uc;

    move-object v0, v1

    .line 2691620
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Uc;->a(I)F

    move-result v0

    .line 2691621
    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v1

    .line 2691622
    iput v4, v1, LX/1UY;->b:F

    .line 2691623
    move-object v1, v1

    .line 2691624
    iput v4, v1, LX/1UY;->c:F

    .line 2691625
    move-object v1, v1

    .line 2691626
    iput v4, v1, LX/1UY;->e:F

    .line 2691627
    move-object v1, v1

    .line 2691628
    iput v0, v1, LX/1UY;->d:F

    .line 2691629
    move-object v0, v1

    .line 2691630
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    .line 2691631
    iget-object v1, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-direct {v2, v3, v0, v5, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691632
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x49c0c29c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2691639
    check-cast p4, LX/3YI;

    const/4 p3, 0x0

    .line 2691640
    iget-boolean v1, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->e:Z

    if-nez v1, :cond_1

    .line 2691641
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x57d08fb    # -3.4000437E35f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2691642
    :cond_1
    invoke-virtual {p4}, LX/35n;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v1

    .line 2691643
    if-eqz v1, :cond_0

    .line 2691644
    invoke-virtual {v1, p3}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->setVisibility(I)V

    .line 2691645
    iget-object v2, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v2, v2

    .line 2691646
    invoke-virtual {p4}, LX/3YI;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f080d24

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2691647
    const p1, 0x7f02079d

    invoke-virtual {v2, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setBackgroundResource(I)V

    .line 2691648
    iput-boolean p3, v1, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->g:Z

    .line 2691649
    invoke-virtual {v2, p3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setCompoundDrawablePadding(I)V

    .line 2691650
    invoke-virtual {v2, p3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setFocusable(Z)V

    .line 2691651
    invoke-virtual {v2, p3}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setClickable(Z)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2691638
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2691633
    check-cast p4, LX/3YI;

    .line 2691634
    invoke-virtual {p4}, LX/35n;->getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    move-result-object v0

    .line 2691635
    if-eqz v0, :cond_0

    .line 2691636
    invoke-virtual {v0}, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a()V

    .line 2691637
    :cond_0
    return-void
.end method
