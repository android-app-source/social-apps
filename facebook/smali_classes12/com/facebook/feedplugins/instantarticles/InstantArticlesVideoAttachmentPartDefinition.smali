.class public Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3YM;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691720
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2691721
    iput-object p1, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    .line 2691722
    iput-object p2, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    .line 2691723
    iput-object p3, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 2691724
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 2691709
    const-class v1, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;

    monitor-enter v1

    .line 2691710
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691711
    sput-object v2, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691712
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691713
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691714
    new-instance p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;)V

    .line 2691715
    move-object v0, p0

    .line 2691716
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691717
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691718
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691719
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3YM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2691725
    sget-object v0, LX/3YM;->g:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2691702
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691703
    iget-object v0, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    new-instance v1, LX/3EE;

    const/4 v2, -0x1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    new-instance v4, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v4}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-direct {v1, p2, v2, v3, v4}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691704
    iget-object v0, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/ArticleIconPartDefinition;

    .line 2691705
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2691706
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691707
    iget-object v0, p0, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v1, LX/2ya;

    invoke-direct {v1, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691708
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2691699
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691700
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2691701
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->INSTANT_ARTICLE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v0

    return v0
.end method
