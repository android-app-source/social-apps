.class public Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition",
            "<",
            "LX/1Pf;",
            "*>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            "*>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2691726
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2691727
    iput-object p1, p0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;

    .line 2691728
    iput-object p2, p0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;

    .line 2691729
    iput-object p3, p0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;->d:Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;

    .line 2691730
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;
    .locals 6

    .prologue
    .line 2691731
    const-class v1, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;

    monitor-enter v1

    .line 2691732
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691733
    sput-object v2, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691734
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691735
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691736
    new-instance p0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;)V

    .line 2691737
    const/16 v3, 0xac0

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    .line 2691738
    iput-object v3, p0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;->a:LX/0Or;

    .line 2691739
    move-object v0, p0

    .line 2691740
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691741
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691742
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691743
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2691744
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691745
    iget-object v0, p0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/instantarticles/InstantArticlePhotoAttachmentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;->d:Lcom/facebook/feedplugins/instantarticles/InstantArticlesVideoAttachmentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2691746
    iget-object v0, p0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/instantarticles/InstantArticleAttachmentLabelPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2691747
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2691748
    iget-object v0, p0, Lcom/facebook/feedplugins/instantarticles/SharedInstantArticleContentAttachmentGroupPartDefinition;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    .line 2691749
    const/16 v1, 0xc3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
