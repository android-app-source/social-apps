.class public Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/7j6;

.field public final d:LX/99j;

.field private final e:LX/JUv;

.field private final f:LX/JV0;

.field public final g:LX/1g6;

.field private final h:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2700740
    const-class v0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/7j6;LX/99j;LX/JUv;LX/JV0;LX/1g6;LX/0Zb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/7j6;",
            "LX/99j;",
            "LX/JUv;",
            "LX/JV0;",
            "LX/1g6;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2700731
    iput-object p1, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->b:LX/0Or;

    .line 2700732
    iput-object p2, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->c:LX/7j6;

    .line 2700733
    iput-object p3, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->d:LX/99j;

    .line 2700734
    iput-object p4, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->e:LX/JUv;

    .line 2700735
    iput-object p5, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->f:LX/JV0;

    .line 2700736
    iput-object p6, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->g:LX/1g6;

    .line 2700737
    iput-object p7, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->h:LX/0Zb;

    .line 2700738
    return-void
.end method

.method public static a(LX/1De;)LX/1Di;
    .locals 2

    .prologue
    .line 2700739
    invoke-static {p0}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    const v1, 0x7f0a009f

    invoke-virtual {v0, v1}, LX/25Q;->i(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;
    .locals 11

    .prologue
    .line 2700741
    const-class v1, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;

    monitor-enter v1

    .line 2700742
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700743
    sput-object v2, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700744
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700745
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700746
    new-instance v3, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v5

    check-cast v5, LX/7j6;

    invoke-static {v0}, LX/99j;->a(LX/0QB;)LX/99j;

    move-result-object v6

    check-cast v6, LX/99j;

    invoke-static {v0}, LX/JUv;->a(LX/0QB;)LX/JUv;

    move-result-object v7

    check-cast v7, LX/JUv;

    invoke-static {v0}, LX/JV0;->a(LX/0QB;)LX/JV0;

    move-result-object v8

    check-cast v8, LX/JV0;

    invoke-static {v0}, LX/1g6;->a(LX/0QB;)LX/1g6;

    move-result-object v9

    check-cast v9, LX/1g6;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;-><init>(LX/0Or;LX/7j6;LX/99j;LX/JUv;LX/JV0;LX/1g6;LX/0Zb;)V

    .line 2700747
    move-object v0, v3

    .line 2700748
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700749
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700750
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700751
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2700604
    invoke-static {p2, p1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v0

    invoke-static {p3, v0}, LX/17Q;->b(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2700605
    iget-object v1, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->h:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2700606
    return-void
.end method

.method public static b(Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLProductItem;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;)LX/1Di;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLProductItem;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;",
            ")",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 2700607
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0a0097

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->e:LX/JUv;

    const/4 v2, 0x0

    .line 2700608
    new-instance v4, LX/JUt;

    invoke-direct {v4, v1}, LX/JUt;-><init>(LX/JUv;)V

    .line 2700609
    sget-object v5, LX/JUv;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/JUs;

    .line 2700610
    if-nez v5, :cond_0

    .line 2700611
    new-instance v5, LX/JUs;

    invoke-direct {v5}, LX/JUs;-><init>()V

    .line 2700612
    :cond_0
    invoke-static {v5, p1, v2, v2, v4}, LX/JUs;->a$redex0(LX/JUs;LX/1De;IILX/JUt;)V

    .line 2700613
    move-object v4, v5

    .line 2700614
    move-object v2, v4

    .line 2700615
    move-object v1, v2

    .line 2700616
    new-instance v4, LX/4XR;

    invoke-direct {v4}, LX/4XR;-><init>()V

    .line 2700617
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->j()Z

    move-result v5

    .line 2700618
    iput-boolean v5, v4, LX/4XR;->bC:Z

    .line 2700619
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->k()Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    move-result-object v5

    .line 2700620
    iput-object v5, v4, LX/4XR;->ce:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    .line 2700621
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->l()Z

    move-result v5

    .line 2700622
    iput-boolean v5, v4, LX/4XR;->cf:Z

    .line 2700623
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->m()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v5

    .line 2700624
    iput-object v5, v4, LX/4XR;->ch:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    .line 2700625
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->n()J

    move-result-wide v6

    .line 2700626
    iput-wide v6, v4, LX/4XR;->cF:J

    .line 2700627
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->o()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v5

    .line 2700628
    iput-object v5, v4, LX/4XR;->cN:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 2700629
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->p()Ljava/lang/String;

    move-result-object v5

    .line 2700630
    iput-object v5, v4, LX/4XR;->cT:Ljava/lang/String;

    .line 2700631
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->q()Ljava/lang/String;

    move-result-object v5

    .line 2700632
    iput-object v5, v4, LX/4XR;->em:Ljava/lang/String;

    .line 2700633
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->r()Ljava/lang/String;

    move-result-object v5

    .line 2700634
    iput-object v5, v4, LX/4XR;->er:Ljava/lang/String;

    .line 2700635
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->s()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    .line 2700636
    iput-object v5, v4, LX/4XR;->eA:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2700637
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->t()Ljava/lang/String;

    move-result-object v5

    .line 2700638
    iput-object v5, v4, LX/4XR;->fO:Ljava/lang/String;

    .line 2700639
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 2700640
    iput-object v5, v4, LX/4XR;->fP:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2700641
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->v()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 2700642
    iput-object v5, v4, LX/4XR;->fR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2700643
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->w()Z

    move-result v5

    .line 2700644
    iput-boolean v5, v4, LX/4XR;->gL:Z

    .line 2700645
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->O()Z

    move-result v5

    .line 2700646
    iput-boolean v5, v4, LX/4XR;->ha:Z

    .line 2700647
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->x()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v5

    .line 2700648
    iput-object v5, v4, LX/4XR;->hr:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 2700649
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->y()Ljava/lang/String;

    move-result-object v5

    .line 2700650
    iput-object v5, v4, LX/4XR;->iB:Ljava/lang/String;

    .line 2700651
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->Q()LX/0Px;

    move-result-object v5

    .line 2700652
    iput-object v5, v4, LX/4XR;->iX:LX/0Px;

    .line 2700653
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    .line 2700654
    iput-object v5, v4, LX/4XR;->jd:Lcom/facebook/graphql/model/GraphQLPage;

    .line 2700655
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->A()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 2700656
    iput-object v5, v4, LX/4XR;->jl:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2700657
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->S()LX/0Px;

    move-result-object v5

    .line 2700658
    iput-object v5, v4, LX/4XR;->kE:LX/0Px;

    .line 2700659
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->R()Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    move-result-object v5

    .line 2700660
    iput-object v5, v4, LX/4XR;->kG:Lcom/facebook/graphql/model/GraphQLCurrencyAmount;

    .line 2700661
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->B()D

    move-result-wide v6

    .line 2700662
    iput-wide v6, v4, LX/4XR;->kH:D

    .line 2700663
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->C()D

    move-result-wide v6

    .line 2700664
    iput-wide v6, v4, LX/4XR;->kI:D

    .line 2700665
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->D()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 2700666
    iput-object v5, v4, LX/4XR;->kJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2700667
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->E()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 2700668
    iput-object v5, v4, LX/4XR;->kK:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2700669
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->G()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 2700670
    iput-object v5, v4, LX/4XR;->kM:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2700671
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->H()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v5

    .line 2700672
    iput-object v5, v4, LX/4XR;->kQ:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2700673
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 2700674
    iput-object v5, v4, LX/4XR;->kR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2700675
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->J()Z

    move-result v5

    .line 2700676
    iput-boolean v5, v4, LX/4XR;->kS:Z

    .line 2700677
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->K()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v5

    .line 2700678
    iput-object v5, v4, LX/4XR;->lH:Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    .line 2700679
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->L()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    .line 2700680
    iput-object v5, v4, LX/4XR;->lY:Lcom/facebook/graphql/model/GraphQLActor;

    .line 2700681
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->M()Ljava/lang/String;

    move-result-object v5

    .line 2700682
    iput-object v5, v4, LX/4XR;->oU:Ljava/lang/String;

    .line 2700683
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->N()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v5

    .line 2700684
    iput-object v5, v4, LX/4XR;->pI:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2700685
    new-instance v5, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v6, 0xa7c5482

    invoke-direct {v5, v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2700686
    iput-object v5, v4, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2700687
    invoke-virtual {v4}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    move-object v2, v4

    .line 2700688
    iget-object v4, v1, LX/JUs;->a:LX/JUt;

    iput-object v2, v4, LX/JUt;->b:Lcom/facebook/graphql/model/GraphQLNode;

    .line 2700689
    iget-object v4, v1, LX/JUs;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2700690
    move-object v1, v1

    .line 2700691
    new-instance v2, LX/JVP;

    invoke-direct {v2, p0, p3, p4, p2}, LX/JVP;-><init>(Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;Lcom/facebook/graphql/model/GraphQLProductItem;)V

    .line 2700692
    iget-object v4, v1, LX/JUs;->a:LX/JUt;

    iput-object v2, v4, LX/JUt;->c:LX/JVP;

    .line 2700693
    move-object v1, v1

    .line 2700694
    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v3}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->f:LX/JV0;

    const/4 v2, 0x0

    .line 2700695
    new-instance v4, LX/JUz;

    invoke-direct {v4, v1}, LX/JUz;-><init>(LX/JV0;)V

    .line 2700696
    sget-object v5, LX/JV0;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/JUy;

    .line 2700697
    if-nez v5, :cond_1

    .line 2700698
    new-instance v5, LX/JUy;

    invoke-direct {v5}, LX/JUy;-><init>()V

    .line 2700699
    :cond_1
    invoke-static {v5, p1, v2, v2, v4}, LX/JUy;->a$redex0(LX/JUy;LX/1De;IILX/JUz;)V

    .line 2700700
    move-object v4, v5

    .line 2700701
    move-object v2, v4

    .line 2700702
    move-object v1, v2

    .line 2700703
    new-instance v2, LX/170;

    invoke-direct {v2}, LX/170;-><init>()V

    .line 2700704
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->r()Ljava/lang/String;

    move-result-object v4

    .line 2700705
    iput-object v4, v2, LX/170;->k:Ljava/lang/String;

    .line 2700706
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->t()Ljava/lang/String;

    move-result-object v4

    .line 2700707
    iput-object v4, v2, LX/170;->o:Ljava/lang/String;

    .line 2700708
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2700709
    iput-object v4, v2, LX/170;->p:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2700710
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->y()Ljava/lang/String;

    move-result-object v4

    .line 2700711
    iput-object v4, v2, LX/170;->A:Ljava/lang/String;

    .line 2700712
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->z()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    .line 2700713
    iput-object v4, v2, LX/170;->C:Lcom/facebook/graphql/model/GraphQLPage;

    .line 2700714
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->F()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2700715
    iput-object v4, v2, LX/170;->K:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2700716
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->I()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2700717
    iput-object v4, v2, LX/170;->L:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2700718
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLProductItem;->M()Ljava/lang/String;

    move-result-object v4

    .line 2700719
    iput-object v4, v2, LX/170;->Y:Ljava/lang/String;

    .line 2700720
    new-instance v4, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v5, 0xa7c5482

    invoke-direct {v4, v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2700721
    iput-object v4, v2, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2700722
    invoke-virtual {v2}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    move-object v2, v2

    .line 2700723
    iget-object v4, v1, LX/JUy;->a:LX/JUz;

    iput-object v2, v4, LX/JUz;->a:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 2700724
    iget-object v4, v1, LX/JUy;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2700725
    move-object v1, v1

    .line 2700726
    new-instance v2, LX/JVO;

    invoke-direct {v2, p0, p3, p4, p2}, LX/JVO;-><init>(Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;Lcom/facebook/graphql/model/GraphQLProductItem;)V

    .line 2700727
    iget-object v4, v1, LX/JUy;->a:LX/JUz;

    iput-object v2, v4, LX/JUz;->b:LX/JVO;

    .line 2700728
    move-object v1, v1

    .line 2700729
    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v3}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    return-object v0
.end method
