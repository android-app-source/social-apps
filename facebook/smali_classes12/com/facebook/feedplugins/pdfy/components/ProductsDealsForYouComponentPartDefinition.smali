.class public Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;",
        ">;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/JV4;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JV4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700229
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2700230
    iput-object p2, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;->d:LX/JV4;

    .line 2700231
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;",
            ">;",
            "LX/1Pf;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2700232
    iget-object v0, p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;->d:LX/JV4;

    const/4 v1, 0x0

    .line 2700233
    new-instance v2, LX/JV3;

    invoke-direct {v2, v0}, LX/JV3;-><init>(LX/JV4;)V

    .line 2700234
    sget-object p0, LX/JV4;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JV2;

    .line 2700235
    if-nez p0, :cond_0

    .line 2700236
    new-instance p0, LX/JV2;

    invoke-direct {p0}, LX/JV2;-><init>()V

    .line 2700237
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/JV2;->a$redex0(LX/JV2;LX/1De;IILX/JV3;)V

    .line 2700238
    move-object v2, p0

    .line 2700239
    move-object v1, v2

    .line 2700240
    move-object v0, v1

    .line 2700241
    iget-object v1, v0, LX/JV2;->a:LX/JV3;

    iput-object p3, v1, LX/JV3;->a:LX/1Pf;

    .line 2700242
    iget-object v1, v0, LX/JV2;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2700243
    move-object v0, v0

    .line 2700244
    iget-object v1, v0, LX/JV2;->a:LX/JV3;

    iput-object p2, v1, LX/JV3;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700245
    iget-object v1, v0, LX/JV2;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2700246
    move-object v0, v0

    .line 2700247
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;
    .locals 5

    .prologue
    .line 2700210
    const-class v1, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;

    monitor-enter v1

    .line 2700211
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700212
    sput-object v2, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700213
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700214
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700215
    new-instance p0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JV4;->a(LX/0QB;)LX/JV4;

    move-result-object v4

    check-cast v4, LX/JV4;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;-><init>(Landroid/content/Context;LX/JV4;)V

    .line 2700216
    move-object v0, p0

    .line 2700217
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700218
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700219
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700220
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2700228
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2700227
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2700223
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700224
    if-eqz p1, :cond_0

    .line 2700225
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2700226
    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2700221
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2700222
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
