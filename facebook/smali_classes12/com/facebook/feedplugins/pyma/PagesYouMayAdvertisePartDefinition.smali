.class public Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;

.field private final c:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702053
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2702054
    iput-object p2, p0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;->b:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;

    .line 2702055
    iput-object p3, p0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;->c:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;

    .line 2702056
    iput-object p1, p0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;->a:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;

    .line 2702057
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;
    .locals 6

    .prologue
    .line 2702035
    const-class v1, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;

    monitor-enter v1

    .line 2702036
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702037
    sput-object v2, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702038
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702039
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702040
    new-instance p0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;-><init>(Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;)V

    .line 2702041
    move-object v0, p0

    .line 2702042
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702043
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702044
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702045
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2702048
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702049
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;->a:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2702050
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;->b:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2702051
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;->c:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2702052
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2702046
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702047
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;->a:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseHeaderComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;->b:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/PagesYouMayAdvertisePartDefinition;->c:Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseFooterComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
