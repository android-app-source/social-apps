.class public Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/JVx;

.field private final d:LX/JVy;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/JVx;LX/JVy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAPageLikeObjectiveBodyComponentPartDefinition;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/feedplugins/pyma/rows/objectives/PYMAGraphQLObjectivesPartDefinition;",
            ">;",
            "LX/JVx;",
            "LX/JVy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2702061
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2702062
    iput-object p1, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->a:LX/0Or;

    .line 2702063
    iput-object p2, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->b:LX/0Or;

    .line 2702064
    iput-object p3, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->c:LX/JVx;

    .line 2702065
    iput-object p4, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->d:LX/JVy;

    .line 2702066
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;
    .locals 7

    .prologue
    .line 2702067
    const-class v1, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;

    monitor-enter v1

    .line 2702068
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2702069
    sput-object v2, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2702070
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2702071
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2702072
    new-instance v5, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;

    const/16 v3, 0x20c2

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v3, 0x20c1

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/JVx;->a(LX/0QB;)LX/JVx;

    move-result-object v3

    check-cast v3, LX/JVx;

    invoke-static {v0}, LX/JVy;->a(LX/0QB;)LX/JVy;

    move-result-object v4

    check-cast v4, LX/JVy;

    invoke-direct {v5, v6, p0, v3, v4}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;-><init>(LX/0Or;LX/0Or;LX/JVx;LX/JVy;)V

    .line 2702073
    move-object v0, v5

    .line 2702074
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2702075
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2702076
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2702077
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;)",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;-",
            "LX/1Pf;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2702078
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702079
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLPYMACategory;

    move-result-object v0

    .line 2702080
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2702081
    :goto_0
    return-object v0

    .line 2702082
    :cond_0
    sget-object v2, LX/JW0;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPYMACategory;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2702083
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->d:LX/JVy;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, LX/JVy;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2702084
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0

    .line 2702085
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0

    .line 2702086
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 2702087
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2702088
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2702089
    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2702090
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2702091
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    .line 2702092
    if-nez v0, :cond_0

    .line 2702093
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702094
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    .line 2702095
    iget-object v1, p0, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->c:LX/JVx;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported PYMA objective: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLPYMACategory;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    .line 2702096
    const/4 v0, 0x0

    .line 2702097
    :goto_0
    return v0

    :cond_0
    invoke-interface {v0, p1}, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2702098
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/pyma/rows/PagesYouMayAdvertiseBodyPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
