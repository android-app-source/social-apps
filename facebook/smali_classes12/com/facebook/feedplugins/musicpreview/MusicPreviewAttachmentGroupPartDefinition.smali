.class public Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;

.field private final b:Lcom/facebook/feedplugins/musicpreview/MusicPreviewWidePartDefinition;

.field private final c:LX/3YR;

.field private final d:Z

.field private final e:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;Lcom/facebook/feedplugins/musicpreview/MusicPreviewWidePartDefinition;LX/3YR;Ljava/lang/Boolean;Landroid/content/res/Resources;)V
    .locals 1
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2694558
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2694559
    iput-object p1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;

    .line 2694560
    iput-object p2, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/musicpreview/MusicPreviewWidePartDefinition;

    .line 2694561
    iput-object p3, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->c:LX/3YR;

    .line 2694562
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->d:Z

    .line 2694563
    iput-object p5, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->e:Landroid/content/res/Resources;

    .line 2694564
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;
    .locals 9

    .prologue
    .line 2694547
    const-class v1, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;

    monitor-enter v1

    .line 2694548
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2694549
    sput-object v2, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2694550
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2694551
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2694552
    new-instance v3, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewWidePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/musicpreview/MusicPreviewWidePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/musicpreview/MusicPreviewWidePartDefinition;

    invoke-static {v0}, LX/3YR;->a(LX/0QB;)LX/3YR;

    move-result-object v6

    check-cast v6, LX/3YR;

    invoke-static {v0}, LX/0rr;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v8

    check-cast v8, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;Lcom/facebook/feedplugins/musicpreview/MusicPreviewWidePartDefinition;LX/3YR;Ljava/lang/Boolean;Landroid/content/res/Resources;)V

    .line 2694553
    move-object v0, v3

    .line 2694554
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2694555
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2694556
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2694557
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2694539
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2694540
    iget-object v2, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->e:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    move v2, v1

    .line 2694541
    :goto_0
    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->d:Z

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 2694542
    :cond_1
    if-eqz v0, :cond_3

    .line 2694543
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/musicpreview/MusicPreviewWidePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2694544
    :goto_1
    const/4 v0, 0x0

    return-object v0

    :cond_2
    move v2, v0

    .line 2694545
    goto :goto_0

    .line 2694546
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/musicpreview/MusicPreviewSquarePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2694523
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2694524
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2694525
    if-nez v0, :cond_0

    .line 2694526
    const/4 v2, 0x0

    .line 2694527
    :goto_0
    move v0, v2

    .line 2694528
    return v0

    .line 2694529
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2694530
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    const/4 p0, 0x0

    .line 2694531
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    move p1, p0

    :goto_1
    if-ge p1, v0, :cond_3

    invoke-virtual {v2, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2694532
    invoke-static {v3}, LX/3YR;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v3

    if-nez v3, :cond_2

    move v3, p0

    .line 2694533
    :goto_2
    move v2, v3

    .line 2694534
    goto :goto_0

    .line 2694535
    :cond_1
    invoke-static {v0}, LX/3YR;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    goto :goto_0

    .line 2694536
    :cond_2
    add-int/lit8 v3, p1, 0x1

    move p1, v3

    goto :goto_1

    .line 2694537
    :cond_3
    const/4 v3, 0x1

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2694538
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewAttachmentGroupPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
