.class public final Lcom/facebook/feedplugins/musicpreview/MusicPreviewView$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;)V
    .locals 0

    .prologue
    .line 2694688
    iput-object p1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView$1;->a:Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2694689
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView$1;->a:Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;

    .line 2694690
    iget-boolean v1, v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->x:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->y:Lcom/facebook/graphql/model/GraphQLMedia;

    if-nez v1, :cond_1

    .line 2694691
    :cond_0
    :goto_0
    return-void

    .line 2694692
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->x:Z

    .line 2694693
    iget-object v1, v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->i:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->i:Ljava/lang/String;

    const-string v2, "com.nttdocomo.android.selection"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2694694
    iget-object v1, v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->y:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    move-object v2, v1

    .line 2694695
    :goto_1
    if-eqz v2, :cond_0

    .line 2694696
    iget-object v1, v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->v:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v3, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->r:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-static {v2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 2694697
    iget-object v2, v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_0

    .line 2694698
    :cond_2
    iget-object v1, v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->u:LX/1qa;

    iget-object v2, v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->y:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getHeight()I

    move-result v3

    sget-object p0, LX/26P;->MusicPreviewCover:LX/26P;

    invoke-virtual {v1, v2, v3, p0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;ILX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    move-object v2, v1

    goto :goto_1
.end method
