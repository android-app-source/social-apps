.class public Lcom/facebook/feedplugins/musicpreview/MusicButton;
.super Landroid/widget/ImageButton;
.source ""


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private c:LX/JRy;

.field private d:Landroid/graphics/Paint;

.field private e:F

.field private f:F

.field private g:F

.field private h:Landroid/graphics/RectF;

.field public i:Landroid/widget/ProgressBar;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2694520
    const v0, 0x7f020bbf

    sput v0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->a:I

    .line 2694521
    const v0, 0x7f020bc0

    sput v0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2694513
    invoke-direct {p0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 2694514
    sget-object v0, LX/JRy;->STOPPED:LX/JRy;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->c:LX/JRy;

    .line 2694515
    iput v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->e:F

    .line 2694516
    iput v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    .line 2694517
    iput v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->g:F

    .line 2694518
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->a()V

    .line 2694519
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2694506
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2694507
    sget-object v0, LX/JRy;->STOPPED:LX/JRy;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->c:LX/JRy;

    .line 2694508
    iput v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->e:F

    .line 2694509
    iput v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    .line 2694510
    iput v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->g:F

    .line 2694511
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->a()V

    .line 2694512
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2694499
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2694500
    sget-object v0, LX/JRy;->STOPPED:LX/JRy;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->c:LX/JRy;

    .line 2694501
    iput v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->e:F

    .line 2694502
    iput v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    .line 2694503
    iput v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->g:F

    .line 2694504
    invoke-direct {p0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->a()V

    .line 2694505
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 2694487
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->d:Landroid/graphics/Paint;

    .line 2694488
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->d:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2694489
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setBackgroundResource(I)V

    .line 2694490
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2694491
    sget v0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->b:I

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setImageResource(I)V

    .line 2694492
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->i:Landroid/widget/ProgressBar;

    .line 2694493
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->i:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2694494
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2694495
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2694496
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2694497
    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/JRx;

    invoke-direct {v1, p0}, LX/JRx;-><init>(Lcom/facebook/feedplugins/musicpreview/MusicButton;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2694498
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2694440
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0593

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2694441
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2694442
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->g:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2694443
    iget v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    iget v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    iget v2, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    iget v3, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->g:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2694444
    return-void
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2694479
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->c:LX/JRy;

    sget-object v1, LX/JRy;->STOPPED:LX/JRy;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 2694480
    :goto_0
    const/high16 v1, 0x43b40000    # 360.0f

    mul-float v3, v0, v1

    .line 2694481
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0594

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2694482
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2694483
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->g:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2694484
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->h:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 2694485
    return-void

    .line 2694486
    :cond_0
    iget v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->e:F

    goto :goto_0
.end method


# virtual methods
.method public getPlayingStatus()LX/JRy;
    .locals 1

    .prologue
    .line 2694522
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->c:LX/JRy;

    return-object v0
.end method

.method public getProgress()F
    .locals 1

    .prologue
    .line 2694478
    iget v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->e:F

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 2694474
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->a(Landroid/graphics/Canvas;)V

    .line 2694475
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->b(Landroid/graphics/Canvas;)V

    .line 2694476
    invoke-super {p0, p1}, Landroid/widget/ImageButton;->onDraw(Landroid/graphics/Canvas;)V

    .line 2694477
    return-void
.end method

.method public final onMeasure(II)V
    .locals 0

    .prologue
    .line 2694472
    invoke-super {p0, p1, p1}, Landroid/widget/ImageButton;->onMeasure(II)V

    .line 2694473
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/high16 v7, 0x40000000    # 2.0f

    const/16 v0, 0x2c

    const v1, 0x5fbbba33

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2694465
    invoke-super {p0, p1, p1, p3, p4}, Landroid/widget/ImageButton;->onSizeChanged(IIII)V

    .line 2694466
    int-to-float v1, p1

    div-float/2addr v1, v7

    iput v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    .line 2694467
    iget v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    const v2, 0x3dcccccd    # 0.1f

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->g:F

    .line 2694468
    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->getWidth()I

    move-result v1

    int-to-double v2, v1

    const-wide v4, 0x3fd1eb851eb851ecL    # 0.28

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 2694469
    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setPadding(IIII)V

    .line 2694470
    new-instance v1, Landroid/graphics/RectF;

    iget v2, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->g:F

    div-float/2addr v2, v7

    iget v3, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->g:F

    div-float/2addr v3, v7

    iget v4, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    mul-float/2addr v4, v7

    iget v5, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->g:F

    div-float/2addr v5, v7

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    mul-float/2addr v5, v7

    iget v6, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->g:F

    div-float/2addr v6, v7

    sub-float/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->h:Landroid/graphics/RectF;

    .line 2694471
    const/16 v1, 0x2d

    const v2, -0x2cc9f143

    invoke-static {v8, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x4b858a79

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2694459
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 2694460
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 2694461
    iget v3, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 2694462
    iget v3, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 2694463
    float-to-double v4, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    float-to-double v2, v2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 2694464
    iget v2, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->f:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/ImageButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    const v2, -0x74637bf9

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPlayingStatus(LX/JRy;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2694448
    iput-object p1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->c:LX/JRy;

    .line 2694449
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->c:LX/JRy;

    sget-object v1, LX/JRy;->PLAYING:LX/JRy;

    if-ne v0, v1, :cond_0

    .line 2694450
    sget v0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->a:I

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setImageResource(I)V

    .line 2694451
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2694452
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->invalidate()V

    .line 2694453
    return-void

    .line 2694454
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->c:LX/JRy;

    sget-object v1, LX/JRy;->BUFFERING:LX/JRy;

    if-ne v0, v1, :cond_1

    .line 2694455
    invoke-virtual {p0, v2}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setImageResource(I)V

    .line 2694456
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 2694457
    :cond_1
    sget v0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->b:I

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setImageResource(I)V

    .line 2694458
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public setProgress(F)V
    .locals 0

    .prologue
    .line 2694445
    iput p1, p0, Lcom/facebook/feedplugins/musicpreview/MusicButton;->e:F

    .line 2694446
    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->invalidate()V

    .line 2694447
    return-void
.end method
