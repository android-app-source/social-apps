.class public Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/JS8;


# static fields
.field public static final r:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

.field public h:Landroid/net/Uri;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Landroid/view/ViewGroup;

.field public l:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/attachments/angora/actionbutton/SaveButton;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:LX/162;

.field public s:LX/JTe;

.field public t:LX/JRz;

.field public u:LX/1qa;

.field public v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/JS5;

.field public x:Z

.field public y:Lcom/facebook/graphql/model/GraphQLMedia;

.field public final z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2694700
    const-class v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->r:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 2694701
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2694702
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->n:LX/0am;

    .line 2694703
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->x:Z

    .line 2694704
    iput-boolean p2, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->z:Z

    .line 2694705
    const-class v0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2694706
    iget-boolean v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->z:Z

    if-eqz v0, :cond_0

    const v0, 0x7f030ba7

    .line 2694707
    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2694708
    const v0, 0x7f0d1ce2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2694709
    const v0, 0x7f0d1ce4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->b:Landroid/widget/TextView;

    .line 2694710
    const v0, 0x7f0d1ce5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->c:Landroid/widget/TextView;

    .line 2694711
    const v0, 0x7f0d1ce8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->d:Landroid/widget/TextView;

    .line 2694712
    const v0, 0x7f0d1ce7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2694713
    const v0, 0x7f0d1ce3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/musicpreview/MusicButton;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    .line 2694714
    const v0, 0x7f0d1ce9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->d(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->n:LX/0am;

    .line 2694715
    const v0, 0x7f0d1ce6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->k:Landroid/view/ViewGroup;

    .line 2694716
    const v0, 0x7f0d1cec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->d(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->l:LX/0am;

    .line 2694717
    const v0, 0x7f0d1ceb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->d(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->m:LX/0am;

    .line 2694718
    invoke-virtual {p0, p0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2694719
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->k:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2694720
    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2694721
    new-instance p1, LX/1Uo;

    invoke-direct {p1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const p2, 0x7f0a045d

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    .line 2694722
    iput-object p2, p1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2694723
    move-object p1, p1

    .line 2694724
    new-instance p2, LX/JRw;

    invoke-direct {p2, v0}, LX/JRw;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {p1, p2}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    .line 2694725
    iget-object p1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2694726
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2694727
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    invoke-virtual {v0, p0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2694728
    return-void

    .line 2694729
    :cond_0
    const v0, 0x7f030ba6

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v1, p1

    check-cast v1, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;

    invoke-static {v6}, LX/JTe;->a(LX/0QB;)LX/JTe;

    move-result-object v2

    check-cast v2, LX/JTe;

    invoke-static {v6}, LX/JRz;->a(LX/0QB;)LX/JRz;

    move-result-object v3

    check-cast v3, LX/JRz;

    invoke-static {v6}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v4

    check-cast v4, LX/1qa;

    const/16 v5, 0x509

    invoke-static {v6, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    new-instance v7, LX/JS5;

    invoke-static {v6}, LX/3N2;->b(LX/0QB;)LX/3N2;

    move-result-object v8

    check-cast v8, LX/3N2;

    invoke-static {v6}, LX/JRz;->a(LX/0QB;)LX/JRz;

    move-result-object v9

    check-cast v9, LX/JRz;

    invoke-static {v6}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v6}, LX/17T;->b(LX/0QB;)LX/17T;

    move-result-object v11

    check-cast v11, LX/17T;

    invoke-static {v6}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v12

    check-cast v12, LX/17d;

    invoke-direct/range {v7 .. v12}, LX/JS5;-><init>(LX/3N2;LX/JRz;Lcom/facebook/content/SecureContextHelper;LX/17T;LX/17d;)V

    move-object v6, v7

    check-cast v6, LX/JS5;

    iput-object v2, v1, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->s:LX/JTe;

    iput-object v3, v1, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->t:LX/JRz;

    iput-object v4, v1, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->u:LX/1qa;

    iput-object v5, v1, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->v:LX/0Or;

    iput-object v6, v1, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->w:LX/JS5;

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 2694730
    int-to-float v0, p2

    int-to-float v1, p1

    div-float/2addr v0, v1

    .line 2694731
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setProgress(F)V

    .line 2694732
    return-void
.end method

.method public final a(Landroid/net/Uri;II)V
    .locals 8

    .prologue
    .line 2694733
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->t:LX/JRz;

    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->q:LX/162;

    iget-object v4, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->f:Ljava/lang/String;

    if-nez p1, :cond_0

    const-string v5, ""

    :goto_0
    move v6, p2

    move v7, p3

    .line 2694734
    iget-object p0, v0, LX/JRz;->a:LX/0Zb;

    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "music_preview_media_player_error"

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p2, "og_song_id"

    invoke-virtual {p1, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "og_object_id"

    invoke-virtual {p1, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "tracking_codes"

    invoke-virtual {p1, p2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "provider_name"

    invoke-virtual {p1, p2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "song_clip_url"

    invoke-virtual {p1, p2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "error_type"

    .line 2694735
    sget-object p3, LX/JRz;->c:LX/0P1;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-static {p3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p3

    .line 2694736
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unknown_error_type_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    move-object p3, p3

    .line 2694737
    invoke-virtual {p1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "error_info"

    .line 2694738
    sget-object p3, LX/JRz;->b:LX/0P1;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    invoke-static {p3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p3

    .line 2694739
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unknown_error_info_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    move-object p3, p3

    .line 2694740
    invoke-virtual {p1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2694741
    return-void

    .line 2694742
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0
.end method

.method public final a(Landroid/net/Uri;LX/JTd;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2694743
    sget-object v0, LX/JS7;->a:[I

    invoke-virtual {p2}, LX/JTd;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2694744
    :goto_0
    return-void

    .line 2694745
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->t:LX/JRz;

    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->q:LX/162;

    iget-object v4, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->f:Ljava/lang/String;

    .line 2694746
    iget-object v5, v0, LX/JRz;->a:LX/0Zb;

    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "music_preview_play"

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p2, "og_song_id"

    invoke-virtual {p1, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "og_object_id"

    invoke-virtual {p1, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "tracking_codes"

    invoke-virtual {p1, p2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "provider_name"

    invoke-virtual {p1, p2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v5, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2694747
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    sget-object v1, LX/JRy;->PLAYING:LX/JRy;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setPlayingStatus(LX/JRy;)V

    goto :goto_0

    .line 2694748
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    sget-object v1, LX/JRy;->PAUSED:LX/JRy;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setPlayingStatus(LX/JRy;)V

    goto :goto_0

    .line 2694749
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    invoke-virtual {v0, v2}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setProgress(F)V

    .line 2694750
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    sget-object v1, LX/JRy;->STOPPED:LX/JRy;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setPlayingStatus(LX/JRy;)V

    goto :goto_0

    .line 2694751
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    invoke-virtual {v0, v2}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setProgress(F)V

    .line 2694752
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    sget-object v1, LX/JRy;->BUFFERING:LX/JRy;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->setPlayingStatus(LX/JRy;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2694753
    iget-object v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->t:LX/JRz;

    if-nez p1, :cond_0

    const-string v1, ""

    :goto_0
    iget-object v2, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->o:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->p:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->q:LX/162;

    iget-object v5, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->f:Ljava/lang/String;

    move-object v6, p2

    .line 2694754
    iget-object p0, v0, LX/JRz;->a:LX/0Zb;

    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "music_preview_exception"

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p2, "song_clip_url"

    invoke-virtual {p1, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "og_song_id"

    invoke-virtual {p1, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "og_object_id"

    invoke-virtual {p1, p2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "tracking_codes"

    invoke-virtual {p1, p2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "provider_name"

    invoke-virtual {p1, p2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "stack_trace"

    invoke-virtual {p1, p2, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2694755
    return-void

    .line 2694756
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x107c53e9

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2694757
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    if-ne p1, v1, :cond_0

    .line 2694758
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->s:LX/JTe;

    iget-object v2, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->h:Landroid/net/Uri;

    invoke-virtual {v1, v2, p0}, LX/JTe;->b(Landroid/net/Uri;LX/JS8;)V

    .line 2694759
    :goto_0
    const v1, -0x1056c198

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2694760
    :cond_0
    iget-object v3, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->i:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 2694761
    :goto_1
    goto :goto_0

    .line 2694762
    :cond_1
    iget-object v3, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->t:LX/JRz;

    iget-object v4, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->o:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->p:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->q:LX/162;

    iget-object v7, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->f:Ljava/lang/String;

    .line 2694763
    iget-object v8, v3, LX/JRz;->a:LX/0Zb;

    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v10, "music_preview_action_sheet"

    invoke-direct {v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v10, "og_song_id"

    invoke-virtual {v9, v10, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "og_object_id"

    invoke-virtual {v9, v10, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "tracking_codes"

    invoke-virtual {v9, v10, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "provider_name"

    invoke-virtual {v9, v10, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    invoke-interface {v8, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2694764
    iget-object v3, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->w:LX/JS5;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->i:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->j:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->f:Ljava/lang/String;

    iget-object v8, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->o:Ljava/lang/String;

    iget-object v9, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->p:Ljava/lang/String;

    iget-object v10, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->q:LX/162;

    invoke-virtual/range {v3 .. v10}, LX/JS5;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;)V

    goto :goto_1
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const v1, 0x3e99999a    # 0.3f

    .line 2694765
    iget-boolean v0, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->z:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 2694766
    :goto_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 2694767
    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 2694768
    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 2694769
    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getPaddingTop()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 2694770
    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 2694771
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2694772
    iget-object v1, p0, Lcom/facebook/feedplugins/musicpreview/MusicPreviewView;->g:Lcom/facebook/feedplugins/musicpreview/MusicButton;

    invoke-virtual {v1}, Lcom/facebook/feedplugins/musicpreview/MusicButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2694773
    invoke-super {p0, p1, v2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2694774
    return-void

    .line 2694775
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method
