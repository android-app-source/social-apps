.class public Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/JRB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2692488
    const-class v0, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/JRB;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/JRB;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2692490
    iput-object p1, p0, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;->b:LX/0Or;

    .line 2692491
    iput-object p2, p0, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;->c:LX/JRB;

    .line 2692492
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;
    .locals 5

    .prologue
    .line 2692493
    const-class v1, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;

    monitor-enter v1

    .line 2692494
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692495
    sput-object v2, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692496
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692497
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692498
    new-instance v4, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;

    const/16 v3, 0x509

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/JRB;->a(LX/0QB;)LX/JRB;

    move-result-object v3

    check-cast v3, LX/JRB;

    invoke-direct {v4, p0, v3}, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;-><init>(LX/0Or;LX/JRB;)V

    .line 2692499
    move-object v0, v4

    .line 2692500
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692501
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/messenger/ActiveNowHScrollItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692502
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692503
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
