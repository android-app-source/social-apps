.class public Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JR0;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/1V0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/JR0;",
            ">;",
            "LX/1V0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692706
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2692707
    iput-object p2, p0, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;->d:LX/0Ot;

    .line 2692708
    iput-object p3, p0, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;->e:LX/1V0;

    .line 2692709
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2692690
    iget-object v1, p0, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;->e:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v0, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v2, p2, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    iget-object v0, p0, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JR0;

    const/4 v3, 0x0

    .line 2692691
    new-instance v4, LX/JQz;

    invoke-direct {v4, v0}, LX/JQz;-><init>(LX/JR0;)V

    .line 2692692
    iget-object p0, v0, LX/JR0;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JQy;

    .line 2692693
    if-nez p0, :cond_0

    .line 2692694
    new-instance p0, LX/JQy;

    invoke-direct {p0, v0}, LX/JQy;-><init>(LX/JR0;)V

    .line 2692695
    :cond_0
    invoke-static {p0, p1, v3, v3, v4}, LX/JQy;->a$redex0(LX/JQy;LX/1De;IILX/JQz;)V

    .line 2692696
    move-object v4, p0

    .line 2692697
    move-object v3, v4

    .line 2692698
    move-object v0, v3

    .line 2692699
    iget-object v3, v0, LX/JQy;->a:LX/JQz;

    iput-object p3, v3, LX/JQz;->b:LX/1Pm;

    .line 2692700
    iget-object v3, v0, LX/JQy;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2692701
    move-object v0, v0

    .line 2692702
    iget-object v3, v0, LX/JQy;->a:LX/JQz;

    iput-object p2, v3, LX/JQz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2692703
    iget-object v3, v0, LX/JQy;->e:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2692704
    move-object v0, v0

    .line 2692705
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;
    .locals 6

    .prologue
    .line 2692679
    const-class v1, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;

    monitor-enter v1

    .line 2692680
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692681
    sput-object v2, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692682
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692683
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692684
    new-instance v5, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x2019

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-direct {v5, v3, p0, v4}, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/1V0;)V

    .line 2692685
    move-object v0, v5

    .line 2692686
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692687
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692688
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692689
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2692678
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2692677
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/messenger/MessengerActiveNowComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2692676
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2692673
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2692674
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2692675
    check-cast v0, LX/0jW;

    return-object v0
.end method
