.class public Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/JRM;

.field public final c:LX/8yV;

.field public final d:LX/JRI;

.field public final e:LX/JRC;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2693032
    const-class v0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/JRM;LX/8yV;LX/JRI;LX/JRC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2693034
    iput-object p1, p0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->b:LX/JRM;

    .line 2693035
    iput-object p2, p0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->c:LX/8yV;

    .line 2693036
    iput-object p3, p0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->d:LX/JRI;

    .line 2693037
    iput-object p4, p0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->e:LX/JRC;

    .line 2693038
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;
    .locals 7

    .prologue
    .line 2693039
    const-class v1, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;

    monitor-enter v1

    .line 2693040
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2693041
    sput-object v2, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2693042
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2693043
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2693044
    new-instance p0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;

    invoke-static {v0}, LX/JRM;->a(LX/0QB;)LX/JRM;

    move-result-object v3

    check-cast v3, LX/JRM;

    invoke-static {v0}, LX/8yV;->a(LX/0QB;)LX/8yV;

    move-result-object v4

    check-cast v4, LX/8yV;

    invoke-static {v0}, LX/JRI;->a(LX/0QB;)LX/JRI;

    move-result-object v5

    check-cast v5, LX/JRI;

    invoke-static {v0}, LX/JRC;->a(LX/0QB;)LX/JRC;

    move-result-object v6

    check-cast v6, LX/JRC;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;-><init>(LX/JRM;LX/8yV;LX/JRI;LX/JRC;)V

    .line 2693045
    move-object v0, p0

    .line 2693046
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2693047
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2693048
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2693049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
