.class public Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JRF;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/1V0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/JRF;",
            ">;",
            "LX/1V0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693028
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2693029
    iput-object p2, p0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;->d:LX/0Ot;

    .line 2693030
    iput-object p3, p0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;->e:LX/1V0;

    .line 2693031
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMessengerGenericFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2693011
    iget-object v1, p0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;->e:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v0, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v2, p2, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    iget-object v0, p0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JRF;

    const/4 v3, 0x0

    .line 2693012
    new-instance v4, LX/JRE;

    invoke-direct {v4, v0}, LX/JRE;-><init>(LX/JRF;)V

    .line 2693013
    iget-object p0, v0, LX/JRF;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JRD;

    .line 2693014
    if-nez p0, :cond_0

    .line 2693015
    new-instance p0, LX/JRD;

    invoke-direct {p0, v0}, LX/JRD;-><init>(LX/JRF;)V

    .line 2693016
    :cond_0
    invoke-static {p0, p1, v3, v3, v4}, LX/JRD;->a$redex0(LX/JRD;LX/1De;IILX/JRE;)V

    .line 2693017
    move-object v4, p0

    .line 2693018
    move-object v3, v4

    .line 2693019
    move-object v0, v3

    .line 2693020
    iget-object v3, v0, LX/JRD;->a:LX/JRE;

    iput-object p2, v3, LX/JRE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2693021
    iget-object v3, v0, LX/JRD;->e:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2693022
    move-object v3, v0

    .line 2693023
    move-object v0, p3

    check-cast v0, LX/1Pk;

    .line 2693024
    iget-object v4, v3, LX/JRD;->a:LX/JRE;

    iput-object v0, v4, LX/JRE;->b:LX/1Pk;

    .line 2693025
    iget-object v4, v3, LX/JRD;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, Ljava/util/BitSet;->set(I)V

    .line 2693026
    move-object v0, v3

    .line 2693027
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;
    .locals 6

    .prologue
    .line 2693000
    const-class v1, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;

    monitor-enter v1

    .line 2693001
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2693002
    sput-object v2, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2693003
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2693004
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2693005
    new-instance v5, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x2021

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-direct {v5, v3, p0, v4}, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/1V0;)V

    .line 2693006
    move-object v0, v5

    .line 2693007
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2693008
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2693009
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2693010
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2692994
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2692999
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/messenger/MessengerGenericPromotionComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2692998
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2692995
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2692996
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2692997
    check-cast v0, LX/0jW;

    return-object v0
.end method
