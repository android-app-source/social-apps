.class public Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionHeaderComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionHeaderComponentPartDefinition;Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685131
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2685132
    iput-object p1, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;->a:Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionHeaderComponentPartDefinition;

    .line 2685133
    iput-object p2, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;->b:Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;

    .line 2685134
    iput-object p3, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;->c:Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;

    .line 2685135
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;
    .locals 6

    .prologue
    .line 2685136
    const-class v1, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;

    monitor-enter v1

    .line 2685137
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685138
    sput-object v2, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685139
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685140
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2685141
    new-instance p0, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;-><init>(Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionHeaderComponentPartDefinition;Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;)V

    .line 2685142
    move-object v0, p0

    .line 2685143
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685144
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685145
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685146
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2685147
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2685148
    iget-object v0, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;->a:Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2685149
    iget-object v0, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;->b:Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2685150
    iget-object v0, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/EventsSuggestionPartDefinition;->c:Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2685151
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2685152
    const/4 v0, 0x1

    return v0
.end method
