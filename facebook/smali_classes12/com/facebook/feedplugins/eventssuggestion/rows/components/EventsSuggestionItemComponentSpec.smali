.class public Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/1DR;

.field public final d:LX/1nu;

.field public final e:LX/JNR;

.field public final f:LX/JNO;

.field public final g:LX/JNG;

.field public final h:LX/Blh;

.field public final i:LX/0kx;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2685742
    const-class v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;

    const-string v1, "events_suggestion"

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1DR;LX/1nu;LX/JNR;LX/JNO;LX/JNG;LX/Blh;LX/0kx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2685744
    iput-object p1, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->b:Landroid/content/Context;

    .line 2685745
    iput-object p2, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->c:LX/1DR;

    .line 2685746
    iput-object p3, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->d:LX/1nu;

    .line 2685747
    iput-object p4, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->e:LX/JNR;

    .line 2685748
    iput-object p5, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->f:LX/JNO;

    .line 2685749
    iput-object p6, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->g:LX/JNG;

    .line 2685750
    iput-object p7, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->h:LX/Blh;

    .line 2685751
    iput-object p8, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->i:LX/0kx;

    .line 2685752
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;
    .locals 12

    .prologue
    .line 2685753
    const-class v1, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;

    monitor-enter v1

    .line 2685754
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685755
    sput-object v2, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685756
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685757
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2685758
    new-instance v3, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v5

    check-cast v5, LX/1DR;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v6

    check-cast v6, LX/1nu;

    invoke-static {v0}, LX/JNR;->a(LX/0QB;)LX/JNR;

    move-result-object v7

    check-cast v7, LX/JNR;

    invoke-static {v0}, LX/JNO;->a(LX/0QB;)LX/JNO;

    move-result-object v8

    check-cast v8, LX/JNO;

    invoke-static {v0}, LX/JNG;->a(LX/0QB;)LX/JNG;

    move-result-object v9

    check-cast v9, LX/JNG;

    invoke-static {v0}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v10

    check-cast v10, LX/Blh;

    invoke-static {v0}, LX/0kx;->a(LX/0QB;)LX/0kx;

    move-result-object v11

    check-cast v11, LX/0kx;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;-><init>(Landroid/content/Context;LX/1DR;LX/1nu;LX/JNR;LX/JNO;LX/JNG;LX/Blh;LX/0kx;)V

    .line 2685759
    move-object v0, v3

    .line 2685760
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685761
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685762
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685763
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
