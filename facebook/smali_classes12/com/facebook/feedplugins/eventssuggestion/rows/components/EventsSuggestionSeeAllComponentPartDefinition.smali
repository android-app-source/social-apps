.class public Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/JNV;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JNV;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686013
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2686014
    iput-object p2, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;->d:LX/JNV;

    .line 2686015
    iput-object p3, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;->e:LX/1V0;

    .line 2686016
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2686017
    new-instance v0, LX/1X6;

    sget-object v1, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v0, p2, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2686018
    iget-object v1, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;->d:LX/JNV;

    const/4 v2, 0x0

    .line 2686019
    new-instance v3, LX/JNU;

    invoke-direct {v3, v1}, LX/JNU;-><init>(LX/JNV;)V

    .line 2686020
    iget-object v4, v1, LX/JNV;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JNT;

    .line 2686021
    if-nez v4, :cond_0

    .line 2686022
    new-instance v4, LX/JNT;

    invoke-direct {v4, v1}, LX/JNT;-><init>(LX/JNV;)V

    .line 2686023
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/JNT;->a$redex0(LX/JNT;LX/1De;IILX/JNU;)V

    .line 2686024
    move-object v3, v4

    .line 2686025
    move-object v2, v3

    .line 2686026
    move-object v1, v2

    .line 2686027
    iget-object v2, v1, LX/JNT;->a:LX/JNU;

    iput-object p2, v2, LX/JNU;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2686028
    iget-object v2, v1, LX/JNT;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2686029
    move-object v1, v1

    .line 2686030
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2686031
    iget-object v2, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;
    .locals 6

    .prologue
    .line 2686032
    const-class v1, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;

    monitor-enter v1

    .line 2686033
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686034
    sput-object v2, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686035
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686036
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686037
    new-instance p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JNV;->a(LX/0QB;)LX/JNV;

    move-result-object v4

    check-cast v4, LX/JNV;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;-><init>(Landroid/content/Context;LX/JNV;LX/1V0;)V

    .line 2686038
    move-object v0, p0

    .line 2686039
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686040
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686041
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686042
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2686043
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2686044
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionSeeAllComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2686045
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2686046
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2686047
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
