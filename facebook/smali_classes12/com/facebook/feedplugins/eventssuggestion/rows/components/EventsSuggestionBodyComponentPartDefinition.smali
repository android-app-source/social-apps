.class public Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/JN6;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JN6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685231
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2685232
    iput-object p2, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;->d:LX/JN6;

    .line 2685233
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2685234
    iget-object v0, p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;->d:LX/JN6;

    const/4 v1, 0x0

    .line 2685235
    new-instance v2, LX/JN5;

    invoke-direct {v2, v0}, LX/JN5;-><init>(LX/JN6;)V

    .line 2685236
    iget-object p0, v0, LX/JN6;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JN4;

    .line 2685237
    if-nez p0, :cond_0

    .line 2685238
    new-instance p0, LX/JN4;

    invoke-direct {p0, v0}, LX/JN4;-><init>(LX/JN6;)V

    .line 2685239
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/JN4;->a$redex0(LX/JN4;LX/1De;IILX/JN5;)V

    .line 2685240
    move-object v2, p0

    .line 2685241
    move-object v1, v2

    .line 2685242
    move-object v0, v1

    .line 2685243
    iget-object v1, v0, LX/JN4;->a:LX/JN5;

    iput-object p3, v1, LX/JN5;->a:LX/1Pn;

    .line 2685244
    iget-object v1, v0, LX/JN4;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2685245
    move-object v0, v0

    .line 2685246
    iget-object v1, v0, LX/JN4;->a:LX/JN5;

    iput-object p2, v1, LX/JN5;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2685247
    iget-object v1, v0, LX/JN4;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2685248
    move-object v0, v0

    .line 2685249
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;
    .locals 5

    .prologue
    .line 2685250
    const-class v1, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;

    monitor-enter v1

    .line 2685251
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685252
    sput-object v2, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685253
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685254
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2685255
    new-instance p0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JN6;->a(LX/0QB;)LX/JN6;

    move-result-object v4

    check-cast v4, LX/JN6;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;-><init>(Landroid/content/Context;LX/JN6;)V

    .line 2685256
    move-object v0, p0

    .line 2685257
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685258
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685259
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685260
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2685261
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2685262
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionBodyComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2685263
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2685264
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2685265
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
