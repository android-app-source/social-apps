.class public Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1PV;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2684488
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2684489
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;->a:LX/0Ot;

    .line 2684490
    move-object v0, p2

    .line 2684491
    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;->b:LX/0Ot;

    .line 2684492
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;
    .locals 5

    .prologue
    .line 2684493
    const-class v1, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;

    monitor-enter v1

    .line 2684494
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2684495
    sput-object v2, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2684496
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2684497
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2684498
    new-instance v3, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;

    const/16 v4, 0x7e3

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x7f0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2684499
    move-object v0, v3

    .line 2684500
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2684501
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2684502
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2684503
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2684504
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2684505
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;->b:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;->a:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 2684506
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2684507
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2684508
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/attachments/animated/TranscodedAnimatedImageShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/animated/AnimatedAttachmentSelector;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, Lcom/facebook/feedplugins/attachments/ImageShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
