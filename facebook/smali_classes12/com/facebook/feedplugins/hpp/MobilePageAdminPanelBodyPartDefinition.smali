.class public Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field public final b:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:LX/1DR;

.field private final e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "LX/JPv;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final f:LX/1LV;

.field private final g:LX/2dq;

.field private final h:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2688862
    sget-object v0, LX/2eF;->a:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1LV;LX/1DR;LX/0ad;LX/2dq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2688893
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2688894
    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->b:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    .line 2688895
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2688896
    iput-object p3, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2688897
    iput-object p5, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->d:LX/1DR;

    .line 2688898
    iput-object p4, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->f:LX/1LV;

    .line 2688899
    iput-object p6, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->h:LX/0ad;

    .line 2688900
    iput-object p7, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->g:LX/2dq;

    .line 2688901
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;
    .locals 11

    .prologue
    .line 2688882
    const-class v1, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;

    monitor-enter v1

    .line 2688883
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688884
    sput-object v2, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688885
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688886
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688887
    new-instance v3, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v7

    check-cast v7, LX/1LV;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v8

    check-cast v8, LX/1DR;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v10

    check-cast v10, LX/2dq;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;-><init>(Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1LV;LX/1DR;LX/0ad;LX/2dq;)V

    .line 2688888
    move-object v0, v3

    .line 2688889
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688890
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688891
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688892
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2688881
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2688866
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 2688867
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 2688868
    check-cast v5, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    .line 2688869
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2688870
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->h:LX/0ad;

    sget-short v1, LX/JPl;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2688871
    iget-object v6, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    if-eqz v1, :cond_0

    .line 2688872
    iget-object v1, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->d:LX/1DR;

    invoke-virtual {v1}, LX/1DR;->a()I

    move-result v2

    move-object v1, p3

    .line 2688873
    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    int-to-float v2, v2

    invoke-static {v1, v2}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    .line 2688874
    iget-object v2, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->d:LX/1DR;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1DR;->a(Landroid/content/Context;)I

    move-result v2

    int-to-float v2, v2

    invoke-static {v2, v1}, LX/2eF;->a(FI)LX/2eF;

    move-result-object v1

    move-object v1, v1

    .line 2688875
    :goto_0
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->I_()I

    move-result v2

    .line 2688876
    invoke-static {v5}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;)LX/0Px;

    move-result-object v3

    .line 2688877
    new-instance v4, LX/JP7;

    invoke-direct {v4, p0, v5, v3}, LX/JP7;-><init>(Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;LX/0Px;)V

    move-object v3, v4

    .line 2688878
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2688879
    const/4 v0, 0x0

    return-object v0

    .line 2688880
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->g:LX/2dq;

    sget-object v2, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->a:LX/1Ua;

    invoke-virtual {v1, v2}, LX/2dq;->a(LX/1Ua;)LX/2eF;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2688863
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2688864
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2688865
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
