.class public Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2689392
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2689393
    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;->a:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;

    .line 2689394
    iput-object p3, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;->b:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;

    .line 2689395
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;->c:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;

    .line 2689396
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;
    .locals 6

    .prologue
    .line 2689397
    const-class v1, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;

    monitor-enter v1

    .line 2689398
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2689399
    sput-object v2, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2689400
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2689401
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2689402
    new-instance p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;-><init>(Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;)V

    .line 2689403
    move-object v0, p0

    .line 2689404
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2689405
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2689406
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2689407
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2689408
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2689409
    new-instance v0, LX/2dx;

    invoke-direct {v0}, LX/2dx;-><init>()V

    .line 2689410
    new-instance v3, LX/JPJ;

    invoke-direct {v3, p2, v0}, LX/JPJ;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2dx;)V

    .line 2689411
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2689412
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    .line 2689413
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->k()LX/0Px;

    move-result-object v4

    .line 2689414
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689415
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 2689416
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    .line 2689417
    const-string v2, "MobilePageAdminPanelFeedUnitItem"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2689418
    :goto_1
    if-eqz v0, :cond_2

    .line 2689419
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;->c:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;

    invoke-virtual {p1, v0, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2689420
    :goto_2
    const/4 v0, 0x0

    return-object v0

    :cond_0
    move v0, v1

    .line 2689421
    goto :goto_1

    .line 2689422
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2689423
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;->a:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2689424
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelPartDefinition;->b:Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelBodyPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2689425
    const/4 v0, 0x1

    return v0
.end method
