.class public Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;

.field private static m:LX/0Xm;


# instance fields
.field public final a:LX/1nu;

.field public final b:LX/1vg;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/0tX;

.field public final e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final f:LX/17Y;

.field public final g:LX/1Kf;

.field public final h:LX/CSL;

.field public final i:LX/0SI;

.field public final j:LX/1nA;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2687872
    const-class v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1vg;Lcom/facebook/content/SecureContextHelper;LX/0tX;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/17Y;LX/1Kf;LX/CSL;LX/0SI;LX/1nA;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "LX/1vg;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0tX;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/17Y;",
            "LX/1Kf;",
            "Lcom/facebook/pages/common/intent_builder/IPageIdentityIntentBuilder;",
            "LX/0SI;",
            "LX/1nA;",
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2687859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2687860
    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->a:LX/1nu;

    .line 2687861
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->b:LX/1vg;

    .line 2687862
    iput-object p3, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2687863
    iput-object p4, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->d:LX/0tX;

    .line 2687864
    iput-object p5, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2687865
    iput-object p6, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->f:LX/17Y;

    .line 2687866
    iput-object p7, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->g:LX/1Kf;

    .line 2687867
    iput-object p8, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->h:LX/CSL;

    .line 2687868
    iput-object p9, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->i:LX/0SI;

    .line 2687869
    iput-object p10, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->j:LX/1nA;

    .line 2687870
    iput-object p11, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->k:LX/0Ot;

    .line 2687871
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;LX/1De;Ljava/lang/String;)LX/1Dh;
    .locals 10

    .prologue
    .line 2687884
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b257f

    invoke-interface {v0, v1}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b258c

    invoke-interface {v0, v1}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v0

    const/4 v5, 0x1

    .line 2687885
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 2687886
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b258f

    invoke-interface {v2, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b258f

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f020cd7

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    const v4, 0x7f02084d

    const v6, 0x7f0a00d5

    .line 2687887
    iget-object v9, p0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->b:LX/1vg;

    invoke-virtual {v9, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v9

    invoke-virtual {v9, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v9

    invoke-virtual {v9, v6}, LX/2xv;->j(I)LX/2xv;

    move-result-object v9

    invoke-virtual {v9}, LX/1n6;->b()LX/1dc;

    move-result-object v9

    move-object v4, v9

    .line 2687888
    invoke-virtual {v3, v4}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b258e

    invoke-interface {v3, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b258e

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v8, v7}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b2578

    invoke-interface {v2, v8, v3}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    move-object v2, v2

    .line 2687889
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f083a82

    invoke-virtual {v2, v3}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a010c

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b2582

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x0

    const v4, 0x7f0b2571

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b2576

    invoke-interface {v1, v5, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    move-object v1, v1

    .line 2687890
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a010c

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b2582

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b2578

    invoke-interface {v1, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x7

    const v3, 0x7f0b2578

    invoke-interface {v1, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;
    .locals 15

    .prologue
    .line 2687873
    const-class v1, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;

    monitor-enter v1

    .line 2687874
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687875
    sput-object v2, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687876
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687877
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687878
    new-instance v3, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v8

    check-cast v8, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v9

    check-cast v9, LX/17Y;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v10

    check-cast v10, LX/1Kf;

    invoke-static {v0}, LX/HSx;->b(LX/0QB;)LX/HSx;

    move-result-object v11

    check-cast v11, LX/CSL;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v12

    check-cast v12, LX/0SI;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v13

    check-cast v13, LX/1nA;

    const/16 v14, 0x1fdc

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;-><init>(LX/1nu;LX/1vg;Lcom/facebook/content/SecureContextHelper;LX/0tX;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/17Y;LX/1Kf;LX/CSL;LX/0SI;LX/1nA;LX/0Ot;)V

    .line 2687879
    move-object v0, v3

    .line 2687880
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687881
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687882
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687883
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
