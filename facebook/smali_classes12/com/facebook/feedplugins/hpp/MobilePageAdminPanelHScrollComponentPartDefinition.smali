.class public Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/JPJ;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/JPE;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JPE;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2689241
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2689242
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;->d:LX/JPE;

    .line 2689243
    iput-object p3, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;->e:LX/1V0;

    .line 2689244
    return-void
.end method

.method private a(LX/1De;LX/JPJ;LX/1Pc;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/JPJ;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2689223
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;->d:LX/JPE;

    const/4 v1, 0x0

    .line 2689224
    new-instance v2, LX/JPD;

    invoke-direct {v2, v0}, LX/JPD;-><init>(LX/JPE;)V

    .line 2689225
    iget-object v3, v0, LX/JPE;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JPC;

    .line 2689226
    if-nez v3, :cond_0

    .line 2689227
    new-instance v3, LX/JPC;

    invoke-direct {v3, v0}, LX/JPC;-><init>(LX/JPE;)V

    .line 2689228
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JPC;->a$redex0(LX/JPC;LX/1De;IILX/JPD;)V

    .line 2689229
    move-object v2, v3

    .line 2689230
    move-object v1, v2

    .line 2689231
    move-object v0, v1

    .line 2689232
    iget-object v1, v0, LX/JPC;->a:LX/JPD;

    iput-object p3, v1, LX/JPD;->a:LX/1Pc;

    .line 2689233
    iget-object v1, v0, LX/JPC;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2689234
    move-object v0, v0

    .line 2689235
    iget-object v1, v0, LX/JPC;->a:LX/JPD;

    iput-object p2, v1, LX/JPD;->b:LX/JPJ;

    .line 2689236
    iget-object v1, v0, LX/JPC;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2689237
    move-object v0, v0

    .line 2689238
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2689239
    new-instance v1, LX/1X6;

    iget-object v2, p2, LX/JPJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v3, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2689240
    iget-object v2, p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;
    .locals 6

    .prologue
    .line 2689212
    const-class v1, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;

    monitor-enter v1

    .line 2689213
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2689214
    sput-object v2, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2689215
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2689216
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2689217
    new-instance p0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JPE;->a(LX/0QB;)LX/JPE;

    move-result-object v4

    check-cast v4, LX/JPE;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;-><init>(Landroid/content/Context;LX/JPE;LX/1V0;)V

    .line 2689218
    move-object v0, p0

    .line 2689219
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2689220
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2689221
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2689222
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2689245
    check-cast p2, LX/JPJ;

    check-cast p3, LX/1Pc;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;->a(LX/1De;LX/JPJ;LX/1Pc;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2689211
    check-cast p2, LX/JPJ;

    check-cast p3, LX/1Pc;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/hpp/MobilePageAdminPanelHScrollComponentPartDefinition;->a(LX/1De;LX/JPJ;LX/1Pc;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2689210
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2689208
    check-cast p1, LX/JPJ;

    .line 2689209
    invoke-virtual {p1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
