.class public Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1nA;

.field public final b:LX/1nu;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2690167
    const-class v0, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nA;LX/1nu;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2690168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2690169
    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;->a:LX/1nA;

    .line 2690170
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;->b:LX/1nu;

    .line 2690171
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;
    .locals 5

    .prologue
    .line 2690172
    const-class v1, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;

    monitor-enter v1

    .line 2690173
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2690174
    sput-object v2, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2690175
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2690176
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2690177
    new-instance p0, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v3

    check-cast v3, LX/1nA;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;-><init>(LX/1nA;LX/1nu;)V

    .line 2690178
    move-object v0, p0

    .line 2690179
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2690180
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2690181
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2690182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
