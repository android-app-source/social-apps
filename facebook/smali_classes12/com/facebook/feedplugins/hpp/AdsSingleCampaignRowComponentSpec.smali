.class public Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/1nu;

.field public final c:LX/1vg;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0W9;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2688354
    const-class v0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1vg;LX/0Ot;LX/0W9;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "LX/1vg;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0W9;",
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2688329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2688330
    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->b:LX/1nu;

    .line 2688331
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->c:LX/1vg;

    .line 2688332
    iput-object p3, p0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->d:LX/0Ot;

    .line 2688333
    iput-object p4, p0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->e:LX/0W9;

    .line 2688334
    iput-object p5, p0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->f:LX/0Ot;

    .line 2688335
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;
    .locals 9

    .prologue
    .line 2688343
    const-class v1, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;

    monitor-enter v1

    .line 2688344
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688345
    sput-object v2, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688346
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688347
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688348
    new-instance v3, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    const/16 v6, 0xbc6

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v7

    check-cast v7, LX/0W9;

    const/16 v8, 0x1fdc

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;-><init>(LX/1nu;LX/1vg;LX/0Ot;LX/0W9;LX/0Ot;)V

    .line 2688349
    move-object v0, v3

    .line 2688350
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688351
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688352
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688353
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;)LX/1Dg;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 2688336
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b257f

    invoke-interface {v0, v1}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b259f

    invoke-interface {v0, v1}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->b:LX/1nu;

    sget-object v2, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {p1, v1, v2, v3}, LX/JPK;->a(LX/1De;LX/1nu;Lcom/facebook/common/callercontext/CallerContext;Landroid/net/Uri;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->a()Ljava/lang/String;

    move-result-object v2

    .line 2688337
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->e:LX/0W9;

    invoke-virtual {v9}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v9

    invoke-static {v9}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v9

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->n()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v9, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v3, v8

    .line 2688338
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->l()Z

    move-result v4

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->s()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    const/4 p2, 0x1

    .line 2688339
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    const v9, 0x7f0b25a0

    invoke-interface {v8, v9}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v8

    const/4 v9, 0x0

    const v10, 0x7f0b2581

    invoke-interface {v8, v9, v10}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v9

    const v10, 0x7f0a010c

    invoke-virtual {v9, v10}, LX/1ne;->n(I)LX/1ne;

    move-result-object v9

    const v10, 0x7f0b2589

    invoke-virtual {v9, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v9

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v9, v10}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v9

    const v10, 0x7f0a010e

    invoke-virtual {v9, v10}, LX/1ne;->n(I)LX/1ne;

    move-result-object v9

    const v10, 0x7f0b2589

    invoke-virtual {v9, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v9

    invoke-virtual {v9, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v9

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v9, v10}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const/4 v10, 0x2

    invoke-interface {v9, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    iget-object v10, v0, Lcom/facebook/feedplugins/hpp/AdsSingleCampaignRowComponentSpec;->c:LX/1vg;

    invoke-static {v1, v10, v5, v4}, LX/JPK;->a(LX/1De;LX/1vg;Ljava/lang/String;Z)LX/1Dh;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    const-string p0, " \u00b7 "

    invoke-direct {v11, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v10

    const v11, 0x7f0a010e

    invoke-virtual {v10, v11}, LX/1ne;->n(I)LX/1ne;

    move-result-object v10

    const v11, 0x7f0b2589

    invoke-virtual {v10, v11}, LX/1ne;->q(I)LX/1ne;

    move-result-object v10

    invoke-virtual {v10, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v10

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v10, v11}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v9

    invoke-interface {v8, v9}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    move-object v0, v8

    .line 2688340
    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 2688341
    const v1, -0x37e8c4b5

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 2688342
    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x7

    const v2, 0x7f0b25a4

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x6

    const v2, 0x7f0b25a1

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
