.class public Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1nA;

.field public final b:LX/1nu;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2690371
    const-class v0, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nA;LX/1nu;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nA;",
            "LX/1nu;",
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2690372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2690373
    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->a:LX/1nA;

    .line 2690374
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->b:LX/1nu;

    .line 2690375
    iput-object p3, p0, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->c:LX/0Ot;

    .line 2690376
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;
    .locals 6

    .prologue
    .line 2690377
    const-class v1, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;

    monitor-enter v1

    .line 2690378
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2690379
    sput-object v2, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2690380
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2690381
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2690382
    new-instance v5, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v3

    check-cast v3, LX/1nA;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    const/16 p0, 0x1fdc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;-><init>(LX/1nA;LX/1nu;LX/0Ot;)V

    .line 2690383
    move-object v0, v5

    .line 2690384
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2690385
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2690386
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2690387
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
