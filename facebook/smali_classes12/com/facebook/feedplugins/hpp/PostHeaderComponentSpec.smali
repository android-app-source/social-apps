.class public Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/1vg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2689957
    const-class v0, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1vg;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2689942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2689943
    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;->b:LX/1nu;

    .line 2689944
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;->c:LX/1vg;

    .line 2689945
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;
    .locals 5

    .prologue
    .line 2689946
    const-class v1, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;

    monitor-enter v1

    .line 2689947
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2689948
    sput-object v2, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2689949
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2689950
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2689951
    new-instance p0, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v4

    check-cast v4, LX/1vg;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;-><init>(LX/1nu;LX/1vg;)V

    .line 2689952
    move-object v0, p0

    .line 2689953
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2689954
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/PostHeaderComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2689955
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2689956
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
