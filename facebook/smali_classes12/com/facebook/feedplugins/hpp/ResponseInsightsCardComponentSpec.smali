.class public Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/1nu;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/17X;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2690057
    const-class v0, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;Lcom/facebook/content/SecureContextHelper;LX/17X;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17X;",
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2690058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2690059
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2690060
    iput-object p3, p0, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->c:LX/17X;

    .line 2690061
    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->a:LX/1nu;

    .line 2690062
    iput-object p4, p0, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->d:LX/0Ot;

    .line 2690063
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;
    .locals 7

    .prologue
    .line 2690064
    const-class v1, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;

    monitor-enter v1

    .line 2690065
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2690066
    sput-object v2, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2690067
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2690068
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2690069
    new-instance v6, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17X;

    const/16 p0, 0x1fdc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;-><init>(LX/1nu;Lcom/facebook/content/SecureContextHelper;LX/17X;LX/0Ot;)V

    .line 2690070
    move-object v0, v6

    .line 2690071
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2690072
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/ResponseInsightsCardComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2690073
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2690074
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
