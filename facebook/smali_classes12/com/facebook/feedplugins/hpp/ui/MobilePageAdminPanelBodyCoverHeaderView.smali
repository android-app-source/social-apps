.class public Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Lcom/facebook/fbui/widget/contentview/ContentView;

.field private e:Lcom/facebook/fbui/widget/text/BadgeTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2690402
    const-class v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;

    const-string v1, "pages_public_view"

    const-string v2, "hpp_cover_header"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2690420
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2690421
    invoke-direct {p0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->a()V

    .line 2690422
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2690417
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2690418
    invoke-direct {p0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->a()V

    .line 2690419
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2690414
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2690415
    invoke-direct {p0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->a()V

    .line 2690416
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2690423
    const v0, 0x7f0308a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2690424
    invoke-virtual {p0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b2570

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 2690425
    new-instance v1, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-static {v0, v0, v3, v3}, LX/4Ab;->b(FFFF)LX/4Ab;

    move-result-object v0

    .line 2690426
    iput-object v0, v1, LX/1Uo;->u:LX/4Ab;

    .line 2690427
    move-object v0, v1

    .line 2690428
    invoke-virtual {p0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020cd3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    .line 2690429
    const v0, 0x7f0d165e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2690430
    const v0, 0x7f0d165d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2690431
    const v0, 0x7f0d165f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->d:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2690432
    const v0, 0x7f0d1660

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->e:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2690433
    const v0, 0x7f0d1661

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2690434
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2690435
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x402ccccd    # 2.7f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2690436
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->d:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f0e0c31

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2690437
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->d:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f0e0c32

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 2690438
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/graphics/PointF;)V
    .locals 2
    .param p2    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2690410
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2690411
    if-eqz p2, :cond_0

    .line 2690412
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p2}, LX/1af;->a(Landroid/graphics/PointF;)V

    .line 2690413
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2690407
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->e:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2690408
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2690409
    return-void
.end method

.method public setBadgeCount(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2690405
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->e:Lcom/facebook/fbui/widget/text/BadgeTextView;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2690406
    return-void
.end method

.method public setProfilePhoto(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2690403
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2690404
    return-void
.end method
