.class public Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2eZ;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Z

.field public c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;

.field public d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2690491
    const-class v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;

    const-string v1, "pages_public_view"

    const-string v2, "hpp_body"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2690486
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 2690487
    const p1, 0x7f0308a6

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2690488
    const p1, 0x7f0d1663

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;

    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;

    .line 2690489
    const p1, 0x7f0d1664

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2690490
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2690476
    const v0, 0x7f0d1665

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2690477
    if-eqz v0, :cond_0

    .line 2690478
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2690479
    :cond_0
    const v0, 0x7f0d165a

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2690480
    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2690481
    const v0, 0x7f0d165c

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2690482
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2690483
    const v0, 0x7f0d165b

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2690484
    invoke-virtual {v0, p3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2690485
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 12
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/44w",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;",
            "Landroid/view/View$OnClickListener;",
            ">;>;Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2690445
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->removeAllViews()V

    .line 2690446
    if-eqz p1, :cond_3

    move v2, v3

    .line 2690447
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2690448
    iget-object v5, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    iget-object v0, v0, LX/44w;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/44w;

    iget-object v1, v1, LX/44w;->b:Ljava/lang/Object;

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v2, v4, :cond_2

    if-eqz p2, :cond_2

    const/4 v4, 0x1

    .line 2690449
    :goto_1
    new-instance v6, LX/JPm;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, LX/JPm;-><init>(Landroid/content/Context;)V

    .line 2690450
    const/4 v11, 0x2

    .line 2690451
    if-nez v0, :cond_4

    .line 2690452
    :cond_0
    :goto_2
    if-eqz v4, :cond_1

    .line 2690453
    const/4 v7, 0x1

    .line 2690454
    if-eqz v7, :cond_5

    .line 2690455
    const v8, 0x7f020cd2

    iput v8, v6, LX/JPm;->b:I

    .line 2690456
    :goto_3
    iget v8, v6, LX/JPm;->b:I

    invoke-virtual {v6, v8}, LX/JPm;->setBackgroundResource(I)V

    .line 2690457
    :cond_1
    iget v7, v6, LX/JPm;->b:I

    invoke-virtual {v6, v7}, LX/JPm;->setBackgroundResource(I)V

    .line 2690458
    if-eqz v1, :cond_6

    const/4 v7, 0x1

    :goto_4
    invoke-virtual {v6, v7}, LX/JPm;->setClickable(Z)V

    .line 2690459
    invoke-virtual {v6, v1}, LX/JPm;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2690460
    move-object v0, v6

    .line 2690461
    invoke-virtual {v5, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 2690462
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v4, v3

    .line 2690463
    goto :goto_1

    .line 2690464
    :cond_3
    return-void

    .line 2690465
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 2690466
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0050

    invoke-static {v8, v9}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v8

    .line 2690467
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 2690468
    sget-object v9, LX/JPn;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->k()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 2690469
    invoke-static {v0, v7}, LX/BiH;->a(Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7, v11, v8}, LX/JPm;->a(Ljava/lang/CharSequence;II)V

    goto :goto_2

    .line 2690470
    :pswitch_0
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a00e7

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-static {v0, v7}, LX/BiH;->a(Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 2690471
    iget-object v10, v6, LX/JPm;->a:Landroid/widget/TextView;

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2690472
    invoke-virtual {v6, v7, v11, v8}, LX/JPm;->a(Ljava/lang/CharSequence;II)V

    .line 2690473
    goto :goto_2

    .line 2690474
    :cond_5
    const v8, 0x7f0214bf

    iput v8, v6, LX/JPm;->b:I

    goto :goto_3

    .line 2690475
    :cond_6
    const/4 v7, 0x0

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2690440
    iget-boolean v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->b:Z

    return v0
.end method

.method public setHasBeenAttached(Z)V
    .locals 0

    .prologue
    .line 2690443
    iput-boolean p1, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->b:Z

    .line 2690444
    return-void
.end method

.method public setHeaderOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2690441
    iget-object v0, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2690442
    return-void
.end method
