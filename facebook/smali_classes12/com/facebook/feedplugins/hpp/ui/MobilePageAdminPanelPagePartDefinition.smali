.class public Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/JPv;",
        "LX/JPw;",
        "LX/1PW;",
        "Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;",
            ">;"
        }
    .end annotation
.end field

.field private static m:LX/0Xm;


# instance fields
.field public final b:LX/1nA;

.field public final c:LX/HVa;

.field public final d:LX/0Zb;

.field public final e:LX/17Q;

.field public final f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1Kf;

.field public final i:LX/CSL;

.field public final j:LX/0SI;

.field public final k:Lcom/facebook/content/SecureContextHelper;

.field public final l:LX/17Y;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2690713
    new-instance v0, LX/JPp;

    invoke-direct {v0}, LX/JPp;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1nA;LX/HVa;LX/0Zb;LX/17Q;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Or;LX/1Kf;LX/CSL;LX/0SI;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nA;",
            "LX/HVa;",
            "LX/0Zb;",
            "LX/17Q;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1Kf;",
            "Lcom/facebook/pages/common/intent_builder/IPageIdentityIntentBuilder;",
            "LX/0SI;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2690700
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 2690701
    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->b:LX/1nA;

    .line 2690702
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->c:LX/HVa;

    .line 2690703
    iput-object p3, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->d:LX/0Zb;

    .line 2690704
    iput-object p4, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->e:LX/17Q;

    .line 2690705
    iput-object p5, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2690706
    iput-object p6, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->g:LX/0Or;

    .line 2690707
    iput-object p7, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->h:LX/1Kf;

    .line 2690708
    iput-object p8, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->i:LX/CSL;

    .line 2690709
    iput-object p9, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->j:LX/0SI;

    .line 2690710
    iput-object p10, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->k:Lcom/facebook/content/SecureContextHelper;

    .line 2690711
    iput-object p11, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->l:LX/17Y;

    .line 2690712
    return-void
.end method

.method private a(LX/JPv;)LX/JPw;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JPv;",
            ")",
            "LX/JPw;"
        }
    .end annotation

    .prologue
    .line 2690651
    move-object/from16 v0, p1

    iget-object v0, v0, LX/JPv;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    move-object/from16 v18, v0

    .line 2690652
    const-wide/16 v8, 0x0

    .line 2690653
    const/4 v12, 0x0

    .line 2690654
    const/4 v13, 0x0

    .line 2690655
    const/4 v10, 0x0

    .line 2690656
    const/4 v7, 0x0

    .line 2690657
    const/4 v11, 0x0

    .line 2690658
    const/16 v16, 0x0

    .line 2690659
    const/4 v6, 0x0

    .line 2690660
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->u()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->u()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->u()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2690661
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->u()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {v4}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v12

    .line 2690662
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->u()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v4

    .line 2690663
    if-eqz v4, :cond_0

    .line 2690664
    new-instance v16, Landroid/graphics/PointF;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v14

    double-to-float v5, v14

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v14

    double-to-float v4, v14

    move-object/from16 v0, v16

    invoke-direct {v0, v5, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2690665
    :cond_0
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2690666
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {v4}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v13

    .line 2690667
    :cond_1
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 2690668
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 2690669
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v10

    .line 2690670
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 2690671
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v14

    .line 2690672
    if-eqz v14, :cond_2

    invoke-virtual {v14}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2690673
    const/4 v4, 0x0

    invoke-virtual {v14, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2690674
    const/4 v4, 0x1

    move v5, v4

    :goto_0
    invoke-virtual {v14}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_2

    .line 2690675
    const-string v4, " \u00b7 "

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v14, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2690676
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2690677
    :cond_2
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 2690678
    :cond_3
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 2690679
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v5

    .line 2690680
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_5

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->j()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->l()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->k()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->p()Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 2690681
    :cond_4
    const/4 v7, 0x1

    .line 2690682
    new-instance v6, LX/JPt;

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->j()Ljava/lang/String;

    move-result-object v14

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->p()Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    move-result-object v15

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->l()Ljava/lang/String;

    move-result-object v17

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->k()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-direct {v6, v14, v15, v0, v4}, LX/JPt;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;Ljava/lang/String;Ljava/lang/String;)V

    .line 2690683
    :cond_5
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v14

    .line 2690684
    const/4 v4, 0x0

    move v5, v4

    :goto_1
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->n()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_6

    const/4 v4, 0x3

    if-ge v5, v4, :cond_6

    .line 2690685
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->n()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    .line 2690686
    new-instance v15, LX/JPq;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v15, v0, v1, v2, v4}, LX/JPq;-><init>(Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;LX/JPv;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;)V

    .line 2690687
    invoke-static {v4, v15}, LX/44w;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/44w;

    move-result-object v4

    invoke-virtual {v14, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2690688
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_1

    .line 2690689
    :cond_6
    invoke-virtual {v14}, LX/0Pz;->b()LX/0Px;

    move-result-object v17

    .line 2690690
    new-instance v15, LX/JPr;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    invoke-direct {v15, v0, v1, v2}, LX/JPr;-><init>(Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;LX/JPv;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    .line 2690691
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 2690692
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 2690693
    invoke-virtual/range {v18 .. v18}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->S()I

    move-result v4

    .line 2690694
    if-eqz v4, :cond_7

    .line 2690695
    const/16 v5, 0x14

    if-le v4, v5, :cond_8

    .line 2690696
    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2690697
    const-string v4, "+"

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2690698
    :cond_7
    :goto_2
    new-instance v5, LX/JPw;

    invoke-direct/range {v5 .. v17}, LX/JPw;-><init>(LX/JPt;ZJLjava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/StringBuilder;Landroid/view/View$OnClickListener;Landroid/graphics/PointF;LX/0Px;)V

    return-object v5

    .line 2690699
    :cond_8
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2
.end method

.method private static a(Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;JLjava/lang/String;Ljava/lang/String;LX/JPt;LX/0lF;Z)Landroid/view/View$OnClickListener;
    .locals 15

    .prologue
    .line 2690649
    iget-object v2, p0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2690650
    new-instance v2, LX/JPu;

    move-object/from16 v0, p5

    iget-object v11, v0, LX/JPt;->a:Ljava/lang/String;

    move-object/from16 v0, p5

    iget-object v12, v0, LX/JPt;->b:Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    move-object/from16 v0, p5

    iget-object v13, v0, LX/JPt;->d:Ljava/lang/String;

    move-object v3, p0

    move-wide/from16 v6, p1

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v10, p7

    move-object/from16 v14, p6

    invoke-direct/range {v2 .. v14}, LX/JPu;-><init>(Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;JJLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;Ljava/lang/String;LX/0lF;)V

    return-object v2
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;
    .locals 15

    .prologue
    .line 2690611
    const-class v1, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    monitor-enter v1

    .line 2690612
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->m:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2690613
    sput-object v2, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->m:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2690614
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2690615
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2690616
    new-instance v3, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v4

    check-cast v4, LX/1nA;

    invoke-static {v0}, LX/HVa;->b(LX/0QB;)LX/HVa;

    move-result-object v5

    check-cast v5, LX/HVa;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v7

    check-cast v7, LX/17Q;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v8

    check-cast v8, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    const/16 v9, 0x15e7

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v10

    check-cast v10, LX/1Kf;

    invoke-static {v0}, LX/HSx;->b(LX/0QB;)LX/HSx;

    move-result-object v11

    check-cast v11, LX/CSL;

    invoke-static {v0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v12

    check-cast v12, LX/0SI;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v13

    check-cast v13, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v14

    check-cast v14, LX/17Y;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;-><init>(LX/1nA;LX/HVa;LX/0Zb;LX/17Q;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0Or;LX/1Kf;LX/CSL;LX/0SI;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 2690617
    move-object v0, v3

    .line 2690618
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2690619
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2690620
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2690621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/JPv;LX/JPw;Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2690631
    iget-object v3, p1, LX/JPv;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2690632
    iget-object v0, p2, LX/JPw;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->setHeaderOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2690633
    iget-object v0, p2, LX/JPw;->d:Landroid/net/Uri;

    iget-object v4, p2, LX/JPw;->j:Landroid/graphics/PointF;

    .line 2690634
    iget-object v5, p3, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;

    invoke-virtual {v5, v0, v4}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->a(Landroid/net/Uri;Landroid/graphics/PointF;)V

    .line 2690635
    iget-object v0, p2, LX/JPw;->e:Landroid/net/Uri;

    .line 2690636
    iget-object v4, p3, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;

    invoke-virtual {v4, v0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->setProfilePhoto(Landroid/net/Uri;)V

    .line 2690637
    iget-object v0, p2, LX/JPw;->f:Ljava/lang/String;

    iget-object v4, p2, LX/JPw;->h:Ljava/lang/String;

    .line 2690638
    iget-object v5, p3, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;

    invoke-virtual {v5, v0, v4}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2690639
    iget-object v0, p2, LX/JPw;->g:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2690640
    iget-object v4, p3, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->c:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;

    invoke-virtual {v4, v0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyCoverHeaderView;->setBadgeCount(Ljava/lang/String;)V

    .line 2690641
    iget-object v4, p2, LX/JPw;->k:LX/0Px;

    iget-boolean v0, p2, LX/JPw;->b:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p3, v4, v0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->a(Ljava/util/List;Z)V

    .line 2690642
    iget-boolean v0, p2, LX/JPw;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/JPw;->a:LX/JPt;

    if-eqz v0, :cond_0

    .line 2690643
    iget-object v0, p1, LX/JPv;->a:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v8, v1

    .line 2690644
    :goto_1
    iget-object v0, p1, LX/JPv;->a:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-static {v3, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v7

    .line 2690645
    iget-object v0, p2, LX/JPw;->a:LX/JPt;

    iget-object v0, v0, LX/JPt;->a:Ljava/lang/String;

    iget-object v1, p2, LX/JPw;->a:LX/JPt;

    iget-object v9, v1, LX/JPt;->c:Ljava/lang/String;

    iget-wide v2, p2, LX/JPw;->c:J

    iget-object v4, p2, LX/JPw;->f:Ljava/lang/String;

    iget-object v1, p2, LX/JPw;->e:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p2, LX/JPw;->a:LX/JPt;

    move-object v1, p0

    invoke-static/range {v1 .. v8}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->a(Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;JLjava/lang/String;Ljava/lang/String;LX/JPt;LX/0lF;Z)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p3, v0, v9, v1}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2690646
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 2690647
    goto :goto_0

    :cond_2
    move v8, v2

    .line 2690648
    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2690630
    sget-object v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2690629
    check-cast p2, LX/JPv;

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->a(LX/JPv;)LX/JPw;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x67f2b28b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2690628
    check-cast p1, LX/JPv;

    check-cast p2, LX/JPw;

    check-cast p4, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;

    invoke-direct {p0, p1, p2, p4}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->a(LX/JPv;LX/JPw;Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;)V

    const/16 v1, 0x1f

    const v2, -0x471d3793

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2690622
    check-cast p2, LX/JPw;

    check-cast p4, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;

    const/4 v1, 0x0

    .line 2690623
    const/4 v0, 0x0

    invoke-virtual {p4, v1, v0}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->a(Ljava/util/List;Z)V

    .line 2690624
    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->setHeaderOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2690625
    iget-boolean v0, p2, LX/JPw;->b:Z

    if-eqz v0, :cond_0

    .line 2690626
    invoke-virtual {p4, v1, v1, v1}, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelBodyView;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2690627
    :cond_0
    return-void
.end method
