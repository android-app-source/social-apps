.class public Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/1nA;

.field public final b:LX/1nu;

.field public final c:LX/JOt;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2688826
    const-class v0, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nA;LX/1nu;LX/JOt;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nA;",
            "LX/1nu;",
            "LX/JOt;",
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2688827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2688828
    iput-object p1, p0, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->a:LX/1nA;

    .line 2688829
    iput-object p2, p0, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->b:LX/1nu;

    .line 2688830
    iput-object p3, p0, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->c:LX/JOt;

    .line 2688831
    iput-object p4, p0, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->d:LX/0Ot;

    .line 2688832
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;LX/1De;Ljava/util/List;)LX/1Di;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x5

    const/4 v1, 0x2

    .line 2688833
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v3, :cond_0

    .line 2688834
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2688835
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    .line 2688836
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    .line 2688837
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2688838
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    .line 2688839
    :goto_2
    iget-object v5, p0, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->b:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    sget-object v6, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v4

    const v5, 0x7f021952

    invoke-virtual {v4, v5}, LX/1nw;->h(I)LX/1nw;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b2588

    invoke-interface {v4, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b2588

    invoke-interface {v4, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const/16 v5, 0x8

    const v6, 0x7f0b0034

    invoke-interface {v4, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    move-object v0, v4

    .line 2688840
    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2688841
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2688842
    :cond_1
    return-object v2

    .line 2688843
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto :goto_2
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;
    .locals 7

    .prologue
    .line 2688844
    const-class v1, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;

    monitor-enter v1

    .line 2688845
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688846
    sput-object v2, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688847
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688848
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688849
    new-instance v6, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v3

    check-cast v3, LX/1nA;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/JOt;->a(LX/0QB;)LX/JOt;

    move-result-object v5

    check-cast v5, LX/JOt;

    const/16 p0, 0x1fdc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;-><init>(LX/1nA;LX/1nu;LX/JOt;LX/0Ot;)V

    .line 2688850
    move-object v0, v6

    .line 2688851
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688852
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/hpp/LikeAndFollowersCardComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688853
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688854
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
