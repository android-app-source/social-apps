.class public Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/JTn;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/JTn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697272
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2697273
    iput-object p2, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;->d:LX/1V0;

    .line 2697274
    iput-object p3, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;->e:LX/JTn;

    .line 2697275
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2697276
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;->e:LX/JTn;

    const/4 v1, 0x0

    .line 2697277
    new-instance v2, LX/JTm;

    invoke-direct {v2, v0}, LX/JTm;-><init>(LX/JTn;)V

    .line 2697278
    sget-object v3, LX/JTn;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JTl;

    .line 2697279
    if-nez v3, :cond_0

    .line 2697280
    new-instance v3, LX/JTl;

    invoke-direct {v3}, LX/JTl;-><init>()V

    .line 2697281
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JTl;->a$redex0(LX/JTl;LX/1De;IILX/JTm;)V

    .line 2697282
    move-object v2, v3

    .line 2697283
    move-object v1, v2

    .line 2697284
    move-object v0, v1

    .line 2697285
    iget-object v1, v0, LX/JTl;->a:LX/JTm;

    iput-object p2, v1, LX/JTm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2697286
    iget-object v1, v0, LX/JTl;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2697287
    move-object v1, v0

    .line 2697288
    move-object v0, p3

    check-cast v0, LX/1Pk;

    invoke-interface {v0}, LX/1Pk;->e()LX/1SX;

    move-result-object v0

    .line 2697289
    iget-object v2, v1, LX/JTl;->a:LX/JTm;

    iput-object v0, v2, LX/JTm;->b:LX/1SX;

    .line 2697290
    iget-object v2, v1, LX/JTl;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2697291
    move-object v0, v1

    .line 2697292
    sget-object v1, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    .line 2697293
    iget-object v2, v0, LX/JTl;->a:LX/JTm;

    iput-object v1, v2, LX/JTm;->c:LX/2ei;

    .line 2697294
    iget-object v2, v0, LX/JTl;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2697295
    move-object v0, v0

    .line 2697296
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2697297
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;->d:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->i:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;
    .locals 6

    .prologue
    .line 2697261
    const-class v1, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2697262
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697263
    sput-object v2, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697264
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697265
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697266
    new-instance p0, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/JTn;->a(LX/0QB;)LX/JTn;

    move-result-object v5

    check-cast v5, LX/JTn;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/JTn;)V

    .line 2697267
    move-object v0, p0

    .line 2697268
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697269
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697270
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697271
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2697298
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2697260
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2697259
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2697256
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2697257
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 2697258
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
