.class public Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/facepile/FacepileView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2698120
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2698121
    invoke-direct {p0}, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->a()V

    .line 2698122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2698123
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2698124
    invoke-direct {p0}, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->a()V

    .line 2698125
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2698126
    const v0, 0x7f030fec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2698127
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->setOrientation(I)V

    .line 2698128
    const v0, 0x7f0d265e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->a:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2698129
    const v0, 0x7f0d265f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->b:Landroid/widget/TextView;

    .line 2698130
    const v0, 0x7f0d2660

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->c:Landroid/view/View;

    .line 2698131
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->c:Landroid/view/View;

    new-instance v1, LX/JUN;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, LX/JUN;-><init>(I)V

    invoke-static {v0, v1}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2698132
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2698133
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2698134
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2698135
    new-instance v6, LX/6UY;

    invoke-direct {v6, v0}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v4, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2698136
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2698137
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->a:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 2698138
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2698139
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-le p2, v0, :cond_2

    .line 2698140
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00b9

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    sub-int v5, p2, v5

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    sub-int v6, p2, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v1, v2

    invoke-virtual {v3, v4, v5, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2698141
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2698142
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 2698143
    goto :goto_1

    .line 2698144
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/ui/PopupFacepileView;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method
