.class public Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JUH;",
        "LX/JUA;",
        "LX/1PW;",
        "LX/JUJ;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/1g5;

.field public final b:LX/1nA;

.field public final c:LX/0wM;

.field public final d:LX/JUO;

.field public final e:LX/0hy;

.field public final f:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/1nA;LX/1g5;LX/0wM;LX/JUO;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697764
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2697765
    iput-object p1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->b:LX/1nA;

    .line 2697766
    iput-object p2, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->a:LX/1g5;

    .line 2697767
    iput-object p3, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->c:LX/0wM;

    .line 2697768
    iput-object p4, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->d:LX/JUO;

    .line 2697769
    iput-object p5, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->e:LX/0hy;

    .line 2697770
    iput-object p6, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->f:Lcom/facebook/content/SecureContextHelper;

    .line 2697771
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;
    .locals 10

    .prologue
    .line 2697772
    const-class v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    monitor-enter v1

    .line 2697773
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697774
    sput-object v2, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697775
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697776
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697777
    new-instance v3, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v4

    check-cast v4, LX/1nA;

    invoke-static {v0}, LX/1g5;->a(LX/0QB;)LX/1g5;

    move-result-object v5

    check-cast v5, LX/1g5;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v6

    check-cast v6, LX/0wM;

    invoke-static {v0}, LX/JUO;->b(LX/0QB;)LX/JUO;

    move-result-object v7

    check-cast v7, LX/JUO;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v8

    check-cast v8, LX/0hy;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;-><init>(LX/1nA;LX/1g5;LX/0wM;LX/JUO;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V

    .line 2697778
    move-object v0, v3

    .line 2697779
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697780
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697781
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697782
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2697783
    check-cast p2, LX/JUH;

    .line 2697784
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    iget-object v1, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-static {v1}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2697785
    sget-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_TAP_MESSAGE:LX/6VK;

    iget-object v1, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v2, p2, LX/JUH;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-static {v1, v2}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    invoke-static {v0, v1}, LX/1g5;->a(LX/6VK;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2697786
    if-eqz v5, :cond_0

    .line 2697787
    const-string v0, "feed_type"

    iget-object v1, p2, LX/JUH;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2697788
    const-string v0, "location_category"

    iget-object v1, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2697789
    :cond_0
    iget-object v0, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-static {v0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v6

    .line 2697790
    new-instance v0, LX/JUA;

    .line 2697791
    new-instance v1, LX/JU7;

    invoke-direct {v1, p0, p2}, LX/JU7;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;LX/JUH;)V

    move-object v1, v1

    .line 2697792
    new-instance v2, LX/JU9;

    invoke-direct {v2, p0, p2}, LX/JU9;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;LX/JUH;)V

    move-object v2, v2

    .line 2697793
    new-instance v3, LX/JU8;

    invoke-direct {v3, p0, p2}, LX/JU8;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;LX/JUH;)V

    move-object v3, v3

    .line 2697794
    iget-object v7, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->b:LX/1nA;

    invoke-virtual {v7, v4, v5}, LX/1nA;->a(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    if-nez v6, :cond_1

    const-string v6, ""

    :goto_0
    invoke-direct/range {v0 .. v6}, LX/JUA;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    iget-object v6, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2ccdd74f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2697795
    check-cast p2, LX/JUA;

    check-cast p4, LX/JUJ;

    .line 2697796
    iget-object v1, p2, LX/JUA;->e:Ljava/lang/String;

    invoke-virtual {p4, v1}, LX/JUJ;->setNameText(Ljava/lang/String;)V

    .line 2697797
    iget-object v1, p2, LX/JUA;->f:Ljava/lang/String;

    invoke-virtual {p4, v1}, LX/JUJ;->setExtraText(Ljava/lang/String;)V

    .line 2697798
    iget-object v1, p2, LX/JUA;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setOnNameClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697799
    iget-object v1, p2, LX/JUA;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setOnLocationClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697800
    iget-object v1, p2, LX/JUA;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setOnNameLocationSectionClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697801
    iget-object v1, p2, LX/JUA;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setPageCoverOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697802
    iget-object v1, p2, LX/JUA;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setCenterCircleOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697803
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->c:LX/0wM;

    invoke-static {}, LX/10A;->a()I

    move-result v2

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const p3, 0x7f0a00e6

    invoke-static {p1, p3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object v1, v1

    .line 2697804
    iget-object v2, p2, LX/JUA;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2}, LX/JUJ;->a(Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;)V

    .line 2697805
    const/16 v1, 0x1f

    const v2, 0x47985921

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2697806
    check-cast p4, LX/JUJ;

    const/4 v0, 0x0

    .line 2697807
    invoke-virtual {p4, v0}, LX/JUJ;->setOnNameClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697808
    invoke-virtual {p4, v0}, LX/JUJ;->setOnLocationClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697809
    invoke-virtual {p4, v0}, LX/JUJ;->setOnNameLocationSectionClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697810
    invoke-virtual {p4, v0}, LX/JUJ;->setPageCoverOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697811
    invoke-virtual {p4, v0}, LX/JUJ;->setCenterCircleOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697812
    invoke-virtual {p4, v0, v0}, LX/JUJ;->a(Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;)V

    .line 2697813
    return-void
.end method
