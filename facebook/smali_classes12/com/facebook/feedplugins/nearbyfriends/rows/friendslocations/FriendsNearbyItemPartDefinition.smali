.class public Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/JUH;",
        "LX/JUI;",
        "LX/1PW;",
        "LX/JUJ;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/JUJ;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

.field private final d:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2697983
    const-class v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2697984
    new-instance v0, LX/JUG;

    invoke-direct {v0}, LX/JUG;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;",
            "Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697978
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 2697979
    iput-object p1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    .line 2697980
    iput-object p2, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->d:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    .line 2697981
    iput-object p3, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->e:LX/0Or;

    .line 2697982
    return-void
.end method

.method private a(LX/JUH;)LX/1aZ;
    .locals 3

    .prologue
    .line 2697966
    const/4 v0, 0x0

    .line 2697967
    iget-object v1, p1, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v1

    .line 2697968
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-eq v1, v2, :cond_0

    .line 2697969
    iget-object v1, p1, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    .line 2697970
    invoke-static {v1}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    .line 2697971
    if-eqz v2, :cond_1

    invoke-static {v2}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLProfile;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2697972
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2697973
    :goto_0
    move-object v1, v2

    .line 2697974
    if-eqz v1, :cond_0

    .line 2697975
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2697976
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2697977
    :cond_0
    return-object v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;
    .locals 6

    .prologue
    .line 2697922
    const-class v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;

    monitor-enter v1

    .line 2697923
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697924
    sput-object v2, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697925
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697926
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697927
    new-instance v5, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;LX/0Or;)V

    .line 2697928
    move-object v0, v5

    .line 2697929
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697930
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697931
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697932
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/JUJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2697965
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2697941
    check-cast p2, LX/JUH;

    .line 2697942
    iget-object v0, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v0

    .line 2697943
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->CHECKIN:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-ne v0, v1, :cond_2

    .line 2697944
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->d:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsCheckinPagePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2697945
    :cond_0
    :goto_0
    new-instance v1, LX/JUI;

    .line 2697946
    const/4 v5, 0x0

    .line 2697947
    iget-object v6, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v6

    .line 2697948
    iget-object v7, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v7

    .line 2697949
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-eq v6, v8, :cond_1

    if-eqz v7, :cond_1

    .line 2697950
    new-instance v5, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v6, "friends_nearby_ego_unit"

    invoke-direct {v5, v6}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v9

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v7

    invoke-virtual {v5, v9, v10, v7, v8}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v5

    iget-object v6, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->o()D

    move-result-wide v7

    double-to-int v6, v7

    const/4 v8, 0x1

    .line 2697951
    const/16 v9, 0xa

    .line 2697952
    const/16 v7, 0xb

    .line 2697953
    :goto_1
    if-le v7, v8, :cond_6

    .line 2697954
    if-gt v6, v9, :cond_5

    .line 2697955
    :goto_2
    move v6, v7

    .line 2697956
    invoke-virtual {v5, v6}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v5

    .line 2697957
    :cond_1
    move-object v2, v5

    .line 2697958
    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->a(LX/JUH;)LX/1aZ;

    move-result-object v3

    iget-object v0, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->APPROXIMATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-ne v0, v4, :cond_4

    const/4 v0, 0x1

    :goto_3
    invoke-direct {v1, v2, v3, v0}, LX/JUI;-><init>(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;LX/1aZ;Z)V

    return-object v1

    .line 2697959
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->APPROXIMATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->CURRENT_CITY:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-ne v0, v1, :cond_0

    .line 2697960
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    .line 2697961
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 2697962
    :cond_5
    add-int/lit8 v7, v7, -0x1

    .line 2697963
    mul-int/lit8 v9, v9, 0x2

    goto :goto_1

    :cond_6
    move v7, v8

    .line 2697964
    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x235ea8f3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2697937
    check-cast p2, LX/JUI;

    check-cast p4, LX/JUJ;

    .line 2697938
    iget-object v1, p2, LX/JUI;->a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {p4, v1}, LX/JUJ;->setPageCoverMap(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 2697939
    iget-object v1, p2, LX/JUI;->b:LX/1aZ;

    invoke-virtual {p4, v1}, LX/JUJ;->setCenterImage(LX/1aZ;)V

    .line 2697940
    const/16 v1, 0x1f

    const v2, -0x1aad8647

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2697933
    check-cast p4, LX/JUJ;

    const/4 v0, 0x0

    .line 2697934
    invoke-virtual {p4, v0}, LX/JUJ;->setPageCoverMap(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 2697935
    invoke-virtual {p4, v0}, LX/JUJ;->setCenterImage(LX/1aZ;)V

    .line 2697936
    return-void
.end method
