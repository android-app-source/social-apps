.class public Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JUH;",
        "LX/JUF;",
        "LX/1PW;",
        "LX/JUJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/JUO;

.field private final c:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2697910
    sget-object v0, LX/0ax;->dX:Ljava/lang/String;

    const-string v1, "feed_friends_locations_pulse"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/JUO;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697906
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2697907
    iput-object p1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;->b:LX/JUO;

    .line 2697908
    iput-object p2, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2697909
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;
    .locals 5

    .prologue
    .line 2697872
    const-class v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;

    monitor-enter v1

    .line 2697873
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697874
    sput-object v2, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697875
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697876
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697877
    new-instance p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;

    invoke-static {v0}, LX/JUO;->b(LX/0QB;)LX/JUO;

    move-result-object v3

    check-cast v3, LX/JUO;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;-><init>(LX/JUO;Lcom/facebook/content/SecureContextHelper;)V

    .line 2697878
    move-object v0, p0

    .line 2697879
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697880
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697881
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697882
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2697902
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2697903
    sget-object v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2697904
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2697905
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2697897
    check-cast p2, LX/JUH;

    .line 2697898
    new-instance v2, LX/JUF;

    .line 2697899
    new-instance v0, LX/JUD;

    invoke-direct {v0, p0, p2}, LX/JUD;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;LX/JUH;)V

    move-object v3, v0

    .line 2697900
    new-instance v0, LX/JUE;

    invoke-direct {v0, p0, p2}, LX/JUE;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;LX/JUH;)V

    move-object v4, v0

    .line 2697901
    iget-object v0, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    iget-object v1, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, ""

    :goto_1
    invoke-direct {v2, v3, v4, v0, v1}, LX/JUF;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    :cond_0
    iget-object v0, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3298de4d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2697889
    check-cast p2, LX/JUF;

    check-cast p4, LX/JUJ;

    .line 2697890
    iget-object v1, p2, LX/JUF;->c:Ljava/lang/String;

    invoke-virtual {p4, v1}, LX/JUJ;->setNameText(Ljava/lang/String;)V

    .line 2697891
    iget-object v1, p2, LX/JUF;->d:Ljava/lang/String;

    invoke-virtual {p4, v1}, LX/JUJ;->setExtraText(Ljava/lang/String;)V

    .line 2697892
    iget-object v1, p2, LX/JUF;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setOnNameClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697893
    iget-object v1, p2, LX/JUF;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setOnLocationClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697894
    iget-object v1, p2, LX/JUF;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setOnNameLocationSectionClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697895
    iget-object v1, p2, LX/JUF;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setPulseProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697896
    const/16 v1, 0x1f

    const v2, 0x68473232

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2697883
    check-cast p4, LX/JUJ;

    const/4 v0, 0x0

    .line 2697884
    invoke-virtual {p4, v0}, LX/JUJ;->setOnNameClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697885
    invoke-virtual {p4, v0}, LX/JUJ;->setOnLocationClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697886
    invoke-virtual {p4, v0}, LX/JUJ;->setOnNameLocationSectionClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697887
    invoke-virtual {p4, v0}, LX/JUJ;->setPulseProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697888
    return-void
.end method
