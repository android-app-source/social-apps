.class public Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/JUH;",
        "LX/JU6;",
        "TE;",
        "LX/JUJ;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/1g5;

.field private final b:LX/1nA;

.field public final c:LX/0wM;

.field public final d:LX/JUO;

.field public final e:LX/3LX;

.field public final f:LX/0kL;

.field public final g:Lcom/facebook/content/SecureContextHelper;

.field public final h:LX/0iA;


# direct methods
.method public constructor <init>(LX/1nA;LX/1g5;LX/0wM;LX/JUO;LX/3LX;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/0iA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697708
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2697709
    iput-object p1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->b:LX/1nA;

    .line 2697710
    iput-object p2, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->a:LX/1g5;

    .line 2697711
    iput-object p3, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->c:LX/0wM;

    .line 2697712
    iput-object p4, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->d:LX/JUO;

    .line 2697713
    iput-object p5, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->e:LX/3LX;

    .line 2697714
    iput-object p6, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->f:LX/0kL;

    .line 2697715
    iput-object p7, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->g:Lcom/facebook/content/SecureContextHelper;

    .line 2697716
    iput-object p8, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->h:LX/0iA;

    .line 2697717
    return-void
.end method

.method private a(LX/JUH;LX/JTs;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/DHx;
    .locals 6

    .prologue
    .line 2697718
    new-instance v0, LX/JU4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/JU4;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;LX/JUH;LX/JTs;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;
    .locals 12

    .prologue
    .line 2697695
    const-class v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    monitor-enter v1

    .line 2697696
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697697
    sput-object v2, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697698
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697699
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697700
    new-instance v3, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v4

    check-cast v4, LX/1nA;

    invoke-static {v0}, LX/1g5;->a(LX/0QB;)LX/1g5;

    move-result-object v5

    check-cast v5, LX/1g5;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v6

    check-cast v6, LX/0wM;

    invoke-static {v0}, LX/JUO;->b(LX/0QB;)LX/JUO;

    move-result-object v7

    check-cast v7, LX/JUO;

    invoke-static {v0}, LX/3LX;->b(LX/0QB;)LX/3LX;

    move-result-object v8

    check-cast v8, LX/3LX;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v11

    check-cast v11, LX/0iA;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;-><init>(LX/1nA;LX/1g5;LX/0wM;LX/JUO;LX/3LX;LX/0kL;Lcom/facebook/content/SecureContextHelper;LX/0iA;)V

    .line 2697701
    move-object v0, v3

    .line 2697702
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697703
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697704
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697705
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;Landroid/view/View;LX/JUH;)V
    .locals 3

    .prologue
    .line 2697706
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->b:LX/1nA;

    iget-object v1, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-static {v1}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-static {v1}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/1y5;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 2697707
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2697638
    check-cast p2, LX/JUH;

    check-cast p3, LX/1Pr;

    const/4 v6, 0x0

    .line 2697639
    new-instance v0, LX/JTr;

    iget-object v1, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-direct {v0, v1}, LX/JTr;-><init>(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;)V

    iget-object v1, p2, LX/JUH;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/JTs;

    .line 2697640
    iget-object v0, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->n()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    .line 2697641
    if-nez v0, :cond_5

    .line 2697642
    sget-object v1, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    .line 2697643
    :goto_0
    move-object v0, v1

    .line 2697644
    iput-object v0, v9, LX/JTs;->a:LX/3OL;

    .line 2697645
    iget-object v0, v9, LX/JTs;->a:LX/3OL;

    move-object v0, v0

    .line 2697646
    sget-object v1, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    move v5, v0

    .line 2697647
    :goto_1
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    iget-object v1, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-static {v1}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2697648
    sget-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_TAP_MESSAGE:LX/6VK;

    iget-object v1, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v2, p2, LX/JUH;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-static {v1, v2}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    invoke-static {v0, v1}, LX/1g5;->a(LX/6VK;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    .line 2697649
    if-eqz v8, :cond_0

    .line 2697650
    const-string v0, "feed_type"

    iget-object v1, p2, LX/JUH;->a:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2697651
    const-string v0, "location_category"

    iget-object v1, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2697652
    :cond_0
    iget-object v0, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-static {v0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v10

    .line 2697653
    new-instance v0, LX/JU6;

    .line 2697654
    new-instance v1, LX/JTy;

    invoke-direct {v1, p0, p2}, LX/JTy;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;LX/JUH;)V

    move-object v1, v1

    .line 2697655
    new-instance v2, LX/JU0;

    invoke-direct {v2, p0, p2}, LX/JU0;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;LX/JUH;)V

    move-object v2, v2

    .line 2697656
    new-instance v3, LX/JTz;

    invoke-direct {v3, p0, p2}, LX/JTz;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;LX/JUH;)V

    move-object v3, v3

    .line 2697657
    if-eqz v5, :cond_2

    move-object v4, v6

    :goto_2
    if-eqz v5, :cond_3

    invoke-direct {p0, p2, v9, v7, v8}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->a(LX/JUH;LX/JTs;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/DHx;

    move-result-object v5

    :goto_3
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v6

    .line 2697658
    iget-object v7, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->APPROXIMATE_LOCATION:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-ne v7, v8, :cond_6

    .line 2697659
    const v7, 0x7f02093c

    .line 2697660
    :goto_4
    move v7, v7

    .line 2697661
    iget-object v8, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    if-nez v8, :cond_4

    const-string v8, ""

    :goto_5
    invoke-direct/range {v0 .. v9}, LX/JU6;-><init>(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;LX/DHx;Ljava/lang/String;ILjava/lang/String;LX/JTs;)V

    return-object v0

    .line 2697662
    :cond_1
    const/4 v0, 0x0

    move v5, v0

    goto :goto_1

    .line 2697663
    :cond_2
    iget-object v4, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->b:LX/1nA;

    invoke-virtual {v4, v7, v8}, LX/1nA;->a(Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)Landroid/view/View$OnClickListener;

    move-result-object v4

    goto :goto_2

    :cond_3
    move-object v5, v6

    goto :goto_3

    :cond_4
    iget-object v8, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    goto :goto_5

    .line 2697664
    :cond_5
    sget-object v1, LX/JU5;->b:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->aj()Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLLocationWaveStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2697665
    sget-object v1, LX/3OL;->NOT_AVAILABLE:LX/3OL;

    goto/16 :goto_0

    .line 2697666
    :pswitch_0
    sget-object v1, LX/3OL;->NOT_SENT:LX/3OL;

    goto/16 :goto_0

    .line 2697667
    :pswitch_1
    sget-object v1, LX/3OL;->SENT_UNDOABLE:LX/3OL;

    goto/16 :goto_0

    .line 2697668
    :pswitch_2
    sget-object v1, LX/3OL;->RECEIVED:LX/3OL;

    goto/16 :goto_0

    .line 2697669
    :pswitch_3
    sget-object v1, LX/3OL;->INTERACTED:LX/3OL;

    goto/16 :goto_0

    :cond_6
    const v7, 0x7f0208d3

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x352f3b0b    # -6840954.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2697670
    check-cast p2, LX/JU6;

    check-cast p4, LX/JUJ;

    .line 2697671
    iget-object v1, p2, LX/JU6;->f:Ljava/lang/String;

    invoke-virtual {p4, v1}, LX/JUJ;->setNameText(Ljava/lang/String;)V

    .line 2697672
    iget-object v1, p2, LX/JU6;->h:Ljava/lang/String;

    invoke-virtual {p4, v1}, LX/JUJ;->setExtraText(Ljava/lang/String;)V

    .line 2697673
    iget-object v1, p2, LX/JU6;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setOnNameClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697674
    iget-object v1, p2, LX/JU6;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setOnLocationClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697675
    iget-object v1, p2, LX/JU6;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setOnNameLocationSectionClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697676
    iget-object v1, p2, LX/JU6;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setPageCoverOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697677
    iget-object v1, p2, LX/JU6;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JUJ;->setCenterCircleOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697678
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;->c:LX/0wM;

    invoke-static {}, LX/10A;->a()I

    move-result v2

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const p3, 0x7f0a00e6

    invoke-static {p1, p3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    move-object v1, v1

    .line 2697679
    iget-object v2, p2, LX/JU6;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2}, LX/JUJ;->a(Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;)V

    .line 2697680
    iget-object v1, p2, LX/JU6;->i:LX/JTs;

    .line 2697681
    iget-object v2, v1, LX/JTs;->a:LX/3OL;

    move-object v1, v2

    .line 2697682
    iget-object v2, p2, LX/JU6;->e:LX/DHx;

    .line 2697683
    new-instance p1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition$5;

    invoke-direct {p1, p0, p4}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition$5;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsAuraPagePartDefinition;LX/JUJ;)V

    move-object p1, p1

    .line 2697684
    invoke-virtual {p4, v1, v2, p1}, LX/JUJ;->a(LX/3OL;LX/DHx;Ljava/lang/Runnable;)V

    .line 2697685
    const/16 v1, 0x1f

    const v2, -0x332ade4a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2697686
    check-cast p4, LX/JUJ;

    const/4 v0, 0x0

    .line 2697687
    invoke-virtual {p4, v0}, LX/JUJ;->setOnNameClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697688
    invoke-virtual {p4, v0}, LX/JUJ;->setOnLocationClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697689
    invoke-virtual {p4, v0}, LX/JUJ;->setOnNameLocationSectionClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697690
    invoke-virtual {p4, v0}, LX/JUJ;->setPageCoverOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697691
    invoke-virtual {p4, v0}, LX/JUJ;->setCenterCircleOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2697692
    invoke-virtual {p4, v0, v0}, LX/JUJ;->a(Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;)V

    .line 2697693
    invoke-virtual {p4, v0, v0, v0}, LX/JUJ;->a(LX/3OL;LX/DHx;Ljava/lang/Runnable;)V

    .line 2697694
    return-void
.end method
