.class public Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;

.field private final c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;

.field private final d:Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;

.field private final e:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697827
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2697828
    iput-object p3, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->a:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    .line 2697829
    iput-object p2, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;

    .line 2697830
    iput-object p1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;

    .line 2697831
    iput-object p4, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->d:Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;

    .line 2697832
    iput-object p5, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->e:LX/0ad;

    .line 2697833
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;
    .locals 9

    .prologue
    .line 2697834
    const-class v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;

    monitor-enter v1

    .line 2697835
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697836
    sput-object v2, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697837
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697838
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697839
    new-instance v3, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;LX/0ad;)V

    .line 2697840
    move-object v0, v3

    .line 2697841
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697842
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697843
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697844
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2697845
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2697846
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2697847
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 2697848
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->e:LX/0ad;

    sget-short v2, LX/JTf;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2697849
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->d:Lcom/facebook/feedplugins/nearbyfriends/rows/components/FriendsLocationsHeaderComponentPartDefinition;

    invoke-static {p1, v1, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2697850
    :goto_0
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->a:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    invoke-static {p1, v1, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2697851
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->b:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsFooterPartDefinition;

    invoke-static {p1, v1, v0}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2697852
    const/4 v0, 0x0

    return-object v0

    .line 2697853
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPartDefinition;->c:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsHeaderPartDefinition;

    invoke-static {p1, v1, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2697854
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2697855
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2697856
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 2697857
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
