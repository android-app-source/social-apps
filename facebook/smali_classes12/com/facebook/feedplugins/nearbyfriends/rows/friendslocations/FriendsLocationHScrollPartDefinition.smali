.class public Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/2eR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2eR",
            "<",
            "LX/JUJ;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final c:LX/2dq;

.field private final d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "LX/JUH;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/1LV;

.field public final f:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;

.field public final g:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

.field public final h:LX/JUO;

.field public final i:Landroid/content/Context;

.field public final j:LX/1DR;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2697459
    new-instance v0, LX/JTt;

    invoke-direct {v0}, LX/JTt;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->a:LX/2eR;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1LV;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;LX/JUO;Landroid/content/Context;LX/1DR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697421
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2697422
    iput-object p1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2697423
    iput-object p2, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->c:LX/2dq;

    .line 2697424
    iput-object p3, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2697425
    iput-object p4, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->e:LX/1LV;

    .line 2697426
    iput-object p5, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->f:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;

    .line 2697427
    iput-object p6, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->g:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    .line 2697428
    iput-object p7, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->h:LX/JUO;

    .line 2697429
    iput-object p8, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->i:Landroid/content/Context;

    .line 2697430
    iput-object p9, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->j:LX/1DR;

    .line 2697431
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;
    .locals 13

    .prologue
    .line 2697432
    const-class v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    monitor-enter v1

    .line 2697433
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697434
    sput-object v2, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697435
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697436
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697437
    new-instance v3, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v5

    check-cast v5, LX/2dq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v7

    check-cast v7, LX/1LV;

    invoke-static {v0}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    invoke-static {v0}, LX/JUO;->b(LX/0QB;)LX/JUO;

    move-result-object v10

    check-cast v10, LX/JUO;

    const-class v11, Landroid/content/Context;

    invoke-interface {v0, v11}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v12

    check-cast v12, LX/1DR;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1LV;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;LX/JUO;Landroid/content/Context;LX/1DR;)V

    .line 2697438
    move-object v0, v3

    .line 2697439
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697440
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697441
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697442
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2697443
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2697444
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2697445
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 2697446
    check-cast v5, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    .line 2697447
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/2eF;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2697448
    iget-object v6, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    const/4 v4, 0x1

    .line 2697449
    invoke-static {v5}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 2697450
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->j:LX/1DR;

    invoke-virtual {v2}, LX/1DR;->a()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v1, v2}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    add-int/lit8 v1, v1, -0x10

    .line 2697451
    const/4 v2, 0x0

    invoke-static {v1, v2, v4}, LX/2eF;->a(IZZ)LX/2eF;

    move-result-object v1

    .line 2697452
    :goto_0
    move-object v1, v1

    .line 2697453
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->I_()I

    move-result v2

    .line 2697454
    invoke-static {v5}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)LX/0Px;

    move-result-object v3

    .line 2697455
    new-instance v4, LX/JTu;

    invoke-direct {v4, p0, v5, v3}, LX/JTu;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;LX/0Px;)V

    move-object v3, v4

    .line 2697456
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2697457
    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationHScrollPartDefinition;->c:LX/2dq;

    const/high16 v2, 0x43720000    # 242.0f

    sget-object v3, LX/2eF;->a:LX/1Ua;

    invoke-virtual {v1, v2, v3, v4}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2697458
    const/4 v0, 0x1

    return v0
.end method
