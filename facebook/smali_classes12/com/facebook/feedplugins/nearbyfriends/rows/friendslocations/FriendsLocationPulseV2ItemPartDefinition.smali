.class public Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/JUH;",
        "LX/JTx;",
        "LX/1PW;",
        "LX/JUJ;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Ljava/lang/String;

.field private static g:LX/0Xm;


# instance fields
.field public final c:Landroid/content/Context;

.field private final d:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:LX/JUO;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2697544
    const-class v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2697545
    sget-object v0, LX/0ax;->dX:Ljava/lang/String;

    const-string v1, "feed_friends_locations_pulse"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;Lcom/facebook/content/SecureContextHelper;LX/JUO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697538
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 2697539
    iput-object p1, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->c:Landroid/content/Context;

    .line 2697540
    iput-object p2, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->d:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;

    .line 2697541
    iput-object p3, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2697542
    iput-object p4, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->f:LX/JUO;

    .line 2697543
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;
    .locals 7

    .prologue
    .line 2697527
    const-class v1, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    monitor-enter v1

    .line 2697528
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697529
    sput-object v2, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697530
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697531
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697532
    new-instance p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/JUO;->b(LX/0QB;)LX/JUO;

    move-result-object v6

    check-cast v6, LX/JUO;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;Lcom/facebook/content/SecureContextHelper;LX/JUO;)V

    .line 2697533
    move-object v0, p0

    .line 2697534
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697535
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697536
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697537
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLLocation;D)LX/697;
    .locals 9

    .prologue
    const-wide v6, 0x408f400000000000L    # 1000.0

    .line 2697524
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v0

    mul-double v2, p1, v6

    invoke-static {v0, v1, v2, v3}, LX/31h;->a(DD)D

    move-result-wide v0

    .line 2697525
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    neg-double v4, p1

    mul-double/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, LX/31h;->a(DD)D

    move-result-wide v2

    .line 2697526
    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v4

    new-instance v5, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v6

    invoke-direct {v5, v0, v1, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v4, v5}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v0

    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v0

    invoke-virtual {v0}, LX/696;->a()LX/697;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/680;Lcom/facebook/graphql/model/GraphQLLocation;D)Z
    .locals 8

    .prologue
    const-wide v6, 0x408f400000000000L    # 1000.0

    .line 2697546
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v0

    mul-double v2, p2, v6

    invoke-static {v0, v1, v2, v3}, LX/31h;->a(DD)D

    move-result-wide v0

    .line 2697547
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    neg-double v4, p2

    mul-double/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, LX/31h;->a(DD)D

    move-result-wide v2

    .line 2697548
    if-eqz p0, :cond_0

    const-wide v4, 0x4056800000000000L    # 90.0

    cmpg-double v0, v0, v4

    if-gtz v0, :cond_0

    const-wide v0, -0x3fa9800000000000L    # -90.0

    cmpl-double v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;LX/680;Lcom/facebook/graphql/model/GraphQLLocation;D)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const-wide v6, 0x408f400000000000L    # 1000.0

    .line 2697514
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v0

    mul-double v2, p3, v6

    invoke-static {v0, v1, v2, v3}, LX/31h;->a(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, LX/31h;->b(D)F

    move-result v0

    float-to-double v0, v0

    .line 2697515
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    neg-double v4, p3

    mul-double/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, LX/31h;->a(DD)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/31h;->b(D)F

    move-result v2

    float-to-double v2, v2

    .line 2697516
    iget-object v4, p1, LX/680;->k:LX/31h;

    move-object v4, v4

    .line 2697517
    sub-double v0, v2, v0

    invoke-virtual {v4, v0, v1}, LX/31h;->e(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 2697518
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2697519
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2697520
    iget-object v3, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020b8b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2697521
    invoke-virtual {v3, v8, v8, v0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2697522
    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2697523
    return-object v1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/JUJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2697513
    sget-object v0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsNearbyItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2697498
    check-cast p2, LX/JUH;

    .line 2697499
    iget-object v0, p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->d:Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationsPulsePagePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2697500
    new-instance v0, LX/JTx;

    .line 2697501
    new-instance v1, LX/JTw;

    invoke-direct {v1, p0, p2}, LX/JTw;-><init>(Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;LX/JUH;)V

    move-object v1, v1

    .line 2697502
    const/4 v3, 0x0

    .line 2697503
    iget-object v2, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;

    .line 2697504
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2697505
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->a()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p1

    :goto_0
    if-ge v3, p1, :cond_1

    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLUser;

    .line 2697506
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p3

    if-eqz p3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p3

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_0

    .line 2697507
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2697508
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2697509
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 2697510
    iget-object v3, p2, LX/JUH;->b:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->k()LX/0Px;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;

    .line 2697511
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsCluster;->j()I

    move-result v3

    move v3, v3

    .line 2697512
    invoke-direct {v0, v1, v2, v3}, LX/JTx;-><init>(LX/6Zz;LX/0Px;I)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x59637d1b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2697494
    check-cast p2, LX/JTx;

    check-cast p4, LX/JUJ;

    .line 2697495
    iget-object v1, p2, LX/JTx;->a:LX/6Zz;

    invoke-virtual {p4, v1}, LX/JUJ;->setPulseMapReadyCallback(LX/6Zz;)V

    .line 2697496
    iget-object v1, p2, LX/JTx;->b:LX/0Px;

    iget v2, p2, LX/JTx;->c:I

    sget-object p0, Lcom/facebook/feedplugins/nearbyfriends/rows/friendslocations/FriendsLocationPulseV2ItemPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p4, v1, v2, p0}, LX/JUJ;->a(LX/0Px;ILcom/facebook/common/callercontext/CallerContext;)V

    .line 2697497
    const/16 v1, 0x1f

    const v2, -0x2519d1f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2697490
    check-cast p4, LX/JUJ;

    const/4 v1, 0x0

    .line 2697491
    invoke-virtual {p4, v1}, LX/JUJ;->setPulseMapReadyCallback(LX/6Zz;)V

    .line 2697492
    const/4 v0, 0x0

    invoke-virtual {p4, v1, v0, v1}, LX/JUJ;->a(LX/0Px;ILcom/facebook/common/callercontext/CallerContext;)V

    .line 2697493
    return-void
.end method
