.class public Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        "V:",
        "LX/3Yi;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/JVh;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static u:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition",
            "<",
            "LX/3Yi;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final g:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

.field private final h:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

.field private final j:LX/0Uh;

.field public final k:LX/3i4;

.field private final l:Landroid/content/res/Resources;

.field private final m:LX/0W3;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0Zb;

.field public final p:LX/17V;

.field private final q:LX/2mt;

.field public final r:LX/17d;

.field public final s:Landroid/content/Context;

.field public final t:LX/39V;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2701371
    const-class v0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->a:Ljava/lang/String;

    .line 2701372
    const-class v0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "native_newsfeed"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;LX/0Uh;LX/3i4;Landroid/content/res/Resources;LX/0W3;LX/0Ot;LX/0Zb;LX/17V;LX/2mt;LX/17d;Landroid/content/Context;LX/39V;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;",
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;",
            "Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/3i4;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Zb;",
            "LX/17V;",
            "LX/2mt;",
            "LX/17d;",
            "Landroid/content/Context;",
            "LX/39V;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701381
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2701382
    iput-object p1, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2701383
    iput-object p2, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    .line 2701384
    iput-object p3, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    .line 2701385
    iput-object p4, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2701386
    iput-object p5, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    .line 2701387
    iput-object p6, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    .line 2701388
    iput-object p7, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    .line 2701389
    iput-object p8, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->j:LX/0Uh;

    .line 2701390
    iput-object p9, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->k:LX/3i4;

    .line 2701391
    iput-object p10, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->l:Landroid/content/res/Resources;

    .line 2701392
    iput-object p11, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->m:LX/0W3;

    .line 2701393
    iput-object p12, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->n:LX/0Ot;

    .line 2701394
    iput-object p13, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->o:LX/0Zb;

    .line 2701395
    iput-object p14, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->p:LX/17V;

    .line 2701396
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->q:LX/2mt;

    .line 2701397
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->r:LX/17d;

    .line 2701398
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->s:Landroid/content/Context;

    .line 2701399
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->t:LX/39V;

    .line 2701400
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;
    .locals 3

    .prologue
    .line 2701373
    const-class v1, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;

    monitor-enter v1

    .line 2701374
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->u:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701375
    sput-object v2, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->u:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701376
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701377
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701378
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701379
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701380
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/JVh;LX/1Po;LX/3Yi;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/JVh;",
            "TE;TV;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2701351
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 2701352
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2701353
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2701354
    const/4 v1, 0x0

    .line 2701355
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 2701356
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    .line 2701357
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v7

    const v8, 0x46a1c4a4

    if-eq v7, v8, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    const v7, -0x1e53800c

    if-ne v6, v7, :cond_4

    .line 2701358
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    .line 2701359
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 2701360
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 2701361
    invoke-virtual {p4, v1}, LX/3Yi;->setCallToActionText(Ljava/lang/String;)V

    .line 2701362
    :goto_2
    return-void

    .line 2701363
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->m:LX/0W3;

    sget-wide v4, LX/0X5;->fx:J

    invoke-interface {v0, v4, v5, v3}, LX/0W4;->a(JZ)Z

    move-result v0

    .line 2701364
    if-eqz v0, :cond_3

    .line 2701365
    iget-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->m:LX/0W3;

    sget-wide v2, LX/0X5;->fw:J

    iget-object v1, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->l:Landroid/content/res/Resources;

    const v4, 0x7f083a8f

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2701366
    invoke-virtual {p4, v0}, LX/3Yi;->setCallToActionText(Ljava/lang/String;)V

    goto :goto_2

    .line 2701367
    :cond_3
    iget-object v0, p4, LX/3Yi;->e:Lcom/facebook/resources/ui/FbButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2701368
    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;
    .locals 21

    .prologue
    .line 2701369
    new-instance v2, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static/range {p0 .. p0}, LX/3i4;->a(LX/0QB;)LX/3i4;

    move-result-object v11

    check-cast v11, LX/3i4;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v12

    check-cast v12, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v13

    check-cast v13, LX/0W3;

    const/16 v14, 0x259

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v15

    check-cast v15, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/17V;->a(LX/0QB;)LX/17V;

    move-result-object v16

    check-cast v16, LX/17V;

    invoke-static/range {p0 .. p0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v17

    check-cast v17, LX/2mt;

    invoke-static/range {p0 .. p0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v18

    check-cast v18, LX/17d;

    const-class v19, Landroid/content/Context;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/39V;->a(LX/0QB;)LX/39V;

    move-result-object v20

    check-cast v20, LX/39V;

    invoke-direct/range {v2 .. v20}, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;-><init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;LX/0Uh;LX/3i4;Landroid/content/res/Resources;LX/0W3;LX/0Ot;LX/0Zb;LX/17V;LX/2mt;LX/17d;Landroid/content/Context;LX/39V;)V

    .line 2701370
    return-object v2
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2701304
    sget-object v0, LX/3Yi;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2701305
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    .line 2701306
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2701307
    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2701308
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2701309
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v1, -0x3659e4dc    # -1360740.5f

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v3

    .line 2701310
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->I()Ljava/lang/String;

    move-result-object v1

    .line 2701311
    iget-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->q:LX/2mt;

    invoke-virtual {v0, p2}, LX/2mt;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v2

    .line 2701312
    iget-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    new-instance v4, LX/2yZ;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v7}, LX/2yZ;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v0, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2701313
    iget-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2701314
    iget-object v7, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    .line 2701315
    invoke-static {v2}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2701316
    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v8

    .line 2701317
    iput-object v2, v8, LX/47H;->a:Ljava/lang/String;

    .line 2701318
    move-object v8, v8

    .line 2701319
    invoke-virtual {v8}, LX/47H;->a()LX/47I;

    move-result-object v8

    .line 2701320
    iget-object v9, v0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->r:LX/17d;

    iget-object p3, v0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->s:Landroid/content/Context;

    invoke-virtual {v9, p3, v8}, LX/17d;->a(Landroid/content/Context;LX/47I;)Landroid/content/Intent;

    move-result-object v8

    .line 2701321
    if-eqz v8, :cond_2

    const-string v9, "extra_fallback_intent"

    const/4 p3, 0x0

    invoke-virtual {v8, v9, p3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    if-nez v8, :cond_2

    .line 2701322
    new-instance v8, LX/JVf;

    invoke-direct {v8, v0, v4, v2, v5}, LX/JVf;-><init>(Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/1Po;)V

    .line 2701323
    :goto_0
    move-object v0, v8

    .line 2701324
    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2701325
    iget-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->i:Lcom/facebook/feedplugins/links/BrowserPrefetchPartDefinition;

    new-instance v2, LX/2yf;

    const/4 v3, 0x1

    invoke-direct {v2, p2, v1, v3}, LX/2yf;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Z)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2701326
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_3

    .line 2701327
    :cond_0
    const/4 v0, 0x0

    .line 2701328
    :goto_1
    move-object v0, v0

    .line 2701329
    if-eqz v0, :cond_1

    .line 2701330
    iget-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2701331
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2701332
    new-instance v0, LX/JVh;

    invoke-direct {v0}, LX/JVh;-><init>()V

    return-object v0

    :cond_2
    new-instance v8, LX/JVg;

    invoke-direct {v8, v0, v4, v1, v3}, LX/JVg;-><init>(Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x78121d45

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2701333
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, LX/JVh;

    check-cast p3, LX/1Po;

    check-cast p4, LX/3Yi;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/JVh;LX/1Po;LX/3Yi;)V

    const/16 v1, 0x1f

    const v2, 0x794df608

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2701334
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 2701335
    iget-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->j:LX/0Uh;

    const/16 v1, 0x512

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 2701336
    :goto_0
    return v0

    .line 2701337
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2701338
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2701339
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLATFORM_INSTANT_APP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v1

    .line 2701340
    if-nez v1, :cond_1

    move v0, v2

    .line 2701341
    goto :goto_0

    .line 2701342
    :cond_1
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2701343
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v3, -0x3659e4dc    # -1360740.5f

    invoke-static {v1, v3}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v1

    .line 2701344
    if-nez v1, :cond_3

    .line 2701345
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "uknown"

    move-object v1, v0

    .line 2701346
    :goto_1
    iget-object v0, p0, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v3, Lcom/facebook/feedplugins/platform/InstantExperienceAttachmentPartDefinition;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Null style info for story with url:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 2701347
    goto :goto_0

    .line 2701348
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 2701349
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2701350
    return-void
.end method
