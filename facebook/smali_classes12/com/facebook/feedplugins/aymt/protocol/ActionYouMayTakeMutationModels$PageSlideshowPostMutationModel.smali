.class public final Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2ecbf29c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2684872
    const-class v0, Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2684873
    const-class v0, Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2684874
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2684875
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2684876
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2684877
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2684878
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2684879
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2684880
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2684881
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2684882
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2684883
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2684884
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2684885
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel;->e:Z

    .line 2684886
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2684887
    new-instance v0, Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel;-><init>()V

    .line 2684888
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2684889
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2684890
    const v0, 0x7c203b67

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2684891
    const v0, 0x16b24c8d

    return v0
.end method
