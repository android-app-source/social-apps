.class public final Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2684870
    const-class v0, Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel;

    new-instance v1, Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2684871
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2684869
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2684859
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2684860
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2684861
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2684862
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 2684863
    if-eqz p0, :cond_0

    .line 2684864
    const-string p2, "post_status"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2684865
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 2684866
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2684867
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2684868
    check-cast p1, Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel$Serializer;->a(Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
