.class public Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;
.super Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final e:LX/1VD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1VD",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1VE;

.field private final g:LX/1V0;

.field private final h:LX/1VH;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1VD;LX/1VE;LX/1V0;LX/1VH;LX/0ad;LX/0Ot;LX/1VI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1VD;",
            "LX/1VE;",
            "LX/1V0;",
            "LX/1VH;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;",
            ">;",
            "LX/1VI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2684531
    invoke-direct/range {p0 .. p8}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1VD;LX/1VE;LX/1V0;LX/1VH;LX/0ad;LX/0Ot;LX/1VI;)V

    .line 2684532
    iput-object p2, p0, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->e:LX/1VD;

    .line 2684533
    iput-object p3, p0, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->f:LX/1VE;

    .line 2684534
    iput-object p4, p0, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->g:LX/1V0;

    .line 2684535
    iput-object p5, p0, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->h:LX/1VH;

    .line 2684536
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;
    .locals 12

    .prologue
    .line 2684537
    const-class v1, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2684538
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2684539
    sput-object v2, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2684540
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2684541
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2684542
    new-instance v3, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1VD;->a(LX/0QB;)LX/1VD;

    move-result-object v5

    check-cast v5, LX/1VD;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v6

    check-cast v6, LX/1VE;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    invoke-static {v0}, LX/1VH;->a(LX/0QB;)LX/1VH;

    move-result-object v8

    check-cast v8, LX/1VH;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    const/16 v10, 0x904

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v11

    check-cast v11, LX/1VI;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1VD;LX/1VE;LX/1V0;LX/1VH;LX/0ad;LX/0Ot;LX/1VI;)V

    .line 2684543
    move-object v0, v3

    .line 2684544
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2684545
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2684546
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2684547
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2684548
    iget-object v0, p0, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->e:LX/1VD;

    invoke-virtual {v0, p1}, LX/1VD;->c(LX/1De;)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1X4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1X4;->a(LX/1Pb;)LX/1X4;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1X4;->c(Z)LX/1X4;

    move-result-object v0

    .line 2684549
    iget-object v1, p0, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->f:LX/1VE;

    invoke-virtual {v1, p2}, LX/1VE;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2684550
    const v1, 0x7f0b1076

    invoke-virtual {v0, v1}, LX/1X4;->h(I)LX/1X4;

    move-result-object v1

    const v2, 0x7f0b00f2

    invoke-virtual {v1, v2}, LX/1X4;->m(I)LX/1X4;

    move-result-object v1

    const v2, 0x7f0b00f3

    invoke-virtual {v1, v2}, LX/1X4;->j(I)LX/1X4;

    .line 2684551
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->f:LX/1VE;

    invoke-virtual {v1, p2}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v1

    .line 2684552
    iget-object v2, p0, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->g:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    .line 2684553
    iget-object v1, p0, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->h:LX/1VH;

    invoke-virtual {v1, p1}, LX/1VH;->c(LX/1De;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1XD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XD;

    move-result-object v1

    sget-object v2, LX/1EO;->NEWSFEED:LX/1EO;

    invoke-virtual {v1, v2}, LX/1XD;->a(LX/1EO;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XD;->a(LX/1X1;)LX/1XD;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2684554
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2684555
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-virtual {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method
