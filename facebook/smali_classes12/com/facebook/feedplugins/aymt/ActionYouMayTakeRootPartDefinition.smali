.class public Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final d:Lcom/facebook/feedplugins/aymt/ActionYouMayTakeFooterComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/aymt/ActionYouMayTakeFooterComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2684556
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2684557
    iput-object p1, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;->a:Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;

    .line 2684558
    iput-object p2, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;->b:Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;

    .line 2684559
    iput-object p3, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;->c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2684560
    iput-object p4, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;->d:Lcom/facebook/feedplugins/aymt/ActionYouMayTakeFooterComponentPartDefinition;

    .line 2684561
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;
    .locals 7

    .prologue
    .line 2684562
    const-class v1, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;

    monitor-enter v1

    .line 2684563
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2684564
    sput-object v2, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2684565
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2684566
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2684567
    new-instance p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeFooterComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/aymt/ActionYouMayTakeFooterComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeFooterComponentPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;-><init>(Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/aymt/ActionYouMayTakeFooterComponentPartDefinition;)V

    .line 2684568
    move-object v0, p0

    .line 2684569
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2684570
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2684571
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2684572
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2684573
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2684574
    invoke-static {p2}, LX/JMi;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2684575
    iget-object v1, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;->a:Lcom/facebook/feedplugins/aymt/ActionYouMayTakeHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2684576
    iget-object v1, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;->b:Lcom/facebook/feedplugins/aymt/AYMTPageSlideshowStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2684577
    iget-object v1, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;->c:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2684578
    iget-object v0, p0, Lcom/facebook/feedplugins/aymt/ActionYouMayTakeRootPartDefinition;->d:Lcom/facebook/feedplugins/aymt/ActionYouMayTakeFooterComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2684579
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2684580
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2684581
    const/4 v0, 0x1

    move v0, v0

    .line 2684582
    if-eqz v0, :cond_0

    .line 2684583
    const/4 v0, 0x1

    move v0, v0

    .line 2684584
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
