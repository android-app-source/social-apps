.class public Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/5up;

.field private final d:LX/JUQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2699175
    const-class v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/5up;LX/JUQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2699177
    iput-object p1, p0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->b:LX/1nu;

    .line 2699178
    iput-object p2, p0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->c:LX/5up;

    .line 2699179
    iput-object p3, p0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->d:LX/JUQ;

    .line 2699180
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;
    .locals 6

    .prologue
    .line 2699181
    const-class v1, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;

    monitor-enter v1

    .line 2699182
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699183
    sput-object v2, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699184
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699185
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699186
    new-instance p0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v4

    check-cast v4, LX/5up;

    invoke-static {v0}, LX/JUQ;->b(LX/0QB;)LX/JUQ;

    move-result-object v5

    check-cast v5, LX/JUQ;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;-><init>(LX/1nu;LX/5up;LX/JUQ;)V

    .line 2699187
    move-object v0, p0

    .line 2699188
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699189
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699190
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699191
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/JUq;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLReactionStoryAction;Ljava/lang/String;)V
    .locals 8
    .param p2    # LX/JUq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pr;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/model/GraphQLReactionStoryAction;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/JUq;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLReactionStoryAction;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2699192
    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SAVE_PAGE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    if-ne v0, v1, :cond_1

    .line 2699193
    iget-object v1, p0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->d:LX/JUQ;

    .line 2699194
    iget-boolean v0, p2, LX/JUq;->a:Z

    move v0, v0

    .line 2699195
    if-eqz v0, :cond_0

    sget-object v0, LX/Cfc;->UNSAVE_PAGE_TAP:LX/Cfc;

    :goto_0
    invoke-virtual {v1, p4, p6, v0}, LX/JUQ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/Cfc;)V

    .line 2699196
    invoke-virtual {p2}, LX/JUq;->a()V

    .line 2699197
    iget-object v2, p0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->c:LX/5up;

    .line 2699198
    iget-boolean v3, p2, LX/JUq;->a:Z

    move v3, v3

    .line 2699199
    if-eqz v3, :cond_2

    sget-object v3, LX/5uo;->SAVE:LX/5uo;

    :goto_1
    invoke-virtual {p5}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->R()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v4

    const-string v5, "native_feed_chaining_box"

    const-string v6, "toggle_button"

    new-instance v7, LX/JUV;

    invoke-direct {v7, p0, p3, p4, p2}, LX/JUV;-><init>(Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JUq;)V

    invoke-virtual/range {v2 .. v7}, LX/5up;->a(LX/5uo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2699200
    :goto_2
    return-void

    .line 2699201
    :cond_0
    sget-object v0, LX/Cfc;->SAVE_PAGE_TAP:LX/Cfc;

    goto :goto_0

    .line 2699202
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsFooterComponentSpec;->d:LX/JUQ;

    invoke-virtual {v0, p1, p4, p5, p6}, LX/JUQ;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLReactionStoryAction;Ljava/lang/String;)V

    goto :goto_2

    .line 2699203
    :cond_2
    sget-object v3, LX/5uo;->UNSAVE:LX/5uo;

    goto :goto_1
.end method
