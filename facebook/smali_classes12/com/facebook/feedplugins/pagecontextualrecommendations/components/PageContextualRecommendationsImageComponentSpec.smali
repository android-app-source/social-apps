.class public Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/JUQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2699356
    const-class v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/JUQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2699358
    iput-object p1, p0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;->b:LX/1nu;

    .line 2699359
    iput-object p2, p0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;->c:LX/JUQ;

    .line 2699360
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;
    .locals 5

    .prologue
    .line 2699361
    const-class v1, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;

    monitor-enter v1

    .line 2699362
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699363
    sput-object v2, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699364
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699365
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699366
    new-instance p0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/JUQ;->b(LX/0QB;)LX/JUQ;

    move-result-object v4

    check-cast v4, LX/JUQ;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;-><init>(LX/1nu;LX/JUQ;)V

    .line 2699367
    move-object v0, p0

    .line 2699368
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699369
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pagecontextualrecommendations/components/PageContextualRecommendationsImageComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699370
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699371
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
