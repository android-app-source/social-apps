.class public Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;",
        "LX/ICM;",
        "LX/1Ps;",
        "LX/3W0;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/17W;

.field private final c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field public final d:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

.field private final e:LX/0bH;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2549328
    sget-object v0, LX/0ax;->gC:Ljava/lang/String;

    sput-object v0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/17W;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;LX/0bH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2549329
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2549330
    iput-object p1, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->b:LX/17W;

    .line 2549331
    iput-object p2, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 2549332
    iput-object p3, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->d:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    .line 2549333
    iput-object p4, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->e:LX/0bH;

    .line 2549334
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;
    .locals 7

    .prologue
    .line 2549317
    const-class v1, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    monitor-enter v1

    .line 2549318
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2549319
    sput-object v2, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2549320
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2549321
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2549322
    new-instance p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->b(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v6

    check-cast v6, LX/0bH;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;-><init>(LX/17W;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;LX/0bH;)V

    .line 2549323
    move-object v0, p0

    .line 2549324
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2549325
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2549326
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2549327
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2549314
    iget-object v6, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->e:LX/0bH;

    new-instance v0, LX/1Nd;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 2549315
    iget-object v0, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->e:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2549316
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2549335
    sget-object v0, LX/3W0;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2549305
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    .line 2549306
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2549307
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 2549308
    new-instance v2, LX/ICJ;

    invoke-direct {v2, p0, p2}, LX/ICJ;-><init>(Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)V

    move-object v2, v2

    .line 2549309
    new-instance v3, LX/ICL;

    invoke-direct {v3, p0, p2}, LX/ICL;-><init>(Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)V

    move-object v3, v3

    .line 2549310
    invoke-static {p2}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2549311
    iget-object v4, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->d:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    sget-object v5, LX/ICQ;->SEEN:LX/ICQ;

    invoke-virtual {v4, v5}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->a(LX/ICQ;)V

    .line 2549312
    :cond_0
    iget-object v4, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    const/4 v5, 0x0

    invoke-interface {p1, v4, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2549313
    new-instance v4, LX/ICM;

    invoke-direct {v4, v0, v1, v2, v3}, LX/ICM;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x48ebc129

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2549299
    check-cast p2, LX/ICM;

    check-cast p4, LX/3W0;

    .line 2549300
    sget-object v1, LX/2mR;->LEFT:LX/2mR;

    iget-object v2, p2, LX/ICM;->b:Ljava/lang/String;

    invoke-virtual {p4, v1, v2}, LX/3W0;->a(LX/2mR;Ljava/lang/CharSequence;)V

    .line 2549301
    sget-object v1, LX/2mR;->LEFT:LX/2mR;

    iget-object v2, p2, LX/ICM;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2}, LX/3W0;->a(LX/2mR;Landroid/view/View$OnClickListener;)V

    .line 2549302
    sget-object v1, LX/2mR;->RIGHT:LX/2mR;

    iget-object v2, p2, LX/ICM;->a:Ljava/lang/String;

    invoke-virtual {p4, v1, v2}, LX/3W0;->a(LX/2mR;Ljava/lang/CharSequence;)V

    .line 2549303
    sget-object v1, LX/2mR;->RIGHT:LX/2mR;

    iget-object v2, p2, LX/ICM;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1, v2}, LX/3W0;->a(LX/2mR;Landroid/view/View$OnClickListener;)V

    .line 2549304
    const/16 v1, 0x1f

    const v2, -0x37d655c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2549294
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2549295
    check-cast p4, LX/3W0;

    const/4 v1, 0x0

    .line 2549296
    sget-object v0, LX/2mR;->LEFT:LX/2mR;

    invoke-virtual {p4, v0, v1}, LX/3W0;->a(LX/2mR;Landroid/view/View$OnClickListener;)V

    .line 2549297
    sget-object v0, LX/2mR;->RIGHT:LX/2mR;

    invoke-virtual {p4, v0, v1}, LX/3W0;->a(LX/2mR;Landroid/view/View$OnClickListener;)V

    .line 2549298
    return-void
.end method
