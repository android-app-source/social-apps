.class public Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/ICV;


# direct methods
.method public constructor <init>(LX/0Or;LX/ICV;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/ICV;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2549463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2549464
    iput-object p1, p0, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;->a:LX/0Or;

    .line 2549465
    iput-object p2, p0, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;->b:LX/ICV;

    .line 2549466
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;
    .locals 5

    .prologue
    .line 2549467
    const-class v1, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;

    monitor-enter v1

    .line 2549468
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2549469
    sput-object v2, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2549470
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2549471
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2549472
    new-instance v4, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2549473
    new-instance v3, LX/ICV;

    invoke-direct {v3}, LX/ICV;-><init>()V

    .line 2549474
    move-object v3, v3

    .line 2549475
    move-object v3, v3

    .line 2549476
    check-cast v3, LX/ICV;

    invoke-direct {v4, p0, v3}, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;-><init>(LX/0Or;LX/ICV;)V

    .line 2549477
    move-object v0, v4

    .line 2549478
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2549479
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2549480
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2549481
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2549482
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2549483
    const-string v1, "story_gallery_survey_actions_type"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2549484
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2549485
    const-string v1, "story_gallery_survey_actions_params_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsParams;

    .line 2549486
    iget-object v1, p0, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;->b:LX/ICV;

    const-class v3, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsServiceHandler;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 2549487
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2549488
    return-object v0

    .line 2549489
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
