.class public Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Zb;

.field private final c:LX/0aG;

.field public final d:LX/03V;

.field private final e:LX/1Ck;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2549426
    const-class v0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0aG;LX/03V;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2549427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2549428
    iput-object p1, p0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->b:LX/0Zb;

    .line 2549429
    iput-object p2, p0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->c:LX/0aG;

    .line 2549430
    iput-object p3, p0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->d:LX/03V;

    .line 2549431
    iput-object p4, p0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->e:LX/1Ck;

    .line 2549432
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;
    .locals 5

    .prologue
    .line 2549433
    new-instance v4, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;-><init>(LX/0Zb;LX/0aG;LX/03V;LX/1Ck;)V

    .line 2549434
    return-object v4
.end method


# virtual methods
.method public final a(LX/ICQ;)V
    .locals 8

    .prologue
    .line 2549435
    new-instance v0, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsParams;

    invoke-virtual {p1}, LX/ICQ;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/feedplugins/storygallerysurvey/service/StoryGallerySurveyActionsParams;-><init>(Ljava/lang/String;)V

    .line 2549436
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2549437
    const-string v1, "story_gallery_survey_actions_params_key"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2549438
    iget-object v6, p0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->e:LX/1Ck;

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->c:LX/0aG;

    const-string v1, "story_gallery_survey_actions_type"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v4, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x7adbbbe6

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    new-instance v1, LX/ICU;

    invoke-direct {v1, p0, p1}, LX/ICU;-><init>(Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;LX/ICQ;)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2549439
    return-void
.end method

.method public final a(LX/ICQ;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)V
    .locals 4

    .prologue
    .line 2549440
    invoke-virtual {p1}, LX/ICQ;->toEventName()Ljava/lang/String;

    move-result-object v0

    .line 2549441
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2549442
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "tracking"

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2549443
    iget-object v1, p0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2549444
    :goto_0
    return-void

    .line 2549445
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->d:LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid user action type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/ICQ;->toEventName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
