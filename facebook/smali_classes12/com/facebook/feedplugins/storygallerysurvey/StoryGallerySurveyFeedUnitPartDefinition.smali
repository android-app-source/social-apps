.class public Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

.field private final b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPrivacyPartDefinition;

.field private final c:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitContentPartDefinition;

.field private final d:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPrivacyPartDefinition;Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitContentPartDefinition;Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2549229
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2549230
    iput-object p1, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;->a:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    .line 2549231
    iput-object p2, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPrivacyPartDefinition;

    .line 2549232
    iput-object p3, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;->c:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitContentPartDefinition;

    .line 2549233
    iput-object p4, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;->d:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;

    .line 2549234
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;
    .locals 7

    .prologue
    .line 2549235
    const-class v1, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;

    monitor-enter v1

    .line 2549236
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2549237
    sput-object v2, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2549238
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2549239
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2549240
    new-instance p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPrivacyPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPrivacyPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPrivacyPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitContentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitContentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;-><init>(Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPrivacyPartDefinition;Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitContentPartDefinition;Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;)V

    .line 2549241
    move-object v0, p0

    .line 2549242
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2549243
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2549244
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2549245
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2549246
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2549247
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2549248
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    .line 2549249
    iget-object v1, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;->d:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2549250
    iget-object v1, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;->c:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitContentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2549251
    iget-object v1, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPrivacyPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2549252
    iget-object v1, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitPartDefinition;->a:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2549253
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2549254
    const/4 v0, 0x1

    return v0
.end method
