.class public Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/2ee;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2549225
    sget-object v0, LX/1Ua;->i:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2549226
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2549227
    iput-object p1, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2549228
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;
    .locals 4

    .prologue
    .line 2549213
    const-class v1, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;

    monitor-enter v1

    .line 2549214
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2549215
    sput-object v2, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2549216
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2549217
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2549218
    new-instance p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2549219
    move-object v0, p0

    .line 2549220
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2549221
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2549222
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2549223
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2549224
    sget-object v0, LX/2ee;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2549210
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2549211
    iget-object v0, p0, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitHeaderPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2549212
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x9194809

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2549204
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/2ee;

    .line 2549205
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2549206
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    .line 2549207
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/2ej;->NOT_SPONSORED:LX/2ej;

    invoke-virtual {p4, v1, v2}, LX/2ee;->a(Ljava/lang/CharSequence;LX/2ej;)V

    .line 2549208
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, LX/2ee;->setMenuButtonActive(Z)V

    .line 2549209
    const/16 v1, 0x1f

    const v2, 0x23de4378

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2549203
    const/4 v0, 0x1

    return v0
.end method
