.class public Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/1vg;

.field public final c:LX/1nu;

.field public final d:LX/7iV;

.field public final e:LX/7j7;

.field public final f:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2701562
    const-class v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "small_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1vg;LX/7iV;LX/7j7;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2701564
    iput-object p1, p0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->c:LX/1nu;

    .line 2701565
    iput-object p2, p0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->b:LX/1vg;

    .line 2701566
    iput-object p3, p0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->d:LX/7iV;

    .line 2701567
    iput-object p4, p0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->e:LX/7j7;

    .line 2701568
    iput-object p5, p0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->f:LX/0ad;

    .line 2701569
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;
    .locals 9

    .prologue
    .line 2701570
    const-class v1, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;

    monitor-enter v1

    .line 2701571
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701572
    sput-object v2, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701573
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701574
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701575
    new-instance v3, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-static {v0}, LX/7iV;->b(LX/0QB;)LX/7iV;

    move-result-object v6

    check-cast v6, LX/7iV;

    invoke-static {v0}, LX/7j7;->b(LX/0QB;)LX/7j7;

    move-result-object v7

    check-cast v7, LX/7j7;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;-><init>(LX/1nu;LX/1vg;LX/7iV;LX/7j7;LX/0ad;)V

    .line 2701576
    move-object v0, v3

    .line 2701577
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701578
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701579
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701580
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
