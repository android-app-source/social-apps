.class public Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/JVo;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JVo;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701619
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2701620
    iput-object p2, p0, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;->e:LX/JVo;

    .line 2701621
    iput-object p3, p0, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;->d:LX/1V0;

    .line 2701622
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2701599
    iget-object v0, p0, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;->e:LX/JVo;

    const/4 v1, 0x0

    .line 2701600
    new-instance v2, LX/JVn;

    invoke-direct {v2, v0}, LX/JVn;-><init>(LX/JVo;)V

    .line 2701601
    iget-object v3, v0, LX/JVo;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JVm;

    .line 2701602
    if-nez v3, :cond_0

    .line 2701603
    new-instance v3, LX/JVm;

    invoke-direct {v3, v0}, LX/JVm;-><init>(LX/JVo;)V

    .line 2701604
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JVm;->a$redex0(LX/JVm;LX/1De;IILX/JVn;)V

    .line 2701605
    move-object v2, v3

    .line 2701606
    move-object v1, v2

    .line 2701607
    move-object v0, v1

    .line 2701608
    iget-object v1, v0, LX/JVm;->a:LX/JVn;

    iput-object p2, v1, LX/JVn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2701609
    iget-object v1, v0, LX/JVm;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2701610
    move-object v0, v0

    .line 2701611
    iget-object v1, v0, LX/JVm;->a:LX/JVn;

    iput-object p3, v1, LX/JVn;->b:LX/1Pn;

    .line 2701612
    iget-object v1, v0, LX/JVm;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2701613
    move-object v0, v0

    .line 2701614
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2701615
    new-instance v1, LX/1X6;

    .line 2701616
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v2

    .line 2701617
    sget-object v3, LX/1Ua;->l:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2701618
    iget-object v2, p0, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;->d:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 2701588
    const-class v1, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;

    monitor-enter v1

    .line 2701589
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701590
    sput-object v2, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701591
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701592
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701593
    new-instance p0, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JVo;->a(LX/0QB;)LX/JVo;

    move-result-object v4

    check-cast v4, LX/JVo;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;-><init>(Landroid/content/Context;LX/JVo;LX/1V0;)V

    .line 2701594
    move-object v0, p0

    .line 2701595
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701596
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701597
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701598
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2701623
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2701587
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/productminilist/ProductMiniListAttachmentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2701583
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2701584
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2701585
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2701586
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2701581
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2701582
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
