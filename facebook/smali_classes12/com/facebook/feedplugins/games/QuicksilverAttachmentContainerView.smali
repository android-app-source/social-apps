.class public Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/video/player/RichVideoPlayer;

.field public c:LX/7NI;

.field public d:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2686081
    const-class v0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2686082
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2686083
    const/4 p1, 0x1

    .line 2686084
    invoke-virtual {p0}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0310d8

    invoke-virtual {v0, v1, p0, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2686085
    const v0, 0x7f0d053b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2686086
    iget-object v0, p0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 2686087
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 2686088
    iget-object v0, p0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2686089
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 2686090
    new-instance v0, LX/7NI;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7NI;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->c:LX/7NI;

    .line 2686091
    iget-object v0, p0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->c:LX/7NI;

    .line 2686092
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 2686093
    iget-object v0, p0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 2686094
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 2686095
    iget-object v0, p0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04G;->INLINE_PLAYER:LX/04G;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 2686096
    iget-object v0, p0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2686097
    return-void
.end method


# virtual methods
.method public final f()V
    .locals 2

    .prologue
    .line 2686098
    iget-object v0, p0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->b:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 2686099
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 2686100
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2686101
    invoke-virtual {p0}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->getPaddingLeft()I

    move-result v1

    sub-int v1, v0, v1

    invoke-virtual {p0}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 2686102
    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->d:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 2686103
    invoke-virtual {p0, v0, v1}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->setMeasuredDimension(II)V

    .line 2686104
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->measureChildren(II)V

    .line 2686105
    return-void
.end method
