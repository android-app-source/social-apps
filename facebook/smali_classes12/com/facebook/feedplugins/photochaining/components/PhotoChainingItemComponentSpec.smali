.class public Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:LX/0Zb;

.field public final d:LX/0gh;

.field private final e:LX/17Q;

.field public final f:LX/1qa;

.field public final g:LX/23R;

.field public final h:LX/17S;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2701007
    const-class v0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;

    const-string v1, "photo_gallery"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/0Zb;LX/0gh;LX/17Q;LX/1qa;LX/23R;LX/17S;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2701009
    iput-object p1, p0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->b:LX/1nu;

    .line 2701010
    iput-object p2, p0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->c:LX/0Zb;

    .line 2701011
    iput-object p3, p0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->d:LX/0gh;

    .line 2701012
    iput-object p4, p0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->e:LX/17Q;

    .line 2701013
    iput-object p5, p0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->f:LX/1qa;

    .line 2701014
    iput-object p6, p0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->g:LX/23R;

    .line 2701015
    iput-object p7, p0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->h:LX/17S;

    .line 2701016
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;
    .locals 11

    .prologue
    .line 2701017
    const-class v1, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;

    monitor-enter v1

    .line 2701018
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701019
    sput-object v2, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701020
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701021
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701022
    new-instance v3, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v6

    check-cast v6, LX/0gh;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v7

    check-cast v7, LX/17Q;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v8

    check-cast v8, LX/1qa;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v9

    check-cast v9, LX/23R;

    invoke-static {v0}, LX/17S;->a(LX/0QB;)LX/17S;

    move-result-object v10

    check-cast v10, LX/17S;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;-><init>(LX/1nu;LX/0Zb;LX/0gh;LX/17Q;LX/1qa;LX/23R;LX/17S;)V

    .line 2701023
    move-object v0, v3

    .line 2701024
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701025
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701026
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701027
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/16 v7, 0x21

    const/4 v6, 0x0

    .line 2701028
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2701029
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->k()I

    move-result v1

    .line 2701030
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->l()I

    move-result v2

    .line 2701031
    if-lez v1, :cond_0

    .line 2701032
    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2701033
    const v3, 0x7f020eae

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2701034
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v3, v6, v6, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2701035
    new-instance v4, Landroid/text/style/ImageSpan;

    invoke-direct {v4, v3, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    const/4 v3, 0x1

    invoke-virtual {v0, v4, v6, v3, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2701036
    const-string v3, " "

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2701037
    :cond_0
    if-lez v2, :cond_1

    .line 2701038
    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2701039
    const v1, 0x7f020eac

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2701040
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v1, v6, v6, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2701041
    new-instance v3, Landroid/text/style/ImageSpan;

    invoke-direct {v3, v1, v6}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v0, v3, v1, v4, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2701042
    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2701043
    :cond_1
    return-object v0
.end method
