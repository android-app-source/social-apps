.class public Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JVU;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2700901
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700902
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2700903
    iput-object p1, p0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;->b:LX/0Ot;

    .line 2700904
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;
    .locals 4

    .prologue
    .line 2700905
    const-class v1, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;

    monitor-enter v1

    .line 2700906
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700907
    sput-object v2, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700908
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700909
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700910
    new-instance v3, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;

    const/16 p0, 0x2098

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;-><init>(LX/0Ot;)V

    .line 2700911
    move-object v0, v3

    .line 2700912
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700913
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700914
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700915
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2700916
    const v0, -0x634da69f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2700917
    check-cast p2, LX/JVV;

    .line 2700918
    iget-object v0, p0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;

    iget-object v1, p2, LX/JVV;->a:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x0

    .line 2700919
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 2700920
    if-nez v3, :cond_1

    .line 2700921
    :cond_0
    :goto_0
    move-object v0, v2

    .line 2700922
    return-object v0

    .line 2700923
    :cond_1
    invoke-static {v3}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2700924
    if-eqz v4, :cond_0

    .line 2700925
    new-instance v2, Landroid/util/SparseArray;

    const/4 v5, 0x1

    invoke-direct {v2, v5}, Landroid/util/SparseArray;-><init>(I)V

    .line 2700926
    const v5, 0x7f0d00f8

    invoke-virtual {v2, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2700927
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Dh;->b(Landroid/util/SparseArray;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b08ec

    invoke-interface {v2, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b08ec

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f020a3d

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    .line 2700928
    iget-object v3, v0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->b:LX/1nu;

    invoke-virtual {v3, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v3

    invoke-static {v4}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    sget-object v5, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    const v5, 0x7f0a00e2

    invoke-virtual {v3, v5}, LX/1nw;->h(I)LX/1nw;

    move-result-object v3

    const v5, 0x7f021448

    invoke-virtual {v3, v5}, LX/1nw;->l(I)LX/1nw;

    move-result-object v3

    sget-object v5, LX/1Up;->e:LX/1Up;

    invoke-virtual {v3, v5}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/16 v5, 0x8

    const/4 v6, 0x2

    invoke-interface {v3, v5, v6}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 2700929
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2700930
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v3

    .line 2700931
    if-eqz v3, :cond_4

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 2700932
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    .line 2700933
    :goto_1
    move-object v6, v3

    .line 2700934
    invoke-static {p1}, LX/0hL;->a(Landroid/content/Context;)Z

    move-result v3

    .line 2700935
    invoke-static {v3}, LX/3rE;->a(Z)LX/3rE;

    move-result-object v7

    .line 2700936
    iget-object p0, v7, LX/3rE;->h:LX/0zr;

    const/4 p2, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    invoke-interface {p0, v6, p2, v0}, LX/0zr;->a(Ljava/lang/CharSequence;II)Z

    move-result p0

    move v7, p0

    .line 2700937
    if-eq v7, v3, :cond_2

    move v3, v4

    .line 2700938
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v5

    const/4 v7, 0x6

    const p0, 0x7f0b08f2

    invoke-interface {v5, v7, p0}, LX/1Dh;->x(II)LX/1Dh;

    move-result-object v5

    const/4 v7, 0x3

    const p0, 0x7f0b08f1

    invoke-interface {v5, v7, p0}, LX/1Dh;->x(II)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b004e

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a00d5

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    if-eqz v3, :cond_3

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    :goto_3
    invoke-virtual {v6, v3}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v1, v6}, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004e

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00d5

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    move-object v3, v3

    .line 2700939
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2700940
    const v3, -0x634da69f

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2700941
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_0

    :cond_2
    move v3, v5

    .line 2700942
    goto/16 :goto_2

    .line 2700943
    :cond_3
    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto :goto_3

    :cond_4
    const-string v3, ""

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2700944
    invoke-static {}, LX/1dS;->b()V

    .line 2700945
    iget v0, p1, LX/1dQ;->b:I

    .line 2700946
    packed-switch v0, :pswitch_data_0

    .line 2700947
    :goto_0
    return-object v2

    .line 2700948
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2700949
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2700950
    check-cast v1, LX/JVV;

    .line 2700951
    iget-object v3, p0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;

    iget-object v4, v1, LX/JVV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, v1, LX/JVV;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v6, v1, LX/JVV;->c:LX/3mj;

    .line 2700952
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 2700953
    iget-object v7, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v7

    .line 2700954
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v7}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object p2

    check-cast p2, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 2700955
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    .line 2700956
    iget-object v9, v3, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->d:LX/0gh;

    const-string p1, "tap_photo"

    invoke-virtual {v9, p1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v9

    sget-object p1, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p1}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object p1

    const/4 p0, 0x1

    invoke-virtual {v9, p1, p0}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 2700957
    invoke-static {v7}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v7

    .line 2700958
    iget-object v9, v3, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->c:LX/0Zb;

    invoke-static {v7}, LX/17Q;->B(LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v9, v7}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2700959
    invoke-static {v5}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v7

    invoke-static {v7}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 2700960
    invoke-static {v7}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object p1

    move-object v7, v3

    move-object v9, v5

    move-object p0, v6

    move-object v1, v0

    .line 2700961
    const/4 v0, 0x0

    .line 2700962
    invoke-static {v9}, LX/17S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v7, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->h:LX/17S;

    sget-object v4, LX/99r;->Photo:LX/99r;

    invoke-virtual {v3, v4}, LX/17S;->a(LX/99r;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2700963
    const/4 v4, 0x0

    invoke-static {v4, v0}, LX/17S;->a(ZLjava/lang/String;)Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    move-result-object v3

    .line 2700964
    :goto_1
    invoke-static {v9}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 2700965
    if-eqz p2, :cond_0

    instance-of v5, p2, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v5, :cond_0

    check-cast p2, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 2700966
    :goto_2
    if-eqz p2, :cond_1

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2700967
    :goto_3
    invoke-static {p2}, LX/9hF;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/9hE;

    move-result-object v5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v5

    sget-object v6, LX/74S;->NEWSFEED:LX/74S;

    invoke-virtual {v5, v6}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/9hD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    .line 2700968
    new-instance v5, LX/JVW;

    invoke-direct {v5, p0}, LX/JVW;-><init>(LX/3mj;)V

    move-object v5, v5

    .line 2700969
    invoke-virtual {v0, v5}, LX/9hD;->a(LX/8Hh;)LX/9hD;

    move-result-object v0

    .line 2700970
    iput-object v3, v0, LX/9hD;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    .line 2700971
    move-object v3, v0

    .line 2700972
    invoke-virtual {v3}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v3

    .line 2700973
    iget-object v0, v7, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->g:LX/23R;

    invoke-static {v4}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2700974
    if-nez v4, :cond_3

    .line 2700975
    const/4 v5, 0x0

    .line 2700976
    :goto_4
    move-object v4, v5

    .line 2700977
    invoke-interface {v0, v8, v3, v4}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2700978
    goto/16 :goto_0

    :cond_0
    move-object p2, v0

    .line 2700979
    goto :goto_2

    .line 2700980
    :cond_1
    invoke-static {v9}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_3

    :cond_2
    move-object v3, v0

    goto :goto_1

    :cond_3
    new-instance v5, LX/JVX;

    iget-object v6, v7, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->f:LX/1qa;

    invoke-direct {v5, v6, v1}, LX/JVX;-><init>(LX/1qa;Landroid/view/View;)V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch -0x634da69f
        :pswitch_0
    .end packed-switch
.end method
