.class public Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;
.super LX/AnF;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final e:LX/2eR;

.field private static final f:Lcom/facebook/common/callercontext/CallerContext;

.field private static o:LX/0Xm;


# instance fields
.field private final g:LX/1qa;

.field public final h:LX/0Zb;

.field private final i:LX/0gh;

.field private final j:LX/17Q;

.field private final k:LX/23R;

.field private final l:LX/9hF;

.field private final m:LX/17S;

.field private final n:LX/1Ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2701179
    new-instance v0, LX/JVZ;

    invoke-direct {v0}, LX/JVZ;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->e:LX/2eR;

    .line 2701180
    const-class v0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;

    const-string v1, "photo_gallery"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1qa;LX/0hB;LX/0Zb;LX/0gh;LX/17Q;LX/1DR;LX/23R;LX/9hF;LX/17S;LX/1Ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701212
    invoke-direct {p0, p1, p3, p7}, LX/AnF;-><init>(Landroid/content/Context;LX/0hB;LX/1DR;)V

    .line 2701213
    iput-object p2, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->g:LX/1qa;

    .line 2701214
    iput-object p4, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->h:LX/0Zb;

    .line 2701215
    iput-object p5, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->i:LX/0gh;

    .line 2701216
    iput-object p6, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->j:LX/17Q;

    .line 2701217
    iput-object p8, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->k:LX/23R;

    .line 2701218
    iput-object p9, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->l:LX/9hF;

    .line 2701219
    iput-object p10, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->m:LX/17S;

    .line 2701220
    iput-object p11, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->n:LX/1Ad;

    .line 2701221
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;
    .locals 15

    .prologue
    .line 2701201
    const-class v1, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;

    monitor-enter v1

    .line 2701202
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701203
    sput-object v2, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701204
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701205
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701206
    new-instance v3, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v5

    check-cast v5, LX/1qa;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v6

    check-cast v6, LX/0hB;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v8

    check-cast v8, LX/0gh;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v9

    check-cast v9, LX/17Q;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v10

    check-cast v10, LX/1DR;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v11

    check-cast v11, LX/23R;

    invoke-static {v0}, LX/9hF;->a(LX/0QB;)LX/9hF;

    move-result-object v12

    check-cast v12, LX/9hF;

    invoke-static {v0}, LX/17S;->a(LX/0QB;)LX/17S;

    move-result-object v13

    check-cast v13, LX/17S;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v14

    check-cast v14, LX/1Ad;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;-><init>(Landroid/content/Context;LX/1qa;LX/0hB;LX/0Zb;LX/0gh;LX/17Q;LX/1DR;LX/23R;LX/9hF;LX/17S;LX/1Ad;)V

    .line 2701207
    move-object v0, v3

    .line 2701208
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701209
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701210
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701211
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 2

    .prologue
    .line 2701198
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2701199
    invoke-interface {p0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2701200
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/widget/ImageView;LX/1bf;Lcom/facebook/widget/CustomViewPager;ZLcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 2701181
    new-instance v3, LX/9iT;

    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {v3, v0, p4}, LX/9iT;-><init>(Landroid/view/Window;Lcom/facebook/widget/CustomViewPager;)V

    .line 2701182
    invoke-static {p1}, LX/17S;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->m:LX/17S;

    sget-object v2, LX/99r;->Photo:LX/99r;

    invoke-virtual {v0, v2}, LX/17S;->a(LX/99r;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2701183
    invoke-static {v6, v1}, LX/17S;->a(ZLjava/lang/String;)Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    move-result-object v0

    .line 2701184
    :goto_0
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 2701185
    if-eqz p6, :cond_0

    instance-of v4, p6, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v4, :cond_0

    check-cast p6, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 2701186
    :goto_1
    if-eqz p6, :cond_1

    invoke-static {p6}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2701187
    :goto_2
    invoke-static {p6}, LX/9hF;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/9hE;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v4

    sget-object v5, LX/74S;->NEWSFEED:LX/74S;

    invoke-virtual {v4, v5}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/9hD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/9hD;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/9hD;->a(LX/8Hh;)LX/9hD;

    move-result-object v1

    .line 2701188
    iput-boolean p5, v1, LX/9hD;->o:Z

    .line 2701189
    move-object v1, v1

    .line 2701190
    iput-object v0, v1, LX/9hD;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    .line 2701191
    move-object v0, v1

    .line 2701192
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v7

    .line 2701193
    new-instance v0, LX/JVb;

    iget-object v5, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->g:LX/1qa;

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, LX/JVb;-><init>(Landroid/widget/ImageView;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1bf;Lcom/facebook/widget/CustomViewPager;LX/1qa;B)V

    .line 2701194
    iget-object v1, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->k:LX/23R;

    iget-object v2, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v7, v0}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2701195
    return-void

    :cond_0
    move-object p6, v1

    .line 2701196
    goto :goto_1

    .line 2701197
    :cond_1
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 2701173
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2701174
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-static {v0}, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2701175
    if-eqz v0, :cond_0

    .line 2701176
    invoke-virtual {p1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2701177
    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 2701178
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()LX/2eR;
    .locals 1

    .prologue
    .line 2701172
    sget-object v0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->e:LX/2eR;

    return-object v0
.end method

.method public final a(LX/AnE;Landroid/support/v4/view/ViewPager;Landroid/view/View;Ljava/lang/Object;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 7

    .prologue
    .line 2701222
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2701223
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, p2

    .line 2701224
    check-cast v4, Lcom/facebook/widget/CustomViewPager;

    .line 2701225
    check-cast p3, LX/JVe;

    move-object v1, p4

    .line 2701226
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2701227
    if-eqz p5, :cond_0

    instance-of v0, p5, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_0

    move-object v0, p5

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 2701228
    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2701229
    :goto_1
    iget-object v2, p3, LX/JVe;->a:LX/JVd;

    move-object v2, v2

    .line 2701230
    iget-object v2, v2, LX/JVd;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2701231
    iget-object v3, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->i:LX/0gh;

    const-string v5, "tap_photo"

    invoke-virtual {v3, v5}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v3

    sget-object v5, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v6}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 2701232
    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 2701233
    iget-object v3, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->h:LX/0Zb;

    invoke-static {v0}, LX/17Q;->B(LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v3, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2701234
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2701235
    invoke-static {v0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v3

    .line 2701236
    const/4 v5, 0x0

    move-object v0, p0

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/widget/ImageView;LX/1bf;Lcom/facebook/widget/CustomViewPager;ZLcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    .line 2701237
    return-void

    .line 2701238
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2701239
    :cond_1
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;LX/99X;LX/AnC;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2701151
    move-object v6, p1

    check-cast v6, LX/JVe;

    move-object v4, p2

    .line 2701152
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2701153
    iget-object v0, v6, LX/JVe;->a:LX/JVd;

    move-object v0, v0

    .line 2701154
    iget-object v7, v0, LX/JVd;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2701155
    invoke-static {v4}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2701156
    if-eqz v4, :cond_0

    invoke-static {v4}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2701157
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2701158
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2701159
    :goto_0
    return-void

    .line 2701160
    :cond_1
    invoke-virtual {p0, p1, p3}, LX/AnF;->a(Landroid/view/View;LX/99X;)V

    .line 2701161
    invoke-static {v4}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2701162
    iget-object v1, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->n:LX/1Ad;

    sget-object v2, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->p()LX/1Ad;

    move-result-object v1

    invoke-static {v0}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2701163
    invoke-virtual {v7, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2701164
    new-instance v0, LX/JVa;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/JVa;-><init>(Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;LX/AnC;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    invoke-virtual {v7, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2701165
    invoke-virtual {v7, v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2701166
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    .line 2701167
    if-eqz v0, :cond_2

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 2701168
    iget-object v1, v6, LX/JVe;->a:LX/JVd;

    move-object v1, v1

    .line 2701169
    iget-object v1, v1, LX/JVd;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2701170
    :cond_2
    iget-object v0, v6, LX/JVe;->a:LX/JVd;

    move-object v0, v0

    .line 2701171
    iget-object v0, v0, LX/JVd;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6}, LX/JVe;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/widget/TextView;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;",
            "Landroid/widget/TextView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2701134
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2701135
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2701136
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    const v2, 0x7f0f0069

    invoke-static {v1, v0, p2, v2}, LX/AnB;->a(Landroid/content/res/Resources;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;Landroid/widget/TextView;I)V

    .line 2701137
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2701138
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-static {v0}, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2701139
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/18M;->c(Lcom/facebook/graphql/model/Sponsorable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2701140
    invoke-virtual {p1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2701141
    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 2701142
    invoke-static {v1}, LX/17Q;->F(LX/0lF;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2701143
    const/4 v2, 0x0

    .line 2701144
    :goto_0
    move-object v1, v2

    .line 2701145
    iget-object v2, p0, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->h:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2701146
    invoke-static {v0}, LX/18M;->b(Lcom/facebook/graphql/model/Sponsorable;)V

    .line 2701147
    :cond_0
    return-void

    :cond_1
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "chained_story_title_impression"

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {v2, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string p1, "native_newsfeed"

    .line 2701148
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2701149
    move-object v2, v2

    .line 2701150
    goto :goto_0
.end method

.method public final c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2701127
    const-class v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    return-object v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2701133
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b08ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2701132
    invoke-virtual {p0}, Lcom/facebook/feedplugins/photochaining/controllers/PhotoChainingViewController;->f()I

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 2701130
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2701131
    const v1, 0x7f0b08ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 2701128
    iget-object v0, p0, LX/AnF;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2701129
    const v1, 0x7f0b08f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method
