.class public Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFindFriendsFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687513
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2687514
    iput-object p1, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;->a:Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    .line 2687515
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;
    .locals 4

    .prologue
    .line 2687516
    const-class v1, Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;

    monitor-enter v1

    .line 2687517
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687518
    sput-object v2, Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687519
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687520
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687521
    new-instance p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;)V

    .line 2687522
    move-object v0, p0

    .line 2687523
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687524
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687525
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687526
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2687527
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2687528
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2687529
    check-cast v0, Lcom/facebook/graphql/model/GraphQLFindFriendsFeedUnit;

    .line 2687530
    iget-object v1, p0, Lcom/facebook/feedplugins/growth/rows/FindFriendsGroupPartDefinition;->a:Lcom/facebook/feedplugins/growth/rows/FindFriendsPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2687531
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2687532
    const/4 v0, 0x1

    return v0
.end method
