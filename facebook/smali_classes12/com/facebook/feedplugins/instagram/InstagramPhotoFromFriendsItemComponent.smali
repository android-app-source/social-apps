.class public Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JPx;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2690770
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2690771
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2690772
    iput-object p1, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;->b:LX/0Ot;

    .line 2690773
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;
    .locals 4

    .prologue
    .line 2690774
    const-class v1, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;

    monitor-enter v1

    .line 2690775
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2690776
    sput-object v2, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2690777
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2690778
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2690779
    new-instance v3, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;

    const/16 p0, 0x1fef

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;-><init>(LX/0Ot;)V

    .line 2690780
    move-object v0, v3

    .line 2690781
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2690782
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2690783
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2690784
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(Landroid/view/View;LX/1X1;)V
    .locals 8

    .prologue
    .line 2690785
    check-cast p2, LX/JPy;

    .line 2690786
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;

    iget-object v2, p2, LX/JPy;->b:LX/4ZX;

    iget-object v3, p2, LX/JPy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/JPy;->c:LX/1Pm;

    iget-object v5, p2, LX/JPy;->f:LX/3mj;

    iget-object v6, p2, LX/JPy;->d:LX/JQ1;

    iget-object v7, p2, LX/JPy;->e:Ljava/lang/Boolean;

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->a(Landroid/view/View;LX/4ZX;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;LX/3mj;LX/JQ1;Ljava/lang/Boolean;)V

    .line 2690787
    return-void
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2690788
    const v0, -0x7267fe0d

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2690789
    const v0, 0x4ca1847e    # 8.4681712E7f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2690790
    check-cast p2, LX/JPy;

    .line 2690791
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 2690792
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 2690793
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;

    iget-object v2, p2, LX/JPy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/JPy;->b:LX/4ZX;

    iget-object v4, p2, LX/JPy;->c:LX/1Pm;

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/4ZX;LX/1Pm;LX/1np;LX/1np;)LX/1Dg;

    move-result-object v1

    .line 2690794
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2690795
    check-cast v0, LX/JQ1;

    iput-object v0, p2, LX/JPy;->d:LX/JQ1;

    .line 2690796
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 2690797
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2690798
    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p2, LX/JPy;->e:Ljava/lang/Boolean;

    .line 2690799
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 2690800
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2690801
    invoke-static {}, LX/1dS;->b()V

    .line 2690802
    iget v0, p1, LX/1dQ;->b:I

    .line 2690803
    sparse-switch v0, :sswitch_data_0

    .line 2690804
    :goto_0
    return-object v2

    .line 2690805
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2690806
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2690807
    check-cast v1, LX/JPy;

    .line 2690808
    iget-object v3, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;

    iget-object v4, v1, LX/JPy;->b:LX/4ZX;

    const/4 v1, 0x1

    const/4 p1, 0x0

    .line 2690809
    iget-object v5, v4, LX/4ZX;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object p2

    .line 2690810
    if-nez p2, :cond_0

    .line 2690811
    :goto_1
    goto :goto_0

    .line 2690812
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2690813
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;->b(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 2690814
    :cond_0
    iget-object v5, v3, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->h:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0gh;

    const-string v6, "tap_photo"

    invoke-virtual {v5, v6}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v5

    sget-object v6, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 2690815
    invoke-virtual {v4}, LX/4ZX;->a()LX/162;

    move-result-object v5

    .line 2690816
    if-eqz v5, :cond_1

    invoke-virtual {v5}, LX/162;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v6, v5

    .line 2690817
    :goto_2
    iget-object v5, v3, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/17S;

    sget-object p0, LX/99r;->Photo:LX/99r;

    invoke-virtual {v5, p0}, LX/17S;->a(LX/99r;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, v3, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->f:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/17S;

    invoke-static {v1, v6}, LX/17S;->a(ZLjava/lang/String;)Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    move-result-object v5

    .line 2690818
    :goto_3
    invoke-static {p2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    invoke-static {p0}, LX/9hF;->f(LX/0Px;)LX/9hE;

    move-result-object p0

    invoke-virtual {p0, p2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object p2

    .line 2690819
    iput-object v6, p2, LX/9hD;->j:Ljava/lang/String;

    .line 2690820
    move-object v6, p2

    .line 2690821
    sget-object p2, LX/74S;->NEWSFEED:LX/74S;

    invoke-virtual {v6, p2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v6

    .line 2690822
    iput-object v5, v6, LX/9hD;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    .line 2690823
    move-object v5, v6

    .line 2690824
    invoke-virtual {v5}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v6

    .line 2690825
    iget-object v5, v3, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/23R;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-interface {v5, p2, v6, p1}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    goto :goto_1

    :cond_1
    move-object v6, p1

    .line 2690826
    goto :goto_2

    :cond_2
    move-object v5, p1

    .line 2690827
    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7267fe0d -> :sswitch_0
        0x4ca1847e -> :sswitch_1
    .end sparse-switch
.end method
