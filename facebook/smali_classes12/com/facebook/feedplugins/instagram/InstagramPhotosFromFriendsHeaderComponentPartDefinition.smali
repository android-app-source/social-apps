.class public Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/JQF;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JQF;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691282
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2691283
    iput-object p2, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;->d:LX/JQF;

    .line 2691284
    iput-object p3, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;->e:LX/1V0;

    .line 2691285
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2691269
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;->e:LX/1V0;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->i:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    iget-object v2, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;->d:LX/JQF;

    const/4 v3, 0x0

    .line 2691270
    new-instance v4, LX/JQE;

    invoke-direct {v4, v2}, LX/JQE;-><init>(LX/JQF;)V

    .line 2691271
    sget-object p0, LX/JQF;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JQD;

    .line 2691272
    if-nez p0, :cond_0

    .line 2691273
    new-instance p0, LX/JQD;

    invoke-direct {p0}, LX/JQD;-><init>()V

    .line 2691274
    :cond_0
    invoke-static {p0, p1, v3, v3, v4}, LX/JQD;->a$redex0(LX/JQD;LX/1De;IILX/JQE;)V

    .line 2691275
    move-object v4, p0

    .line 2691276
    move-object v3, v4

    .line 2691277
    move-object v2, v3

    .line 2691278
    iget-object v3, v2, LX/JQD;->a:LX/JQE;

    iput-object p2, v3, LX/JQE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691279
    iget-object v3, v2, LX/JQD;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2691280
    move-object v2, v2

    .line 2691281
    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    invoke-virtual {v0, p1, p3, v1, v2}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;
    .locals 6

    .prologue
    .line 2691286
    const-class v1, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2691287
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691288
    sput-object v2, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691289
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691290
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691291
    new-instance p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JQF;->a(LX/0QB;)LX/JQF;

    move-result-object v4

    check-cast v4, LX/JQF;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/JQF;LX/1V0;)V

    .line 2691292
    move-object v0, p0

    .line 2691293
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691294
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691295
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691296
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2691268
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2691267
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2691266
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2691263
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691264
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2691265
    check-cast v0, LX/0jW;

    return-object v0
.end method
