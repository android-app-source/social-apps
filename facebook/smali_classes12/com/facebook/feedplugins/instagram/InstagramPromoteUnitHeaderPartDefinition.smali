.class public Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
        "LX/1Ps;",
        "LX/3YF;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/17S;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17S;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691457
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2691458
    iput-object p1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->a:Landroid/content/Context;

    .line 2691459
    iput-object p2, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->b:LX/17S;

    .line 2691460
    iput-object p3, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2691461
    iput-object p4, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2691462
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;
    .locals 7

    .prologue
    .line 2691387
    const-class v1, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;

    monitor-enter v1

    .line 2691388
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691389
    sput-object v2, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691390
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691391
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691392
    new-instance p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/17S;->a(LX/0QB;)LX/17S;

    move-result-object v4

    check-cast v4, LX/17S;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;-><init>(Landroid/content/Context;LX/17S;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2691393
    move-object v0, p0

    .line 2691394
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691395
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691396
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691397
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2691398
    sget-object v0, LX/3YF;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2691399
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2691400
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2691401
    check-cast v0, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    .line 2691402
    iget-object v1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->b:LX/17S;

    sget-object v2, LX/99r;->Ego:LX/99r;

    invoke-virtual {v1, v2}, LX/17S;->b(LX/99r;)V

    .line 2691403
    iget-object v1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->i:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691404
    iget-object v1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->b:LX/17S;

    invoke-virtual {v1}, LX/17S;->j()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2691405
    iget-object v1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->b:LX/17S;

    invoke-virtual {v1}, LX/17S;->d()I

    move-result v1

    .line 2691406
    if-lez v1, :cond_0

    .line 2691407
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0810e2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2691408
    :goto_0
    const v1, 0x7f0d15bc

    iget-object v2, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2691409
    const/4 v0, 0x0

    .line 2691410
    :goto_1
    return-object v0

    .line 2691411
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2691412
    iget-object v1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0810de

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2691413
    :cond_1
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2691414
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->n()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    if-lez v4, :cond_5

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;

    .line 2691415
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->a()Ljava/lang/String;

    move-result-object v1

    .line 2691416
    :goto_2
    const/4 v6, 0x0

    .line 2691417
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->n()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result p1

    move v7, v6

    :goto_3
    if-ge v7, p1, :cond_8

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;

    .line 2691418
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnitItem;->j()LX/0Px;

    move-result-object p2

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result p3

    move v5, v6

    :goto_4
    if-ge v5, p3, :cond_7

    invoke-virtual {p2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2691419
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->at()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    .line 2691420
    if-eqz v4, :cond_6

    .line 2691421
    :goto_5
    move-object v4, v4

    .line 2691422
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2691423
    invoke-static {v4}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/1y5;

    move-result-object v6

    .line 2691424
    if-nez v6, :cond_9

    .line 2691425
    const/4 v7, 0x0

    .line 2691426
    :goto_6
    move-object v6, v7

    .line 2691427
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v4

    .line 2691428
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    move v1, v3

    .line 2691429
    :goto_7
    if-gtz v1, :cond_4

    .line 2691430
    iget-object v1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0810df

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v7, v8

    const/4 v8, 0x1

    .line 2691431
    const-string p1, "Instagram"

    move-object p1, p1

    .line 2691432
    aput-object p1, v7, v8

    invoke-virtual {v1, v3, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2691433
    :goto_8
    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 2691434
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    .line 2691435
    :try_start_0
    new-instance v8, LX/1yN;

    invoke-direct {v8, v3, v7}, LX/1yN;-><init>(II)V

    invoke-static {v1, v8}, LX/1yM;->a(Ljava/lang/String;LX/1yN;)LX/1yL;

    move-result-object v3

    invoke-static {v6, v3}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntity;LX/1yL;)Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 2691436
    :goto_9
    const-string v3, "Instagram"

    move-object v3, v3

    .line 2691437
    if-eq v3, v4, :cond_2

    .line 2691438
    const-string v4, "162454007121996"

    move-object v4, v4

    .line 2691439
    new-instance v6, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v7, 0x25d6af

    invoke-direct {v6, v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "page/"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v6, v7}, LX/16z;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v4

    move-object v4, v4

    .line 2691440
    :try_start_1
    new-instance v6, LX/1yN;

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v6, v7, v3}, LX/1yN;-><init>(II)V

    invoke-static {v1, v6}, LX/1yM;->a(Ljava/lang/String;LX/1yN;)LX/1yL;

    move-result-object v3

    invoke-static {v4, v3}, LX/16z;->a(Lcom/facebook/graphql/model/GraphQLEntity;LX/1yL;)Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch LX/47A; {:try_start_1 .. :try_end_1} :catch_1

    .line 2691441
    :cond_2
    :goto_a
    invoke-static {v1, v5, v2, v2}, LX/16z;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    move-object v0, v1

    .line 2691442
    goto/16 :goto_1

    .line 2691443
    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    goto/16 :goto_7

    .line 2691444
    :cond_4
    iget-object v3, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v7, 0x7f0810e1

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v4, v8, p1

    const/4 p1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v8, p1

    const/4 p1, 0x2

    .line 2691445
    const-string p2, "Instagram"

    move-object p2, p2

    .line 2691446
    aput-object p2, v8, p1

    invoke-virtual {v3, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 2691447
    goto/16 :goto_8

    .line 2691448
    :catch_0
    move-exception v3

    .line 2691449
    const-string v6, "InstagramPromoteUnitHeaderPartDefinition"

    invoke-virtual {v3}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_9

    .line 2691450
    :catch_1
    move-exception v3

    .line 2691451
    const-string v4, "InstagramPromoteUnitHeaderPartDefinition"

    invoke-virtual {v3}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_a

    :cond_5
    move-object v1, v2

    goto/16 :goto_2

    .line 2691452
    :cond_6
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_4

    .line 2691453
    :cond_7
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto/16 :goto_3

    .line 2691454
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 2691455
    :cond_9
    invoke-interface {v6}, LX/1y5;->e()Ljava/lang/String;

    move-result-object v7

    .line 2691456
    invoke-interface {v6}, LX/1y5;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "profile/"

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v7, v8, p1}, LX/16z;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v7

    goto/16 :goto_6
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xf7cbc67

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2691375
    check-cast p2, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    check-cast p4, LX/3YF;

    .line 2691376
    if-nez p2, :cond_1

    .line 2691377
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x1ecca689

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2691378
    :cond_1
    :try_start_0
    iget-object v1, p4, LX/3YF;->b:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setTextWithEntities(LX/175;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 2691379
    :goto_1
    iget-object v1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->b:LX/17S;

    .line 2691380
    sget v2, LX/1TU;->k:I

    invoke-static {v1, v2}, LX/17S;->a(LX/17S;I)Z

    move-result v2

    move v1, v2

    .line 2691381
    if-eqz v1, :cond_0

    .line 2691382
    iget-object v1, p4, LX/3YF;->c:Landroid/widget/ImageView;

    const v2, 0x7f020d77

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2691383
    goto :goto_0

    .line 2691384
    :catch_0
    move-exception v1

    .line 2691385
    const-string v2, "InstagramPromoteUnitHeaderView"

    invoke-virtual {v1}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2691386
    const/4 v0, 0x1

    return v0
.end method
