.class public Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691592
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2691593
    iput-object p1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;->a:Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;

    .line 2691594
    iput-object p2, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;->b:Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;

    .line 2691595
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;
    .locals 5

    .prologue
    .line 2691596
    const-class v1, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;

    monitor-enter v1

    .line 2691597
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691598
    sput-object v2, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691599
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691600
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691601
    new-instance p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;-><init>(Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;)V

    .line 2691602
    move-object v0, p0

    .line 2691603
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691604
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691605
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691606
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2691607
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691608
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;->a:Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2691609
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitPartDefinition;->b:Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2691610
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2691611
    const/4 v0, 0x1

    return v0
.end method
