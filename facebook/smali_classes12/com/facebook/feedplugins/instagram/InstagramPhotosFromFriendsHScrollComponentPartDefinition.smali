.class public Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/JQA;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JQA;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691155
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2691156
    iput-object p2, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;->d:LX/JQA;

    .line 2691157
    iput-object p3, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;->e:LX/1V0;

    .line 2691158
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2691159
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;->d:LX/JQA;

    const/4 v1, 0x0

    .line 2691160
    new-instance v2, LX/JQ9;

    invoke-direct {v2, v0}, LX/JQ9;-><init>(LX/JQA;)V

    .line 2691161
    sget-object v3, LX/JQA;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JQ8;

    .line 2691162
    if-nez v3, :cond_0

    .line 2691163
    new-instance v3, LX/JQ8;

    invoke-direct {v3}, LX/JQ8;-><init>()V

    .line 2691164
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/JQ8;->a$redex0(LX/JQ8;LX/1De;IILX/JQ9;)V

    .line 2691165
    move-object v2, v3

    .line 2691166
    move-object v1, v2

    .line 2691167
    move-object v0, v1

    .line 2691168
    iget-object v1, v0, LX/JQ8;->a:LX/JQ9;

    iput-object p3, v1, LX/JQ9;->a:LX/1Pm;

    .line 2691169
    iget-object v1, v0, LX/JQ8;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2691170
    move-object v0, v0

    .line 2691171
    iget-object v1, v0, LX/JQ8;->a:LX/JQ9;

    iput-object p2, v1, LX/JQ9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691172
    iget-object v1, v0, LX/JQ8;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2691173
    move-object v0, v0

    .line 2691174
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2691175
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2691176
    iget-object v2, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;->e:LX/1V0;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;
    .locals 6

    .prologue
    .line 2691143
    const-class v1, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;

    monitor-enter v1

    .line 2691144
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691145
    sput-object v2, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691146
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691147
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691148
    new-instance p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JQA;->a(LX/0QB;)LX/JQA;

    move-result-object v4

    check-cast v4, LX/JQA;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;-><init>(Landroid/content/Context;LX/JQA;LX/1V0;)V

    .line 2691149
    move-object v0, p0

    .line 2691150
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691151
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691152
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691153
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2691154
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2691142
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2691141
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2691138
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691139
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2691140
    check-cast v0, LX/0jW;

    return-object v0
.end method
