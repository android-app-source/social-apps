.class public Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "LX/JQL;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;

.field private final e:Landroid/content/Context;

.field private final f:LX/1DR;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;Landroid/content/Context;LX/1DR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691341
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2691342
    iput-object p1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2691343
    iput-object p2, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2691344
    iput-object p3, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    .line 2691345
    iput-object p4, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->d:Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;

    .line 2691346
    iput-object p5, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->e:Landroid/content/Context;

    .line 2691347
    iput-object p6, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->f:LX/1DR;

    .line 2691348
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;
    .locals 10

    .prologue
    .line 2691349
    const-class v1, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;

    monitor-enter v1

    .line 2691350
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691351
    sput-object v2, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691352
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691353
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691354
    new-instance v3, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v9

    check-cast v9, LX/1DR;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;Landroid/content/Context;LX/1DR;)V

    .line 2691355
    move-object v0, v3

    .line 2691356
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691357
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691358
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691359
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2691360
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2691361
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x1

    .line 2691362
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2691363
    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    .line 2691364
    new-instance v3, LX/2dx;

    invoke-direct {v3}, LX/2dx;-><init>()V

    .line 2691365
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/2eF;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691366
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->b:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSwitcherPartDefinition;

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691367
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->f:LX/1DR;

    invoke-virtual {v0}, LX/1DR;->a()I

    move-result v0

    .line 2691368
    iget-object v1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->e:Landroid/content/Context;

    int-to-float v0, v0

    invoke-static {v1, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    .line 2691369
    iget-object v7, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    invoke-static {v1, v4, v4}, LX/2eF;->a(IZZ)LX/2eF;

    move-result-object v1

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->I_()I

    move-result v2

    .line 2691370
    invoke-static {v6}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;)LX/0Px;

    move-result-object v4

    .line 2691371
    new-instance v5, LX/JQI;

    invoke-direct {v5, p0, v4, v3, v6}, LX/JQI;-><init>(Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;Ljava/util/List;LX/2dx;Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;)V

    move-object v3, v5

    .line 2691372
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/JQH;

    invoke-direct {v5, p0, v6}, LX/JQH;-><init>(Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;)V

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v7, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2691373
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2691374
    const/4 v0, 0x1

    return v0
.end method
