.class public Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/JQ4;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JQ4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691001
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2691002
    iput-object p2, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;->d:LX/JQ4;

    .line 2691003
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
            ">;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2690988
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;->d:LX/JQ4;

    const/4 v1, 0x0

    .line 2690989
    new-instance v2, LX/JQ3;

    invoke-direct {v2, v0}, LX/JQ3;-><init>(LX/JQ4;)V

    .line 2690990
    sget-object p0, LX/JQ4;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JQ2;

    .line 2690991
    if-nez p0, :cond_0

    .line 2690992
    new-instance p0, LX/JQ2;

    invoke-direct {p0}, LX/JQ2;-><init>()V

    .line 2690993
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/JQ2;->a$redex0(LX/JQ2;LX/1De;IILX/JQ3;)V

    .line 2690994
    move-object v2, p0

    .line 2690995
    move-object v1, v2

    .line 2690996
    move-object v0, v1

    .line 2690997
    iget-object v1, v0, LX/JQ2;->a:LX/JQ3;

    iput-object p2, v1, LX/JQ3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2690998
    iget-object v1, v0, LX/JQ2;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2690999
    move-object v0, v0

    .line 2691000
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;
    .locals 5

    .prologue
    .line 2690971
    const-class v1, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;

    monitor-enter v1

    .line 2690972
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2690973
    sput-object v2, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2690974
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2690975
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2690976
    new-instance p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/JQ4;->a(LX/0QB;)LX/JQ4;

    move-result-object v4

    check-cast v4, LX/JQ4;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;-><init>(Landroid/content/Context;LX/JQ4;)V

    .line 2690977
    move-object v0, p0

    .line 2690978
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2690979
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2690980
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2690981
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2690987
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2690986
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2690985
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2690982
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2690983
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2690984
    check-cast v0, LX/0jW;

    return-object v0
.end method
