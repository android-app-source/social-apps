.class public final Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JQL;

.field public final synthetic b:Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;LX/JQL;)V
    .locals 0

    .prologue
    .line 2691463
    iput-object p1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition$1;->b:Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;

    iput-object p2, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition$1;->a:LX/JQL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x3a696803

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2691464
    check-cast p1, Lcom/facebook/drawee/view/DraweeView;

    .line 2691465
    iget-object v1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition$1;->b:Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;

    iget-object v2, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition$1;->a:LX/JQL;

    iget-object v2, v2, LX/JQL;->a:LX/4ZX;

    .line 2691466
    const/4 v5, 0x0

    .line 2691467
    iget-object v4, v2, LX/4ZX;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object p0

    .line 2691468
    iget-object v4, v1, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->d:LX/0gh;

    const-string v6, "tap_photo"

    invoke-virtual {v4, v6}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v4

    sget-object v6, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v6

    const/4 p1, 0x1

    invoke-virtual {v4, v6, p1}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 2691469
    invoke-virtual {v2}, LX/4ZX;->a()LX/162;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, LX/4ZX;->a()LX/162;

    move-result-object v4

    invoke-virtual {v4}, LX/162;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2691470
    :goto_0
    iget-object v6, v1, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->c:LX/17S;

    sget-object p1, LX/99r;->Photo:LX/99r;

    invoke-virtual {v6, p1}, LX/17S;->a(LX/99r;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 p1, 0x1

    invoke-static {p1, v4}, LX/17S;->a(ZLjava/lang/String;)Lcom/facebook/feed/thirdparty/instagram/InstagramGalleryDeepLinkBinder$InstagramDeepLinkBinderConfig;

    move-result-object v6

    .line 2691471
    :goto_1
    invoke-static {p0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    invoke-static {p1}, LX/9hF;->f(LX/0Px;)LX/9hE;

    move-result-object p1

    invoke-virtual {p1, p0}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object p0

    .line 2691472
    iput-object v4, p0, LX/9hD;->j:Ljava/lang/String;

    .line 2691473
    move-object v4, p0

    .line 2691474
    sget-object p0, LX/74S;->NEWSFEED:LX/74S;

    invoke-virtual {v4, p0}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v4

    .line 2691475
    iput-object v6, v4, LX/9hD;->r:Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;

    .line 2691476
    move-object v4, v4

    .line 2691477
    invoke-virtual {v4}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v4

    .line 2691478
    iget-object v6, v1, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->f:LX/23R;

    iget-object p0, v1, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->b:Landroid/content/Context;

    invoke-interface {v6, p0, v4, v5}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2691479
    const v1, 0x3fe08c91

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move-object v4, v5

    .line 2691480
    goto :goto_0

    :cond_1
    move-object v6, v5

    .line 2691481
    goto :goto_1
.end method
