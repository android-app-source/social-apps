.class public Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/JQL;",
        "LX/JQM;",
        "LX/1PW;",
        "LX/JQO;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/17S;

.field public final d:LX/0gh;

.field private final e:LX/9hF;

.field public final f:LX/23R;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2691565
    const-class v0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;

    const-string v1, "photo_gallery"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/17S;LX/0gh;LX/9hF;LX/23R;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/17S;",
            "LX/0gh;",
            "LX/9hF;",
            "LX/23R;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691557
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 2691558
    iput-object p1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->b:Landroid/content/Context;

    .line 2691559
    iput-object p2, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->c:LX/17S;

    .line 2691560
    iput-object p3, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->d:LX/0gh;

    .line 2691561
    iput-object p4, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->e:LX/9hF;

    .line 2691562
    iput-object p5, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->f:LX/23R;

    .line 2691563
    iput-object p6, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->g:LX/0Or;

    .line 2691564
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;
    .locals 10

    .prologue
    .line 2691499
    const-class v1, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;

    monitor-enter v1

    .line 2691500
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691501
    sput-object v2, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691502
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691503
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691504
    new-instance v3, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/17S;->a(LX/0QB;)LX/17S;

    move-result-object v5

    check-cast v5, LX/17S;

    invoke-static {v0}, LX/0gh;->a(LX/0QB;)LX/0gh;

    move-result-object v6

    check-cast v6, LX/0gh;

    invoke-static {v0}, LX/9hF;->a(LX/0QB;)LX/9hF;

    move-result-object v7

    check-cast v7, LX/9hF;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v8

    check-cast v8, LX/23R;

    const/16 v9, 0x509

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;-><init>(Landroid/content/Context;LX/17S;LX/0gh;LX/9hF;LX/23R;LX/0Or;)V

    .line 2691505
    move-object v0, v3

    .line 2691506
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691507
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691508
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691509
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/JQO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2691556
    sget-object v0, LX/JQO;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2691535
    check-cast p2, LX/JQL;

    const/4 v6, 0x0

    .line 2691536
    iget-object v0, p2, LX/JQL;->a:LX/4ZX;

    iget-object v0, v0, LX/4ZX;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p2, LX/JQL;->a:LX/4ZX;

    iget-object v0, v0, LX/4ZX;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 2691537
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 2691538
    new-instance v0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition$1;

    invoke-direct {v0, p0, p2}, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition$1;-><init>(Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;LX/JQL;)V

    move-object v2, v0

    .line 2691539
    iget-object v0, p2, LX/JQL;->a:LX/4ZX;

    iget-object v0, v0, LX/4ZX;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->at()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 2691540
    iget-object v3, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0810e4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2691541
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->c:LX/17S;

    .line 2691542
    sget v4, LX/1TU;->i:I

    invoke-static {v0, v4}, LX/17S;->a(LX/17S;I)Z

    move-result v4

    move v0, v4

    .line 2691543
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 2691544
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->c:LX/17S;

    invoke-virtual {v0}, LX/17S;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->c:LX/17S;

    invoke-virtual {v0}, LX/17S;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2691545
    iget-object v0, p2, LX/JQL;->a:LX/4ZX;

    iget-object v0, v0, LX/4ZX;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->as()Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p2, LX/JQL;->a:LX/4ZX;

    iget-object v0, v0, LX/4ZX;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->as()Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2691546
    :goto_1
    new-instance v4, LX/JQK;

    invoke-direct {v4, p0, v0}, LX/JQK;-><init>(Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;Ljava/lang/String;)V

    move-object v0, v4

    .line 2691547
    iget-object v4, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0810e0

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v6, v0

    .line 2691548
    :goto_2
    new-instance v0, LX/JQM;

    invoke-direct/range {v0 .. v5}, LX/JQM;-><init>(LX/1aZ;Landroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2691549
    iput-object v6, v0, LX/JQM;->f:Landroid/view/View$OnClickListener;

    .line 2691550
    return-object v0

    .line 2691551
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_0

    .line 2691552
    :cond_1
    const-string v0, ""

    goto :goto_1

    .line 2691553
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f08002b

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2691554
    new-instance v0, LX/JQJ;

    invoke-direct {v0, p0}, LX/JQJ;-><init>(Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;)V

    move-object v0, v0

    .line 2691555
    move-object v6, v0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x19dea2e1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2691514
    check-cast p2, LX/JQM;

    check-cast p4, LX/JQO;

    .line 2691515
    iget-object v1, p2, LX/JQM;->a:LX/1aZ;

    .line 2691516
    iget-object v2, p4, LX/JQO;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2691517
    iget-object v1, p2, LX/JQM;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JQO;->setPhotoClickListener(Landroid/view/View$OnClickListener;)V

    .line 2691518
    iget-object v1, p2, LX/JQM;->c:Ljava/lang/String;

    .line 2691519
    iget-object v2, p4, LX/JQO;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2691520
    iget-object v1, p2, LX/JQM;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/JQO;->setInstallClickListener(Landroid/view/View$OnClickListener;)V

    .line 2691521
    iget-object v1, p2, LX/JQM;->d:Ljava/lang/String;

    .line 2691522
    iget-object v2, p4, LX/JQO;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v2, v1}, Lcom/facebook/friends/ui/SmartButtonLite;->setText(Ljava/lang/CharSequence;)V

    .line 2691523
    iget-object v1, p2, LX/JQM;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2691524
    if-eqz v1, :cond_0

    .line 2691525
    new-instance v2, LX/1Uo;

    invoke-virtual {p4}, LX/JQO;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-direct {v2, p2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object p2, LX/1Up;->c:LX/1Up;

    invoke-virtual {v2, p2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v2

    invoke-virtual {v2}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    .line 2691526
    iget-object p2, p4, LX/JQO;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {p2, v2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2691527
    iget-object v2, p4, LX/JQO;->c:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-virtual {v2, p2}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2691528
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/instagram/InstagramPromoteUnitItemPartDefinition;->c:LX/17S;

    .line 2691529
    sget v2, LX/1TU;->j:I

    invoke-static {v1, v2}, LX/17S;->a(LX/17S;I)Z

    move-result v2

    move v1, v2

    .line 2691530
    if-eqz v1, :cond_1

    .line 2691531
    iget-object v2, p4, LX/JQO;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    const p0, 0x7f0101ec

    invoke-virtual {v2, p0}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    .line 2691532
    iget-object v2, p4, LX/JQO;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {p4}, LX/JQO;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f0a00d2

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v2, p0}, Lcom/facebook/friends/ui/SmartButtonLite;->setTextColor(I)V

    .line 2691533
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x5f7bc7fd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2691534
    :cond_1
    iget-object v2, p4, LX/JQO;->e:Lcom/facebook/friends/ui/SmartButtonLite;

    const p0, 0x7f0101f8

    invoke-virtual {v2, p0}, Lcom/facebook/friends/ui/SmartButtonLite;->setStyle(I)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2691510
    check-cast p4, LX/JQO;

    const/4 v0, 0x0

    .line 2691511
    invoke-virtual {p4, v0}, LX/JQO;->setPhotoClickListener(Landroid/view/View$OnClickListener;)V

    .line 2691512
    invoke-virtual {p4, v0}, LX/JQO;->setInstallClickListener(Landroid/view/View$OnClickListener;)V

    .line 2691513
    return-void
.end method
