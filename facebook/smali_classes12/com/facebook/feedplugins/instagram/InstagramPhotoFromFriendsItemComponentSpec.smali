.class public Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field private final b:LX/11S;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1vg;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/961;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17S;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2690836
    const-class v0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;

    const-string v1, "photo_gallery"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/11S;LX/0Or;LX/1vg;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11S;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/1vg;",
            "LX/0Ot",
            "<",
            "LX/961;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17S;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/23R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2690837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2690838
    iput-object p1, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->b:LX/11S;

    .line 2690839
    iput-object p2, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->c:LX/0Or;

    .line 2690840
    iput-object p3, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->d:LX/1vg;

    .line 2690841
    iput-object p4, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->e:LX/0Ot;

    .line 2690842
    iput-object p5, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->f:LX/0Ot;

    .line 2690843
    iput-object p6, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->g:LX/0Ot;

    .line 2690844
    iput-object p7, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->h:LX/0Ot;

    .line 2690845
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;
    .locals 11

    .prologue
    .line 2690846
    const-class v1, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;

    monitor-enter v1

    .line 2690847
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2690848
    sput-object v2, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2690849
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2690850
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2690851
    new-instance v3, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v4

    check-cast v4, LX/11S;

    const/16 v5, 0x509

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v6

    check-cast v6, LX/1vg;

    const/16 v7, 0x1a38

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x76f

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xf2f

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x97

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;-><init>(LX/11S;LX/0Or;LX/1vg;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2690852
    move-object v0, v3

    .line 2690853
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2690854
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2690855
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2690856
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/4ZX;LX/1Pm;LX/1np;LX/1np;)LX/1Dg;
    .locals 12
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/4ZX;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Pm;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
            ">;",
            "LX/4ZX;",
            "LX/1Pm;",
            "LX/1np",
            "<",
            "LX/JQ1;",
            ">;",
            "LX/1np",
            "<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2690857
    iget-object v4, p3, LX/4ZX;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2690858
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2690859
    :goto_0
    if-eqz v2, :cond_1

    invoke-static {v2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v2

    move-object v3, v2

    .line 2690860
    :goto_1
    iget-object v2, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    sget-object v5, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v5

    .line 2690861
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->at()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 2690862
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2690863
    :goto_2
    iget-object v2, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->b:LX/11S;

    sget-object v6, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->A()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-interface {v2, v6, v8, v9}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v6

    .line 2690864
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->F()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 2690865
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    .line 2690866
    :goto_3
    new-instance v7, LX/JQ1;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v4, v2}, LX/JQ1;-><init>(Ljava/lang/String;Z)V

    move-object/from16 v0, p5

    invoke-virtual {v0, v7}, LX/1np;->a(Ljava/lang/Object;)V

    .line 2690867
    invoke-virtual/range {p5 .. p5}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1KL;

    new-instance v4, LX/JPz;

    invoke-direct {v4, p0, p2}, LX/JPz;-><init>(Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object/from16 v0, p4

    invoke-interface {v0, v2, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 2690868
    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, LX/1np;->a(Ljava/lang/Object;)V

    .line 2690869
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v4, 0x0

    invoke-interface {v2, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v4, 0x7f020a3c

    invoke-interface {v2, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    sget-object v5, LX/1Up;->g:LX/1Up;

    invoke-virtual {v4, v5}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b1d80

    invoke-interface {v4, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b1d80

    invoke-interface {v4, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    invoke-static {p1}, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;->d(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x2

    invoke-interface {v2, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const v5, 0x7f0b1d80

    invoke-interface {v2, v5}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const/4 v5, 0x6

    const v7, 0x7f0b0917

    invoke-interface {v2, v5, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v7, 0x0

    invoke-interface {v5, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v5, v7}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v5

    const/4 v7, 0x7

    const v8, 0x7f0b1d81

    invoke-interface {v5, v7, v8}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    const/4 v7, 0x2

    const v8, 0x7f0b0917

    invoke-interface {v5, v7, v8}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v7, 0x7f0b0050

    invoke-virtual {v3, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v7, 0x7f0a09f5

    invoke-virtual {v3, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    const/4 v7, 0x0

    invoke-virtual {v3, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004e

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a043b

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x1

    const v7, 0x7f0b1d82

    invoke-interface {v5, v6, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v5, 0x7f0a015a

    invoke-virtual {v3, v5}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v5, 0x7f0b0033

    invoke-interface {v3, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const/4 v5, 0x4

    invoke-interface {v3, v5}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    const/16 v5, 0x8

    const v6, 0x7f0b0917

    invoke-interface {v3, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    iget-object v2, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->d:LX/1vg;

    invoke-virtual {v2, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v2

    const v6, 0x7f020abc

    invoke-virtual {v2, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v6

    invoke-virtual/range {p6 .. p6}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f0a00d2

    :goto_4
    invoke-virtual {v6, v2}, LX/2xv;->j(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-static {p1}, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;->e(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    return-object v2

    .line 2690870
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2690871
    :cond_1
    const/4 v2, 0x0

    move-object v3, v2

    goto/16 :goto_1

    .line 2690872
    :cond_2
    const/4 v2, 0x0

    move-object v3, v2

    goto/16 :goto_2

    .line 2690873
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 2690874
    :cond_4
    const v2, 0x7f0a00e7

    goto :goto_4
.end method

.method public final a(Landroid/view/View;LX/4ZX;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;LX/3mj;LX/JQ1;Ljava/lang/Boolean;)V
    .locals 8
    .param p2    # LX/4ZX;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Pm;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/3mj;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/4ZX;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
            ">;",
            "LX/1Pm;",
            "LX/3mj;",
            "LX/JQ1;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 2690875
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v6

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p4, p6, v0}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 2690876
    new-instance v4, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {p2}, LX/4ZX;->a()LX/162;

    move-result-object v0

    sget-object v1, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v0, v3, v1}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    .line 2690877
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/961;

    iget-object v1, p2, LX/4ZX;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, LX/961;->a(Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/1L9;)V

    .line 2690878
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2690879
    new-array v0, v6, [Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object p3, v0, v7

    invoke-interface {p4, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2690880
    :goto_1
    return-void

    :cond_0
    move v0, v7

    .line 2690881
    goto :goto_0

    .line 2690882
    :cond_1
    new-instance v0, LX/JQ0;

    invoke-direct {v0, p0, p5, p4, p3}, LX/JQ0;-><init>(Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponentSpec;LX/3mj;LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-static {p1, v0}, LX/7kh;->a(Landroid/view/View;Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_1
.end method
