.class public Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691311
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2691312
    iput-object p1, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;->a:Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;

    .line 2691313
    iput-object p2, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;->b:Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;

    .line 2691314
    iput-object p3, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;->c:Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;

    .line 2691315
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;
    .locals 6

    .prologue
    .line 2691316
    const-class v1, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;

    monitor-enter v1

    .line 2691317
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691318
    sput-object v2, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691319
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691320
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691321
    new-instance p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;-><init>(Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;)V

    .line 2691322
    move-object v0, p0

    .line 2691323
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691324
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691325
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691326
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2691327
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2691328
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;->a:Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2691329
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;->b:Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsHScrollComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2691330
    iget-object v0, p0, Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsPartDefinition;->c:Lcom/facebook/feedplugins/instagram/InstagramPhotosFromFriendsFooterComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2691331
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2691332
    const/4 v0, 0x1

    return v0
.end method
